'use strict';

angular.module('stepApp')
    .factory('PfmsLoanAppFlowSearch', function ($resource) {
        return $resource('api/_search/pfmsLoanAppFlows/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
