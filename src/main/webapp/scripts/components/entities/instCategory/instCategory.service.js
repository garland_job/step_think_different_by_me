'use strict';

angular.module('stepApp')
    .factory('InstCategory', function ($resource, DateUtils) {
        return $resource('api/instCategorys/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createdDate = DateUtils.convertLocaleDateFromServer(data.createdDate);
                    data.updatedDate = DateUtils.convertLocaleDateFromServer(data.updatedDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.createdDate = DateUtils.convertLocaleDateToServer(data.createdDate);
                    data.updatedDate = DateUtils.convertLocaleDateToServer(data.updatedDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.createdDate = DateUtils.convertLocaleDateToServer(data.createdDate);
                    data.updatedDate = DateUtils.convertLocaleDateToServer(data.updatedDate);
                    return angular.toJson(data);
                }
            }
        });
    })
    .factory('InstCategoryByStatus', function ($resource) {
        return $resource('api/instCategorysByStat/:stat', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('InstCategoryByStatusAndType', function ($resource) {
        return $resource('api/instCategorys/byStatusAndType/:activeStatus/:instType', {}, {
            'query': { method: 'GET', isArray: true}
        });
    })

    .factory('InstLevelBytype', function ($resource) {
        return $resource('api/instCategorys/InstLevelBytype/:name/:level', {}, {
            'get': { method: 'GET', isArray: false}
        });
    });
