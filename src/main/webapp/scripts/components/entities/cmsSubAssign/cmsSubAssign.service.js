'use strict';

angular.module('stepApp')
    .factory('CmsSubAssign', function ($resource, DateUtils) {
        return $resource('api/cmsSubAssigns/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }).factory('AllSubjectInfo', function ($resource) {
        return $resource('api/cmsSubAssigns/findSubject/:tradeId', {}, {
            'get': { method: 'GET', isArray: true}
        });
    }).factory('AllCmsSubjectByTrade', function ($resource) {
        return $resource('api/cmsSubAssigns/cmsSubjectByTrade/:tradeId', {}, {
            'get': { method: 'GET', isArray: true}
        });
    });
