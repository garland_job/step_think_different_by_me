'use strict';

angular.module('stepApp')
    .factory('InstInfraInfoTemp', function ($resource, DateUtils) {
        return $resource('api/instInfraInfoTemps/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    //data.instLandTemp.mutationDate = DateUtils.convertLocaleDateToServer(data.instLandTemp.mutationDate);
                    //data.instLandTemp.receiverDate = DateUtils.convertLocaleDateToServer(data.instLandTemp.receiverDate);
                    //data.instLandTemp.mutationDate = DateUtils.convertLocaleDateToServer(data.instLandTemp.lastTaxPaymentDate);
                    //data.instLandTemp.landRegistrationDate = DateUtils.convertLocaleDateToServer(data.instLandTemp.landRegistrationDate);
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
