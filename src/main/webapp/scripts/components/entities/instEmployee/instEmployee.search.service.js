'use strict';

angular.module('stepApp')
    .factory('InstEmployeeSearch', function ($resource) {
        return $resource('api/_search/instEmployees/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    })
    .factory('InstEmployeeCode', function ($resource) {
        return $resource('api/instEmployees/code/:code', {}, {
            'get': { method: 'GET', isArray: false}
        });
    }).factory('InstEmployeeOfCurrentInstByCode', function ($resource) {
        return $resource('api/instEmployees/institute/employee/:code', {}, {
            'get': { method: 'GET', isArray: false}
        });
    }).factory('InstEmployeeOfCurrentInstByIndex', function ($resource) {
        return $resource('api/instEmployees/institute/current/employee/:index', {}, {
            'get': { method: 'GET', isArray: false}
        });
    }).factory('InstEmployeeByIndex', function ($resource) {
        return $resource('api/instEmployees/indexNo/:indexNo', {}, {
            'get': { method: 'GET', isArray: false}
        });
    }).factory('InstEmployeeOneByIndex', function ($resource) {
        return $resource('api/instEmployees/employee/:indexNo', {}, {
            'get': { method: 'GET', isArray: false}
        });
    })
    .factory('CurrentInstEmployee', function ($resource) {
        return $resource('api/instEmployees/current', {}, {
            'get': { method: 'GET', isArray: false}
        });
    })
    .factory('InstEmployeePendingList', function ($resource) {
        return $resource('api/instEmployees/pendingList/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('InstEmployeeApproveList', function ($resource) {
        return $resource('api/instEmployees/approveList/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('GovtInstEmployeeApproveList', function ($resource) {
        return $resource('api/instEmployees/govtapproveList/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('FindteacherListByDeptHead', function ($resource) {
        return $resource('api/instEmployees/findteacherListByDeptHead/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('FindteacherListByInstitute', function ($resource) {
        return $resource('api/instEmployees/findTeacherListByInstitute', {}, {
            'query': { method: 'GET', isArray: true}
        });
    })
    .factory('FindApprovedTeacherList', function ($resource) {
        return $resource('api/instEmployees/findApprovedTeacherList', {}, {
            'query': { method: 'GET', isArray: true}
        });
    })
    .factory('FindApprovedStaffList', function ($resource) {
        return $resource('api/instEmployees/findApprovedStaffList', {}, {
            'query': { method: 'GET', isArray: true}
        });
    })
    .factory('FindstaffListByDeptHead', function ($resource) {
        return $resource('api/instEmployees/findstaffListByDeptHead/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    }) .factory('FindStaffsListByInstitute', function ($resource) {
        return $resource('api/instEmployees/findStaffsListByInstitute', {}, {
            'query': { method: 'GET', isArray: true}
        });
    })
    .factory('NonGovtInstEmployeeApproveList', function ($resource) {
        return $resource('api/instEmployees/nongovtapproveList/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('InstStaffApproveList', function ($resource) {
        return $resource('api/instEmployees/staffApproveList/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('InstEmployeeDeclineList', function ($resource) {
        return $resource('api/instEmployees/declinedList/:id', {}, {
            'get': { method: 'GET', isArray: true}
        });
    }).factory('getEmplInfoByCode', function ($resource) {
      return $resource('api/instEmployees/getEmplInfoByCode/:code', {}, {
          'get': { method: 'GET', isArray: false}
      });
  }).factory('AllMpoEnlistedEmployeeOfCurrent', function ($resource) {
      return $resource('api/instEmployees/current/mpoEnlisted', {}, {
          'query': { method: 'GET', isArray: true}
      });
  }).factory('JpAdminsByInstituteId', function ($resource) {
      return $resource('api/instEmployees/jpadmin/institute/:id', {}, {
          'query': { method: 'GET', isArray: true}
      });
  }).factory('DeptHeadByDeptAndInstitute', function ($resource) {
      return $resource('api/instEmployees/institute/employeeByDeptId/:deptId', {}, {
          'query': { method: 'GET', isArray: false}
      });
  });
