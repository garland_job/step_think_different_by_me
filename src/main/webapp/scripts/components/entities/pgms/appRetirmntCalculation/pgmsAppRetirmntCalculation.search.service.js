'use strict';

angular.module('stepApp')
    .factory('PgmsAppRetirmntCalculationSearch', function ($resource) {
        return $resource('api/_search/pgmsAppRetirmntCalculations/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
