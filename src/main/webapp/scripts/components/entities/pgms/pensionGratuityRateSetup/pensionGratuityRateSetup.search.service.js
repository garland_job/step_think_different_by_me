'use strict';

angular.module('stepApp')
    .factory('PensionGratuityRateSetupSearch', function ($resource) {
        return $resource('api/_search/pgmsPenGrSetups/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
