'use strict';

angular.module('stepApp')
    .factory('PensionGratuityRateSetup', function ($resource, DateUtils) {
        return $resource('api/pensionGratuityRateSetups/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.effectiveDate = DateUtils.convertLocaleDateFromServer(data.effectiveDate);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.effectiveDate = DateUtils.convertLocaleDateToServer(data.effectiveDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.effectiveDate = DateUtils.convertLocaleDateToServer(data.effectiveDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'delete': {
                method: 'DELETE',
                transformRequest: function (data) {
                    //data.effectiveDate = DateUtils.convertLocaleDateToServer(data.effectiveDate);
                    //data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    //data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
        }).factory('PgmsPenGrRateList', function ($resource) {
              return $resource('api/pensionGratuityRateSetups/pgmsPenGrRateLists/:penGrSetId', {},{
                      'get': { method: 'GET', isArray: true}
              });
        }).factory('PensionAndGratuityRateByWorkingYear', function ($resource) {
            return $resource('api/pensionGratuityRateSetups/ByWorkingYear/:workYear', {},{
                'query': { method: 'GET', isArray: true}
            });
        }).factory('MaxVersionBySetupType', function ($resource) {
            return $resource('api/pensionGratuityRateSetups/bySetupType/:pSetupType', {},{
                'get': { method: 'GET', isArray: false}
            });
        }).factory('GetAllPensionAndGratuityRate', function ($resource) {
            return $resource('api/pensionGratuityRateSetups/getAll', {},{
                'query': { method: 'GET', isArray: true}
            });
        }).factory('GetAllVersionInfo', function ($resource) {
            return $resource('api/pensionGratuityRateSetups/getAllVersionInfo', {},{
                'query': { method: 'GET', isArray: true}
            });
        }).factory('GetAllByVersion', function ($resource) {
            return $resource('api/pensionGratuityRateSetups/getAllByVersion/:version', {},{
                'query': { method: 'GET', isArray: true}
            });
        }).factory('GetAllByVersionAndSetupType', function ($resource) {
            return $resource('api/pensionGratuityRateSetups/getAllByVersionAndSetupType/:version/:setupType', {},{
                'query': { method: 'GET', isArray: true}
            });
        });
