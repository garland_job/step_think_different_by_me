'use strict';

angular.module('stepApp')
    .factory('PgmsAppRetirmntPen', function ($resource, DateUtils) {
        return $resource('api/pgmsAppRetirmntPens/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.appDate = DateUtils.convertLocaleDateFromServer(data.appDate);
                    data.aprvDate = DateUtils.convertLocaleDateFromServer(data.aprvDate);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.appDate = DateUtils.convertLocaleDateToServer(data.appDate);
                    data.aprvDate = DateUtils.convertLocaleDateToServer(data.aprvDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.appDate = DateUtils.convertLocaleDateToServer(data.appDate);
                    data.aprvDate = DateUtils.convertLocaleDateToServer(data.aprvDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    }).factory('HrNomineeInfosByEmpId', function ($resource) {
        return $resource('api/HrRetirmntNminesInfos/:empId', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    }).factory('RetirementNomineeInfosByPenId', function ($resource) {
          return $resource('api/pgmsAppRetirmntPens/retirementNomineesInfos/:penId', {},
          {
              'get': { method: 'GET', isArray: true}
          });
    }).factory('DeleteRetirementNomineeInfosByPenId', function ($resource) {
          return $resource('api/deleteRetirementNomineesInfos/:penId', {},
          {
              'get': { method: 'GET'}
          });
    }).factory('PgmsAppRetirementAttachByTypeAndPen', function ($resource) {
          return $resource('api/pgmsAppRetirmntPens/byTypeAndPen/:attacheType/:retirementPenId', {},
          {
              'query': { method: 'GET', isArray: true}
          });
    }).factory('DeleteRetirementAttachInfosByPenId', function ($resource) {
          return $resource('api/deleteRetirementAttachInfos/:penId', {},
          {
              'get': { method: 'GET'}
          });
    }).factory('PgmsAppRetirementPending', function ($resource) {
          return $resource('api/pgmsAppRetirmntPens/applicationPendingList/:statusType', {},
          {
              'query': { method: 'GET', isArray: true }
          });
    }).factory('PgmsAppRetirementAttachByPenId', function ($resource) {
        return $resource('api/pgmsAppRetirmntPensByPenId/:retirementPenId', {},
        {
            'get': { method: 'GET', isArray: true }
        });
    }).factory('BankInfosByEmpId', function ($resource) {
        return $resource('api/pgmsAppRetirmntPens/bankInfoByEmpId/:empId', {},
        {
            'query': { method: 'GET' ,isArray: true}
        });
    }).factory('EmpRetirmentAppByEmpId', function ($resource) {
        return $resource('api/empRetirmentAppByEmpId/:empId', {},
        {
            'get': { method: 'GET', isArray: true  }
        });
    }).factory('CalculationInfosByPenId', function ($resource) {
        return $resource('api/retirmentCalculationInfos/:pensionId', {},
        {
              'get': { method: 'GET' }
        });
    }).factory('DeleteRetirementCaculationInfosByPenId', function ($resource) {
        return $resource('api/deleteRetirementCalculationInfos/:penId', {},
        {
             'get': { method: 'GET' }
        });
    }).factory('CalculationLastPayCodeInfoByEmpId', function ($resource) {
        return $resource('api/calculationLastPayCodeInfoByEmpId/:empId', {},
        {
           'get': { method: 'GET', isArray: true }
        });
    }).factory('AllForwaringLists', function ($resource) {
        return $resource('api/pgmsAppRetirmntPens/forwardToList/:type', {}, {
            'query': { method: 'GET', isArray: true}
        });
    }).factory('ForwardRetirePensionApplication', function ($resource, DateUtils) {
        return $resource('api/pgmsAppRetirmntPens/forward/:forwardTo', {}, {
            'forward': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.date = DateUtils.convertLocaleDateToServer(data.date);
                    return angular.toJson(data);
                }
            }

        });
    }).factory('PgmsRetPensionAppByStatus', function ($resource) {
        return $resource('api/pgmsAppRetirmntPens/applicationListByApproveStatus/:statusType', {},
            {
                'query': { method: 'GET', isArray: true }
            });
    }).factory('PensionAppApprovedListByRole', function ($resource) {
        return $resource('api/pgmsAppRetirmntPens/approvedListByRole', {},
            {
                'query': { method: 'GET', isArray: true }
            });
    });
