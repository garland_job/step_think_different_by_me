'use strict';

angular.module('stepApp')
    .factory('SisStudentInfo', function ($resource, DateUtils) {
        return $resource('api/sisStudentInfos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    })
    .factory('SisStudentInfoGovt', function ($resource) {
        return $resource('api/sisStudentInfos/findStudentInfoUser', {}, {
            'query': {method: 'GET', isArray: true}
        })
    })
    .factory('instituteStudentInfo', function ($resource) {
          return $resource('api/sisStudentInfos/findStudentInfoById/:id', {}, {
                            'get': { method: 'GET', isArray: false}
          });
    })
    .factory('sisStudentInfoInstituteWise', function ($resource) {
                return $resource('api/sisStudentInfos/findStudentInfoByInstitute/:id', {}, {
               'get': { method: 'GET', isArray: false}
           });
    })

    .factory('StudentListByIdFilter', function ($resource) {
    return $resource('api/sisStudentInfos/getAllStudentListByIdFilter/:mobileNo', {},
        {
            'query': {method: 'GET', isArray: true}
        });
});
