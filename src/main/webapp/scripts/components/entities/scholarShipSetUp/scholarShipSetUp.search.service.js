'use strict';

angular.module('stepApp')
    .factory('ScholarShipSetUpSearch', function ($resource) {
        return $resource('api/_search/scholarShipSetUps/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
