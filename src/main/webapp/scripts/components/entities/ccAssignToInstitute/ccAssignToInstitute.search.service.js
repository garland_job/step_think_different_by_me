'use strict';

angular.module('stepApp')
    .factory('CcAssignToInstituteSearch', function ($resource) {
        return $resource('api/_search/ccAssignToInstitutes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
