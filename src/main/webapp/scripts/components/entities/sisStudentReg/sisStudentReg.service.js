'use strict';

angular.module('stepApp')
    .factory('SisStudentReg', function ($resource, DateUtils) {
        return $resource('api/sisStudentRegs/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    })
    .factory('SisStudentRegByAppId', function ($resource, DateUtils) {
        return $resource('api/sisStudentRegs/appid/:applicationId', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.mobileNo = '0'+parseInt(data.mobileNo);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    })
   /* .factory('SisStudentRegInfo', function ($resource) {
        return $resource('api/sisStudentRegs/findSisStudentInfoUser', {}, {
            'query': {method: 'GET', isArray: true}
        })
    })*/
    .factory('InstituteInformation', function ($resource) {
        return $resource('api/sisStudentRegs/findInstituteId/:code', {}, {
            'get': { method: 'GET', isArray: false}
        });
    })
    .factory('TradeInformation', function ($resource) {
        return $resource('api/sisStudentRegs/findTradeId/:cmsCurriculumId', {}, {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('AllCurriculumInfo', function ($resource) {
        return $resource('api/sisStudentRegs/findCurriculumId/:instituteId', {}, {
            'get': { method: 'GET', isArray: true}
        });
    });

