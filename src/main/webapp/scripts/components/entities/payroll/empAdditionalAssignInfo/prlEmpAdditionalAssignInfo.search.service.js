'use strict';

angular.module('stepApp')
    .factory('PrlEmpAdditionalAssignInfoSearch', function ($resource) {
        return $resource('api/_search/prlEmpAdditionalAssignInfos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
