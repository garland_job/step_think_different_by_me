'use strict';

angular.module('stepApp')
    .factory('ErpAuthFlowProcessSearch', function ($resource) {
        return $resource('api/_search/erpAuthFlowProcesss/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
