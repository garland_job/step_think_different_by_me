'use strict';

angular.module('stepApp')
    .factory('InstEmpSalaryAdjSearch', function ($resource) {
        return $resource('api/_search/instEmpSalaryAdjs/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
