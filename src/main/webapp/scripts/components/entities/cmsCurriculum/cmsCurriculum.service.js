/*
 'use strict';

 angular.module('stepApp')
 .factory('CmsCurriculum', function ($resource, DateUtils) {
 return $resource('api/cmsCurriculums/:id', {}, {
 'query': { method: 'GET', isArray: true},
 'get': {
 method: 'GET',
 transformResponse: function (data) {
 data = angular.fromJson(data);
 return data;
 }
 },
 'update': { method:'PUT'}
 });
 });
 */

'use strict';

angular.module('stepApp')
    .factory('CmsCurriculum', function ($resource, DateUtils) {
        return $resource('api/cmsCurriculums/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': {method: 'PUT'}
        });
    })
    .factory('CmsCurriculumQuery', function ($resource, DateUtils) {
        return $resource('api/cmsCurriculums/findAllcmsCurriculumByOrderID/', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('FindActivecmsCurriculums', function ($resource) {
    return $resource('api/cmsCurriculums/allActivecmsCurriculums', {}, {
        'query': {method: 'GET', isArray: true}
    });
    })
    .factory('CmsCurriculumSearch', function ($resource) {
        return $resource('api/_search/cmsCurriculums/:query', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('CmsCurriculumByCode', function ($resource) {
        return $resource('api/cmsCurriculums/code/:code', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })

    .factory('CmsCurriculumByName', function ($resource) {
        return $resource('api/cmsCurriculums/name/:name', {}, {
            'query': {method: 'GET', isArray: true}
        });
    })
    .factory('findCmsCurriculumByInstituteIIS', function ($resource) {
        return $resource('api/cmsCurriculums/findCmsCurriculumByInstituteIIS/:id', {}, {
            'get': {method: 'GET', isArray: true}
        });
    })

    .factory('CmsCurriculumByCurrentInst', function ($resource) {
        return $resource('api/iisCurriculumInfos/curriculum/currentInstitute', {}, {
            'query': {method: 'GET', isArray: true}
        });
    });
