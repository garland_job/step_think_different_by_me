'use strict';

angular.module('stepApp')
    .factory('SecurityConfigurationTableSearch', function ($resource) {
        return $resource('api/_search/securityConfigurationTables/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
