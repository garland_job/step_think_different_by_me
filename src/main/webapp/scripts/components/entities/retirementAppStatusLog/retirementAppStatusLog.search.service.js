'use strict';

angular.module('stepApp')
    .factory('RetirementAppStatusLogSearch', function ($resource) {
        return $resource('api/_search/retirementAppStatusLogs/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
