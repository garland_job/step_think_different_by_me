'use strict';

angular.module('stepApp')
    .factory('MaritalInfoSearch', function ($resource) {
        return $resource('api/_search/maritalInfos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
