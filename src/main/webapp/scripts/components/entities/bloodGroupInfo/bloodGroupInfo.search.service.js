'use strict';

angular.module('stepApp')
    .factory('BloodGroupInfoSearch', function ($resource) {
        return $resource('api/_search/bloodGroupInfos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
