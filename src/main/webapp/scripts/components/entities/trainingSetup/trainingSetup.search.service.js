'use strict';

angular.module('stepApp')
    .factory('TrainingSetupSearch', function ($resource) {
        return $resource('api/_search/trainingSetups/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
