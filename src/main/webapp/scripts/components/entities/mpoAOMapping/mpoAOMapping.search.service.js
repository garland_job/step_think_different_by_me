'use strict';

angular.module('stepApp')
    .factory('MpoAOMappingSearch', function ($resource) {
        return $resource('api/_search/mpoAOMappings/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
