'use strict';

angular.module('stepApp')
    .factory('DmlAuditLogHistorySearch', function ($resource) {
        return $resource('api/_search/dmlAuditLogHistorys/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
