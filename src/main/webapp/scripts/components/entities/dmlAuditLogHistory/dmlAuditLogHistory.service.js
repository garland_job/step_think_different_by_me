'use strict';

angular.module('stepApp')
    .factory('DmlAuditLogHistory', function ($resource, DateUtils) {
        return $resource('api/dmlAuditLogHistorys/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.eventDttm = DateUtils.convertLocaleDateFromServer(data.eventDttm);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.eventDttm = DateUtils.convertLocaleDateToServer(data.eventDttm);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.eventDttm = DateUtils.convertLocaleDateToServer(data.eventDttm);
                    return angular.toJson(data);
                }
            }
        });
    });
