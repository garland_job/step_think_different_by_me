'use strict';

angular.module('stepApp')
    .factory('ErpAuthorityFlowSearch', function ($resource) {
        return $resource('api/_search/erpAuthorityFlows/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
