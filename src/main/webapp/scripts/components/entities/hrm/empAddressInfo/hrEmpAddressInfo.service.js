'use strict';

angular.module('stepApp')
    .factory('HrEmpAddressInfo', function ($resource, DateUtils) {
        return $resource('api/hrEmpAddressInfos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'approve': {method: 'POST'}
        });
    }).factory('HrEmpAddressInfoApprover', function ($resource) {
        return $resource('api/hrEmpAddressInfosApprover/:id', {},
            {
                'update': { method: 'POST'},
                'query': { method: 'GET', isArray: true}
            });
    }).factory('HrEmpAddressInfoApproverLog', function ($resource) {
        return $resource('api/hrEmpAddressInfosApprover/log/:entityId', {},
            {
                'update': { method: 'POST'},
                'query': { method: 'GET', isArray: true}
            });
    })
    .factory('HrEmpAddressInfoDeptHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrEmpAddressInfoDeptHeadPendingApprovalList/:deptIds', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpAddressInfoInstHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrEmpAddressInfoInstituteHeadPendingApprovalList/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpAddressInfoAdminApprovalList', function ($resource)
    {
        return $resource('api/hrEmpAddressInfoAdminApprovalListByLogStatus/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpAddressInfoUpdateApprover', function ($resource)
    {
        return $resource('api/hrEmpAddressInfoUpdateRequestApprove/', {},
        {
            'update': { method: 'POST'}
        });
    });

