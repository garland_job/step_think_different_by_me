'use strict';

angular.module('stepApp')
    .factory('HrEmpAcrInfo', function ($resource, DateUtils) {
        return $resource('api/hrEmpAcrInfos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.acrDate = DateUtils.convertLocaleDateFromServer(data.acrDate);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.acrDate = DateUtils.convertLocaleDateToServer(data.acrDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.acrDate = DateUtils.convertLocaleDateToServer(data.acrDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    })
    .factory('HrEmpAcrInfoDeptHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrEmpAcrInfoDeptHeadPendingApprovalList/:deptIds', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpAcrInfoInstHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrEmpAcrInfoInstituteHeadPendingApprovalList/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpAcrInfoApprover', function ($resource) {
        return $resource('api/hrEmpAcrInfosApprover/:id', {},
        {
            'update': { method: 'POST'},
            'query': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpAcrInfoUpdateApprover', function ($resource)
    {
        return $resource('api/hrEmpAcrInfoUpdateRequestApprove/', {},
        {
            'update': { method: 'POST'}
        });
    });
