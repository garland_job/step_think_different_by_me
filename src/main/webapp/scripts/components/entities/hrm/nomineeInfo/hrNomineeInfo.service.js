'use strict';

angular.module('stepApp')
    .factory('HrNomineeInfo', function ($resource, DateUtils) {
        return $resource('api/hrNomineeInfos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.birthDate = DateUtils.convertLocaleDateFromServer(data.birthDate);
                    data.actionDate = DateUtils.convertLocaleDateFromServer(data.actionDate);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.birthDate = DateUtils.convertLocaleDateToServer(data.birthDate);
                    data.actionDate = DateUtils.convertLocaleDateToServer(data.actionDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.birthDate = DateUtils.convertLocaleDateToServer(data.birthDate);
                    data.actionDate = DateUtils.convertLocaleDateToServer(data.actionDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    }).factory('HrNomineeInfoApprover', function ($resource) {
        return $resource('api/hrNomineeInfosApprover/:id', {},
            {
                'update': { method: 'POST'},
                'query': { method: 'GET', isArray: true}
            });
    }).factory('HrNomineeInfoApproverLog', function ($resource) {
        return $resource('api/hrNomineeInfosApprover/log/:entityId', {},
            {
                'update': { method: 'POST'},
                'query': { method: 'GET', isArray: true}
            });
    }).factory('HrNomineeInfoByNomineeNidAndEmpId', function ($resource) {
        return $resource('api/hrNomineeInfos/byNomineeNidAndEmpId/:nid/:empId', {},{
            'get': { method: 'GET', isArray: false}
        });
    })
    .factory('HrNomineeInfoDeptHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrNomineeInfoDeptHeadPendingApprovalList/:deptIds', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrNomineeInfoInstHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrNomineeInfoInstituteHeadPendingApprovalList/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrNomineeInfoAdminApprovalList', function ($resource)
    {
        return $resource('api/hrNomineeInfoAdminApprovalListByLogStatus/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrNomineeInfoUpdateApprover', function ($resource)
    {
        return $resource('api/hrNomineeInfoUpdateRequestApprove/', {},
        {
            'update': { method: 'POST'}
        });
    });
