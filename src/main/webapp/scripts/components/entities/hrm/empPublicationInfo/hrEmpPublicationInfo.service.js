'use strict';

angular.module('stepApp')
    .factory('HrEmpPublicationInfo', function ($resource, DateUtils) {
        return $resource('api/hrEmpPublicationInfos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.publicationDate = DateUtils.convertLocaleDateFromServer(data.publicationDate);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.publicationDate = DateUtils.convertLocaleDateToServer(data.publicationDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.publicationDate = DateUtils.convertLocaleDateToServer(data.publicationDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    }).factory('HrEmpPublicationInfoApprover', function ($resource) {
        return $resource('api/hrEmpPublicationInfosApprover/:id', {},
            {
                'update': { method: 'POST'},
                'query': { method: 'GET', isArray: true}
            });
    }).factory('HrEmpPublicationInfoApproverLog', function ($resource) {
        return $resource('api/hrEmpPublicationInfosApprover/log/:entityId', {},
            {
                'update': { method: 'POST'},
                'query': { method: 'GET', isArray: true}
            });
    })
    .factory('HrEmpPublicationInfoDeptHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrEmpPublicationInfoDeptHeadPendingApprovalList/:deptIds', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpPublicationInfoInstHeadPendingApprovalList', function ($resource)
    {
        return $resource('api/hrEmpPublicationInfoInstituteHeadPendingApprovalList/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpPublicationInfoAdminApprovalList', function ($resource)
    {
        return $resource('api/hrEmpPublicationInfoAdminApprovalListByLogStatus/', {},
        {
            'get': { method: 'GET', isArray: true}
        });
    })
    .factory('HrEmpPublicationInfoUpdateApprover', function ($resource)
    {
        return $resource('api/hrEmpPublicationInfoUpdateRequestApprove/', {},
        {
            'update': { method: 'POST'}
        });
    });
