'use strict';

angular.module('stepApp')
    .factory('BloodGroupSearch', function ($resource) {
        return $resource('api/_search/bloodGroups/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
