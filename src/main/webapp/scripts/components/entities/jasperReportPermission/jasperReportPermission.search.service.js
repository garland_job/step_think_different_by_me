'use strict';

angular.module('stepApp')
    .factory('JasperReportPermissionSearch', function ($resource) {
        return $resource('api/_search/jasperReportPermissions/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
