'use strict';

angular.module('stepApp')
    .factory('AlmAttendanceInformation', function ($resource, DateUtils) {
        return $resource('api/almAttendanceInformations/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);

                    data.attendanceDate = DateUtils.convertLocaleDateFromServer(data.attendanceDate);
                    data.processDate = DateUtils.convertLocaleDateFromServer(data.processDate);
                    data.createDate = DateUtils.convertLocaleDateFromServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateFromServer(data.updateDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.attendanceDate = DateUtils.convertLocaleDateFromServer(data.attendanceDate);
                    data.processDate = DateUtils.convertLocaleDateToServer(data.processDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.attendanceDate = DateUtils.convertLocaleDateFromServer(data.attendanceDate);
                    data.processDate = DateUtils.convertLocaleDateToServer(data.processDate);
                    data.createDate = DateUtils.convertLocaleDateToServer(data.createDate);
                    data.updateDate = DateUtils.convertLocaleDateToServer(data.updateDate);
                    return angular.toJson(data);
                }
            }
        });
    });
