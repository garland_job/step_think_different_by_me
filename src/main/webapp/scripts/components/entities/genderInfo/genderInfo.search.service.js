'use strict';

angular.module('stepApp')
    .factory('GenderInfoSearch', function ($resource) {
        return $resource('api/_search/genderInfos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
