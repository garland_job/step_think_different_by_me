'use strict';

angular.module('stepApp')
    .factory('JasperReportSecuritySearch', function ($resource) {
        return $resource('api/_search/jasperReportSecuritys/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
