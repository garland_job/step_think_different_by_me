'use strict';

angular.module('stepApp')
    .factory('User', function ($resource) {
        return $resource('api/users/:login', {}, {
                'query': {method: 'GET', isArray: true},
                'get': {
                    method: 'GET',
                    isArray:false,
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        return data;
                    }
                },
                'update': { method:'PUT' }
            });
        }).factory('UserCustomUpdate', function ($resource) {
        return $resource('api/users/customUpdate', {}, {
                'update': { method:'PUT' }
            });
        }).factory('UserBlocked', function ($resource) {
        return $resource('api/blockusers', {}, {
            'query': {method: 'GET', isArray: true}
            /*,
            'get': {
                method: 'GET',
                isArray:false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }*/
            });
        })
        .factory('UsersBySearchItems', function ($resource) {
                return $resource('api/users/getUsersBySearch/:searchCategory/:searchItem', {}, {
                    'query': {method: 'GET', isArray: true}
                });
            })
            .factory('UpdateUsersByLoginAndPassword', function ($resource) {
                return $resource('api/users/updateUserByEntity/:userLogin/:userPassword', {}, {
                    'query': {method: 'GET', isArray: true}
                });
            })
            .factory('UsersByAuthority', function ($resource) {
                return $resource('api/users/getUserByAuthority/:userbyauthority', {}, {
                    'query': {method: 'GET', isArray: true}
                });
            })
            .factory('UsersByLogin', function ($resource) {
                return $resource('api/usersByAffiliation/:login', {}, {
                    'get': {method: 'GET', isArray: false}
                })
            })
            .factory('UsersByIds', function ($resource) {
              return $resource('api/users/getUsers/:id', {}, {
                  'get': {method: 'GET', isArray: false}
                });
            })
            .factory('UserListByLogin', function ($resource) {
                return $resource('api/users/getAllUserListByLogin/:login', {},
                    {
                        'query': {method: 'GET', isArray: true}
                    });
            })
            .factory('UserAuthoritiesByUserId',function ($resource) {
                return $resource('api/users/findAuthorities/:userId',{},
                {
                  'query': {method: 'GET', isArray: true}
                });
            })
            .factory('RemoveSingleAuthority',function ($resource) {
                return $resource('api/users/removeAuthority/:userId/:rights',{},{
                    'query': {method: 'GET', isArray: false}
                });
            });

'use strict';

angular.module('stepApp')
    .factory('UsersSearch', function ($resource) {
        return $resource('api/_search/users/:query', {}, {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    });
