'use strict';
//
//angular.module('stepApp')
//    .controller('JobPortalController',
//        ['$scope', '$state', 'Principal', 'User', 'UsersSearch', 'ParseLinks',
//        function ($scope, $state, Principal, User, UsersSearch, ParseLinks) {
//
//        $scope.users = [];
//        $scope.authorities = ["ROLE_USER", "ROLE_ADMIN", "ROLE_EMPLOYER"];
//        $scope.isAuthenticated = Principal.isAuthenticated;
//        $scope.userRole = 'ROLE_USER';
//
//        Principal.identity().then(function (account) {
//            $scope.account = account;
//
//            if($scope.isInArray('ROLE_ADMIN', $scope.account.authorities))
//            {
//                $scope.userRole = 'ROLE_ADMIN';
//            }
//            else if($scope.isInArray('ROLE_EMPLOYER', $scope.account.authorities))
//            {
//                $scope.userRole = 'ROLE_EMPLOYER';
//            }
//            else if($scope.isInArray('ROLE_USER', $scope.account.authorities))
//            {
//                $scope.userRole = 'ROLE_USER';
//            }
//        });
//
//        $scope.isInArray = function isInArray(value, array) {
//            return array.indexOf(value) > -1;
//        };
//
//    }]);

angular.module('stepApp')
    .controller('JobPortalController',
        ['$scope', '$stateParams', '$filter', '$state','AvailableJobs', 'Principal', 'JobSearch', 'CurJobSearchLocation','JobCategoriesWithCounter',
            function ($scope, $stateParams, $filter, $state,AvailableJobs, Principal, JobSearch, CurJobSearchLocation, JobCategoriesWithCounter) {
                Principal.identity().then(function (account) {
                    $scope.account = account;
                    $scope.isAuthenticated = Principal.isAuthenticated;
                });

                $scope.time = 0;
                $scope.total = 0;
                $scope.page = 0;
                $scope.per_page = 20;
                $scope.jobs = [];
                $scope.cats = [];
                $scope.date = $filter('date')(new Date(),'MMM dd, yyyy');

                JobCategoriesWithCounter.query({page: 0, size: 500}, function (result, headers) {
                    $scope.cats = result;
                });

                $scope.submit = function () {
                    var form = $scope.form;
                    $scope.submitted = true;
                    if (form.$valid) {
                        var searchData = {'q': $scope.q};
                        $state.go('search', searchData);
                    }
                };
                AvailableJobs.query({page: 0, size: 2000}, function(result){
                    if(result.length > 0){
                        angular.forEach(result, function(data){
                            var leftDay = Math.round(dateCompare(new Date(data.applicationDeadline)));
                            data.leftDays = leftDay;
                            //show alert for jobs when deadline left 3 days
                            if(leftDay < 4){
                                data.showAlert = true;
                            }
                            $scope.jobs.push(data);
                        })
                    }
                });

                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                $scope.sector = function () {
                    JobSearch.query({query: 'type:*'}, function (result) {
                        $scope.jobs = result.slice(0, 8);
                        $scope.total = result.length;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.location = function () {
                    CurJobSearchLocation.query({location: 'bangladesh'}, function (result) {
                        $scope.jobs = result.slice(0, 8);
                        $scope.total = result.length;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.entry = function () {
                    JobSearch.query({query: 'type:entry'}, function (result) {
                        $scope.jobs = result.slice(0, 8);
                        $scope.total = result.length;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.part_time = function () {
                    JobSearch.query({query: 'type:part_time'}, function (result) {
                        $scope.jobs = result.slice(0, 8);
                        $scope.total = result.length;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });

                };
                $scope.company = function () {
                    JobSearch.query({query: 'employer.name:*'}, function (result) {
                        $scope.jobs = result.slice(0, 8);
                        $scope.total = result.length;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                function  dateCompare (deadline) {
                    var d2 = deadline;
                    var d1 = new Date();
                    var miliseconds = d2-d1;
                    var seconds = miliseconds/1000;
                    var minutes = seconds/60;
                    var hours = minutes/60;
                    var days = hours/24;
                    console.log("difference :"+days);
                    return days+1;
                };

            }]);

/* jp-dashboard.controller.js */

angular.module('stepApp')
    .controller('JpDashboardController',
        ['$scope', '$state', 'Principal','DataUtils','JpEmployee', 'Employer', 'TempEmployer','TempEmployerSearch','ParseLinks','MyFirstEmploymentHistory',
            function ($scope, $state, Principal,DataUtils,JpEmployee, Employer, TempEmployer,TempEmployerSearch, ParseLinks,MyFirstEmploymentHistory) {


                $scope.users = [];
                $scope.authorities = ["ROLE_USER", "ROLE_ADMIN", "ROLE_EMPLOYER","ROLE_JPADMIN","ROLE_JOBSEEKER"];
                $scope.isAuthenticated = Principal.isAuthenticated;
                $scope.userRole = 'ROLE_JOBSEEKER';
                $scope.searchQueryOrganization = "";
                $scope.jpEmployee = {};

                Principal.identity().then(function (account) {
                    $scope.account = account;
                    console.log("sdasd :"+$scope.account.authorities);
                    if($scope.isInArray('ROLE_ADMIN', $scope.account.authorities)){
                        $scope.userRole = 'ROLE_ADMIN';
                    }else if($scope.isInArray('ROLE_EMPLOYER', $scope.account.authorities)){
                        $scope.userRole = 'ROLE_EMPLOYER';
                    }else if($scope.isInArray('ROLE_JPADMIN', $scope.account.authorities)){
                        $scope.userRole = 'ROLE_JPADMIN';
                    }else if($scope.isInArray('ROLE_USER', $scope.account.authorities)){
                        $scope.userRole = 'ROLE_USER';
                        MyFirstEmploymentHistory.get({},function(result) {
                            if(result != null){
                                $scope.jpEmployee = result.jpEmployee;
                                $scope.jpEmployee.totalExperience = $scope.calculateAge(result.startFrom);
                                JpEmployee.update($scope.jpEmployee);
                            }
                        });
                    }else if($scope.isInArray('ROLE_JOBSEEKER', $scope.account.authorities))
                    {
                        $scope.userRole = 'ROLE_JOBSEEKER';
                        MyFirstEmploymentHistory.get({},function(result) {
                            if(result != null){
                                $scope.jpEmployee = result.jpEmployee;
                                $scope.jpEmployee.totalExperience = $scope.calculateAge(result.startFrom);
                                JpEmployee.update($scope.jpEmployee);
                            }
                        });
                    }
                });

                $scope.calculateAge = function(birthday) {
                    var ageDifMs = Date.now() - new Date(birthday);
                    var ageDate = new Date(ageDifMs);
                    return Math.abs(ageDate.getUTCFullYear() - 1970);
                };

                $scope.isInArray = function isInArray(value, array) {
                    return array.indexOf(value) > -1;
                };

                $scope.tempEmployers = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    Employer.query({page: $scope.page, size: 5000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.tempEmployers = result;
                        $scope.total = headers('x-total-count');
                    });
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();
                $scope.search = function () {
                    console.log("search query :"+$scope.searchQueryOrganization);
                    TempEmployerSearch.query({query: $scope.searchQueryOrganization}, function(result) {
                        $scope.tempEmployers = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };
                $scope.search2 = function (s) {
                    console.log("search query :"+s);
                    TempEmployerSearch.query({query: s}, function(result) {
                        $scope.tempEmployers = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.tempEmployer = {
                        name: null,
                        alternativeCompanyName: null,
                        contactPersonName: null,
                        personDesignation: null,
                        contactNumber: null,
                        companyInformation: null,
                        address: null,
                        city: null,
                        zipCode: null,
                        companyWebsite: null,
                        industryType: null,
                        businessDescription: null,
                        companyLogo: null,
                        companyLogoContentType: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;

                // bulk operations start
                $scope.areAllTempEmployersSelected = false;

                $scope.updateTempEmployersSelection = function (TempEmployerArray, selectionValue) {
                    for (var i = 0; i < TempEmployerArray.length; i++)
                    {
                        TempEmployerArray[i].isSelected = selectionValue;
                    }
                };

                $scope.order = function (predicate, reverse) {
                    $scope.predicate = predicate;
                    $scope.reverse = reverse;
                    TempEmployer.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.tempEmployers = result;
                        $scope.total = headers('x-total-count');
                    });
                };
                // bulk operations end
            }]);




