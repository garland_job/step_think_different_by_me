'use strict';

angular.module('stepApp')
    .controller('JPReportController',
        ['$rootScope', '$scope', '$sce', '$stateParams', '$filter', '$location', '$state', 'Country', 'Principal', 'ParseLinks', 'GetAllJasperReportByModule', 'GetJasperParamByJasperReport', 'Institute', 'OrganizationType', 'Employer', 'OrganizationCategory', 'Division', 'District', 'Upazila', 'hrDepartmentSetupsFindAllOrganization', 'DlContCatSet', 'GovernmentInstitutes', 'findAllCurriculumByInstituteId', 'cmsTradesByCurriculum',
            'DlSourceSetUp', 'DlContSCatSet', 'DlContTypeSet', 'HrDepartmentHeadSetup', 'DlBookInfo', 'DlFileType', 'GetCountry', 'HrDesignationHeadSetup', 'MiscTypeSetup', 'HrEmplTypeInfo', 'HrEmpTransferInfo', 'MiscTypeSetupByCategory', 'JobType', 'Cat', 'Job', 'HrGazetteSetup', 'HrGazetteSetupCustomListByStatus', 'HrDepartmentSetup', 'DepartmentsByOrgType',

            function ($rootScope, $scope, $sce, $stateParams, $filter, $location, $state, Country, Principal, ParseLinks, GetAllJasperReportByModule, GetJasperParamByJasperReport, Institute, OrganizationType, Employer, OrganizationCategory, Division, District, Upazila, hrDepartmentSetupsFindAllOrganization, DlContCatSet, GovernmentInstitutes, findAllCurriculumByInstituteId, cmsTradesByCurriculum,
                      DlSourceSetUp, DlContSCatSet, DlContTypeSet, HrDepartmentHeadSetup, DlBookInfo, DlFileType, GetCountry, HrDesignationHeadSetup, MiscTypeSetup, HrEmplTypeInfo, HrEmpTransferInfo, MiscTypeSetupByCategory, JobType, Cat, Job, HrGazetteSetup, HrGazetteSetupCustomListByStatus, HrDepartmentSetup, DepartmentsByOrgType) {

                $scope.dataList = [];
                $scope.curriculumByInst = [];
                $scope.flag = 0;
                $scope.DepartmentLists = [];

                Country.query({size: 500}, function (result) {
                    $scope.countries =result;
                    console.log("countries");
                    console.log($scope.countries);
                    console.log("countries");
                });


                $scope.typeheadCascade = function (result) {
                    console.log("typeheadCascade");
                    console.log(result);
                    findAllCurriculumByInstituteId.get({id: result.id}, function (result) {
                        $scope.curriculumByInsts = result;
                    });
                    console.log($scope.curriculumByInst);
                };
                $scope.getTradeByCurriculum = function (s) {
                    console.log("tradeByCurriculum");
                    console.log("tradeByCurriculum");
                    console.log(s);
                    cmsTradesByCurriculum.get({id: s.id}, function (result) {
                        $scope.tradeByCurriculum = result;
                    });
                };
                HrGazetteSetupCustomListByStatus.query({stat: true}, function (data) {
                    console.log('>>>>>>>>>>>>>>>>>>>>>data');
                    console.log(data);
                    console.log('>>>>>>>>>>>>>>>>>>>>>data');
                    $scope.gazetteNames = data;
                });

                // HrDepartmentSetup.query({stat: true}, function (pOrganizationType) {
                //
                //     $scope.gazetteNames = data;
                // });
                hrDepartmentSetupsFindAllOrganization.get({}, function (deptData) {

                    angular.forEach(deptData,function(dept){
                        $scope.DepartmentLists.push(dept.departmentInfo.departmentName);
                    });
                    // console.log("This is my call");
                    // console.log(deptData);
                    // console.log("This is my call");
                });


                GovernmentInstitutes.query({}, function (data) {
                    console.log('---------------------');
                    console.log(data);
                    $scope.dataList = data;
                });


                Principal.identity().then(function (account) {
                    $scope.account = account;
                    $scope.isAuthenticated = Principal.isAuthenticated;
                });
                $scope.activeInactives = [
                    {id: 1, name: "true", value: "Active"},
                    {id: 2, name: "false", value: "InActive"}
                ];
                $scope.genders = [
                    {id: 1, value: 'Male'},
                    {id: 0, value: 'Female'}
                ];
                $scope.empTypes = [
                    {id: 1, name: "DTE_Employee", value: "DTE Employee"},
                    {id: 2, name: "Staff", value: "Staff"},
                    {id: 3, name: "Teacher", value: "Teacher"}
                ];
                $scope.orgTypes = [
                    {id: 1, name: "Organization", value: "Organization"},
                    {id: 2, name: "Institute", value: "Institute"},
                ];
                $scope.instTypes = [
                    {id: 1, name: "Government", value: "Government"},
                    {id: 2, name: "NonGovernment", value: "NonGovernment"},
                ];

                $scope.instEmpTypes = [
                    {id: 1, name: "Officer", value: "Officer"},
                    {id: 2, name: "Staff", value: "Staff"},
                    {id: 3, name: "Teacher", value: "Teacher"}
                ];

                $scope.desigTypes = [
                    {id: 1, name: "HRM", value: "HRM"},
                    {id: 2, name: "Teacher", value: "Teacher"},
                    {id: 1, name: "Staff", value: "Staff"},
                    {id: 1, name: "Non-Govt-Employee", value: "Non-Govt-Employee"}
                ];
                //if($scope.moduleName=='job_portal'){
                //
                //}
                $scope.moduleName = $stateParams.module;
                $scope.institutes = Institute.query({size: 2000});
                $scope.organizations = OrganizationType.query({size: 200});
                $scope.employers = Employer.query({size: 500});
                $scope.organizationCategories = OrganizationCategory.query({size: 500});
                $scope.cats = Cat.query({size: 500});
                $scope.dlContCategories = DlContCatSet.query({size: 500});
                $scope.dlContSubCategories = DlContSCatSet.query({size: 500});
                $scope.contentSources = DlSourceSetUp.query({size: 500});
                $scope.contentTypes = DlContTypeSet.query({size: 500});
                //$scope.HrDepartments=HrDepartmentHeadSetup.query({size:500});
                $scope.HrDepartments = HrDepartmentSetup.query({size: 500});
                $scope.books = DlBookInfo.query({size: 500});
                $scope.fileTypes = DlFileType.query({size: 500});

                $scope.countries = GetCountry.query({size: 500});

                $scope.hrDesignations = HrDesignationHeadSetup.query({size: 500});
                $scope.hrMiscTypes = MiscTypeSetup.query({size: 500});



                $scope.jobTypes = JobType.query({size: 500});
                $scope.categories = Cat.query({size: 500});
                $scope.jobTitle = Job.query({size: 500});
                $scope.DepartmentByOrganization = HrDepartmentSetup.query({size: 500});
                $scope.hrTranFroms = [];
                $scope.hrTranTos = [];
                HrEmpTransferInfo.query({size: 500}, function (result) {
                    console.log("TOOTOTOTO")
                    console.log(result);
                    console.log("TOOTOTOTO")
                    angular.forEach(result, function (item) {
                        $scope.hrTranFroms.push(item.fromOrganization.name);
                    });
                    angular.forEach(result, function (item) {
                        $scope.hrTranTos.push(item.toOrganization.name);
                    });
                });
                MiscTypeSetupByCategory.get({cat: 'TrainingType', stat: true}, function (result) {
                    $scope.trainingTypeMisc = result;
                    console.log("Training amar Type");
                    console.log($scope.trainingTypeMisc);
                    console.log("Training amar Type");
                });

                $scope.countrys = [];
                Country.query({page: $scope.page, size: 2000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.countrys = result;
                    $scope.total = headers('x-total-count');
                    console.log('countries');
                    console.log($scope.countrys);
                });



                $scope.years = [
                    {id: 1, theYear: 2018},
                    {id: 2, theYear: 2017},
                    {id: 3, theYear: 2016},
                    {id: 4, theYear: 2015},
                    {id: 5, theYear: 2014},
                    {id: 6, theYear: 2013},
                    {id: 7, theYear: 2012},
                    {id: 8, theYear: 2011},
                    {id: 9, theYear: 2010},
                    {id: 10, theYear: 2009},
                    {id: 11, theYear: 2008},
                    {id: 12, theYear: 2007},
                    {id: 13, theYear: 2006},
                    {id: 14, theYear: 2005},
                    {id: 15, theYear: 2004},
                    {id: 16, theYear: 2003},
                    {id: 17, theYear: 2002},
                    {id: 18, theYear: 2001},
                    {id: 19, theYear: 2000}
                ];
                $scope.jobPlaceMenStatus = [
                    {id: 1, name: '0', value: 'No'},
                    {id: 2, name: '1', value: 'Yes'}
                ];

                $scope.salaryType = [
                    {id: 1, name: 'MONTHLY', value: 'MONTHLY'},
                    {id: 2, name: 'ONETIME', value: 'ONETIME'}
                ];

                $scope.months = [
                    {id: 1, theMonth: 'January', shortMonth: 'Jan'},
                    {id: 2, theMonth: 'February', shortMonth: 'Feb'},
                    {id: 3, theMonth: 'March', shortMonth: 'Mar'},
                    {id: 4, theMonth: 'April', shortMonth: 'Apr'},
                    {id: 5, theMonth: 'May', shortMonth: 'May'},
                    {id: 6, theMonth: 'June', shortMonth: 'Jun'},
                    {id: 7, theMonth: 'July', shortMonth: 'Jul'},
                    {id: 8, theMonth: 'August', shortMonth: 'Aug'},
                    {id: 9, theMonth: 'September', shortMonth: 'Sep'},
                    {id: 10, theMonth: 'October', shortMonth: 'Oct'},
                    {id: 11, theMonth: 'November', shortMonth: 'Nov'},
                    {id: 12, theMonth: 'December', shortMonth: 'Dec'}

                ];


                //division district upazila
                $scope.divisions = Division.query({size: 10});
                var allDistrict = District.query({page: $scope.page, size: 65}, function (result, headers) {
                    return result;
                });
                var allUpazila = Upazila.query({page: $scope.page, size: 500}, function (result, headers) {
                    return result;
                });
                $scope.updatedDistrict = function (select) {
                    $scope.districts = [];
                    angular.forEach(allDistrict, function (district) {
                        if (select == district.division.name) {
                            $scope.districts.push(district);
                        }
                    });
                };
                $scope.updatedUpazila = function (select) {
                    $scope.upazilas = [];
                    angular.forEach(allUpazila, function (upazila) {
                        if (select == upazila.district.name) {
                            $scope.upazilas.push(upazila);
                        }
                    });
                };
                //division district upazila

                //$scope.moduleName = "job_portal";
                $scope.jasperReports = [];
                var loadModule = function () {
                    GetAllJasperReportByModule.query({module: $scope.moduleName}, function (result) {

                            $scope.jasperReports = result;
                        },
                        function (response) {
                        }
                    );
                };

//         $scope.list_change=function(data){
//         };

                loadModule();
                $scope.reportChange = function () {
                    $scope.obj = $scope.jasperReport.id;
                    $scope.jasperReportParameters = [];
                    GetJasperParamByJasperReport.query({id: $scope.jasperReport.id}, function (result) {
                        $scope.jasperReportParameters = result;
                    });
                    ;
                };
                var paramDate;
                //start preview method
                $scope.reportPreview = function () {
                    console.log("$scope.jasperReportParameters");
                    console.log($scope.jasperReportParameters);
                    var parmavar = "";
                    var urlString = "";
                    // urlString = "http://202.4.121.77:9090/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports&reportUnit=%2FDTE%2F"+$stateParams.module+"%2F";
                    urlString = "http://202.4.121.78:9090/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2FDTE%2Fmpo&reportUnit=%2FDTE%2F" + $stateParams.module + "%2F";
                    urlString = urlString + $scope.jasperReport.path + "&standAlone=true&j_username=demo&j_password=12345&decorate=no";
                    var parmavar = "";
                    angular.forEach($scope.jasperReportParameters, function (data) {
                        console.log("data .name");
                        console.log(data.name);

                        parmavar = parmavar + "&" + data.name + "=";
                        console.log(parmavar);
                        console.log("data.actiontype : " + data.actiontype);
                        if (data.actiontype) {
                            if (data.type == 'combo') {
                                if (data.actiontype.toString().trim() != "") {
                                    console.log("combo");
                                    console.log(data.actiontype);
                                    console.log(parmavar);
                                    parmavar = parmavar + data.actiontype;
                                    console.log(parmavar);
                                }

                                parmavar = parmavar;
                            } else if (data.type == 'text') {
                                if (data.actiontype.toString().trim() != "")
                                    parmavar = parmavar + data.actiontype;
                                parmavar = parmavar;
                            } else if (data.type == 'date') {
                                if (data.actiontype.toString().trim() != "")
                                    paramDate = data.actiontype;
                                parmavar = parmavar + paramDate.getFullYear() + '-' + (paramDate.getMonth() + 1) + '-' + paramDate.getDate() + "%25";
                                //                           // parmavar = parmavar + data.actiontype;
                                parmavar = parmavar + "%25";
                            } else if (data.type == 'month' || data.type == 'year') {

                                if (data.actiontype.toString().trim() != "")
                                    parmavar = parmavar + data.actiontype;
                            } else if (data.type == 'auto-suggestion') {
                                console.log("$scope.flag*****");
                                console.log($scope.flag);
                                console.log("inside auto-suggestion");
                                if (data.actiontype.toString().trim() != "" && $scope.flag == 0) {
                                    console.log(parmavar);
                                    console.log(data.actiontype);
                                    data.actiontype = data.actiontype.code;
                                    console.log("data.actiontype.code");
                                    console.log(data.actiontype);
                                    parmavar = parmavar + data.actiontype;
                                    console.log(parmavar);
                                    if (data.validationexp != null && data.curriculum != null) {
                                        parmavar = parmavar + '&' + 'curriculum=' + data.validationexp.id + '&' + 'technology=' + data.curriculum.id;
                                    }
                                    console.log(parmavar);

                                }
                                /*if (data.validationexp.toString().trim() != "" && $scope.flag==1) {
                                 console.log("validationexp***");
                                 console.log(parmavar);
                                 console.log(data.validationexp);
                                 data.actiontype= data.validationexp.name;
                                 console.log("data.validationexp.name");
                                 console.log(data.actiontype);
                                 parmavar = parmavar + data.actiontype;
                                 console.log(parmavar);

                                 }
                                 if (data.curriculum.toString().trim() != "" && $scope.flag==2) {
                                 console.log("curriculum****");
                                 console.log(parmavar);
                                 console.log(data.curriculum);
                                 data.actiontype= data.curriculum.name;
                                 console.log("data.actiontype.name");
                                 console.log(data.actiontype);
                                 parmavar = parmavar + data.actiontype;
                                 console.log(parmavar);
                                 }*/

                            }
                        }
                        else {
                            if (data.type == 'date') {
                                if (parmavar == 'fromDate') {
                                    parmavar = parmavar + "1900-11-28%25";
                                } else if (parmavar == 'toDate') {
                                    parmavar = parmavar + "2100-11-28%25";
                                }
                            } else {
                                parmavar = parmavar + "";
                            }
                        }//end else
                    });

                    $scope.rptBaseurl = urlString + parmavar;
                    console.log('.......URI JP');
                    console.log($scope.rptBaseurl);
                    $scope.url = $sce.trustAsResourceUrl($scope.rptBaseurl);
                };
                //end preview method
            }]);
