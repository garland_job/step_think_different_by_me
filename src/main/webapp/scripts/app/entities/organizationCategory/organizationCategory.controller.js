'use strict';

angular.module('stepApp')
    .controller('OrganizationCategoryController',
    ['$scope', '$state', '$modal', 'OrganizationCategory', 'OrganizationCategorySearch', 'ParseLinks',
    function ($scope, $state, $modal, OrganizationCategory, OrganizationCategorySearch, ParseLinks) {

        $scope.organizationCategorys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            OrganizationCategory.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.organizationCategorys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            OrganizationCategorySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.organizationCategorys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.organizationCategory = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);
/*organizationCategory-dialog.controller.js*/

angular.module('stepApp').controller('OrganizationCategoryDialogController',
    //['$scope', '$stateParams', 'entity','$state', 'OrganizationCategory',

    ['$scope','$rootScope','$stateParams','$state','entity','OrganizationCategory',
        function($scope, $rootScope, $stateParams, $state, entity, OrganizationCategory) {
            console.log($stateParams.id);
            $scope.organizationCategory = {};
            OrganizationCategory.get({id : $stateParams.id}, function(result) {
                console.log(result);
                $scope.organizationCategory = result;
            });
            /*$scope.load = function(id) {

             };*/

            var onSaveSuccess = function (result) {
                //$scope.$emit('stepApp:organizationCategoryUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('organizationCategory', null, { reload: true });
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                //console.log($scope.organizationCategory);
                if ($scope.organizationCategory.id != null) {
                    OrganizationCategory.update($scope.organizationCategory, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('Organization Category has been successfully updated');
                } else {
                    OrganizationCategory.save($scope.organizationCategory, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('Organization Category has been successfully saved');
                }
            };

            $scope.clear = function() {
                // $modalInstance.dismiss('cancel');
            };
        }]
//]
);
/*organizationCategory-detail.controller.js*/

angular.module('stepApp')
    .controller('OrganizationCategoryDetailController',
        ['$scope','$rootScope','$stateParams','entity','OrganizationCategory',
            function ($scope, $rootScope, $stateParams, entity, OrganizationCategory) {
                $scope.organizationCategory = entity;
                $scope.load = function (id) {
                    OrganizationCategory.get({id: id}, function(result) {
                        $scope.organizationCategory = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:organizationCategoryUpdate', function(event, result) {
                    $scope.organizationCategory = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*organizationCategory-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('OrganizationCategoryDeleteController',

        ['$scope','$rootScope','$modalInstance','entity','OrganizationCategory',
            function($scope, $rootScope, $modalInstance, entity, OrganizationCategory) {

                $scope.organizationCategory = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    OrganizationCategory.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('Deleted');
                        });
                };

            }]);
