'use strict';

angular.module('stepApp').controller('SecurityConfigurationTableDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'SecurityConfigurationTable',
        function($scope, $stateParams, $state, entity, SecurityConfigurationTable) {

        $scope.securityConfigurationTable = entity;
        $scope.load = function(id) {
            SecurityConfigurationTable.get({id : id}, function(result) {
                $scope.securityConfigurationTable = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:securityConfigurationTableUpdate', result);
            $state.go('securityConfigurationTable', null, { reload: true });
        };

        $scope.save = function () {
            if ($scope.securityConfigurationTable.id != null) {
                SecurityConfigurationTable.update($scope.securityConfigurationTable, onSaveFinished);
            } else {
                SecurityConfigurationTable.save($scope.securityConfigurationTable, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $state.go('securityConfigurationTable', null, { reload: true });
        };
}]);
