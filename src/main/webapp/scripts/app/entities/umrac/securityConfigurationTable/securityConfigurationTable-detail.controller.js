'use strict';

angular.module('stepApp')
    .controller('SecurityConfigurationTableDetailController', function ($scope, $rootScope, $stateParams, entity, SecurityConfigurationTable) {
        $scope.securityConfigurationTable = entity;
        $scope.load = function (id) {
            SecurityConfigurationTable.get({id: id}, function(result) {
                $scope.securityConfigurationTable = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:securityConfigurationTableUpdate', function(event, result) {
            $scope.securityConfigurationTable = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
