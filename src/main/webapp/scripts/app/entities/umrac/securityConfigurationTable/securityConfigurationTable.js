'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('securityConfigurationTable', {
                parent: 'umrac',
                url: '/securityConfigurationTables',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.securityConfigurationTable.home.title'
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/securityConfigurationTable/securityConfigurationTables.html',
                        controller: 'SecurityConfigurationTableController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('securityConfigurationTable');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('securityConfigurationTable.detail', {
                parent: 'securityConfigurationTable',
                url: '/securityConfigurationTable/{id}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.securityConfigurationTable.detail.title'
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/securityConfigurationTable/securityConfigurationTable-detail.html',
                        controller: 'SecurityConfigurationTableDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('securityConfigurationTable');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SecurityConfigurationTable', function($stateParams, SecurityConfigurationTable) {
                        return SecurityConfigurationTable.get({id : $stateParams.id});
                    }]
                }
            })
            .state('securityConfigurationTable.new', {
                parent: 'securityConfigurationTable',
                url: '/new',
                data: {
                    authorities: [],
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/securityConfigurationTable/securityConfigurationTable-dialog.html',
                        controller: 'SecurityConfigurationTableDialogController',
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            firstLoginPwdChange: null,
                            maxInvPwdAttempt: null,
                            maxPwdRepatation: null,
                            noOfDatePwdExp: null,
                            reqPwdLen: null,
                            reqPwdNalphaChar: null,
                            pwdNalphaChar: null,
                            userNameLength: null,
                            userNameNalphaAllow: null,
                            userNameNalphaCharAllow: null,
                            userNameNalphaChar: null,
                            id: null
                        };
                    }
                }

            })
            .state('securityConfigurationTable.edit', {
                parent: 'securityConfigurationTable',
                url: '/{id}/edit',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.securityConfigurationTable.detail.title'
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/securityConfigurationTable/securityConfigurationTable-dialog.html',
                        controller: 'SecurityConfigurationTableDialogController',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('securityConfigurationTable');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SecurityConfigurationTable', function($stateParams, SecurityConfigurationTable) {
                        return SecurityConfigurationTable.get({id : $stateParams.id});
                    }]
                }
            });
    });
