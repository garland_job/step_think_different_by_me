'use strict';

angular.module('stepApp')
    .controller('SecurityConfigurationTableController', function ($scope, SecurityConfigurationTable, SecurityConfigurationTableSearch, ParseLinks) {
        $scope.securityConfigurationTables = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            SecurityConfigurationTable.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.securityConfigurationTables = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            SecurityConfigurationTable.get({id: id}, function(result) {
                $scope.securityConfigurationTable = result;
                $('#deleteSecurityConfigurationTableConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            SecurityConfigurationTable.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSecurityConfigurationTableConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SecurityConfigurationTableSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.securityConfigurationTables = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.securityConfigurationTable = {
                firstLoginPwdChange: null,
                maxInvPwdAttempt: null,
                maxPwdRepatation: null,
                noOfDatePwdExp: null,
                reqPwdLen: null,
                reqPwdNalphaChar: null,
                pwdNalphaChar: null,
                userNameLength: null,
                userNameNalphaAllow: null,
                userNameNalphaCharAllow: null,
                userNameNalphaChar: null,
                id: null
            };
        };
    });
