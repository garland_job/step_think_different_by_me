'use strict';

angular.module('stepApp')
    .controller('UmracModuleSetupController',
    ['$scope', 'UmracModuleSetup', 'UmracModuleSetupSearch', 'ParseLinks',
    function ($scope, UmracModuleSetup, UmracModuleSetupSearch, ParseLinks) {
        $scope.umracModuleSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UmracModuleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracModuleSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            UmracModuleSetup.get({id: id}, function(result) {
                $scope.umracModuleSetup = result;
                $('#deleteUmracModuleSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            UmracModuleSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteUmracModuleSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            UmracModuleSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.umracModuleSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.umracModuleSetup = {
                moduleId: null,
                moduleName: null,
                moduleUrl: null,
                description: null,
                status: null,
                createDate: null,
                createBy: null,
                updatedBy: null,
                updatedTime: null,
                id: null
            };
        };
    }]);


/* DIALOG */

angular.module('stepApp').controller('UmracModuleSetupDialogController',
    ['$scope', '$stateParams', '$state', '$rootScope','entity', 'UmracModuleSetup',
        function ($scope, $stateParams, $state, $rootScope, entity, UmracModuleSetup) {

            $scope.umracModuleSetup = entity;

            $scope.load = function (id) {
                UmracModuleSetup.get({id: id}, function (result) {
                    $scope.umracModuleSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracModuleSetupUpdate', result);
                //$modalInstance.close(result);
                $state.go('umracModuleSetup');
            };
            var onSaveError = function (result) {
                $scope.moduleName = false;
                console.log('---------------Error------------------------')
            };
            $scope.save = function () {

                $scope.moduleName = $rootScope.layerDataCheck('umracModuleSetup.moduleName', $scope.umracModuleSetup.moduleName, 'text', 50, 1, 'atozAndAtoZ', true, 0, 9);
                //alert($scope.moduleName+"----wrong data in Module Name ");


                if ($scope.umracModuleSetup.id != null && $scope.moduleName == true) {
                    UmracModuleSetup.update($scope.umracModuleSetup, onSaveFinished,onSaveError);
                } else if ($scope.umracModuleSetup.id == null && $scope.moduleName == true) {
                    UmracModuleSetup.save($scope.umracModuleSetup, onSaveFinished,onSaveError);
                }

            };

            $scope.clear = function () {
                //$modalInstance.dismiss('cancel');
                $state.go('umracModuleSetup');
            };
        }]);

/* DETAIL */

angular.module('stepApp')
    .controller('UmracModuleSetupDetailController',
    ['$scope', '$rootScope', '$stateParams', 'entity','UmracModuleSetup',
    function ($scope, $rootScope, $stateParams, entity, UmracModuleSetup) {
        $scope.umracModuleSetup = entity;
        $scope.load = function (id) {
            UmracModuleSetup.get({id: id}, function(result) {
                $scope.umracModuleSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:umracModuleSetupUpdate', function(event, result) {
            $scope.umracModuleSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);

