'use strict';

angular.module('stepApp').controller('UmracModuleSetupDialogController',
    ['$scope', '$stateParams', '$state', '$rootScope','entity', 'UmracModuleSetup',
        function ($scope, $stateParams, $state, $rootScope, entity, UmracModuleSetup) {

            $scope.umracModuleSetup = entity;

            $scope.load = function (id) {
                UmracModuleSetup.get({id: id}, function (result) {
                    $scope.umracModuleSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracModuleSetupUpdate', result);
                //$modalInstance.close(result);
                $state.go('umracModuleSetup');
            };
            var onSaveError = function (result) {
                $scope.moduleName = false;
                console.log('---------------Error------------------------')
            };
            $scope.save = function () {

                $scope.moduleName = $rootScope.layerDataCheck('umracModuleSetup.moduleName', $scope.umracModuleSetup.moduleName, 'text', 50, 1, 'atozAndAtoZ', true, 0, 9);
                //alert($scope.moduleName+"----wrong data in Module Name ");


                if ($scope.umracModuleSetup.id != null && $scope.moduleName == true) {
                    UmracModuleSetup.update($scope.umracModuleSetup, onSaveFinished,onSaveError);
                } else if ($scope.umracModuleSetup.id == null && $scope.moduleName == true) {
                    UmracModuleSetup.save($scope.umracModuleSetup, onSaveFinished,onSaveError);
                }

            };

            $scope.clear = function () {
                //$modalInstance.dismiss('cancel');
                $state.go('umracModuleSetup');
            };
        }]);
