'use strict';

angular.module('stepApp').controller('UmracActionSetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracActionSetup','UmracSubmoduleSetup', 'ParseLinks','GetAuthorities', 'UmracActionSetupActionIdBySubmoduleId',
        function($scope, $stateParams, $state, entity, UmracActionSetup, UmracSubmoduleSetup, ParseLinks, GetAuthorities, UmracActionSetupActionIdBySubmoduleId) {

        $scope.umracActionSetup = entity;
        $scope.umracActionSetupActionId = [];
        $scope.umracSubmoduleSetups = [];
        UmracSubmoduleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
            $scope.links = ParseLinks.parse(headers('link'));
            $scope.umracSubmoduleSetups = result;
        });
        GetAuthorities.query({size: 2000}, function (data) {
            $scope.authorities = [];
            $scope.authorities = data;
            /*$scope.singleJhiRoles = data;
            for (var i = 0; i < $scope.singleJhiRoles.length; i++) {
                for (var j = 0; j < $scope.authorities.length; j++) {
                    if ($scope.authorities[j].NAME == $scope.singleJhiRoles[i].authorityName) {
                        $scope.authorities.splice(j, 1);
                    }
                }
            }*/
            console.log($scope.authorities);
        }, function (err) {
            console.log(err)
        });
        $scope.load = function(id) {
            UmracActionSetup.get({id : id}, function(result) {
                $scope.umracActionSetup = result;
            });
        };

        $scope.getActionId = function(submoduleId) {
            $scope.umracActionSetupActionId = [];
            console.log('hi '+submoduleId);
            UmracActionSetupActionIdBySubmoduleId.query({id : submoduleId.id}, function(result) {
                $scope.umracActionSetupActionId = result;
                $scope.umracActionSetup.actionId = $scope.umracActionSetupActionId.actionId +1;
                console.log('hi '+$scope.umracActionSetup.actionId);
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:umracActionSetupUpdate', result);
            //$modalInstance.close(result);
            $state.go('umracActionSetup');
        };

        $scope.save = function () {
            if ($scope.umracActionSetup.id != null) {
                UmracActionSetup.update($scope.umracActionSetup, onSaveFinished);
            } else {
                UmracActionSetup.save($scope.umracActionSetup, onSaveFinished);
            }
        };

        $scope.clear = function() {
            //$modalInstance.dismiss('cancel');
            $state.go('umracActionSetup');
        };

        $scope.filterSubModuleByStatus = function () {
            return function (item) {
                if (item.status == true)
                {
                    return true;
                }
                return false;
            };
        };


}]);
