'use strict';

angular.module('stepApp')
    .controller('UmracActionSetupController',
    ['$scope', 'UmracActionSetup', 'UmracActionSetupSearch', 'ParseLinks',
    function ($scope, UmracActionSetup, UmracActionSetupSearch, ParseLinks) {
        $scope.umracActionSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UmracActionSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracActionSetups = result;
                console.log($scope.umracActionSetups);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            UmracActionSetup.get({id: id}, function(result) {
                $scope.umracActionSetup = result;
                $('#deleteUmracActionSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            UmracActionSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteUmracActionSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            UmracActionSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.umracActionSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.umracActionSetup = {
                actionId: null,
                actionName: null,
                actionUrl: null,
                description: null,
                status: null,
                createDate: null,
                createBy: null,
                updatedBy: null,
                updatedTime: null,
                id: null
            };
        };
    }]);

/* DIALOG */
angular.module('stepApp').controller('UmracActionSetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracActionSetup','UmracSubmoduleSetup', 'ParseLinks','GetAuthorities', 'UmracActionSetupActionIdBySubmoduleId',
        function($scope, $stateParams, $state, entity, UmracActionSetup, UmracSubmoduleSetup, ParseLinks, GetAuthorities, UmracActionSetupActionIdBySubmoduleId) {

        $scope.umracActionSetup = entity;
        $scope.umracSubmoduleSetups = [];
        UmracSubmoduleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
            $scope.links = ParseLinks.parse(headers('link'));
            $scope.umracSubmoduleSetups = result;
        });
        GetAuthorities.query({size: 2000}, function (data) {
            $scope.authorities = [];
            $scope.authorities = data;
            /*$scope.singleJhiRoles = data;
            for (var i = 0; i < $scope.singleJhiRoles.length; i++) {
                for (var j = 0; j < $scope.authorities.length; j++) {
                    if ($scope.authorities[j].NAME == $scope.singleJhiRoles[i].authorityName) {
                        $scope.authorities.splice(j, 1);
                    }
                }
            }*/
            console.log($scope.authorities);
        }, function (err) {
            console.log(err)
        });
        $scope.load = function(id) {
            UmracActionSetup.get({id : id}, function(result) {
                $scope.umracActionSetup = result;
            });
        };

        /*$scope.getActionId = function(submoduleId) {
            $scope.umracActionSetupActionId = [];
            console.log('hi '+submoduleId);
            UmracActionSetupActionIdBySubmoduleId.query({id : submoduleId.id}, function(result) {
                $scope.umracActionSetupActionId = result;
                //console.log('hi '+$scope.umracActionSetupActionId);
                //console.log($scope.umracActionSetupActionId[0].actionId);
                //var activityId = $scope.umracActionSetupActionId[0].actionId;
                $scope.umracActionSetup.actionId = parseInt($scope.umracActionSetupActionId[0].actionId)+1;
                //console.log('hi '+$scope.umracActionSetup.actionId);
            });

            UmracActionSetupActionIdBySubmoduleId.query({id : submoduleId.id}).then(function (result) {
                  $scope.umracActionSetupActionId = result;
                  $scope.umracActionSetup.actionId = parseInt($scope.umracActionSetupActionId[0].actionId)+1;
            }).catch(function activateError(error) {
               if (!error.handled) {
                  $scope.umracActionSetup.actionId ='';
               }
            });
        };*/
        $scope.getActionId = function(submoduleId) {
            $scope.umracActionSetupActionId = [];
            UmracActionSetupActionIdBySubmoduleId.query({
              id : submoduleId.id
            }, function(data) {
              $scope.umracActionSetupActionId = data;
              $scope.umracActionSetup.actionId = parseInt($scope.umracActionSetupActionId[0].actionId)+1;
            }, function(err) {
              $scope.umracActionSetup.actionId ='';
            });
          };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:umracActionSetupUpdate', result);
            //$modalInstance.close(result);
            $state.go('umracActionSetup');
        };

        $scope.save = function () {
            if ($scope.umracActionSetup.id != null) {
                UmracActionSetup.update($scope.umracActionSetup, onSaveFinished);
            } else {
                UmracActionSetup.save($scope.umracActionSetup, onSaveFinished);
            }
        };

        $scope.clear = function() {
            //$modalInstance.dismiss('cancel');
            $state.go('umracActionSetup');
        };

        $scope.filterSubModuleByStatus = function () {
            return function (item) {
                if (item.status == true)
                {
                    return true;
                }
                return false;
            };
        };


}]);

/* DETAIL */

angular.module('stepApp')
    .controller('UmracActionSetupDetailController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'UmracActionSetup',
    function ($scope, $rootScope, $stateParams, entity, UmracActionSetup) {
        $scope.umracActionSetup = entity;
        $scope.load = function (id) {
            UmracActionSetup.get({id: id}, function(result) {
                $scope.umracActionSetup = result;

            });
        };
        var unsubscribe = $rootScope.$on('stepApp:umracActionSetupUpdate', function(event, result) {
            $scope.umracActionSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
