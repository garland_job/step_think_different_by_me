'use strict';

angular.module('stepApp')
    .controller('UmracSubmoduleSetupController',
    ['$scope', 'UmracSubmoduleSetup', 'UmracSubmoduleSetupSearch', 'ParseLinks',
    function ($scope, UmracSubmoduleSetup, UmracSubmoduleSetupSearch, ParseLinks) {
        $scope.umracSubmoduleSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UmracSubmoduleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracSubmoduleSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            UmracSubmoduleSetup.get({id: id}, function(result) {
                $scope.umracSubmoduleSetup = result;
                $('#deleteUmracSubmoduleSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            UmracSubmoduleSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteUmracSubmoduleSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            UmracSubmoduleSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.umracSubmoduleSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.umracSubmoduleSetup = {
                subModuleId: null,
                subModuleName: null,
                subModuleUrl: null,
                description: null,
                status: null,
                createDate: null,
                createBy: null,
                updatedBy: null,
                updatedTime: null,
                id: null
            };
        };
    }]);

/* DIALOG */

angular.module('stepApp').controller('UmracSubmoduleSetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracSubmoduleSetup', 'UmracModuleSetup', 'ParseLinks',
        function ($scope, $stateParams, $state, entity, UmracSubmoduleSetup, UmracModuleSetup, ParseLinks) {

            $scope.umracSubmoduleSetup = entity;
            $scope.umracmodulesetups = [];

            UmracModuleSetup.query({page: $scope.page, size: 2000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracmodulesetups = result;
            });

            $scope.load = function (id) {
                UmracSubmoduleSetup.get({id: id}, function (result) {
                    $scope.umracSubmoduleSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracSubmoduleSetupUpdate', result);
                $state.go('umracSubmoduleSetup');
            };

            $scope.save = function () {
                if ($scope.umracSubmoduleSetup.id != null) {
                    UmracSubmoduleSetup.update($scope.umracSubmoduleSetup, onSaveFinished);
                } else {
                    UmracSubmoduleSetup.save($scope.umracSubmoduleSetup, onSaveFinished);
                }
            };

            $scope.clear = function () {
                $state.go('umracSubmoduleSetup');
            };

            $scope.filterModuleByStatus = function () {
                return function (item) {
                    if (item.status == true) {
                        return true;
                    }
                    return false;
                };
            };
        }]);

/* DETAIL */

angular.module('stepApp')
    .controller('UmracSubmoduleSetupDetailController',
     ['$scope', '$rootScope', '$stateParams', 'entity', 'UmracSubmoduleSetup', 'UmracModuleSetup',
     function ($scope, $rootScope, $stateParams, entity, UmracSubmoduleSetup, UmracModuleSetup) {
        $scope.umracSubmoduleSetup = entity;
        $scope.load = function (id) {
            UmracSubmoduleSetup.get({id: id}, function(result) {
                $scope.umracSubmoduleSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:umracSubmoduleSetupUpdate', function(event, result) {
            $scope.umracSubmoduleSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
