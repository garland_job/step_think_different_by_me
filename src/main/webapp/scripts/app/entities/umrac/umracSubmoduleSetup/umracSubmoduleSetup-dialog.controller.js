'use strict';

angular.module('stepApp').controller('UmracSubmoduleSetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracSubmoduleSetup', 'UmracModuleSetup', 'ParseLinks',
        function ($scope, $stateParams, $state, entity, UmracSubmoduleSetup, UmracModuleSetup, ParseLinks) {

            $scope.umracSubmoduleSetup = entity;
            $scope.umracmodulesetups = [];

            UmracModuleSetup.query({page: $scope.page, size: 2000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracmodulesetups = result;
            });

            $scope.load = function (id) {
                UmracSubmoduleSetup.get({id: id}, function (result) {
                    $scope.umracSubmoduleSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracSubmoduleSetupUpdate', result);
                $state.go('umracSubmoduleSetup');
            };

            $scope.save = function () {
                if ($scope.umracSubmoduleSetup.id != null) {
                    UmracSubmoduleSetup.update($scope.umracSubmoduleSetup, onSaveFinished);
                } else {
                    UmracSubmoduleSetup.save($scope.umracSubmoduleSetup, onSaveFinished);
                }
            };

            $scope.clear = function () {
                $state.go('umracSubmoduleSetup');
            };

            $scope.filterModuleByStatus = function () {
                return function (item) {
                    if (item.status == true) {
                        return true;
                    }
                    return false;
                };
            };
        }]);
