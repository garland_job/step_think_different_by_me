'use strict';

 angular.module('stepApp')
     .controller('UmracPasswordChangeController',
     ['$scope', 'User', 'UsersSearch', 'ParseLinks', 'Language', 'UsersBySearchItems',
     function ($scope, User, UsersSearch, ParseLinks, Language, UsersBySearchItems) {
         $scope.users = [];
         $scope.searchCategory = "";
         $scope.searchKey = "";
         //$scope.authorities = ["ROLE_USER", "ROLE_ADMIN", "ROLE_MANAGER", "ROLE_EMPLOYER"];
         $scope.authorities = [];

         Language.getAll().then(function (languages) {
             $scope.languages = languages;
         });

         $scope.page = 0;
         $scope.loadAll = function () {
             //User.query({page: $scope.page, per_page: 10}, function (result, headers) {
             User.query({page: $scope.page, per_page: 10, size: 20000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.users = result;
 				$scope.total = headers('x-total-count');
 				console.log($scope.users);
 				$scope.searchCategory = "";
                $scope.searchKey = "";
             });
         };

         $scope.loadUserBySearch = function (searchCategory, searchItem) {
             UsersBySearchItems.query({searchCategory : searchCategory, searchItem : searchItem}, function(result, headers) {
                 console.log(result);
                 $scope.links = ParseLinks.parse(headers('link'));
                 $scope.users = result;
                 $scope.total = headers('x-total-count');
                 console.log($scope.users);
             });
         };

         $scope.loadPage = function (page) {
             $scope.page = page;
             $scope.loadAll();
         };
         $scope.loadAll();

         $scope.setActive = function (user, isActivated) {
             user.activated = isActivated;
             User.update(user, function () {
                 $scope.loadAll();
                 $scope.clear();
             });
         };

         $scope.clear = function () {
             $scope.user = {
                 id: null, login: null, firstName: null, lastName: null, email: null,
                 activated: null, langKey: null, createdBy: null, createdDate: null,
                 lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                 resetKey: null, authorities: null
             };
         };

 		$scope.search = function () {
             UsersSearch.query({
                 query: $scope.searchQuery
             }, (function (result) {
                 $scope.users = result;
             }), function (response) {
                 if (response.status === 404) {
                     $scope.loadAll();
                 }
             });
         };

 		$scope.areAllUsersSelected = false;

         $scope.updateUsersSelection = function (userArray, selectionValue) {
             for (var i = 0; i < userArray.length; i++)
             {
                 userArray[i].isSelected = selectionValue;
             }
         };

         $scope.import = function (){
             for (var i = 0; i < $scope.users.length; i++){
                 var user = $scope.users[i];
                 if(user.isSelected){
                     //User.update(user);
                     //TODO: handle bulk import
                 }
             }
         };

         $scope.export = function (){
             for (var i = 0; i < $scope.users.length; i++){
                 var user = $scope.users[i];
                 if(user.isSelected){
                     //User.update(user);
                     //TODO: handle bulk export
                 }
             }
         };

         $scope.deleteSelected = function (){
             for (var i = 0; i < $scope.users.length; i++){
                 var user = $scope.users[i];
                 if(user.isSelected){
                     //User.delete(user);
                     //user deletion deliberately disabled
                 }
             }
         };

         $scope.sync = function (){
             for (var i = 0; i < $scope.users.length; i++){
                 var user = $scope.users[i];
                 if(user.isSelected){
                     User.update(user);
                 }
             }
         };

         $scope.order = function (predicate, reverse) {
             $scope.predicate = predicate;
             $scope.reverse = reverse;
             User.query({page: $scope.page, per_page: 10000}, function (result, headers) {
                 $scope.links = ParseLinks.parse(headers('link'));
                 $scope.users = result;
                 $scope.total = headers('x-total-count');
             });

         };
     }]);
