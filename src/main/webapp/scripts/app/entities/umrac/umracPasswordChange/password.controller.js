'use strict';

angular.module('stepApp')
    .controller('UserPasswordController',
    ['$scope', 'Auth', 'Principal','User', '$stateParams', 'UpdateUsersByLoginAndPassword',
    function ($scope, Auth, Principal, User, $stateParams, UpdateUsersByLoginAndPassword) {

        $scope.user = [];
        //alert($stateParams.login);
        Principal.identity().then(function(account) {
                    $scope.account = account;
                });
        User.get({login : $stateParams.login}, function (result) {
              $scope.user = result;
              //$scope.authorities = $scope.user.authorities;
              console.log($scope.user);
            //$scope.account = $stateParams.login;

          });


        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.changePassword = function () {
            if ($scope.password !== $scope.confirmPassword) {
                $scope.doNotMatch = 'ERROR';
            } else {
                $scope.doNotMatch = null;
                //Auth.changePassword($scope.password).then(function () {
                UpdateUsersByLoginAndPassword.query({userLogin : $scope.user.login, userPassword : $scope.password}, function(result, headers) {
                    $scope.error = null;
                    $scope.success = 'OK';
                    $scope.user = [];
                    $scope.password = null;
                    $scope.confirmPassword = null;
                });
            }
        };
    }]);
