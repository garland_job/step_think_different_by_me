'use strict';

angular.module('stepApp').controller('UmracIdentitySetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracIdentitySetup',
        function($scope, $stateParams, $state, entity, UmracIdentitySetup) {

        $scope.umracIdentitySetup = entity;
        $scope.load = function(id) {
            UmracIdentitySetup.get({id : id}, function(result) {
                $scope.umracIdentitySetup = result;

            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:umracIdentitySetupUpdate', result);
            $state.go('umracIdentitySetup');
        };

        $scope.save = function () {
            if ($scope.umracIdentitySetup.id != null) {
                UmracIdentitySetup.update($scope.umracIdentitySetup, onSaveFinished);
            } else {
                UmracIdentitySetup.save($scope.umracIdentitySetup, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $state.go('umracIdentitySetup');
        };
        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.blockForLength = true;
        $scope.blockForUserName = true;
        $scope.matchPass = function (pass, conPass) {
            if (pass != conPass) {
                $scope.notMatched = true;
                $scope.notMatcheds = "Password doesn't matched !";
            } else {
                $scope.notMatched = false;
                $scope.notMatcheds = "";
            }
        };
        $scope.checkDigit = function (result2) {
            $scope.getLength = result2.length;
            if ($scope.getLength < 8) {
                $scope.blockForLength = true;
                $scope.blockForLengthss = "Your password is required to be at least 8 characters";
            } else {
                $scope.blockForLength = false;
                $scope.blockForLengthss = "";
            }
        };
        $scope.checkUsername = function (result2) {
            $scope.getLength = result2.length;
            if ($scope.getLength < 8) {
                $scope.blockForUserName = true;
                $scope.blockForUserNamess = "Your user name is required to be at least 8 characters";
            } else {
                $scope.blockForUserName = false;
                $scope.blockForUserNamess = "";
            }
        }
}]);
