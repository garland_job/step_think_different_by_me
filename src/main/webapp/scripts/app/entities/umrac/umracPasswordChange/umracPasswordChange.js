'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('umracPasswordChange', {
                parent: 'umrac',
                url: '/umracPasswordChange',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'user-management.home.title'
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/umracPasswordChange/umracPasswordChanges.html',
                        controller: 'UmracPasswordChangeController'
                    }
                },
                /*views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/umracPasswordChange/umracPasswordChanges.html',
                        controller: 'UmracPasswordChangeController'
                    }
                },*/
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('user.management');
                        return $translate.refresh();
                    }]
                }
            })
            .state('umracPasswordChange.password', {
                parent: 'umrac',
                url: '/umracPasswordChange/password/{login}',
                data: {
                    authorities: [],
                    pageTitle: 'global.menu.account.password'
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/umracPasswordChange/password.html',
                        controller: 'UserPasswordController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('password');
                        return $translate.refresh();
                    }],entity: ['$stateParams', 'User', function ($stateParams, User) {
                        return User.get({login: $stateParams.login});
                    }]
                }
            })
            /*.state('umracIdentitySetup.new', {
                parent: 'umracIdentitySetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/umracIdentitySetup/umracIdentitySetup-dialog.html',
                        controller: 'UmracIdentitySetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('umracIdentitySetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            empId: null,
                            userName: null,
                            uPw: null,
                            confm_Pw: null,
                            status: true,
                            createDate: null,
                            createBy: null,
                            updatedBy: null,
                            updatedTime: null,
                            id: null
                        };
                    }
                }
            })
            .state('umracIdentitySetup.edit', {
                parent: 'umracIdentitySetup',
                url: '/edit/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'stepApp.umracIdentitySetup.detail.title'
                },
                views: {
                    'umracManagementView@umrac': {
                        templateUrl: 'scripts/app/entities/umrac/umracIdentitySetup/umracIdentitySetup-dialog.html',
                        controller: 'UmracIdentitySetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('umracIdentitySetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'UmracIdentitySetup', function ($stateParams, UmracIdentitySetup) {
                        return UmracIdentitySetup.get({id: $stateParams.id});
                    }]
                }
            })*/
            /*.state('umracIdentitySetup.new', {
                parent: 'umracIdentitySetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/umracIdentitySetup/umracIdentitySetup-dialog.html',
                        controller: 'UmracIdentitySetupDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    empId: null,
                                    userName: null,
                                    uPw: null,
                                    confm_Pw: null,
                                    status: true,
                                    createDate: null,
                                    createBy: null,
                                    updatedBy: null,
                                    updatedTime: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('umracIdentitySetup', null, { reload: true });
                    }, function() {
                        $state.go('umracIdentitySetup');
                    })
                }]
            })
            .state('umracIdentitySetup.edit', {
                parent: 'umracIdentitySetup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/umracIdentitySetup/umracIdentitySetup-dialog.html',
                        controller: 'UmracIdentitySetupDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['UmracIdentitySetup', function(UmracIdentitySetup) {
                                return UmracIdentitySetup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('umracIdentitySetup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })*/
            ;
    });
