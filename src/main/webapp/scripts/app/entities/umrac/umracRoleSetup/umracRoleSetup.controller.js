'use strict';

angular.module('stepApp')
    .controller('UmracRoleSetupController',
        ['$scope', 'UmracRoleSetup', 'UmracRoleSetupSearch', 'ParseLinks',
        function ($scope, UmracRoleSetup, UmracRoleSetupSearch, ParseLinks) {
        $scope.umracRoleSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UmracRoleSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracRoleSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            UmracRoleSetup.get({id: id}, function(result) {
                $scope.umracRoleSetup = result;
                $('#deleteUmracRoleSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            UmracRoleSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteUmracRoleSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            UmracRoleSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.umracRoleSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.umracRoleSetup = {
                roleId: null,
                roleName: null,
                description: null,
                status: null,
                createDate: null,
                createBy: null,
                updatedBy: null,
                updatedTime: null,
                id: null
            };
        };
    }]);
/* DIALOG */
angular.module('stepApp').controller('UmracRoleSetupDialogController',
    ['$scope', '$state', '$stateParams', 'entity', 'UmracRoleSetup', 'TreeService', 'UserRoleRightTree','UserRoleRightTreeById',
        function ($scope, $state, $stateParams, entity, UmracRoleSetup, TreeService, UserRoleRightTree, UserRoleRightTreeById) {

            $scope.umracRoleSetup = entity;
            $scope.load = function (id) {
                buildTreeById(id);
                //UmracRoleSetup.get({id: id}, function (result) {
                //    $scope.umracRoleSetup = result;
                //    console.log("Update: "+$scope.umracRoleSetup);
                //    //buildTree();
                //
                //});
            };

            $scope.tc = {};

            if($stateParams.id == null){
                buildTree();
            }else if($stateParams.id != null){
                $scope.load($stateParams.id);
            }
            //buildTree();
            function buildTree() {
                TreeService.getTree().then(function (result) {
                    //$scope.tc.tree = result.data;
                    //console.log(result)
                    UserRoleRightTree.query(function(result) {
                     $scope.tc.tree = result;
                     console.log(result);
                     });
                }, function (result) {
                    alert("Tree no available, Error: " + result);
                });
            }

            function buildTreeById(id) {
                TreeService.getTree().then(function (result) {
                    //$scope.tc.tree = result.data;
                    //console.log(result)
                    UserRoleRightTreeById.query({id: id}, function(result) {
                        $scope.tc.tree = result;
                    });
                }, function (result) {
                    alert("Tree no available, Error: " + result);
                });
            }

            /*$scope.toggleSelection = function toggleSelection(code) {
             var idx = $scope.selection.indexOf(code);

             // is currently selected
             if (idx > -1) {
             $scope.selection.splice(idx, 1);
             }

             // is newly selected
             else {
             $scope.selection.push(employeeName);
             }
             };*/


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:umracRoleSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('umracRoleSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                /*console.log($scope.tc.tree);
                console.log($scope.tc.tree[0].children);
                console.log($scope.tc.tree[0].children[0]);
                console.log($scope.tc.tree[0].children[0].children);
                console.log($scope.tc.tree.length);
                console.log($scope.tc.tree[0].children.length);
                console.log($scope.tc.tree[0].children.length);*/
                //console.log($scope.tc.tree[0].children[0]);
                //console.log($scope.tc.tree[0].children[0].children);
                var rights = '';
                $scope.umracRoleSetup.roleContext = '';
                for (var i = 0; i < $scope.tc.tree.length; i++) {
                    for (var j = 0; j < $scope.tc.tree[i].children.length; j++) {
                        //console.log(i);
                        //console.log(j);
                        //console.log($scope.tc.tree[i].children[j].children);
                        for (var k = 0; k < $scope.tc.tree[i].children[j].children.length; k++) {
                            //console.log(k);

                            if($scope.tc.tree[i].children[j].children[k].checked == true){
                                console.log($scope.tc.tree[i].children[j].children[k].name);
                                console.log($scope.tc.tree[i].children[j].children[k].code);
                                rights +=$scope.tc.tree[i].children[j].children[k].code+',';
                            }
                        }
                    }
                }
                console.log(rights);
                $scope.umracRoleSetup.roleContext = rights.substring(0,rights.length-1);
                $scope.isSaving = true;
                 if ($scope.umracRoleSetup.id != null) {
                 UmracRoleSetup.update($scope.umracRoleSetup, onSaveSuccess);
                 } else {
                 UmracRoleSetup.save($scope.umracRoleSetup, onSaveSuccess);
                 }
            };

            $scope.clear = function () {
                $scope.isSaving = false;
                $state.go('umracRoleSetup');
            };
        }]);
/* DETAIL */
angular.module('stepApp')
    .controller('UmracRoleSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'UmracRoleSetup',
        function ($scope, $rootScope, $stateParams, entity, UmracRoleSetup) {
        $scope.umracRoleSetup = entity;
        $scope.load = function (id) {
            UmracRoleSetup.get({id: id}, function(result) {
                $scope.umracRoleSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:umracRoleSetupUpdate', function(event, result) {
            $scope.umracRoleSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
