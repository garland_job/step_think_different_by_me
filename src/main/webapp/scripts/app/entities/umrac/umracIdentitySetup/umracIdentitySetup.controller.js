'use strict';

angular.module('stepApp')
    .controller('UmracIdentitySetupController',
    ['$scope', 'UmracIdentitySetup', 'UmracIdentitySetupSearch', 'ParseLinks',
    function ($scope, UmracIdentitySetup, UmracIdentitySetupSearch, ParseLinks) {
        $scope.umracIdentitySetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UmracIdentitySetup.query({page: $scope.page, size: 9000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracIdentitySetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            UmracIdentitySetup.get({id: id}, function(result) {
                $scope.umracIdentitySetup = result;
                $('#deleteUmracIdentitySetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            UmracIdentitySetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteUmracIdentitySetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            UmracIdentitySetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.umracIdentitySetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.umracIdentitySetup = {
                empId: null,
                userName: null,
                uPw: null,
                confm_Pw: null,
                status: null,
                createDate: null,
                createBy: null,
                updatedBy: null,
                updatedTime: null,
                id: null
            };
        };
    }]);

/* DIALOG */

angular.module('stepApp').controller('UmracIdentitySetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracIdentitySetup',
        function($scope, $stateParams, $state, entity, UmracIdentitySetup) {

        $scope.umracIdentitySetup = entity;
        $scope.load = function(id) {
            UmracIdentitySetup.get({id : id}, function(result) {
                $scope.umracIdentitySetup = result;

            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:umracIdentitySetupUpdate', result);
            $state.go('umracIdentitySetup');
        };

        $scope.save = function () {
            if ($scope.umracIdentitySetup.id != null) {
                UmracIdentitySetup.update($scope.umracIdentitySetup, onSaveFinished);
            } else {
                UmracIdentitySetup.save($scope.umracIdentitySetup, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $state.go('umracIdentitySetup');
        };
        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.blockForLength = true;
        $scope.blockForUserName = true;
        $scope.matchPass = function (pass, conPass) {
            if (pass != conPass) {
                $scope.notMatched = true;
                $scope.notMatcheds = "Password doesn't matched !";
            } else {
                $scope.notMatched = false;
                $scope.notMatcheds = "";
            }
        };
        $scope.checkDigit = function (result2) {
            $scope.getLength = result2.length;
            if ($scope.getLength < 8) {
                $scope.blockForLength = true;
                $scope.blockForLengthss = "Your password is required to be at least 8 characters";
            } else {
                $scope.blockForLength = false;
                $scope.blockForLengthss = "";
            }
        };
        $scope.checkUsername = function (result2) {
            $scope.getLength = result2.length;
            if ($scope.getLength < 8) {
                $scope.blockForUserName = true;
                $scope.blockForUserNamess = "Your user name is required to be at least 8 characters";
            } else {
                $scope.blockForUserName = false;
                $scope.blockForUserNamess = "";
            }
        }
}]);

/* DETAIL */

angular.module('stepApp')
    .controller('UmracIdentitySetupDetailController',
     ['$scope', '$rootScope', '$stateParams', 'entity', 'UmracIdentitySetup',
     function ($scope, $rootScope, $stateParams, entity, UmracIdentitySetup) {
        $scope.umracIdentitySetup = entity;
        $scope.load = function (id) {
            UmracIdentitySetup.get({id: id}, function(result) {
                $scope.umracIdentitySetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:umracIdentitySetupUpdate', function(event, result) {
            $scope.umracIdentitySetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
