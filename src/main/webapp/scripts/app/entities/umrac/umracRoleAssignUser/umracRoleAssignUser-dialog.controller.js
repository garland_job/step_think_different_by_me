'use strict';

angular.module('stepApp').controller('UmracRoleAssignUserDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracRoleAssignUser', 'UmracIdentitySetup', 'UmracRoleSetup', 'UserExistsChecker','ParseLinks', 'User',
        function ($scope, $stateParams, $state, entity, UmracRoleAssignUser, UmracIdentitySetup, UmracRoleSetup, UserExistsChecker, ParseLinks, User) {

            $scope.umracRoleAssignUser = entity;
            $scope.umracidentitysetups = [];
            $scope.umracrolesetups = [];
            $scope.existingUsers = [];

            UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracidentitysetups = result;
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                });
            });
            UmracRoleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracrolesetups = result;
            });
            $scope.load = function (id) {
                UmracRoleAssignUser.get({id: id}, function (result) {
                    $scope.umracRoleAssignUser = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracRoleAssignUserUpdate', result);
                $state.go('umracRoleAssignUser');
            };

            $scope.save = function () {
                /*$scope.userCheker = '';
                UserExistsChecker.get({id: id}, function (result) {
                    $scope.userCheker = result;
                });*/

                console.log($scope.umracRoleAssignUser);
                if ($scope.umracRoleAssignUser.id != null) {
                    UmracRoleAssignUser.update($scope.umracRoleAssignUser, onSaveFinished);
                } else {
                    UmracRoleAssignUser.save($scope.umracRoleAssignUser, onSaveFinished);
                }

            };

            $scope.clear = function () {
                $state.go('umracRoleAssignUser');
            };
            $scope.changeNewUser = function () {
                $scope.umracRoleAssignUser.userId = null;
                $scope.umracidentitysetups = [];
                //$scope.existingUsers = [];
                UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.umracidentitysetups = result;
                });
            };
            $scope.changeExistingUser = function () {
               $scope.umracRoleAssignUser.procUserId = null;
                $scope.existingUsers = [];
                //$scope.umracidentitysetups = [];
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                });
            };
            $scope.filterUserByStatus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
            $scope.filterRoleByStatus = function () {
            return function (item) {
                if (item.status == true)
                {
                    return true;
                }
                return false;
            };
            };
        }]);

/*Existing User*/
angular.module('stepApp').controller('UmracRoleAssignExitingUserDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracRoleAssignUser', 'UmracIdentitySetup', 'UmracRoleSetup', 'UserExistsChecker','ParseLinks','User','GetAuthorities',
        function ($scope, $stateParams, $state, entity, UmracRoleAssignUser, UmracIdentitySetup, UmracRoleSetup, UserExistsChecker, ParseLinks, User, GetAuthorities) {

            $scope.umracRoleAssignUser = entity;
            $scope.umracidentitysetups = [];
            $scope.umracrolesetups = [];
            $scope.existingUsers = [];
            $scope.authorities = [];

            UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracidentitysetups = result;
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                    /*UmracRoleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.umracrolesetups = result;
                    });*/
                    GetAuthorities.query({size: 2000}, function (data) {
                        $scope.authorities = [];
                        $scope.authorities = data;
                        /*$scope.singleJhiRoles = data;
                        for (var i = 0; i < $scope.singleJhiRoles.length; i++) {
                            for (var j = 0; j < $scope.authorities.length; j++) {
                                if ($scope.authorities[j].NAME == $scope.singleJhiRoles[i].authorityName) {
                                    $scope.authorities.splice(j, 1);
                                }
                            }
                        }*/
                        console.log($scope.authorities);
                    }, function (err) {
                        console.log(err)
                    });
                });
            });

            $scope.load = function (id) {
                UmracRoleAssignUser.get({id: id}, function (result) {
                    $scope.umracRoleAssignUser = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracRoleAssignUserUpdate', result);
                $state.go('umracRoleAssignUser');
            };

            $scope.save = function () {
                /*$scope.userCheker = '';
                UserExistsChecker.get({id: id}, function (result) {
                    $scope.userCheker = result;
                });*/

                if ($scope.umracRoleAssignUser.id != null) {
                    UmracRoleAssignUser.update($scope.umracRoleAssignUser, onSaveFinished);
                } else {
                    UmracRoleAssignUser.save($scope.umracRoleAssignUser, onSaveFinished);
                }


            };

            $scope.clear = function () {
                $state.go('umracRoleAssignUser');
            };
            $scope.changeNewUser = function () {
                $scope.umracRoleAssignUser.userId = null;
                $scope.umracidentitysetups = [];
                //$scope.existingUsers = [];
                UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.umracidentitysetups = result;
                });
            };
            $scope.changeExistingUser = function () {
                $scope.umracRoleAssignUser.procUserId = null;
                $scope.existingUsers = [];
                //$scope.umracidentitysetups = [];
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                });
            };
            $scope.filterUserByStatus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
            $scope.filterExitingUserByStatus = function () {
                return function (item) {
                    if (item.activated == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
            $scope.filterRoleByStatus = function () {
            return function (item) {
                if (item.status == true)
                {
                    return true;
                }
                return false;
            };
            };


        }]);
