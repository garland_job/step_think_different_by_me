'use strict';

angular.module('stepApp')
    .controller('UmracRoleAssignUserController',
    ['$scope', 'UmracRoleAssignUser', 'UmracRoleAssignUserSearch', 'ParseLinks',
    function ($scope, UmracRoleAssignUser, UmracRoleAssignUserSearch, ParseLinks) {
        $scope.umracRoleAssignUsers = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UmracRoleAssignUser.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracRoleAssignUsers = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            UmracRoleAssignUser.get({id: id}, function(result) {
                $scope.umracRoleAssignUser = result;
                $('#deleteUmracRoleAssignUserConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            UmracRoleAssignUser.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteUmracRoleAssignUserConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            UmracRoleAssignUserSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.umracRoleAssignUsers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.umracRoleAssignUser = {
                description: null,
                status: null,
                createDate: null,
                createBy: null,
                updatedBy: null,
                updatedTime: null,
                id: null
            };
        };
    }]);

/* DIALOG */

angular.module('stepApp').controller('UmracRoleAssignUserDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracRoleAssignUser', 'UmracIdentitySetup', 'UmracRoleSetup', 'UserExistsChecker','ParseLinks', 'User', 'UserListByLogin',
        function ($scope, $stateParams, $state, entity, UmracRoleAssignUser, UmracIdentitySetup, UmracRoleSetup, UserExistsChecker, ParseLinks, User, UserListByLogin) {

            $scope.umracRoleAssignUser = entity;
            $scope.umracidentitysetups = [];
            $scope.umracrolesetups = [];
            $scope.existingUsers = [];

            $scope.userInfoObj = function (value,servicio) {
                $scope.umracRoleAssignUser.procUserId = value.originalObject;
            };

            /*UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracidentitysetups = result;
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                });
            });*/
            UmracRoleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracrolesetups = result;
            });
            $scope.load = function (id) {
                UmracRoleAssignUser.get({id: id}, function (result) {
                    $scope.umracRoleAssignUser = result;
                });
            };

            $scope.findUserList = function (procUserIds) {
                //$scope.userLogin = procUserIds.toString();
                UserListByLogin.query({code: procUserIds.login}, function (resultProcUserIds) {
                    $scope.existingUsers = resultProcUserIds;
                    console.log("--------------------------");
                    console.log($scope.existingUsers);
                })
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracRoleAssignUserUpdate', result);
                $state.go('umracRoleAssignUser');
            };

            $scope.save = function () {
                /*$scope.userCheker = '';
                UserExistsChecker.get({id: id}, function (result) {
                    $scope.userCheker = result;
                });*/

                console.log($scope.umracRoleAssignUser);
                if ($scope.umracRoleAssignUser.id != null) {
                    UmracRoleAssignUser.update($scope.umracRoleAssignUser, onSaveFinished);
                } else {
                    UmracRoleAssignUser.save($scope.umracRoleAssignUser, onSaveFinished);
                }

            };

            $scope.clear = function () {
                $state.go('umracRoleAssignUser');
            };
            $scope.changeNewUser = function () {
                $scope.umracRoleAssignUser.userId = null;
                $scope.umracidentitysetups = [];
                //$scope.existingUsers = [];
                UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.umracidentitysetups = result;
                });
            };
            $scope.changeExistingUser = function () {
               $scope.umracRoleAssignUser.procUserId = null;
                $scope.existingUsers = [];
                //$scope.umracidentitysetups = [];
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                });
            };
            $scope.filterUserByStatus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
            $scope.filterRoleByStatus = function () {
            return function (item) {
                if (item.status == true)
                {
                    return true;
                }
                return false;
            };
            };
        }]);

/*Existing User*/
angular.module('stepApp').controller('UmracRoleAssignExitingUserDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'UmracRoleAssignUser', 'UmracIdentitySetup', 'UmracRoleSetup', 'UserExistsChecker','ParseLinks','User','GetAuthorities',
        function ($scope, $stateParams, $state, entity, UmracRoleAssignUser, UmracIdentitySetup, UmracRoleSetup, UserExistsChecker, ParseLinks, User, GetAuthorities) {

            $scope.umracRoleAssignUser = entity;
            $scope.umracidentitysetups = [];
            $scope.umracrolesetups = [];
            $scope.existingUsers = [];
            $scope.authorities = [];
            $scope.userInfoObj = function (value,servicio) {
                $scope.umracRoleAssignUser.procUserId = value.originalObject;
            };

            GetAuthorities.query({size: 2000}, function (data) {
                $scope.authorities = [];
                $scope.authorities = data;
                /*$scope.singleJhiRoles = data;
                 for (var i = 0; i < $scope.singleJhiRoles.length; i++) {
                 for (var j = 0; j < $scope.authorities.length; j++) {
                 if ($scope.authorities[j].NAME == $scope.singleJhiRoles[i].authorityName) {
                 $scope.authorities.splice(j, 1);
                 }
                 }
                 }*/
            }, function (err) {
            });

            /*UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.umracidentitysetups = result;
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                    /!*UmracRoleSetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.umracrolesetups = result;
                    });*!/

                });


            });*/

            $scope.load = function (id) {
                UmracRoleAssignUser.get({id: id}, function (result) {
                    $scope.umracRoleAssignUser = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracRoleAssignUserUpdate', result);
                $state.go('umracRoleAssignUser');
            };

            $scope.save = function () {
                /*$scope.userCheker = '';
                UserExistsChecker.get({id: id}, function (result) {
                    $scope.userCheker = result;
                });*/

                if ($scope.umracRoleAssignUser.id != null) {
                    UmracRoleAssignUser.update($scope.umracRoleAssignUser, onSaveFinished);
                } else {
                    UmracRoleAssignUser.save($scope.umracRoleAssignUser, onSaveFinished);
                }
            };

            $scope.clear = function () {
                $state.go('umracRoleAssignUser');
            };
            $scope.changeNewUser = function () {
                $scope.umracRoleAssignUser.userId = null;
                $scope.umracidentitysetups = [];
                //$scope.existingUsers = [];
                UmracIdentitySetup.query({page: $scope.page, size: 2000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.umracidentitysetups = result;
                });
            };
            $scope.changeExistingUser = function () {
                $scope.umracRoleAssignUser.procUserId = null;
                $scope.existingUsers = [];
                //$scope.umracidentitysetups = [];
                User.query({page: $scope.page, size: 10000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.existingUsers = result;
                });
            };
            $scope.filterUserByStatus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
            $scope.filterExitingUserByStatus = function () {
                return function (item) {
                    if (item.activated == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
            $scope.filterRoleByStatus = function () {
            return function (item) {
                if (item.status == true)
                {
                    return true;
                }
                return false;
            };
            };


        }]);


/* DETAIL */

angular.module('stepApp')
    .controller('UmracRoleAssignUserDetailController',
     ['$scope', '$rootScope', '$stateParams', 'entity', 'UmracRoleAssignUser', 'UmracIdentitySetup', 'UmracRoleSetup',
     function ($scope, $rootScope, $stateParams, entity, UmracRoleAssignUser, UmracIdentitySetup, UmracRoleSetup) {
        $scope.umracRoleAssignUser = entity;
        $scope.load = function (id) {
            UmracRoleAssignUser.get({id: id}, function(result) {
                $scope.umracRoleAssignUser = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:umracRoleAssignUserUpdate', function(event, result) {
            $scope.umracRoleAssignUser = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);

/*Rights Management of Existing User*/
/*Addd By Lemon*/
angular.module('stepApp').controller('UmracRightsManagementExistingUserController',
    ['$scope', '$stateParams', '$state', 'entity','UserAuthoritiesByUserId','RemoveSingleAuthority',
        function ($scope, $stateParams, $state, entity, UserAuthoritiesByUserId,RemoveSingleAuthority) {

            $scope.user={};

            $scope.userInfoObj = function (value,servicio) {
                $scope.user = value.originalObject;
		if($scope.user!==null)
			$scope.manageUser();

            };
            $scope.showData=false;
            $scope.userAuthorities=[];
            $scope.secureUser={};

            $scope.manageUser=function() {
                UserAuthoritiesByUserId.query({userId:$scope.user.id},function (result) {
                    $scope.userAuthorities=result;
                    $scope.showData=true;
                    $scope.secureUser=$scope.user;
                });
            };

            $scope.delete=function (rights,index) {
                RemoveSingleAuthority.query({userId:$scope.secureUser.id,rights:rights},function (results) {
                    if(results.isOk) {
                        $scope.userAuthorities.splice(index,1);
                    }
                })
            };

            /*var onSaveFinished = function (result) {
                $scope.$emit('stepApp:umracRoleAssignUserUpdate', result);
                $state.go('umracRoleAssignUser');
            };

            $scope.save = function () {
                /!*$scope.userCheker = '';
                 UserExistsChecker.get({id: id}, function (result) {
                 $scope.userCheker = result;
                 });*!/

                // if ($scope.umracRoleAssignUser.id != null) {
                //     UmracRoleAssignUser.update($scope.umracRoleAssignUser, onSaveFinished);
                // } else {
                //     UmracRoleAssignUser.save($scope.umracRoleAssignUser, onSaveFinished);
                // }
            };*/

            $scope.clear = function () {
                $state.go('umracRoleAssignUser');
            };


        }]);
