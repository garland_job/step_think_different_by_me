'use strict';

angular.module('stepApp')
    .controller('DeoController', function ($scope, $state, $modal, Deo, DeoSearch, ParseLinks, ActiveDeoHistLogs) {

        $scope.deos = [];
        $scope.deoHistLogs = ActiveDeoHistLogs.query();;
        $scope.page = 0;
        //ActiveDeoHistLogs.query();
        $scope.loadAll = function() {
            Deo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.deos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DeoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.deos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.deo = {
                contactNo: null,
                name: null,
                designation: null,
                orgName: null,
                dateCrated: null,
                dateModified: null,
                status: null,
                email: null,
                activated: null,
                id: null
            };
        };
    });
/*deo-dialog.controller.js*/

angular.module('stepApp').controller('DeoDialogController',
    ['$scope','$state', '$stateParams', '$q', 'Deo', 'District','Auth','DeoHistLog',
        function($scope,$state, $stateParams, $q, Deo, District,Auth,DeoHistLog) {
            $scope.deo = {};
            $scope.registerAccount = {};
            $scope.deoHistLog = {};

            if($stateParams.id){
                Deo.get({id: $stateParams.id}, function(result){
                    $scope.deo = result;
                });
            };

            var allDistrict= District.query({page: $scope.page, size: 65}, function(result, headers) { return result;});

            $scope.districts = allDistrict;
            /*$scope.load = function(id) {
             Deo.get({id : id}, function(result) {
             $scope.deo = result;
             });
             };*/

            var onSaveSuccess = function (result) {
                $scope.deoHistLog.deo = result;
                DeoHistLog.save($scope.deoHistLog);
                $scope.isSaving = false;
                // location.reload();
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            var onDeoLogSaveSuccess = function (result) {

                $scope.isSaving = false;
                $state.go('deo', null, { reload: true });
                //location.reload();
            };

            var onDeoLogSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.deo.id != null) {
                    Deo.update($scope.deo, onSaveSuccess, onSaveError);
                } else {
                    //if ($scope.registerAccount.password !== $scope.confirmPassword) {
                    if($scope.notMatched == true){
                        $scope.doNotMatch = 'ERROR';
                    }else{
                        $scope.registerAccount.langKey = 'en';
                        //$scope.registerAccount.firstName = "DEO Of "+$scope.deoHistLog.district.name;
                        $scope.registerAccount.firstName = "DEO";
                        //$scope.registerAccount.district = scope.deoHistLog.district;
                        $scope.doNotMatch = null;
                        $scope.error = null;
                        $scope.errorUserExists = null;
                        $scope.errorEmailExists = null;
                        $scope.registerAccount.activated = true;
                        $scope.registerAccount.authorities = ["ROLE_DEO"];

                        Auth.createDeoAccount($scope.registerAccount).then(function (result) {
                            $scope.success = 'OK';
                            $scope.deo.user = result;
                            $scope.deo.email = $scope.registerAccount.email;
                            //console.log('comes inside success');
                            Deo.save($scope.deo, function (result2) {
                                $scope.deoHistLog.deo = result2;
                                DeoHistLog.save($scope.deoHistLog,onDeoLogSaveSuccess);
                            });
                        }).catch(function (response) {
                            $scope.success = null;
                            if (response.status === 400 && response.data === 'login already in use') {
                                $scope.errorUserExists = 'ERROR';
                            } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                                $scope.errorEmailExists = 'ERROR';
                            } else {
                                $scope.error = 'ERROR';
                            }
                        });
                    }
                    //Deo.save($scope.deo, onSaveSuccess, onSaveError);
                }
            };

            $scope.matchPass=function(pass,conPass){
                console.log('----------');
                if(pass != conPass){
                    console.log('7777777777');
                    $scope.notMatched = true;
                }else{
                    console.log('6666666666');
                    $scope.notMatched = false;
                }

            };
            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
            };
        }]);
/*deo-detail.controller.js*/

angular.module('stepApp')
    .controller('DeoDetailController', function ($scope, $rootScope, $stateParams, entity, Deo, User) {
        $scope.deo = entity;
        $scope.load = function (id) {
            Deo.get({id: id}, function(result) {
                $scope.deo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:deoUpdate', function(event, result) {
            $scope.deo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*deo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('DeoDeleteController', function($scope, $modalInstance, entity, Deo) {

        $scope.deo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Deo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
