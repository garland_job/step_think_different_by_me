'use strict';

angular.module('stepApp')
    .controller('PrlPayscaleBasicInfoController', function ($scope, $state, PrlPayscaleBasicInfo, PrlPayscaleBasicInfoSearch, ParseLinks) {

        $scope.prlPayscaleBasicInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.stateName = "prlPayscaleInfo";
        $scope.page = 0;
        $scope.loadAll = function() {
            PrlPayscaleBasicInfo.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlPayscaleBasicInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlPayscaleBasicInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlPayscaleBasicInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlPayscaleBasicInfo = {
                serialNumber: null,
                basicAmount: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlPayscaleBasicInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlPayscaleBasicInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'PrlPayscaleBasicInfo', 'PrlPayscaleInfo',
        function($scope, $stateParams, $modalInstance, entity, PrlPayscaleBasicInfo, PrlPayscaleInfo) {

            $scope.prlPayscaleBasicInfo = entity;
            $scope.prlpayscaleinfos = PrlPayscaleInfo.query();
            $scope.load = function(id) {
                PrlPayscaleBasicInfo.get({id : id}, function(result) {
                    $scope.prlPayscaleBasicInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlPayscaleBasicInfoUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.prlPayscaleBasicInfo.id != null) {
                    PrlPayscaleBasicInfo.update($scope.prlPayscaleBasicInfo, onSaveSuccess, onSaveError);
                } else {
                    PrlPayscaleBasicInfo.save($scope.prlPayscaleBasicInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*prlPayscaleBasicInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlPayscaleBasicInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlPayscaleBasicInfo, PrlPayscaleInfo) {
        $scope.prlPayscaleBasicInfo = entity;
        $scope.load = function (id) {
            PrlPayscaleBasicInfo.get({id: id}, function(result) {
                $scope.prlPayscaleBasicInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlPayscaleBasicInfoUpdate', function(event, result) {
            $scope.prlPayscaleBasicInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlPayscaleBasicInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlPayscaleBasicInfoDeleteController', function($scope, $uibModalInstance, entity, PrlPayscaleBasicInfo) {

        $scope.prlPayscaleBasicInfo = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlPayscaleBasicInfo.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
