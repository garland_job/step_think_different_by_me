'use strict';

angular.module('stepApp')
    .controller('PrlGeneratedSalaryInfoController', function ($scope, $state, PrlGeneratedSalaryInfo, PrlGeneratedSalaryInfoSearch, ParseLinks) {

        $scope.prlGeneratedSalaryInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            PrlGeneratedSalaryInfo.query({page: $scope.page - 1, size: 2000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlGeneratedSalaryInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlGeneratedSalaryInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlGeneratedSalaryInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlGeneratedSalaryInfo = {
                yearName: null,
                monthName: null,
                salaryType: null,
                processDate: null,
                disburseStatus: 'N',
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlGeneratedSalaryInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlGeneratedSalaryInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlGeneratedSalaryInfo','User','Principal','DateUtils','PrlSalaryGenerationProc','PrlSalaryDisburseProc','PrlGeneratedSalaryInfoFilter','PrlGeneratedSalaryInfoHistory','PrlOnetimeAllowInfoByStat','MiscTypeSetupByCategory','HrEmpWorkAreaDtlInfoByStatus',
        function($scope, $rootScope, $stateParams, $state, entity, PrlGeneratedSalaryInfo, User, Principal, DateUtils, PrlSalaryGenerationProc,PrlSalaryDisburseProc,PrlGeneratedSalaryInfoFilter,PrlGeneratedSalaryInfoHistory,PrlOnetimeAllowInfoByStat,MiscTypeSetupByCategory,HrEmpWorkAreaDtlInfoByStatus) {

            $scope.prlGeneratedSalaryInfo = entity;
            $scope.load = function(id) {
                PrlGeneratedSalaryInfo.get({id : id}, function(result) {
                    $scope.prlGeneratedSalaryInfo = result;
                });
            };

            $scope.workAreaList         = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.workAreaDtlList      = HrEmpWorkAreaDtlInfoByStatus.get({stat:'true'});
            $scope.workAreaDtlListFiltr = $scope.workAreaDtlList;
            $scope.prlOnetimeAllowInfos = PrlOnetimeAllowInfoByStat.get({stat: 'true'});
            $scope.prlGeneratedSalaryInfoHist = {};
            $scope.alreadyGenerated = false;
            $scope.isGenerate = true;
            $scope.alreadyDisbursed = false;
            $scope.allDataSelected = false;

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.loadWorkAreaDetailByWork = function(workArea)
            {
                $scope.workAreaDtlListFiltr = [];
                console.log("WorkAreaOrOrg:"+JSON.stringify(workArea)+"");
                angular.forEach($scope.workAreaDtlList,function(workAreaDtl)
                {
                    if(workArea.id == workAreaDtl.workArea.id){
                        $scope.workAreaDtlListFiltr.push(workAreaDtl);
                    }
                });
            };

            $scope.checkAlreadyGeneratedSalary = function ()
            {
                $scope.allDataSelected = false;
                console.log("Check old data month: "+$scope.prlGeneratedSalaryInfo.monthName+", year: "+$scope.prlGeneratedSalaryInfo.yearName+", type: "+$scope.prlGeneratedSalaryInfo.salaryType );
                if($scope.prlGeneratedSalaryInfo.salaryType != null &&
                    $scope.prlGeneratedSalaryInfo.monthName != null &&
                    $scope.prlGeneratedSalaryInfo.yearName != null &&
                    $scope.prlGeneratedSalaryInfo.workAreaDtl != null
                )
                {
                    var otaid = 0;
                    if($scope.prlGeneratedSalaryInfo.onetimeAllowInfo && $scope.prlGeneratedSalaryInfo.onetimeAllowInfo.id )
                        otaid = $scope.prlGeneratedSalaryInfo.onetimeAllowInfo.id

                    if($scope.prlGeneratedSalaryInfo.salaryType==='ONETIME' && otaid > 0 && $scope.prlGeneratedSalaryInfo.workAreaDtl.id > 0)
                    {
                        $scope.allDataSelected = true;
                        PrlGeneratedSalaryInfoFilter.get({year: $scope.prlGeneratedSalaryInfo.yearName,
                            month: $scope.prlGeneratedSalaryInfo.monthName,
                            saltype:$scope.prlGeneratedSalaryInfo.salaryType,
                            otaid:otaid,
                            orgType:'Organization',orgInstId:$scope.prlGeneratedSalaryInfo.workAreaDtl.id}, function (result)
                        {
                            console.log("resultOnMontyYearTypeOneTime orgId:"+$scope.prlGeneratedSalaryInfo.workAreaDtl.id+",json:  "+JSON.stringify(result));
                            //$scope.generatedSalaryDto = resultDto;

                            if(result!=null && result.length > 0)
                            {
                                $scope.alreadyGenerated = true;
                                if(result[0].disburseStatus==='Y')
                                    $scope.alreadyDisbursed = true;
                                else $scope.alreadyDisbursed = false;
                            }
                            else
                            {
                                $scope.alreadyGenerated = false;
                                $scope.alreadyDisbursed = false;
                            }

                            //console.log("PST: AllowanceExtraLen: "+$scope.allowanceExtraList.length+", DeductionExtraLen: "+$scope.deductionExtraList.length);
                            $scope.isLoading = false;
                        }, function (response)
                        {
                            console.log("error: "+response);
                        });
                    }
                    else if($scope.prlGeneratedSalaryInfo.salaryType==='MONTHLY' && $scope.prlGeneratedSalaryInfo.workAreaDtl.id > 0)
                    {
                        $scope.allDataSelected = true;
                        PrlGeneratedSalaryInfoFilter.get({year: $scope.prlGeneratedSalaryInfo.yearName,
                            month: $scope.prlGeneratedSalaryInfo.monthName,
                            saltype:$scope.prlGeneratedSalaryInfo.salaryType,
                            otaid:otaid,
                            orgType:'Organization',orgInstId:$scope.prlGeneratedSalaryInfo.workAreaDtl.id}, function (result)
                        {
                            console.log("resultOnMontyYearType Monthly orgId:"+$scope.prlGeneratedSalaryInfo.workAreaDtl.id+",json:  "+JSON.stringify(result));
                            //$scope.generatedSalaryDto = resultDto;

                            if(result!=null && result.length > 0)
                            {
                                $scope.alreadyGenerated = true;
                                if(result[0].disburseStatus==='Y')
                                    $scope.alreadyDisbursed = true;
                                else $scope.alreadyDisbursed = false;
                            }
                            else
                            {
                                $scope.alreadyGenerated = false;
                                $scope.alreadyDisbursed = false;
                            }
                            //console.log("PST: AllowanceExtraLen: "+$scope.allowanceExtraList.length+", DeductionExtraLen: "+$scope.deductionExtraList.length);
                            $scope.isLoading = false;
                        }, function (response)
                        {
                            console.log("error: "+response);
                        });

                    }
                }

            };

            $scope.getLastInsertedDataHistory = function ()
            {
                console.log("data inserted history " );

                PrlGeneratedSalaryInfoHistory.get({}, function (result)
                {
                    console.log("GeneratedSalaryDto: "+JSON.stringify(result));
                    $scope.prlGeneratedSalaryInfoHist = result;
                    $scope.isLoading = false;
                }, function (response)
                {
                    console.log("error: "+response);
                });
            };

            $scope.getLastInsertedDataHistory();

            $scope.yearList = $rootScope.generateYearList(new Date().getFullYear()-1, 5);

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlGeneratedSalaryInfoUpdate', result);
                $scope.isSaving = false;
                $scope.responseMsg = result;
                //$state.go('prlGeneratedSalaryInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
            };

            $scope.generateSalary = function () {
                $scope.isSaving = true;
                $scope.prlGeneratedSalaryInfo.organizationType = "Organization";
                $scope.prlGeneratedSalaryInfo.orgInstId = $scope.prlGeneratedSalaryInfo.workAreaDtl.id;

                $scope.prlGeneratedSalaryInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlGeneratedSalaryInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.prlGeneratedSalaryInfo.id != null) {
                    PrlGeneratedSalaryInfo.update($scope.prlGeneratedSalaryInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlGeneratedSalaryInfo.updated');
                } else {
                    $scope.prlGeneratedSalaryInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlGeneratedSalaryInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlGeneratedSalaryInfo.save($scope.prlGeneratedSalaryInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlGeneratedSalaryInfo.created');
                }
            };

            $scope.disburseSalary = function ()
            {
                $scope.isSaving = true;
                $scope.prlGeneratedSalaryInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlGeneratedSalaryInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.prlGeneratedSalaryInfo.processDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.prlGeneratedSalaryInfo.createBy = $scope.loggedInUser.id;
                $scope.prlGeneratedSalaryInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                console.log(JSON.stringify( $scope.prlGeneratedSalaryInfo));
                PrlSalaryDisburseProc.update($scope.prlGeneratedSalaryInfo, onSaveDisburseSuccess, onSaveDisburseError);
                $rootScope.setWarningMessage('stepApp.prlGeneratedSalaryInfo.updated');
            };

            var onSaveDisburseSuccess = function (result) {
                $scope.$emit('stepApp:prlGeneratedSalaryInfoUpdate', result);
                $scope.isSaving = false;
                $scope.responseMsg = result;
                //$state.go('prlGeneratedSalaryInfo');
            };

            var onSaveDisburseError = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
            };

            $scope.clear = function() {

            };
        }]);


angular.module('stepApp').controller('PrlGeneratedSalaryInfoInstituteOrganizationController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlGeneratedSalaryInfo','User','Principal','DateUtils','PrlSalaryGenerationProc','PrlSalaryDisburseProc','PrlGeneratedSalaryInfoFilter','PrlGeneratedSalaryInfoHistoryByInst','PrlGeneratedSalaryInfoHistoryByOrg','PrlOnetimeAllowInfoByStat','InstituteByLogin','HrEmpWorkAreaDtlInfoByLogin',
        function($scope, $rootScope, $stateParams, $state, entity, PrlGeneratedSalaryInfo, User, Principal, DateUtils, PrlSalaryGenerationProc,PrlSalaryDisburseProc,PrlGeneratedSalaryInfoFilter,PrlGeneratedSalaryInfoHistoryByInst,PrlGeneratedSalaryInfoHistoryByOrg, PrlOnetimeAllowInfoByStat,InstituteByLogin,HrEmpWorkAreaDtlInfoByLogin) {

            $scope.prlGeneratedSalaryInfo = entity;
            $scope.load = function(id) {
                PrlGeneratedSalaryInfo.get({id : id}, function(result) {
                    $scope.prlGeneratedSalaryInfo = result;
                });
            };

            $scope.prlOnetimeAllowInfos = PrlOnetimeAllowInfoByStat.get({stat: 'true'});

            $scope.prlGeneratedSalaryInfoHist = {};

            $scope.alreadyGenerated = false;
            $scope.isGenerate = true;
            $scope.alreadyDisbursed = false;
            $scope.allDataSelected = false;
            $scope.organizationType = "Institute";
            $scope.loggedInOrgInstId = 0;

            $scope.userRole = 'ROLE_INSTITUTE';

            Principal.identity().then(function (account) {
                $scope.account = account;

                console.log(JSON.stringify($scope.account.authorities));

                if($scope.isInArray('ROLE_INSTITUTE', $scope.account.authorities))
                {
                    $scope.userRole = 'ROLE_INSTITUTE';
                    $scope.organizationType = "Institute";
                    $scope.getLastInsertedDataHistoryByInst();

                    InstituteByLogin.query({}, function (result) {
                        $scope.logInInstitute = result;
                        $scope.loggedInOrgInstId = $scope.logInInstitute.id;
                    });
                }
                else if($scope.isInArray('ROLE_ORG_USER', $scope.account.authorities))
                {
                    $scope.userRole = 'ROLE_ORG_USER';
                    $scope.organizationType = "Organization";
                    $scope.getLastInsertedDataHistoryByOrg();

                    HrEmpWorkAreaDtlInfoByLogin.query({}, function (result) {
                        $scope.logInOrganization = result;
                        $scope.loggedInOrgInstId = $scope.logInOrganization.id;
                    });
                }
            });

            $scope.isInArray = function isInArray(value, array) {
                return array.indexOf(value) > -1;
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.checkAlreadyGeneratedSalary = function ()
            {
                $scope.allDataSelected = false;
                console.log("Check old data month: "+$scope.prlGeneratedSalaryInfo.monthName+", year: "+$scope.prlGeneratedSalaryInfo.yearName+", type: "+$scope.prlGeneratedSalaryInfo.salaryType );
                if($scope.prlGeneratedSalaryInfo.salaryType != null &&
                    $scope.prlGeneratedSalaryInfo.monthName != null &&
                    $scope.prlGeneratedSalaryInfo.yearName != null
                )
                {
                    var otaid = 0;
                    if($scope.prlGeneratedSalaryInfo.onetimeAllowInfo && $scope.prlGeneratedSalaryInfo.onetimeAllowInfo.id )
                        otaid = $scope.prlGeneratedSalaryInfo.onetimeAllowInfo.id

                    if($scope.prlGeneratedSalaryInfo.salaryType==='ONETIME' && otaid > 0 && $scope.loggedInOrgInstId > 0)
                    {

                        $scope.allDataSelected = true;
                        PrlGeneratedSalaryInfoFilter.get({year: $scope.prlGeneratedSalaryInfo.yearName,
                            month: $scope.prlGeneratedSalaryInfo.monthName,
                            saltype:$scope.prlGeneratedSalaryInfo.salaryType,
                            otaid:otaid,
                            orgType:$scope.organizationType,orgInstId:$scope.loggedInOrgInstId}, function (result)
                        {
                            console.log("OnetimeSalaryGenerationCheck RefId:"+$scope.loggedInOrgInstId+", orgType:  "+$scope.organizationType);
                            //$scope.generatedSalaryDto = resultDto;

                            if(result!=null && result.length > 0)
                            {
                                $scope.alreadyGenerated = true;
                                if(result[0].disburseStatus==='Y')
                                    $scope.alreadyDisbursed = true;
                                else $scope.alreadyDisbursed = false;
                            }
                            else
                            {
                                $scope.alreadyGenerated = false;
                                $scope.alreadyDisbursed = false;
                            }

                            //console.log("PST: AllowanceExtraLen: "+$scope.allowanceExtraList.length+", DeductionExtraLen: "+$scope.deductionExtraList.length);
                            $scope.isLoading = false;
                        }, function (response)
                        {
                            console.log("error: "+response);
                        });
                    }
                    else if($scope.prlGeneratedSalaryInfo.salaryType==='MONTHLY' && $scope.loggedInOrgInstId > 0)
                    {
                        $scope.allDataSelected = true;
                        PrlGeneratedSalaryInfoFilter.get({year: $scope.prlGeneratedSalaryInfo.yearName,
                            month: $scope.prlGeneratedSalaryInfo.monthName,
                            saltype:$scope.prlGeneratedSalaryInfo.salaryType,
                            otaid:otaid,
                            orgType:$scope.organizationType,orgInstId:$scope.loggedInOrgInstId}, function (result)
                        {
                            console.log("MonthlySelaryGenerationCheck RefId:"+$scope.loggedInOrgInstId+", orgType:  "+$scope.organizationType);
                            //$scope.generatedSalaryDto = resultDto;

                            if(result!=null && result.length > 0)
                            {
                                $scope.alreadyGenerated = true;
                                if(result[0].disburseStatus==='Y')
                                    $scope.alreadyDisbursed = true;
                                else $scope.alreadyDisbursed = false;
                            }
                            else
                            {
                                $scope.alreadyGenerated = false;
                                $scope.alreadyDisbursed = false;
                            }

                            //console.log("PST: AllowanceExtraLen: "+$scope.allowanceExtraList.length+", DeductionExtraLen: "+$scope.deductionExtraList.length);
                            $scope.isLoading = false;
                        }, function (response)
                        {
                            console.log("error: "+response);
                        });

                    }
                }

            };

            $scope.getLastInsertedDataHistoryByInst = function ()
            {
                console.log("data inserted history for logged in institute " );

                PrlGeneratedSalaryInfoHistoryByInst.get({}, function (result)
                {
                    console.log("GeneratedSalaryDto: "+JSON.stringify(result));
                    $scope.prlGeneratedSalaryInfoHist = result;
                    $scope.isLoading = false;
                }, function (response)
                {
                    console.log("error: "+response);
                });
            };

            $scope.getLastInsertedDataHistoryByOrg = function ()
            {
                console.log("data inserted history for logged in organization " );

                PrlGeneratedSalaryInfoHistoryByOrg.get({}, function (result)
                {
                    console.log("GeneratedSalaryDto: "+JSON.stringify(result));
                    $scope.prlGeneratedSalaryInfoHist = result;
                    $scope.isLoading = false;
                }, function (response)
                {
                    console.log("error: "+response);
                });
            };

            $scope.yearList = $rootScope.generateYearList(new Date().getFullYear()-1, 5);

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlGeneratedSalaryInfoUpdate', result);
                $scope.isSaving = false;
                $scope.responseMsg = result;
                //$state.go('prlGeneratedSalaryInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
            };

            $scope.generateSalary = function () {
                $scope.isSaving = true;

                console.log("GenerateSalary orgType:"+$scope.organizationType+", orgInstId:  "+$scope.loggedInOrgInstId);

                $scope.prlGeneratedSalaryInfo.organizationType = $scope.organizationType;
                $scope.prlGeneratedSalaryInfo.orgInstId = $scope.loggedInOrgInstId;

                $scope.prlGeneratedSalaryInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlGeneratedSalaryInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.prlGeneratedSalaryInfo.id != null) {
                    PrlGeneratedSalaryInfo.update($scope.prlGeneratedSalaryInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlGeneratedSalaryInfo.updated');
                } else {
                    $scope.prlGeneratedSalaryInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlGeneratedSalaryInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlGeneratedSalaryInfo.save($scope.prlGeneratedSalaryInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlGeneratedSalaryInfo.created');
                }
            };

            $scope.disburseSalary = function ()
            {
                $scope.isSaving = true;
                $scope.prlGeneratedSalaryInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlGeneratedSalaryInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.prlGeneratedSalaryInfo.processDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.prlGeneratedSalaryInfo.createBy = $scope.loggedInUser.id;
                $scope.prlGeneratedSalaryInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                console.log(JSON.stringify( $scope.prlGeneratedSalaryInfo));
                PrlSalaryDisburseProc.update($scope.prlGeneratedSalaryInfo, onSaveDisburseSuccess, onSaveDisburseError);
                $rootScope.setWarningMessage('stepApp.prlGeneratedSalaryInfo.updated');
            };

            var onSaveDisburseSuccess = function (result) {
                $scope.$emit('stepApp:prlGeneratedSalaryInfoUpdate', result);
                $scope.isSaving = false;
                $scope.responseMsg = result;
                //$state.go('prlGeneratedSalaryInfo');
            };

            var onSaveDisburseError = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
            };

            $scope.clear = function() {

            };
        }]);

/*prlGeneratedSalaryInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlGeneratedSalaryInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlGeneratedSalaryInfo) {
        $scope.prlGeneratedSalaryInfo = entity;
        $scope.load = function (id) {
            PrlGeneratedSalaryInfo.get({id: id}, function(result) {
                $scope.prlGeneratedSalaryInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlGeneratedSalaryInfoUpdate', function(event, result) {
            $scope.prlGeneratedSalaryInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlGeneratedSalaryInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlGeneratedSalaryInfoDeleteController', function($scope, $rootScope, $modalInstance, entity, PrlGeneratedSalaryInfo) {

        $scope.prlGeneratedSalaryInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlGeneratedSalaryInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlGeneratedSalaryInfo.deleted');
                });
        };

    });
