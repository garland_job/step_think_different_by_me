'use strict';

angular.module('stepApp')
    .controller('PrlLocalitySetInfoController', function ($scope, $rootScope, $state, PrlLocalitySetInfo, PrlLocalitySetInfoSearch, ParseLinks) {

        $scope.prlLocalitySetInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "prlLocalityInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            PrlLocalitySetInfo.query({page: $scope.page, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlLocalitySetInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlLocalitySetInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlLocalitySetInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlLocalitySetInfo = {
                name: null,
                type: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlLocalitySetInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlLocalitySetInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlLocalitySetInfo', 'PrlLocalityInfo','User','Principal','DateUtils','PrlLocalitySetInfoUniqueness',
        function($scope, $rootScope, $stateParams, $state, entity, PrlLocalitySetInfo, PrlLocalityInfo, User, Principal, DateUtils,PrlLocalitySetInfoUniqueness) {

            $scope.prlLocalitySetInfo = entity;
            $scope.prllocalityinfos = PrlLocalityInfo.query();
            $scope.load = function(id) {
                PrlLocalitySetInfo.get({id : id}, function(result) {
                    $scope.prlLocalitySetInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.localitySetAlreadyExist = false;
            $scope.checkLocalitySetUniqByName = function()
            {
                console.log("name: "+$scope.prlLocalitySetInfo.name)
                if($scope.prlLocalitySetInfo.name != null && $scope.prlLocalitySetInfo.name.length > 0)
                {
                    $scope.editForm.name.$pending = true;
                    PrlLocalitySetInfoUniqueness.get({name: $scope.prlLocalitySetInfo.name}, function(result)
                    {
                        //console.log(JSON.stringify(result));
                        $scope.isSaving = !result.isValid;
                        $scope.editForm.name.$pending = false;
                        if(result.isValid)
                        {
                            console.log("valid");
                            $scope.localitySetAlreadyExist = false;
                        }
                        else
                        {
                            console.log("not valid");
                            $scope.localitySetAlreadyExist = true;
                        }
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.name.$pending = false;
                    });
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlLocalitySetInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('prlLocalitySetInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.prlLocalitySetInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlLocalitySetInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.prlLocalitySetInfo.id != null) {
                    PrlLocalitySetInfo.update($scope.prlLocalitySetInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlLocalitySetInfo.updated');
                }
                else
                {
                    $scope.prlLocalitySetInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlLocalitySetInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlLocalitySetInfo.save($scope.prlLocalitySetInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlLocalitySetInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*prlLocalitySetInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlLocalitySetInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlLocalitySetInfo, PrlLocalityInfo) {
        $scope.prlLocalitySetInfo = entity;
        $scope.load = function (id) {
            PrlLocalitySetInfo.get({id: id}, function(result) {
                $scope.prlLocalitySetInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlLocalitySetInfoUpdate', function(event, result) {
            $scope.prlLocalitySetInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlLocalitySetInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlLocalitySetInfoDeleteController', function($scope, $rootScope, $modalInstance, entity, PrlLocalitySetInfo) {

        $scope.prlLocalitySetInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlLocalitySetInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlLocalitySetInfo.deleted');
                });
        };

    });
