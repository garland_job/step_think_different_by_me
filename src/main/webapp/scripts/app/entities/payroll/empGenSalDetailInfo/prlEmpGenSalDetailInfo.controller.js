'use strict';

angular.module('stepApp')
    .controller('PrlEmpGenSalDetailInfoController', function ($scope, $state, PrlEmpGenSalDetailInfo, PrlEmpGenSalDetailInfoSearch, ParseLinks) {

        $scope.prlEmpGenSalDetailInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            PrlEmpGenSalDetailInfo.query({page: $scope.page - 1, size: 2000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlEmpGenSalDetailInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlEmpGenSalDetailInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlEmpGenSalDetailInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlEmpGenSalDetailInfo = {
                allowDeducType: null,
                allowDeducId: null,
                allowDeducName: null,
                allowDeducValue: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });

/* prlEmpGenSalDetailInfo-dialog.controller.js */

angular.module('stepApp').controller('PrlEmpGenSalDetailInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlEmpGenSalDetailInfo', 'PrlEmpGeneratedSalInfo','User','Principal','DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, PrlEmpGenSalDetailInfo, PrlEmpGeneratedSalInfo, User, Principal, DateUtils) {

            $scope.prlEmpGenSalDetailInfo = entity;
            $scope.prlempgeneratedsalinfos = PrlEmpGeneratedSalInfo.query();
            $scope.load = function(id) {
                PrlEmpGenSalDetailInfo.get({id : id}, function(result) {
                    $scope.prlEmpGenSalDetailInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlEmpGenSalDetailInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('prlEmpGenSalDetailInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.prlEmpGenSalDetailInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlEmpGenSalDetailInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.prlEmpGenSalDetailInfo.id != null) {
                    PrlEmpGenSalDetailInfo.update($scope.prlEmpGenSalDetailInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlEmpGenSalDetailInfo.updated');
                } else {
                    $scope.prlEmpGenSalDetailInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlEmpGenSalDetailInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlEmpGenSalDetailInfo.save($scope.prlEmpGenSalDetailInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlEmpGenSalDetailInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);

/* prlEmpGenSalDetailInfo-detail.controller.js */

angular.module('stepApp')
    .controller('PrlEmpGenSalDetailInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlEmpGenSalDetailInfo, PrlEmpGeneratedSalInfo) {
        $scope.prlEmpGenSalDetailInfo = entity;
        $scope.load = function (id) {
            PrlEmpGenSalDetailInfo.get({id: id}, function(result) {
                $scope.prlEmpGenSalDetailInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlEmpGenSalDetailInfoUpdate', function(event, result) {
            $scope.prlEmpGenSalDetailInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

/* prlEmpGenSalDetailInfo-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('PrlEmpGenSalDetailInfoDeleteController', function($scope, $rootScope, $modalInstance, entity, PrlEmpGenSalDetailInfo) {

        $scope.prlEmpGenSalDetailInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlEmpGenSalDetailInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlEmpGenSalDetailInfo.deleted');
                });
        };

    });
