'use strict';

angular.module('stepApp')
    .controller('PrlEmpAdditionalAssignInfoController', function ($scope, PrlEmpAdditionalAssignInfo, PrlEmpAdditionalAssignInfoSearch, ParseLinks) {
        $scope.prlEmpAdditionalAssignInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PrlEmpAdditionalAssignInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.prlEmpAdditionalAssignInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PrlEmpAdditionalAssignInfo.get({id: id}, function(result) {
                $scope.prlEmpAdditionalAssignInfo = result;
                $('#deletePrlEmpAdditionalAssignInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PrlEmpAdditionalAssignInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePrlEmpAdditionalAssignInfoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PrlEmpAdditionalAssignInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlEmpAdditionalAssignInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlEmpAdditionalAssignInfo = {
                effectedDateFrom: null,
                effectedDateTo: null,
                allowanceAmount: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
