'use strict';

angular.module('stepApp')
    .controller('PrlEmpAdditionalAssignInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlEmpAdditionalAssignInfo, HrEmployeeInfo, PrlAllowDeductInfo) {
        $scope.prlEmpAdditionalAssignInfo = entity;
        $scope.load = function (id) {
            PrlEmpAdditionalAssignInfo.get({id: id}, function(result) {
                $scope.prlEmpAdditionalAssignInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlEmpAdditionalAssignInfoUpdate', function(event, result) {
            $scope.prlEmpAdditionalAssignInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
