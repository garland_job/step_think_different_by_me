'use strict';

angular.module('stepApp').controller('PrlEmpAdditionalAssignInfoDialogController',
    ['$scope', '$stateParams','$state', 'entity', 'PrlEmpAdditionalAssignInfo', 'HrEmployeeInfoCompactList', 'PrlAllowDeductInfoByCategory','PrlEmpAdditionalAssignInfoUniquenessCheck','DateUtils','User','Principal',
        function($scope, $stateParams,$state, entity, PrlEmpAdditionalAssignInfo, HrEmployeeInfoCompactList, PrlAllowDeductInfoByCategory,PrlEmpAdditionalAssignInfoUniquenessCheck, DateUtils, User, Principal) {

        $scope.prlEmpAdditionalAssignInfo = entity;

        $scope.hremployeeinfos = HrEmployeeInfoCompactList.get({key:'activeStatus',value:'true'});

        $scope.prlallowdeductinfos = PrlAllowDeductInfoByCategory.get({cat:'AA'});
        $scope.load = function(id) {
            PrlEmpAdditionalAssignInfo.get({id : id}, function(result) {
                $scope.prlEmpAdditionalAssignInfo = result;
            });
        };

        $scope.loggedInUser =   {};
        $scope.getLoggedInUser = function ()
        {
            Principal.identity().then(function (account)
            {
                User.get({login: account.login}, function (result)
                {
                    $scope.loggedInUser = result;
                });
            });
        };
        $scope.getLoggedInUser();

        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        var onSaveFinished = function (result)
        {
            $scope.isSaving = false;
            $scope.$emit('stepApp:prlEmpAdditionalAssignInfoUpdate', result);
            $state.go('prlEmpAdditionalAssignInfo');
        };

        $scope.save = function ()
        {
            $scope.isSaving = true;
            $scope.prlEmpAdditionalAssignInfo.updateBy = $scope.loggedInUser.id;
            $scope.prlEmpAdditionalAssignInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
            if ($scope.prlEmpAdditionalAssignInfo.id != null)
            {
                PrlEmpAdditionalAssignInfo.update($scope.prlEmpAdditionalAssignInfo, onSaveFinished);
            } else
            {
                $scope.prlEmpAdditionalAssignInfo.createBy = $scope.loggedInUser.id;
                $scope.prlEmpAdditionalAssignInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                PrlEmpAdditionalAssignInfo.save($scope.prlEmpAdditionalAssignInfo, onSaveFinished);
            }
        };

        $scope.additionalAssignAlreadyExist = false;
        $scope.additionalAssignValidationChecking = false;
        $scope.checkAdditionAssignByEmployeeAndAllowance = function ()
        {
            var currentId = 0;
            if($scope.prlEmpAdditionalAssignInfo.employeeInfo!=null && $scope.prlEmpAdditionalAssignInfo.employeeInfo.id!=null &&
                $scope.prlEmpAdditionalAssignInfo.allowanceInfo!=null && $scope.prlEmpAdditionalAssignInfo.allowanceInfo.id!=null)
            {
                if($scope.prlEmpAdditionalAssignInfo != null && $scope.prlEmpAdditionalAssignInfo.id != null)
                {
                    currentId  = $scope.prlEmpAdditionalAssignInfo.id;
                }
                console.log("Emp: "+$scope.prlEmpAdditionalAssignInfo.employeeInfo.fullName+", Allowance: "+$scope.prlEmpAdditionalAssignInfo.allowanceInfo.name+", curId: "+currentId);

                $scope.additionalAssignValidationChecking = true;

                PrlEmpAdditionalAssignInfoUniquenessCheck.get({curid:currentId,
                    empid: $scope.prlEmpAdditionalAssignInfo.employeeInfo.id,
                    allowid: $scope.prlEmpAdditionalAssignInfo.allowanceInfo.id}, function(result)
                {
                    console.log(JSON.stringify(result));
                    $scope.isSaving = !result.isValid;
                    $scope.additionalAssignValidationChecking = false;
                    if(result.isValid)
                    {
                        console.log("valid");
                        $scope.additionalAssignAlreadyExist = false;
                    }
                    else
                    {
                        console.log("not valid");
                        $scope.additionalAssignAlreadyExist = true;
                    }
                },function(response)
                {
                    console.log("data connection failed");
                    $scope.additionalAssignValidationChecking = false;
                });
            }
        };

        $scope.$watch('prlEmpAdditionalAssignInfo.employeeInfo', function()
        {
            if($scope.prlEmpAdditionalAssignInfo.employeeInfo!=null && $scope.prlEmpAdditionalAssignInfo.employeeInfo.id!=null)
            {
                $scope.checkAdditionAssignByEmployeeAndAllowance();
            }
        });

        $scope.clear = function() {

        };
}]);
