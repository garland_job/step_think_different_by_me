'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('prlEmpAdditionalAssignInfo', {
                parent: 'payroll',
                url: '/prlEmpAdditionalAssignInfos',
                data: {
                    authorities: ['ROLE_USER','ROLE_ORG_USER','ROLE_HRM_USER','ROLE_HRM_ADMIN','ROLE_ADMIN'],
                    pageTitle: 'stepApp.prlEmpAdditionalAssignInfo.home.title'
                },
                views: {
                    'payrollManagementView@payroll': {
                        templateUrl: 'scripts/app/entities/payroll/empAdditionalAssignInfo/prlEmpAdditionalAssignInfos.html',
                        controller: 'PrlEmpAdditionalAssignInfoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('prlEmpAdditionalAssignInfo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('prlEmpAdditionalAssignInfo.detail', {
                parent: 'prlEmpAdditionalAssignInfo',
                url: '/prlEmpAdditionalAssignInfo/{id}',
                data: {
                    authorities: ['ROLE_USER','ROLE_ORG_USER','ROLE_HRM_USER'],
                    pageTitle: 'stepApp.prlEmpAdditionalAssignInfo.detail.title'
                },
                views: {
                    'payrollManagementView@payroll': {
                        templateUrl: 'scripts/app/entities/payroll/empAdditionalAssignInfo/prlEmpAdditionalAssignInfo-detail.html',
                        controller: 'PrlEmpAdditionalAssignInfoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('prlEmpAdditionalAssignInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PrlEmpAdditionalAssignInfo', function($stateParams, PrlEmpAdditionalAssignInfo) {
                        return PrlEmpAdditionalAssignInfo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('prlEmpAdditionalAssignInfo.new', {
                parent: 'prlEmpAdditionalAssignInfo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER','ROLE_ORG_USER','ROLE_HRM_USER'],
                },
                views: {
                    'payrollManagementView@payroll': {
                        templateUrl: 'scripts/app/entities/payroll/empAdditionalAssignInfo/prlEmpAdditionalAssignInfo-dialog.html',
                        controller: 'PrlEmpAdditionalAssignInfoDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('prlEmpAdditionalAssignInfo');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            effectedDateFrom: null,
                            effectedDateTo: null,
                            allowanceAmount: null,
                            activeStatus: false,
                            createDate: null,
                            createBy: null,
                            updateDate: null,
                            updateBy: null,
                            id: null
                        };
                    }
                }
            })
            .state('prlEmpAdditionalAssignInfo.edit', {
                parent: 'prlEmpAdditionalAssignInfo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER','ROLE_ORG_USER','ROLE_HRM_USER'],
                },
                views: {
                    'payrollManagementView@payroll': {
                        templateUrl: 'scripts/app/entities/payroll/empAdditionalAssignInfo/prlEmpAdditionalAssignInfo-dialog.html',
                        controller: 'PrlEmpAdditionalAssignInfoDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('prlEmpAdditionalAssignInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PrlEmpAdditionalAssignInfo', function($stateParams, PrlEmpAdditionalAssignInfo) {
                        return PrlEmpAdditionalAssignInfo.get({id : $stateParams.id});
                    }]
                }
            });
    });
