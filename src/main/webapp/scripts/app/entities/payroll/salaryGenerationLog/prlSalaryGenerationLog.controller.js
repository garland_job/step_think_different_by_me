'use strict';

angular.module('stepApp')
    .controller('PrlSalaryGenerationLogController', function ($scope, $state, PrlSalaryGenerationLog, PrlSalaryGenerationLogSearch, ParseLinks) {

        $scope.prlSalaryGenerationLogs = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            PrlSalaryGenerationLog.query({page: $scope.page - 1, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlSalaryGenerationLogs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlSalaryGenerationLogSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlSalaryGenerationLogs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlSalaryGenerationLog = {
                generateDate: null,
                comments: null,
                actionStatus: null,
                generateBy: null,
                createDate: null,
                createBy: null,
                id: null
            };
        };
    });
/*prlSalaryGenerationLog-dialog.controller.js*/

angular.module('stepApp').controller('PrlSalaryGenerationLogDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlSalaryGenerationLog', 'PrlGeneratedSalaryInfo','User','Principal','DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, PrlSalaryGenerationLog, PrlGeneratedSalaryInfo, User, Principal, DateUtils) {

            $scope.prlSalaryGenerationLog = entity;
            $scope.prlgeneratedsalaryinfos = PrlGeneratedSalaryInfo.query();
            $scope.load = function(id) {
                PrlSalaryGenerationLog.get({id : id}, function(result) {
                    $scope.prlSalaryGenerationLog = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlSalaryGenerationLogUpdate', result);
                $scope.isSaving = false;
                $state.go('prlSalaryGenerationLog');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.prlSalaryGenerationLog.generateBy = $scope.loggedInUser.login;
                $scope.prlSalaryGenerationLog.generateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.prlSalaryGenerationLog.id != null) {
                    PrlSalaryGenerationLog.update($scope.prlSalaryGenerationLog, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlSalaryGenerationLog.updated');
                } else {
                    $scope.prlSalaryGenerationLog.createBy = $scope.loggedInUser.id;
                    $scope.prlSalaryGenerationLog.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlSalaryGenerationLog.save($scope.prlSalaryGenerationLog, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlSalaryGenerationLog.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*prlSalaryGenerationLog-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlSalaryGenerationLogDetailController', function ($scope, $rootScope, $stateParams, entity, PrlSalaryGenerationLog, PrlGeneratedSalaryInfo) {
        $scope.prlSalaryGenerationLog = entity;
        $scope.load = function (id) {
            PrlSalaryGenerationLog.get({id: id}, function(result) {
                $scope.prlSalaryGenerationLog = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlSalaryGenerationLogUpdate', function(event, result) {
            $scope.prlSalaryGenerationLog = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlSalaryGenerationLog-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlSalaryGenerationLogDeleteController', function($scope, $rootScope, $modalInstance, entity, PrlSalaryGenerationLog) {

        $scope.prlSalaryGenerationLog = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlSalaryGenerationLog.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlSalaryGenerationLog.deleted');
                });
        };

    });
