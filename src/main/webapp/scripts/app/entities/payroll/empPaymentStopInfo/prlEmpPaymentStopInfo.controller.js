'use strict';

angular.module('stepApp')
    .controller('PrlEmpPaymentStopInfoController', function ($scope, $state, PrlEmpPaymentStopInfo, PrlEmpPaymentStopInfoSearch, ParseLinks) {

        $scope.prlEmpPaymentStopInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            PrlEmpPaymentStopInfo.query({page: $scope.page - 1, size: 2000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlEmpPaymentStopInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlEmpPaymentStopInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlEmpPaymentStopInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlEmpPaymentStopInfo = {
                effectedDateFrom: null,
                effectedDateTo: null,
                stopActionType: null,
                activeStatus: true,
                comments: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlEmpPaymentStopInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlEmpPaymentStopInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlEmpPaymentStopInfo', 'HrEmployeeInfo','User','Principal','DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, PrlEmpPaymentStopInfo, HrEmployeeInfo, User, Principal, DateUtils) {

            $scope.prlEmpPaymentStopInfo = entity;
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                PrlEmpPaymentStopInfo.get({id : id}, function(result) {
                    $scope.prlEmpPaymentStopInfo = result;
                });
            };

            $scope.hrEmployee = function (value,servicio) {
                $scope.prlEmpPaymentStopInfo.employeeInfo = value.originalObject;
            }

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlEmpPaymentStopInfoUpdate', result);
                $state.go('prlEmpPaymentStopInfo');
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                //console.log(JSON.stringify($scope.prlEmpPaymentStopInfo));
                $scope.isSaving = true;
                $scope.prlEmpPaymentStopInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlEmpPaymentStopInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.prlEmpPaymentStopInfo.id != null) {
                    PrlEmpPaymentStopInfo.update($scope.prlEmpPaymentStopInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlEmpPaymentStopInfo.updated');
                } else {
                    $scope.prlEmpPaymentStopInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlEmpPaymentStopInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlEmpPaymentStopInfo.save($scope.prlEmpPaymentStopInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlEmpPaymentStopInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*prlEmpPaymentStopInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlEmpPaymentStopInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlEmpPaymentStopInfo, HrEmployeeInfo) {
        $scope.prlEmpPaymentStopInfo = entity;
        $scope.load = function (id) {
            PrlEmpPaymentStopInfo.get({id: id}, function(result) {
                $scope.prlEmpPaymentStopInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlEmpPaymentStopInfoUpdate', function(event, result) {
            $scope.prlEmpPaymentStopInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlEmpPaymentStopInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlEmpPaymentStopInfoDeleteController', function($scope, $modalInstance, entity, PrlEmpPaymentStopInfo) {

        $scope.prlEmpPaymentStopInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlEmpPaymentStopInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlEmpPaymentStopInfo.deleted');
                });
        };

    });
