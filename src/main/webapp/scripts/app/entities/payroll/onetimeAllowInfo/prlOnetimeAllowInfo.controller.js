'use strict';

angular.module('stepApp')
    .controller('PrlOnetimeAllowInfoController', function ($rootScope, $scope, $state, PrlOnetimeAllowInfo, PrlOnetimeAllowInfoSearch, ParseLinks) {

        $scope.prlOnetimeAllowInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.stateName = "prlLocalityInfo";
        $scope.page = 0;
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            PrlOnetimeAllowInfo.query({page: $scope.page, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlOnetimeAllowInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlOnetimeAllowInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlOnetimeAllowInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlOnetimeAllowInfo = {
                name: null,
                year: null,
                basicAmountPercentage: null,
                effectiveDate: null,
                religion: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlOnetimeAllowInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlOnetimeAllowInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PrlOnetimeAllowInfo','User','Principal','DateUtils','PrlAllowDeductInfoByType',
        function($scope, $rootScope, $stateParams, $state, entity, PrlOnetimeAllowInfo, User, Principal, DateUtils,PrlAllowDeductInfoByType) {

            $scope.prlOnetimeAllowInfo = entity;
            $scope.prlAllowanceList = PrlAllowDeductInfoByType.query({type:'OnetimeAllowance'});
            $scope.load = function(id) {
                PrlOnetimeAllowInfo.get({id : id}, function(result) {
                    $scope.prlOnetimeAllowInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlOnetimeAllowInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('prlOnetimeAllowInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.prlOnetimeAllowInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlOnetimeAllowInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.prlOnetimeAllowInfo.id != null)
                {
                    PrlOnetimeAllowInfo.update($scope.prlOnetimeAllowInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlOnetimeAllowInfo.updated');
                } else
                {
                    $scope.prlOnetimeAllowInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlOnetimeAllowInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlOnetimeAllowInfo.save($scope.prlOnetimeAllowInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlOnetimeAllowInfo.created');
                }
            };

            $scope.clear = function() {
            };
        }]);
/*prlOnetimeAllowInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlOnetimeAllowInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlOnetimeAllowInfo) {
        $scope.prlOnetimeAllowInfo = entity;
        $scope.load = function (id) {
            PrlOnetimeAllowInfo.get({id: id}, function(result) {
                $scope.prlOnetimeAllowInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlOnetimeAllowInfoUpdate', function(event, result) {
            $scope.prlOnetimeAllowInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlOnetimeAllowInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlOnetimeAllowInfoDeleteController', function($scope, $rootScope, $modalInstance, entity, PrlOnetimeAllowInfo) {

        $scope.prlOnetimeAllowInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlOnetimeAllowInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlOnetimeAllowInfo.deleted');
                });
        };

    });
