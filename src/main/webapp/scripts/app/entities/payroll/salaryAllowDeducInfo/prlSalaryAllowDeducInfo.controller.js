'use strict';

angular.module('stepApp')
    .controller('PrlSalaryAllowDeducInfoController', function ($scope, $state, PrlSalaryAllowDeducInfo, PrlSalaryAllowDeducInfoSearch, ParseLinks) {

        $scope.prlSalaryAllowDeducInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            PrlSalaryAllowDeducInfo.query({page: $scope.page - 1, size: 2000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlSalaryAllowDeducInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlSalaryAllowDeducInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlSalaryAllowDeducInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlSalaryAllowDeducInfo = {
                allowDeducType: null,
                allowDeducValue: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlSalaryAllowDeducInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlSalaryAllowDeducInfoDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PrlSalaryAllowDeducInfo', 'PrlSalaryStructureInfo', 'PrlAllowDeductInfo','User','Principal','DateUtils',
        function($scope, $stateParams, $state, entity, PrlSalaryAllowDeducInfo, PrlSalaryStructureInfo, PrlAllowDeductInfo, User, Principal, DateUtils) {

            $scope.prlSalaryAllowDeducInfo = entity;
            $scope.prlsalarystructureinfos = PrlSalaryStructureInfo.query();
            $scope.prlallowdeductinfos = PrlAllowDeductInfo.query();
            $scope.load = function(id) {
                PrlSalaryAllowDeducInfo.get({id : id}, function(result) {
                    $scope.prlSalaryAllowDeducInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlSalaryAllowDeducInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('prlSalaryAllowDeducInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.prlSalaryAllowDeducInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlSalaryAllowDeducInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.prlSalaryAllowDeducInfo.id != null) {
                    PrlSalaryAllowDeducInfo.update($scope.prlSalaryAllowDeducInfo, onSaveSuccess, onSaveError);
                } else {
                    $scope.prlSalaryAllowDeducInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlSalaryAllowDeducInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlSalaryAllowDeducInfo.save($scope.prlSalaryAllowDeducInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
            };
        }]);
/*prlSalaryAllowDeducInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlSalaryAllowDeducInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlSalaryAllowDeducInfo, PrlSalaryStructureInfo, PrlAllowDeductInfo) {
        $scope.prlSalaryAllowDeducInfo = entity;
        $scope.load = function (id) {
            PrlSalaryAllowDeducInfo.get({id: id}, function(result) {
                $scope.prlSalaryAllowDeducInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlSalaryAllowDeducInfoUpdate', function(event, result) {
            $scope.prlSalaryAllowDeducInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlSalaryAllowDeducInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlSalaryAllowDeducInfoDeleteController', function($scope, $modalInstance, entity, PrlSalaryAllowDeducInfo) {

        $scope.prlSalaryAllowDeducInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlSalaryAllowDeducInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
