'use strict';

angular.module('stepApp')
    .controller('PrlPayscaleAllowanceInfoController', function ($scope, $state, PrlPayscaleAllowanceInfo, PrlPayscaleAllowanceInfoSearch, ParseLinks) {

        $scope.prlPayscaleAllowanceInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.stateName = "prlPayscaleInfo";
        $scope.page = 1;
        $scope.loadAll = function() {
            PrlPayscaleAllowanceInfo.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlPayscaleAllowanceInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlPayscaleAllowanceInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlPayscaleAllowanceInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlPayscaleAllowanceInfo = {
                fixedBasic: null,
                basicMinimum: null,
                basicMaximum: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlPayscaleAllowanceInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlPayscaleAllowanceInfoDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PrlPayscaleAllowanceInfo', 'PrlPayscaleInfo', 'PrlAllowDeductInfo',
        function($scope, $stateParams, $state, entity, PrlPayscaleAllowanceInfo, PrlPayscaleInfo, PrlAllowDeductInfo) {

            $scope.prlPayscaleAllowanceInfo = entity;
            $scope.prlpayscaleinfos = PrlPayscaleInfo.query();
            $scope.prlallowdeductinfos = PrlAllowDeductInfo.query();
            $scope.load = function(id) {
                PrlPayscaleAllowanceInfo.get({id : id}, function(result) {
                    $scope.prlPayscaleAllowanceInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlPayscaleAllowanceInfoUpdate', result);
                $uibModalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.prlPayscaleAllowanceInfo.id != null) {
                    PrlPayscaleAllowanceInfo.update($scope.prlPayscaleAllowanceInfo, onSaveSuccess, onSaveError);
                } else {
                    PrlPayscaleAllowanceInfo.save($scope.prlPayscaleAllowanceInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $uibModalInstance.dismiss('cancel');
            };
        }]);
/*prlPayscaleAllowanceInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlPayscaleAllowanceInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlPayscaleAllowanceInfo, PrlPayscaleInfo, PrlAllowDeductInfo) {
        $scope.prlPayscaleAllowanceInfo = entity;
        $scope.load = function (id) {
            PrlPayscaleAllowanceInfo.get({id: id}, function(result) {
                $scope.prlPayscaleAllowanceInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlPayscaleAllowanceInfoUpdate', function(event, result) {
            $scope.prlPayscaleAllowanceInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlPayscaleAllowanceInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlPayscaleAllowanceInfoDeleteController', function($scope, $modalInstance, entity, PrlPayscaleAllowanceInfo) {

        $scope.prlPayscaleAllowanceInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlPayscaleAllowanceInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
