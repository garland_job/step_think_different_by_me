'use strict';

angular.module('stepApp')
    .controller('PrlLocalityInfoController',
    function ($rootScope, $scope, $state, PrlLocalityInfo, PrlLocalityInfoSearch, ParseLinks) {

        $scope.prlLocalityInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "prlLocalityInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }

            PrlLocalityInfo.query({page: $scope.page, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.prlLocalityInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PrlLocalityInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prlLocalityInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.prlLocalityInfo = {
                name: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*prlLocalityInfo-dialog.controller.js*/

angular.module('stepApp').controller('PrlLocalityInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state','entity', 'PrlLocalityInfo', 'District','User','Principal','DateUtils','PrlLocalityInfoUniqueness',
        function($scope, $rootScope, $stateParams, $state, entity, PrlLocalityInfo, District, User, Principal, DateUtils,PrlLocalityInfoUniqueness) {

            $scope.prlLocalityInfo = entity;
            $scope.districts = District.query({size:500});
            $scope.load = function(id) {
                PrlLocalityInfo.get({id : id}, function(result) {
                    $scope.prlLocalityInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:prlLocalityInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('prlLocalityInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.localityAlreadyExist = false;
            $scope.checkLocalityUniqByNameAndDistrict = function()
            {
                console.log("DistId: "+$scope.prlLocalityInfo.district.id+", name: "+$scope.prlLocalityInfo.name)
                if($scope.prlLocalityInfo.district.id !=null && $scope.prlLocalityInfo.name != null && $scope.prlLocalityInfo.name.length > 0)
                {
                    $scope.editForm.name.$pending = true;
                    PrlLocalityInfoUniqueness.get({distid:$scope.prlLocalityInfo.district.id, name: $scope.prlLocalityInfo.name}, function(result)
                    {
                        //console.log(JSON.stringify(result));
                        $scope.isSaving = !result.isValid;
                        $scope.editForm.name.$pending = false;
                        if(result.isValid)
                        {
                            console.log("valid");
                            $scope.localityAlreadyExist = false;
                        }
                        else
                        {
                            console.log("not valid");
                            $scope.localityAlreadyExist = true;
                        }
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.name.$pending = false;
                    });
                }
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.prlLocalityInfo.updateBy = $scope.loggedInUser.id;
                $scope.prlLocalityInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                //console.log(JSON.stringify($scope.prlLocalityInfo));
                if ($scope.prlLocalityInfo.id != null)
                {
                    PrlLocalityInfo.update($scope.prlLocalityInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.prlLocalityInfo.updated');
                }
                else
                {
                    $scope.prlLocalityInfo.createBy = $scope.loggedInUser.id;
                    $scope.prlLocalityInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PrlLocalityInfo.save($scope.prlLocalityInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.prlLocalityInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*prlLocalityInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PrlLocalityInfoDetailController', function ($scope, $rootScope, $stateParams, entity, PrlLocalityInfo, District) {
        $scope.prlLocalityInfo = entity;
        $scope.load = function (id) {
            PrlLocalityInfo.get({id: id}, function(result) {
                $scope.prlLocalityInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:prlLocalityInfoUpdate', function(event, result) {
            $scope.prlLocalityInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*prlLocalityInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('PrlLocalityInfoDeleteController', function($scope, $rootScope, $modalInstance, entity, PrlLocalityInfo) {

        $scope.prlLocalityInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            PrlLocalityInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.prlLocalityInfo.deleted');
                });
        };

    });
