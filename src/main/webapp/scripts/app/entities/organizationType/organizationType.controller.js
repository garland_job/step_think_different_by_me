'use strict';

angular.module('stepApp')
    .controller('OrganizationTypeController',

    ['$scope','$state','$modal','OrganizationType','OrganizationTypeSearch','ParseLinks',
    function ($scope, $state, $modal, OrganizationType, OrganizationTypeSearch, ParseLinks) {

        $scope.organizationTypes = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            OrganizationType.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.organizationTypes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            OrganizationTypeSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.organizationTypes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.organizationType = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);
/*organizationType-dialog.controller.js*/

angular.module('stepApp').controller('OrganizationTypeDialogController',
    ['$scope', '$rootScope', '$stateParams','$state', 'entity', 'OrganizationType',
        function($scope, $rootScope, $stateParams,$state, entity, OrganizationType) {

            $scope.organizationType = {};
            $scope.organizationType.status = true;
            /*$scope.load = function(id) {

             };*/

            OrganizationType.get({id : $stateParams.id}, function(result) {
                $scope.organizationType = result;
            });

            var onSaveSuccess = function (result) {
                //$scope.$emit('stepApp:organizationTypeUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('organizationType', null, { reload: true });
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.organizationType.id != null) {
                    OrganizationType.update($scope.organizationType, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('Organization Type has been successfully updated');
                } else {
                    OrganizationType.save($scope.organizationType, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('Organization Type has been successfully saved');
                }
            };

            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
            };
        }]);
/*organizationType-detail.controller.js*/

angular.module('stepApp')
    .controller('OrganizationTypeDetailController',
        ['$scope','$rootScope','$stateParams','entity','OrganizationType',
            function ($scope, $rootScope, $stateParams, entity, OrganizationType) {
                $scope.organizationType = entity;
                $scope.load = function (id) {
                    OrganizationType.get({id: id}, function(result) {
                        $scope.organizationType = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:organizationTypeUpdate', function(event, result) {
                    $scope.organizationType = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*organizationType-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('OrganizationTypeDeleteController',
        ['$scope','$rootScope','$modalInstance','entity','OrganizationType',
            function($scope, $rootScope, $modalInstance, entity, OrganizationType) {

                $scope.organizationType = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    OrganizationType.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('Organization Type has been successfully deleted');
                        });
                };

            }]);
