'use strict';

angular.module('stepApp').controller('FeePaymentTypeSetupDialogController',
    ['$scope', '$state', '$stateParams', 'entity', 'FeePaymentTypeSetup',
        function($scope, $state, $stateParams, entity, FeePaymentTypeSetup) {

        $scope.feePaymentTypeSetup = entity;
        $scope.load = function(id) {
            FeePaymentTypeSetup.get({id : id}, function(result) {
                $scope.feePaymentTypeSetup = result;
            });
        };

        var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:feePaymentTypeSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('feePaymentTypeSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.feePaymentTypeSetup.id != null) {
                FeePaymentTypeSetup.update($scope.feePaymentTypeSetup, onSaveSuccess);
            } else {
                FeePaymentTypeSetup.save($scope.feePaymentTypeSetup, onSaveSuccess);
            }
        };

        $scope.clear = function() {
            $scope.isSaving = false;
            $state.go('feePaymentTypeSetup');
        };
}]);
