'use strict';

angular.module('stepApp')
    .controller('FeePaymentTypeSetupController', function ($scope, $state, FeePaymentTypeSetup, FeePaymentTypeSetupSearch, ParseLinks) {
        $scope.feePaymentTypeSetup = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        console.log('hi');
        $scope.loadAll = function() {
            /*FeePaymentTypeSetup.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.feePaymentTypeSetups = result;
            });*/

            FeePaymentTypeSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.feePaymentTypeSetup = result;
                console.log($scope.feePaymentTypeSetup);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        /*$scope.delete = function (id) {
            FeePaymentTypeSetup.get({id: id}, function(result) {
                $scope.feePaymentTypeSetup = result;
                $('#deleteFeePaymentTypeSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            FeePaymentTypeSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteFeePaymentTypeSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };*/

        $scope.search = function () {
            FeePaymentTypeSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.feePaymentTypeSetup = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.feePaymentTypeSetup = {
                paymentTypeId: null,
                name: null,
                status: null,
                description: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    });
