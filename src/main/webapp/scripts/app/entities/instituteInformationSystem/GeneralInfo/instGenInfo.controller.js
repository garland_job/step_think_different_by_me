'use strict';

angular.module('stepApp')
    .controller('InstGenInfoController',
        ['$scope', '$state', 'User', '$modal', 'InstGenInfo', 'InstGenInfoSearch', 'Principal', 'ParseLinks',
            function ($scope, $state, User, $modal, InstGenInfo, InstGenInfoSearch, Principal, ParseLinks) {

                Principal.identity().then(function (account) {
                    $scope.account = account;
                });

                $scope.instGenInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstGenInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));

                        angular.forEach(result, function (genInfo) {
                            if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                                genInfo.edit = false;
                                genInfo.approved = true;
                                genInfo.reject = true;
                                if (genInfo.status != 1) {
                                    $scope.instGenInfos.push(genInfo);
                                }
                            } else if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                                genInfo.edit = true;
                                genInfo.approved = false;
                                genInfo.reject = false;
                                $scope.instGenInfos.push(genInfo);
                            } else {
                                genInfo.edit = false;
                                genInfo.approved = false;
                                genInfo.reject = false;
                                $scope.instGenInfos.push(genInfo);
                            }

                        });
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstGenInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instGenInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instGenInfo = {
                        code: null,
                        name: null,
                        publicationDate: null,
                        type: null,
                        village: null,
                        postOffice: null,
                        postCode: null,
                        landPhone: null,
                        mobileNo: null,
                        email: null,
                        consArea: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*instGenInfo-dialog.controller.js*/

angular.module('stepApp').controller('InstGenInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'Division', '$filter', 'entity', '$q', 'District', 'InstGenInfo', 'Institute', 'InstGenInfosLocalitys', 'Upazila', 'InstGenInfoByStatus', 'CheckLogin', 'CheckEmail', 'InstCategory', 'InstLevel', 'InstCategoryActive', 'InstLevelByCategory', 'InstCategoryByStatusAndType','DateUtils',
        function ($scope, $rootScope, $stateParams, $state, Division, $filter, entity, $q, District, InstGenInfo, Institute, InstGenInfosLocalitys, Upazila, InstGenInfoByStatus, CheckLogin, CheckEmail, InstCategory, InstLevel, InstCategoryActive, InstLevelByCategory, InstCategoryByStatusAndType,DateUtils) {
            $scope.instGenInfo = entity;

            $scope.instLevels = [];
            $scope.loadInstLevel = function (instCategory) {
                $scope.instLevels = InstLevelByCategory.query({catId: instCategory.id});
            };
            $scope.divisions = Division.query();
            $scope.mpoEnlistedDisable = true;
            $scope.mpoRelatedFieldDisable = true;
            $scope.mpoCodeError = false;
            $scope.allDistrict = [];
            $scope.allUpazila = [];
            $scope.instLevels = [];
            $scope.checkActivity = false;
            $scope.dtmax = new Date();

            if ($stateParams.id != null) {
                $scope.fullColumn = true;
                InstGenInfo.get({id : $stateParams.id},function(result){
                    $scope.instGenInfo= result;
                    $scope.instGenInfo.district = result.upazila.district;
                    $scope.instGenInfo.division = result.upazila.district.division;
                });
            } else {
                $scope.fullColumn = false;
            }

            if ($scope.allDistrict.length == 0) {
                District.query({page: $scope.page, size: 100}, function (result, headers) {
                    $scope.allDistrict = result;
                });
            }
            if ($scope.allUpazila.length == 0) {
                Upazila.query({page: $scope.page, size: 2000}, function (result, headers) {
                    $scope.allUpazila = result;
                });
            }
            $scope.updatedDistrict = function (select) {
                if ($scope.allDistrict.length == 0) {
                    District.query({page: $scope.page, size: 100}, function (result, headers) {
                        $scope.allDistrict = result;
                        $scope.districts = [];
                        angular.forEach($scope.allDistrict, function (district) {
                            if (select != undefined && select.id == district.division.id) {
                                $scope.districts.push(district);
                            }
                        });
                    });
                } else {

                    $scope.districts = [];
                    angular.forEach($scope.allDistrict, function (district) {
                        if (select != undefined && select.id == district.division.id) {
                            $scope.districts.push(district);
                        }
                    });
                }
            };
            $scope.updatedUpazila = function (select) {
                if ($scope.allUpazila.length == 0) {
                    Upazila.query({page: $scope.page, size: 5000}, function (result, headers) {
                        $scope.allUpazila = result;
                        $scope.upazilas = [];
                        angular.forEach($scope.allUpazila, function (upazila) {
                            if (select != undefined && select.id == upazila.district.id) {
                                $scope.upazilas.push(upazila);
                            }
                        });
                    });
                } else {
                    $scope.upazilas = [];
                    angular.forEach($scope.allUpazila, function (upazila) {
                        if (select != undefined && select.id == upazila.district.id) {
                            $scope.upazilas.push(upazila);
                        }
                    });
                }
            };

            $scope.retriveGenInfo = function () {
                if ($scope.instGenInfo == null) {
                    $scope.instGenInfo = entity;
                }
                else {
                    $scope.updatedDistrict($scope.instGenInfo.upazila.district.division);
                    $scope.updatedUpazila($scope.instGenInfo.upazila.district);
                    if ($scope.instGenInfo.type != "NonGovernment") {
                        $scope.mpoEnlistedDisable = true;
                        $scope.mpoRelatedFieldDisable = true;
                        $scope.instGenInfo.dateOfMpo = null;
                    }
                    else {
                        $scope.mpoEnlistedDisable = false;
                        $scope.mpoRelatedFieldDisable = true;
                    }
                }
            };
            $q.all([entity.$promise]).then(function () {
                $scope.instGenInfo = entity;
                $scope.instGenInfo.instLevel = $scope.instGenInfo.instLevel;
                $scope.instGenInfoType($scope.instGenInfo.type);
                $scope.loadInstLevel($scope.instGenInfo.instCategory);
                $scope.retriveGenInfo();

                if (entity.upazila) {
                    if (entity.upazila.district) {
                        if (entity.upazila.district.division) {
                            $scope.instGenInfo.division = entity.upazila.district.division;
                            $scope.instGenInfo.district = entity.upazila.district;
                            $scope.instGenInfo.upazila = entity.upazila;
                        }
                        else {
                            $scope.division = "Select Division"
                        }
                    }
                    else {
                        $scope.district = "Select District";
                        $scope.division = "Select Division"
                    }
                }
                else {
                    $scope.division = "Select Division"
                    $scope.district = "Select District";
                    $scope.upazila = "Select Upazilla";
                }
            });

            $scope.click = function (data) {
                if (data) {
                    $scope.instGenInfo.lastExpireDateOfAffiliation = null;
                    $scope.instGenInfo.dateOfMpo = null;
                    $scope.mpoRelatedFieldDisable = false;
                    $scope.mpoCodeError = true;
                    $scope.validateMpoCode();
                    if ($('.req-start').html().indexOf('*') == -1)
                        $('.req-start').append('<strong style="color:red"> * </strong>');
                } else {
                    $scope.instGenInfo.lastExpireDateOfAffiliation = null;
                    $scope.instGenInfo.dateOfMpo = null;
                    $scope.mpoRelatedFieldDisable = true;
                    $scope.mpoCodeError = false;
                }
            }

            $scope.validateMpoCode = function () {
                if ($scope.instGenInfo.mpoCode && $scope.instGenInfo.mpoCode.length) {
                    $scope.mpoCodeError = false;
                }
                else {
                    $scope.mpoCodeError = true;
                }
            }

            InstGenInfosLocalitys.get({}, function (result) {

                $scope.instGenInfolocalitys = result;
                angular.forEach(result, function (data) {
                });
            });

            $scope.deadlineValidation = function () {
                var d1 = Date.parse($scope.instGenInfo.firstAffiliationDate);
                var d2 = Date.parse($scope.instGenInfo.publicationDate);
                if (d1 < d2) {
                    $scope.dateError = true;
                } else {
                    $scope.dateError = false;
                }
            };


            $scope.instGenInfoType = function (data) {
                InstCategoryByStatusAndType.query({activeStatus: 1, instType: data}, function (result) {
                    $scope.instCategories = result;
                });

                if (data != "NonGovernment") {
                    $scope.mpoEnlistedDisable = true;
                    $scope.mpoRelatedFieldDisable = true;
                    if ($scope.instGenInfo.id == null) {
                        $scope.instGenInfo.mpoEnlisted = false;
                    }
                    $scope.instGenInfo.firstRecognitionDate = null;
                    $scope.instGenInfo.firstAffiliationDate = null;
                    $scope.instGenInfo.lastExpireDateOfAffiliation = null;
                    $scope.instGenInfo.dateOfMpo = null;
                }
                else {
                    $scope.mpoEnlistedDisable = false;
                    $scope.mpoRelatedFieldDisable = true;
                    $scope.instGenInfo.mpoEnlisted = false;
                    if ($scope.instGenInfo.mpoEnlisted == true) {
                        $scope.mpoCodeError = true;
                    }
                    else {
                        $scope.mpoCodeError = false;
                    }
                }
            }

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instGenInfoUpdate', result);
                $scope.isSaving = false;
                if ($scope.checkActivity)
                    $state.go('instituteInfo.generalInfo', {}, {reload: true});
                else {
                    $scope.instGenInfo = entity;
                    $state.go('instituteInfo.confirmationMsg');
                }
            };

            var onSaveError = function (result) {
                $rootScope.setErrorMessage('There is an error in submitting application');
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = false;
                if ($scope.instGenInfo.id != null && $scope.preCondition()) {
                    console.log('For Save');
                    $scope.instGenInfo.status = 2;
                    $scope.checkActivity = true;
                    InstGenInfo.update($scope.instGenInfo, onSaveSuccess, onSaveError);
                } else if($scope.instGenInfo.id == null && $scope.preCondition()) {
                    console.log('For Update');
                    $scope.instGenInfo.status = 0;
                    InstGenInfo.save($scope.instGenInfo, onSaveSuccess, onSaveError);
                }else {
                    $rootScope.setErrorMessage('There is an error in submitting application');
                    $scope.isSaving = false;
                    console.log('For Failed');
                }

            };
            $scope.clear = function () {
                $state.go('instituteInfo.generalInfo', {}, {reload: true});
                //$scope.instGenInfo = entity;
                //localStorage.removeItem('instGenInfo');
            };
            $scope.doArchive = function () {
                localStorage.removeItem('instGenInfo');
                localStorage.setItem('instGenInfo', JSON.stringify($scope.instGenInfo));
                $state.go('instituteInfo.generalInfo.view');
            };
            $scope.previewInfo = false;
            $scope.showPreview = function (value) {
                $scope.previewInfo = value;
            }
            $scope.editForm = {};


            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.code', $scope.instGenInfo.code, 'number', 10, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.eiin', $scope.instGenInfo.eiin, 'number', 10, 4, '0to9', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.centerCode', $scope.instGenInfo.centerCode, 'number', 10, 4, 'number', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.name', $scope.instGenInfo.name, 'text', 100, 5, 'atozAndAtoZ().-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.mobileNo', $scope.instGenInfo.mobileNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.altMobileNo', $scope.instGenInfo.altMobileNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.postCode', $scope.instGenInfo.postCode, 'number', 4, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.landPhone', $scope.instGenInfo.landPhone, 'number', 20, 5, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.faxNum', $scope.instGenInfo.faxNum, 'number', 20, 5, '0to9-', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.email', $scope.instGenInfo.email, 'email', 60, 10, 'email', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.publicationDate',DateUtils.convertLocaleDateToDMY($scope.instGenInfo.publicationDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGenInfo.firstAffiliationDate', DateUtils.convertLocaleDateToDMY($scope.instGenInfo.firstAffiliationDate), 'date', 5, 60, 'date', true, '',''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;

            }

        }]);

/*instGenInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('InstGenInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstGenInfo', 'Institute', 'Upazila',
            function ($scope, $rootScope, $stateParams, entity, InstGenInfo, Institute, Upazila) {

                InstGenInfo.get({id: 0}, function (result) {
                    $scope.instGenInfo = result;
                    if ($scope.instGenInfo.mpoEnlisted == true) {
                        $scope.mpoListed = 'Yes';
                    }
                    else {
                        $scope.mpoListed = 'No';
                    }
                    if ($scope.instGenInfo.type == 'Government') {
                        $scope.instGenInfo.mpoEnlisted = false;
                        $scope.hidempoEnlisted = true;
                    }
                });

                var unsubscribe = $rootScope.$on('stepApp:instGenInfoUpdate', function (event, result) {
                    $scope.instGenInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*instGenInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstGenInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstGenInfo',
            function ($scope, $modalInstance, entity, InstGenInfo) {

                $scope.instGenInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstGenInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*instGenInfo-reject-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstGenInfoRejectController',
        ['$scope', '$modalInstance', 'Institute', '$state', 'entity', 'InstGenInfo', '$stateParams','RejectInstGenInfo',
            function ($scope, $modalInstance, Institute, $state, entity, InstGenInfo, $stateParams,RejectInstGenInfo) {
                //$scope.instGenInfo = entity;
                InstGenInfo.get({id: $stateParams.id}, function (result) {
                    $scope.instGenInfo = result;
                });

                var onSaveSuccess = function (result) {
                    $modalInstance.close(result);
                    $scope.isSaving = false;
                    $state.go('instituteInfo.pendingInstituteList', {}, {reload: true});
                    $rootScope.setSuccessMessage('Successfully Rejected.');
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                    $modalInstance.close(result);
                    $rootScope.setErrorMessage('Institute Reject have problem');
                };

                $scope.clear = function () {
                    $modalInstance.close();
                };
                //$scope.confirmReject = function () {
                //    $scope.isSaving = true;
                //    $scope.instGenInfo.status = -1;
                //    InstGenInfo.update($scope.instGenInfo, onSaveSuccess, onSaveError);
                //};

                $scope.confirmReject = function () {
                    $scope.instGenInfo.status = -1;
                    $scope.instGenInfo.id = $stateParams.id;
                    $scope.instGenInfo.rejectComments = $scope.causeDeny;
                    RejectInstGenInfo.update($scope.instGenInfo, onSaveSuccess, onSaveError);
                    $scope.isSaving = false;
                    $modalInstance.close();
                    $state.go('instituteInfo.pendingInstituteList', {}, {reload: true});

                };
            }]);

angular.module('stepApp')
    .controller('InstGenInfoDetailViewController',
        ['$scope', '$rootScope', '$stateParams', 'InstGenInfo', 'Institute', 'Upazila', '$state',
            function ($scope, $rootScope, $stateParams, InstGenInfo, Institute, Upazila, $state) {


                var unsubscribe = $rootScope.$on('stepApp:instGenInfoUpdate', function (event, result) {
                    $scope.instGenInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.retriveGenInfo = function () {
                    return JSON.parse(localStorage.getItem('instGenInfo'));
                };
                $scope.backButton = function () {
                    window.history.back();
                };
                $scope.removeGenInfo = function () {

                    $scope.instGenInfo = localStorage.getItem('instGenInfo');
                    JSON.parse($scope.instGenInfo);
                    if ($scope.instGenInfo == null) {
                        $scope.instGenInfo = entity;
                    }
                    else {
                        $scope.instGenInfo = JSON.parse($scope.instGenInfo);
                        localStorage.removeItem('instGenInfo');
                    }
                };
                var onSaveSuccess = function (result) {
                    $scope.$emit('stepApp:instGenInfoUpdate', result);
                    $scope.isSaving = false;
                    $scope.removeGenInfo();
                    if ($scope.checkActivity)
                        $state.go('instituteInfo.generalInfo', {}, {reload: true});
                    else {
                        $rootScope.setSuccessMessage("Institute Registration Complete with code: '" + result.code + "'. Please wait for admin approve")
                        //$rootScope.setErrorMessage("Institute Registration Complete with code: "+result.code+" Please wait for admin approve")
                        $state.go('instituteInfo');
                    }
                };
                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.save = function () {
                    $scope.isSaving = true;
                    InstGenInfo.save($scope.instGenInfo, onSaveSuccess, onSaveError);

                };


                $scope.instGenInfo = $scope.retriveGenInfo();

            }]);

/*instGenInfo-approve-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstGenInfoApproveController',
        ['$scope', '$modalInstance', 'Institute', '$state', 'entity', 'InstGenInfo', 'InstGenInfoAllApprove',
            function ($scope, $modalInstance, Institute, $state, entity, InstGenInfo, InstGenInfoAllApprove) {
                $scope.instGenInfo = entity;
                $scope.institute = {};
                var onSaveSuccess = function (result) {
                    $scope.$emit('stepApp:instituteUpdate', result);
                    $scope.instGenInfo.status = 1;
                    $scope.instGenInfo.id = $scope.holdId;
                    $scope.instGenInfo.institute = result;
                    InstGenInfo.update($scope.instGenInfo, onInstGenInfoSaveSuccess, onInstGenInfoSaveError);
                    $modalInstance.close(result);
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.confirmApprove = function () {
                    // $scope.instGenInfo.status = 1;
                    InstGenInfoAllApprove.update($scope.instGenInfo, onInstGenInfoSaveSuccess, onInstGenInfoSaveError);
                    $scope.isSaving = true;

                };
                var onInstGenInfoSaveSuccess = function (result) {
                    $modalInstance.close(result);
                    $scope.isInstGenInfoSaving = true;
                    $state.go('instituteInfo.pendingInstituteList', {}, {reload: true});

                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

            }])
    .controller('InstGenInfoDeclineController',
        ['$scope', '$stateParams', '$modalInstance', 'Institute', '$state', 'entity', 'InstGenInfo', 'RejectInstGenInfo',
            function ($scope, $stateParams, $modalInstance, Institute, $state, entity, InstGenInfo, RejectInstGenInfo) {
                $scope.instGenInfo = entity;

                $scope.decline = function () {
                    $scope.instGenInfo.status = -1;
                    $scope.instGenInfo.id = $stateParams.id;
                    $scope.instGenInfo.rejectComments = $scope.causeDeny;
                    RejectInstGenInfo.update($scope.instGenInfo, onInstGenInfoSaveSuccess, onInstGenInfoSaveError);
                    $scope.isSaving = false;
                    $modalInstance.close();
                    $state.go('instituteInfo.pendingInstituteList', {}, {reload: true});

                };

                var onInstGenInfoSaveSuccess = function (result) {


                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

                $scope.clear = function () {
                    $modalInstance.close();
                };
            }]);





