'use strict';

angular.module('stepApp')
    .controller('InstInfraInfoController',
        ['$scope', '$state', '$modal', 'InstInfraInfo', 'InstInfraInfoSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstInfraInfo, InstInfraInfoSearch, ParseLinks) {
                $scope.instInfraInfos = [];

                $scope.page = 0;
                $scope.loadAll = function () {
                    InstInfraInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instInfraInfos = result;
                        if ($scope.instInfraInfos.length == 0) {
                            $state.go('instGenInfo.instInfraInfo.new');
                        }
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstInfraInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instInfraInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instInfraInfo = {
                        status: null,
                        id: null
                    };
                };
            }]);

/*instInfraInfo-dialog.controller.js*/

angular.module('stepApp').controller('InstInfraInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', '$timeout', '$q', 'InstInfraInfo', 'Institute', 'InstBuilding', 'InstLand', 'entity', 'InstInfraBuildingDirections', 'AllInstInfraInfo', 'InstLabInfo', 'InstShopInfo', 'InstPlayGroundInfo', 'InstBuildingTemp', 'InstLandTemp', 'InstInfraInfoTemp', 'InstShopInfoTemp', 'InstLabInfoTemp', 'InstPlayGroundInfoTemp', 'InstInfraInfoTempAll','DateUtils',
        function ($scope, $rootScope, $stateParams, $state, $timeout, $q, InstInfraInfo, Institute, InstBuilding, InstLand, entity, InstInfraBuildingDirections, AllInstInfraInfo, InstLabInfo, InstShopInfo, InstPlayGroundInfo, InstBuildingTemp, InstLandTemp, InstInfraInfoTemp, InstShopInfoTemp, InstLabInfoTemp, InstPlayGroundInfoTemp, InstInfraInfoTempAll, DateUtils) {

            $scope.instBuildings = [];
            $scope.isPreCondition = true;
            $scope.preConditionStatus = [];
            //$scope.calendar = {
            //    opened: {},
            //    dateFormat: 'yyyy-MM-dd',
            //    dateOptions: {},
            //    open: function ($event, which) {
            //        $event.preventDefault();
            //        $event.stopPropagation();
            //        $scope.calendar.opened[which] = true;
            //    }
            //};
            $scope.instInfraInfo = entity;
            $scope.instInfraBuildingDirections = InstInfraBuildingDirections.get({}, function (result) {
                $scope.positionLabels = result;
            });

            $q.all([$scope.instInfraInfo.$promise]).then(function () {
                $scope.instLand = $scope.instInfraInfo.instLandTemp;
                if ($scope.instInfraInfo.id != null) {
                    InstInfraInfoTempAll.get({institueid: $scope.instInfraInfo.institute.id}, function (result) {

                        if (result.instBuildingInfoList.length > 0) {
                            $scope.instBuildings = result.instBuildingInfoList;
                        }
                        else {
                            $scope.instBuildings = [{
                                name: null,
                                totalArea: null,
                                direction: null,
                                classRoom: null,
                                officeRoom: null,
                                teachersRoom: null,
                                workshopRoom: null,
                                trainingRoom: null,
                                meetingRoom: null,
                                researchRoom: null,
                                labRoom: null,
                                computerLab: null,
                                counselingRoom: null,
                                auditoriumRoom: null,
                                commonRoom: null,
                                maleBathroom: null,
                                femaleBathroom: null,
                                storeRoom: null,
                                totalShop: null,
                                dStatus: null,
                                dMwash: null,
                                dFwash: null,
                                labNo: null,
                                workshopNo: null,
                                id: null
                            }];
                        }

                        if (result.instShopInfoList.length > 0) {
                            $scope.instShopInfos = result.instShopInfoList;
                        }
                        else {
                            $scope.instShopInfos = [{
                                nameOrNumber: null,
                                buildingNameOrNumber: null,
                                length: null,
                                width: null,
                                id: null
                            }];
                        }

                        if (result.instLabInfoList.length > 0) {
                            $scope.instLabInfos = result.instLabInfoList;
                        }
                        else {
                            $scope.instLabInfos = [{
                                nameOrNumber: null,
                                buildingNameOrNumber: null,
                                length: null,
                                width: null,
                                totalBooks: null,
                                id: null
                            }];
                        }

                        if (result.instPlayGroundInfoList.length > 0) {
                            $scope.instPlayGroundInfos = result.instPlayGroundInfoList;
                        }
                        else {
                            $scope.instPlayGroundInfos = [{
                                playgroundNo: null,
                                area: null,
                                id: null
                            }];
                        }


                    }, function (result) {
                    });
                }
                else {

                    $scope.instBuildings = [{
                        name: null,
                        totalArea: null,
                        direction: null,
                        classRoom: null,
                        officeRoom: null,
                        teachersRoom: null,
                        workshopRoom: null,
                        trainingRoom: null,
                        meetingRoom: null,
                        researchRoom: null,
                        labRoom: null,
                        computerLab: null,
                        counselingRoom: null,
                        auditoriumRoom: null,
                        commonRoom: null,
                        maleBathroom: null,
                        femaleBathroom: null,
                        storeRoom: null,
                        totalShop: null,
                        dStatus: null,
                        dMwash: null,
                        dFwash: null,
                        labNo: null,
                        workshopNo: null,
                        id: null
                    }];
                    $scope.instShopInfos = [{
                        nameOrNumber: null,
                        buildingNameOrNumber: null,
                        length: null,
                        width: null,
                        id: null
                    }];
                    $scope.instLabInfos = [{
                        nameOrNumber: null,
                        buildingNameOrNumber: null,
                        length: null,
                        width: null,
                        totalBooks: null,
                        id: null
                    }];
                    $scope.instPlayGroundInfos = [{
                        playgroundNo: null,
                        area: null,
                        id: null
                    }];
                }

                return $scope.instInfraInfo.$promise;
            });
            var onInstLandSaveSuccess = function (result) {
                $scope.$emit('stepApp:instInfraInfoUpdate', result);
                $scope.isSaving = false;
                $scope.instInfraInfo.instLandTemp = result;
                if (!$scope.instInfraInfo.id) {
                    InstInfraInfoTemp.save($scope.instInfraInfo, onInstInfraInfoSaveSuccess, onInstInfraInfoSaveError);
                    $rootScope.setSuccessMessage('stepApp.instInfraInfo.created');
                }
                else {
                    InstInfraInfoTemp.update($scope.instInfraInfo, onInstInfraInfoSaveSuccess, onInstInfraInfoSaveError);
                    $rootScope.setWarningMessage('stepApp.instInfraInfo.updated');
                }
            };
            var onPlayGroundSave = function (result) {
            }
            var onBuildingSave = function (result) {
            }

            var onLabSave = function (result) {

            };

            var onShopSave = function (result) {

            };

            var onInstInfraInfoSaveSuccess = function (result) {
                $scope.$emit('stepApp:instInfraInfoUpdate', result);
                $scope.isSaving = false;
                angular.forEach($scope.instShopInfos, function (data) {
                    if (data.id != null) {
                        InstShopInfoTemp.update(data, onShopSave);
                    }
                    else if (data.id == null){
                        if (data.nameOrNumber != null && data.buildingNameOrNumber != null && data.length && data.width != null) {
                            data.instInfraInfoTemp = result;
                            InstShopInfoTemp.save(data, onShopSave);
                        }
                    }
                });
                //$scope.instBuilding.dStatus =  true;
                angular.forEach($scope.instLabInfos, function (data) {
                    if (data.id != null) {
                        InstLabInfoTemp.update(data, onLabSave);
                    }
                    else if (data.id == null){
                        if (data.nameOrNumber != null && data.buildingNameOrNumber != null && data.length && data.width != null && data.totalBooks != null) {
                            data.instInfraInfoTemp = result;
                            InstLabInfoTemp.save(data, onLabSave);
                        }

                    }
                });
                angular.forEach($scope.instBuildings, function (data) {
                    if (data.id != null) {
                        InstBuildingTemp.update(data);
                    }
                    else if (data.id == null){
                        if (data.classRoom != null && data.officeRoom != null) {
                            data.totalRoom = 22;
                            data.instInfraInfoTemp = result;
                            InstBuildingTemp.save(data);
                        }
                    }
                });

                angular.forEach($scope.instPlayGroundInfos, function (data) {
                    if (data.id != null) {
                        InstPlayGroundInfoTemp.update(data, onPlayGroundSave);
                    }
                    else if (data.id == null){
                        if (data.playgroundNo != null && data.area != null) {
                            data.instInfraInfoTemp = result;
                            InstPlayGroundInfoTemp.save(data, onPlayGroundSave);
                        }
                    }
                });
                $state.go('instituteInfo.infrastructureInfo', {}, {reload: true});
                $rootScope.setSuccessMessage('stepApp.instInfraInfo.created');
            };


            var onInstBuildingSaveError = function (result) {
                $scope.isSaving = false;
            };

            var onInstLandSaveError = function (result) {
                $scope.isSaving = false;
            };

            var onInstInfraInfoSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instInfraInfo.id != null && $scope.mainPrecondition()) {
                    if ($scope.instInfraInfo.status = 1) {
                        $scope.instInfraInfo.status = 3;
                        InstLandTemp.update($scope.instLand, onInstLandSaveSuccess, onInstLandSaveError);
                        $rootScope.setSuccessMessage('stepApp.instInfraInfo.created');
                    } else {
                        $scope.instInfraInfo.status = 0;
                        InstLandTemp.update($scope.instLand, onInstLandSaveSuccess, onInstLandSaveError);
                        $rootScope.setSuccessMessage('stepApp.instInfraInfo.created');
                    }

                } else if ($scope.instInfraInfo.id == null && $scope.mainPrecondition()){
                    $scope.instInfraInfo.status = 0;
                    InstLandTemp.save($scope.instLand, onInstLandSaveSuccess, onInstLandSaveError);
                    $rootScope.setSuccessMessage('stepApp.instInfraInfo.created');
                }else {
                        alert('You entered wrong data, please enter correct data in all field');
                }
            };
            $scope.todate = new Date();
            $scope.clear = function () {
            };
            $scope.addWorkShopMore = function () {
                $scope.instShopInfos.push({
                    nameOrNumber: null,
                    buildingNameOrNumber: null,
                    length: null,
                    width: null,
                    id: null
                });
            };

            $scope.removeShopInfoInfo = function (ShopInfo) {
                var index = $scope.instShopInfos.indexOf(ShopInfo);
                $scope.instShopInfos.splice(index, 1);

            }
            $scope.addLibraryMore = function () {
                $scope.instLabInfos.push({
                    nameOrNumber: null,
                    buildingNameOrNumber: null,
                    length: null,
                    width: null,
                    totalBooks: null,
                    id: null
                });
            };
            $scope.removeBuildingInfo = function (building) {
                var index = $scope.instBuildings.indexOf(building);
                $scope.instBuildings.splice(index, 1);

            }
            $scope.removeLibraryInfo = function (bankInfo) {
                var index = $scope.instLabInfos.indexOf(bankInfo);
                $scope.instLabInfos.splice(index, 1);

            }
            $scope.addBuildingMore = function () {
                $scope.instBuildings.push({
                    name: null,
                    totalArea: null,
                    direction: null,
                    classRoom: null,
                    officeRoom: null,
                    teachersRoom: null,
                    workshopRoom: null,
                    trainingRoom: null,
                    meetingRoom: null,
                    researchRoom: null,
                    labRoom: null,
                    computerLab: null,
                    counselingRoom: null,
                    auditoriumRoom: null,
                    commonRoom: null,
                    maleBathroom: null,
                    femaleBathroom: null,
                    storeRoom: null,
                    totalShop: null,
                    dStatus: null,
                    dMwash: null,
                    labNo: null,
                    workshopNo: null,
                    id: null
                });
                $timeout(function() {
                    $rootScope.refreshRequiredFields();
                }, 100);
            };
            $scope.removeBuildingInfo = function (instBuilding) {
                var index = $scope.instBuildings.indexOf(instBuilding);
                $scope.instBuildings.splice(index, 1);

            }

            $scope.addPlayGroundMore = function () {
                $scope.instPlayGroundInfos.push({
                    playgroundNo: null,
                    area: null,
                    id: null
                });
            }
            $scope.removePlayGroundInfo = function (PlayGroundInfo) {
                var index = $scope.instPlayGroundInfos.indexOf(PlayGroundInfo);
                $scope.instPlayGroundInfos.splice(index, 1);

            }
            $scope.mainPrecondition = function () {

                angular.forEach($scope.instBuildings, function (itemBuilding) {
                    $scope.preConditionInstBuilding(itemBuilding);
                    console.log("Bappi instBuilding");
                    console.log($scope.preConditionStatus);
                    console.log("Kanak instBuilding");
                });

                 angular.forEach($scope.instLabInfos, function (itemInstLabInfo) {
                     $scope.preConditionInstLabInfo(itemInstLabInfo);
                     console.log("Bappi instLabInfo");
                     console.log($scope.preConditionStatus);
                     console.log("Kanak instLabInfo");
                 });
                 angular.forEach($scope.instShopInfos, function (iteminstShopInfo) {
                     $scope.preConditionInstShopInfo(iteminstShopInfo);
                     console.log("Bappi instShopInfo");
                     console.log($scope.preConditionStatus);
                     console.log("Kanak instShopInfo");
                 });

                 angular.forEach($scope.instPlayGroundInfos, function (iteminstPlayGroundInfo) {
                     $scope.preConditionPlayGroundInfo(iteminstPlayGroundInfo);
                     console.log("Bappi instPlayGroundInfo");
                     console.log($scope.preConditionStatus);
                     console.log("Kanak instPlayGroundInfo");
                 });
                 $scope.preConditionInstLand();

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                console.log("Bappi Da");
                console.log($scope.preConditionStatus);
                console.log("Kanak Da");
                return $scope.isPreCondition;
            };

            $scope.preConditionInstBuilding = function(instBuilding) {

                $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.name', instBuilding.name, 'text', 300, 2, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.totalArea', instBuilding.totalArea, 'number', 11, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.classRoom', instBuilding.classRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.officeRoom', instBuilding.officeRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.teachersRoom', instBuilding.teachersRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.workshopRoom', instBuilding.workshopRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.trainingRoom', instBuilding.trainingRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.meetingRoom', instBuilding.meetingRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.researchRoom', instBuilding.researchRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.labRoom', instBuilding.labRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.auditoriumRoom', instBuilding.auditoriumRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.prayerHall', instBuilding.prayerHall, 'text', 3, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.maleBathroom', instBuilding.maleBathroom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.femaleBathroom', instBuilding.femaleBathroom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.storeRoom', instBuilding.storeRoom, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.totalShop', instBuilding.totalShop, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.dMwash', instBuilding.dMwash, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.dFwash', instBuilding.dFwash, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.labNo', instBuilding.labNo, 'number', 5, 1, '0to9', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('instBuilding.workshopNo', instBuilding.workshopNo, 'number', 5, 1, '0to9', true, '', ''));
                // if($scope.preConditionStatus.indexOf(false) !== -1){
                //     $scope.isPreCondition = false;
                // }else {
                //     $scope.isPreCondition = true;
                // }
                // return $scope.isPreCondition;
            };
            $scope.preConditionInstLabInfo = function(instLabInfo) {
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLabInfo.nameOrNumber', instLabInfo.nameOrNumber, 'text', 150, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLabInfo.buildingNameOrNumber', instLabInfo.buildingNameOrNumber, 'text', 150, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLabInfo.length', instLabInfo.length, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLabInfo.width', instLabInfo.width, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLabInfo.totalBooks', instLabInfo.totalBooks, 'number', 50, 1, '0to9', true, '', ''));

                // if($scope.preConditionStatus.indexOf(false) !== -1){
                //     $scope.isPreCondition = false;
                // }else {
                //     $scope.isPreCondition = true;
                // }
                // return $scope.isPreCondition;
            }
            $scope.preConditionInstShopInfo = function(instShopInfo) {
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instShopInfo.nameOrNumber', instShopInfo.nameOrNumber, 'text', 150, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instShopInfo.buildingNameOrNumber', instShopInfo.buildingNameOrNumber, 'text', 150, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instShopInfo.length', instShopInfo.length, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instShopInfo.width', instShopInfo.width, 'number', 50, 1, '0to9', true, '', ''));
                // if($scope.preConditionStatus.indexOf(false) !== -1){
                //     $scope.isPreCondition = false;
                // }else {
                //     $scope.isPreCondition = true;
                // }
                // return $scope.isPreCondition;
            };
            $scope.preConditionInstLand = function() {
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.jlNo', $scope.instLand.jlNo, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.ledgerNo', $scope.instLand.ledgerNo, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.dagNo', $scope.instLand.dagNo, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.amountOfLand', $scope.instLand.amountOfLand, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.landRegistrationLedgerNo', $scope.instLand.landRegistrationLedgerNo, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.landRegistrationDate', DateUtils.convertLocaleDateToDMY($scope.instLand.landRegistrationDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.lastTaxPaymentDate', DateUtils.convertLocaleDateToDMY($scope.instLand.lastTaxPaymentDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.boundaryNorth', $scope.instLand.boundaryNorth, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.boundarySouth', $scope.instLand.boundarySouth, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.boundaryEast', $scope.instLand.boundaryEast, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.distncFromDistHQ', $scope.instLand.distncFromDistHQ, 'text', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.distncFromMainRd', $scope.instLand.distncFromMainRd, 'text', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.height', $scope.instLand.height, 'text', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.width', $scope.instLand.width, 'text', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.mutationNo', $scope.instLand.mutationNo, 'text', 50, 1, 'atozAndAtoZ0-9-/', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.mutationDate', DateUtils.convertLocaleDateToDMY($scope.instLand.mutationDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.deedNo', $scope.instLand.deedNo, 'text', 150, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.giverName', $scope.instLand.giverName, 'text', 150, 1, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.receiverName', $scope.instLand.receiverName, 'text', 150, 1, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLand.receiverDate', DateUtils.convertLocaleDateToDMY($scope.instLand.receiverDate), 'date', 60, 5, 'date', true, '', ''));
                // if($scope.preConditionStatus.indexOf(false) !== -1){
                //     $scope.isPreCondition = false;
                // }else {
                //     $scope.isPreCondition = true;
                // }
                // return $scope.isPreCondition;
            };

            $scope.preConditionPlayGroundInfo = function(instPlayGroundInfo) {
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instPlayGroundInfo.playgroundNo', instPlayGroundInfo.playgroundNo, 'text', 150, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instPlayGroundInfo.area', instPlayGroundInfo.area, 'text', 50, 1, '0to9', true, '', ''));
                // if($scope.preConditionStatus.indexOf(false) !== -1){
                //     $scope.isPreCondition = false;
                // }else {
                //     $scope.isPreCondition = true;
                // }
                // return $scope.isPreCondition;
            }
        }]);
/*instInfraInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('InstInfraInfoDetailController',
        ['$scope', '$q', '$state', '$rootScope', '$stateParams', 'entity', 'InstInfraInfo', 'Institute', 'InstBuilding', 'InstituteByLogin', 'InstLand', 'AllInstInfraInfo', 'InstInfraInfoTempAll',
            function ($scope, $q, $state, $rootScope, $stateParams, entity, InstInfraInfo, Institute, InstBuilding, InstituteByLogin, InstLand, AllInstInfraInfo, InstInfraInfoTempAll) {


                InstituteByLogin.get({}, function (result) {
                    $scope.institute = result;

                    InstInfraInfoTempAll.get({institueid: result.id}, function (result) {
                        $scope.instInfraInfo = result;
                        $scope.instShopInfos = result.instShopInfoList;
                        $scope.instLabInfos = result.instLabInfoList;
                        $scope.instBuildings = result.instBuildingInfoList;


                        $scope.instPlayGroundInfos = result.instPlayGroundInfoList;

                        if (!$scope.instInfraInfo) {
                            $state.go('instituteInfo.infrastructureInfo.new', {}, {reload: true});
                        }

                    });
                });


            }]);
/*instInfraInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstInfraInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstInfraInfo',
            function ($scope, $rootScope, $modalInstance, entity, InstInfraInfo) {

                $scope.instInfraInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstInfraInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.InstInfraInfo.deleted');
                };

            }]);


angular.module('stepApp')
    .controller('InstInfraInfoApproveController',
        ['$scope', '$q', '$modalInstance', 'InstituteInfraInfoByInstitute', 'InstituteLandByInstitute', 'InstituteBuildingByInstitute', 'Institute', '$state', 'entity', 'InstInfraInfo', 'InstituteBuilding', 'InstituteInfraInfo', 'InstituteLand',
            function ($scope, $q, $modalInstance, InstituteInfraInfoByInstitute, InstituteLandByInstitute, InstituteBuildingByInstitute, Institute, $state, entity, InstInfraInfo, InstituteBuilding, InstituteInfraInfo, InstituteLand) {
                $scope.infraInfo = entity;
                $q.all([$scope.infraInfo.$promise]).then(function () {
                    $scope.inStituteInfra = $scope.infraInfo;
                    InstituteInfraInfoByInstitute.get({id: $scope.infraInfo.institute.id}, function (result) {
                        $scope.tempInstituteInfraInfo = result;
                        $scope.tempInstituteLand = result.instLand;
                        $scope.tempInstituteBuilding = result.instBuilding;
                    }, function (result) {
                        $scope.tempInstituteInfraInfo = {};
                        $scope.tempInstituteLand = {};
                        $scope.tempInstituteBuilding = {};

                    });
                    return $scope.infraInfo.$promise;
                });

                var onSaveSuccess = function (result) {
                    $modalInstance.close(result);
                    $scope.isSaving = false;
                };


                var onInstBuildingSaveSuccess = function (result) {

                    $scope.inStituteInfra.instBuilding = result
                    if (!$scope.tempInstituteLand.id) {
                        $scope.inStituteInfra.instLand.id = null;
                        InstituteLand.save($scope.inStituteInfra.instLand, onInstLandSaveSuccess, onInstLandSaveError);
                    }
                    else {
                        $scope.inStituteInfra.instLand.id = $scope.tempInstituteLand.id;
                        InstituteLand.update($scope.inStituteInfra.instLand, onInstLandSaveSuccess, onInstLandSaveError);
                    }

                };
                var onInstLandSaveSuccess = function (result) {
                    $scope.$emit('stepApp:instInfraInfoUpdate', result);
                    $scope.isSaving = false;
                    $scope.inStituteInfra.instLand = result;
                    if (!$scope.tempInstituteInfraInfo.id) {
                        $scope.inStituteInfra.id = null;

                        InstituteInfraInfo.save($scope.inStituteInfra, onInstInfraInfoSaveSuccess, onInstInfraInfoSaveError);
                    }
                    else {
                        InstituteInfraInfo.update($scope.inStituteInfra, onInstInfraInfoSaveSuccess, onInstInfraInfoSaveError);
                    }

                };
                var onInstInfraInfoSaveSuccess = function (result) {
                    $scope.$emit('stepApp:instInfraInfoUpdate', result);
                    $scope.isSaving = false;
                    $modalInstance.close(result);
                    $state.go('instituteInfo.approve', {}, {reload: true});
                };

                var onInstBuildingSaveError = function (result) {
                    $scope.isSaving = false;
                };
                var onInstLandSaveError = function (result) {
                    $scope.isSaving = false;
                };

                var onInstInfraInfoSaveError = function (result) {
                    $scope.isSaving = false;
                };
                var onInstInfraStatusUpdateError = function (result) {
                    $scope.isSaving = false;
                };

                var onInstBuildingSave = function (result) {
                    $scope.isSaving = true;
                    if (!$scope.tempInstituteBuilding.id) {
                        $scope.inStituteInfra.instBuilding.id = null;
                        InstituteBuilding.save($scope.inStituteInfra.instBuilding, onInstBuildingSaveSuccess, onInstBuildingSaveError);
                    }
                    else {
                        $scope.inStituteInfra.instBuilding.id = $scope.tempInstituteBuilding.id;
                        InstituteBuilding.update($scope.inStituteInfra.instBuilding, onInstBuildingSaveSuccess, onInstBuildingSaveError);
                    }


                };


                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                    $state.go('instituteInfo.approve');
                };

                $scope.confirmApprove = function () {
                    $scope.isSaving = true;
                    $scope.infraInfo.status = 1;

                    InstInfraInfo.update($scope.infraInfo, onInstBuildingSave, onInstInfraStatusUpdateError);

                };
                var onInstGenInfoSaveSuccess = function (result) {
                    $scope.isInstGenInfoSaving = true;
                    $state.go('instGenInfo.InstInfraInfo');

                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

            }])
    .controller('InstInfraInfoDeclineController',
        ['$scope', '$modalInstance', '$stateParams', 'Institute', '$state', 'InstInfraInfo', 'InstInfraInfoDecline',
            function ($scope, $modalInstance, $stateParams, Institute, $state, InstInfraInfo, InstInfraInfoDecline) {
                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.decline = function () {
                    InstInfraInfoDecline.update({id: $stateParams.id}, $scope.causeDeny);
                    $modalInstance.close();
                }

            }]);




