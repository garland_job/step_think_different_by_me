'use strict';

angular.module('stepApp')
    .controller('IisCurriculumInfoController',
        ['$scope', '$state', '$modal', 'IisCurriculumInfo', 'IisCurriculumInfoSearch', 'ParseLinks', 'FindCurriculumByInstId', 'CurriculumsOfCurrentInst',
            function ($scope, $state, $modal, IisCurriculumInfo, IisCurriculumInfoSearch, ParseLinks, FindCurriculumByInstId, CurriculumsOfCurrentInst) {

                $scope.FindCurriculumByInstId = {};

                $scope.page = 0;

                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    IisCurriculumInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.iisCurriculumInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };
                CurriculumsOfCurrentInst.query({page: $scope.page, size: 200}, function (result, headers) {
                    $scope.findCurriculumByInstIds = result;

                });

                $scope.clear = function () {
                    $scope.iisCurriculumInfo = {
                        firstDate: null,
                        lastDate: null,
                        mpoEnlisted: null,
                        recNo: null,
                        mpoDate: null,
                        id: null
                    };
                };
            }]);
/*iisCurriculumInfo-dialog.controller.js*/

angular.module('stepApp').controller('IisCurriculumInfoDialogController',
    ['$scope', '$rootScope', '$state', 'InstituteByLogin', '$stateParams', 'entity', 'IisCurriculumInfo', 'CmsCurriculum', 'IisCurriculumInfoTemp', 'FindActivecmsCurriculums','DateUtils',
        function ($scope, $rootScope, $state, InstituteByLogin, $stateParams, entity, IisCurriculumInfo, CmsCurriculum, IisCurriculumInfoTemp, FindActivecmsCurriculums, DateUtils) {
            $scope.iisCurriculumInfo = entity;
            $scope.logInInstitute = {};
            $scope.cmscurriculums = FindActivecmsCurriculums.query({page: $scope.page, size: 1000});


            InstituteByLogin.query({}, function (result) {
                $scope.logInInstitute = result;
                $scope.affiliationDate = result.firstAffiliationDate;
                console.log(result.firstAffiliationDate);
                if ($scope.logInInstitute.type == 'NonGovernment') {
                    $scope.iisCurriculumInfo.mpoEnlisted = true;
                }
            });
            $scope.load = function (id) {
                IisCurriculumInfoTemp.get({page: $scope.page, size: 1000, id: id}, function (result) {
                    $scope.iisCurriculumInfo = result;
                });
            };
            $scope.deadlineValidation = function () {
                var d1 = Date.parse($scope.iisCurriculumInfo.lastDate);
                var d2 = Date.parse($scope.iisCurriculumInfo.firstDate);
                if (d1 <= d2) {
                    $scope.dateError = true;
                } else {
                    $scope.dateError = false;
                }
            };
            $scope.toDay = new Date();
            $rootScope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $rootScope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:iisCurriculumInfoUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('instituteInfo.iisCurriculumInfo', {}, {reload: true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.setFirstAcdrattach = function ($file, iisCurriculumInfo, firstAcdrAttach, firstAcdrAttachCntType, firstAcdrAttachName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            iisCurriculumInfo[firstAcdrAttach] = base64Data;
                            iisCurriculumInfo[firstAcdrAttachCntType] = $file.type;
                            iisCurriculumInfo[firstAcdrAttachName] = $file.name;
                        });
                    };
                }
            };
            $scope.setLastAcdrattach = function ($file, iisCurriculumInfo, lastAcdrAttach, lastAcdrAttachCntType, lastAcdrAttachName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            iisCurriculumInfo[lastAcdrAttach] = base64Data;
                            iisCurriculumInfo[lastAcdrAttachCntType] = $file.type;
                            iisCurriculumInfo[lastAcdrAttachName] = $file.name;
                        });
                    };
                }
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.iisCurriculumInfo.id != null && $scope.preCondition()) {
                    if ($scope.logInInstitute.type == 'Government') {
                        $scope.iisCurriculumInfo.mpoEnlisted = false;
                    }
                    if ($scope.iisCurriculumInfo.mpoEnlisted == null) {
                        $scope.iisCurriculumInfo.mpoEnlisted = true;
                    }
                    IisCurriculumInfoTemp.update($scope.iisCurriculumInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.iisCurriculumInfo.updated');
                } else if($scope.iisCurriculumInfo.id == null && $scope.preCondition()){
                    $scope.iisCurriculumInfo.institute = $scope.logInInstitute;

                    if ($scope.logInInstitute.type == 'Government') {
                        $scope.iisCurriculumInfo.mpoEnlisted = false;
                    }
                    if ($scope.iisCurriculumInfo.mpoEnlisted == null) {
                        $scope.iisCurriculumInfo.mpoEnlisted = true;
                    }
                    IisCurriculumInfoTemp.save($scope.iisCurriculumInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.iisCurriculumInfo.created');
                }else{
                    $scope.isSaving = false;
                    $rootScope.setErrorMessage('stepApp.iisCurriculumInfo.validationError');

                }
            };


            /*$scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };*/

            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCurriculumInfo.acSession', $scope.iisCurriculumInfo.acSession, 'text', 150, 8, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCurriculumInfo.firstDate', DateUtils.convertLocaleDateToDMY($scope.iisCurriculumInfo.firstDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCurriculumInfo.recNo', $scope.iisCurriculumInfo.recNo, 'text', 150, 2, 'atozAndAtoZ0-9-/', false, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCurriculumInfo.lastRecNo', $scope.iisCurriculumInfo.lastRecNo, 'text', 150, 2, 'atozAndAtoZ0-9-/', false, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCurriculumInfo.lastDate', DateUtils.convertLocaleDateToDMY($scope.iisCurriculumInfo.lastDate), 'date', 60, 5, 'date', true, '',''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            };
            $scope.clear = function () {
                //$modalInstance.dismiss('cancel');
            };
        }]);
/*iisCurriculumInfo-detail.controller.js*/
angular.module('stepApp')
    .controller('IisCurriculumInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'IisCurriculumInfo', 'CmsCurriculum', 'IisCurriculumInfoTemp',
            function ($scope, $rootScope, $stateParams, entity, IisCurriculumInfo, CmsCurriculum, IisCurriculumInfoTemp) {
                $scope.iisCurriculumInfo = entity;

                $scope.load = function (id) {
                    IisCurriculumInfoTemp.get({id: id}, function (result) {
                        $scope.iisCurriculumInfo = result;
                    });
                };

                IisCurriculumInfo.get({id: $stateParams.id}, function (result) {
                    $scope.iisCurriculumInfo = result;
                });

                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = "" + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };


                // var unsubscribe = $rootScope.$on('stepApp:iisCurriculumInfoUpdate', function (event, result) {
                //     $scope.iisCurriculumInfo = result;
                // });
                // $scope.$on('$destroy', unsubscribe);

            }]);
/*iisCurriculumInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('IisCurriculumInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'IisCurriculumInfo',
            function ($scope, $rootScope, $modalInstance, entity, IisCurriculumInfo) {

                $scope.iisCurriculumInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    IisCurriculumInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.iisCurriculumInfo.deleted');
                };

            }]);

/*iisCurriculumInfo-approve-dialog.controller.js*/
angular.module('stepApp')
    .controller('IisCurriculumInfoApproveController',
        ['$scope', '$modalInstance', 'Institute', '$state', 'entity', 'InstFinancialInfo', 'IisCurriculumsByInstituteAndStatus', '$stateParams', 'IisCurriculumInfo',
            function ($scope, $modalInstance, Institute, $state, entity, InstFinancialInfo, IisCurriculumsByInstituteAndStatus, $stateParams, IisCurriculumInfo) {
                $scope.iisCurriculums = [];
                var onSaveSuccess = function (result) {
                    $scope.$emit('stepApp:instituteUpdate', result);

                    $modalInstance.close(result);
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmApprove = function () {
                    $scope.isSaving = true;
                    IisCurriculumsByInstituteAndStatus.query({
                        instituteId: $stateParams.instid,
                        status: 0
                    }, function (result) {
                        $scope.iisCurriculums = result;
                        angular.forEach(result, function (data) {
                            if (data.id != null) {
                                data.status = 3;
                                IisCurriculumInfo.update(data, onSaveSuccess, onSaveError);

                            }

                        });
                    });


                }
            }])
    .controller('IisCurriculumInfoDeclineController',
        ['$scope', '$modalInstance', 'Institute', '$state', 'InstFinancialInfo', 'InstFinancialInfoDecline', '$stateParams',
            function ($scope, $modalInstance, Institute, $state, InstFinancialInfo, InstFinancialInfoDecline, $stateParams) {

            }]);


