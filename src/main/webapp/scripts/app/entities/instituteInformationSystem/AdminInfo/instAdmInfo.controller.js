'use strict';

angular.module('stepApp')
    .controller('InstAdmInfoController',
        ['$scope', '$state', '$modal', 'InstAdmInfo', 'InstAdmInfoSearch', 'ParseLinks', 'Principal',
            function ($scope, $state, $modal, InstAdmInfo, InstAdmInfoSearch, ParseLinks, Principal) {

                $scope.instAdmInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstAdmInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        angular.forEach(result, function (adminInfo) {
                            if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                                adminInfo.edit = false;
                                adminInfo.approve = true;
                                adminInfo.reject = true;
                                if (adminInfo.status != 1) {
                                    $scope.instAdmInfos.push(adminInfo);
                                }
                            }

                            else if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                                adminInfo.edit = true;
                                adminInfo.approve = false;
                                adminInfo.reject = false;
                                $scope.instAdmInfos.push(adminInfo);
                            } else {
                                adminInfo.edit = false;
                                adminInfo.approve = false;
                                adminInfo.reject = false;
                                $scope.instAdmInfos.push(adminInfo);
                            }

                        });
                        if ($scope.instAdmInfos.length == 0) {
                            $state.go('instGenInfo.instAdmInfo.new');
                        }

                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstAdmInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instAdmInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instAdmInfo = {
                        adminCounselorName: null,
                        counselorMobileNo: null,
                        insHeadName: null,
                        insHeadMobileNo: null,
                        deoName: null,
                        deoMobileNo: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*instAdmInfo-dialog.controller.js*/

angular.module('stepApp').controller('InstAdmInfoDialogController',
    ['$scope', '$rootScope', '$timeout', '$stateParams', '$q', '$state', 'entity', 'InstAdmInfo', 'Institute', 'governingBodys', 'InstGovernBody', 'InstituteAllInfo', 'InstGovernBodyTemp', 'InstAdmInfoTemp', 'InstituteAllInfosTemp', 'InstituteByLogin',
        function ($scope, $rootScope, $timeout, $stateParams, $q, $state, entity, InstAdmInfo, Institute, governingBodys, InstGovernBody, InstituteAllInfo, InstGovernBodyTemp, InstAdmInfoTemp, InstituteAllInfosTemp, InstituteByLogin) {


            $scope.instAdmInfo = entity;
            $scope.instGovernBodys = governingBodys;
            $scope.succeessMsg = '';
            $scope.instituteType = '';

            if($stateParams.id !=null){
                InstAdmInfoTemp.get({id : $stateParams.id},function(result){
                    $scope.instAdmInfo = result;
                });
            }

            $q.all([$scope.instAdmInfo.$promise]).then(function () {
                if ($scope.instAdmInfo.id == null) {

                }
                else {
                    InstituteAllInfosTemp.get({id: $scope.instAdmInfo.institute.id}, function (result) {
                        $scope.instGovernBodys = result.instGovernBody;
                    })
                }
            });

            InstituteByLogin.query({}, function (result) {
                $scope.instituteType = result.type;

            });
            $scope.AddMore = function () {
                $scope.instGovernBodys.push(
                    {
                        name: null,
                        position: null,
                        mobileNo: null,
                        status: null
                    }
                );
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);
            }
            $scope.removeGovernBody = function (GovernBody) {
                var index = $scope.instGovernBodys.indexOf(GovernBody);
                $scope.instGovernBodys.splice(index, 1);

            }
            $scope.load = function (id) {
                InstAdmInfo.get({id: id}, function (result) {
                    $scope.instAdmInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:adminInfo', result);
                $scope.isSaving = false;

                angular.forEach($scope.instGovernBodys, function (data) {
                    if (data.id) {
                        //InstGovernBody.update(data, onGoverningBodySaveSuccess, onGoverningBodySaveError);
                        InstGovernBodyTemp.update(data, onGoverningBodySaveSuccess, onGoverningBodySaveError);
                        $rootScope.setSuccessMessage('stepApp.instAdmInfo.created');
                    }
                    else {
                        if (data.name != null && data.position != null) {
                            data.institute = result.institute;
                            //InstGovernBody.save(data, onGoverningBodySaveSuccess, onGoverningBodySaveError);
                            InstGovernBodyTemp.save(data, onGoverningBodySaveSuccess, onGoverningBodySaveError);
                            $rootScope.setWarningMessage('stepApp.instAdmInfo.updated');
                        }
                    }

                });

                $state.go('instituteInfo.adminInfo', {}, {reload: true});
            };

            $scope.mobileValidation = function () {

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;

                angular.forEach($scope.instGovernBodys, function (data) {
                    $scope.preCondition2(data);
                });
                if ($scope.instAdmInfo.id != null && $scope.preCondition()) {
                    if ($scope.instAdmInfo.status == 1) {
                        $scope.instAdmInfo.status = 3;
                        $scope.succeessMsg = 'stepApp.instAdmInfo.updated';
                        InstAdmInfo.update($scope.instAdmInfo, onSaveSuccess, onSaveError);

                    } else if ($scope.preCondition()){
                        $scope.instAdmInfo.status = 0;
                        $scope.succeessMsg = 'stepApp.instAdmInfo.created';
                        InstAdmInfoTemp.update($scope.instAdmInfo, onSaveSuccess, onSaveError);
                    }

                } else if ($scope.instAdmInfo.id == null && $scope.preCondition()) {
                    $scope.instAdmInfo.status = 0;
                    $scope.succeessMsg = 'stepApp.instAdmInfo.created';
                    InstAdmInfoTemp.save($scope.instAdmInfo, onSaveSuccess, onSaveError);
                }else{
                    $rootScope.setErrorMessage('There is an error in submitting application');
                    $scope.isSaving = false;
                }
            };


            var onGoverningBodySaveSuccess = function (result) {
                $scope.isSaving = false;
                $rootScope.setWarningMessage($scope.succeessMsg);
                $state.go('instituteInfo.adminInfo', {}, {reload: true});

            }
            var onGoverningBodySaveError = function (result) {
                $scope.isSaving = false;
            }

            $scope.clear = function () {
                $scope.instAdmInfo = null;
            };
            $scope.preConditionStatus = [];

            $scope.preCondition = function(){
                $scope.isPreCondition = true;

                $scope.preConditionStatus.push($rootScope.layerDataCheck('instAdmInfo.adminCounselorName', $scope.instAdmInfo.adminCounselorName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instAdmInfo.counselorMobileNo', $scope.instAdmInfo.counselorMobileNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instAdmInfo.insHeadName', $scope.instAdmInfo.insHeadName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instAdmInfo.insHeadMobileNo', $scope.instAdmInfo.insHeadMobileNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instAdmInfo.deoName', $scope.instAdmInfo.deoName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instAdmInfo.deoMobileNo', $scope.instAdmInfo.deoMobileNo, 'number', 11, 11, '0to9', true, '', ''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }

            $scope.preCondition2 = function(data) {
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGovernBody.name', data.name, 'text', 150, 5, 'atozAndAtoZ().-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGovernBody.position', data.position, 'text', 150, 5, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instGovernBody.mobileNo', data.mobileNo, 'number', 11, 11, '0to9', true, '', ''));
            }
        }]);


/*instAdmInfo-detail.controller.js*/
angular.module('stepApp')
    .controller('InstAdmInfoDetailController',
        ['$scope', '$q', '$state', '$rootScope', '$stateParams', 'entity', 'InstAdmInfo', 'Institute', 'InstituteAllInfo', 'InstAdmInfoTemp', 'InstituteAllInfosTemp',
            function ($scope, $q, $state, $rootScope, $stateParams, entity, InstAdmInfo, Institute, InstituteAllInfo, InstAdmInfoTemp, InstituteAllInfosTemp) {

                $scope.instAdmInfo = entity;
                InstAdmInfoTemp.get({id: 0}, function (result) {
                    $scope.instAdmInfo = result;
                    console.log(')))))))))))))))))');
                    console.log(result);

                    if (!$scope.instAdmInfo) {
                        $state.go('instituteInfo.adminInfo.new', {}, {reload: true});
                    }

                    InstituteAllInfosTemp.get({id: $scope.instAdmInfo.institute.id}, function (result) {
                        $scope.instGovernBodys = result.instGovernBody;
                    });
                }, function () {
                    $state.go('instituteInfo.adminInfo.new', {}, {reload: true});
                });

                var unsubscribe = $rootScope.$on('stepApp:instAdmInfoUpdate', function (event, result) {
                    $scope.instAdmInfo = result;
                });

                $scope.$on('$destroy', unsubscribe);

            }]);

/*instAdmInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstAdmInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstAdmInfo',
            function ($scope, $rootScope, $modalInstance, entity, InstAdmInfo) {

                $scope.instAdmInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstAdmInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.InstAdmInfo.deleted');
                        });
                };

            }]);

/*instAdmInfo-approve-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstAdmInfoApproveController',
        ['$scope', '$location', '$rootScope', '$modalInstance', 'Institute', '$state', 'entity', 'InstAdmInfo', '$stateParams',
            function ($scope, $location, $rootScope, $modalInstance, Institute, $state, entity, InstAdmInfo, $stateParams) {
                //$scope.instAdmInfo = entity;
                $scope.instAdmInfo = InstAdmInfo.get({id: $stateParams.admid});
                var onSaveSuccess = function (result) {


                    $modalInstance.close(result);
                    $scope.isSaving = false;
                    $location.path("#/institute-info/approve/" + $stateParams.id);

                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.confirmApprove = function () {
                    $scope.isSaving = true;
                    $scope.instAdmInfo.status = 1;
                    InstAdmInfo.update($scope.instAdmInfo, onSaveSuccess, onSaveError);


                };
                var onInstGenInfoSaveSuccess = function (result) {
                    console.log(result);
                    $scope.isInstGenInfoSaving = true;

                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

            }])
    .controller('InstAdmInfoDeclineController',
        ['$scope', '$location', '$rootScope', '$modalInstance', 'Institute', '$state', 'entity', 'InstAdmInfo', '$stateParams', 'InstAdmInfoDecline',
            function ($scope, $location, $rootScope, $modalInstance, Institute, $state, entity, InstAdmInfo, $stateParams, InstAdmInfoDecline) {
                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.decline = function () {
                    InstAdmInfoDecline.update({id: $stateParams.id}, $scope.causeDeny);
                    $modalInstance.close();
                }
            }]);





