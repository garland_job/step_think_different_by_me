'use strict';

angular.module('stepApp')
    .controller('InstMemShipController',
        ['$scope', '$state', '$modal', 'InstMemShip', 'InstMemShipSearch', 'ParseLinks', 'CurrentInstCommitteeMembers', 'User', 'UserCustomUpdate',
            function ($scope, $state, $modal, InstMemShip, InstMemShipSearch, ParseLinks, CurrentInstCommitteeMembers, User, UserCustomUpdate) {

                $scope.instMemShips = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    CurrentInstCommitteeMembers.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instMemShips = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstMemShipSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instMemShips = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.deactivateMember = function (data) {
                    data.user.activated = false;
                    UserCustomUpdate.update(data.user, function () {
                        $scope.loadAll();
                    });
                };

                $scope.activateMember = function (data) {
                    data.user.activated = true;
                    UserCustomUpdate.update(data.user, function () {
                        $scope.loadAll();
                    });
                };
                $scope.clear = function () {
                    $scope.instMemShip = {
                        fullName: null,
                        dob: null,
                        gender: null,
                        address: null,
                        email: null,
                        contact: null,
                        designation: null,
                        orgName: null,
                        orgAdd: null,
                        orgContact: null,
                        date: null,
                        id: null
                    };
                };
            }]);

/*instMemShip-dialog.controller.js*/


angular.module('stepApp').controller('InstMemShipDialogController',
    ['$scope', '$rootScope', '$state', 'MemShipByEmail', '$stateParams', 'entity', 'InstMemShip', 'Auth', 'User','DateUtils',
        function ($scope, $rootScope, $state, MemShipByEmail, $stateParams, entity, InstMemShip, Auth, User, DateUtils) {

            $scope.instMemShip = entity;
            $scope.load = function (id) {
                InstMemShip.get({id: id}, function (result) {
                    $scope.instMemShip = result;
                });
            };

            $scope.toDay = new Date();
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
            var onSaveSuccess = function (result) {


                $scope.$emit('stepApp:instMemShipUpdate', result);
                $scope.isSaving = false;
                $state.go('instituteInfo.instMemShip', {}, {reload: true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            MemShipByEmail.get({email: $scope.instMemShip.email}, function (instMemShip) {

                $scope.message = "The  email address is already existed.";
            });
            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instMemShip.id != null && $scope.preCondition()) {
                    InstMemShip.update($scope.instMemShip, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.instMemShip.updated');
                } else if ($scope.instMemShip.id == null && $scope.preCondition()){
                    $scope.instMemShip.user.email = $scope.instMemShip.email;
                    $scope.instMemShip.user.firstName = $scope.instMemShip.fullName;
                    $scope.instMemShip.user.langKey = "en";
                    $scope.instMemShip.user.password = "123456";
                    $scope.instMemShip.user.authorities = ["ROLE_MANEGINGCOMMITTEE"];

                    Auth.createAccount($scope.instMemShip.user).then(function (res) {
                        $scope.instMemShip.user = res;

                        InstMemShip.save($scope.instMemShip, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.instMemShip.created');
                    }).catch(function (response) {
                        $scope.success = null;
                        if (response.status === 400 && response.data === 'login already in use') {
                            $scope.errorUserExists = 'ERROR';
                        } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                            $scope.errorEmailExists = 'ERROR';
                        } else {
                            $scope.error = 'ERROR';
                        }
                    });

                }
            };

            $scope.clear = function () {
            };
            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.email', $scope.instMemShip.email, 'email', 60, 5, 'email', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.fullName', $scope.instMemShip.fullName, 'text', 30, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.dob',DateUtils.convertLocaleDateToDMY($scope.instMemShip.dob), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.contact', $scope.instMemShip.contact, 'number', 11, 11, '0to9', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.designation', $scope.instMemShip.designation, 'text', 10, 1, 'atozAndAtoZ.-', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.orgName', $scope.instMemShip.orgName, 'text', 10, 1, 'atozAndAtoZ.-', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instMemShip.orgContact', $scope.instMemShip.orgContact, 'number', 11, 11, '0to9', true, '','' ));


                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }


        }]);
/*instMemShip-detail.controller.js*/

angular.module('stepApp')
    .controller('InstMemShipDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstMemShip',
            function ($scope, $rootScope, $stateParams, entity, InstMemShip) {
                $scope.instMemShip = entity;
                $scope.load = function (id) {
                    InstMemShip.get({id: id}, function (result) {
                        $scope.instMemShip = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instMemShipUpdate', function (event, result) {
                    $scope.instMemShip = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*instMemShip-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('InstMemShipDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstMemShip',
            function ($scope, $rootScope, $modalInstance, entity, InstMemShip) {

                $scope.instMemShip = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstMemShip.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.InstMemShip.deleted');
                };

            }]);


