'use strict';

angular.module('stepApp')
    .controller('InstFinancialInfoController',
        ['$scope', '$state', '$modal', 'InstFinancialInfo', 'InstFinancialInfoSearch', 'Principal', 'ParseLinks',
            function ($scope, $state, $modal, InstFinancialInfo, InstFinancialInfoSearch, Principal, ParseLinks) {

                $scope.instFinancialInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstFinancialInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        angular.forEach(result, function (financialInfo) {
                            if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                                financialInfo.edit = false;
                                financialInfo.approve = true;
                                financialInfo.reject = true;
                                if (financialInfo.status != 1) {
                                    $scope.instFinancialInfos.push(financialInfo);
                                }
                            } else if (Principal.hasAnyAuthority(['ROLE_USER'])) {
                                financialInfo.edit = true;
                                financialInfo.approve = false;
                                financialInfo.reject = false;
                                $scope.instFinancialInfos.push(financialInfo);
                            } else {
                                financialInfo.edit = false;
                                financialInfo.approve = false;
                                financialInfo.reject = false;
                                $scope.instFinancialInfos.push(financialInfo);
                            }

                        });
                        if ($scope.instFinancialInfos.length == 0) {
                            $state.go('instGenInfo.instFinancialInfo.new');
                        }
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstFinancialInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instFinancialInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instFinancialInfo = {
                        bankName: null,
                        branchName: null,
                        accountType: null,
                        accountNo: null,
                        issueDate: null,
                        expireDate: null,
                        amount: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*instFinancialInfo-dialog.controller.js*/

angular.module('stepApp').controller('InstFinancialInfoDialogController',
    ['$scope', 'BankSetup', '$rootScope', '$stateParams', '$state', 'entity', 'InstFinancialInfo', '$q', 'Division', 'DistrictsByDivision', 'UpazilasByDistrict', 'BankBranchsByUpazilaAndBank', 'InstFinancialInfoOne', '$timeout',
        function ($scope, BankSetup, $rootScope, $stateParams, $state, entity, InstFinancialInfo, $q, Division, DistrictsByDivision, UpazilasByDistrict, BankBranchsByUpazilaAndBank, InstFinancialInfoOne, $timeout) {

            $scope.instFinancialInfo = entity;
            $scope.bankSetups = BankSetup.query();
            $scope.divisions = Division.query();
            //$scope.division = {};
            $scope.districts = [];
            //$scope.district = {};
            $scope.upazilas = [];
            //$scope.upazila = {};
            $scope.bankBranchs = [];
            $scope.hideAddMore = true;
            $scope.instFinancialInfos = entity;

            $scope.toDay = new Date();

            $rootScope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $rootScope.calendar.opened[which] = true;
                }
            };
            $scope.updatedDistrict = function (division) {
                DistrictsByDivision.query({page: $scope.page, size: 100, id: division.id}, function (result) {
                    $scope.districts = result;
                    $scope.upazilas = [];
                    $scope.bankBranchs = [];
                });

            };
            $scope.updatedUpazila = function (district) {
                UpazilasByDistrict.query({page: $scope.page, size: 100, id: district.id}, function (result) {
                    $scope.upazilas = result;
                    $scope.bankBranchs = [];
                });

            };
            $scope.updatedBankBranch = function (upazila, bank) {
                BankBranchsByUpazilaAndBank.query({
                    page: $scope.page,
                    size: 100,
                    upazilaId: upazila.id,
                    bankSetupId: bank.id
                }, function (result) {
                    $scope.bankBranchs = result;
                });

            };

            InstFinancialInfoOne.get({id: $stateParams.id}, function (result) {

                $scope.instFinancialInfos = [1];
                $scope.instFinancialInfos[0] = result;
                //$scope.instFinancialInfos[0].bankBranch = result.bankBranch;
                $scope.division = result.bankBranch.upazila.district.division;
                $scope.district = result.bankBranch.upazila.district;
                $scope.districts.push(result.bankBranch.upazila.district);
                $scope.upazila = result.bankBranch.upazila;
                $scope.upazilas.push(result.bankBranch.upazila);
                $scope.bankBranchs.push(result.bankBranch);
                //$scope.updatedDistrict(result.bankBranch.upazila.district.division);
                //  $scope.editForm.issueDate.$invalid = false;
                $scope.deadlineValidation(result.issueDate, result.expireDate);

                $scope.hideAddMore = true;
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);
            });
            $scope.incrementInstGenINfoStatus = true;
            $scope.load = function (id) {
                InstFinancialInfo.get({id: id}, function (result) {
                    $scope.instFinancialInfos = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instFinancialInfoUpdate', result);
                $rootScope.setWarningMessage('stepApp.instFinancialInfo.updated');
                $scope.isSaving = false;
                $state.go('instituteInfo.financialInfo', {}, {reload: true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.addMore = function () {
                $scope.instFinancialInfos.push(
                    {
                        branchName: null,
                        accountType: null,
                        accountNo: null,
                        issueDate: null,
                        expireDate: null,
                        amount: null,
                        status: null,
                        id: null
                    }
                );
                $rootScope.refreshRequiredFields();
            };


            $scope.removeBankInfo = function (bankInfo) {
                var index = $scope.instFinancialInfos.indexOf(bankInfo);
                $scope.instFinancialInfos.splice(index, 1);

            }
            $scope.deadlineValidation = function (issueDate, expireDate) {
                var d1 = Date.parse(expireDate);
                var d2 = Date.parse(issueDate);
                if (d1 < d2) {
                    $scope.dateError = true;
                } else {
                    $scope.dateError = false;
                }
                console.log('-- '+ $scope.dateError);
            };
            $scope.save = function () {
                $scope.isSaving = true;

                $scope.instFinancialInfos.forEach(function (data, i) {
                    if (data.id != null && $scope.preCondition()) {
                        //$scope.incrementInstGenINfoStatus
                        data.status = 0;
                        InstFinancialInfo.update(data, onSaveSuccess, onSaveError);
                        if (data.status == 1) {
                            if ($scope.incrementInstGenINfoStatus) {
                                data.status = 3;
                                $scope.incrementInstGenINfoStatus = false;
                                InstFinancialInfo.update(data, onSaveSuccess, onSaveError);
                                $rootScope.setWarningMessage('stepApp.instFinancialInfo.updated');
                            } else {
                                data.status = 0;
                                InstFinancialInfo.update(data, onSaveSuccess, onSaveError);
                                $rootScope.setWarningMessage('stepApp.instFinancialInfo.updated');
                            }
                        } else {
                            data.status = 0;
                            InstFinancialInfo.update(data, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.instFinancialInfo.updated');
                        }
                    } else if (data.id == null && $scope.preCondition()) {
                        InstFinancialInfo.save(data, onSaveSuccess, onSaveError);
                        data.status = 0;
                        $rootScope.setSuccessMessage('stepApp.instFinancialInfo.created');
                    }
                });
            };
            $scope.clear = function () {
                $scope.instFinancialInfo = null;
            };
            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instFinancialInfo.accountNo', $scope.instFinancialInfos.accountNo, 'text', 20, 5, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instFinancialInfo.amount', $scope.instFinancialInfos.amount, 'number', 1000000, 0, '0-9', true, '', ''));
                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }
        }]);

/*instFinancialInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('InstFinancialInfoDetailController',
        ['$scope', '$state', '$q', '$rootScope', '$stateParams', 'entity', 'InstFinancialInfo',
            function ($scope, $state, $q, $rootScope, $stateParams, entity, InstFinancialInfo) {

                InstFinancialInfo.get({id: 0}, function (result) {
                    $scope.instFinancialInfos = result;

                    if ($scope.instFinancialInfos.length <= 0) {
                        $state.go('instituteInfo.financialInfo.new', {}, {reload: true});
                    }

                }, function () {
                    $state.go('instituteInfo.financialInfo.new', {}, {reload: true});
                });

                var unsubscribe = $rootScope.$on('stepApp:instFinancialInfoUpdate', function (event, result) {
                    $scope.instFinancialInfos = result;
                });

                $scope.$on('$destroy', unsubscribe);

            }]);


/*instFinancialInfo-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('InstFinancialInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstFinancialInfo',
            function ($scope, $rootScope, $modalInstance, entity, InstFinancialInfo) {

                $scope.instFinancialInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstFinancialInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.InstFinancialInfo.deleted');
                };

            }]);

/*instFinancialInfo-approve-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstFinancialInfoApproveController',
        ['$scope', '$modalInstance', 'Institute', '$state', 'entity', 'InstFinancialInfo', 'InstFinancialInfoApprove', '$stateParams',
            function ($scope, $modalInstance, Institute, $state, entity, InstFinancialInfo, InstFinancialInfoApprove, $stateParams) {
                $scope.instFinancialInfo = entity;
                var onSaveSuccess = function (result) {
                    $scope.$emit('stepApp:instituteUpdate', result);

                    $modalInstance.close(result);
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmApprove = function () {
                    $scope.isSaving = true;
                    $scope.instFinancialInfo.status = 1;
                    InstFinancialInfoApprove.approve($stateParams.instid, onSaveSuccess, onSaveError);


                };
                var onInstGenInfoSaveSuccess = function (result) {
                    $scope.isInstGenInfoSaving = true;
                    $state.go('instGenInfo.generalinfo');

                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

            }])
    .controller('InstFinancialInfoDeclineController',
        ['$scope', '$modalInstance', 'Institute', '$state', 'InstFinancialInfo', 'InstFinancialInfoDecline', '$stateParams',
            function ($scope, $modalInstance, Institute, $state, InstFinancialInfo, InstFinancialInfoDecline, $stateParams) {
                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.decline = function () {
                    InstFinancialInfoDecline.update({id: $stateParams.instid}, $scope.causeDeny);
                    $modalInstance.close();
                }
            }]);




