'use strict';

angular.module('stepApp')
    .controller('IisCourseInfoController', function ($scope, $state, $modal, IisCourseInfo, IisCourseInfoSearch, ParseLinks, $stateParams, IisCurriculumInfo, CmsTrade, CmsSubject, FindCourseByInstId, IisCourseInfosTempOfCurrentInst) {
        $scope.FindCourseByInstId = {};

        $scope.iisCourseInfos = [];
        $scope.page = 0;
        $scope.loadAll = function () {
            IisCourseInfosTempOfCurrentInst.query({page: $scope.page, size: 200}, function (result, headers) {
                //$scope.links = ParseLinks.parse(headers('link'));
                $scope.iisCourseInfos = result;
                console.log("===================================");
                console.log($scope.iisCourseInfos);
                if ($scope.iisCourseInfos.length < 1) {
                    $state.go('instituteInfo.iisCourseInfo.new', {}, {reload: true});
                }


            });
            /* IisCourseInfo.query({page: $scope.page, size: 20}, function(result, headers) {
             $scope.links = ParseLinks.parse(headers('link'));
             $scope.iisCourseInfos = result;
             console.log("===================================");
             console.log($scope.iisCourseInfos);
             if($scope.iisCourseInfos.length < 1){
             $state.go('instituteInfo.iisCourseInfo.new',{},{reload:true});
             }
             });*/
        };

        FindCourseByInstId.query({page: $scope.page, size: 200}, function (result, headers) {

            $scope.findCourseByInstIds = result;

            console.log("=======================Amanur Rahman=================");
            console.log(result);
            console.log("=======================Amanur Rahman=================");

        });
        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            IisCourseInfoSearch.query({query: $scope.searchQuery}, function (result) {
                $scope.iisCourseInfos = result;
            }, function (response) {
                if (response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.iisCourseInfo = {
                perDateEdu: null,
                perDateBteb: null,
                mpoEnlisted: null,
                dateMpo: null,
                seatNo: null,
                shift: null,
                id: null
            };
        };

        //$scope.iisCourseInfo = entity;
        $scope.iiscurriculuminfos = IisCurriculumInfo.query();
        $scope.cmstrades = CmsTrade.query();
        $scope.cmssubjects = CmsSubject.query();
        // $scope.iisCourseInfo.mpoEnlisted=true;

        $scope.load = function (id) {
            IisCourseInfo.get({id: id}, function (result) {
                $scope.iisCourseInfo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:iisCourseInfoUpdate', result);
            //$modalInstance.close(result);
            $scope.isSaving = false;
            $state.go('instituteInfo.iisCourseInfo', {}, {reload: true});
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.iisCourseInfo.id != null) {
                IisCourseInfo.update($scope.iisCourseInfo, onSaveSuccess, onSaveError);
            } else {
                IisCourseInfo.save($scope.iisCourseInfo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function () {
            //$modalInstance.dismiss('cancel');
        };

    });

/*iisCourseInfo-dialog.controller.js*/

angular.module('stepApp').controller('IisCourseInfoDialogController',
    ['$scope', '$rootScope', '$state', 'InstituteByLogin', '$stateParams', 'entity', 'IisCourseInfo', 'IisCurriculumInfo', 'CmsTrade', 'CmsSubject', 'FindCurriculumByInstId', 'CmsCurriculum', 'IisCurriculumsOfCurrentInstitute', 'CmsSubByCurriculum', 'CmsTradesByCurriculum', 'IisCourseInfoTemp', 'IisCurriculumInfoTempOfCurrentInstitute','DateUtils','Principal','IisCurriculumByInstIdForDept',
        function ($scope, $rootScope, $state, InstituteByLogin, $stateParams, entity, IisCourseInfo, IisCurriculumInfo, CmsTrade, CmsSubject, FindCurriculumByInstId, CmsCurriculum, IisCurriculumsOfCurrentInstitute, CmsSubByCurriculum, CmsTradesByCurriculum, IisCourseInfoTemp, IisCurriculumInfoTempOfCurrentInstitute, DateUtils,Principal,IisCurriculumByInstIdForDept) {

            $scope.iisCourseInfo = {};
            $scope.FindCurriculumByInstId = {};
            $scope.logInInstitute = {};
            $scope.iiscurriculuminfos = IisCurriculumInfo.query();
            $scope.toDay = new Date();
            $rootScope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $rootScope.calendar.opened[which] = true;
                }
            };

            if ($stateParams.id != null) {
                if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                    IisCourseInfoTemp.get({id: $stateParams.id}, function (result) {
                        $scope.iisCourseInfo = result;
                        $scope.iisCourseInfo.shift = result.shift;
                        CmsTradesByCurriculum.query({id: result.cmsTrade.cmsCurriculum.id}, function (data) {
                            $scope.cmstrades = data;
                        });
                    });
                }else if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])){
                    IisCourseInfo.get({id:$stateParams.id}, function(result){
                        $scope.iisCourseInfo = result.iisCourseInfoTemp;
                        IisCurriculumByInstIdForDept.query({instituteId:result.institute.id},function(cmsCurriculums){
                            $scope.cmsCurriculums = cmsCurriculums;
                        });
                        CmsTradesByCurriculum.query({id: result.cmsTrade.cmsCurriculum.id}, function (data) {
                            $scope.cmstrades = data;
                        });
                    });
                }
            }

            if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                $scope.cmsCurriculums = [];
                IisCurriculumInfoTempOfCurrentInstitute.query({}, function (results) {
                    console.log('%%%%%%%%%%%%');
                    console.log(results);
                    angular.forEach(results,function(value){
                        $scope.cmsCurriculums.push(value.cmsCurriculum);
                        console.log(value.cmsCurriculum);
                    });
                });
            }

            InstituteByLogin.query({}, function (result) {
                $scope.logInInstitute = result;
                if ($scope.logInInstitute.type == 'NonGovernment') {
                    $scope.iisCourseInfo.mpoEnlisted = true;
                }
            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:iisCourseInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('instituteInfo.iisCourseInfo', {}, {reload: true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.setMinisMemoFile = function ($file, iisCourseInfo, minisMemoFile, minisMemoFileCntType, minisMemoFileName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            iisCourseInfo[minisMemoFile] = base64Data;
                            iisCourseInfo[minisMemoFileCntType] = $file.type;
                            iisCourseInfo[minisMemoFileName] = $file.name;
                        });
                    };
                }
            };
            $scope.setBtebMemoFile = function ($file, iisCourseInfo, btebMemoFile, btebMemoFileCntType, btebMemoFileName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            iisCourseInfo[btebMemoFile] = base64Data;
                            iisCourseInfo[btebMemoFileCntType] = $file.type;
                            iisCourseInfo[btebMemoFileName] = $file.name;
                        });
                    };
                }
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.iisCourseInfo.id != null && $scope.preCondition()) {
                    if ($scope.logInInstitute.type == 'Government') {
                        $scope.iisCourseInfo.mpoEnlisted = null;
                    }
                    if ($scope.iisCourseInfo.mpoEnlisted == null && $scope.logInInstitute.type == 'Nongovernment') {
                        $scope.iisCourseInfo.mpoEnlisted = true;
                    }
                    if ($scope.iisCourseInfo.noOfPostT != null) {
                        $scope.iisCourseInfo.totalPostT = $scope.iisCourseInfo.noOfPostT;
                    }
                    if ($scope.iisCourseInfo.noOfPostStaff != null) {
                        $scope.iisCourseInfo.totalPostStaff = $scope.iisCourseInfo.noOfPostStaff;
                    }
                    IisCourseInfoTemp.update($scope.iisCourseInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.iisCourseInfo.updated');
                } else if ($scope.iisCourseInfo.id == null && $scope.preCondition()){
                    if ($scope.logInInstitute.type == 'Government') {
                        $scope.iisCourseInfo.mpoEnlisted = null;
                    }
                    if ($scope.iisCourseInfo.mpoEnlisted == null && $scope.logInInstitute.type == 'Nongovernment') {
                        $scope.iisCourseInfo.mpoEnlisted = true;
                    }
                    $scope.iisCourseInfo.institute = $scope.logInInstitute;
                    if ($scope.iisCourseInfo.noOfPostT != null) {
                        $scope.iisCourseInfo.totalPostT = $scope.iisCourseInfo.noOfPostT;
                    }
                    if ($scope.iisCourseInfo.noOfPostStaff != null) {
                        $scope.iisCourseInfo.totalPostStaff = $scope.iisCourseInfo.noOfPostStaff;
                    }
                    IisCourseInfoTemp.save($scope.iisCourseInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.iisCourseInfo.created');
                }
            };


            $scope.clear = function () {
            };

            $scope.setTrades = function (cmsCurriculum) {
                $scope.cmstrades = [];
                console.log('&&&&&&&&&&&&&&77');
                console.log(cmsCurriculum);
                CmsTradesByCurriculum.query({id: cmsCurriculum.id}, function (result) {
                    console.log('^^^^^^^^^^^^^^^^^');
                    console.log(result);
                    $scope.cmstrades = result;
                });
            };

            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.perDateEdu',DateUtils.convertLocaleDateToDMY($scope.iisCourseInfo.perDateEdu), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.memoNoMinistry', $scope.iisCourseInfo.memoNoMinistry, 'text', 30, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.perDateBteb',DateUtils.convertLocaleDateToDMY($scope.iisCourseInfo.perDateBteb), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.memoNo', $scope.iisCourseInfo.memoNo, 'text', 30, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.seatNo', $scope.iisCourseInfo.seatNo, 'number', 10, 1, '0to9', true, '','' ));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }
        }]);

/*iisCourseInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('IisCourseInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'IisCourseInfo', 'IisCurriculumInfo', 'CmsTrade', 'CmsSubject', 'IisCourseInfoTemp',
            function ($scope, $rootScope, $stateParams, entity, IisCourseInfo, IisCurriculumInfo, CmsTrade, CmsSubject, IisCourseInfoTemp) {
                $scope.iisCourseInfo = entity;
                $scope.load = function (id) {
                    IisCourseInfoTemp.get({id: id}, function (result) {
                        $scope.iisCourseInfo = result;
                    });
                };

                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = "" + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

                IisCourseInfo.get({id: $stateParams.id}, function (result) {
                    $scope.iisCourseInfo = result;
                });
                var unsubscribe = $rootScope.$on('stepApp:iisCourseInfoUpdate', function (event, result) {
                    $scope.iisCourseInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*iisCourseInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('IisCourseInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'IisCourseInfo',

            function ($scope, $rootScope, $modalInstance, entity, IisCourseInfo) {
                $scope.iisCourseInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    IisCourseInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.iisCourseInfo.deleted');
                };
            }]);

angular.module('stepApp').controller('IisCourseInfoByAODialogController',
    ['$scope','$modalInstance' ,'$rootScope', '$state', 'InstituteByLogin', '$stateParams', 'entity', 'IisCourseInfo', 'IisCurriculumInfo', 'CmsTrade', 'CmsSubject', 'FindCurriculumByInstId', 'CmsCurriculum', 'IisCurriculumsOfCurrentInstitute', 'CmsSubByCurriculum', 'CmsTradesByCurriculum', 'IisCourseInfoTemp', 'IisCurriculumInfoTempOfCurrentInstitute','DateUtils','Principal','findAllCurriculumByInstituteId','IisCourseInfoByAO','Institute',
        function ($scope,$modalInstance ,$rootScope, $state, InstituteByLogin, $stateParams, entity, IisCourseInfo, IisCurriculumInfo, CmsTrade, CmsSubject, FindCurriculumByInstId, CmsCurriculum, IisCurriculumsOfCurrentInstitute, CmsSubByCurriculum, CmsTradesByCurriculum, IisCourseInfoTemp, IisCurriculumInfoTempOfCurrentInstitute, DateUtils,Principal,findAllCurriculumByInstituteId,IisCourseInfoByAO,Institute) {

            $scope.iisCourseInfo = entity;
            $scope.iisCurriculumInfo = {};
            $scope.cmsCurriculums = [];

            if ($stateParams.iisCourseInfoId != null) {
                if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE']) || Principal.hasAnyAuthority(['ROLE_AD'])){
                    IisCourseInfo.get({id: $stateParams.iisCourseInfoId}, function(result){
                        console.log('******************');
                        console.log(result);
                        $scope.iisCourseInfo = result;
                        $scope.iisCurriculumInfo.cmsCurriculum = result.cmsTrade.cmsCurriculum;
                        $scope.cmsCurriculums.push(result.cmsTrade.cmsCurriculum);
                        $scope.setTrades();
                    });
                }
            }else if($stateParams.instituteId !=null){
                findAllCurriculumByInstituteId.get({id:$stateParams.instituteId},function(cmsCurriculums){
                    $scope.cmsCurriculums = cmsCurriculums;
                    Institute.get({id:$stateParams.instituteId},function(instituteData){
                        $scope.iisCourseInfo = {};
                        $scope.iisCourseInfo.institute = instituteData;
                    });
                    //CmsTradesByCurriculum.query({id: result.cmsTrade.cmsCurriculum.id}, function (data) {
                    //    $scope.cmstrades = data;
                    //});
                });
            }

            //if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
            //    IisCurriculumInfoTempOfCurrentInstitute.query({}, function (result) {
            //        $scope.cmsCurriculums = result;
            //    });
            //}

            //InstituteByLogin.query({}, function (result) {
            //    $scope.logInInstitute = result;
            //    if ($scope.logInInstitute.type == 'NonGovernment') {
            //        $scope.iisCourseInfo.mpoEnlisted = true;
            //    }
            //});
            var onSaveSuccess = function (result) {
                $state.go('mpo.details',null, {reload: true});
                $modalInstance.close(result);
                $modalInstance.dismiss('cancel');
                $('#myModal').hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            };

            var onSaveError = function (result) {
                $state.go('mpo.details', {}, {reload: true});
                $scope.isSaving = false;
                $modalInstance.dismiss('cancel');
                $('#myModal').hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

            };

            $scope.setMinisMemoFile = function ($file, iisCourseInfo, minisMemoFile, minisMemoFileCntType, minisMemoFileName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            iisCourseInfo[minisMemoFile] = base64Data;
                            iisCourseInfo[minisMemoFileCntType] = $file.type;
                            iisCourseInfo[minisMemoFileName] = $file.name;
                        });
                    };
                }
            };
            $scope.setBtebMemoFile = function ($file, iisCourseInfo, btebMemoFile, btebMemoFileCntType, btebMemoFileName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            iisCourseInfo[btebMemoFile] = base64Data;
                            iisCourseInfo[btebMemoFileCntType] = $file.type;
                            iisCourseInfo[btebMemoFileName] = $file.name;
                        });
                    };
                }
            };

            $scope.save = function () {
                $scope.isSaving = true;

                if ($scope.iisCourseInfo.id != null) {
                    IisCourseInfoByAO.save($scope.iisCourseInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.iisCourseInfo.updated');
                } else{
                    IisCourseInfoByAO.save($scope.iisCourseInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.iisCourseInfo.created');
                }
            };


            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
                $state.go('mpo.details');
                $('#myModal').hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            };

            $scope.setTrades = function () {
                CmsTradesByCurriculum.query({id: $scope.iisCurriculumInfo.cmsCurriculum.id}, function (result) {
                    $scope.cmstrades = result;
                });
            };

            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.perDateEdu',DateUtils.convertLocaleDateToDMY($scope.iisCourseInfo.perDateEdu), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.memoNoMinistry', $scope.iisCourseInfo.memoNoMinistry, 'text', 30, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.perDateBteb',DateUtils.convertLocaleDateToDMY($scope.iisCourseInfo.perDateBteb), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.memoNo', $scope.iisCourseInfo.memoNo, 'text', 30, 1, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('iisCourseInfo.seatNo', $scope.iisCourseInfo.seatNo, 'number', 10, 1, '0to9', true, '','' ));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }
        }]);
