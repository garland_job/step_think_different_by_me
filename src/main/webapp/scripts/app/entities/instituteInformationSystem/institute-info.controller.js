'use strict';

angular.module('stepApp')
    .controller('InstituteInfoController',
        ['$scope', '$location', 'instituteList', 'Principal', 'InstGenInfoByStatus', 'ParseLinks', 'InstituteByLogin',
            function ($scope, $location, instituteList, Principal, InstGenInfoByStatus, ParseLinks, InstituteByLogin) {

                Principal.identity().then(function (account) {
                    $scope.account = account;
                });

                $scope.instCurriculums = [];
                $scope.instCources = [];
                $scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.currentPageNumber = 'Page Number ' + 1;

                $scope.pageChangeHandler = function (num) {
                    $scope.currentPageNumber = 'Page Number ' + num;
                };

                InstituteByLogin.query({}, function (result) {
                    $scope.logInInstitute = result;
                });

                $scope.location = $location.path();
                $scope.instGenInfos = [];
                $scope.page = 0;
                $scope.showapprove = false;
                $scope.loadAll = function () {
                    InstGenInfoByStatus.query({
                        status: instituteList,
                        page: $scope.page,
                        size: 1000
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instGenInfos = result;
                        if ($scope.instGenInfos[0].status === 1) {
                            $scope.showapprove = true;
                        }
                        $scope.total = headers('x-total-count');
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

            }]);
angular.module('stepApp')
    .controller('InstituteInfoListController',
        ['$scope', '$location', 'instituteList', 'Principal', 'InstGenInfoByStatus', 'ParseLinks', 'InstituteByLogin', 'Institute', 'GovernmentInstitutes', 'NonGovernmentInstitutes',
            function ($scope, $location, instituteList, Principal, InstGenInfoByStatus, ParseLinks, InstituteByLogin, Institute, GovernmentInstitutes, NonGovernmentInstitutes) {

                Principal.identity().then(function (account) {
                    $scope.account = account;
                });
                $scope.govInst = false;
                $scope.nonGovInst = false;
                $scope.currentPages = 1;
                InstituteByLogin.query({}, function (result) {
                    $scope.logInInstitute = result;
                });


                Institute.query({size: 10000}, function (result) {
                    $scope.instGenInfos = result;

                });
                GovernmentInstitutes.query({size: 10000}, function (result) {
                    $scope.govtInstitute = result;


                });
                NonGovernmentInstitutes.query({size: 10000}, function (result) {
                    $scope.nongovtInstitute = result;

                });
                $scope.showGov = function () {
                    $scope.govInst = true;
                    $scope.nonGovInst = false;
                };
                $scope.showNonGov = function () {
                    $scope.govInst = false;
                    $scope.nonGovInst = true;

                };


            }]);
angular.module('stepApp')
    .controller('InstitutePendingInfoController',
        ['$scope', '$location', 'instituteList', 'Principal', 'InstGenInfoByStatus', 'ParseLinks', 'InstituteByLogin', 'InstGenInfosPendingInfoList',
            function ($scope, $location, instituteList, Principal, InstGenInfoByStatus, ParseLinks, InstituteByLogin, InstGenInfosPendingInfoList) {

                Principal.identity().then(function (account) {
                    $scope.account = account;
                });

                InstituteByLogin.query({}, function (result) {
                    $scope.logInInstitute = result;
                });

                $scope.location = $location.path();
                $scope.instGenInfos = [];
                $scope.page = 0;
                $scope.showapprove = false;
                $scope.loadAll = function () {
                    InstGenInfosPendingInfoList.query({page: $scope.page, size: 2000}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instGenInfos = result;
                        if ($scope.instGenInfos[0].status === 1) {
                            $scope.showapprove = true;
                        }
                        $scope.total = headers('x-total-count');
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

            }]);
angular.module('stepApp')
    .controller('InstituteInfoRejectedListController',
        ['$scope', '$location', 'instituteList', 'Principal', 'InstGenInfoByStatus', 'ParseLinks', 'InstGenInfosRejectedList',
            function ($scope, $location, instituteList, Principal, InstGenInfoByStatus, ParseLinks, InstGenInfosRejectedList) {

                Principal.identity().then(function (account) {
                    $scope.account = account;
                });

                $scope.location = $location.path();
                $scope.instGenInfos = [];
                $scope.page = 0;
                $scope.showapprove = false;
                $scope.loadAll = function () {
                    InstGenInfosRejectedList.query({page: $scope.page, size: 1000}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instGenInfos = result;


                        if ($scope.instGenInfos[0].status === 1) {
                            $scope.showapprove = true;
                        }
                        $scope.total = headers('x-total-count');
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

            }])
    .controller('InstituteMpoListController',
    ['$scope', '$location', 'instituteList', 'Principal', 'InstGenInfoByStatus', 'ParseLinks', 'InstGenInfosRejectedList', 'MpoListedInstitutes', 'InstituteByInstLevelName',
        function ($scope, $location, instituteList, Principal, InstGenInfoByStatus, ParseLinks, InstGenInfosRejectedList, MpoListedInstitutes, InstituteByInstLevelName) {

            Principal.identity().then(function (account) {
                $scope.account = account;
            });

            $scope.location = $location.path();
            $scope.instGenInfos = [];
            $scope.page = 0;
            $scope.showapprove = false;
            $scope.loadAll = function () {
                MpoListedInstitutes.query({}, function (result) {
                    $scope.instGenInfos = result;

                });
            };
            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

        }]).controller('LevelWiseInstitutesController',
    ['$scope', '$location', 'instituteList', 'Principal', 'InstGenInfoByStatus', 'ParseLinks', 'InstituteByInstLevelName', '$stateParams',
        function ($scope, $location, instituteList, Principal, InstGenInfoByStatus, ParseLinks, InstituteByInstLevelName, $stateParams) {
            $scope.institutes = [];
            $scope.page = 0;
            $scope.showapprove = false;
            $scope.loadAll = function () {
                InstituteByInstLevelName.query({name: $stateParams.levelName}, function (result) {
                    $scope.institutes = result;

                });
            };
            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

        }])
    .controller('InstituteInfoApproveController',
        ['$scope', '$rootScope', '$stateParams', 'Principal', 'instituteList', 'InstGenInfo', 'InstituteAllInfo', 'AllInstInfraInfo', 'InstituteAllInfosTemp', 'InstituteStatusByInstitute', 'InstInfraInfoTempAll',
            function ($scope, $rootScope, $stateParams, Principal, instituteList, InstGenInfo, InstituteAllInfo, AllInstInfraInfo, InstituteAllInfosTemp, InstituteStatusByInstitute, InstInfraInfoTempAll) {

                // Previously only general info approval was applicable, now each tab need to approve. So, When init check status of all items tab and do action

                //alert($stateParams.id);

                $scope.instituteStatus = {};
                $scope.updateGen = false;
                $scope.newCur = false;
                $scope.newCourse = false;
                $scope.newAdm = false;
                $scope.newFinance = false;
                $scope.newCount = false;
                $scope.updateAdm = false;
                $scope.updateCur = false;
                $scope.updateFinance = false;
                $scope.updateCount = false;
                $scope.updateCourse = false;

                InstGenInfo.get({id: $stateParams.id}, function (result) {
                    if (result.institute != null) {
                        InstituteStatusByInstitute.get({id: result.institute.id}, function (instStatus) {
                            $scope.instituteStatus = instStatus;
                        });
                    }

                    // Initialization of TAB
                    $scope.instGenInfo = result;
                    $scope.generalInfoTab = false;
                    $scope.stuffInfoTab = false;
                    $scope.academicInfoTab = false;
                    $scope.financialInfoTab = false;
                    $scope.infrastructureInfoTab = false;
                    $scope.courseInfoTab = false;
                    $scope.curriculumInfoTab = false;
                    $scope.adminInfoTab = false;
                    $scope.hideGenInfoApprove = true;
                    $scope.hideAdmInfoApprove = false;
                    $scope.hideAcaInfoApprove = false;
                    $scope.hideStuffInfoApprove = false;
                    $scope.hideFinnInfoApprove = false;
                    $scope.hideInfraInfoApprove = false;
                    $scope.hideCurriculumInfoApprove = false;
                    $scope.hideCourseInfoApprove = false;
                    $scope.successMsg = $rootScope.setSuccessMessage;

                    if ($scope.instGenInfo.type == 'Government') {
                        $scope.instGenInfo.mpoEnlisted = false;
                        $scope.hidempoEnlisted = true;
                    }

                    if ($scope.instGenInfo.mpoEnlisted == true) {
                        $scope.mpoListed = 'Yes';
                    }
                    else {
                        $scope.mpoListed = 'No';
                    }

                    $scope.previewImage = function (content, contentType, name) {
                        var blob = $rootScope.b64toBlob(content, contentType);
                        $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                        $rootScope.viewerObject.contentType = contentType;
                        $rootScope.viewerObject.pageTitle = "" + name;
                        // call the modal
                        $rootScope.showFilePreviewModal();
                    };
                    if ($scope.instGenInfo.status != null && $scope.instGenInfo.status > 0) {
                        //AllInstInfraInfo.get({institueid: $scope.instGenInfo.institute.id},function(result){
                        InstInfraInfoTempAll.get({institueid: $scope.instGenInfo.institute.id}, function (result) {
                            $scope.instShopInfos = result.instShopInfoList;
                            $scope.instLabInfos = result.instLabInfoList;
                            $scope.instPlayGroundInfos = result.instPlayGroundInfoList;
                            $scope.instBuildings = result.instBuildingInfoList;
                        });
                    }

                    if ($scope.instGenInfo.status == null || $scope.instGenInfo.status == 0 || $scope.instGenInfo.status == 2) {
                        if ($scope.instGenInfo.status == null) {
                            $scope.instGenInfo.status = 0;
                        }
                        $scope.hideGenInfoApprove = false;
                    }

                    //alert('Hi Institute');
                    if (result.institute) {
                        // Previous used -> InstituteAllInfo
                        InstituteAllInfosTemp.get({id: $scope.instGenInfo.institute.id}, function (res) {
                            $scope.instGenInfo = res.instGenInfo;
                            if ($scope.instGenInfo.status == 2) {
                                $scope.updateGen = true;
                            }
                            $scope.instAdmInfo = res.instAdmInfo;
                            if ($scope.instAdmInfo != null) {
                                if ($scope.instAdmInfo.status == null || $scope.instAdmInfo.status == 0) {
                                    $scope.newAdm = true;
                                }
                                if ($scope.instAdmInfo.status == 3) {
                                    $scope.updateAdm = true;
                                }
                            }
                            $scope.insAcademicInfo = res.insAcademicInfo;
                            $scope.instEmpCount = res.instEmpCount;
                            if ($scope.instEmpCount != null) {
                                if ($scope.instEmpCount.status == 0) {
                                    $scope.newCount = true;
                                }
                                if ($scope.instEmpCount.status == 3) {
                                    $scope.updateCount = true;
                                }
                            }

                            $scope.instFinancialInfos = res.instFinancialInfo;
                            if ($scope.instFinancialInfos != null) {
                                angular.forEach($scope.instFinancialInfos, function (financialInfo) {

                                    if (financialInfo.status == 0) {
                                        $scope.newFinance = true;
                                    }
                                    if (financialInfo.status == 4) {
                                        $scope.updateFinance = true;
                                    }
                                });
                            }


                            $scope.instInfraInfo = res.instInfraInfo;
                            $scope.instGovernBodys = res.instGovernBody;
                            $scope.instCurriculums = res.instCurriculums;
                            if ($scope.instCurriculums != null) {
                                angular.forEach($scope.instCurriculums, function (curriculum) {
                                    if (curriculum.status == 0) {
                                        $scope.newCur = true;
                                    }
                                    if (curriculum.status == 3) {
                                        $scope.updateCur = true;
                                    }
                                });
                            }


                            $scope.instCources = res.instCourses;
                            if ($scope.instCources != null) {
                                angular.forEach($scope.instCources, function (course) {
                                    if (course.status == null || course.status == 0) {
                                        $scope.newCourse = true;
                                    }
                                    if (course.status == 3) {
                                        $scope.updateCourse = true;
                                    }
                                });
                            }

                            $scope.instituteAllInfo = res;
                            // Temp data test
                            if ($scope.instituteAllInfo.instGenInfo == null) {
                                $scope.generalInfoTab = true;
                            } else {
                                if ($scope.instGenInfo.status == 0) {
                                    $scope.hideGenInfoApprove = true;
                                }
                            }
                            if ($scope.instituteAllInfo.instAdmInfo == null) {
                                $scope.adminInfoTab = true;
                            } else {
                                if ($scope.instAdmInfo.status == 1 || $scope.instAdmInfo.status == 2) {
                                    $scope.hideAdmInfoApprove = true;
                                }
                            }
                            if ($scope.instituteAllInfo.insAcademicInfo == null) {
                                $scope.academicInfoTab = true;
                            } else {
                                if ($scope.insAcademicInfo.status == 1 || $scope.insAcademicInfo.status == 2) {
                                    $scope.hideAcaInfoApprove = true;
                                }
                            }

                            if ($scope.instCources.length == 0) {
                                $scope.courseInfoTab = true;
                            } else {
                                if ($scope.instCources[0].status == 1 || $scope.instCources[0].status == 2) {
                                    $scope.hideCourseInfoApprove = true;
                                }
                            }

                            if ($scope.instCurriculums.length == 0) {
                                $scope.curriculumInfoTab = true;
                            } else {
                                if ($scope.instCurriculums[0].status == 1 || $scope.instCurriculums[0].status == 2) {
                                    $scope.hideCurriculumInfoApprove = true;
                                }
                            }
                            if ($scope.instituteAllInfo.instEmpCount == null) {
                                $scope.stuffInfoTab = true;
                            } else {
                                if ($scope.instEmpCount.status == 1 || $scope.instEmpCount.status == 2) {
                                    $scope.hideStuffInfoApprove = true;
                                }
                            }
                            if ($scope.instituteAllInfo.instFinancialInfo.length === 0) {
                                $scope.financialInfoTab = true;
                            } else {
                                if ($scope.instFinancialInfos[0].status == 1 || $scope.instFinancialInfos[0].status == 2) {
                                    $scope.hideFinnInfoApprove = true;
                                }
                            }
                            if ($scope.instituteAllInfo.instInfraInfo == null) {
                                $scope.infrastructureInfoTab = true;
                            } else {
                                if ($scope.instInfraInfo.status == 1 || $scope.instInfraInfo.status == 2) {
                                    $scope.hideInfraInfoApprove = true;
                                }
                            }
                        });
                    }
                    else {
                        $scope.generalInfoTab = false;
                        $scope.stuffInfoTab = true;
                        $scope.academicInfoTab = true;
                        $scope.financialInfoTab = true;
                        $scope.infrastructureInfoTab = true;
                        $scope.adminInfoTab = true;
                    }
                });
            }])


    .controller('InstituteInfoDetailViewController',
        ['$scope', '$rootScope', '$stateParams', 'Principal', 'instituteList', 'InstGenInfo', 'InstituteAllInfo', 'AllInstInfraInfo', 'InstituteAllInfosTemp', 'InstituteStatusByInstitute', 'InstInfraInfoTempAll', 'InstBuildingsByInstitute', 'InstLabsByInstitute', 'InstPlayGroundsByInstitute', 'InstShopsByInstitute','InstGovernBodyTempByInstitute',
            function ($scope, $rootScope, $stateParams, Principal, instituteList, InstGenInfo, InstituteAllInfo, AllInstInfraInfo, InstituteAllInfosTemp, InstituteStatusByInstitute, InstInfraInfoTempAll, InstBuildingsByInstitute, InstLabsByInstitute, InstPlayGroundsByInstitute, InstShopsByInstitute,InstGovernBodyTempByInstitute) {

                // Previously only general info approval was applicable, now each tab need to approve. So, When init check status of all items tab and do action

                //alert($stateParams.id);
                $scope.govtInstitute = [];
                $scope.instId = $stateParams.id;

                InstBuildingsByInstitute.query({instituteId: $stateParams.id}, function (result1) {
                        $scope.instBuildings = result1;
                    }
                );
                InstLabsByInstitute.query({instituteId: $stateParams.id}, function (result2) {
                        $scope.instLabInfos = result2;
                    }
                );
                InstPlayGroundsByInstitute.query({instituteId: $stateParams.id}, function (result3) {
                        $scope.instPlayGroundInfos = result3;

                    }
                );
                InstShopsByInstitute.query({instituteId: $stateParams.id}, function (result4) {
                        $scope.instShopInfos = result4;
                    }
                );

                //InstituteAllInfosTemp.get({id: $stateParams.id}, function (result) {
                //    $scope.instGovernBodys = result.instGovernBody;
                //});

                // Institute.get({id:$stateParams.id}, function (instResult) {
                //     $scope.institute = instResult;
                //     console.log('=================Suharta==============');
                //     console.log(instResult);
                //     console.log('=================Suharta==============');
                // });


                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = " " + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };


                InstituteAllInfo.get({id: $stateParams.id}, function (res) {

                    $scope.instGenInfo = res.instGenInfo;
                    $scope.instAdmInfo = res.instAdmInfo;
                    $scope.insAcademicInfo = res.insAcademicInfo;
                    $scope.instEmpCount = res.instEmpCount;
                    $scope.instFinancialInfos = res.instFinancialInfo;
                    $scope.instInfraInfo = res.instInfraInfo;
                    //   $scope.instBuildings=InstBuildingsByInstitute.query({instituteId:res.institute.id});
                    $scope.instGovernBodys = res.instGovernBody;
                    $scope.instCurriculums = res.instCurriculums;
                    $scope.instCources = res.instCourses;
                    $scope.instituteAllInfo = res;
                    InstGovernBodyTempByInstitute.query({instituteId:$stateParams.id} ,function(result){
                        $scope.instGovernBodys = result;
                    });

                    if ($scope.instituteAllInfo.instGenInfo == null) {
                        $scope.generalInfoTab = true;
                    } else {
                        if ($scope.instGenInfo.status == 0) {
                            $scope.hideGenInfoApprove = true;
                        }
                    }
                    if ($scope.instituteAllInfo.instAdmInfo == null) {

                        $scope.adminInfoTab = true;
                    } else {
                        if ($scope.instAdmInfo.status == 1 || $scope.instAdmInfo.status == 2) {
                            $scope.hideAdmInfoApprove = true;
                        }
                    }
                    if ($scope.instituteAllInfo.insAcademicInfo == null) {
                        $scope.academicInfoTab = true;
                    } else {
                        if ($scope.insAcademicInfo.status == 1 || $scope.insAcademicInfo.status == 2) {
                            $scope.hideAcaInfoApprove = true;
                        }
                    }

                    if ($scope.instCources.length == 0) {
                        $scope.courseInfoTab = true;
                    } else {
                        if ($scope.instCources[0].status == 1 || $scope.instCources[0].status == 2) {
                            $scope.hideCourseInfoApprove = true;
                        }
                    }

                    if ($scope.instCurriculums.length == 0) {
                        $scope.curriculumInfoTab = true;
                    } else {
                        if ($scope.instCurriculums[0].status == 1 || $scope.instCurriculums[0].status == 2) {
                            $scope.hideCurriculumInfoApprove = true;
                        }
                    }
                    if ($scope.instituteAllInfo.instEmpCount == null) {
                        $scope.stuffInfoTab = true;

                    } else {
                        if ($scope.instEmpCount.status == 1 || $scope.instEmpCount.status == 2) {
                            $scope.hideStuffInfoApprove = true;
                        }
                    }
                    if ($scope.instituteAllInfo.instFinancialInfo.length === 0) {
                        $scope.financialInfoTab = true;
                    } else {
                        if ($scope.instFinancialInfos[0].status == 1 || $scope.instFinancialInfos[0].status == 2) {
                            $scope.hideFinnInfoApprove = true;
                        }
                    }
                    if ($scope.instituteAllInfo.instInfraInfo == null) {
                        $scope.infrastructureInfoTab = true;
                    } else {
                        if ($scope.instInfraInfo.status == 1 || $scope.instInfraInfo.status == 2) {
                            $scope.hideInfraInfoApprove = true;
                        }
                    }
                });


            }]);

angular.module('stepApp')
    .controller('CurriculumMpoListController',
        ['$scope', '$location', 'instituteList', 'Principal', 'IisCurriculumsMpoEnlisted', 'ParseLinks',
            function ($scope, $location, instituteList, Principal, IisCurriculumsMpoEnlisted, ParseLinks) {
                $scope.location = $location.path();
                $scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.currentPageNumber = 'Page Number ' + 1;

                $scope.pageChangeHandler = function (num) {
                    $scope.currentPageNumber = 'Page Number ' + num;
                };
                $scope.iisCurriculumInfos = [];
                IisCurriculumsMpoEnlisted.query({page: 0, size: 2000}, function (result) {
                    $scope.iisCurriculumInfos = result;
                });


            }]);

/*mpoEnlistedCourse.controller.js*/

angular.module('stepApp')
    .controller('CourseMpoListController',
        ['$scope', '$location', 'instituteList', 'Principal', 'MpoEnlistedIISCourses', 'ParseLinks',
            function ($scope, $location, instituteList, Principal, MpoEnlistedIISCourses, ParseLinks) {
                $scope.location = $location.path();
                $scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.currentPageNumber = 'Page Number ' + 1;

                $scope.pageChangeHandler = function (num) {
                    $scope.currentPageNumber = 'Page Number ' + num;
                };
                $scope.iisCourseInfos = [];
                MpoEnlistedIISCourses.query({page: 0, size: 2000}, function (result) {
                    $scope.iisCourseInfos = result;
                });


            }]);

/*institute-dashboard.controller.js*/
angular.module('stepApp')
    .controller('InstituteDashboardController',
        ['$scope', 'Principal', 'InstituteByLogin',
            function ($scope, Principal, InstituteByLogin) {

                InstituteByLogin.query({}, function (result) {
                    $scope.logInInstitute = result;
                });
            }]);

/*inst-jobplacement-officer.controller.js*/

angular.module('stepApp')
    .controller('InstituteInfoJobPlacementOfficerController',
        ['$scope', '$state', '$modal', 'InsAcademicInfo', 'InsAcademicInfoSearch', 'Principal', 'ParseLinks', 'JpAdminsByInstituteId', 'InstituteByLogin',
            function ($scope, $state, $modal, InsAcademicInfo, InsAcademicInfoSearch, Principal, ParseLinks, JpAdminsByInstituteId, InstituteByLogin) {


                $scope.instEmployees = [];
                $scope.page = 0;
                InstituteByLogin.query({}, function (result) {
                    JpAdminsByInstituteId.query({id: result.id}, function (result2) {
                        $scope.instEmployees = result2;
                    });
                });

                $scope.search = function () {
                    InsAcademicInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.insAcademicInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.insAcademicInfo = {
                        counselorName: null,
                        curriculum: null,
                        totalTechTradeNo: null,
                        tradeTechDetails: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*inst-code-chage-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstMpoCodeChangeDialogController',
        ['$scope', '$stateParams', '$rootScope', '$modalInstance', '$state', 'Institute', 'InstVacancyGeneralRole',
            function ($scope, $stateParams, $rootScope, $modalInstance, $state, Institute, InstVacancyGeneralRole) {

                $scope.institute = {};
                Institute.get({id: $stateParams.id}, function (result) {
                    $scope.institute = result;
                });
                $scope.confirmApprove = function () {
                    if ($scope.institute.id != null) {
                        Institute.update($scope.institute, onSaveSuccess, onSaveError);
                    }
                };
                var onSaveSuccess = function (result) {
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('MPO Code Updated Successfully.');
                    $state.go('instituteInfo.mpoEnlistedInst', null, {reload: true});
                };
                var onSaveError = function (result) {
                };
                $scope.clear = function () {
                    $modalInstance.close();
                    window.history.back();
                }
            }]);

/*curriculum-mpo-date-chage-dialog.controller.js*/

angular.module('stepApp')
    .controller('CurriculumMpoDateChangeDialogController',
        ['$scope', '$stateParams', '$rootScope', '$modalInstance', '$state', 'IisCurriculumInfo', 'IisCurriculumInfoMpo', 'InstVacancyGeneralRole',
            function ($scope, $stateParams, $rootScope, $modalInstance, $state, IisCurriculumInfo, IisCurriculumInfoMpo, InstVacancyGeneralRole) {

                $scope.iisCurriculumInfo = {};

                IisCurriculumInfo.get({id: $stateParams.id}, function (result) {
                    $scope.iisCurriculumInfo = result;
                });

                $scope.confirmApprove = function () {
                    if ($scope.iisCurriculumInfo.id != null) {
                        IisCurriculumInfoMpo.update($scope.iisCurriculumInfo, onSaveSuccess, onSaveError);
                    }
                };

                var onSaveSuccess = function (result) {

                    if (result.vacancyAssigned == null || result.vacancyAssigned == false) {
                        InstVacancyGeneralRole.save(result);
                        $modalInstance.close();
                        $state.go('instituteInfo.mpoEnlistedCurriculum', null, {reload: true});
                        $rootScope.setSuccessMessage('MPO Date Updated Successfully.');
                    } else {
                        $modalInstance.close();
                        $rootScope.setErrorMessage('MPO General Vacancy already assigned');
                        $state.go('instituteInfo.mpoEnlistedCurriculum', null, {reload: true});
                    }
                };

                var onSaveError = function (result) {
                };
                $scope.clear = function () {
                    $modalInstance.close();
                    window.history.back();

                }

            }]);

/*course-mpo-date-chage-dialog.controller.js*/
angular.module('stepApp')
    .controller('CourseMpoDateChangeDialogController',
        ['$scope', '$stateParams', '$rootScope', '$modalInstance', '$state', 'IisCourseInfo', 'InstVacancySpecialRole',
            function ($scope, $stateParams, $rootScope, $modalInstance, $state, IisCourseInfo, InstVacancySpecialRole) {

                $scope.iisCourseInfo = {};

                IisCourseInfo.get({id: $stateParams.id}, function (result) {
                    $scope.iisCourseInfo = result;
                });

                $scope.confirmApprove = function () {
                    if ($scope.iisCourseInfo.id != null) {
                        $scope.iisCourseInfo.vacancyRoleApplied = false;
                        IisCourseInfo.update($scope.iisCourseInfo, onSaveSuccess, onSaveError);
                    }
                };

                var onSaveSuccess = function (result) {


                    if (result.vacancyRoleApplied == null || result.vacancyRoleApplied == false) {
                        InstVacancySpecialRole.apply(result);
                        $modalInstance.close();
                        $rootScope.setSuccessMessage('MPO Date Updated Successfully.');
                        $state.go('instituteInfo.mpoEnlistedCourse', null, {reload: true});
                    } else {
                        $modalInstance.close();
                        $rootScope.setErrorMessage('MPO Specialization Vacancy already Created');
                        $state.go('instituteInfo.mpoEnlistedCourse', null, {reload: true});
                    }
                };

                var onSaveError = function (result) {
                };
                $scope.clear = function () {
                    $modalInstance.close();
                    window.history.back();

                }

            }]);


angular.module('stepApp').controller('MpoActiveInactiveController',
    ['$scope', '$stateParams', '$rootScope', '$modalInstance', '$state', 'entity', 'Institute', 'Principal', 'User', 'DateUtils', 'MpoEnlistingActiveInactive',
        function ($scope, $stateParams, $rootScope, $modalInstance, $state, entity, Institute, Principal, User, DateUtils, MpoEnlistingActiveInactive) {

            $scope.instGenInfo = entity;
            $scope.activeStatus = {};

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:hrDepartmentHeadInfoUpdate', result);
                $scope.isSaving = false;
            };

            if($stateParams.activeStatus == true){
                $scope.activeStatus = false;
            }else{
                $scope.activeStatus = true;
            }
            $scope.save = function () {
                MpoEnlistingActiveInactive.update({instId: $stateParams.id, stat: $stateParams.activeStatus});
                $modalInstance.close();
                $state.go('instituteInfo.detailView', null, {reload: true});
                $rootScope.setWarningMessage('stepApp.institute.updated');
            };
            $scope.clear = function () {
                $modalInstance.close();
                $state.go('instituteInfo.detailView', null, {reload: true});
            };
        }]);
//['ngAnimate', 'ngTouch', 'ui.grid', 'ui.grid.importer', 'ui.grid.rowEdit', 'ui.grid.edit'

    angular.module('stepApp')
    .controller('MainCtrl', ['$scope','$rootScope' ,'$http', '$interval', '$q', 'Institute','ImportInstGenInfo',
        function ($scope,$rootScope ,$http, $interval, $q,Institute,ImportInstGenInfo) {
        $scope.data = [];
        $rootScope.institutes = [];
        //$scope.institute = {};

        $scope.gridOptions = {
            enableGridMenu: true,
            importerDataAddCallback: function( grid, newObjects ) {
                $scope.data = $scope.data.concat( newObjects );
            },
            onRegisterApi: function(gridApi){
                $scope.gridApi = gridApi;
                gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
            },
            data: 'data'
        };

        $scope.saveRow = function(rowEntity) {
            console.log('%%%%%%%%%%%%%%%%%%');
            console.log(rowEntity);
            $scope.institute = {};
            $scope.institute.name = rowEntity.NAME;
            $scope.institute.code = rowEntity.CODE;
            $scope.institute.type = rowEntity.TYPE;
            $scope.institute.dateOfEstablishment = rowEntity.DATE_OF_ESTB;
            $scope.institute.firstAffiliationDate = rowEntity.FIRST_AFFILIATION_DATE;
            $scope.institute.mpoCode = rowEntity.MPO_CODE;
            $scope.institute.postOffice = rowEntity.POST_OFFICE;
            $scope.institute.mobile = rowEntity.MOBILE;
            $scope.institute.upazila  = rowEntity.UPAZILA;
            $scope.institute.email=rowEntity.EMAIL;
            $scope.institute.instCategory = rowEntity.INST_LEVEL;
            $scope.institute.instLevel = rowEntity.INST_CATEGORY;
            $scope.institute.mpoEnlisted = rowEntity.MPO_ENLISTED;


            //$scope.institute.type = rowEntity.TYPE;
            //$scope.institute.type = rowEntity.TYPE;
            //$scope.institute.type = rowEntity.TYPE;
            //$scope.institute.type = rowEntity.TYPE;
            $rootScope.institutes.push($scope.institute);
            console.log($rootScope.institutes);
            // create a fake promise - normally you'd use the promise returned by $http or $resource
            var promise = $q.defer();
            //Institute.save($scope.institute);
            $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );
            console.log('**************');
            console.log($scope.institute);
            // fake a delay of 3 seconds whilst the save occurs, return error if gender is "male"
            //$interval( function() {
            //    if (rowEntity.Gender === 'male' ){
            //        promise.reject();
            //    } else {
            //        promise.resolve();
            //    }
            //}, 3000, 1);
        };

        var handleFileSelect = function( event ){
            var target = event.srcElement || event.target;

            if (target && target.files && target.files.length === 1) {
                var fileObject = target.files[0];
                $scope.gridApi.importer.importFile( fileObject );
                target.form.reset();
            }
        };

        var fileChooser = document.querySelectorAll('.file-chooser');

        if ( fileChooser.length !== 1 ){
            console.log('Found > 1 or < 1 file choosers within the menu item, error, cannot continue');
        } else {
            fileChooser[0].addEventListener('change', handleFileSelect, false);  // TODO: why the false on the end?  Google
        }

        $scope.importAll = function(){
            console.log('&&&&&&&&&&&&&&&&&&');
            console.log($rootScope.institutes);
            ImportInstGenInfo.import($rootScope.institutes);
            //console.log($scope.institutes);
        };


    }]);

