'use strict';

angular.module('stepApp')
    .controller('InstVacancyController', function ($scope, $state, $modal, InstVacancy, InstVacancySearch, ParseLinks, InstVacancysCurrentInst, $stateParams, InstVacancysByInstitute, Principal) {

        $scope.instVacancys = [];
        $scope.page = 0;
        $scope.generalVacancyList = [];
        $scope.tradeVacancyList = [];
        $scope.totalGeneralVacancy = 0;
        $scope.totalTradeVacancy = 0;

        if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
            InstVacancysByInstitute.query({instituteId: $stateParams.instituteId}, function (result) {
                $scope.instVacancys = result;
            });
        } else {
            InstVacancysCurrentInst.query({}, function (result) {
                $scope.instVacancys = result;
                angular.forEach($scope.instVacancys, function (vacancy) {
                    if (vacancy.cmsTrade == null) {
                        $scope.generalVacancyList.push(vacancy);
                        $scope.totalGeneralVacancy = $scope.totalGeneralVacancy + vacancy.totalVacancy;
                    } else {
                        $scope.tradeVacancyList.push(vacancy);
                        $scope.totalTradeVacancy = $scope.totalTradeVacancy + vacancy.totalVacancy;
                        //angular.forEach($scope.tradeVacancyList, function (tradevancy) {
                        //    tradeVacancy=tradeVacancy+tradevancy.totalVacancy;
                        //});

                    }
                });

                //$scope.tottalVccancy=totalvacancy-tradeVacancy;
                //$scope.tottalTradeVccancy=tradeVacancy;


                //console.log("t v");
                //console.log(totalvacancy);

            });
        }


        $scope.search = function () {
            InstVacancySearch.query({query: $scope.searchQuery}, function (result) {
                $scope.instVacancys = result;
            }, function (response) {
                if (response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instVacancy = {
                dateCreated: null,
                dateModified: null,
                status: null,
                empType: null,
                totalVacancy: null,
                filledUpVacancy: null,
                id: null
            };
        };
    });

/*instVacancy-dialog.controller.js*/

angular.module('stepApp').controller('InstVacancyDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'InstVacancy', 'Institute', 'CmsTrade', 'CmsSubject', 'InstEmplDesignation', 'IisTradesOfCurrentInst', 'CmsSubAssignByTrade', '$state', 'ActiveInstEmplDesignationByType', 'Principal', 'IisTradesByInstitute',
        function ($scope, $rootScope, $stateParams, entity, InstVacancy, Institute, CmsTrade, CmsSubject, InstEmplDesignation, IisTradesOfCurrentInst, CmsSubAssignByTrade, $state, ActiveInstEmplDesignationByType, Principal, IisTradesByInstitute) {

            $scope.instVacancy = entity;

            if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                IisTradesByInstitute.query({id: $stateParams.instituteId}, function (result) {
                    $scope.cmstrades = result;
                });
            } else {
                $scope.cmstrades = IisTradesOfCurrentInst.query();
            }
            $scope.cmsSubjects = [];
            $scope.instempldesignations = ActiveInstEmplDesignationByType.query({type: 'Employee'});
            $scope.load = function (id) {
                InstVacancy.get({id: id}, function (result) {
                    $scope.instVacancy = result;
                });
            };

            var onSaveSuccess = function (result) {
                if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                    $state.go('instVacancyAdmin', {}, {reload: true});
                } else {
                    $state.go('instVacancy', {}, {reload: true});
                }

                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instVacancy.id != null) {
                    InstVacancy.update($scope.instVacancy, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.instVacancy.updated');
                } else {
                    if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                        Institute.get({id: $stateParams.instituteId}, function (result) {
                            $scope.instVacancy.institute = result;
                            InstVacancy.save($scope.instVacancy, onSaveSuccess, onSaveError);
                        });

                    } else {
                        InstVacancy.save($scope.instVacancy, onSaveSuccess, onSaveError);
                    }

                    $rootScope.setSuccessMessage('stepApp.instVacancy.created');
                }
            };

            $scope.setSubjects = function (id) {
                $scope.cmsSubjects = CmsSubAssignByTrade.query({id: id}, function (result) {
                });
            };

            $scope.clear = function () {
            };
        }]);

/*instVacancy-detail.controller.js*/

angular.module('stepApp')
    .controller('InstVacancyDetailController', function ($scope, $rootScope, $stateParams, entity, InstVacancy, Institute, CmsTrade, CmsSubject, InstEmplDesignation) {
        $scope.instVacancy = entity;
        $scope.load = function (id) {
            InstVacancy.get({id: id}, function (result) {
                $scope.instVacancy = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:instVacancyUpdate', function (event, result) {
            $scope.instVacancy = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

/*instVacancy-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstVacancyDeleteController', function ($scope, $rootScope, $modalInstance, entity, InstVacancy) {

        $scope.instVacancy = entity;
        $scope.clear = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            InstVacancy.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
            $rootScope.setErrorMessage('stepApp.InstVacancy.deleted');
        };

    });


