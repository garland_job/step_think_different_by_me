'use strict';

angular.module('stepApp')
    .controller('InstCategoryController',
        ['$scope', '$state', '$modal', 'InstCategory', 'InstCategorySearch', 'ParseLinks',
            function ($scope, $state, $modal, InstCategory, InstCategorySearch, ParseLinks) {

                $scope.instCategorys = [];
                $scope.predicate = 'id';
                $scope.reverse = false;
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstCategory.query({page: $scope.page, size: 1000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instCategorys = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstCategorySearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instCategorys = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instCategory = {
                        code: null,
                        name: null,
                        description: null,
                        pStatus: null,
                        createdDate: null,
                        updatedDate: null,
                        createdBy: null,
                        updatedBy: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*instCategory-dialog.controller.js*/

angular.module('stepApp').controller('InstCategoryDialogController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'InstCategory', 'InstLevelBytype',
        function ($scope, $rootScope, $state, $stateParams, entity, InstCategory, InstLevelBytype) {

            $scope.instCategory = entity;
            $scope.instCategory.pStatus = true;
            $scope.load = function (id) {
                InstCategory.get({id: id}, function (result) {
                    $scope.instCategory = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instCategoryUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('setup.instCategory', {}, {reload: true});
            };

            $scope.instGenInfoType = function (name, type) {

                InstLevelBytype.get({name: name, level: type}, function (result) {
                        $scope.res = result;
                        if ($scope.res.id != null) {
                            $scope.dataDuplicate = "This Data is already Exist"
                        } else {
                            $scope.dataDuplicate = ""

                        }

                    }
                );
            }


            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instCategory.id != null && $scope.preCondition()) {
                    InstCategory.update($scope.instCategory, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instCategory.updated');
                } else if($scope.instCategory.id == null && $scope.preCondition()){
                    InstCategory.save($scope.instCategory, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instCategory.created');
                }
            };

            $scope.clear = function () {
                $scope.instCategory = null;
            };
            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instCategory.name', $scope.instCategory.name, 'text', 150, 2, 'atozAndAtoZ', true, '', ''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            };
        }]);


/*instCategory-detail.controller.js*/
angular.module('stepApp')
    .controller('InstCategoryDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstCategory',
            function ($scope, $rootScope, $stateParams, entity, InstCategory) {
                $scope.instCategory = entity;
                $scope.load = function (id) {
                    InstCategory.get({id: id}, function (result) {
                        $scope.instCategory = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instCategoryUpdate', function (event, result) {
                    $scope.instCategory = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*instCategory-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstCategoryDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstCategory',
            function ($scope, $rootScope, $modalInstance, entity, InstCategory) {

                $scope.instCategory = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstCategory.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });

                };

            }]);


