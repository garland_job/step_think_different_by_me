'use strict';

angular.module('stepApp')
    .controller('InstEmpCountController',
        ['$scope', '$state', '$modal', 'InstEmpCount', 'InstEmpCountSearch', 'ParseLinks', 'IisCourseInfosTempOfCurrentInst', 'InstituteAllInfo', 'InstituteByLogin',
            function ($scope, $state, $modal, InstEmpCount, InstEmpCountSearch, ParseLinks, IisCourseInfosTempOfCurrentInst, InstituteAllInfo, InstituteByLogin) {

                $scope.instEmpCounts = [];
                $scope.logInInstitute = {};
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmpCount.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmpCounts = result;
                        if ($scope.instEmpCounts.length == 0) {
                            $state.go('instGenInfo.instEmpCount.new');
                        }
                    });
                };

                InstituteByLogin.query({}, function (result) {
                    $scope.logInInstitute = result;

                    InstituteAllInfo.get({id: $scope.logInInstitute.id}, function (res) {

                        $scope.iisCourseInfos = res.instCourses;


                    });
                });


                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmpCountSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmpCounts = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmpCount = {
                        totalMaleTeacher: null,
                        totalFemaleTeacher: null,
                        totalMaleEmployee: null,
                        totalFemaleEmployee: null,
                        totalGranted: null,
                        totalEngaged: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*instEmpCount-dialog.controller.js*/

angular.module('stepApp').controller('InstEmpCountDialogController',
    ['$scope', '$rootScope', '$stateParams', '$q', '$state', 'entity', 'InstEmpCount', 'Institute',
        function ($scope, $rootScope, $stateParams, $q, $state, entity, InstEmpCount, Institute) {

            $scope.instEmpCount = entity;
            $scope.showMessage = false;
            $scope.flagData = $scope.instEmpCount.totalGranted;

            InstEmpCount.get({id: $stateParams.id}, function (result) {
                $scope.instEmpCount = result;


                $scope.count1 = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher;

                $scope.count2 = $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;

                $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher + $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;

            });

            $scope.totalGranted = function (data) {
                if (data > $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher + $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee) {
                    $scope.instEmpCount.totalGranted = $scope.flagData;
                    $scope.showMessage = true;
                }
                else {
                    $scope.flagData = data;
                    $scope.showMessage = false;
                }

            };

            $scope.totalTeach1 = function () {
                $scope.count1 = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher;
                $scope.count2 = $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;
                $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher + $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;

            };


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmpCountUpdate', result);
                $scope.isSaving = false;
                $state.go('instituteInfo.stuffInfo', {}, {reload: true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $state.go('instGenInfo.instEmpCount');
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instEmpCount.id != null) {
                    if ($scope.instEmpCount.status == 1) {
                        $scope.instEmpCount.status = 3;
                        $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher + $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;
                        InstEmpCount.update($scope.instEmpCount, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.instEmpCount.updated');

                    } else {
                        $scope.instEmpCount.status = 0;
                        $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher + $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;
                        InstEmpCount.update($scope.instEmpCount, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.instEmpCount.updated');
                    }

                } else {
                    $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalMaleTeacher + $scope.instEmpCount.totalFemaleTeacher + $scope.instEmpCount.totalMaleEmployee + $scope.instEmpCount.totalFemaleEmployee;
                    InstEmpCount.save($scope.instEmpCount, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmpCount.created');
                }
            };

            $scope.clear = function () {
                $scope.instEmpCount = null;
            };
        }]);
/*instEmpCount-detail.controller.js*/

angular.module('stepApp')
    .controller('InstEmpCountDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstEmpCount',
            function ($scope, $rootScope, $stateParams, entity, InstEmpCount) {

                InstEmpCount.get({id: 0}, function (result) {
                    $scope.instEmpCount = result;
                });


                var unsubscribe = $rootScope.$on('stepApp:instEmpCountUpdate', function (event, result) {
                    $scope.instEmpCount = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*instEmpCount-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstEmpCountDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstEmpCount',
            function ($scope, $rootScope, $modalInstance, entity, InstEmpCount) {

                $scope.instEmpCount = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmpCount.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.instEmpCount.deleted');

                };

            }]);

/*instEmpCountInfo-approve-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstEmpCountApproveController',
        ['$scope', '$q', '$modalInstance', 'Institute', '$state', 'entity', 'InstEmpCount', 'InstituteEmpCount', 'InstituteEmpCountByInstitute',
            function ($scope, $q, $modalInstance, Institute, $state, entity, InstEmpCount, InstituteEmpCount, InstituteEmpCountByInstitute) {
                $scope.instEmpCount = entity;
                $scope.tempInstituteEmpCount = entity;

                $q.all([$scope.instEmpCount.$promise]).then(function () {
                    if ($scope.instEmpCount.institute != null) {
                        InstituteEmpCountByInstitute.get({id: $scope.instEmpCount.institute.id}, function (response) {
                            $scope.instituteEmpCount = response;
                        });
                    }
                    else {
                        $scope.instituteEmpCount = null;
                    }

                    return $scope.instEmpCount.$promise;
                });


                var onSaveSuccess = function (result) {

                    $modalInstance.close(result);
                    $scope.isSaving = false;
                    $state.go('instituteInfo.approve', {}, {reload: true});
                };
                var onSaveInstituteEmpCountSave = function (result) {


                    if ($scope.instituteEmpCount != null) {
                        $scope.tempInstituteEmpCount.id = $scope.instituteEmpCount.id;
                        InstituteEmpCount.update($scope.tempInstituteEmpCount, onSaveSuccess, onSaveError);
                    }
                    else {
                        $scope.instEmpCount.id = null;
                        InstituteEmpCount.save($scope.tempInstituteEmpCount, onSaveSuccess, onSaveError);
                    }


                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                    $state.go('instituteInfo.approve', {}, {reload: true});
                };
                $scope.confirmApprove = function () {
                    $scope.isSaving = true;
                    $scope.instEmpCount.status = 1;
                    InstEmpCount.update($scope.instEmpCount, onSaveInstituteEmpCountSave, onSaveError);


                };
                var onInstGenInfoSaveSuccess = function (result) {
                    $scope.isInstGenInfoSaving = true;
                    $state.go('instGenInfo.InstEmpCount');

                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

            }])
    .controller('InstEmpCountDeclineController',
        ['$scope', '$stateParams', '$modalInstance', 'Institute', '$state', 'entity', 'InstEmpCount', 'InstEmpCountDecline',
            function ($scope, $stateParams, $modalInstance, Institute, $state, entity, InstEmpCount, InstEmpCountDecline) {
                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.decline = function () {
                    InstEmpCountDecline.update({id: $stateParams.id}, $scope.causeDeny);
                    $modalInstance.close();
                }
            }]);




