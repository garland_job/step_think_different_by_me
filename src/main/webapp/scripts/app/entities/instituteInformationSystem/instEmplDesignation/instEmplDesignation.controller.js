'use strict';

angular.module('stepApp')
    .controller('InstEmplDesignationController',
        ['$scope', '$state', '$modal', 'InstEmplDesignation', 'InstEmplDesignationSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstEmplDesignation, InstEmplDesignationSearch, ParseLinks) {

                $scope.instEmplDesignations = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmplDesignation.query({page: $scope.page, size: 1000}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmplDesignations = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmplDesignationSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmplDesignations = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmplDesignation = {
                        code: null,
                        name: null,
                        description: null,
                        type: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*instEmplDesignation-dialog.controller.js*/

angular.module('stepApp').controller('InstEmplDesignationDialogController',
    ['$scope', '$state', '$rootScope', '$stateParams', 'entity', 'InstEmplDesignation', 'InstCategory', 'InstLevel', 'InstDesignationByName', 'InstDesignationByCode',
        function ($scope, $state, $rootScope, $stateParams, entity, InstEmplDesignation, InstCategory, InstLevel, InstDesignationByName, InstDesignationByCode) {

            $scope.instEmplDesignation = {};
            $scope.instEmplDesignation.status = true;

            InstEmplDesignation.get({id: $stateParams.id}, function (result) {
                $scope.instEmplDesignation = result;
            }, function (response) {
                if (response.status == 404) {
                    $scope.instEmplDesignation.status = true;

                }

            });
            $scope.instCategories = InstCategory.query();
            $scope.instLevels = InstLevel.query();
            $scope.load = function (id) {

            };

            InstDesignationByName.get({name: $scope.instEmplDesignation.name}, function (instEmplDesignation) {

                $scope.message = "The  Name is already existed.";
            });
            InstDesignationByCode.get({code: $scope.instEmplDesignation.code}, function (instEmplDesignation) {

                $scope.message = "The  Code is already existed.";
            });
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmplDesignationUpdate', result);
                /*
                 $modalInstance.close(result);
                 */
                $state.go('setup.instEmplDesignation', {}, {reload: true});
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instEmplDesignation.id != null) {
                    InstEmplDesignation.update($scope.instEmplDesignation, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmplDesignation.updated');
                } else {
                    InstEmplDesignation.save($scope.instEmplDesignation, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmplDesignation.created');
                }
            };

            $scope.clear = function () {
                $state.go('setup.instEmplDesignation', null, {reload: true});
            };
        }]);

/*instEmplDesignation-detail.controller.js*/

angular.module('stepApp')
    .controller('InstEmplDesignationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstEmplDesignation',
            function ($scope, $rootScope, $stateParams, entity, InstEmplDesignation) {
                $scope.instEmplDesignation = entity;
                $scope.load = function (id) {
                    InstEmplDesignation.get({id: id}, function (result) {
                        $scope.instEmplDesignation = result;
                    });
                };


                var unsubscribe = $rootScope.$on('stepApp:instEmplDesignationUpdate', function (event, result) {
                    $scope.instEmplDesignation = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*instEmplDesignation-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('InstEmplDesignationDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstEmplDesignation',
            function ($scope, $rootScope, $modalInstance, entity, InstEmplDesignation) {

                $scope.instEmplDesignation = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmplDesignation.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            window.history.back();
                        });
                    $rootScope.setErrorMessage('stepApp.InstEmplDesignation.deleted');
                };

            }]);



