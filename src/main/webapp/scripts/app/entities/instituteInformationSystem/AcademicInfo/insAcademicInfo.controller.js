'use strict';

angular.module('stepApp')
    .controller('InsAcademicInfoController',
        ['$scope', '$state', '$modal', 'InsAcademicInfo', 'InsAcademicInfoSearch', 'Principal', 'ParseLinks',
            function ($scope, $state, $modal, InsAcademicInfo, InsAcademicInfoSearch, Principal, ParseLinks) {


                $scope.insAcademicInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InsAcademicInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        angular.forEach(result, function (academicInfo) {
                            if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                                academicInfo.edit = false;
                                academicInfo.approve = true;
                                academicInfo.reject = true;
                                if (academicInfo.status != 1) {
                                    $scope.insAcademicInfos.push(academicInfo);
                                }
                            } else if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                                academicInfo.edit = true;
                                academicInfo.approve = false;
                                academicInfo.reject = false;
                                $scope.insAcademicInfos.push(academicInfo);
                            } else {
                                academicInfo.edit = false;
                                academicInfo.approve = false;
                                academicInfo.reject = false;
                                $scope.insAcademicInfos.push(academicInfo);
                            }

                        });
                        if ($scope.insAcademicInfos.length == 0) {
                            $state.go('instGenInfo.insAcademicInfo.new');
                        }
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InsAcademicInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.insAcademicInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.insAcademicInfo = {
                        counselorName: null,
                        curriculum: null,
                        totalTechTradeNo: null,
                        tradeTechDetails: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*insAcademicInfo-dialog.controller.js*/

angular.module('stepApp').controller('InsAcademicInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', '$q', 'entity', 'InsAcademicInfo', 'Institute', 'InstGenInfo',
        function ($scope, $rootScope, $stateParams, $state, $q, entity, InsAcademicInfo, Institute, InstGenInfo) {

            $scope.insAcademicInfo = entity;
            $scope.insAcademicInfo.InstGenInfo = InstGenInfo.get({id: 0});


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:insAcademicInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('instituteInfo.academicInfo', {}, {reload: true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.insAcademicInfo.id != null) {
                    if ($scope.insAcademicInfo.status == 1) {
                        $scope.insAcademicInfo.status = 3
                        InsAcademicInfo.update($scope.insAcademicInfo, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.insAcademicInfo.updated');
                    } else {
                        $scope.insAcademicInfo.status = 0;
                        InsAcademicInfo.update($scope.insAcademicInfo, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.insAcademicInfo.updated');
                    }
                } else {
                    $scope.insAcademicInfo.status = 0;
                    InsAcademicInfo.save($scope.insAcademicInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.insAcademicInfo.created');
                }
            };

            $scope.clear = function () {
                $scope.insAcademicInfo = null;
            };
        }]);


/* insAcademicInfo-detail.controller.js*/
angular.module('stepApp')
    .controller('InsAcademicInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InsAcademicInfo', 'Institute',
            function ($scope, $rootScope, $stateParams, entity, InsAcademicInfo, Institute) {
                $scope.insAcademicInfo = entity;
                $scope.load = function (id) {
                    InsAcademicInfo.get({id: id}, function (result) {
                        $scope.insAcademicInfo = result;
                    });
                    $scope.$on('$destroy', unsubscribe);
                }
            }]);

/*insAcademicInfo-delete-dialog.controller.js*/


angular.module('stepApp')
    .controller('InsAcademicInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InsAcademicInfo',
            function ($scope, $modalInstance, entity, InsAcademicInfo) {

                $scope.insAcademicInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InsAcademicInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*insAcademicInfo-approve-dialog.controller.js*/
angular.module('stepApp')
    .controller('InsAcademicInfoApproveController',
        ['$scope', '$location', '$rootScope', '$modalInstance', 'Institute', '$state', 'InsAcademicInfo', '$stateParams',
            function ($scope, $location, $rootScope, $modalInstance, Institute, $state, InsAcademicInfo, $stateParams) {
                $scope.insAcademicInfo = InsAcademicInfo.get({id: $stateParams.acaid});

                var onSaveSuccess = function (result) {

                    $scope.isSaving = false;
                    $modalInstance.close(result);


                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.confirmApprove = function () {
                    $scope.isSaving = true;
                    $scope.insAcademicInfo.status = 1;

                    onSaveSuccess($scope.insAcademicInfo);
                    InsAcademicInfo.update($scope.insAcademicInfo, onSaveSuccess, onSaveError);


                };
                var onInstGenInfoSaveSuccess = function (result) {
                    $scope.isInstGenInfoSaving = true;
                    $state.go('instituteInfo.pendingInstituteList');

                };

                var onInstGenInfoSaveError = function (result) {
                    $scope.isInstGenInfoSaving = false;

                };

            }])
    .controller('InsAcademicInfoDeclineController',
        ['$scope', '$state', '$modalInstance', '$location', '$rootScope', '$stateParams', 'InstituteAcademicInfoDecline',
            function ($scope, $state, $modalInstance, $location, $rootScope, $stateParams, InstituteAcademicInfoDecline) {
                $scope.clear = function () {
                    $modalInstance.close();
                };
                $scope.decline = function () {
                    InstituteAcademicInfoDecline.update({id: $stateParams.id}, $scope.causeDeny);
                    $modalInstance.close();
                }
            }]);

