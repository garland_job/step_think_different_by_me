'use strict';

angular.module('stepApp')
    .controller('InstLevelController',
        ['$scope', '$state', '$modal', 'InstLevel', 'InstLevelSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstLevel, InstLevelSearch, ParseLinks) {

                $scope.instLevels = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstLevel.query({page: $scope.page, size: 1000}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instLevels = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstLevelSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instLevels = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instLevel = {
                        code: null,
                        name: null,
                        description: null,
                        pStatus: null,
                        createdDate: null,
                        updatedDate: null,
                        createdBy: null,
                        updatedBy: null,
                        status: null,
                        id: null
                    };
                };
            }]);
/*instLevel-dialog.controller.js*/

angular.module('stepApp').controller('InstLevelDialogController',
    ['$scope', '$state', '$rootScope', '$stateParams', 'entity', 'InstLevel', 'InstLevelByName', 'InstLevelByCode', 'InstCategoryActive', 'InstCategoryByStatusAndType', 'InstLevelBytype',
        function ($scope, $state, $rootScope, $stateParams, entity, InstLevel, InstLevelByName, InstLevelByCode, InstCategoryActive, InstCategoryByStatusAndType, InstLevelBytype) {

            $scope.instLevel = {};
            $scope.instLevel.pStatus = true;

            // $scope.load = function (id) {
                InstLevel.get({id: $stateParams.id}, function (result) {
                    $scope.instLevel = result;
                    console.log("levelmmmmmmmmmmmm");
                    console.log($scope.instLevel);

                    if ($scope.instLevel.type != null) {
                        InstCategoryByStatusAndType.query({activeStatus: 1, instType: $scope.instLevel.type}, function (result) {
                            $scope.instCategories = result;
                        });

                    }
                });
            // };



                $scope.instGenInfoType = function (data) {
                InstCategoryByStatusAndType.query({activeStatus: 1, instType: data}, function (result) {
                    $scope.instCategories = result;
                });
            }


            InstLevelByName.get({name: $scope.instLevel.name}, function (instLevel) {

                $scope.message = "The  Name is already existed.";
            });
            InstLevelByCode.get({code: $scope.instLevel.code}, function (instLevel) {

                $scope.message = "The  Code is already existed.";
            });
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instLevelUpdate', result);

                $state.go('setup.instLevel', {}, {reload: true});

                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instLevel.id != null && $scope.preCondition()) {
                    InstLevel.update($scope.instLevel, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instLevel.updated');
                } else if ($scope.instLevel.id == null && $scope.preCondition()) {
                    InstLevel.save($scope.instLevel, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instLevel.created');
                }
            };

            $scope.clear = function () {
                $state.go('setup.instLevel', null, {reload: true});
            };
            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instLevel.name', $scope.instLevel.name, 'text', 50, 5, 'atozAndAtoZ().-', false, '', ''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }

        }]);

/*instLevel-detail.controller.js*/
angular.module('stepApp')
    .controller('InstLevelDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstLevel',
            function ($scope, $rootScope, $stateParams, entity, InstLevel) {
                $scope.instLevel = entity;
                $scope.load = function (id) {
                    InstLevel.get({id: id}, function (result) {
                        $scope.instLevel = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instLevelUpdate', function (event, result) {
                    $scope.instLevel = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*instLevel-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstLevelDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstLevel',
            function ($scope, $rootScope, $modalInstance, entity, InstLevel) {

                $scope.instLevel = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstLevel.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });

                };

            }]);


