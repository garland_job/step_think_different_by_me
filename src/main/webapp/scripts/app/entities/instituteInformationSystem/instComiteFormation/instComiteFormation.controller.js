'use strict';

angular.module('stepApp')
    .controller('InstComiteFormationController',
        ['$scope', '$state', '$modal', 'InstComiteFormation', 'InstComiteFormationSearch', 'ParseLinks', 'InstComiteFormationByCurrentInst',
            function ($scope, $state, $modal, InstComiteFormation, InstComiteFormationSearch, ParseLinks, InstComiteFormationByCurrentInst) {

                $scope.instComiteFormations = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    // InstComiteFormation.query({page: $scope.page, size: 20}, function(result, headers) {
                    InstComiteFormationByCurrentInst.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instComiteFormations = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstComiteFormationSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instComiteFormations = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instComiteFormation = {
                        comiteName: null,
                        comiteType: null,
                        address: null,
                        timeFrom: null,
                        timeTo: null,
                        formationDate: null,
                        id: null
                    };
                };
            }]);


/*instComiteFormation-dialog.controller.js*/
angular.module('stepApp').controller('InstComiteFormationDialogController',
    ['$scope', '$rootScope', '$state', '$filter', '$stateParams', 'entity', 'InstComiteFormation', 'InstMemShip', 'CurrentInstCommitteeMemberByEmail',
        function ($scope, $rootScope, $state, $filter, $stateParams, entity, InstComiteFormation, InstMemShip, CurrentInstCommitteeMemberByEmail) {

            $scope.instComiteFormation = entity;

            $scope.toDay = new Date();
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
            $scope.instmemships = InstMemShip.query();
            $scope.load = function (id) {
                InstComiteFormation.get({id: id}, function (result) {
                    $scope.instComiteFormation = result;
                });
            };
           /* //TODO: Need to format am/pm in Tomorrows
            $scope.instComiteFormation.timeFrom = new Date('Thu Jan 01 1970 01:00:00 GMT+0600 (+06)');
            $scope.instComiteFormation.timeTo = new Date('Thu Jan 01 1970 13:59:00 GMT+0600 (+06)');
*/
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instComiteFormationUpdate', result);
                $scope.isSaving = false;
                $state.go('instituteInfo.instComiteFormation', {}, {reload: true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                //$scope.instComiteFormation.timeFrom = $filter('date')($scope.instComiteFormation.timeFrom, 'HH:mm aa');
                //$scope.instComiteFormation.timeTo = $filter('date')($scope.instComiteFormation.timeTo, 'HH:mm aa');
                console.log($scope.instComiteFormation.timeFrom);
                console.log($scope.instComiteFormation.timeTo);
                $scope.isSaving = true;
                if ($scope.instComiteFormation.id != null) {
                    InstComiteFormation.update($scope.instComiteFormation, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.instComiteFormation.updated');
                } else {
                    InstComiteFormation.save($scope.instComiteFormation, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instComiteFormation.created');
                }
            };


            $scope.clear = function () {
            };
        }]);
/*instComiteFormation-detail.controller.js*/

angular.module('stepApp')
    .controller('InstComiteFormationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstComiteFormation', 'InstMemShip',
            function ($scope, $rootScope, $stateParams, entity, InstComiteFormation, InstMemShip) {
                $scope.instComiteFormation = entity;
                $scope.load = function (id) {
                    InstComiteFormation.get({id: id}, function (result) {
                        $scope.instComiteFormation = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instComiteFormationUpdate', function (event, result) {
                    $scope.instComiteFormation = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*instComiteFormation-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstComiteFormationDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstComiteFormation',
            function ($scope, $modalInstance, entity, InstComiteFormation) {

                $scope.instComiteFormation = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstComiteFormation.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.InstComiteFormation.deleted');
                };

            }]);

/*instComiteFormation-assignMember.controller.js*/


angular.module('stepApp').controller('InstComiteFormationAssignMemsController',
    ['$scope', '$state', '$stateParams', 'entity', 'InstComiteFormation', 'InstMemShip', 'CurrentInstCommitteeMemberByEmail', 'InstComiteFormationWith', 'InstComiteFormationWithCommittee',
        function ($scope, $state, $stateParams, entity, InstComiteFormation, InstMemShip, CurrentInstCommitteeMemberByEmail, InstComiteFormationWith, InstComiteFormationWithCommittee) {


            InstComiteFormationWithCommittee.get({id: $stateParams.id}, function (result) {
                $scope.instComiteFormation = result;
            });
            $scope.personEmail = null;


            $scope.searchCommittee2 = function searchCommittee2() {
                CurrentInstCommitteeMemberByEmail.get({email: $scope.personEmail}, function (result) {
                    $scope.instMemShip = result;
                });

            };
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instComiteFormationUpdate', result);
                $scope.isSaving = false;
                $state.go('instituteInfo.instComiteFormation', {}, {reload: true});

            };
            $scope.addMember = function addMember() {

                InstComiteFormationWith.update({id: $scope.instComiteFormation.id}, $scope.instMemShip, onAddMember);


            };

            var onAddMember = function (result) {
                location.reload();
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instComiteFormation.id != null) {
                    InstComiteFormation.update($scope.instComiteFormation, onSaveSuccess, onSaveError);
                } else {
                    InstComiteFormation.save($scope.instComiteFormation, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function () {
                //  $modalInstance.dismiss('cancel');
            };
        }]);




