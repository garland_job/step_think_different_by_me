'use strict';

angular.module('stepApp')
    .controller('InstGovBodyAccessController', function ($scope, $state, $modal, InstGovBodyAccess, InstGovBodyAccessSearch, ParseLinks, InstGovBodyAccessCurrentInstitute) {

        $scope.instGovBodyAccesss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            InstGovBodyAccessCurrentInstitute.get({}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.instGovBodyAccesss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            InstGovBodyAccessSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.instGovBodyAccesss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instGovBodyAccess = {
                dateCreated: null,
                dateModified: null,
                status: null,
                id: null
            };
        };
    });
/*instGovBodyAccess-dialog.controller.js*/


angular.module('stepApp').controller('InstGovBodyAccessDialogController',
    ['$scope', '$stateParams', '$q', 'entity', 'InstGovBodyAccess', 'Institute', 'User','Auth',
        function($scope, $stateParams, $q, entity, InstGovBodyAccess, Institute, User, Auth) {

            $scope.instGovBodyAccess = entity;
            $scope.institutes = Institute.query();
            $scope.users = User.query();
            $scope.load = function(id) {
                InstGovBodyAccess.get({id : id}, function(result) {
                    $scope.instGovBodyAccess = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instGovBodyAccessUpdate', result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instGovBodyAccess.id != null) {
                    InstGovBodyAccess.update($scope.instGovBodyAccess, onSaveSuccess, onSaveError);
                } else {
                    $scope.instGovBodyAccess.user.firstName = $scope.instGovBodyAccess.fullName;
                    $scope.instGovBodyAccess.user.langKey = "en";
                    $scope.instGovBodyAccess.user.activated = true;
                    $scope.instGovBodyAccess.user.password = "123456";
                    $scope.instGovBodyAccess.user.authorities = ["ROLE_INSTGOVBODY"];
                    Auth.createAccount($scope.instGovBodyAccess.user).then(function (res) {
                        $scope.instGovBodyAccess.user = res;
                        InstGovBodyAccess.save($scope.instGovBodyAccess, onSaveSuccess, onSaveError);
                    }).catch(function (response) {
                        $scope.success = null;
                        if (response.status === 400 && response.data === 'login already in use') {
                            $scope.errorUserExists = 'ERROR';
                        } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                            $scope.errorEmailExists = 'ERROR';
                        } else {
                            $scope.error = 'ERROR';
                        }
                    });

                }
            };

            $scope.clear = function() {

            };
        }]);
/*instGovBodyAccess-detail.controller.js*/
angular.module('stepApp')
    .controller('InstGovBodyAccessDetailController', function ($scope, $rootScope, $stateParams, entity, InstGovBodyAccess, Institute, User) {
        $scope.instGovBodyAccess = entity;
        $scope.load = function (id) {
            InstGovBodyAccess.get({id: id}, function(result) {
                $scope.instGovBodyAccess = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:instGovBodyAccessUpdate', function(event, result) {
            $scope.instGovBodyAccess = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

/*instGovBodyAccess-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('InstGovBodyAccessDeleteController', function($scope, $modalInstance, entity, InstGovBodyAccess) {

        $scope.instGovBodyAccess = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            InstGovBodyAccess.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });

