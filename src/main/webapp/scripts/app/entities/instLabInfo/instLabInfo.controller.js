'use strict';

angular.module('stepApp')
    .controller('InstLabInfoController',
    ['$scope', '$state', '$modal', 'InstLabInfo', 'InstLabInfoSearch', 'ParseLinks',
    function ($scope, $state, $modal, InstLabInfo, InstLabInfoSearch, ParseLinks) {

        $scope.instLabInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            InstLabInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.instLabInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            InstLabInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.instLabInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instLabInfo = {
                nameOrNumber: null,
                buildingNameOrNumber: null,
                length: null,
                width: null,
                totalBooks: null,
                id: null
            };
        };
    }]);

/* instLabInfo-dialog.controller.js */
angular.module('stepApp').controller('InstLabInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'InstLabInfo', 'InstInfraInfo',
        function($scope, $stateParams, $modalInstance, entity, InstLabInfo, InstInfraInfo) {

            $scope.instLabInfo = entity;
            $scope.instinfrainfos = InstInfraInfo.query();
            $scope.load = function(id) {
                InstLabInfo.get({id : id}, function(result) {
                    $scope.instLabInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instLabInfoUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instLabInfo.id != null) {
                    InstLabInfo.update($scope.instLabInfo, onSaveSuccess, onSaveError);
                } else {
                    InstLabInfo.save($scope.instLabInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/* instLabInfo-detail.controller.js */

angular.module('stepApp')
    .controller('InstLabInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstLabInfo', 'InstInfraInfo',
            function ($scope, $rootScope, $stateParams, entity, InstLabInfo, InstInfraInfo) {
                $scope.instLabInfo = entity;
                $scope.load = function (id) {
                    InstLabInfo.get({id: id}, function(result) {
                        $scope.instLabInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instLabInfoUpdate', function(event, result) {
                    $scope.instLabInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/* instLabInfo-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('InstLabInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstLabInfo',
            function($scope, $modalInstance, entity, InstLabInfo) {

                $scope.instLabInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstLabInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);
