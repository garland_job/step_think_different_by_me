'use strict';

angular.module('stepApp')
    .controller('AssetTypeSetupController',
    ['$scope', '$rootScope', 'AssetTypeSetup', 'AssetTypeSetupSearch', 'ParseLinks',
    function ($scope, $rootScope, AssetTypeSetup, AssetTypeSetupSearch, ParseLinks) {
        $scope.assetTypeSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetTypeSetup.query({page: $scope.page, size: 200}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetTypeSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetTypeSetup.get({id: id}, function(result) {
                $scope.assetTypeSetup = result;
                $('#deleteAssetTypeSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetTypeSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetTypeSetup.deleted');
                    $('#deleteAssetTypeSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetTypeSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetTypeSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetTypeSetup = {
                typeName: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);


/*-------Starting--------assetTypeSetup.js-----------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetTypeSetup', {
                parent: 'assetManagements',
                url: '/assetTypeSetups',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTypeSetup.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTypeSetup/assetTypeSetups.html',
                        controller: 'AssetTypeSetupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTypeSetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetTypeSetup.detail', {
                parent: 'assetTypeSetup',
                url: '/assetTypeSetup/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTypeSetup.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTypeSetup/assetTypeSetup-detail.html',
                        controller: 'AssetTypeSetupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTypeSetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetTypeSetup', function($stateParams, AssetTypeSetup) {
                        return AssetTypeSetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetTypeSetup.new', {
                parent: 'assetTypeSetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTypeSetup.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTypeSetup/assetTypeSetup-dialog.html',
                        controller: 'AssetTypeSetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTypeSetup');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            typeName: null,
                            description: null,
                            status: true,
                            id: null
                        };
                    }
                }
            })

            .state('assetTypeSetup.edit', {
                parent: 'assetTypeSetup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTypeSetup.detail.title'
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTypeSetup/assetTypeSetup-dialog.html',
                        controller: 'AssetTypeSetupDialogController'
                    }
                },



                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTypeSetup');
                        return $translate.refresh();
                    }],


                    entity: ['$stateParams','AssetTypeSetup', function($stateParams, AssetTypeSetup) {

                        return AssetTypeSetup.get({id : $stateParams.id});
                    }]

                }

            });
    });

/*-------Ending--------assetTypeSetup.js-----------*/


/*----Starting------assetTypeSetup-detail.controller.js-----------------*/

angular.module('stepApp')
    .controller('AssetTypeSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, AssetTypeSetup) {
                $scope.assetTypeSetup = entity;
                $scope.load = function (id) {
                    AssetTypeSetup.get({id: id}, function(result) {
                        $scope.assetTypeSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetTypeSetupUpdate', function(event, result) {
                    $scope.assetTypeSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*----Ending------assetTypeSetup-detail.controller.js-----------------*/


/*------Starting-----assetTypeSetup-dialog.controller.js-----------*/

angular.module('stepApp').controller('AssetTypeSetupDialogController',
    ['$scope','$rootScope', '$stateParams', 'entity','$state', 'AssetTypeSetup','$location',
        function ($scope, $rootScope, $stateParams, entity,$state, AssetTypeSetup,$location) {

            $scope.assetTypeSetup = entity;

            $scope.load = function (id) {
                AssetTypeSetup.get({id: id}, function (result) {
                    $scope.assetTypeSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetTypeSetupUpdate', result);
                $state.go('assetTypeSetup', null,{ reload: true }),
                    function (){
                        $state.go('assetTypeSetup');
                    };
            }

            $scope.save = function () {
                if ($scope.assetTypeSetup.id != null) {
                    AssetTypeSetup.update($scope.assetTypeSetup, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetTypeSetup.updated');
                } else {
                    AssetTypeSetup.save($scope.assetTypeSetup, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetTypeSetup.created');
                }

            };

            $scope.clear = function () {

            };
        }]);

/*------Ending-----assetTypeSetup-dialog.controller.js-----------*/
