'use strict';

angular.module('stepApp')
    .controller('AssetCategorySetupController',
    ['$scope', '$rootScope', 'AssetCategorySetup', 'AssetCategorySetupSearch', 'ParseLinks',
    function ($scope, $rootScope, AssetCategorySetup, AssetCategorySetupSearch, ParseLinks) {
        $scope.assetCategorySetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetCategorySetup.query({page: $scope.page, size: 200}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetCategorySetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetCategorySetup.get({id: id}, function(result) {
                $scope.assetCategorySetup = result;
                $('#deleteAssetCategorySetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetCategorySetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetCategorySetup.deleted');
                    $('#deleteAssetCategorySetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetCategorySetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetCategorySetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetCategorySetup = {
                categoryName: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);

/*-----Starting--------assetCategorySetup.js------------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetCategorySetup', {
                parent: 'assetManagements',
                url: '/assetCategorySetups',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetCategorySetup.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetCategorySetup/assetCategorySetups.html',
                        controller: 'AssetCategorySetupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetCategorySetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetCategorySetup.detail', {
                parent: 'assetCategorySetup',
                url: '/assetCategorySetup/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetCategorySetup.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetCategorySetup/assetCategorySetup-detail.html',
                        controller: 'AssetCategorySetupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetCategorySetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetCategorySetup', function($stateParams, AssetCategorySetup) {
                        return AssetCategorySetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetCategorySetup.new', {
                parent: 'assetCategorySetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetCategorySetup.detail.title'
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetCategorySetup/assetCategorySetup-dialog.html',
                        controller: 'AssetCategorySetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetCategorySetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetCategorySetup', function($stateParams, AssetCategorySetup) {
                        return {
                            categoryName: null,
                            description: null,
                            status: null,
                            id: null
                        };
                    }]
                }

            })
            .state('assetCategorySetup.edit', {
                parent: 'assetCategorySetup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetCategorySetup.detail.title'
                },


                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetCategorySetup/assetCategorySetup-dialog.html',
                        controller: 'AssetCategorySetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetCategorySetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetCategorySetup', function($stateParams, AssetCategorySetup) {
                        console.log("stateParam"+AssetCategorySetup.get({id : $stateParams.id}));
                        return AssetCategorySetup.get({id : $stateParams.id});
                    }]
                }

            });
    });


/*-----Ending--------assetCategorySetup.js------------*/

/*-----Starting----assetCategorySetup-detail.controller.js-----------*/


angular.module('stepApp')
    .controller('AssetCategorySetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetCategorySetup', 'AssetTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, AssetCategorySetup, AssetTypeSetup) {
                $scope.assetCategorySetup = entity;
                $scope.load = function (id) {
                    AssetCategorySetup.get({id: id}, function(result) {
                        $scope.assetCategorySetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetCategorySetupUpdate', function(event, result) {
                    $scope.assetCategorySetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*-----Ending----assetCategorySetup-detail.controller.js-----------*/

/*----Starting-----assetCategorySetup-dialog.controller.js-------*/


angular.module('stepApp').controller('AssetCategorySetupDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetCategorySetup','$state', 'AssetTypeSetup',
        function($scope, $rootScope, $stateParams, entity, AssetCategorySetup,$state, AssetTypeSetup) {

            $scope.message = '';
            $scope.assetCategorySetup = {};
            $scope.assetCategorySetup.status = true;

            AssetCategorySetup.get({id : $stateParams.id},function(result) {
                $scope.assetCategorySetup = result;
                if($scope.assetCategorySetup == null ){
                    $scope.typeCode = '';
                }
                else {
                    $scope.typeCode = $scope.assetCategorySetup.assetTypeSetup.typeCode;
                }
            });

            $scope.assettypesetups = AssetTypeSetup.query();
            $scope.load = function(id) {
                AssetCategorySetup.get({id : id}, function(result) {
                    $scope.assetCategorySetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetCategorySetupUpdate', result);
                $state.go('assetCategorySetup', null,{ reload: true }),
                    function (){
                        $state.go('assetCategorySetup');
                    };

            };

            $scope.save = function () {
                if ($scope.assetCategorySetup.id != null) {
                    AssetCategorySetup.update($scope.assetCategorySetup, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetCategorySetup.updated');
                } else {
                    AssetCategorySetup.save($scope.assetCategorySetup, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetCategorySetup.created');
                }
            };
            $scope.clear = function() {

            };
        }]);


/*----Ending-----assetCategorySetup-dialog.controller.js-------*/
