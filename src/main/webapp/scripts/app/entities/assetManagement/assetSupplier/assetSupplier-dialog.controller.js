 /*
'use strict';

angular.module('stepApp').controller('AssetSupplierDialogController',
    ['$scope', '$rootScope','$state', '$stateParams', 'entity', 'AssetSupplier', 'Country','ParseLinks',
        function($scope, $rootScope, $state, $stateParams, entity, AssetSupplier, Country, ParseLinks) {

        $scope.assetSupplier = entity;
        $scope.countrys = [];

         Country.query({page: $scope.page, size: 5000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.countrys = result;
                        $scope.total = headers('x-total-count');
                    });

        $scope.load = function(id) {
            AssetSupplier.get({id : id}, function(result) {
                $scope.assetSupplier = result;
                $scope.assetSupplier.contactNo = parseInt(result.contactNo);
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:assetSupplierUpdate', result);

            $state.go('assetSupplier', null,{ reload: true }),
                function (){
                    $state.go('assetSupplier');
                };

        };

        $scope.save = function () {
            if ($scope.assetSupplier.id != null) {
                AssetSupplier.update($scope.assetSupplier, onSaveFinished);
                $rootScope.setWarningMessage('stepApp.assetSupplier.updated');
            } else {
                AssetSupplier.save($scope.assetSupplier, onSaveFinished);
                $rootScope.setSuccessMessage('stepApp.assetSupplier.created');
            }
        };

        $scope.clear = function() {

        };
}]);
*/
