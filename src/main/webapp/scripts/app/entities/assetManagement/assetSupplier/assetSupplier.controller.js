'use strict';

angular.module('stepApp')
    .controller('AssetSupplierController',
     ['$scope', '$rootScope', 'AssetSupplier', 'AssetSupplierSearch', 'ParseLinks',
     function ($scope, $rootScope, AssetSupplier, AssetSupplierSearch, ParseLinks) {
        $scope.assetSuppliers = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetSupplier.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetSuppliers = result;
            });
        };

        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetSupplier.get({id: id}, function(result) {
                $scope.assetSupplier = result;
                $('#deleteAssetSupplierConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetSupplier.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetSupplier.deleted');
                    $('#deleteAssetSupplierConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetSupplierSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetSuppliers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetSupplier = {
                name: null,
                supplierId: null,
                address: null,
                products: null,
                contactNo: null,
                telephoneNo: null,
                email: null,
                webSite: null,
                faxNo: null,
                id: null
            };
        };
    }]);


/*------Starting-----assetSupplier.js-------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetSupplier', {
                parent: 'assetManagements',
                url: '/assetSuppliers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetSupplier.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetSupplier/assetSuppliers.html',
                        controller: 'AssetSupplierController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetSupplier');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetSupplier.detail', {
                parent: 'assetSupplier',
                url: '/assetSupplier/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetSupplier.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetSupplier/assetSupplier-detail.html',
                        controller: 'AssetSupplierDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetSupplier');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetSupplier', function($stateParams, AssetSupplier) {
                        return AssetSupplier.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetSupplier.new', {
                parent: 'assetSupplier',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetSupplier.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetSupplier/assetSupplier-dialog.html',
                        controller: 'AssetSupplierDialogController'
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            name: null,
                            supplierId: null,
                            address: null,
                            products: null,
                            contactNo: null,
                            telephoneNo: null,
                            email: null,
                            webSite: null,
                            faxNo: null,
                            status:true,
                            id: null
                        };
                    }
                }

            })
            .state('assetSupplier.edit', {
                parent: 'assetSupplier',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetSupplier/assetSupplier-dialog.html',
                        controller: 'AssetSupplierDialogController'

                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetSupplier');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetSupplier', function($stateParams, AssetSupplier) {
                        return AssetSupplier.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*------Ending-----assetSupplier.js-------*/

/*-----Starting---------assetSupplier-detail.controller.js----------*/

angular.module('stepApp')
    .controller('AssetSupplierDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetSupplier',
            function ($scope, $rootScope, $stateParams, entity, AssetSupplier) {
                $scope.assetSupplier = entity;
                $scope.load = function (id) {
                    AssetSupplier.get({id: id}, function(result) {
                        $scope.assetSupplier = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetSupplierUpdate', function(event, result) {
                    $scope.assetSupplier = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*-----Ending---------assetSupplier-detail.controller.js----------*/


/*------Starting--------assetSupplier-dialog.controller.js--------*/

angular.module('stepApp').controller('AssetSupplierDialogController',
    ['$scope', '$rootScope','$state', '$stateParams', 'entity', 'AssetSupplier', 'Country','ParseLinks',
        function($scope, $rootScope, $state, $stateParams, entity, AssetSupplier, Country, ParseLinks) {

            $scope.assetSupplier = entity;
            $scope.countrys = [];

            Country.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.countrys = result;
                $scope.total = headers('x-total-count');
            });

            $scope.load = function(id) {
                AssetSupplier.get({id : id}, function(result) {
                    $scope.assetSupplier = result;
                    $scope.assetSupplier.contactNo = parseInt(result.contactNo);
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetSupplierUpdate', result);

                $state.go('assetSupplier', null,{ reload: true }),
                    function (){
                        $state.go('assetSupplier');
                    };

            };

            $scope.save = function () {
                if ($scope.assetSupplier.id != null) {
                    AssetSupplier.update($scope.assetSupplier, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetSupplier.updated');
                } else {
                    AssetSupplier.save($scope.assetSupplier, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetSupplier.created');
                }
            };

            $scope.clear = function() {

            };
        }]);

/*------Ending--------assetSupplier-dialog.controller.js--------*/
