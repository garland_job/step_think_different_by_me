'use strict';

angular.module('stepApp')
    .controller('AssetAccuisitionSetupController',
    ['$scope', '$rootScope', 'AssetAccuisitionSetup', 'AssetAccuisitionSetupSearch', 'ParseLinks',
        function ($scope, $rootScope, AssetAccuisitionSetup, AssetAccuisitionSetupSearch, ParseLinks) {
        $scope.assetAccuisitionSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetAccuisitionSetup.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetAccuisitionSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetAccuisitionSetup.get({id: id}, function(result) {
                $scope.assetAccuisitionSetup = result;
                $('#deleteAssetAccuisitionSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetAccuisitionSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetAccuisitionSetup.deleted');
                    $('#deleteAssetAccuisitionSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetAccuisitionSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetAccuisitionSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetAccuisitionSetup = {
                trackID: null,
                code: null,
                name: null,
                Description: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                remarks: null,
                status: null,
                id: null
            };
        };
    }]);

/*  ---- Starting----------assetAccuisitionSetup.js ---------------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetAccuisitionSetup', {
                parent: 'assetManagements',
                url: '/assetAccuisitionSetups',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetAccuisitionSetup.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAccuisitionSetup/assetAccuisitionSetups.html',
                        controller: 'AssetAccuisitionSetupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAccuisitionSetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetAccuisitionSetup.detail', {
                parent: 'assetAccuisitionSetup',
                url: '/assetAccuisitionSetup/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetAccuisitionSetup.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAccuisitionSetup/assetAccuisitionSetup-detail.html',
                        controller: 'AssetAccuisitionSetupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAccuisitionSetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetAccuisitionSetup', function($stateParams, AssetAccuisitionSetup) {
                        return AssetAccuisitionSetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetAccuisitionSetup.new', {
                parent: 'assetAccuisitionSetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAccuisitionSetup/assetAccuisitionSetup-dialog.html',
                        controller: 'AssetAccuisitionSetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAccuisitionSetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            trackID: null,
                            code: null,
                            name: null,
                            Description: null,
                            createDate: null,
                            createBy: null,
                            updateDate: null,
                            updateBy: null,
                            remarks: null,
                            status: true,
                            id: null
                        };
                    }
                }

            })
            .state('assetAccuisitionSetup.edit', {
                parent: 'assetAccuisitionSetup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAccuisitionSetup/assetAccuisitionSetup-dialog.html',
                        controller: 'AssetAccuisitionSetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAccuisitionSetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['AssetAccuisitionSetup','$stateParams', function(AssetAccuisitionSetup,$stateParams) {
                        return AssetAccuisitionSetup.get({id : $stateParams.id});
                    }]
                }

            });
    });

/*  ---- Ending----------assetAccuisitionSetup.js ---------------*/

/*----- Starting----assetAccuisitionSetup-detail.controller.js--------*/

angular.module('stepApp')
    .controller('AssetAccuisitionSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetAccuisitionSetup',
            function ($scope, $rootScope, $stateParams, entity, AssetAccuisitionSetup) {
                $scope.assetAccuisitionSetup = entity;
                $scope.load = function (id) {
                    AssetAccuisitionSetup.get({id: id}, function(result) {
                        $scope.assetAccuisitionSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetAccuisitionSetupUpdate', function(event, result) {
                    $scope.assetAccuisitionSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*----- Ending----assetAccuisitionSetup-detail.controller.js--------*/

/*-----Starting-----assetAccuisitionSetup-dialog.controller.js--------------*/

angular.module('stepApp').controller('AssetAccuisitionSetupDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'AssetAccuisitionSetup',
        function($scope, $rootScope, $stateParams, $state, entity, AssetAccuisitionSetup) {

            $scope.assetAccuisitionSetup = entity;
            $scope.load = function(id) {
                AssetAccuisitionSetup.get({id : id}, function(result) {
                    $scope.assetAccuisitionSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetAccuisitionSetupUpdate', result);
                $state.go('assetAccuisitionSetup');
            };

            $scope.save = function () {
                if ($scope.assetAccuisitionSetup.id != null) {
                    AssetAccuisitionSetup.update($scope.assetAccuisitionSetup, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetAccuisitionSetup.updated');
                } else {
                    AssetAccuisitionSetup.save($scope.assetAccuisitionSetup, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetAccuisitionSetup.created');
                }
            };

            $scope.clear = function() {
                $state.go('assetAccuisitionSetup');
            };
        }]);

/*-----Ending-----assetAccuisitionSetup-dialog.controller.js--------------*/
