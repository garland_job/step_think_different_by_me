
/*


'use strict';

angular.module('stepApp').controller('AssetAuctionInformationDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetAuctionInformation', '$state', 'AssetDistribution','AssetRecord',
        function ($scope, $rootScope, $stateParams, entity, AssetAuctionInformation, $state, AssetDistribution,AssetRecord) {

            $scope.assetAuctionInformation = {};
            $scope.assetdistributions = AssetDistribution.query();
            $scope.assetRecords = AssetRecord.query();
            if($stateParams.id) {
                AssetAuctionInformation.get({id: $stateParams.id}, function (result) {
                    $scope.assetAuctionInformation = result;
                });
            }

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetAuctionInformationUpdate', result);

                $state.go('assetAuctionInformation', null, {reload: true}),
                    function () {
                        $state.go('assetAuctionInformation');
                    };

            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.refNoAssign = function(ref){
                $scope.assetAuctionInformation.refNo = ref;
            };

            $scope.DateChecked = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Last Repair Date will be greater then Purchase date!')
                    $scope.assetAuctionInformation.lastRepairDate=null;
                }
            };

            $scope.AssetValueChange = function (CodeOfAsset) {

                angular.forEach($scope.assetdistributions, function (code) {
                      console.log('------------');
                      console.log(code);
                    if (CodeOfAsset == code.id) {
                        $scope.assetdistributions.assetNam = code.assetRecord.assetName;
                        $scope.assetdistributions.purchaseDate = code.assetRecord.purchaseDate;
                        $scope.assetdistributions.vendorName = code.assetRecord.vendorName;
                        $scope.assetdistributions.empName = code.employee.name;
                        $scope.assetdistributions.empDepartment = code.employee.department;

                    }

                })

            };

            $scope.save = function () {
                if ($scope.assetAuctionInformation.id != null) {
                    AssetAuctionInformation.update($scope.assetAuctionInformation, onSaveFinished);
                    $rootScope.setErrorMessage('stepApp.assetAuctionInformation.deleted');
                } else {
                    AssetAuctionInformation.save($scope.assetAuctionInformation, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetAuctionInformation.created');
                }
            };

            $scope.clear = function () {

            };
        }]);
*/
