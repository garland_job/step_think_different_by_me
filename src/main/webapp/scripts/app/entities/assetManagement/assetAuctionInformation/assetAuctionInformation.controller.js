'use strict';

angular.module('stepApp')
    .controller('AssetAuctionInformationController',
    ['$scope', '$rootScope', 'AssetAuctionInformation', 'AssetAuctionInformationSearch', 'ParseLinks',
    function ($scope, $rootScope, AssetAuctionInformation, AssetAuctionInformationSearch, ParseLinks) {
        $scope.assetAuctionInformations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetAuctionInformation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetAuctionInformations = result;
                console.log($scope.assetAuctionInformations);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetAuctionInformation.get({id: id}, function(result) {
                $scope.assetAuctionInformation = result;
                $rootScope.setErrorMessage('stepApp.assetAuctionInformation.deleted');
                $('#deleteAssetAuctionInformationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetAuctionInformation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAssetAuctionInformationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetAuctionInformationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetAuctionInformations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetAuctionInformation = {
                lastRepairDate: null,
                isAuctionBefore: null,
                id: null
            };
        };
    }]);

/*----Starting----assetAuctionInformation.js-------*/


angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetAuctionInformation', {
                parent: 'assetManagements',
                url: '/assetAuctionInformations',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetAuctionInformation.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAuctionInformation/assetAuctionInformations.html',
                        controller: 'AssetAuctionInformationController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAuctionInformation');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetAuctionInformation.detail', {
                parent: 'assetManagements',
                url: '/assetAuctionInformation/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetAuctionInformation.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAuctionInformation/assetAuctionInformation-detail.html',
                        controller: 'AssetAuctionInformationDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAuctionInformation');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetAuctionInformation', function($stateParams, AssetAuctionInformation) {
                        return AssetAuctionInformation.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetAuctionInformation.new', {
                parent: 'assetAuctionInformation',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAuctionInformation/assetAuctionInformation-dialog.html',
                        controller: 'AssetAuctionInformationDialogController'
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            lastRepairDate: null,
                            isAuctionBefore:true,
                            id: null
                        };
                    }
                }

            })
            .state('assetAuctionInformation.edit', {
                parent: 'assetAuctionInformation',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetAuctionInformation/assetAuctionInformation-dialog.html',
                        controller: 'AssetAuctionInformationDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetAuctionInformation');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetAuctionInformation', function($stateParams, AssetAuctionInformation) {
                        return AssetAuctionInformation.get({id : $stateParams.id});
                    }]
                }

            });
    });

/*----Ending----assetAuctionInformation.js-------*/

/*-----Starting------assetAuctionInformation-detail.controller.js--------*/

angular.module('stepApp')
    .controller('AssetAuctionInformationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetAuctionInformation', 'AssetDistribution',
            function ($scope, $rootScope, $stateParams, entity, AssetAuctionInformation, AssetDistribution) {
                $scope.assetAuctionInformation = entity;
                $scope.load = function (id) {
                    AssetAuctionInformation.get({id: id}, function(result) {
                        $scope.assetAuctionInformation = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetAuctionInformationUpdate', function(event, result) {
                    $scope.assetAuctionInformation = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*-----Ending------assetAuctionInformation-detail.controller.js--------*/

/*-----------Starting--------assetAuctionInformation-dialog.controller.js-----------------*/


angular.module('stepApp').controller('AssetAuctionInformationDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetAuctionInformation', '$state', 'AssetDistribution','AssetRecord',
        function ($scope, $rootScope, $stateParams, entity, AssetAuctionInformation, $state, AssetDistribution,AssetRecord) {

            $scope.assetAuctionInformation = {};
            $scope.assetdistributions = AssetDistribution.query();
            $scope.assetRecords = AssetRecord.query();
            if($stateParams.id) {
                AssetAuctionInformation.get({id: $stateParams.id}, function (result) {
                    $scope.assetAuctionInformation = result;
                });
            }

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetAuctionInformationUpdate', result);
                $state.go('assetAuctionInformation', null, {reload: true}),
                    function () {
                        $state.go('assetAuctionInformation');
                    };

            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.refNoAssign = function(ref){
                $scope.assetAuctionInformation.refNo = ref;
            };

            $scope.DateChecked = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Last Repair Date will be greater then Purchase date!')
                    $scope.assetAuctionInformation.lastRepairDate=null;
                }
            };

            $scope.AssetValueChange = function (CodeOfAsset) {
                angular.forEach($scope.assetdistributions, function (code) {
                    if (CodeOfAsset == code.id) {
                        $scope.assetdistributions.assetNam = code.assetRecord.assetName;
                        $scope.assetdistributions.purchaseDate = code.assetRecord.purchaseDate;
                        $scope.assetdistributions.vendorName = code.assetRecord.vendorName;
                        $scope.assetdistributions.empName = code.employee.name;
                        $scope.assetdistributions.empDepartment = code.employee.department;
                    }
                })
            };

            $scope.save = function () {
                if ($scope.assetAuctionInformation.id != null) {
                    AssetAuctionInformation.update($scope.assetAuctionInformation, onSaveFinished);
                    $rootScope.setErrorMessage('stepApp.assetAuctionInformation.deleted');
                } else {
                    AssetAuctionInformation.save($scope.assetAuctionInformation, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetAuctionInformation.created');
                }
            };
            $scope.clear = function () {
            };
        }]);


/*-----------Ending--------assetAuctionInformation-dialog.controller.js-----------------*/
