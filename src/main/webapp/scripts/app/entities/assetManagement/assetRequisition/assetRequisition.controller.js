'use strict';
angular.module('stepApp')
    .controller('AssetRequisitionController',
        ['$rootScope', '$scope', '$state', 'Principal', 'AssetRequisition', 'AssetRequisitionSearch', 'ParseLinks', 'AssetRequisitionInfo', 'AssetRequisitionList',
            function ($rootScope, $scope, $state,Principal, AssetRequisition, AssetRequisitionSearch, ParseLinks,AssetRequisitionInfo,AssetRequisitionList) {
                $scope.assetRequisitions = [];
                $scope.AssetRequisitionInfo = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                        /*AssetRequisition.query({page: $scope.page, size: 2000}, function(result, headers) {
                         $scope.links = ParseLinks.parse(headers('link'));
                         $scope.assetRequisitions = result;
                         });*/
                        AssetRequisitionList.query({reqStatus: 1, size: 20000}, function (result) {
                            $scope.assetRequisitions = result;
                            console.log('####===---===####');
                            console.log($scope.assetRequisitions);
                        });
                    }else
                    if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])) {
                        AssetRequisitionInfo.query({page: $scope.page, size: 2000}, function(result, headers) {
                            $scope.links = ParseLinks.parse(headers('link'));
                            $scope.assetRequisitions = result;
                        });
                    }
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();
                $scope.delete = function (id) {
                    AssetRequisition.get({id: id}, function(result) {
                        $scope.assetRequisition = result;
                        $('#deleteAssetRequisitionConfirmation').modal('show');
                    });
                };
                $scope.confirmDelete = function (id) {
                    AssetRequisition.delete({id: id},
                        function () {
                            $scope.loadAll();
                            $rootScope.setErrorMessage('stepApp.assetRequisition.deleted');
                            $('#deleteAssetRequisitionConfirmation').modal('hide');
                            $scope.clear();
                        });
                };
                $scope.getRefID = function (id) {
                    $rootScope.assetRefId =id;
                    $state.go('assetDistribution.new');
                };
                $scope.search = function () {
                    AssetRequisitionSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.assetRequisitions = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };
                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };
                $scope.clear = function () {
                    $scope.assetRequisition = {
                        empId: null,
                        empName: null,
                        designation: null,
                        department: null,
                        requisitionId: null,
                        requisitionDate: null,
                        quantity: null,
                        reasonOfReq: null,
                        reqStatus: null,
                        status: null,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        remarks: null,
                        id: null
                    };
                };
            }]);
/*---------Starting-----assetRequisition.js--------------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetRequisition', {
                parent: 'assetManagements',
                url: '/assetRequisitions',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.assetRequisition.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRequisition/assetRequisitions.html',
                        controller: 'AssetRequisitionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRequisition');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetRequisition.detail', {
                parent: 'assetRequisition',
                url: '/assetRequisition/{id}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.assetRequisition.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRequisition/assetRequisition-detail.html',
                        controller: 'AssetRequisitionDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRequisition');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRequisition', function($stateParams, AssetRequisition) {
                        return AssetRequisition.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetRequisition.new', {
                parent: 'assetRequisition',
                url: '/new',
                data: {
                    authorities: [],
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRequisition/assetRequisition-dialog.html',
                        controller: 'AssetRequisitionDialogController'
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            empId: null,
                            empName: null,
                            designation: null,
                            department: null,
                            requisitionId: null,
                            requisitionDate: null,
                            quantity: null,
                            reasonOfReq: null,
                            reqStatus: 1,
                            status: true,
                            createDate: null,
                            createBy: null,
                            updateDate: null,
                            updateBy: null,
                            remarks: null,
                            id: null
                        };
                    }
                }
            })
            .state('assetRequisition.edit', {
                parent: 'assetRequisition',
                url: '/{id}/edit',
                data: {
                    authorities: [],
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRequisition/assetRequisition-dialog.html',
                        controller: 'AssetRequisitionDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRequisition');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRequisition', function($stateParams, AssetRequisition) {
                        return AssetRequisition.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*---------Ending-----assetRequisition.js--------------*/

/*-----Starting------assetRequisition-detail.controller.js----------*/

angular.module('stepApp')
    .controller('AssetRequisitionDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetRequisition', 'AssetTypeSetup', 'AssetCategorySetup', 'AssetRecord',
            function ($scope, $rootScope, $stateParams, entity, AssetRequisition, AssetTypeSetup, AssetCategorySetup, AssetRecord) {
                $scope.assetRequisition = entity;
                $scope.load = function (id) {
                    AssetRequisition.get({id: id}, function(result) {
                        $scope.assetRequisition = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetRequisitionUpdate', function(event, result) {
                    $scope.assetRequisition = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*-----Ending------assetRequisition-detail.controller.js----------*/

/*------Starting------assetRequisition-dialog.controller.js---------------*/

angular.module('stepApp').controller('AssetRequisitionDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'AssetRequisition', 'AssetTypeSetup', 'AssetCategorySetup', 'AssetRecord', 'HrEmployeeInfo', 'ParseLinks', 'HrEmployeeInfoByEmployeeId', 'AssetCategorySetupByTypeId', 'AssetRecordByTypeIdAndCategoryId',
        function ($scope, $rootScope, $stateParams, $state, entity, AssetRequisition, AssetTypeSetup, AssetCategorySetup, AssetRecord, HrEmployeeInfo, ParseLinks, HrEmployeeInfoByEmployeeId, AssetCategorySetupByTypeId, AssetRecordByTypeIdAndCategoryId) {

            $scope.assetRequisition = entity;
            $scope.hrEmployeeInfos = [];
            $scope.assetcategorysetups = AssetCategorySetup.query();
            $scope.assetDetails = [];
            $scope.assettypesetups = AssetTypeSetup.query();

            $scope.hrmEmployee = [];
            $scope.hrmEmployees = [];
            $scope.getHrmEmployee = [];

            HrEmployeeInfo.query({page: $scope.page, size: 1000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.hrmEmployee = result;
                $scope.hrmEmployee.forEach(function (data) {
                    $scope.hrmEmployees.push(data.employeeId);
                });

            });

            $scope.load = function (id) {
                AssetRequisition.get({id: id}, function (result) {
                    $scope.assetRequisition = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetRequisitionUpdate', result);
                $state.go('assetRequisition');
            };

            $scope.getAssetTypes = function (id) {

            }

            $scope.getAssetRecords = function (catId, typeId) {
                AssetRecordByTypeIdAndCategoryId.query({typeId: typeId.id, categoryId: catId.id}, function (result) {
                    $scope.assetrecords = result;
                });
            }

            $scope.getAssetCategories = function (typeId) {
                AssetCategorySetupByTypeId.query({id: typeId.id}, function (result) {
                    $scope.assetcategorysetups = result;
                });
            }

            $scope.getEmployee = function (id) {
                HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                    $scope.getHrmEmployee = result;
                    if ($scope.getHrmEmployee.fullName.toString() != null) {
                        $scope.assetRequisition.empName = $scope.getHrmEmployee.fullName;
                    } else {
                        $scope.assetRequisition.empName = 'Not Found';
                    }
                    if ($scope.getHrmEmployee.designationInfo.designationInfo != null) {
                        if ($scope.getHrmEmployee.designationInfo.designationInfo.designationName != null) {
                            $scope.assetRequisition.designation = $scope.getHrmEmployee.designationInfo.designationInfo.designationName;
                        }
                    } else {
                        $scope.assetRequisition.designation = 'Not Found';
                    }
                    if ($scope.getHrmEmployee.departmentInfo.departmentInfo != null) {
                        if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                            $scope.assetRequisition.department = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                        }
                    } else {
                        $scope.assetRequisition.department = 'Not Found';
                    }

                });
            }

            $scope.getAssetDetails = function (recordId) {
                AssetRecord.get({id: recordId.id}, function (result) {
                    $scope.assetDetails = result;
                    if ($scope.assetDetails.recordCode != null) {
                        $scope.assetRequisition.assetCode = $scope.assetDetails.recordCode;
                    } else {
                        $scope.assetRequisition.assetCode = 'Not Found';
                    }
                    $scope.assetRequisition.available = 'Yes';
                    $scope.assetRequisition.assetStatus = $scope.assetDetails.status ? 'Active' : 'Inactive';

                });
            }

            $scope.save = function () {
                if ($scope.assetRequisition.id != null) {
                    AssetRequisition.update($scope.assetRequisition, onSaveFinished);
                    $rootScope.setWarningMessage('You have successfully request for an asset.');
                } else {
                    AssetRequisition.save($scope.assetRequisition, onSaveFinished);
                    $rootScope.setSuccessMessage('You have successfully request for an asset.');
                }
            };

            $scope.clear = function () {
                $state.go('assetRequisition');
            };
        }]);

/*------Ending------assetRequisition-dialog.controller.js---------------*/
