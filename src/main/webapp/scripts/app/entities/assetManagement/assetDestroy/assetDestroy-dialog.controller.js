
/*
'use strict';

angular.module('stepApp').controller('AssetDestroyDialogController',
    ['$scope','$state', '$stateParams', '$q', 'entity', 'AssetDestroy', 'AssetDistribution', 'AssetRecord',
        function($scope,$state, $stateParams, $q, entity, AssetDestroy, AssetDistribution, AssetRecord) {
        $scope.assetDestroy =entity;
        $scope.assetRecords=AssetRecord.query();
        if($stateParams.id) {
            AssetDestroy.get({id: $stateParams.id}, function (result) {
                $scope.assetDestroy = result;
            });
        }

        $scope.assetrecords = AssetRecord.query();
        $scope.load = function(id) {
            AssetDestroy.get({id : id}, function(result) {
                $scope.assetDestroy = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:assetDestroyUpdate', result);
            $state.go('assetDestroy', null,{ reload: true }),
                function (){
                    $state.go('assetDestroy');
                };
        };


            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.refNoAssign = function(ref){
                $scope.assetDestroy.transferReference = ref;
            };

            $scope.DateChecked = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Destroyed Date will be greater then Purchase date!')
                    $scope.assetDestroy.destroyDate=null;
                }
            };

            $scope.AssetValueChange = function(CodeOfAsset){

                angular.forEach($scope.assetDistributions, function(code){

                    if(CodeOfAsset == code.id){
                        $scope.assetDestroy.assetDistribution.assetNam = code.assetRecord.assetName;
                        $scope.assetDestroy.assetDistribution.purchaseDate = code.assetRecord.purchaseDate;
                        $scope.assetDestroy.assetDistribution.vendorName = code.assetRecord.vendorName;
                        $scope.assetDestroy.assetDistribution.empName = code.employee.name;
                        $scope.assetDestroy.assetDistribution.empDepartment = code.employee.department;

                    }

                })

                console.log( $scope.assetDestroy.assetDistribution.empName);
                console.log($scope.assetDestroy.assetDistribution.empDepartment);
            };

            $scope.save = function () {
                console.log($scope.assetDestroy);
            if ($scope.assetDestroy.id != null) {
                AssetDestroy.update($scope.assetDestroy, onSaveFinished);
            } else {
                AssetDestroy.save($scope.assetDestroy, onSaveFinished);
            }
        };

        $scope.clear = function() {

        };
}]);
*/
