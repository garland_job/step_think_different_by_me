'use strict';

angular.module('stepApp')
    .controller('AssetDestroyController',
    ['$scope', 'AssetDestroy', 'AssetDestroySearch', 'ParseLinks',
    function ($scope, AssetDestroy, AssetDestroySearch, ParseLinks) {
        $scope.assetDestroys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetDestroy.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetDestroys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetDestroy.get({id: id}, function(result) {
                $scope.assetDestroy = result;
                $('#deleteAssetDestroyConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetDestroy.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAssetDestroyConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetDestroySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetDestroys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetDestroy = {
                transferReference: null,
                destroyDate: null,
                usedPeriod: null,
                causeOfDate: null,
                id: null
            };
        };
    }]);

/*-----Starting-------assetDestroy.js---------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetDestroy', {
                parent: 'assetManagements',
                url: '/assetDestroys',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDestroy.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDestroy/assetDestroys.html',
                        controller: 'AssetDestroyController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDestroy');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetDestroy.detail', {
                parent: 'assetManagements',
                url: '/assetDestroy/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDestroy.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDestroy/assetDestroy-detail.html',
                        controller: 'AssetDestroyDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDestroy');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetDestroy', function($stateParams, AssetDestroy) {
                        return AssetDestroy.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetDestroy.new', {
                parent: 'assetDestroy',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDestroy.home.title'
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDestroy/assetDestroy-dialog.html',
                        controller: 'AssetDestroyDialogController'
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            transferReference: null,
                            destroyDate: null,
                            usedPeriod: null,
                            destroyDescript: null,
                            id: null
                        };
                    }
                }
            })
            .state('assetDestroy.edit', {
                parent: 'assetManagements',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDestroy.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl:'scripts/app/entities/assetManagement/assetDestroy/assetDestroy-dialog.html',
                        controller: 'AssetDestroyDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDestroy');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetDestroy', function($stateParams, AssetDestroy) {
                        return AssetDestroy.get({id : $stateParams.id});
                    }]
                }
            });
    });


/*-----Ending-------assetDestroy.js---------*/

/*-------Starting----------assetDestroy-detail.controller.js----------------*/

angular.module('stepApp')
    .controller('AssetDestroyDetailController',
        ['$scope', '$rootScope', '$stateParams','entity', 'AssetDestroy', 'AssetDistribution', 'AssetRecord',
            function ($scope, $rootScope, $stateParams, entity, AssetDestroy, AssetDistribution, AssetRecord) {
                $scope.assetDestroy = entity;
                $scope.load = function (id) {
                    AssetDestroy.get({id: id}, function(result) {
                        $scope.assetDestroy = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetDestroyUpdate', function(event, result) {
                    $scope.assetDestroy = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*-------Ending----------assetDestroy-detail.controller.js----------------*/

/*-----Starting-----assetDestroy-dialog.controller.js----------*/

angular.module('stepApp').controller('AssetDestroyDialogController',
    ['$scope','$state', '$stateParams', '$q', 'entity', 'AssetDestroy', 'AssetDistribution', 'AssetRecord',
        function($scope,$state, $stateParams, $q, entity, AssetDestroy, AssetDistribution, AssetRecord) {
            $scope.assetDestroy =entity;
            $scope.assetRecords=AssetRecord.query();
            if($stateParams.id) {
                AssetDestroy.get({id: $stateParams.id}, function (result) {
                    $scope.assetDestroy = result;
                });
            }

            $scope.assetrecords = AssetRecord.query();
            $scope.load = function(id) {
                AssetDestroy.get({id : id}, function(result) {
                    $scope.assetDestroy = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetDestroyUpdate', result);
                $state.go('assetDestroy', null,{ reload: true }),
                    function (){
                        $state.go('assetDestroy');
                    };
            };


            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.refNoAssign = function(ref){
                $scope.assetDestroy.transferReference = ref;
            };

            $scope.DateChecked = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Destroyed Date will be greater then Purchase date!')
                    $scope.assetDestroy.destroyDate=null;
                }
            };

            $scope.AssetValueChange = function(CodeOfAsset){
                angular.forEach($scope.assetDistributions, function(code){
                    if(CodeOfAsset == code.id){
                        $scope.assetDestroy.assetDistribution.assetNam = code.assetRecord.assetName;
                        $scope.assetDestroy.assetDistribution.purchaseDate = code.assetRecord.purchaseDate;
                        $scope.assetDestroy.assetDistribution.vendorName = code.assetRecord.vendorName;
                        $scope.assetDestroy.assetDistribution.empName = code.employee.name;
                        $scope.assetDestroy.assetDistribution.empDepartment = code.employee.department;
                    }
                })
            };

            $scope.save = function () {
                console.log($scope.assetDestroy);
                if ($scope.assetDestroy.id != null) {
                    AssetDestroy.update($scope.assetDestroy, onSaveFinished);
                } else {
                    AssetDestroy.save($scope.assetDestroy, onSaveFinished);
                }
            };

            $scope.clear = function() {

            };
        }]);

/*-----Ending-----assetDestroy-dialog.controller.js----------*/
