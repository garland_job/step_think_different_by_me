'use strict';

angular.module('stepApp')
    .controller('AssetTransferController',
    ['$scope', '$rootScope', 'AssetTransfer', 'AssetTransferSearch', 'ParseLinks',
    function ($scope, $rootScope, AssetTransfer, AssetTransferSearch, ParseLinks) {
        $scope.assetTransfers = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetTransfer.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetTransfers = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetTransfer.get({id: id}, function(result) {
                $scope.assetTransfer = result;
                $('#deleteAssetTransferConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetTransfer.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetTransfer.deleted');
                    $('#deleteAssetTransferConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetTransferSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetTransfers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetTransfer = {
                date: null,
                id: null
            };
        };
    }]);


/*----------Starting-------assetTransfer.js-----------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetTransfer', {
                parent: 'assetManagements',
                url: '/assetTransfers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTransfer.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTransfer/assetTransfers.html',
                        controller: 'AssetTransferController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTransfer');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetTransfer.detail', {
                parent: 'assetManagements',
                url: '/assetTransfer/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTransfer.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTransfer/assetTransfer-detail.html',
                        controller: 'AssetTransferDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTransfer');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetTransfer', function($stateParams, AssetTransfer) {
                        return AssetTransfer.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetTransfer.new', {
                parent: 'assetTransfer',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTransfer.detail.title'
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTransfer/assetTransfer-dialog.html',
                        controller: 'AssetTransferDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTransfer');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetTransfer', function($stateParams, AssetTransfer) {
                        return null;
                    }]
                }
            })
            .state('assetTransfer.edit', {
                parent: 'assetTransfer',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetTransfer.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetTransfer/assetTransfer-dialog.html',
                        controller: 'AssetTransferDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetTransfer');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetTransfer', function($stateParams, AssetTransfer) {
                        return AssetTransfer.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*----------Ending-------assetTransfer.js-----------*/


/*---Starting------assetTransfer-detail.controller.js-----------*/

angular.module('stepApp')
    .controller('AssetTransferDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetTransfer', 'Employee', 'AssetRecord',
            function ($scope, $rootScope, $stateParams, entity, AssetTransfer, Employee, AssetRecord) {
                $scope.assetTransfer = entity;
                $scope.load = function (id) {
                    AssetTransfer.get({id: id}, function(result) {
                        $scope.assetTransfer = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetTransferUpdate', function(event, result) {
                    $scope.assetTransfer = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*---Ending------assetTransfer-detail.controller.js-----------*/

/*-----Starting----assetTransfer-dialog.controller.js--------*/

angular.module('stepApp').controller('AssetTransferDialogController',
    ['$scope', '$rootScope', '$stateParams','entity', 'AssetTransfer','$state', 'Employee', 'AssetRecord','HrEmployeeInfoByEmployeeId','AssetRecordName', 'AssetDistributionQuantity',
        function($scope, $rootScope, $stateParams, entity, AssetTransfer,$state, Employee, AssetRecord, HrEmployeeInfoByEmployeeId,AssetRecordName,AssetDistributionQuantity) {

            $scope.assetTransfer = entity;
            $scope.employees = Employee.query();
            $scope.assetrecords = AssetRecord.query();
            $scope.AssetRecordName=[];
            $scope.AssetDistributionQuantity=[];

            $scope.load = function(id) {
                AssetTransfer.get({id : id}, function(result) {
                    $scope.assetTransfer = result;
                });
            };


            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveFinished = function (result) {
                AssetDistributionQuantity.get({id:$scope.id,assetCode : $scope.assetCoded,quantity:$scope.quantity}, function(result) {
                });
                $scope.$emit('stepApp:assetTransferUpdate', result);
                $state.go('assetTransfer');



            };

            $scope.getAssetDistributionInfo = function(employeeId,assetCode){
                AssetRecordName.get({employeeId:employeeId,assetCode:assetCode}, function (result) {
                    $scope.assetName = result.assetName;
                    $scope.id = result.id;
                    $scope.assetCoded =assetCode;
                    $scope.assetTransfer.assetName = $scope.assetName;
                    $scope.assetTransfer.availableQty = result.approveQuantity;
                });
            };

            $scope.AssignNumberCompaired = function(availQty,assignQty){
                $scope.quantity = availQty-assignQty;
                if(availQty<assignQty){
                    alert('Assign Quantity should be less than Available Quantity!');
                    $scope.assetTransfer.assignQty = "";
                }
            };

            $scope.save = function () {
                if ($scope.assetTransfer.id != null) {
                    AssetTransfer.update($scope.assetTransfer, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetTransfer.updated');
                } else {
                    AssetTransfer.save($scope.assetTransfer, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetTransfer.created');
                }
            };

            $scope.clear = function() {

            };

            $scope.getEmployeeFrom = function (id) {
                $rootScope.fromEmployee = id;
                HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                    $scope.getHrmEmployee = result;
                    $scope.assetTransfer.employeeFrom = result.fullName;
                    if($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName!=null){
                        $scope.assetTransfer.empFromDept = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                    }else{
                        $scope.assetTransfer.empFromDept='Not Found';
                    }
                });
            };
            $scope.getEmployeeTo = function (id) {
                $rootScope.toEmployee = id;
                if ($rootScope.fromEmployee == $rootScope.toEmployee) {
                    alert('Please choose different Employee !');
                    $scope.assetTransfer.empToDept = '';
                } else{
                    HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                        $scope.getHrmEmployee = result;
                        $scope.assetTransfer.employeeTo = result.fullName;
                        if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                            $scope.assetTransfer.empToDept = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                        } else {
                            $scope.assetTransfer.empToDept = 'Not Found';
                        }
                    });
                }
            }

        }]);

/*-----Ending----assetTransfer-dialog.controller.js--------*/
