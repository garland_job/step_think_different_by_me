 /*
'use strict';

angular.module('stepApp').controller('AssetTransferDialogController',
    ['$scope', '$rootScope', '$stateParams','entity', 'AssetTransfer','$state', 'Employee', 'AssetRecord','HrEmployeeInfoByEmployeeId','AssetRecordName', 'AssetDistributionQuantity',
        function($scope, $rootScope, $stateParams, entity, AssetTransfer,$state, Employee, AssetRecord, HrEmployeeInfoByEmployeeId,AssetRecordName,AssetDistributionQuantity) {

        $scope.assetTransfer = entity;
        $scope.employees = Employee.query();
        $scope.assetrecords = AssetRecord.query();
        $scope.AssetRecordName=[];
        $scope.AssetDistributionQuantity=[];

        $scope.load = function(id) {
            AssetTransfer.get({id : id}, function(result) {
                $scope.assetTransfer = result;
            });
        };


        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        var onSaveFinished = function (result) {
            console.log($scope.id);
            AssetDistributionQuantity.get({id:$scope.id,assetCode : $scope.assetCoded,quantity:$scope.quantity}, function(result) {
            });
            $scope.$emit('stepApp:assetTransferUpdate', result);
            $state.go('assetTransfer');



        };

        $scope.getAssetDistributionInfo = function(fromEmployee){
            AssetRecordName.get({employeeId:fromEmployee}, function (result) {
                $scope.assetName = result.assetName;
                $scope.id = result.id;
                $scope.assetCoded =result.assetCode;
                console.log('===============');
                console.log($scope.assetName);
                $scope.assetTransfer.assetName = $scope.assetName;
                $scope.assetTransfer.assetCode = result.assetCode;
                $scope.assetTransfer.availableQty = result.approveQuantity;
            });
        };

        $scope.AssignNumberCompaired = function(availQty,assignQty){
            $scope.quantity = availQty-assignQty;
            console.log('----------Quantity------------');
            console.log($scope.quantity);

          if(availQty<assignQty){
             alert('Assign Quantity should be less than Available Quantity!');
              $scope.assetTransfer.assignQty = "";
          }
        };

        $scope.save = function () {
            if ($scope.assetTransfer.id != null) {
                AssetTransfer.update($scope.assetTransfer, onSaveFinished);
                $rootScope.setWarningMessage('stepApp.assetTransfer.updated');
            } else {
                AssetTransfer.save($scope.assetTransfer, onSaveFinished);
                $rootScope.setSuccessMessage('stepApp.assetTransfer.created');
            }
        };

        $scope.clear = function() {

        };

            $scope.getEmployeeFrom = function (id) {
                $rootScope.fromEmployee = id;
                HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                    $scope.getHrmEmployee = result;
                    $scope.assetTransfer.employeeFrom = result.fullName;
                    if($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName!=null){
                        $scope.assetTransfer.empFromDept = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                    }else{
                        $scope.assetTransfer.empFromDept='Not Found';
                    }
                });
            };
            $scope.getEmployeeTo = function (id) {
                $rootScope.toEmployee = id;
                if ($rootScope.fromEmployee == $rootScope.toEmployee) {
                    alert('Please choose different Employee !');
                    $scope.assetTransfer.empToDept = '';
                } else{
                    HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                        $scope.getHrmEmployee = result;
                        $scope.assetTransfer.employeeTo = result.fullName;
                        if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                            $scope.assetTransfer.empToDept = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                        } else {
                            $scope.assetTransfer.empToDept = 'Not Found';
                        }
                    });
                }
            }

}]);
*/
