'use strict';

angular.module('stepApp')
    .controller('AssetRecordController',
    ['$scope', '$rootScope', 'AssetRecord', 'AssetRecordSearch', 'ParseLinks',
    function ($scope, $rootScope, AssetRecord, AssetRecordSearch, ParseLinks) {
        $scope.assetRecords = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetRecord.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetRecords = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetRecord.get({id: id}, function(result) {
                $scope.assetRecord = result;
                $rootScope.setErrorMessage('stepApp.assetRecord.deleted');
                $('#deleteAssetRecordConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetRecord.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAssetRecordConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetRecordSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetRecords = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetRecord = {
                assetName: null,
                vendorName: null,
                supplierName: null,
                purchaseDate: null,
                orderNo: null,
                price: null,
                status: null,
                id: null
            };
        };
    }]);

/*----Starting-----assetRecord.js--------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetRecord', {
                parent: 'assetManagements',
                url: '/assetRecords',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetRecord.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRecord/assetRecords.html',
                        controller: 'AssetRecordController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRecord');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetRecord.detail', {
                parent: 'assetRecord',
                url: '/assetRecord/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetRecord.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRecord/assetRecord-detail.html',
                        controller: 'AssetRecordDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRecord');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRecord', function($stateParams, AssetRecord) {
                        // return AssetRecord.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetRecord.new', {
                parent: 'assetRecord',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRecord/assetRecord-dialog.html',
                        controller: 'AssetRecordDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRecord');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            recordCode: 'AutoGenId',
                            assetName: null,
                            vendorName: null,
                            supplierName: null,
                            purchaseDate: null,
                            orderNo: null,
                            price: null,
                            status: true,
                            statusAsset: '1',
                            assetGroup: '1',
                            id: null
                        };
                    }
                }

            })

            .state('assetRecord.edit', {
                parent: 'assetRecord',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRecord/assetRecord-dialog.html',
                        controller: 'AssetRecordDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRecord');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRecord', function($stateParams, AssetRecord) {
                        return AssetRecord.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*----Ending-----assetRecord.js--------*/

/*------Starting----assetRecord-detail.controller.js---------*/

angular.module('stepApp')
    .controller('AssetRecordDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetRecord', 'AssetCategorySetup', 'AssetTypeSetup', 'AssetSupplier',
            function ($scope, $rootScope, $stateParams, entity, AssetRecord, AssetCategorySetup, AssetTypeSetup, AssetSupplier) {
                $scope.assetRecord = entity;
                if($stateParams.id !=null){
                    AssetRecord.get({id: $stateParams.id}, function(result) {
                        $scope.assetRecord = result;
                    });
                }

                var unsubscribe = $rootScope.$on('stepApp:assetRecordUpdate', function(event, result) {
                    $scope.assetRecord = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*------Ending----assetRecord-detail.controller.js---------*/

/*----Starting------assetRecord-dialog.controller.js----------*/

angular.module('stepApp').controller('AssetRecordDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetRecord', 'AssetCategorySetup','$state' ,'AssetTypeSetup', 'AssetSupplier', 'AssetAccuisitionSetup','AssetCategorySetupByTypeId','Country','ParseLinks',
        function($scope, $rootScope, $stateParams, entity, AssetRecord, AssetCategorySetup,$state, AssetTypeSetup, AssetSupplier, AssetAccuisitionSetup, AssetCategorySetupByTypeId, Country, ParseLinks) {

            $scope.assetRecord = entity;
            $scope.assetcategorysetups = AssetCategorySetup.query();
            $scope.assettypesetups = AssetTypeSetup.query();
            $scope.assetsuppliers = AssetSupplier.query();
            $scope.assetAccuisitionSetups = AssetAccuisitionSetup.query();
            $scope.countrys = [];

            Country.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.countrys = result;
                $scope.total = headers('x-total-count');
            });


            $scope.load = function(id) {
                AssetRecord.get({id : id}, function(result) {
                    $scope.assetRecord = result;

                });
            };

            $scope.DateChecked = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Expire Date will be greater then Purchase date!')
                    $scope.assetRecord.expireDate=null;
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.getAssetName = function (typeIds) {
                AssetCategorySetupByTypeId.query({id : typeIds},function(result) {
                    $scope.assetcategorysetups = result;
                });
            }

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetRecordUpdate', result);
                $state.go('assetRecord', null,{ reload: true }),
                    function (){
                        $state.go('assetRecord');
                    };
            };

            $scope.save = function () {
                if ($scope.assetRecord.id != null) {
                    AssetRecord.update($scope.assetRecord, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetRecord.updated');
                } else {
                    AssetRecord.save($scope.assetRecord, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetRecord.created');
                }
            };

            $scope.clear = function() {
            };

            $scope.filterAssetCatTypeSupplierByStatus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };
        }]);

/*----Ending------assetRecord-dialog.controller.js----------*/

