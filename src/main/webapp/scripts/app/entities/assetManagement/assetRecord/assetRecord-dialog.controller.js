
/*
'use strict';

angular.module('stepApp').controller('AssetRecordDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetRecord', 'AssetCategorySetup','$state' ,'AssetTypeSetup', 'AssetSupplier', 'AssetAccuisitionSetup','AssetCategorySetupByTypeId','Country','ParseLinks',
        function($scope, $rootScope, $stateParams, entity, AssetRecord, AssetCategorySetup,$state, AssetTypeSetup, AssetSupplier, AssetAccuisitionSetup, AssetCategorySetupByTypeId, Country, ParseLinks) {

        $scope.assetRecord = entity;
        $scope.assetcategorysetups = AssetCategorySetup.query();
        $scope.assettypesetups = AssetTypeSetup.query();
        $scope.assetsuppliers = AssetSupplier.query();
        $scope.assetAccuisitionSetups = AssetAccuisitionSetup.query();
        $scope.countrys = [];

         Country.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.countrys = result;
                $scope.total = headers('x-total-count');
            });


        $scope.load = function(id) {
            AssetRecord.get({id : id}, function(result) {
                $scope.assetRecord = result;

            });
        };

        $scope.DateChecked = function(date1,date2){
            if(moment(date1).isAfter(date2))
            {
                alert('Expire Date will be greater then Purchase date!')
                $scope.assetRecord.expireDate=null;
            }
        };

        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        $scope.getAssetName = function (typeIds) {
            AssetCategorySetupByTypeId.query({id : typeIds},function(result) {
                $scope.assetcategorysetups = result;
            });
        }

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:assetRecordUpdate', result);
            $state.go('assetRecord', null,{ reload: true }),
                function (){
                    $state.go('assetRecord');
                };
        };

        $scope.save = function () {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '+$scope.assetRecord);
            if ($scope.assetRecord.id != null) {
                AssetRecord.update($scope.assetRecord, onSaveFinished);
                $rootScope.setWarningMessage('stepApp.assetRecord.updated');
            } else {
                AssetRecord.save($scope.assetRecord, onSaveFinished);
                $rootScope.setSuccessMessage('stepApp.assetRecord.created');
            }
        };

        $scope.clear = function() {
        };

        $scope.filterAssetCatTypeSupplierByStatus = function () {
                                    return function (item) {
                                        if (item.status == true)
                                        {
                                            return true;
                                        }
                                        return false;
                                    };
                                };
}]);
*/
