'use strict';

angular.module('stepApp')
    .controller('AssetRepairController',
    ['$scope', '$rootScope', 'AssetRepair', 'AssetRepairSearch', 'ParseLinks',
    function ($scope, $rootScope, AssetRepair, AssetRepairSearch, ParseLinks) {
        $scope.assetRepairs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetRepair.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetRepairs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetRepair.get({id: id}, function(result) {
                $scope.assetRepair = result;
                $('#deleteAssetRepairConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetRepair.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetRepair.deleted');
                    $('#deleteAssetRepairConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetRepairSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetRepairs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetRepair = {
                refNo: null,
                repairedBy: null,
                dateOfProblem: null,
                repairDate: null,
                repairCost: null,
                id: null
            };
        };
    }]);


/*----Starting-------assetRepair.js---------------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetRepair', {
                parent: 'assetManagements',
                url: '/assetRepairs',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetRepair.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRepair/assetRepairs.html',
                        controller: 'AssetRepairController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRepair');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetRepair.detail', {
                parent: 'assetManagements',
                url: '/assetRepair/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetRepair.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRepair/assetRepair-detail.html',
                        controller: 'AssetRepairDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRepair');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRepair', function($stateParams, AssetRepair) {
                        return AssetRepair.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assetRepair.new', {
                parent: 'assetRepair',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRepair/assetRepair-dialog.html',
                        controller: 'AssetRepairDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRepair');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRepair', function($stateParams, AssetRepair) {
                        return null;
                    }]
                }

            })
            .state('assetRepair.edit', {
                parent: 'assetRepair',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },

                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetRepair/assetRepair-dialog.html',
                        controller: 'AssetRepairDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetRepair');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetRepair', function($stateParams, AssetRepair) {
                        return AssetRepair.get({id : $stateParams.id});
                    }]
                }
            });
    });


/*----Ending-------assetRepair.js---------------*/

/*------Starting-----assetRepair-detail.controller.js-----------*/

angular.module('stepApp')
    .controller('AssetRepairDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetRepair', 'Employee', 'AssetRecord',
            function ($scope, $rootScope, $stateParams, entity, AssetRepair, Employee, AssetRecord) {
                $scope.assetRepair = entity;
                $scope.load = function (id) {
                    AssetRepair.get({id: id}, function(result) {
                        $scope.assetRepair = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetRepairUpdate', function(event, result) {
                    $scope.assetRepair = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*------Ending-----assetRepair-detail.controller.js-----------*/


/*-------Starting------assetRepair-dialog.controller.js----------*/

angular.module('stepApp').controller('AssetRepairDialogController',
    ['$scope', '$rootScope','$stateParams','entity','AssetRepair','$state','AssetRecord','Employee','AssetDistribution',
        function($scope, $rootScope, $stateParams, entity, AssetRepair,$state, AssetRecord, Employee, AssetDistribution) {
            $scope.AssetValueChange={};
            $scope.assetRepair = entity;
            $scope.assetRepairInfo = {};
            $scope.employees = Employee.query();
            $scope.assetdistributions = AssetDistribution.query();
            $scope.assetRecords = AssetRecord.query();
            $scope.load = function(id) {
                AssetRepair.get({id : id}, function(result) {
                    $scope.assetRepair = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:assetRepairUpdate', result);

                $state.go('assetRepair', null,{ reload: true }),
                    function (){
                        $state.go('assetRepair');
                    };
            };

            $scope.getAssetRecords = function () {
                AssetRecord.get({id: $scope.AssetRepair.assetCode.id}, function (result) {
                    $scope.assetRecordDetails = result;
                    $scope.assetRepair.assetName = result.assetName;

                });
            };

            $scope.refNoAssign = function(ref){
                $scope.assetRepair.refNo = ref;
            };

            $scope.checkingDate = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Repair Date will be greater then Date Of Problem !')
                    $scope.assetRepair.repairDate=null;
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.AssetValueChange = function(CodeOfAsset){
                angular.forEach($scope.assetdistributions, function(code){
                    if(CodeOfAsset == code.id){
                        $scope.assetdistributions.assetNam = code.assetRecord.assetName;
                        $scope.assetdistributions.purchaseDate = code.assetRecord.purchaseDate;
                        $scope.assetdistributions.vendorName = code.assetRecord.vendorName;
                    }
                })
            };

            $scope.save = function () {
                if ($scope.assetRepair.id != null) {
                    AssetRepair.update($scope.assetRepair, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetRepair.updated');
                } else {
                    AssetRepair.save($scope.assetRepair, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetRepair.created');
                }
            };

            $scope.clear = function() {

            };
        }]);

/*-------Ending------assetRepair-dialog.controller.js----------*/

