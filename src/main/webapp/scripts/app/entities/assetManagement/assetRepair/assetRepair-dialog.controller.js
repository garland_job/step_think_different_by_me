 /*
'use strict';

angular.module('stepApp').controller('AssetRepairDialogController',
    ['$scope', '$rootScope','$stateParams','entity','AssetRepair','$state','AssetRecord','Employee','AssetDistribution',
        function($scope, $rootScope, $stateParams, entity, AssetRepair,$state, AssetRecord, Employee, AssetDistribution) {
        $scope.AssetValueChange={};
        $scope.assetRepair = entity;
        $scope.assetRepairInfo = {};
        $scope.employees = Employee.query();
        $scope.assetdistributions = AssetDistribution.query();
        $scope.assetRecords = AssetRecord.query();
        $scope.load = function(id) {
            AssetRepair.get({id : id}, function(result) {
                $scope.assetRepair = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:assetRepairUpdate', result);

            $state.go('assetRepair', null,{ reload: true }),
                function (){
                    $state.go('assetRepair');
                };
        };

            $scope.getAssetRecords = function () {
                AssetRecord.get({id: $scope.AssetRepair.assetCode.id}, function (result) {
                    console.log(result);
                    $scope.assetRecordDetails = result;
                    $scope.assetRepair.assetName = result.assetName;

                });
            }


            $scope.refNoAssign = function(ref){
                console.log('--ref---');
                console.log(ref);
                $scope.assetRepair.refNo = ref;
            };

            $scope.checkingDate = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Repair Date will be greater then Date Of Problem !')
                    $scope.assetRepair.repairDate=null;
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.AssetValueChange = function(CodeOfAsset){

                angular.forEach($scope.assetdistributions, function(code){

                    if(CodeOfAsset == code.id){
                        $scope.assetdistributions.assetNam = code.assetRecord.assetName;
                        $scope.assetdistributions.purchaseDate = code.assetRecord.purchaseDate;
                        $scope.assetdistributions.vendorName = code.assetRecord.vendorName;


                    }

                })

                console.log( $scope.assetdistributions.empName);
                console.log($scope.assetdistributions.empDepartment);
            };

            $scope.save = function () {

                console.log($scope.assetRepair);

            if ($scope.assetRepair.id != null) {
                AssetRepair.update($scope.assetRepair, onSaveFinished);
                $rootScope.setWarningMessage('stepApp.assetRepair.updated');
            } else {
                AssetRepair.save($scope.assetRepair, onSaveFinished);
                $rootScope.setSuccessMessage('stepApp.assetRepair.created');
            }
        };

        $scope.clear = function() {

        };
}]);
*/
