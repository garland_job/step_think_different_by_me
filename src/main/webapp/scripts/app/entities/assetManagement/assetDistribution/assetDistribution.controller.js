'use strict';

angular.module('stepApp')
    .controller('AssetDistributionController',
    ['$scope', 'AssetDistribution', 'AssetDistributionSearch', 'ParseLinks',
    function ($scope, AssetDistribution, AssetDistributionSearch, ParseLinks) {
        $scope.assetDistributions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AssetDistribution.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetDistributions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AssetDistribution.get({id: id}, function(result) {
                $scope.assetDistribution = result;
                $('#deleteAssetDistributionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AssetDistribution.delete({id: id},
                function () {
                    $scope.loadAll();
                    $rootScope.setErrorMessage('stepApp.assetDistribution.deleted');
                    $('#deleteAssetDistributionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AssetDistributionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.assetDistributions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assetDistribution = {
                transferRef: null,
                assignedDdate: null,
                remartks: null,
                id: null
            };
        };
    }]);

/*-----Starting------assetDistribution.js-------------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assetDistribution', {
                parent: 'assetManagements',
                url: '/assetDistributions',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDistribution.home.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDistribution/assetDistributions.html',
                        controller: 'AssetDistributionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDistribution');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('assetDistribution.detail', {
                parent: 'assetDistribution',
                url: '/assetDistribution/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDistribution.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDistribution/assetDistribution-detail.html',
                        controller: 'AssetDistributionDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDistribution');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetDistribution', function ($stateParams, AssetDistribution) {
                        return AssetDistribution.get({id: $stateParams.id});
                    }]
                }
            })
            .state('assetDistribution.new', {
                parent: 'assetDistribution',
                url: '/new/{reqId}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDistribution.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDistribution/assetDistribution-dialog.html',
                        controller: 'AssetDistributionDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDistribution');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            transferRef: 'Auto ID',

                            assignedDdate: null,
                            employeeid: null,
                            createDate: null,
                            createBy: null,
                            updatedBy: null,
                            updatedTime: null,
                            id: null
                        };
                    }
                }
            })
            .state('assetDistribution.admin', {
                parent: 'assetDistribution',
                url: '/admin/{reqId}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDistribution.detail.title'
                },
                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDistribution/assetDistributionAdmin-dialog.html',
                        controller: 'AssetDistributionDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDistribution');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            transferRef: 'Auto ID',

                            assignedDdate: null,
                            employeeid: null,
                            createDate: null,
                            createBy: null,
                            updatedBy: null,
                            updatedTime: null,
                            availableQuantity: null,
                            id: null
                        };
                    }
                }
            })
            .state('assetDistribution.edit', {
                parent: 'assetDistribution',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.assetDistribution.detail.title'
                },


                views: {
                    'assetManagementView@assetManagements': {
                        templateUrl: 'scripts/app/entities/assetManagement/assetDistribution/assetDistribution-dialog.html',
                        controller: 'AssetDistributionDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('assetDistribution');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AssetDistribution', function ($stateParams, AssetDistribution) {
                        return AssetDistribution.get({id: $stateParams.id});
                    }]
                }
            });
    });

/*-----Ending------assetDistribution.js-------------*/

/*------Starting---------assetDistribution-detail.controller.js---------*/

angular.module('stepApp')
    .controller('AssetDistributionDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AssetDistribution', 'Employee', 'AssetRecord',
            function ($scope, $rootScope, $stateParams, entity, AssetDistribution, Employee, AssetRecord) {
                $scope.assetDistribution = entity;
                $scope.load = function (id) {
                    AssetDistribution.get({id: id}, function(result) {
                        $scope.assetDistribution = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:assetDistributionUpdate', function(event, result) {
                    $scope.assetDistribution = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*------Ending---------assetDistribution-detail.controller.js---------*/

/*-----Starting-----assetDistribution-dialog.controller.js----------*/

angular.module('stepApp').controller('AssetDistributionDialogController',
    ['$rootScope', '$scope', '$stateParams', 'entity', 'AssetDistribution', '$state', 'HrEmployeeInfo', 'AssetRecord', 'ParseLinks', 'HrEmployeeInfoByEmployeeId', 'assetRequisitionsByRefId','Institute', 'AssetRecordQuantity', 'AssetRequisitionUpdatedStatus',
        function ($rootScope, $scope, $stateParams, entity, AssetDistribution, $state, HrEmployeeInfo, AssetRecord, ParseLinks, HrEmployeeInfoByEmployeeId, assetRequisitionsByRefId, Institute,AssetRecordQuantity,AssetRequisitionUpdatedStatus) {

            $scope.assetRequisition = [];
            $scope.AssetRecordQuantity = [];
            $scope.AssetRequisitionUpdatedStatus = [];
            $scope.hrmEmployee = [];
            $scope.hrmEmployees = [];
            $scope.getHrmEmployee = [];

            $scope.assetRecord = {};
            $scope.assetRecords = {};
            $scope.assetDistribution = entity;
            $scope.assetInstitutes = [];

            $rootScope.assetRefId=$stateParams.reqId;
            $scope.employeeGlobal =$stateParams.reqId;
            var assetCode = '';
            var requisitionId = $stateParams.reqId;
            Institute.query({page: $scope.page, size: 3000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetInstitutes = result;
            });

            AssetRecord.query({page: $scope.page, size: 3000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetRecords = result;
            });

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.getAssetDetails = function (assetRecordId) {
                $scope.id=assetRecordId;
                AssetRecord.get({id : assetRecordId}, function(result) {
                    $scope.assetRecord = result;
                    $scope.assetDistribution.assetRecord  = $scope.assetRecord;
                    $scope.assetDistribution.assetCode  = $scope.assetRecord.recordCode;
                    $scope.assetDistribution.assetName  = $scope.assetRecord.assetName;
                    $scope.assetDistribution.assetStatus  = $scope.assetRecord.status;
                    $scope.assetDistribution.availableQuantity  = $scope.assetRecord.quantity;
                    $scope.assetDistribution.transferRef = $scope.assetRecord.referenceNo;
                });
            }

            if($scope.employeeGlobal != null){
                assetRequisitionsByRefId.get({refId: $scope.employeeGlobal}, function (result, headers) {
                    $scope.assetRequisition = result;

                    $scope.assetDistribution.requisitonId = $rootScope.assetRefId;
                    $scope.assetDistribution.transferRef = $scope.assetRequisition.requisitionId;
                    $scope.assetDistribution.requestedQuantity = $scope.assetRequisition.quantity;

                    AssetRecord.get({id : $scope.assetRequisition.assetRecord.id}, function(result) {
                        $scope.id=$scope.assetRequisition.assetRecord.id;
                        $scope.assetRecord = result;
                        $scope.assetDistribution.assetRecord  = $scope.assetRecord;
                        $scope.assetDistribution.assetCode  = $scope.assetRecord.recordCode;
                        $scope.assetDistribution.assetName  = $scope.assetRecord.assetName;
                        $scope.assetDistribution.assetStatus  = $scope.assetRecord.status;
                        $scope.assetDistribution.availableQuantity  = $scope.assetRecord.quantity;;

                    });

                    HrEmployeeInfoByEmployeeId.get({id: $scope.assetRequisition.empId}, function (result) {
                        $scope.assetDistribution.empId = $scope.assetRequisition.empId;
                        $scope.getHrmEmployee = result;
                        $scope.assetDistribution.emploeeIds = $scope.assetRequisition.empId;
                        if ($scope.getHrmEmployee.fullName != null) {
                            $scope.assetDistribution.empName = $scope.getHrmEmployee.fullName;
                        } else {
                            $scope.assetDistribution.empName = 'Not Found';
                        }

                        if ($scope.getHrmEmployee.designationInfo.designationInfo.designationName != null) {
                            $scope.assetDistribution.designation = $scope.getHrmEmployee.designationInfo.designationInfo.designationName;
                        }
                        else {
                            $scope.assetDistribution.designation = 'Not Found';
                        }

                        if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                            $scope.assetDistribution.department = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                        }
                        else {
                            $scope.assetDistribution.department = 'Not Found';
                        }

                    });

                });
            }

            $scope.getEmployee = function (id) {
                HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                    $scope.getHrmEmployee = result;

                    if($scope.assetRequisition.empId!=null){
                        $scope.assetDistribution.emploeeIds = $scope.assetRequisition.empId;
                    }else{
                        $scope.assetDistribution.emploeeIds = id;
                    }
                    if ($scope.getHrmEmployee.fullName!= null) {
                        $scope.assetDistribution.empName = $scope.getHrmEmployee.fullName;
                    } else {
                        $scope.assetDistribution.empName = 'Not Found';
                    }

                    if ($scope.getHrmEmployee.designationInfo.designationInfo.designationName != null) {
                        $scope.assetDistribution.designation = $scope.getHrmEmployee.designationInfo.designationInfo.designationName;
                    }
                    else {
                        $scope.assetDistribution.designation = 'Not Found';
                    }

                    if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                        $scope.assetDistribution.department = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                    }
                    else {
                        $scope.assetDistribution.department = 'Not Found';
                    }

                });
            }

            $scope.AssetValueChange = function (CodeOfAsset) {
                angular.forEach($scope.assetrecords, function (code) {
                    if (CodeOfAsset == code.id) {
                        $scope.assetrecords.assetName = code.assetName;
                        $scope.assetrecords.assetStatus = code.status;
                    }
                })
            };

            $scope.deductionRecordQuantity = function(availableQty,approveQty,assetCoded){
                if(availableQty<approveQty){
                    alert('Approved/Assign Quantity should be less then available quantity!');
                $scope.assetDistribution.approveQuantity ='';
                }
                assetCode = assetCoded
                $scope.quantity = availableQty-approveQty;

            };

            var onSaveSuccess = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
                console.log(JSON.stringify(result));
            };

            var onSaveFinished = function (result) {
                AssetRequisitionUpdatedStatus.query({id: requisitionId}, function () {
                    $scope.assetRequisition, onSaveSuccess;

                });
                AssetRecordQuantity.get({id:$scope.id,assetCode : assetCode,quantity:$scope.quantity}, function(result) {
                });
                $scope.$emit('stepApp:assetDistributionUpdate', result);
                $state.go('assetDistribution');

            };
            $scope.save = function () {
                if ($scope.assetDistribution.id != null) {
                    AssetDistribution.update($scope.assetDistribution, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetDistribution.updated');
                } else {

                    AssetDistribution.save($scope.assetDistribution, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetDistribution.created');
                    console.log($scope.assetDistribution);
                }
            };

            $scope.clear = function () {
                $state.go('assetDistribution');
            };

        }]);


/*-----Ending-----assetDistribution-dialog.controller.js----------*/
