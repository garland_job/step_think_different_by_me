 /*
'use strict';

angular.module('stepApp').controller('AssetDistributionDialogController',
    ['$rootScope', '$scope', '$stateParams', 'entity', 'AssetDistribution', '$state', 'HrEmployeeInfo', 'AssetRecord', 'ParseLinks', 'HrEmployeeInfoByEmployeeId', 'assetRequisitionsByRefId','Institute', 'AssetRecordQuantity',
        function ($rootScope, $scope, $stateParams, entity, AssetDistribution, $state, HrEmployeeInfo, AssetRecord, ParseLinks, HrEmployeeInfoByEmployeeId, assetRequisitionsByRefId, Institute,AssetRecordQuantity) {

            $scope.assetRequisition = [];
            $scope.AssetRecordQuantity = [];
            $scope.hrmEmployee = [];
            $scope.hrmEmployees = [];
            $scope.getHrmEmployee = [];

            $scope.assetRecord = {};
            $scope.assetRecords = {};
            $scope.assetDistribution = entity;
            $scope.assetInstitutes = [];

            console.log('REF ID: '+$stateParams.reqId);
            $rootScope.assetRefId=$stateParams.reqId;
            $scope.employeeGlobal =$stateParams.reqId;
            var assetCode = '';

            Institute.query({page: $scope.page, size: 3000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetInstitutes = result;
            });

            AssetRecord.query({page: $scope.page, size: 3000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assetRecords = result;
            });

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.getAssetDetails = function (assetRecordId) {
                $scope.id=assetRecordId;
                AssetRecord.get({id : assetRecordId}, function(result) {
                    $scope.assetRecord = result;
                    $scope.assetDistribution.assetRecord  = $scope.assetRecord;
                    $scope.assetDistribution.assetCode  = $scope.assetRecord.recordCode;
                    $scope.assetDistribution.assetName  = $scope.assetRecord.assetName;
                    $scope.assetDistribution.assetStatus  = $scope.assetRecord.status;
                    $scope.assetDistribution.availableQuantity  = $scope.assetRecord.quantity;
                    $scope.assetDistribution.transferRef = $scope.assetRecord.referenceNo;
                });
            }

            if($scope.employeeGlobal != null){
                assetRequisitionsByRefId.get({refId: $scope.employeeGlobal}, function (result, headers) {
                    $scope.assetRequisition = result;

                    $scope.assetDistribution.requisitonId = $rootScope.assetRefId;
                    $scope.assetDistribution.transferRef = $scope.assetRequisition.requisitionId;
                    $scope.assetDistribution.requestedQuantity = $scope.assetRequisition.quantity;

                    AssetRecord.get({id : $scope.assetRequisition.assetRecord.id}, function(result) {
                        $scope.id=$scope.assetRequisition.assetRecord.id;
                        $scope.assetRecord = result;
                        $scope.assetDistribution.assetRecord  = $scope.assetRecord;
                        $scope.assetDistribution.assetCode  = $scope.assetRecord.recordCode;
                        $scope.assetDistribution.assetName  = $scope.assetRecord.assetName;
                        $scope.assetDistribution.assetStatus  = $scope.assetRecord.status;
                        $scope.assetDistribution.availableQuantity  = $scope.assetRecord.quantity;;

                    });

                    HrEmployeeInfoByEmployeeId.get({id: $scope.assetRequisition.empId}, function (result) {
                        $scope.assetDistribution.empId = $scope.assetRequisition.empId;
                        $scope.getHrmEmployee = result;
                        $scope.assetDistribution.emploeeIds = $scope.assetRequisition.empId;
                        if ($scope.getHrmEmployee.fullName != null) {
                            $scope.assetDistribution.empName = $scope.getHrmEmployee.fullName;
                        } else {
                            $scope.assetDistribution.empName = 'Not Found';
                        }

                        if ($scope.getHrmEmployee.designationInfo.designationInfo.designationName != null) {
                            $scope.assetDistribution.designation = $scope.getHrmEmployee.designationInfo.designationInfo.designationName;
                        }
                        else {
                            $scope.assetDistribution.designation = 'Not Found';
                        }

                        if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                            $scope.assetDistribution.department = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                        }
                        else {
                            $scope.assetDistribution.department = 'Not Found';
                        }

                    });

                });
            }

            $scope.getEmployee = function (id) {
                HrEmployeeInfoByEmployeeId.get({id: id}, function (result) {
                    $scope.getHrmEmployee = result;

                    if($scope.assetRequisition.empId!=null){
                        $scope.assetDistribution.emploeeIds = $scope.assetRequisition.empId;
                    }else{
                        $scope.assetDistribution.emploeeIds = id;
                    }
                    if ($scope.getHrmEmployee.fullName!= null) {
                        $scope.assetDistribution.empName = $scope.getHrmEmployee.fullName;
                    } else {
                        $scope.assetDistribution.empName = 'Not Found';
                    }

                    if ($scope.getHrmEmployee.designationInfo.designationInfo.designationName != null) {
                        $scope.assetDistribution.designation = $scope.getHrmEmployee.designationInfo.designationInfo.designationName;
                    }
                    else {
                        $scope.assetDistribution.designation = 'Not Found';
                    }

                    if ($scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName != null) {
                        $scope.assetDistribution.department = $scope.getHrmEmployee.departmentInfo.departmentInfo.departmentName;
                    }
                    else {
                        $scope.assetDistribution.department = 'Not Found';
                    }

                });
            }

            $scope.AssetValueChange = function (CodeOfAsset) {
                angular.forEach($scope.assetrecords, function (code) {
                    if (CodeOfAsset == code.id) {
                        $scope.assetrecords.assetName = code.assetName;
                        $scope.assetrecords.assetStatus = code.status;
                    }
                })
                console.log($scope.assetrecords.assetStatus);
            };

            $scope.deductionRecordQuantity = function(availableQty,approveQty,assetCoded){
                 assetCode = assetCoded
                $scope.quantity = availableQty-approveQty;
                console.log('----------Quantity------------');
                console.log($scope.quantity);

            };

            var onSaveFinished = function (result) {
                console.log($scope.id);
                AssetRecordQuantity.get({id:$scope.id,assetCode : assetCode,quantity:$scope.quantity}, function(result) {
                });
                $scope.$emit('stepApp:assetDistributionUpdate', result);
                $state.go('assetDistribution');

            };
            $scope.save = function () {

                if ($scope.assetDistribution.id != null) {

                    AssetDistribution.update($scope.assetDistribution, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.assetDistribution.updated');
                } else {

                    AssetDistribution.save($scope.assetDistribution, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.assetDistribution.created');
                    console.log($scope.assetDistribution);
                }
            };

            $scope.clear = function () {
                $state.go('assetDistribution');
            };

        }]);
*/
