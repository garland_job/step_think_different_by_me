'use strict';

angular.module('stepApp')
    .controller('JobController',
     ['$scope', '$state', '$modal', 'Job', 'JobSearch', 'ParseLinks', 'ArchiveJobs','ArchiveJobsForJpadmin','Principal',
     function ($scope, $state, $modal, Job, JobSearch, ParseLinks, ArchiveJobs, ArchiveJobsForJpadmin, Principal) {

        $scope.jobs = [];
        $scope.page = 0;

        $scope.loadAll = function () {
            if(Principal.hasAnyAuthority(['ROLE_JPADMIN'])){
                ArchiveJobsForJpadmin.query({page: $scope.page, size: 5000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.jobs = result;
                    $scope.total = headers('x-total-count');
                });
            }else{
                ArchiveJobs.query({page: $scope.page, size: 5000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.jobs = result;
                    $scope.total = headers('x-total-count');
                });
            }

        };

        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.loadAll();

        $scope.search = function () {
            JobSearch.query({query: $scope.searchQuery}, function (result) {
                $scope.jobs = result;
            }, function (response) {
                if (response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.job = {
                title: null,
                type: null,
                minimumSalary: null,
                maximumSalary: null,
                vacancy: null,
                description: null,
                benefits: null,
                educationRequirements: null,
                experienceRequirements: null,
                otherRequirements: null,
                publishedAt: null,
                applicationDeadline: null,
                location: null,
                status: null,
                id: null
            };
        };

        // bulk operations start
        $scope.areAllJobsSelected = false;

        $scope.updateJobsSelection = function (jobArray, selectionValue) {
            for (var i = 0; i < jobArray.length; i++) {
                jobArray[i].isSelected = selectionValue;
            }
        };


        $scope.import = function () {
            for (var i = 0; i < $scope.jobs.length; i++) {
                var job = $scope.jobs[i];
                if (job.isSelected) {
                    //Job.update(job);
                    //TODO: handle bulk export
                }
            }
        };

        $scope.export = function () {
            for (var i = 0; i < $scope.jobs.length; i++) {
                var job = $scope.jobs[i];
                if (job.isSelected) {
                    //Job.update(job);
                    //TODO: handle bulk export
                }
            }
        };

        $scope.deleteSelected = function () {
            for (var i = 0; i < $scope.jobs.length; i++) {
                var job = $scope.jobs[i];
                if (job.isSelected) {
                    Job.delete(job);
                }
            }
        };

        $scope.sync = function () {
            for (var i = 0; i < $scope.jobs.length; i++) {
                var job = $scope.jobs[i];
                if (job.isSelected) {
                    Job.update(job);
                }
            }
        };

        $scope.order = function (predicate, reverse) {
            $scope.predicate = predicate;
            $scope.reverse = reverse;
            Job.query({page: $scope.page, size: 5000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.jobs = result;
                $scope.total = headers('x-total-count');
            });
        };
        // bulk operations end

    }]);
/* create-job.controller.js */
angular.module('stepApp').controller('CreateJobController',
    ['$scope', '$stateParams', 'entity', 'Job', 'Cat', 'Employer', 'Country', 'User',
        function($scope, $stateParams, entity, Job, Cat, Employer, Country, User) {

            $scope.job = entity;
            $scope.cats = Cat.query({size: 500});
            $scope.employers = Employer.query({size: 500});
            $scope.countrys = Country.query({size: 500});
            $scope.users = User.query({size: 500});
            $scope.load = function(id) {
                Job.get({id : id}, function(result) {
                    $scope.job = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:jobUpdate', result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.job.id != null) {
                    Job.update($scope.job, onSaveSuccess, onSaveError);
                } else {
                    Job.save($scope.job, onSaveSuccess, onSaveError);
                }
            };
        }]);

/* job-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('JobDeleteController',
        ['$scope', '$modalInstance', 'entity', 'Job',
            function($scope, $modalInstance, entity, Job) {

                $scope.job = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    Job.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*  job-detail.controller.js */

angular.module('stepApp')
    .controller('JobDetailController',
        ['$scope', '$rootScope', '$stateParams', 'Principal', 'entity', 'EmployeeJobApplication', 'Job',
            function ($scope, $rootScope, $stateParams, Principal, entity, EmployeeJobApplication, Job) {
                $scope.job = entity;
                $scope.canApply=false;
                $scope.load = function (id) {
                    Job.get({id: id}, function(result) {
                        console.log('job: '+result);
                        $scope.job = result;
                    });
                };
                $scope.users = [];
                $scope.authorities = ["ROLE_USER", "ROLE_ADMIN", "ROLE_EMPLOYER"];
                $scope.userRole = 'ROLE_USER';

                Principal.identity().then(function (account) {
                    if(account != null) {
                        $scope.account = account;

                        if($scope.isInArray('ROLE_ADMIN', $scope.account.authorities))
                        {
                            $scope.userRole = 'ROLE_ADMIN';
                        }
                        else if($scope.isInArray('ROLE_EMPLOYER', $scope.account.authorities))
                        {
                            $scope.userRole = 'ROLE_EMPLOYER';
                        }
                        else if($scope.isInArray('ROLE_JOBSEEKER', $scope.account.authorities))
                        {
                            $scope.canApply=true;
                            $scope.userRole = 'ROLE_USER';
                        }
                    }else{
                        $scope.canApply=true;
                    }
                });
                $scope.isInArray = function isInArray(value, array) {
                    return array.indexOf(value) > -1;
                };

                var unsubscribe = $rootScope.$on('stepApp:jobUpdate', function(event, result) {
                    $scope.job = result;
                });

                $scope.$on('$destroy', unsubscribe);

                EmployeeJobApplication.query({id:'my'},function(result){
                    $scope.empJobs = result;
                    console.log($scope.empJobs);

                    for(var i=0; i<$scope.empJobs.length; i++){
                        if($scope.empJobs[i].job.id==$scope.jobID){
                            $scope.canApply=false;
                        }
                    }
                });

            }]);

/* job-dialog.controller.js */
angular.module('stepApp').controller('JobDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Job', 'Cat', 'Employer', 'Country', 'User',
        function ($scope, $stateParams, $modalInstance, entity, Job, Cat, Employer, Country, User) {

            $scope.job = entity;
            $scope.cats = Cat.query({size: 500});
            $scope.employers = Employer.query({size: 500});
            $scope.countrys = Country.query({size: 500});
            $scope.users = User.query({size: 500});

            $scope.load = function (id) {
                Job.get({id: id}, function (result) {
                    $scope.job = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:jobUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.job.id != null) {
                    Job.update($scope.job, onSaveSuccess, onSaveError);
                } else {
                    Job.save($scope.job, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
            };

        }]);
