'use strict';

angular.module('stepApp')
    .controller('PayScaleController',
    ['$scope', '$state', '$modal', 'PayScale', 'PayScaleSearch', 'ParseLinks',
    function ($scope, $state, $modal, PayScale, PayScaleSearch, ParseLinks) {

        $scope.payScales = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PayScale.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.payScales = result;
                $scope.total = headers('x-total-count');
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            PayScaleSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.payScales = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.payScale = {
                code: null,
                payScaleClass: null,
                grade: null,
                gradeName: null,
                basicAmount: null,
                houseAllowance: null,
                medicalAllowance: null,
                welfareAmount: null,
                retirementAmount: null,
                date: null,
                id: null
            };
        };
    }]);

/* payScale-form.controller.js */
angular.module('stepApp').controller('PayScaleFormController',
    ['$scope', '$state', '$stateParams', 'entity', 'PayScale', 'User','$rootScope', 'GazetteSetup',
        function($scope, $state, $stateParams, entity, PayScale, User,$rootScope, GazetteSetup) {

            $scope.payScale = entity;
            $scope.users = User.query();
            $scope.gazettesetup = GazetteSetup.query();
            $scope.load = function(id) {
                PayScale.get({id : id}, function(result) {
                    $scope.payScale = result;
                    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+$scope.payScale);
                });
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:payScaleUpdate', result);
                $scope.isSaving = false;
                $state.go('payScale');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $rootScope.setErrorMessage('Your Input is not correct.');
                $state.go('payScale');
            };

            $scope.save = function () {
                console.log($scope.payScale);
                $scope.isSaving = true;
                if ($scope.payScale.id != null) {
                    PayScale.update($scope.payScale, onSaveSuccess, onSaveError);
                } else {
                    PayScale.save($scope.payScale, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {

            };
        }]);

/* payScale-detail.controller.js */
angular.module('stepApp')
    .controller('PayScaleDetailController',
        ['$scope','$rootScope','$stateParams','entity','PayScale','User',
            function ($scope, $rootScope, $stateParams, entity, PayScale, User) {
                $scope.payScale = entity;
                $scope.load = function (id) {
                    PayScale.get({id: id}, function(result) {
                        $scope.payScale = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:payScaleUpdate', function(event, result) {
                    $scope.payScale = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*payScale-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('PayScaleDeleteController',
        ['$scope','$state','$modalInstance','entity','PayScale',
            function($scope, $state, $modalInstance, entity, PayScale) {

                $scope.payScale = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    PayScale.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $state.go('payScale');
                        });
                };

            }]);
