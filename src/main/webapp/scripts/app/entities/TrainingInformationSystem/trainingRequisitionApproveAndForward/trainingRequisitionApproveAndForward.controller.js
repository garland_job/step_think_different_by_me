'use strict';

angular.module('stepApp')
    .controller('TrainingRequisitionApproveAndForwardController', function ($scope, TrainingRequisitionApproveAndForward, TrainingRequisitionApproveAndForwardSearch, ParseLinks) {
        $scope.trainingRequisitionApproveAndForwards = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TrainingRequisitionApproveAndForward.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.trainingRequisitionApproveAndForwards = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainingRequisitionApproveAndForward.get({id: id}, function(result) {
                $scope.trainingRequisitionApproveAndForward = result;
                $('#deleteTrainingRequisitionApproveAndForwardConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainingRequisitionApproveAndForward.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainingRequisitionApproveAndForwardConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TrainingRequisitionApproveAndForwardSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.trainingRequisitionApproveAndForwards = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainingRequisitionApproveAndForward = {
                approveStatus: null,
                approveByAuthority: null,
                forwardToAuthority: null,
                logComments: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*trainingRequisitionApproveAndForward-dialog.controller.js*/

angular.module('stepApp')
    .controller('TrainingRequisitionApproveAndForwardDialogController',
        ['$scope', '$stateParams', '$modalInstance', 'entity', 'TrainingRequisitionApproveAndForward', 'TrainingRequisitionForm','Principal',
            function($scope, $stateParams, $modalInstance, entity, TrainingRequisitionApproveAndForward, TrainingRequisitionForm,Principal) {

                $scope.trainingRequisitionApproveAndForward = entity;
                $scope.trainingrequisitionforms = TrainingRequisitionForm.query();
                $scope.trainingRequisitionApproveAndForward.forwardToAuthority = "DG";
                $scope.forwardAuthority = true;

                $scope.load = function(id) {
                    TrainingRequisitionApproveAndForward.get({id : id}, function(result) {
                        $scope.trainingRequisitionApproveAndForward = result;
                    });
                };

                if(Principal.hasAnyAuthority(['ROLE_DG'])){
                    $scope.trainingRequisitionApproveAndForward.forwardToAuthority = "User";
                    $scope.forwardAuthority = false;
                }

                TrainingRequisitionForm.get({id:$stateParams.trainingReqId},function(result){
                    $scope.trainingRequisitionApproveAndForward.trainingRequisitionForm = result;
                });

                var onSaveFinished = function (result) {
                    $scope.$emit('stepApp:trainingRequisitionApproveAndForwardUpdate', result);
                    $modalInstance.close(result);
                };

                $scope.save = function () {
                    if ($scope.trainingRequisitionApproveAndForward.id != null) {
                        TrainingRequisitionApproveAndForward.update($scope.trainingRequisitionApproveAndForward, onSaveFinished);
                    } else {
                        TrainingRequisitionApproveAndForward.save($scope.trainingRequisitionApproveAndForward, onSaveFinished);
                    }
                };

                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
            }])
    .controller('TrainingRequisitionDeclineController',
        ['$scope','$stateParams' ,'$modalInstance', 'TrainingRequisitionApproveAndForward', '$state', 'entity', 'TrainingRequisitionForm','TrainingRequisitionDecline',
            function($scope,$stateParams ,$modalInstance, TrainingRequisitionApproveAndForward, $state, entity, TrainingRequisitionForm,TrainingRequisitionDecline) {

                $scope.decline = function(){
                    TrainingRequisitionForm.get({id:$stateParams.trainingReqId},function(result){
                        $scope.trainingRequisitionApproveAndForward.trainingRequisitionForm = result;
                        $scope.trainingRequisitionApproveAndForward.comments = "Training Requisition Decline";
                        TrainingRequisitionDecline.save($scope.trainingRequisitionApproveAndForward,onSaveFinished);
                    });
                };

                var onSaveFinished = function (result) {
                    $modalInstance.close();
                    $state.go('trainingInfo.reqPendingList',{},{reload:true});
                };

                $scope.clear = function() {
                    $modalInstance.close();
                };
            }])
    .controller('TrainingRequisitionRejectController',
        ['$scope','$state','$stateParams','$modalInstance', 'TrainingRequisitionApproveAndForward', 'entity', 'TrainingRequisitionReject','TrainingRequisitionForm',
            function($scope,$state,$stateParams, $modalInstance, TrainingRequisitionApproveAndForward, entity, TrainingRequisitionReject,TrainingRequisitionForm) {
                $scope.trainingRequisitionApproveAndForward = entity;

                var onSaveSuccess = function (result) {
                    $modalInstance.close(result);
                    $scope.isSaving = false;
                    $state.go('trainingInfo.reqPendingList',{},{reload:true});
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function() {
                    $modalInstance.close();
                };
                $scope.confirmReject = function () {
                    $scope.isSaving = true;
                    console.log('Reject-- '+$stateParams.reqID);
                    TrainingRequisitionForm.get({id:$stateParams.reqID},function(result){

                        $scope.trainingRequisitionApproveAndForward.trainingRequisitionForm = result;
                        TrainingRequisitionReject.save($scope.trainingRequisitionApproveAndForward, onSaveSuccess, onSaveError);
                    });
                };
            }]);
/*trainingRequisitionApproveAndForward-detail.controller.js*/

angular.module('stepApp')
    .controller('TrainingRequisitionApproveAndForwardDetailController', function ($scope, $rootScope, $stateParams, entity, TrainingRequisitionApproveAndForward, TrainingRequisitionForm) {
        $scope.trainingRequisitionApproveAndForward = entity;
        $scope.load = function (id) {
            TrainingRequisitionApproveAndForward.get({id: id}, function(result) {
                $scope.trainingRequisitionApproveAndForward = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:trainingRequisitionApproveAndForwardUpdate', function(event, result) {
            $scope.trainingRequisitionApproveAndForward = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*trainingRequisitionApprovedList.controller.js*/

angular.module('stepApp')
    .controller('TrainingRequisitionApprovedListController',
        function ($scope,$stateParams, TrainingRequisitionForm,ParseLinks,TrainingRequisitionByApproveStatus) {
            $scope.trainingRequisitionForms = [];
            $scope.page = 0;
            $scope.statusFind = '';
            $scope.loadAll = function() {
                TrainingRequisitionByApproveStatus.query({approveStatus:$stateParams.status}, function (result, headers) {
                    $scope.trainingRequisitionForms = result;
                    if($stateParams.status == 3){
                        $scope.statusFind = 3;
                    }else if ($stateParams.status == 0){
                        $scope.statusFind = 0;
                    }else if ($stateParams.status == -1){
                        $scope.statusFind = -1;
                    }else{
                        $scope.statusFind = '';
                    }
                });
            };

            $scope.loadPage = function(page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            $scope.search = function () {
                TrainingRequisitionFormSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.trainingRequisitionForms = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };
            $scope.clear = function () {
                $scope.trainingRequisitionForm = {
                    requisitionCode: null,
                    trainingType: null,
                    session: null,
                    applyDate: null,
                    reason: null,
                    fileName: null,
                    file: null,
                    fileContentType: null,
                    fileContentName: null,
                    applyBy: null,
                    instituteId: null,
                    status: null,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };
            //
            //$scope.byteSize = function (base64String) {
            //    if (!angular.isString(base64String)) {
            //        return '';
            //    }
            //    function endsWith(suffix, str) {
            //        return str.indexOf(suffix, str.length - suffix.length) !== -1;
            //    }
            //    function paddingSize(base64String) {
            //        if (endsWith('==', base64String)) {
            //            return 2;
            //        }
            //        if (endsWith('=', base64String)) {
            //            return 1;
            //        }
            //        return 0;
            //    }
            //    function size(base64String) {
            //        return base64String.length / 4 * 3 - paddingSize(base64String);
            //    }
            //    function formatAsBytes(size) {
            //        return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            //    }
            //
            //    return formatAsBytes(size(base64String));
            //};
        });
