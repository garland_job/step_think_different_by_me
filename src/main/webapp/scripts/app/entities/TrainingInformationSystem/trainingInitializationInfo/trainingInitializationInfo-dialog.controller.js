/*
'use strict';

angular.module('stepApp').controller('TrainingInitializationInfoDialogController',
    ['$scope', '$state', '$rootScope', '$stateParams','entity', 'TrainingInitializationInfo', 'TrainingHeadSetup', 'TrainingRequisitionForm','TrainingReqDataByReqCode',
        'TrainerInformation','Division','District','DistrictsByDivision','HrEmployeeInfoByEmployeeId','HrEmployeeInfo','TraineeListByRequisition','TrainerListByInitializationId','GetAllTrainingHeadByStatus',
        function($scope, $state, $rootScope, $stateParams, entity, TrainingInitializationInfo, TrainingHeadSetup, TrainingRequisitionForm,TrainingReqDataByReqCode,
                 TrainerInformation,Division,District, DistrictsByDivision,HrEmployeeInfoByEmployeeId,HrEmployeeInfo,TraineeListByRequisition,TrainerListByInitializationId,GetAllTrainingHeadByStatus) {

        $scope.trainingInitializationInfo = entity;
        $scope.trainingheadsetups = GetAllTrainingHeadByStatus.query({status:true});
        $scope.trainingrequisitionforms = TrainingRequisitionForm.query();
        $scope.divisions = Division.query();
        $scope.timeScheduleDiv = true;
        $scope.trainingName = '';
        $scope.applyDate = '';
        $scope.employeeName = '';
        $scope.organizationName = '';
        $scope.sessionYear = '';
        //$scope.trainingInitializationInfo.district = '';
        $scope.trainerInfos = [];
        $scope.trainerInformation = {};
        $scope.trainerInfoDiv = true;
        $scope.endDateError = false;
        $scope.endDateInvalid = true;
        $scope.hremployeeinfos = HrEmployeeInfo.query({size:2000});
        $scope.approveError = false;

        $scope.getDistricts = function(){
            DistrictsByDivision.query({id:$scope.trainingInitializationInfo.division.id},function(result){
                $scope.districts = result;
            });
        };

        if( $stateParams.id != null){
            $scope.endDateInvalid = false;
            TrainingInitializationInfo.get({id : $stateParams.id}, function(result) {
                $scope.trainingInitializationInfo = result;
                console.log($scope.trainingInitializationInfo);
                console.log("==========X==========");
                // Get District by  Division when Edit
                DistrictsByDivision.query({id:result.division.id},function(result){
                    $scope.districts = result;
                });
                $scope.trainingInitializationInfo.requisitionId = result.trainingRequisitionForm.requisitionCode;
                $scope.getTrainingRequisitionData(result.trainingRequisitionForm.requisitionCode);
            });
            TrainerListByInitializationId.query({pInitializationId: $stateParams.id}, function (data) {
                $scope.trainerInfos = data;
            });
        }

        var onSaveFinished = function (result) {
            angular.forEach($scope.trainerInfos, function(data){
                console.log(data);
                data.trainingInitializationInfo = result;
                if(data.hrEmployeeInfo != null){
                    TrainerInformation.save(data);
                }else{
                    data.hrEmployeeInfo = null;
                    TrainerInformation.save(data);
                }
            });
            $state.go('trainingInfo.trainingInitializationInfo', null, { reload: true });
            $scope.$emit('stepApp:trainingInitializationInfoUpdate', result);
        };
        var onUpdateFinished = function (result) {
            angular.forEach($scope.trainerInfos, function(data){
                console.log('Update Call For Trainer Update');
                console.log(data);
                data.trainingInitializationInfo = result;
                if(data.hrEmployeeInfo !=null){
                    TrainerInformation.update(data);
                }else{
                    data.hrEmployeeInfo = null;
                    TrainerInformation.update(data);
                }
            });
            $state.go('trainingInfo.trainingInitializationInfo', null, { reload: true });
            $scope.$emit('stepApp:trainingInitializationInfoUpdate', result);
        };

        $scope.save = function () {
            if ($scope.trainingInitializationInfo.id != null) {
                TrainingInitializationInfo.update($scope.trainingInitializationInfo, onUpdateFinished);
                $rootScope.setWarningMessage('stepApp.trainingInitializationInfo.updated');
            } else {
                if($scope.trainingInitializationInfo.trainingType == null){
                    $scope.trainingInitializationInfo.trainingType = 'DTE';
                }
                TrainingInitializationInfo.save($scope.trainingInitializationInfo, onSaveFinished);
                $rootScope.setSuccessMessage('stepApp.trainingInitializationInfo.created');
            }
        };

        $scope.clear = function() {
            $state.go('trainingInfo.trainingInitializationInfo', null, { reload: true });
        };
        $scope.isPageShow = function(value) {
            $scope.timeScheduleDiv = false;
        };

       $scope.getTrainingRequisitionData = function(requisitionId){

           TrainingReqDataByReqCode.get({trainingReqcode:requisitionId},function(result){

               if(result.approveStatus >= 3){
                   $scope.trainingInitializationInfo.trainingRequisitionForm = result;
                   $scope.trainingName = result.trainingHeadSetup.headName;
                   $scope.applyDate = result.applyDate;
                   $scope.sessionYear = result.session;
                   if(result.applyBy == 'DTE Employee'){
                       $scope.organizationName = 'Directorate of Technical Education';
                       if(result.hrEmployeeInfo !=null){
                           $scope.employeeName = result.hrEmployeeInfo.fullName;
                       }
                       //$scope.employeeId = result.hrEmployeeInfo.employeeid;
                   }else if(result.applyBy == 'Institute'){
                       $scope.organizationName = result.institute.name;
                   }else {
                       $scope.organizationName = 'Not a Organization'
                   }
                   TraineeListByRequisition.query({pTrainingReqId : result.id},function(res){
                       $scope.trainingInitializationInfo.numberOfTrainee = res.length;
                   });
               }else{
                   $scope.approveError = true;
               }
           });

       };


        $scope.AddMoreTrainerInfo = function(){

            $scope.trainerInfos.push(
                {
                    status: null,
                    trainerType:null,
                    id: null
                }
            );
        };

        $scope.trainerInfos.push(
            {
                status: null,
                trainerType:null,
                id: null
            }
        );

        $scope.durationCalculate = function(a,b){
            $scope.endDateError = false;
            $scope.trainingInitializationInfo.duration = '';
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
            var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
            if(Math.floor((utc2 - utc1) / _MS_PER_DAY) >= 0){
                $scope.trainingInitializationInfo.duration = Math.floor((utc2 - utc1) / _MS_PER_DAY)+1;
                $scope.endDateError = false;
                $scope.endDateInvalid = false;
            }else{
                $scope.endDateError = true;
                $scope.endDateInvalid = true;
            }
        }


    }]);
*/
