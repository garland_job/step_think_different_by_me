'use strict';

angular.module('stepApp')
    .controller('TrainerEvaluationInfoController', function ($scope, TrainerEvaluationInfo, TrainerEvaluationInfoSearch, ParseLinks) {
        $scope.trainerEvaluationInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TrainerEvaluationInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.trainerEvaluationInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainerEvaluationInfo.get({id: id}, function(result) {
                $scope.trainerEvaluationInfo = result;
                $('#deleteTrainerEvaluationInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainerEvaluationInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainerEvaluationInfoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TrainerEvaluationInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.trainerEvaluationInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainerEvaluationInfo = {
                sessionYear: null,
                performance: null,
                remarks: null,
                evaluationDate: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*trainerEvaluationInfo-dialog.controller.js*/

angular.module('stepApp').controller('TrainerEvaluationInfoDialogController',
    ['$scope', '$state','$stateParams', '$rootScope', 'entity', 'TrainerEvaluationInfo', 'TrainingHeadSetup', 'TrainingInitializationInfo', 'TraineeInformation','TrainerListByTrainingCode','TrainingInitializeDataByTrainingCode',
        function($scope,$state ,$stateParams, $rootScope, entity, TrainerEvaluationInfo, TrainingHeadSetup, TrainingInitializationInfo, TraineeInformation,TrainerListByTrainingCode,TrainingInitializeDataByTrainingCode) {

            $scope.trainerEvaluationInfo = entity;
            $scope.trainingheadsetups = TrainingHeadSetup.query();
            $scope.traininginitializationinfos = TrainingInitializationInfo.query();
            $scope.traineeinformations = TraineeInformation.query();
            $scope.trainerLists = [];
            $scope.trainerEvaluationInfo2 = {};
            $scope.load = function(id) {
                TrainerEvaluationInfo.get({id : id}, function(result) {
                    $scope.trainerEvaluationInfo = result;
                });
            };

            $scope.TrainerListByTrainingCode = function (trainingCode) {
                TrainerListByTrainingCode.query({pTrainingCode : trainingCode}, function(result) {
                    $scope.trainerLists = result;
                });
                TrainingInitializeDataByTrainingCode.query({pTrainingCode : trainingCode},function(rest){
                    $scope.trainerEvaluationInfo.trainingInitializationInfo = rest;
                });
            };

            var onSaveFinished = function (result) {
                $state.go('trainingInfo.trainerEvaluationInfo', null, { reload: true });
                $scope.$emit('stepApp:trainerEvaluationInfoUpdate', result);
            };

            $scope.save = function () {
                //if ($scope.trainerEvaluationInfo.id != null) {
                //    TrainerEvaluationInfo.update($scope.trainerEvaluationInfo, onSaveFinished);
                //} else {
                angular.forEach($scope.trainerLists,function(trainer){
                    $scope.trainerEvaluationInfo2.trainerInformation = trainer;
                    $scope.trainerEvaluationInfo2.performance = trainer.performance;
                    $scope.trainerEvaluationInfo2.remarks = trainer.remarks;
                    $scope.trainerEvaluationInfo2.evaluationDate = $scope.trainerEvaluationInfo.evaluationDate;
                    $scope.trainerEvaluationInfo2.trainingInitializationInfo = $scope.trainerEvaluationInfo.trainingInitializationInfo;
                    $scope.trainerEvaluationInfo2.remarks = trainer.remarks;
                    TrainerEvaluationInfo.save($scope.trainerEvaluationInfo2);
                });
                $rootScope.setSuccessMessage('stepApp.trainerEvaluationInfo.created');
                $state.go('trainingInfo.trainerEvaluationInfo', null, { reload: true });
                $scope.$emit('stepApp:trainerEvaluationInfoUpdate', result);

                //}
            };

            $scope.clear = function() {
                //  $modalInstance.dismiss('cancel');
            };

            if ($stateParams.id) {
                $scope.load($stateParams.id);
            }
            else {
                $scope.trainerLists.push({
                    performance: '',
                    remarks: ''
                });
            }
        }]);
/*trainerEvaluationInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('TrainerEvaluationInfoDetailController', function ($scope, $rootScope, $stateParams, entity, TrainerEvaluationInfo, TrainingHeadSetup, TrainingInitializationInfo, TraineeInformation) {
        $scope.trainerEvaluationInfo = entity;
        $scope.load = function (id) {
            TrainerEvaluationInfo.get({id: id}, function(result) {
                $scope.trainerEvaluationInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:trainerEvaluationInfoUpdate', function(event, result) {
            $scope.trainerEvaluationInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
