'use strict';

angular.module('stepApp')
    .controller('TraineeInformationController', function ($scope, TraineeInformation, TraineeInformationSearch, ParseLinks) {
        $scope.traineeInformations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TraineeInformation.query({page: $scope.page, size: 200}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.traineeInformations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TraineeInformation.get({id: id}, function(result) {
                $scope.traineeInformation = result;
                $('#deleteTraineeInformationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TraineeInformation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTraineeInformationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TraineeInformationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.traineeInformations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.traineeInformation = {
                traineeId: null,
                traineeName: null,
                session: null,
                gender: null,
                organization: null,
                address: null,
                division: null,
                district: null,
                contactNumber: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*traineeInformation-dialog.controller.js*/

angular.module('stepApp').controller('TraineeInformationDialogController',
    ['$scope', '$state', '$stateParams', 'entity', 'TraineeInformation', 'TrainingHeadSetup', 'HrEmployeeInfo','HrEmployeeInfoByEmployeeId','Division','District',
        function($scope, $state, $stateParams, entity, TraineeInformation, TrainingHeadSetup, HrEmployeeInfo,HrEmployeeInfoByEmployeeId,Division,District) {

            $scope.traineeInformation = entity;
            $scope.trainingheadsetups = TrainingHeadSetup.query();
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.divisions = Division.query();
            $scope.districts = District.query();
            $scope.traineeInfos = [];

            //$scope.getDistrictByDivision = function(){
            //    if($scope.traineeInformation.division != null){
            //        DistrictsByDivision.query({id:$scope.traineeInformation.division.id},function(result){
            //            $scope.districts = result;
            //        });
            //    }
            //};

            //$scope.getDistrictByDivision = function(){
            //    District.query(function(result){
            //        $scope.districts = result;
            //    });
            //};

            $scope.load = function(id) {
                TraineeInformation.get({id : id}, function(result) {
                    $scope.traineeInformation = result;
                });
            };

            var onSaveFinished = function (result) {
                $state.go('trainingInfo.traineeInformation', null, { reload: true });
                $scope.$emit('stepApp:traineeInformationUpdate', result);
                //  $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.traineeInformation.id != null) {
                    TraineeInformation.update($scope.traineeInformation, onSaveFinished);
                } else {
                    angular.forEach($scope.traineeInfos,function(data){
                        TraineeInformation.save(data);
                    });
                    // TraineeInformation.save($scope.traineeInformation, onSaveFinished);
                }
                $state.go('trainingInfo.traineeInformation', null, { reload: true });
            };

            $scope.clear = function() {
                $state.go('trainingInfo.traineeInformation', null, { reload: true });
            };

            $scope.getHrEmployeeInfo = function(value,indexValue) {
                console.log(indexValue);
                HrEmployeeInfoByEmployeeId.get({id: value}, function (hrEmployeeData) {
                    console.log('--- '+ hrEmployeeData.fullName);
                    $scope.traineeInfos[indexValue].hrEmployeeInfo = hrEmployeeData;
                    $scope.traineeInfos[indexValue].traineeName = hrEmployeeData.fullName;
                    $scope.traineeInfos[indexValue].gender = hrEmployeeData.gender;
                    $scope.traineeInfos[indexValue].organization = hrEmployeeData.organizationType;
                    //  $scope.traineeInformation.contactNumber = hrEmployeeData.mobileNumber;
                });
            }

            $scope.AddMoreTraineeInfo = function(){

                $scope.traineeInfos.push(
                    {
                        status: null,
                        id: null
                    }
                );
            };

            $scope.traineeInfos.push(
                {
                    status: null,
                    id: null
                }
            );
        }]);
/*traineeInformation-detail.controller.js*/

angular.module('stepApp')
    .controller('TraineeInformationDetailController', function ($scope, $rootScope, $stateParams, entity, TraineeInformation, TrainingHeadSetup, HrEmployeeInfo) {
        $scope.traineeInformation = entity;
        $scope.load = function (id) {
            TraineeInformation.get({id: id}, function(result) {
                $scope.traineeInformation = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:traineeInformationUpdate', function(event, result) {
            $scope.traineeInformation = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
