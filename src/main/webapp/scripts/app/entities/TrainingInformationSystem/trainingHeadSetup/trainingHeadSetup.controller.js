'use strict';

angular.module('stepApp')
    .controller('TrainingHeadSetupController', function ($scope, TrainingHeadSetup, TrainingHeadSetupSearch, ParseLinks) {
        $scope.trainingHeadSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TrainingHeadSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.trainingHeadSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainingHeadSetup.get({id: id}, function(result) {
                $scope.trainingHeadSetup = result;
                $('#deleteTrainingHeadSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainingHeadSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainingHeadSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TrainingHeadSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.trainingHeadSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainingHeadSetup = {
                headName: null,
                description: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*trainingHeadSetup-dialog.controller.js*/

angular.module('stepApp').controller('TrainingHeadSetupDialogController',
    ['$scope', '$state', '$rootScope', '$stateParams', 'entity', 'TrainingHeadSetup',
        function($scope, $state, $rootScope, $stateParams, entity, TrainingHeadSetup) {

            $scope.trainingHeadSetup = entity;
            $scope.load = function(id) {
                TrainingHeadSetup.get({id : id}, function(result) {
                    $scope.trainingHeadSetup = result;
                });
            };

            var onSaveFinished = function (result) {
                $state.go('trainingInfo.trainingHeadSetup', null, { reload: true });
                $scope.$emit('stepApp:trainingHeadSetupUpdate', result);

            };

            $scope.save = function () {
                if ($scope.trainingHeadSetup.id != null) {
                    TrainingHeadSetup.update($scope.trainingHeadSetup, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.trainingHeadSetup.updated');
                } else {
                    if($scope.trainingHeadSetup.status == null){
                        $scope.trainingHeadSetup.status = true;
                    }
                    TrainingHeadSetup.save($scope.trainingHeadSetup, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.trainingHeadSetup.created');
                }
            };

            $scope.clear = function() {
                $state.go('trainingInfo.trainingHeadSetup', null, { reload: true });
            };
        }]);
/*trainingHeadSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('TrainingHeadSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'TrainingHeadSetup',
            function ($scope, $rootScope, $stateParams, entity, TrainingHeadSetup) {
                $scope.trainingHeadSetup = entity;
                $scope.load = function (id) {
                    TrainingHeadSetup.get({id: id}, function(result) {
                        $scope.trainingHeadSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:trainingHeadSetupUpdate', function(event, result) {
                    $scope.trainingHeadSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
