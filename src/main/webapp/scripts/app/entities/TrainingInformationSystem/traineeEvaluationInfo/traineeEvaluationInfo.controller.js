'use strict';

angular.module('stepApp')
    .controller('TraineeEvaluationInfoController', function ($scope, TraineeEvaluationInfo, TraineeEvaluationInfoSearch, ParseLinks) {
        $scope.traineeEvaluationInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TraineeEvaluationInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.traineeEvaluationInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TraineeEvaluationInfo.get({id: id}, function(result) {
                $scope.traineeEvaluationInfo = result;
                $('#deleteTraineeEvaluationInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TraineeEvaluationInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTraineeEvaluationInfoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TraineeEvaluationInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.traineeEvaluationInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.traineeEvaluationInfo = {
                sessionYear: null,
                performance: null,
                mark: null,
                remarks: null,
                evaluationDate: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*traineeEvaluationInfo-dialog.controller.js*/

angular.module('stepApp').controller('TraineeEvaluationInfoDialogController',
    ['$scope', '$state', '$stateParams', '$rootScope','entity', 'TraineeEvaluationInfo', 'TrainingInitializationInfo','TraineeListByTrainingCode','TrainingInitializeDataByTrainingCode',
        function($scope, $state, $stateParams,$rootScope, entity, TraineeEvaluationInfo, TrainingInitializationInfo,TraineeListByTrainingCode,TrainingInitializeDataByTrainingCode) {

            $scope.traineeEvaluationInfo = entity;
            $scope.traineeEvaluationInfoList = [];
            $scope.traineeLists = [];
            $scope.traineeEvaluationInfo2 = {};
            $scope.traineeEvaluationInfo2.trainingInitializationInfo = {};
            $scope.load = function(id) {
                TraineeEvaluationInfo.get({id : id}, function(result) {
                    $scope.traineeEvaluationInfo = result;
                });
            };

            $scope.TraineeListByTrainingCode = function (trainingCode) {
                //  $scope.headSetup = headSetup;
                TraineeListByTrainingCode.query({pTrainingCode : trainingCode}, function(result) {
                    $scope.traineeLists = result;
                });
                TrainingInitializeDataByTrainingCode.query({pTrainingCode : trainingCode},function(rest){
                    // $scope.trainingInitializationInfo = res;
                    console.log(rest);
                    $scope.traineeEvaluationInfo2.trainingInitializationInfo = rest;
                });
            };

            var onSaveFinished = function (result) {
                $state.go('trainingInfo.traineeEvaluationInfo', null, { reload: true });
                $scope.$emit('stepApp:traineeEvaluationInfoUpdate', result);
            };

            $scope.save = function () {
                angular.forEach($scope.traineeLists, function (traineeList) {
                    $scope.traineeEvaluationInfo2.id = "";
                    $scope.traineeEvaluationInfo2.traineeInformation = traineeList;
                    $scope.traineeEvaluationInfo2.performance = traineeList.performance;
                    $scope.traineeEvaluationInfo2.mark = traineeList.mark;
                    $scope.traineeEvaluationInfo2.remarks = traineeList.remarks;
                    $scope.traineeEvaluationInfo2.evaluationDate = $scope.traineeEvaluationInfo.evaluationDate;
                    console.log($scope.traineeEvaluationInfo2);
                    TraineeEvaluationInfo.save($scope.traineeEvaluationInfo2);
                });
                $rootScope.setSuccessMessage('stepApp.traineeEvaluationInfo.created');
                $state.go('trainingInfo.traineeEvaluationInfo', null, { reload: true });
            };

            if ($stateParams.id) {
                $scope.load($stateParams.id);
            }
            else {
                $scope.traineeLists.push({
                    performance: '',
                    mark: '',
                    remarks: ''
                });
            }

            $scope.clear = function() {
                $state.go('trainingInfo.traineeEvaluationInfo', null, { reload: true });
            };
        }]);
/*traineeEvaluationInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('TraineeEvaluationInfoDetailController', function ($scope, $rootScope, $stateParams, entity, TraineeEvaluationInfo, TrainingHeadSetup, HrEmployeeInfo, TrainingInitializationInfo) {
        $scope.traineeEvaluationInfo = entity;
        $scope.load = function (id) {
            TraineeEvaluationInfo.get({id: id}, function(result) {
                $scope.traineeEvaluationInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:traineeEvaluationInfoUpdate', function(event, result) {
            $scope.traineeEvaluationInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
