'use strict';

angular.module('stepApp')
    .controller('TrainerInformationController', function ($scope, TrainerInformation, TrainerInformationSearch, ParseLinks) {
        $scope.trainerInformations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TrainerInformation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.trainerInformations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainerInformation.get({id: id}, function(result) {
                $scope.trainerInformation = result;
                $('#deleteTrainerInformationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainerInformation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainerInformationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TrainerInformationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.trainerInformations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainerInformation = {
                trainerId: null,
                trainerName: null,
                trainerType: null,
                address: null,
                designation: null,
                department: null,
                organization: null,
                mobileNumber: null,
                emailId: null,
                specialSkills: null,
                trainingAssignStatus: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*trainerInformation-dialog.controller.js*/

angular.module('stepApp').controller('TrainerInformationDialogController',
    ['$scope', '$stateParams',  'entity', 'TrainerInformation', 'TrainingInitializationInfo', 'HrEmployeeInfo',
        function($scope, $stateParams, entity, TrainerInformation, TrainingInitializationInfo, HrEmployeeInfo) {

            $scope.trainerInformation = entity;
            $scope.traininginitializationinfos = TrainingInitializationInfo.query();
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                TrainerInformation.get({id : id}, function(result) {
                    $scope.trainerInformation = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:trainerInformationUpdate', result);
            };

            $scope.save = function () {
                if ($scope.trainerInformation.id != null) {
                    TrainerInformation.update($scope.trainerInformation, onSaveFinished);
                } else {
                    TrainerInformation.save($scope.trainerInformation, onSaveFinished);
                }
            };

            $scope.clear = function() {

            };
        }]);
/*trainerInformation-detail.controller.js*/

angular.module('stepApp')
    .controller('TrainerInformationDetailController', function ($scope, $rootScope, $stateParams, entity, TrainerInformation, TrainingInitializationInfo, HrEmployeeInfo) {
        $scope.trainerInformation = entity;
        $scope.load = function (id) {
            TrainerInformation.get({id: id}, function(result) {
                $scope.trainerInformation = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:trainerInformationUpdate', function(event, result) {
            $scope.trainerInformation = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
