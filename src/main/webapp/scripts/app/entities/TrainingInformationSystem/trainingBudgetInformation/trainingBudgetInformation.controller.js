'use strict';

angular.module('stepApp')
    .controller('TrainingBudgetInformationController', function ($scope, TrainingBudgetInformation, TrainingBudgetInformationSearch, ParseLinks) {
        $scope.trainingBudgetInformations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TrainingBudgetInformation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.trainingBudgetInformations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainingBudgetInformation.get({id: id}, function(result) {
                $scope.trainingBudgetInformation = result;
                $('#deleteTrainingBudgetInformationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainingBudgetInformation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainingBudgetInformationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TrainingBudgetInformationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.trainingBudgetInformations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainingBudgetInformation = {
                budgetAmount: null,
                expenseAmount: null,
                remarks: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*trainingBudgetInformation-dialog.controller.js*/

angular.module('stepApp').controller('TrainingBudgetInformationDialogController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'TrainingBudgetInformation', 'TrainingInitializationInfo',
        'TrainingInitializeDataByTrainingCode','GetBudgetInfoByInitializationId',
        function($scope, $rootScope,$state, $stateParams, entity, TrainingBudgetInformation, TrainingInitializationInfo,
                 TrainingInitializeDataByTrainingCode,GetBudgetInfoByInitializationId) {

            $scope.trainingBudgetInformation = entity;
            $scope.traininginitializationinfos = TrainingInitializationInfo.query({filter: 'trainingbudgetinformation-is-null'});
            $scope.budgetError = false;
            $scope.budgetInvalid = true;
            $scope.load = function(id) {
                TrainingBudgetInformation.get({id : id}, function(result) {
                    $scope.trainingBudgetInformation = result;
                });
            };

            if($stateParams.id != null){
                $scope.budgetInvalid = false;
            }
            var onSaveFinished = function (result) {
                $state.go('trainingInfo.trainingBudgetInformation', null, { reload: true });
                $scope.$emit('stepApp:trainingBudgetInformationUpdate', result);

            };

            $scope.save = function () {
                if ($scope.trainingBudgetInformation.id != null) {
                    TrainingBudgetInformation.update($scope.trainingBudgetInformation, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.trainingBudgetInformation.updated');
                } else {
                    if($scope.trainingBudgetInformation.status == null){
                        $scope.trainingBudgetInformation.status = true;
                    }
                    TrainingBudgetInformation.save($scope.trainingBudgetInformation, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.trainingBudgetInformation.created');
                }
            };

            $scope.clear = function() {
                $state.go('trainingInfo.trainingBudgetInformation', null, { reload: true });
            };

            $scope.getTrainingInitializeData = function(){

                TrainingInitializeDataByTrainingCode.get({pTrainingCode:$scope.trainingBudgetInformation.trainingCode},function(result){
                    console.log(result.id);
                    GetBudgetInfoByInitializationId.get({initializeId :result.id}, function(res) {
                        console.log(res);
                        if(res != null){
                            $scope.budgetError = true;
                            $scope.budgetInvalid = true;
                        }
                    },function(response){
                        if(response.status===404){
                            $scope.budgetError = false;
                            $scope.budgetInvalid = false;
                        }
                    });
                    $scope.trainingBudgetInformation.trainingInitializationInfo = result;
                });
            };
        }]);
/*trainingBudgetInformation-detail.controller.js*/

angular.module('stepApp')
    .controller('TrainingBudgetInformationDetailController', function ($scope, $rootScope, $stateParams, entity, TrainingBudgetInformation, TrainingInitializationInfo) {
        $scope.trainingBudgetInformation = entity;
        $scope.load = function (id) {
            TrainingBudgetInformation.get({id: id}, function(result) {
                $scope.trainingBudgetInformation = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:trainingBudgetInformationUpdate', function(event, result) {
            $scope.trainingBudgetInformation = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
