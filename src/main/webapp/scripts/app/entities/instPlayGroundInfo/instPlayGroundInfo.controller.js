'use strict';

angular.module('stepApp')
    .controller('InstPlayGroundInfoController',
    ['$scope', '$state', '$modal', 'InstPlayGroundInfo', 'InstPlayGroundInfoSearch', 'ParseLinks',
    function ($scope, $state, $modal, InstPlayGroundInfo, InstPlayGroundInfoSearch, ParseLinks) {

        $scope.instPlayGroundInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            InstPlayGroundInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.instPlayGroundInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            InstPlayGroundInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.instPlayGroundInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instPlayGroundInfo = {
                playgroundNo: null,
                area: null,
                id: null
            };
        };
    }]);
/*instPlayGroundInfo-dialog.controller.js */

angular.module('stepApp').controller('InstPlayGroundInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'InstPlayGroundInfo', 'InstInfraInfo',
        function($scope, $stateParams, $modalInstance, entity, InstPlayGroundInfo, InstInfraInfo) {

            $scope.instPlayGroundInfo = entity;
            $scope.instinfrainfos = InstInfraInfo.query();
            $scope.load = function(id) {
                InstPlayGroundInfo.get({id : id}, function(result) {
                    $scope.instPlayGroundInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instPlayGroundInfoUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instPlayGroundInfo.id != null) {
                    InstPlayGroundInfo.update($scope.instPlayGroundInfo, onSaveSuccess, onSaveError);
                } else {
                    InstPlayGroundInfo.save($scope.instPlayGroundInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);

/* instPlayGroundInfo-detail.controller.js */

angular.module('stepApp')
    .controller('InstPlayGroundInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstPlayGroundInfo', 'InstInfraInfo',
            function ($scope, $rootScope, $stateParams, entity, InstPlayGroundInfo, InstInfraInfo) {
                $scope.instPlayGroundInfo = entity;
                $scope.load = function (id) {
                    InstPlayGroundInfo.get({id: id}, function(result) {
                        $scope.instPlayGroundInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instPlayGroundInfoUpdate', function(event, result) {
                    $scope.instPlayGroundInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/* instPlayGroundInfo-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('InstPlayGroundInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstPlayGroundInfo',
            function($scope, $modalInstance, entity, InstPlayGroundInfo) {

                $scope.instPlayGroundInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstPlayGroundInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);
