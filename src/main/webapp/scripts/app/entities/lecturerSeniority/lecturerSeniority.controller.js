'use strict';

angular.module('stepApp')
    .controller('LecturerSeniorityController',
    ['$scope','$state','$modal','InstCategoryByStatusAndType','InstLevelByCategory','ParseLinks','InstEmployeeByInstituteAndDesigAndMpoStatus',
    function ($scope, $state, $modal, InstCategoryByStatusAndType, InstLevelByCategory, ParseLinks,InstEmployeeByInstituteAndDesigAndMpoStatus) {

        $scope.lecturerSenioritys = [];
        $scope.instEmployees = [];

        InstCategoryByStatusAndType.query({activeStatus: true, instType: 'NonGovernment'}, function (result) {
            $scope.instCategories = result;
        });

        $scope.loadInstLevel = function (instCategory) {
            $scope.instLevels = InstLevelByCategory.query({catId: instCategory.id});
        };
        $scope.page = 0;
        $scope.loadAll = function() {
            //LecturerSeniority.query({page: $scope.page, size: 20}, function(result, headers) {
            //    $scope.links = ParseLinks.parse(headers('link'));
            //    $scope.lecturerSenioritys = result;
            //    $scope.total = headers('x-total-count');
            //});
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.institute = function (value,servicio) {
            // console.log(value.originalObject);
            $scope.instituteInfo = value.originalObject;
            InstEmployeeByInstituteAndDesigAndMpoStatus.query({instituteId:$scope.instituteInfo.id, desigName:'Lecturer',mpoStatus:true},function(instEmployees){
                $scope.instEmployees = instEmployees;
            });
        }

        $scope.loadAll();

        $scope.search = function () {
            //LecturerSenioritySearch.query({query: $scope.searchQuery}, function(result) {
            //    $scope.lecturerSenioritys = result;
            //}, function(response) {
            //    if(response.status === 404) {
            //        $scope.loadAll();
            //    }
            //});
        };

        //InstEmployeeByInstituteAndDesigAndMpoStatus.query({},function(results){
        //
        //});

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.lecturerSeniority = {
                serial: null,
                name: null,
                subject: null,
                firstMPOEnlistingDate: null,
                joiningDateAsLecturer: null,
                dob: null,
                remarks: null,
                id: null
            };
        };

        // bulk operations start
        $scope.areAllLecturerSenioritysSelected = false;

        $scope.updateLecturerSenioritysSelection = function (lecturerSeniorityArray, selectionValue) {
            for (var i = 0; i < lecturerSeniorityArray.length; i++)
            {
            lecturerSeniorityArray[i].isSelected = selectionValue;
            }
        };

    }]);
