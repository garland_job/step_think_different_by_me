'use strict';

angular.module('stepApp')
    .controller('AlmEarningMethodController',

    ['$scope', 'AlmEarningMethod', 'AlmEarningMethodSearch', 'ParseLinks',
    function ($scope, AlmEarningMethod, AlmEarningMethodSearch, ParseLinks) {
        $scope.almEarningMethods = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmEarningMethod.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almEarningMethods = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmEarningMethod.get({id: id}, function(result) {
                $scope.almEarningMethod = result;
                $('#deleteAlmEarningMethodConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmEarningMethod.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmEarningMethodConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmEarningMethodSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almEarningMethods = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almEarningMethod = {
                earningMethodName: null,
                description: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almEarningMethod-dialog.controller.js*/

angular.module('stepApp').controller('AlmEarningMethodDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'AlmEarningMethod','User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, AlmEarningMethod, User, Principal, DateUtils) {

            $scope.almEarningMethod = entity;

            $scope.load = function(id) {
                AlmEarningMethod.get({id : id}, function(result) {
                    $scope.almEarningMethod = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almEarningMethodUpdate', result);
                $scope.isSaving = false;
                $state.go("almEarningMethod");
            };

            $scope.save = function () {
                $scope.almEarningMethod.updateBy = $scope.loggedInUser.id;
                $scope.almEarningMethod.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almEarningMethod.id != null) {
                    AlmEarningMethod.update($scope.almEarningMethod, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almEarningMethod.updated');
                } else {
                    $scope.almEarningMethod.createBy = $scope.loggedInUser.id;
                    $scope.almEarningMethod.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmEarningMethod.save($scope.almEarningMethod, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.almEarningMethod.created');
                }
            };

        }]);


/*almEarningMethod-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmEarningMethodDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmEarningMethod',
            function ($scope, $rootScope, $stateParams, entity, AlmEarningMethod) {
                $scope.almEarningMethod = entity;
                $scope.load = function (id) {
                    AlmEarningMethod.get({id: id}, function(result) {
                        $scope.almEarningMethod = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almEarningMethodUpdate', function(event, result) {
                    $scope.almEarningMethod = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
