'use strict';

angular.module('stepApp')
    .controller('AlmEmpLeaveCancellationController',
    ['$scope', 'HrEmployeeInfo', 'AlmEmpLeaveCancellation', 'AlmEmpLeaveCancellationSearch', 'ParseLinks',
    function ($scope, HrEmployeeInfo, AlmEmpLeaveCancellation, AlmEmpLeaveCancellationSearch, ParseLinks) {
        $scope.almEmpLeaveCancellations = [];
        $scope.page = 0;
        $scope.newRequestList = [];

        HrEmployeeInfo.get({id: 'my'}, function (result) {
            $scope.hrEmployeeInfo = result;

        });


        $scope.loadAll = function()
        {
            $scope.requestEntityCounter = 1;
            $scope.newRequestList = [];
            AlmEmpLeaveCancellation.query(function(result)
            {
                $scope.requestEntityCounter++;
                angular.forEach(result,function(dtoInfo)
                {
                    if(dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id){
                        $scope.newRequestList.push(dtoInfo);
                    }
                });
            });
            $scope.newRequestList.sort($scope.sortById);
        };

        $scope.loadAll();

        $scope.sortById = function(a,b){
            return b.id - a.id;
        };

        $scope.searchText = "";
        $scope.updateSearchText = "";

        $scope.clearSearchText = function (source)
        {
            if(source=='request')
            {
                $timeout(function(){
                    $('#searchText').val('');
                    angular.element('#searchText').triggerHandler('change');
                });
            }
        };



       /* $scope.loadAll = function() {
            AlmEmpLeaveCancellation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almEmpLeaveCancellations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();*/

        $scope.delete = function (id) {
            AlmEmpLeaveCancellation.get({id: id}, function(result) {
                $scope.almEmpLeaveCancellation = result;
                $('#deleteAlmEmpLeaveCancellationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmEmpLeaveCancellation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmEmpLeaveCancellationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmEmpLeaveCancellationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almEmpLeaveCancellations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almEmpLeaveCancellation = {
                requestDate: null,
                requestType: null,
                cancelStatus: null,
                causeOfCancellation: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almEmpLeaveCancellation-dialog.controller.js*/

angular.module('stepApp').controller('AlmEmpLeaveCancellationDialogController',
    ['$scope', '$stateParams', '$state','$q' ,'entity', 'AlmEmpLeaveCancellation', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfo', 'AlmEmpLeaveApplication','AlmEmpPersonalLeaveApplication','HrDepartmentActiveHeadInfoByDept',
        function($scope, $stateParams, $state,$q, entity, AlmEmpLeaveCancellation, User, Principal, DateUtils, HrEmployeeInfo, AlmEmpLeaveApplication, AlmEmpPersonalLeaveApplication,HrDepartmentActiveHeadInfoByDept) {

            $scope.almEmpLeaveCancellation = entity;
            $scope.hremployeeinfos = [];
            $scope.almempleaveapplications1 = [];
            $scope.deptHead = null;

            HrEmployeeInfo.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.hremployeeinfos.push(dtoInfo);
                    }
                });
            });

            AlmEmpLeaveApplication.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almempleaveapplications1.push(dtoInfo);
                    }
                });
            });

            $scope.almempleaveapplications = [];

            HrEmployeeInfo.get({id: 'my'}, function (result) {

                $scope.hrEmployeeInfo = result;
                $q.all($scope.hrEmployeeInfo).then(function () {
                    console.log('---------------leave canselation application---------')
                    console.log($scope.hrEmployeeInfo)
                    $scope.designationName      = $scope.hrEmployeeInfo.designationInfo.designationInfo.designationName;
                    $scope.departmentName       = $scope.hrEmployeeInfo.departmentInfo.departmentInfo.departmentName;
                    if($scope.hrEmployeeInfo.workArea){
                        $scope.workAreaName = $scope.hrEmployeeInfo.workArea.typeName;
                    }else{
                        $scope.workAreaName = "";
                    }
                    AlmEmpPersonalLeaveApplication.get({id:$scope.hrEmployeeInfo.id},function (result) {
                        $scope.almempleaveapplications = result;
                    })
                    HrDepartmentActiveHeadInfoByDept.get({deptId: $scope.hrEmployeeInfo.departmentInfo.id}, function (result) {
                        console.log(result);
                        $scope.deptHead = result;
                    });
                })
                //$scope.leaveApplicationList();
            });

            $scope.isExitsData = true;
            $scope.exitsDataMessage = '';
            $scope.duplicateCheckByLeave = function(){
                $scope.isExitsData = true;
                $scope.exitsDataMessage = '';
                AlmEmpLeaveCancellation.query(function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.almEmpLeaveApplication.id == $scope.almEmpLeaveCancellation.almEmpLeaveApplication.id){
                            $scope.isExitsData = false;
                            $scope.exitsDataMessage = 'You Already applied for this leave.';
                        }
                    });
                });
            };

            // $scope.leaveApplicationList = function(){
            //     AlmEmpLeaveApplication.query(function(result){
            //         angular.forEach(result,function(dtoInfo)
            //         {
            //             if(dtoInfo.employeeInfo.id ==  $scope.hrEmployeeInfo.id && dtoInfo.applicationLeaveStatus == 'Approved'){
            //                 $scope.almempleaveapplications.push(dtoInfo);
            //             }
            //         });
            //     });
            // };

            $scope.onChaneEmployeeLeaveApplication = function(){
                $scope.almLeaveType=        $scope.almEmpLeaveCancellation.almEmpLeaveApplication.almLeaveType;
                $scope.leaveFromDate=       $scope.almEmpLeaveCancellation.almEmpLeaveApplication.leaveFromDate;
                $scope.leaveToDate=         $scope.almEmpLeaveCancellation.almEmpLeaveApplication.leaveToDate  ;
                $scope.isHalfDayLeave=      $scope.almEmpLeaveCancellation.almEmpLeaveApplication.isHalfDayLeave ;
                $scope.halfDayLeaveInfo=    $scope.almEmpLeaveCancellation.almEmpLeaveApplication.halfDayLeaveInfo ;
                $scope.leaveDays=           $scope.almEmpLeaveCancellation.almEmpLeaveApplication.leaveDays ;
                $scope.reasonOfLeave=       $scope.almEmpLeaveCancellation.almEmpLeaveApplication.reasonOfLeave  ;
                $scope.contactNo=           $scope.almEmpLeaveCancellation.almEmpLeaveApplication.contactNo  ;
            };

            $scope.load = function(id) {
                AlmEmpLeaveCancellation.get({id : id}, function(result) {
                    $scope.almEmpLeaveCancellation = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almEmpLeaveCancellationUpdate', result);
                $scope.isSaving = false;
                $state.go("almEmpLeaveCancellation");
            };

            $scope.save = function () {
                $scope.almEmpLeaveCancellation.updateBy = $scope.loggedInUser.id;
                $scope.almEmpLeaveCancellation.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almEmpLeaveCancellation.id != null) {
                    AlmEmpLeaveCancellation.update($scope.almEmpLeaveCancellation, onSaveFinished);
                } else {
                    $scope.almEmpLeaveCancellation.nextApprover = $scope.deptHead;
                    $scope.almEmpLeaveCancellation.createBy = $scope.loggedInUser.id;
                    $scope.almEmpLeaveCancellation.employeeInfo = $scope.hrEmployeeInfo;
                    $scope.almEmpLeaveCancellation.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.almEmpLeaveCancellation.requestDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.almEmpLeaveCancellation.cancelStatus = 'Pending';
                    AlmEmpLeaveCancellation.save($scope.almEmpLeaveCancellation, onSaveFinished);
                }
            };

        }]);
/*almEmpLeaveCancellation-detail.controller.js*/

angular.module('stepApp')
    .controller('AlmEmpLeaveCancellationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmEmpLeaveCancellation', 'HrEmployeeInfo', 'AlmEmpLeaveApplication',
            function ($scope, $rootScope, $stateParams, entity, AlmEmpLeaveCancellation, HrEmployeeInfo, AlmEmpLeaveApplication) {
                $scope.almEmpLeaveCancellation = entity;
                $scope.load = function (id) {
                    AlmEmpLeaveCancellation.get({id: id}, function(result) {
                        $scope.almEmpLeaveCancellation = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almEmpLeaveCancellationUpdate', function(event, result) {
                    $scope.almEmpLeaveCancellation = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*almAppCancelRequest.controller.js*/

angular.module('stepApp')
    .controller('AlmAppCancelRequestLController',
        ['$scope', '$state', '$rootScope', '$timeout', 'DataUtils', 'AlmEmpLeaveCancellation', 'AlmEmpLeaveApplication',
            'HrEmployeeInfo', 'Principal', 'AlmEmpLeaveBalance', 'DateUtils',

            function($scope, $state, $rootScope, $timeout, DataUtils, AlmEmpLeaveCancellation, AlmEmpLeaveApplication,
                     HrEmployeeInfo, Principal, AlmEmpLeaveBalance, DateUtils)
            {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;
                $scope.currentApprover  = null;

                $scope.loggedInUser =   {};
                $scope.getLoggedInUser = function ()
                {
                    Principal.identity().then(function (account)
                    {
                        User.get({login: account.login}, function (result)
                        {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function()
                {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];

                    Principal.identity().then(function (account)
                    {
                    AlmEmpLeaveCancellation.query({login:account.login},function(result)
                    {
                        console.log(result)
                        $scope.requestEntityCounter++;
                        angular.forEach(result,function(dtoInfo)
                        {
                            if($scope.currentApprover == null){
                                $scope.currentApprover = dtoInfo.nextApprover
                            }
                            if(dtoInfo.cancelStatus == "Pending"){
                                $scope.newRequestList.push(dtoInfo);
                            }
                        });
                        $scope.newRequestList.sort($scope.sortById);
                        $scope.loadApprovedRejectList(result);
                    });
                    })
                };

                $scope.sortById = function(a,b){
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function ()
                {
                    if($scope.approvalObj.isApproved)
                        $scope.approvalObj.cancelStatus='Approved';
                    else $scope.approvalObj.cancelStatus='Reject';
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function ()
                {

                    if($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.cancelStatus = 'Approved';
                    }
                    else {
                        $scope.approvalObj.cancelStatus='Reject';
                    }
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj){

                    AlmEmpLeaveCancellation.update(requestObj, function(result)
                    {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        $scope.updateLeaveBalance(requestObj);
                        $scope.loadAll();
                    });
                };

                $scope.updateLeaveBalance = function (requestObj) {
                    $scope.isSaving = false;
                    AlmEmpLeaveBalance.query(function(result){
                        angular.forEach(result,function(dtoInfo) {
                            if(dtoInfo.employeeInfo.id == requestObj.employeeInfo.id
                                && dtoInfo.almLeaveType.id == requestObj.almEmpLeaveApplication.almLeaveType.id){
                                $scope.almEmpLeaveBalance = dtoInfo;
                                if(requestObj.cancelStatus == 'Approved'){
                                    dtoInfo.leaveBalance = dtoInfo.leaveBalance + requestObj.almEmpLeaveApplication.leaveDays;
                                    dtoInfo.leaveOnApply = dtoInfo.leaveOnApply - requestObj.almEmpLeaveApplication.leaveDays;
                                }
                                dtoInfo.updateBy = $scope.loggedInUser.id;
                                dtoInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                                AlmEmpLeaveBalance.update(dtoInfo);
                            }

                        });
                    });

                };

                $scope.loadApprovedRejectList = function (result)
                {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                        angular.forEach(result,function(requestObj) {
                            if(requestObj.cancelStatus=="Approved") {
                                $scope.approvedList.push(requestObj);
                            }
                            if(requestObj.cancelStatus=="Reject") {
                                $scope.rejectedList.push(requestObj);
                            }
                        });
                    };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source)
                {
                    if(source=='request')
                    {
                        $timeout(function(){
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };


                $scope.approvalViewDetail = function (dataObj)
                {
                    $scope.approvalObj = dataObj;
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType){
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        employeeId:null,
                        entityName:null,
                        requestFrom:null,
                        requestSummary: null,
                        requestDate:null,
                        approveState: null,
                        logStatus:null,
                        logComments:null,
                        actionType:'',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function(event, data)
                {
                    $scope.loadAll();
                });


                $scope.loadEmployee = function () {
                    HrEmployeeInfo.get({id: 'my'}, function (result) {
                        $scope.hrEmployeeInfo = result;

                    }, function (response) {
                        $scope.hasProfile = false;
                        $scope.noEmployeeFound = true;
                        $scope.isSaving = false;
                    })
                };

                $scope.sort = function(keyname, source){
                    if(source=='request')
                    {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if(source=='approved')
                    {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if(source=='rejected')
                    {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };

                $scope.loadAll();

            }])
;
