'use strict';

angular.module('stepApp')
    .controller('AlmLeavGrpTypeMapController',
    ['$scope', 'AlmLeavGrpTypeMap', 'AlmLeavGrpTypeMapSearch', 'ParseLinks',
    function ($scope, AlmLeavGrpTypeMap, AlmLeavGrpTypeMapSearch, ParseLinks) {
        $scope.almLeavGrpTypeMaps = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmLeavGrpTypeMap.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almLeavGrpTypeMaps = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmLeavGrpTypeMap.get({id: id}, function(result) {
                $scope.almLeavGrpTypeMap = result;
                $('#deleteAlmLeavGrpTypeMapConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmLeavGrpTypeMap.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmLeavGrpTypeMapConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmLeavGrpTypeMapSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almLeavGrpTypeMaps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almLeavGrpTypeMap = {
                reason: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almLeavGrpTypeMap-dialog.controller.js*/

angular.module('stepApp').controller('AlmLeavGrpTypeMapDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'AlmLeavGrpTypeMap', 'User', 'Principal', 'DateUtils', 'AlmLeaveGroup', 'AlmLeaveType',
        function($scope, $stateParams, $state, entity, AlmLeavGrpTypeMap, User, Principal, DateUtils, AlmLeaveGroup, AlmLeaveType) {

            $scope.almLeavGrpTypeMap = entity;
            $scope.almleavegroups = [];
            $scope.almleavetypes = [];


            AlmLeaveGroup.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almleavegroups.push(dtoInfo);
                    }
                });
            });

            AlmLeaveType.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almleavetypes.push(dtoInfo);
                    }
                });
            });

            $scope.load = function(id) {
                AlmLeavGrpTypeMap.get({id : id}, function(result) {
                    $scope.almLeavGrpTypeMap = result;
                });
            };

            $scope.isExitsData = true;
            $scope.duplicateCheckByGroupAndType = function(){
                $scope.isExitsData = true;
                AlmLeavGrpTypeMap.query(function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.almLeaveGroup.id == $scope.almLeavGrpTypeMap.almLeaveGroup.id && dtoInfo.almLeaveType.id == $scope.almLeavGrpTypeMap.almLeaveType.id){
                            $scope.isExitsData = false;
                        }
                    });
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almLeavGrpTypeMapUpdate', result);
                $scope.isSaving = false;
                $state.go("almLeavGrpTypeMap");
            };

            $scope.save = function () {
                $scope.almLeavGrpTypeMap.updateBy = $scope.loggedInUser.id;
                $scope.almLeavGrpTypeMap.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almLeavGrpTypeMap.id != null) {
                    AlmLeavGrpTypeMap.update($scope.almLeavGrpTypeMap, onSaveFinished);
                } else {
                    $scope.almLeavGrpTypeMap.createBy = $scope.loggedInUser.id;
                    $scope.almLeavGrpTypeMap.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmLeavGrpTypeMap.save($scope.almLeavGrpTypeMap, onSaveFinished);
                }
            };

        }]);

/*almLeavGrpTypeMap-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmLeavGrpTypeMapDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmLeavGrpTypeMap', 'AlmLeaveGroup', 'AlmLeaveType',
            function ($scope, $rootScope, $stateParams, entity, AlmLeavGrpTypeMap, AlmLeaveGroup, AlmLeaveType) {
                $scope.almLeavGrpTypeMap = entity;
                $scope.load = function (id) {
                    AlmLeavGrpTypeMap.get({id: id}, function(result) {
                        $scope.almLeavGrpTypeMap = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almLeavGrpTypeMapUpdate', function(event, result) {
                    $scope.almLeavGrpTypeMap = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
