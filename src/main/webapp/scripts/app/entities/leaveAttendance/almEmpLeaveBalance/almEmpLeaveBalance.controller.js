'use strict';

angular.module('stepApp')
    .controller('AlmEmpLeaveBalanceController',
    ['$scope', 'AlmEmpLeaveBalance', 'AlmEmpLeaveBalanceSearch', 'ParseLinks',
    function ($scope, AlmEmpLeaveBalance, AlmEmpLeaveBalanceSearch, ParseLinks) {
        $scope.almEmpLeaveBalances = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmEmpLeaveBalance.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almEmpLeaveBalances = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmEmpLeaveBalance.get({id: id}, function(result) {
                $scope.almEmpLeaveBalance = result;
                $('#deleteAlmEmpLeaveBalanceConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmEmpLeaveBalance.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmEmpLeaveBalanceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmEmpLeaveBalanceSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almEmpLeaveBalances = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almEmpLeaveBalance = {
                yearOpenDate: null,
                year: null,
                leaveEarned: null,
                leaveTaken: null,
                leaveForwarded: null,
                attendanceLeave: null,
                leaveOnApply: null,
                leaveBalance: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almEmpLeaveBalance-dialog.controller.js*/

angular.module('stepApp').controller('AlmEmpLeaveBalanceDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'AlmEmpLeaveBalance', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfo', 'AlmLeaveType',
        function($scope, $stateParams, $state, entity, AlmEmpLeaveBalance, User, Principal, DateUtils, HrEmployeeInfo, AlmLeaveType) {

            $scope.almEmpLeaveBalance = entity;
            $scope.hremployeeinfos = [];
            $scope.almleavetypes = [];

            HrEmployeeInfo.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.hremployeeinfos.push(dtoInfo);
                    }
                });
            });

            AlmLeaveType.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almleavetypes.push(dtoInfo);
                    }
                });
            });

            $scope.load = function(id) {
                AlmEmpLeaveBalance.get({id : id}, function(result) {
                    $scope.almEmpLeaveBalance = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almEmpLeaveBalanceUpdate', result);
                $scope.isSaving = false;
                $state.go("almEmpLeaveBalance");
            };

            $scope.save = function () {
                $scope.almEmpLeaveBalance.updateBy = $scope.loggedInUser.id;
                $scope.almEmpLeaveBalance.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almEmpLeaveBalance.id != null) {
                    AlmEmpLeaveBalance.update($scope.almEmpLeaveBalance, onSaveFinished);
                } else {
                    $scope.almEmpLeaveBalance.createBy = $scope.loggedInUser.id;
                    $scope.almEmpLeaveBalance.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmEmpLeaveBalance.save($scope.almEmpLeaveBalance, onSaveFinished);
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);

/*almEmpLeaveBalance-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmEmpLeaveBalanceDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmEmpLeaveBalance', 'HrEmployeeInfo', 'AlmEmpLeaveApplication',
            function ($scope, $rootScope, $stateParams, entity, AlmEmpLeaveBalance, HrEmployeeInfo, AlmEmpLeaveApplication) {
                $scope.almEmpLeaveBalance = entity;
                $scope.load = function (id) {
                    AlmEmpLeaveBalance.get({id: id}, function(result) {
                        $scope.almEmpLeaveBalance = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almEmpLeaveBalanceUpdate', function(event, result) {
                    $scope.almEmpLeaveBalance = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
