'use strict';

angular.module('stepApp')
    .controller('AlmEarningFrequencyController',
    ['$scope', 'AlmEarningFrequency', 'AlmEarningFrequencySearch', 'ParseLinks',
    function ($scope, AlmEarningFrequency, AlmEarningFrequencySearch, ParseLinks) {
        $scope.almEarningFrequencys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmEarningFrequency.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almEarningFrequencys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmEarningFrequency.get({id: id}, function(result) {
                $scope.almEarningFrequency = result;
                $('#deleteAlmEarningFrequencyConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmEarningFrequency.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmEarningFrequencyConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmEarningFrequencySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almEarningFrequencys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almEarningFrequency = {
                earningFrequencyName: null,
                description: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almEarningFrequency-dialog.controller.js*/

angular.module('stepApp').controller('AlmEarningFrequencyDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'AlmEarningFrequency', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, AlmEarningFrequency, User, Principal, DateUtils) {

            $scope.almEarningFrequency = entity;

            $scope.load = function(id) {
                AlmEarningFrequency.get({id : id}, function(result) {
                    $scope.almEarningFrequency = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almEarningFrequencyUpdate', result);
                $scope.isSaving = false;
                $state.go("almEarningFrequency");
            };

            $scope.save = function () {
                $scope.almEarningFrequency.updateBy = $scope.loggedInUser.id;
                $scope.almEarningFrequency.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almEarningFrequency.id != null) {
                    AlmEarningFrequency.update($scope.almEarningFrequency, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almEarningFrequency.updated');
                } else {
                    $scope.almEarningFrequency.createBy = $scope.loggedInUser.id;
                    $scope.almEarningFrequency.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmEarningFrequency.save($scope.almEarningFrequency, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.almEarningFrequency.created');
                }
            };

        }]);

/*almEarningFrequency-detail.controller.js*/

angular.module('stepApp')
    .controller('AlmEarningFrequencyDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmEarningFrequency',
            function ($scope, $rootScope, $stateParams, entity, AlmEarningFrequency) {
                $scope.almEarningFrequency = entity;
                $scope.load = function (id) {
                    AlmEarningFrequency.get({id: id}, function(result) {
                        $scope.almEarningFrequency = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almEarningFrequencyUpdate', function(event, result) {
                    $scope.almEarningFrequency = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
