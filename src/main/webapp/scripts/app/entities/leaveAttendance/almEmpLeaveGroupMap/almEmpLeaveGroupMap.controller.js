'use strict';

angular.module('stepApp')
    .controller('AlmEmpLeaveGroupMapController',
    ['$scope', 'AlmEmpLeaveGroupMap', 'AlmEmpLeaveGroupMapSearch', 'ParseLinks',
    function ($scope, AlmEmpLeaveGroupMap, AlmEmpLeaveGroupMapSearch, ParseLinks) {
        $scope.almEmpLeaveGroupMaps = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmEmpLeaveGroupMap.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almEmpLeaveGroupMaps = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmEmpLeaveGroupMap.get({id: id}, function(result) {
                $scope.almEmpLeaveGroupMap = result;
                $('#deleteAlmEmpLeaveGroupMapConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmEmpLeaveGroupMap.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmEmpLeaveGroupMapConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmEmpLeaveGroupMapSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almEmpLeaveGroupMaps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almEmpLeaveGroupMap = {
                effectiveDate: null,
                reason: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almEmpLeaveGroupMap-dialog.controller.js*/

angular.module('stepApp').controller('AlmEmpLeaveGroupMapDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'AlmEmpLeaveGroupMap', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfo', 'AlmLeaveGroup',
        function($scope, $stateParams, $state, entity, AlmEmpLeaveGroupMap, User, Principal, DateUtils, HrEmployeeInfo, AlmLeaveGroup) {

            $scope.almEmpLeaveGroupMap = entity;
            $scope.hremployeeinfos = [];
            $scope.almleavegroups = [];

            HrEmployeeInfo.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.hremployeeinfos.push(dtoInfo);
                    }
                });
            });

            AlmLeaveGroup.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almleavegroups.push(dtoInfo);
                    }
                });
            });


            $scope.load = function(id) {
                AlmEmpLeaveGroupMap.get({id : id}, function(result) {
                    $scope.almEmpLeaveGroupMap = result;
                });
            };
            $scope.isExitsData = true;
            $scope.duplicateCheckByGroupAndEmployee = function(){
                $scope.isExitsData = true;
                AlmEmpLeaveGroupMap.query(function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.employeeInfo.id == $scope.almEmpLeaveGroupMap.employeeInfo.id
                            && dtoInfo.almLeaveGroup.id == $scope.almEmpLeaveGroupMap.almLeaveGroup.id
                            && dtoInfo.activeStatus){
                            $scope.isExitsData = false;
                        }
                    });
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almEmpLeaveGroupMapUpdate', result);
                $scope.isSaving = false;
                $state.go("hrm.leavecanform");
            };

            $scope.save = function () {
                $scope.almEmpLeaveGroupMap.updateBy = $scope.loggedInUser.id;
                $scope.almEmpLeaveGroupMap.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almEmpLeaveGroupMap.id != null) {
                    AlmEmpLeaveGroupMap.update($scope.almEmpLeaveGroupMap, onSaveFinished);
                } else {
                    $scope.almEmpLeaveGroupMap.createBy = $scope.loggedInUser.id;
                    $scope.almEmpLeaveGroupMap.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmEmpLeaveGroupMap.save($scope.almEmpLeaveGroupMap, onSaveFinished);
                }
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);
/*almEmpLeaveGroupMap-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmEmpLeaveGroupMapDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmEmpLeaveGroupMap', 'HrEmployeeInfo', 'AlmLeaveGroup',
            function ($scope, $rootScope, $stateParams, entity, AlmEmpLeaveGroupMap, HrEmployeeInfo, AlmLeaveGroup) {
                $scope.almEmpLeaveGroupMap = entity;
                $scope.load = function (id) {
                    AlmEmpLeaveGroupMap.get({id: id}, function(result) {
                        $scope.almEmpLeaveGroupMap = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almEmpLeaveGroupMapUpdate', function(event, result) {
                    $scope.almEmpLeaveGroupMap = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
