'use strict';

angular.module('stepApp')
    .controller('AlmEmpLeaveApplicationController',
        ['$scope', '$q', 'HrEmployeeInfo', 'AlmEmpLeaveApplication', 'AlmEmpLeaveApplicationSearch', 'ParseLinks', 'HrEmployeeInfoByLogin','AlmEmpPersonalLeaveApplication',
            function ($scope,$q, HrEmployeeInfo, AlmEmpLeaveApplication, AlmEmpLeaveApplicationSearch, ParseLinks,HrEmployeeInfoByLogin, AlmEmpPersonalLeaveApplication) {

                $scope.newRequestList = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;
                $scope.leaveApplication = []

                // HrEmployeeInfo.get({id: 'my'}, function (result) {
                //     $scope.hrEmployeeInfo = result;
                //
                // });

                $scope.loadAll = function () {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];
                    AlmEmpLeaveApplication.query(function (result) {
                        $scope.requestEntityCounter++;
                        angular.forEach(result, function (dtoInfo) {
                            if (dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id) {
                                $scope.newRequestList.push(dtoInfo);
                            }
                        });
                    });
                    $scope.newRequestList.sort($scope.sortById);
                };
                HrEmployeeInfoByLogin.get(function (result) {
                    $scope.hrEmployeeInfo = result;
                    $q.all($scope.hrEmployeeInfo).then(function () {
                        AlmEmpPersonalLeaveApplication.get({id:$scope.hrEmployeeInfo.id},function (result) {
                            $scope.leaveApplication = result;
                        })
                    })

                   // $scope.loadAll();

                });

                $scope.sortById = function (a, b) {
                    return b.id - a.id;
                };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source) {
                    if (source == 'request') {
                        $timeout(function () {
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };


                /*$scope.almEmpLeaveApplications = [];
                 $scope.page = 0;
                 $scope.loadAll = function() {
                 AlmEmpLeaveApplication.query({page: $scope.page, size: 20}, function(result, headers) {
                 $scope.links = ParseLinks.parse(headers('link'));
                 $scope.almEmpLeaveApplications = result;
                 });
                 };
                 $scope.loadPage = function(page) {
                 $scope.page = page;
                 $scope.loadAll();
                 };
                 $scope.loadAll();*/

                $scope.delete = function (id) {
                    AlmEmpLeaveApplication.get({id: id}, function (result) {
                        $scope.almEmpLeaveApplication = result;
                        $('#deleteAlmEmpLeaveApplicationConfirmation').modal('show');
                    });
                };

                $scope.confirmDelete = function (id) {
                    AlmEmpLeaveApplication.delete({id: id},
                        function () {
                            $scope.loadAll();
                            $('#deleteAlmEmpLeaveApplicationConfirmation').modal('hide');
                            $scope.clear();
                        });
                };

                $scope.search = function () {
                    AlmEmpLeaveApplicationSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.almEmpLeaveApplications = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.almEmpLeaveApplication = {
                        applicationDate: null,
                        applicationLeaveStatus: null,
                        leaveFromDate: null,
                        leaveToDate: null,
                        isHalfDayLeave: null,
                        halfDayLeaveInfo: null,
                        reasonOfLeave: null,
                        contactNo: null,
                        isWithCertificate: null,
                        assignResposibilty: null,
                        paymentType: null,
                        activeStatus: false,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);

/*almEmpLeaveApplication-dialog.controller.js*/

angular.module('stepApp').controller('AlmEmpLeaveApplicationDialogController',
    ['$scope', '$rootScope', '$sce', '$http', '$stateParams', '$state', 'entity', 'AlmEmpLeaveApplication', 'AlmLeavGrpTypeMap', 'AlmLeaveRule', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfo', 'AlmLeaveType', 'AlmEmpLeaveBalance', 'DataUtils','HrDepartmentActiveHeadInfoByDept',
        function ($scope, $rootScope, $sce, $http, $stateParams, $state, entity, AlmEmpLeaveApplication, AlmLeavGrpTypeMap, AlmLeaveRule, User, Principal, DateUtils, HrEmployeeInfo, AlmLeaveType, AlmEmpLeaveBalance, DataUtils, HrDepartmentActiveHeadInfoByDept) {

            $scope.almEmpLeaveApplication = entity;
            $scope.hremployeeinfos = [];
            $scope.almleavetypes = [];
            $scope.deptHead = [];

            HrEmployeeInfo.query(function (result) {
                angular.forEach(result, function (dtoInfo) {
                    if (dtoInfo.activeStatus) {
                        $scope.hremployeeinfos.push(dtoInfo);
                    }
                });
            });

            AlmLeaveType.query(function (result) {
                angular.forEach(result, function (dtoInfo) {
                    if (dtoInfo.activeStatus) {
                        $scope.almleavetypes.push(dtoInfo);
                    }
                });
            });

            $scope.almLeaveRule = null;

            $scope.load = function (id) {
                AlmEmpLeaveApplication.get({id: id}, function (result) {
                    $scope.almEmpLeaveApplication = result;
                });
                // $scope.onChangeLeaveType();
            };

            HrEmployeeInfo.get({id: 'my'}, function (result) {

                $scope.hrEmployeeInfo = result;
                $scope.designationName = $scope.hrEmployeeInfo.designationInfo.designationName;
                $scope.departmentName = $scope.hrEmployeeInfo.departmentInfo.departmentName;
               // $scope.workAreaName = $scope.hrEmployeeInfo.workArea.typeName;
                HrDepartmentActiveHeadInfoByDept.get({deptId: $scope.hrEmployeeInfo.departmentInfo.id}, function (result) {
                    console.log(result);
                    $scope.deptHead = result;
                });
                //$scope.loadGroupByEmployee();
            });

            /*$scope.almLeaveGroup = null;
             $scope.loadGroupByEmployee = function(){
             $scope.almLeaveGroup = null;
             AlmEmpLeaveGroupMap.query(function(result){
             angular.forEach(result,function(dtoInfo){
             if(dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id){
             $scope.almLeaveGroup = dtoInfo.almLeaveGroup;
             $scope.almEmpLeaveApplication.almLeaveGroup = dtoInfo.almLeaveGroup;
             }
             });
             $scope.loadLeaveTypeByLeaveGroup();
             });
             };

             $scope.loadLeaveTypeByLeaveGroup = function(){
             $scope.almleavetypes = [];
             if($scope.almLeaveGroup){
             AlmLeavGrpTypeMap.query(function(result){
             angular.forEach(result,function(dtoInfo){
             if(dtoInfo.almLeaveGroup.id == $scope.almLeaveGroup.id){
             $scope.almleavetypes.push(dtoInfo.almLeaveType);
             }
             });
             });
             }
             };*/

            $scope.onChangeLeaveType = function () {
                $scope.loadAlmLeaveRule();
                $scope.getEmpLeaveBalanceInfo();
            };
            $scope.isGenderCheck = true;
            $scope.genderErrorMessage = '';
            $scope.loadAlmLeaveRule = function () {
                $scope.isGenderCheck = true;
                $scope.almLeaveRule = null;
                AlmLeaveRule.query(function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.almLeaveType.id == $scope.almEmpLeaveApplication.almLeaveType.id) {
                            console.log($scope.hrEmployeeInfo.gender);
                            if (dtoInfo.almGender != "Both" && dtoInfo.almGender != $scope.hrEmployeeInfo.gender) {
                                $scope.isGenderCheck = false;
                                $scope.genderErrorMessage = "You are not allowed to take this leave."
                            }
                            $scope.almLeaveRule = dtoInfo;
                        }
                    });
                });
            };


            $scope.getEmpLeaveBalanceInfo = function () {
                $scope.almEmpLeaveApplication.leaveBalance = 0;
                AlmEmpLeaveBalance.query(function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id
                            && dtoInfo.almLeaveType.id == $scope.almEmpLeaveApplication.almLeaveType.id) {
                            $scope.almEmpLeaveApplication.leaveBalance = dtoInfo.leaveBalance;
                            $scope.originalLeaveBalance = dtoInfo.leaveBalance;
                            $scope.almEmpLeaveBalance = dtoInfo;
                        }

                    });
                });

                /*var data = {
                 employeeId:     $scope.hrEmployeeInfo.id,
                 leaveTypeId:    $scope.almEmpLeaveApplication.almLeaveType.id

                 };

                 var config = {
                 params: data,
                 headers : {'Accept' : 'application/json'}
                 };
                 $http.get('api/almEmpLeaveBalByEmpInfoAndLeaveType/', config).then(function(response){

                 $scope.almEmpLeaveApplication.leaveBalance = response.data.leaveBalance;
                 $scope.leaveOnApply = response.data.leaveOnApply;


                 }, function(response){
                 console.log("Failed: ");
                 });*/
            };

            $scope.isDateCheck = true;
            $scope.fromDateCheckMsg = '';
            $scope.fromDateChecking = function () {
                $scope.isDateCheck = true;
                $scope.fromDateCheckMsg = '';
                if ($scope.isHalfDayLeave) {
                    $scope.almEmpLeaveApplication.leaveDays = 0.5;
                    $scope.almEmpLeaveApplication.leaveToDate = null;
                    $scope.almLeaveRuleCheck();
                } else {
                    $scope.fromDateF = $scope.almEmpLeaveApplication.leaveFromDate;
                    $scope.toDateT = $scope.almEmpLeaveApplication.leaveToDate;
                    if ($scope.fromDateF == '' || $scope.fromDateF == null) {
                        $scope.fromDateCheckMsg = "Select From Date First";
                        $scope.isDateCheck = false;
                    }
                    if ($scope.fromDateF != '' && $scope.fromDateF != null && $scope.toDateT != '' && $scope.toDateT != null) {
                        if ($scope.fromDateF > $scope.toDateT) {
                            $scope.fromDateCheckMsg = "From Date should be less than End Date.";
                            $scope.isDateCheck = false;
                        }
                        $scope.t2 = $scope.toDateT.getTime();
                        $scope.t1 = $scope.fromDateF.getTime();
                        $scope.result = parseInt(($scope.t2 - $scope.t1) / (24 * 3600 * 1000));
                        $scope.almEmpLeaveApplication.leaveDays = $scope.result + 1;
                        if ($scope.almEmpLeaveApplication.leaveDays > 15.0) {
                            $scope.showDdo = true;
                        } else {
                            $scope.showDdo = false;
                        }
                        $scope.almLeaveRuleCheck();
                    }
                }

            };
            $scope.showDdo = false;
            $scope.toDateChecking = function () {
                $scope.isDateCheck = true;
                $scope.fromDateCheckMsg = '';
                //$scope.checkAlmLeaveRuleMinimumGap();
                $scope.duplicateCheckByFromDateAndToDate();
                $scope.fromDateF = $scope.almEmpLeaveApplication.leaveFromDate;
                $scope.toDateT = $scope.almEmpLeaveApplication.leaveToDate;
                if ($scope.fromDateF == '' || $scope.fromDateF == null) {
                    $scope.fromDateCheckMsg = "Select From Date First";
                    $scope.isDateCheck = false;
                }
                if ($scope.fromDateF != '' && $scope.fromDateF != null && $scope.toDateT != '' && $scope.toDateT != null) {
                    if ($scope.toDateT < $scope.fromDateF) {
                        $scope.fromDateCheckMsg = "From Date should be less than End Date.";
                        $scope.isDateCheck = false;
                    }
                    $scope.t2 = $scope.toDateT.getTime();
                    $scope.t1 = $scope.fromDateF.getTime();
                    $scope.result = parseInt(($scope.t2 - $scope.t1) / (24 * 3600 * 1000));
                    $scope.almEmpLeaveApplication.leaveDays = $scope.result + 1;
                    if ($scope.almEmpLeaveApplication.leaveDays > 15.0) {
                        $scope.showDdo = true;
                    } else {
                        $scope.showDdo = false;
                    }
                    $scope.almLeaveRuleCheck();
                }
            };

            $scope.isHalfDayLeaveCheck = function () {
                if ($scope.almEmpLeaveApplication.isHalfDayLeave) {

                    $scope.almEmpLeaveApplication.leaveDays = 0.5;
                    $scope.almEmpLeaveApplication.leaveToDate = null;
                    $scope.almLeaveRuleCheck();
                }
            };

            $scope.almLeaveRuleCheck = function () {
                $scope.checkAlmLeaveRuleNegativeBalance();
                $scope.checkAlmLeaveRuleConsecutiveBalance();
                $scope.checkAlmLeaveRuleNoOfInstance();
            };

            $scope.almNegErrorMessage = '';
            $scope.isNegativeBalanceCheck = true;
            $scope.checkAlmLeaveRuleNegativeBalance = function () {
                $scope.almEmpLeaveApplication.leaveBalance = 0;
                $scope.isNegativeBalanceCheck = true;
                $scope.almNegErrorMessage = '';
                var leaveBalance = 0;
                var originalLeaveBalance = 0;
                originalLeaveBalance = $scope.originalLeaveBalance;
                $scope.almEmpLeaveApplication.leaveBalance = originalLeaveBalance - $scope.almEmpLeaveApplication.leaveDays;
                leaveBalance = $scope.almEmpLeaveApplication.leaveBalance;
                if (!$scope.almLeaveRule.isNegBalanceAllowed && leaveBalance < 0) {
                    $scope.almNegErrorMessage = 'Leave Balance Cannot be negative.';
                    $scope.isNegativeBalanceCheck = false;
                } else if ($scope.almLeaveRule.isNegBalanceAllowed && -(leaveBalance) > $scope.almLeaveRule.maxNegBalance) {
                    $scope.almNegErrorMessage = 'Leave Balance Cannot smaller than ' + $scope.almLeaveRule.maxNegBalance;
                    $scope.isNegativeBalanceCheck = false;
                } else {
                    $scope.isNegativeBalanceCheck = true;
                }
            };

            $scope.almConsectiveLeaveErrorMessage = '';
            $scope.isConsecutiveBalanceCheck = true;
            $scope.checkAlmLeaveRuleConsecutiveBalance = function () {
                $scope.isConsecutiveBalanceCheck = true;
                $scope.almConsectiveLeaveErrorMessage = '';

                if ($scope.almEmpLeaveApplication.leaveDays > $scope.almLeaveRule.maxConsecutiveLeaves) {
                    $scope.almConsectiveLeaveErrorMessage = 'Leave Days Cannot be greater than ' + $scope.almLeaveRule.maxConsecutiveLeaves;
                    $scope.isConsecutiveBalanceCheck = false;
                }
            };

            $scope.almMinGapErrorMessage = '';
            $scope.isMinimumGapCheck = true;
            $scope.checkAlmLeaveRuleMinimumGap = function () {
                $scope.isMinimumGapCheck = true;
                $scope.almMinGapErrorMessage = '';
                $scope.fromDateF = $scope.almEmpLeaveApplication.leaveFromDate;
                var gap = -$scope.almLeaveRule.minGapBetweenTwoLeaves;
                $scope.fd = new Date($scope.fromDateF.getTime() + gap * 24 * 60 * 60 * 1000);
                console.log('Checking date: ' + $scope.fd);
                AlmEmpLeaveApplication.query(function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id
                            && dtoInfo.almLeaveType.id == $scope.almEmpLeaveApplication.almLeaveType.id
                            && dtoInfo.applicationLeaveStatus != 'Rejected'
                            && DateUtils.convertLocaleDateToServer($scope.fd) <= dtoInfo.leaveFromDate) {
                            $scope.almMinGapErrorMessage = 'Minimum Gap Between two leaves ' + $scope.almLeaveRule.minGapBetweenTwoLeaves;
                            $scope.isMinimumGapCheck = false;
                        }
                    });
                });
            };

            $scope.isExitsData = true;
            $scope.duplicateCheckByFromDateAndToDate = function () {
                $scope.isExitsData = true;
                $scope.almDuplicateErrorMessage = '';
                $scope.fromDateL = $scope.almEmpLeaveApplication.leaveFromDate;
                AlmEmpLeaveApplication.query(function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id
                            && dtoInfo.leaveFromDate == DateUtils.convertLocaleDateToServer($scope.fromDateL)) {
                            $scope.isExitsData = false;
                            $scope.almDuplicateErrorMessage = 'You already applied leave for this day.';
                        }
                    });
                });
            };

            $scope.almLeaveNoOfInsErrorMessage = '';
            $scope.isNoOfInstanceCheck = true;
            $scope.checkAlmLeaveRuleNoOfInstance = function () {
                $scope.isNoOfInstanceCheck = true;
                $scope.almLeaveNoOfInsErrorMessage = '';
                var noOfInstance = 0;
                AlmEmpLeaveApplication.query(function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id
                            && dtoInfo.almLeaveType.id == $scope.almEmpLeaveApplication.almLeaveType.id
                            && dtoInfo.applicationLeaveStatus == 'Approved') {
                            noOfInstance += 1;
                        }
                    });
                });
                if (noOfInstance > $scope.almLeaveRule.noOfInstanceAllowed) {
                    $scope.almLeaveNoOfInsErrorMessage = 'You can not take this leave more than ' + $scope.almLeaveRule.noOfInstanceAllowed;
                    $scope.isNoOfInstanceCheck = false;
                }
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };

            $scope.populateEmployeeInfo = function (employeeInfo) {

                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                });
                $scope.designationName = $scope.hrEmployeeInfo.designationInfo.designationName;
                $scope.departmentName = $scope.hrEmployeeInfo.departmentInfo.departmentName;
                $scope.workAreaName = $scope.hrEmployeeInfo.workArea.typeName;
            };

            $scope.getLoggedInUser();

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setLeaveCertificate = function ($file, almEmpLeaveApplication) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            almEmpLeaveApplication.leaveCertificate = base64Data;
                            almEmpLeaveApplication.leaveCertContentType = $file.type;
                            if (almEmpLeaveApplication.id == null) {
                                almEmpLeaveApplication.leaveCertificateName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.leaveCertificate, modelInfo.leaveCertificateContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.leaveCertificateContentType;
                $rootScope.viewerObject.pageTitle = "Certificate Document";
                $rootScope.showPreviewModal();
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almEmpLeaveApplicationUpdate', result);
                $scope.isSaving = false;

                if ($scope.almEmpLeaveBalance) {
                    $scope.almEmpLeaveBalance.leaveBalance = $scope.almEmpLeaveApplication.leaveBalance;
                    $scope.almEmpLeaveBalance.updateBy = $scope.loggedInUser.id;
                    $scope.almEmpLeaveBalance.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.almEmpLeaveBalance.leaveOnApply = $scope.almEmpLeaveBalance.leaveOnApply + $scope.almEmpLeaveApplication.leaveDays;
                    AlmEmpLeaveBalance.update($scope.almEmpLeaveBalance);
                }
                $state.go("almEmpLeaveApplication");
            };

            $scope.save = function () {
                $scope.almEmpLeaveApplication.updateBy = $scope.loggedInUser.id;
                $scope.almEmpLeaveApplication.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.almEmpLeaveApplication.applicationDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.almEmpLeaveApplication.employeeInfo = $scope.hrEmployeeInfo;
                $scope.almEmpLeaveApplication.applicationLeaveStatus = 'Pending';

                if ($scope.preCondition()) {
                    if ($scope.almEmpLeaveApplication.id != null) {
                        AlmEmpLeaveApplication.update($scope.almEmpLeaveApplication, onSaveFinished);
                        $rootScope.setWarningMessage('stepApp.almEmpLeaveApplication.updated');
                    } else {
                        $scope.almEmpLeaveApplication.nextApprover = $scope.deptHead;
                        $scope.almEmpLeaveApplication.createBy = $scope.loggedInUser.id;
                        $scope.almEmpLeaveApplication.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        AlmEmpLeaveApplication.save($scope.almEmpLeaveApplication, onSaveFinished);
                        $rootScope.setSuccessMessage('stepApp.almEmpLeaveApplication.created');
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.date = new Date();
            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.applicationDate',DateUtils.convertLocaleDateToDMY($scope.almEmpLeaveApplication.applicationDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.leaveFromDate',DateUtils.convertLocaleDateToDMY($scope.almEmpLeaveApplication.leaveFromDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.leaveToDate',DateUtils.convertLocaleDateToDMY($scope.almEmpLeaveApplication.leaveToDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.leaveDays', $scope.almEmpLeaveApplication.leaveDays, 'number', 2, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.responsibleEmpCont', $scope.almEmpLeaveApplication.responsibleEmpCont, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.responsibleEmpAdd', $scope.almEmpLeaveApplication.responsibleEmpAdd, 'text', 200, 10, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.contactNo', $scope.almEmpLeaveApplication.contactNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.emergencyContactNo', $scope.almEmpLeaveApplication.emergencyContactNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.addressWhileLeave', $scope.almEmpLeaveApplication.addressWhileLeave, 'text', 200, 10, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('almEmpLeaveApplication.reasonOfLeave', $scope.almEmpLeaveApplication.reasonOfLeave, 'text', 200, 10, 'atozAndAtoZ0-9.-', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.initiateAlmEmpLeaveBalanceModel = function () {
                return {
                    yearOpenDate: null,
                    year: null,
                    leaveEarned: null,
                    leaveTaken: null,
                    leaveForwarded: null,
                    attendanceLeave: null,
                    leaveOnApply: null,
                    leaveBalance: null,
                    activeStatus: true
                };
            };

        }]);


/*almEmpLeaveApplication-detail.controller.js*/

angular.module('stepApp')
    .controller('AlmEmpLeaveApplicationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmEmpLeaveApplication',
            function ($scope, $rootScope, $stateParams, entity, AlmEmpLeaveApplication) {
                $scope.almEmpLeaveApplication = entity;
                $scope.load = function (id) {
                    AlmEmpLeaveApplication.get({id: id}, function (result) {
                        $scope.almEmpLeaveApplication = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almEmpLeaveApplicationUpdate', function (event, result) {
                    $scope.almEmpLeaveApplication = result;
                    $rootScope.setErrorMessage('stepApp.almEmpLeaveApplication.deleted');
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*almApplicationRequest.controller.js*/
angular.module('stepApp')
    .controller('AlmApproveDialogController',
        ['$scope','$stateParams','$rootScope','$q','$modalInstance','AlmEmpLeaveApplication','ApproverHrEmployees','AlmEmpLeaveApplicationApprove','Principal','User','DateUtils','AlmEmpLeaveCancellation',
            function ($scope,$stateParams,$rootScope,$q,$modalInstance,AlmEmpLeaveApplication,ApproverHrEmployees,AlmEmpLeaveApplicationApprove,Principal,User,DateUtils,AlmEmpLeaveCancellation) {
                console.log($stateParams.id)
                $scope.approverList = []
                $scope.application = {}
                $scope.almApplicationStatusLog = {
                    status:"",
                    remarks:"",
                    applicationType:$stateParams.applicationType,
                    applicationId:$stateParams.id,
                    employeeId:null,
                    createDate:null,
                    updateDate:null,
                    createBy:null,
                    updateBy:null,
                    nextApprover:null,
                    currentApproverRemarks:"",
                }
                AlmEmpLeaveApplication.get({id:$stateParams.id},function (result) {
                    console.log(result)
                    $scope.application = result;
                    $scope.currentApprover = result.nextApprover
                    $q.all($scope.currentApprover).then(function () {
                        console.log($scope.currentApprover)
                        $scope.almApplicationStatusLog.employeeId = $scope.currentApprover.id

                        ApproverHrEmployees.get({gradeCode:$scope.currentApprover.gradeInfo.gradeCode}, function(result){
                            console.log("-------------------Approver---------------------------")
                            console.log(result)
                            $scope.approverList = result;
                        })
                    })
                });
                $scope.confirmApprove = function(){
                    Principal.identity().then(function (account) {
                        User.get({login: account.login}, function (result) {
                            console.log(result)
                            $scope.almApplicationStatusLog.status = "Forwarded";
                            $scope.almApplicationStatusLog.updateBy = result;
                            $scope.almApplicationStatusLog.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                            $scope.almApplicationStatusLog.createBy = result;
                            $scope.almApplicationStatusLog.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            $scope.almApplicationStatusLog.employeeInfo = $scope.hrEmployeeInfo;
                            $scope.almApplicationStatusLog.applicationLeaveStatus = 'Pending';
                            $scope.almApplicationStatusLog.remarks = 'Application Forwarded to '+$scope.almApplicationStatusLog.nextApprover.fullName+
                                ' - ' + $scope.almApplicationStatusLog.nextApprover.designationInfo.designationInfo.designationName ;
                            console.log($scope.almApplicationStatusLog)
                            AlmEmpLeaveApplicationApprove.approve($scope.almApplicationStatusLog, function (result) {
                                $scope.application.nextApprover = $scope.almApplicationStatusLog.nextApprover
                                $scope.application.remarks =  $scope.application.remarks + 'Application Forwarded to '+
                                                                $scope.almApplicationStatusLog.nextApprover.fullName +
                                                                ' - ' + $scope.almApplicationStatusLog.nextApprover.designationInfo.designationInfo.designationName
                                if($stateParams.applicationType == 'LEAVE_CANCELLATION'){
                                    AlmEmpLeaveCancellation.update($scope.application,onSaveSuccess)
                                } else{
                                    AlmEmpLeaveApplication.update($scope.application,onSaveSuccess)
                                }

                            }, onSaveError);
                        });
                    });


                    //AlmEmpLeaveApplicationApprove.approve($scope.almApplicationStatusLog, onSaveSuccess, onSaveError);

                };

                var onSaveSuccess = function(result){
                    console.log('-------------save success-------')
                    console.log(result);
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('Application Forwarded Successfully.');
                };

                var onSaveError = function(result){
                    console.log('-------------save failed-------')
                    console.log(result);
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('Application Forwarded Successfully.');
                };
                $scope.clear = function(){
                    $modalInstance.close();
                    window.history.back();
                    //$state.go('mpo.details');
                }

            }]);
angular.module('stepApp')
    .controller('AlmApplicationRequestLController',
        ['$scope', '$state', '$rootScope', '$timeout', 'DataUtils', 'AlmEmpLeaveApplication', 'HrEmployeeInfo', 'Principal', 'DateUtils', 'AlmEmpLeaveBalance', 'ApproverHrEmployees','$q',
            function ($scope, $state, $rootScope, $timeout, DataUtils, AlmEmpLeaveApplication, HrEmployeeInfo, Principal, DateUtils, AlmEmpLeaveBalance, ApproverHrEmployees,$q) {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;
                $scope.currentApprover = null;

                $scope.loggedInUser = {};
                $scope.getLoggedInUser = function () {
                    Principal.identity().then(function (account) {
                        User.get({login: account.login}, function (result) {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function () {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];
                    Principal.identity().then(function (account) {
                        AlmEmpLeaveApplication.query({login:account.login},function (result) {
                            $scope.requestEntityCounter++;
                            angular.forEach(result, function (dtoInfo) {
                                console.log(dtoInfo.nextApprover)
                                if($scope.currentApprover == null){
                                    $scope.currentApprover = dtoInfo.nextApprover
                                }
                                if (dtoInfo.applicationLeaveStatus == 'Pending') {
                                    $scope.newRequestList.push(dtoInfo);
                                }

                            });
                            // $q.all([result]).then(function () {
                            //     console.log('---------------------------------')
                            //     console.log($scope.currentApprover)
                            //     if($scope.currentApprover !=null){
                            //         ApproverHrEmployees.get({gradeCode:$scope.currentApprover.gradeInfo.gradeCode}, function(result){
                            //             console.log("-------------------Approver---------------------------")
                            //             console.log(result)
                            //         })
                            //     }
                            // })
                            $scope.loadApprovedRejectList(result);
                        });
                        $scope.newRequestList.sort($scope.sortById);

                    });


                };

                $scope.sortById = function (a, b) {
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function () {
                    if ($scope.approvalObj.isApproved)
                        $scope.approvalObj.applicationLeaveStatus = 'Approved';
                    else $scope.approvalObj.applicationLeaveStatus = 'Reject';
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function () {
                    if ($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.applicationLeaveStatus = 'Approved';
                    }
                    else {
                        $scope.approvalObj.applicationLeaveStatus = 'Reject';
                    }
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj) {
                    AlmEmpLeaveApplication.update(requestObj, function (result) {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        $scope.updateLeaveBalance(requestObj);
                        $scope.loadAll();
                    });
                };

                $scope.updateLeaveBalance = function (requestObj) {
                    $scope.isSaving = false;
                    AlmEmpLeaveBalance.query(function (result) {
                        angular.forEach(result, function (dtoInfo) {
                            if (dtoInfo.employeeInfo.id == requestObj.employeeInfo.id
                                && dtoInfo.almLeaveType.id == requestObj.almLeaveType.id) {
                                $scope.almEmpLeaveBalance = dtoInfo;
                                if (requestObj.applicationLeaveStatus == 'Approved') {
                                    dtoInfo.leaveOnApply = dtoInfo.leaveOnApply - requestObj.leaveDays;
                                } else {
                                    dtoInfo.leaveBalance = dtoInfo.leaveBalance + requestObj.leaveDays;
                                    dtoInfo.leaveOnApply = dtoInfo.leaveOnApply - requestObj.leaveDays;
                                }
                                dtoInfo.updateBy = $scope.loggedInUser.id;
                                dtoInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                                AlmEmpLeaveBalance.update(dtoInfo);
                            }

                        });
                    });

                };

                $scope.loadApprovedRejectList = function (result) {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                    angular.forEach(result, function (requestObj) {
                        if (requestObj.applicationLeaveStatus == 'Approved') {
                            $scope.approvedList.push(requestObj);
                        }
                        if (requestObj.applicationLeaveStatus == 'Reject') {
                            $scope.rejectedList.push(requestObj);
                        }
                    });
                };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source) {
                    if (source == 'request') {
                        $timeout(function () {
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source) {
                    if (source == 'approved') {
                        $timeout(function () {
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source) {
                    if (source == 'approved') {
                        $timeout(function () {
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };

                $scope.approvalViewDetail = function (dataObj) {
                    $scope.approvalObj = dataObj;
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType) {
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        employeeId: null,
                        entityName: null,
                        requestFrom: null,
                        requestSummary: null,
                        requestDate: null,
                        approveState: null,
                        logStatus: null,
                        logComments: null,
                        actionType: '',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function (event, data) {
                    $scope.loadAll();
                });


                $scope.loadEmployee = function () {
                    HrEmployeeInfo.get({id: 'my'}, function (result) {
                        $scope.hrEmployeeInfo = result;

                    }, function (response) {
                        $scope.hasProfile = false;
                        $scope.noEmployeeFound = true;
                        $scope.isSaving = false;
                    })
                };

                $scope.sort = function (keyname, source) {
                    if (source == 'request') {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if (source == 'approved') {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if (source == 'rejected') {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };
                $scope.loadAll();

            }])
;
