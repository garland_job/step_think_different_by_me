'use strict';

angular.module('stepApp')
    .controller('AlmWeekendConfigurationController',
    ['$scope', 'AlmWeekendConfiguration', 'AlmWeekendConfigurationSearch', 'ParseLinks',
    function ($scope, AlmWeekendConfiguration, AlmWeekendConfigurationSearch, ParseLinks) {
        $scope.almWeekendConfigurations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmWeekendConfiguration.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almWeekendConfigurations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmWeekendConfiguration.get({id: id}, function(result) {
                $scope.almWeekendConfiguration = result;
                $('#deleteAlmWeekendConfigurationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmWeekendConfiguration.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmWeekendConfigurationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmWeekendConfigurationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almWeekendConfigurations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almWeekendConfiguration = {
                dayName: null,
                isHalfDay: null,
                halfDay: null,
                description: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almWeekendConfiguration-dialog.controller.js*/

angular.module('stepApp').controller('AlmWeekendConfigurationDialogController',
    ['$scope', '$rootScope','$stateParams', 'entity', 'AlmWeekendConfiguration', 'Principal', '$state', 'User', 'DateUtils',
        function($scope, $rootScope,$stateParams, entity, AlmWeekendConfiguration, Principal,  $state, User, DateUtils) {

            $scope.almWeekendConfiguration = entity;
            $scope.load = function(id) {
                AlmWeekendConfiguration.get({id : id}, function(result) {
                    $scope.almWeekendConfiguration = result;
                });
            };
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:almWeekendConfigurationUpdate', result);
                $scope.isSaving = false;
                $state.go('almWeekendConfiguration');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.isExitsData = true;
            $scope.duplicateCheckByDayName = function(){
                $scope.isExitsData = true;
                AlmWeekendConfiguration.query(function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.dayName == $scope.almWeekendConfiguration.dayName){
                            $scope.isExitsData = false;
                        }
                    });
                });
            };

            $scope.save = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.isSaving = true;
                        $scope.almWeekendConfiguration.updateBy = result.id;
                        $scope.almWeekendConfiguration.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.almWeekendConfiguration.id != null)
                        {
                            AlmWeekendConfiguration.update($scope.almWeekendConfiguration, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.almWeekendConfiguration.updated');
                        }
                        else
                        {
                            $scope.almWeekendConfiguration.activeStatus = true;
                            $scope.almWeekendConfiguration.createBy = result.id;
                            $scope.almWeekendConfiguration.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            AlmWeekendConfiguration.save($scope.almWeekendConfiguration, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.almWeekendConfiguration.created');
                        }
                    });
                });
            };
        }]);
/*almWeekendConfiguration-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmWeekendConfigurationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmWeekendConfiguration',
            function ($scope, $rootScope, $stateParams, entity, AlmWeekendConfiguration) {
                $scope.almWeekendConfiguration = entity;
                $scope.load = function (id) {
                    AlmWeekendConfiguration.get({id: id}, function(result) {
                        $scope.almWeekendConfiguration = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almWeekendConfigurationUpdate', function(event, result) {
                    $scope.almWeekendConfiguration = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
