'use strict';

angular.module('stepApp')
    .controller('LeaveAttendanceController',
    ['$scope', '$state',
    function($scope, $state ){
        $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"];
        $scope.data = [300, 500, 100, 40, 120];
    }]);

