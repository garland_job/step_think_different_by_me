'use strict';

angular.module('stepApp')
    .controller('AlmAttendanceConfigurationController',

    ['$scope', 'AlmAttendanceConfiguration', 'AlmAttendanceConfigurationSearch', 'ParseLinks',
    function ($scope, AlmAttendanceConfiguration, AlmAttendanceConfigurationSearch, ParseLinks) {
        $scope.almAttendanceConfigurations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmAttendanceConfiguration.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                console.log(result);
                $scope.almAttendanceConfigurations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmAttendanceConfiguration.get({id: id}, function(result) {
                $scope.almAttendanceConfiguration = result;
                $('#deleteAlmAttendanceConfigurationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmAttendanceConfiguration.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmAttendanceConfigurationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmAttendanceConfigurationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almAttendanceConfigurations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almAttendanceConfiguration = {
                employeeType: null,
                effectedDateFrom: null,
                effectedDateTo: null,
                isUntilFurtherNotice: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }])
angular.module('stepApp')
    .controller('AlmTimeSheetUploadController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'ALmUploadTimeSheet',
    function ($scope, $rootScope, $stateParams, $state, entity, ALmUploadTimeSheet) {
        $scope.attachment = entity;
        $scope.page = 0;
        $scope.setFile = function ($file) {
            try {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {

                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            try {
                                $scope.attachment.file = base64Data;
                                $scope.attachment.fileContentType = $file.type;
                                $scope.attachment.fileName = $file.name;
                                console.log($scope.attachment)
                            } catch (e) {
                                console.log(e)
                            }
                        });
                    };
                }
            } catch (e) { console.log(e)}
        };
        var onSaveFinished = function (result) {
           console.log("------------------------------")
           console.log(result)
        };
        $scope.save = function () {
                $rootScope.setSuccessMessage('stepApp.almAttendanceConfiguration.created');
            ALmUploadTimeSheet.save($scope.attachment, onSaveFinished);

            };

    }]);
/*almAttendanceConfiguration-dialog.controller.js*/

angular.module('stepApp').controller('AlmAttendanceConfigurationDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'AlmAttendanceConfiguration','User','Principal','DateUtils', 'AlmShiftSetup','GetAllHrEmployeeInfo', 'HrDepartmentSetupByStatus', 'HrEmployeesByDepartment', '$q',
        function($scope, $rootScope, $stateParams, $state, entity, AlmAttendanceConfiguration, User, Principal, DateUtils, AlmShiftSetup, GetAllHrEmployeeInfo, HrDepartmentSetupByStatus, HrEmployeesByDepartment, $q) {

            $scope.almAttendanceConfiguration = entity;
            $scope.hremployeeinfos = [];
            $scope.hremployeeinfosbydept = [];
            $scope.hrDepartments = [];
            $scope.almshiftsetups = [];
            $scope.predicate = 'id';
            $scope.reverse = true;
            $scope.page = 0;
            $scope.department = null;
            $scope.selectedEmployee = [];

            //HrEmployeeInfo.query({page: $scope.page, size: 10000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
            //    $scope.hremployeeinfos = result;
            //});
            if(null == $scope.almAttendanceConfiguration.id){
                HrDepartmentSetupByStatus.get({stat:'true'},function(result){
                    $scope.hrDepartments = result;
                });

            }else{
                GetAllHrEmployeeInfo.query({},function(result){
                    result(result)
                    $scope.hremployeeinfos = result;
                });
            }

            // HrDepartmentSetupByStatus.get({stat:'true'});

            AlmShiftSetup.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almshiftsetups.push(dtoInfo);
                    }
                });
            });
            $scope.load = function(id) {
                AlmAttendanceConfiguration.get({id : id}, function(result) {
                    $scope.almAttendanceConfiguration = result;
                });
            };
            $scope.customFilter = function(hremployee) {
                if ($scope.q) {
                    return (hremployee.fullName + hremployee.employeeId + hremployee.designationInfo.designationInfo.designationName ).indexOf($scope.q) >= 0;
                } else {
                    return true;
                }
            };
            $scope.findEmployeeByDept = function(department) {
                console.log("------------------------------------------------------------");
                console.log(department);
                HrEmployeesByDepartment.get({deptId : department.id}, function(result) {
                    $scope.hremployeeinfosbydept = result;
                    $scope.selectedEmployee = [];
                    console.log( $scope.selectedEmployee.length)
                });


            };

            $scope.selectAll = function() {
                var toggleStatus = $scope.isAllSelected;
                angular.forEach($scope.hremployeeinfosbydept, function(hremployeeinfo){
                    if($scope.customFilter(hremployeeinfo)){
                        if(toggleStatus){
                            $scope.selectedEmployee.push(hremployeeinfo)
                        }else{
                            var index = $scope.selectedEmployee.indexOf(hremployeeinfo);
                            if (index > -1) {
                                $scope.selectedEmployee.splice(index, 1);
                            }

                        }
                        hremployeeinfo.selected = toggleStatus;
                    }
                });
                console.log($scope.selectedEmployee);

            }

            $scope.optionSelect = function(hremployee){
                if(hremployee.selected){
                    $scope.selectedEmployee.push(hremployee)
                }else{
                    var index = $scope.selectedEmployee.indexOf(hremployee);
                    if (index > -1) {
                        $scope.selectedEmployee.splice(index, 1);
                    }

                }
                $scope.isAllSelected = $scope.hremployeeinfosbydept.every(function(hremployeeinfo){
                    return hremployeeinfo.selected;
                })
                console.log($scope.selectedEmployee);
            }
            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almAttendanceConfigurationUpdate', result);
                $scope.isSaving = false;
                $state.go("almAttendanceConfiguration");
            };

            $scope.save = function () {
                $scope.almAttendanceConfiguration.updateBy = $scope.loggedInUser.id;
                $scope.almAttendanceConfiguration.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almAttendanceConfiguration.id != null) {
                    AlmAttendanceConfiguration.update($scope.almAttendanceConfiguration, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almAttendanceConfiguration.updated');
                } else {
                    $rootScope.setSuccessMessage('stepApp.almAttendanceConfiguration.created');
                    angular.forEach($scope.selectedEmployee, function(hremployeeinfo){
                        if(hremployeeinfo.selected){
                            console.log("-----------------------selected----------------");
                            $scope.almAttendanceConfiguration.id = null;
                            $scope.almAttendanceConfiguration.employeeInfo = hremployeeinfo;
                            $scope.almAttendanceConfiguration.employeeType = 'will be added later.';
                            $scope.almAttendanceConfiguration.createBy = $scope.loggedInUser.id;
                            $scope.almAttendanceConfiguration.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            AlmAttendanceConfiguration.save($scope.almAttendanceConfiguration);
                        }
                    })
                    $q.all($scope.selectedEmployee).then(function () {
                        onSaveFinished([]);
                    });

                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);

/*almAttendanceConfiguration-detail.controller.js*/

angular.module('stepApp')
    .controller('AlmAttendanceConfigurationDetailController',

        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmAttendanceConfiguration', 'HrEmployeeInfo', 'AlmShiftSetup',
            function ($scope, $rootScope, $stateParams, entity, AlmAttendanceConfiguration, HrEmployeeInfo, AlmShiftSetup) {
                $scope.almAttendanceConfiguration = entity;
                $scope.load = function (id) {
                    AlmAttendanceConfiguration.get({id: id}, function(result) {
                        $scope.almAttendanceConfiguration = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almAttendanceConfigurationUpdate', function(event, result) {
                    $scope.almAttendanceConfiguration = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
