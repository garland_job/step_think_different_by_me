'use strict';

angular.module('stepApp')
    .controller('AlmOnDutyLeaveAppController',
    ['$scope','HrEmployeeInfo', 'AlmOnDutyLeaveApp', 'AlmOnDutyLeaveAppSearch', 'ParseLinks',
    function ($scope,HrEmployeeInfo, AlmOnDutyLeaveApp, AlmOnDutyLeaveAppSearch, ParseLinks) {
        $scope.almOnDutyLeaveApps = [];
        $scope.page = 0;


        HrEmployeeInfo.get({id: 'my'}, function (result) {
            $scope.hrEmployeeInfo = result;

        });


        $scope.loadAll = function()
        {
            $scope.requestEntityCounter = 1;
            $scope.newRequestList = [];
            AlmOnDutyLeaveApp.query(function(result)
            {
                $scope.requestEntityCounter++;
                angular.forEach(result,function(dtoInfo)
                {
                    if(dtoInfo.employeeInfo.id == $scope.hrEmployeeInfo.id){
                        $scope.newRequestList.push(dtoInfo);
                    }
                });
            });
            $scope.newRequestList.sort($scope.sortById);
        };

        $scope.loadAll();

        $scope.sortById = function(a,b){
            return b.id - a.id;
        };

        $scope.searchText = "";
        $scope.updateSearchText = "";

        $scope.clearSearchText = function (source)
        {
            if(source=='request')
            {
                $timeout(function(){
                    $('#searchText').val('');
                    angular.element('#searchText').triggerHandler('change');
                });
            }
        };


       /* $scope.loadAll = function() {
            AlmOnDutyLeaveApp.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almOnDutyLeaveApps = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();*/

        $scope.delete = function (id) {
            AlmOnDutyLeaveApp.get({id: id}, function(result) {
                $scope.almOnDutyLeaveApp = result;
                $('#deleteAlmOnDutyLeaveAppConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmOnDutyLeaveApp.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmOnDutyLeaveAppConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmOnDutyLeaveAppSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almOnDutyLeaveApps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almOnDutyLeaveApp = {
                applicationDate: null,
                dutyDate: null,
                dutyInTimeHour: null,
                dutyInTimeMin: null,
                dutyOutTimeHour: null,
                dutyOutTimeMin: null,
                endDutyDate: null,
                reason: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almOnDutyLeaveApp-dialog.controller.js*/

angular.module('stepApp').controller('AlmOnDutyLeaveAppDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'AlmOnDutyLeaveApp', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfo', 'AlmDutySide',
        function($scope, $rootScope, $stateParams, $state, entity, AlmOnDutyLeaveApp, User, Principal, DateUtils, HrEmployeeInfo, AlmDutySide) {

            $scope.almOnDutyLeaveApp = entity;
            $scope.hremployeeinfos = [];
            $scope.almdutysides = [];

            HrEmployeeInfo.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.hremployeeinfos.push(dtoInfo);
                    }
                });
            });

            AlmDutySide.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almdutysides.push(dtoInfo);
                    }
                });
            });

            $scope.load = function(id) {
                AlmOnDutyLeaveApp.get({id : id}, function(result) {
                    $scope.almOnDutyLeaveApp = result;
                });
            };

            HrEmployeeInfo.get({id: 'my'}, function (result) {

                $scope.hrEmployeeInfo = result;
                $scope.designationName      = $scope.hrEmployeeInfo.designationInfo.designationName;
                $scope.departmentName       = $scope.hrEmployeeInfo.departmentInfo.departmentName;
                $scope.workAreaName         = $scope.hrEmployeeInfo.workArea.typeName;
            });

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almOnDutyLeaveAppUpdate', result);
                $scope.isSaving = false;
                $state.go("almOnDutyLeaveApp");
            };

            $scope.clear = function(){
                $scope.applicationDate=  null,
                    $scope.dutyDate = null,
                    $scope.dutyInTimeHour = null,
                    $scope.dutyInTimeMin = null,
                    $scope.dutyOutTimeHour = null,
                    $scope.dutyOutTimeMin= null,
                    $scope.endDutyDate= null,
                    $scope.reason= null,
                    $scope.activeStatus= true,
                    $scope.isMoreDay= false
            };

            $scope.save = function () {
                $scope.almOnDutyLeaveApp.applicationLeaveStatus = 'Pending';
                if(!$scope.almOnDutyLeaveApp.isMoreDay){
                    $scope.almOnDutyLeaveApp.endDutyDate = DateUtils.convertLocaleDateToServer(new Date());
                }
                $scope.almOnDutyLeaveApp.updateBy = $scope.loggedInUser.id;
                $scope.almOnDutyLeaveApp.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almOnDutyLeaveApp.id != null) {
                    AlmOnDutyLeaveApp.update($scope.almOnDutyLeaveApp, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almOnDutyLeaveApp.updated');
                } else {
                    $scope.almOnDutyLeaveApp.employeeInfo = $scope.hrEmployeeInfo;
                    $scope.almOnDutyLeaveApp.createBy = $scope.loggedInUser.id;
                    $scope.almOnDutyLeaveApp.applicationDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.almOnDutyLeaveApp.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmOnDutyLeaveApp.save($scope.almOnDutyLeaveApp, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.almOnDutyLeaveApp.created');
                }
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.hourData = {
                hourOptions: [
                    '00',
                    '01',
                    '02',
                    '03',
                    '04',
                    '05',
                    '06',
                    '07',
                    '08',
                    '09',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                    '20',
                    '21',
                    '22',
                    '23'
                ]
            };

            $scope.minData = {
                minOptions: [
                    '00',
                    '01',
                    '02',
                    '03',
                    '04',
                    '05',
                    '06',
                    '07',
                    '08',
                    '09',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                    '20',
                    '21',
                    '22',
                    '23',
                    '24',
                    '25',
                    '26',
                    '27',
                    '28',
                    '29',
                    '30',
                    '31',
                    '32',
                    '33',
                    '34',
                    '35',
                    '36',
                    '37',
                    '38',
                    '39',
                    '40',
                    '41',
                    '42',
                    '43',
                    '44',
                    '45',
                    '46',
                    '47',
                    '48',
                    '49',
                    '50',
                    '51',
                    '52',
                    '53',
                    '54',
                    '55',
                    '56',
                    '57',
                    '58',
                    '59'
                ]
            };

        }]);
/*almOnDutyLeaveApp-detail.controller.js*/

angular.module('stepApp')
    .controller('AlmOnDutyLeaveAppDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmOnDutyLeaveApp',
            function ($scope, $rootScope, $stateParams, entity, AlmOnDutyLeaveApp) {
                $scope.almOnDutyLeaveApp = entity;
                $scope.load = function (id) {
                    AlmOnDutyLeaveApp.get({id: id}, function(result) {
                        $scope.almOnDutyLeaveApp = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almOnDutyLeaveAppUpdate', function(event, result) {
                    $scope.almOnDutyLeaveApp = result;

                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*almOnDutyAppRequest.controller.js*/

angular.module('stepApp')
    .controller('AlmOnDutyAppRequestLController',
        ['$scope', '$state', '$rootScope', '$timeout', 'DataUtils', 'AlmOnDutyLeaveApp',
            'HrEmployeeInfo', 'Principal', 'DateUtils',
            function($scope, $state, $rootScope, $timeout, DataUtils, AlmOnDutyLeaveApp,
                     HrEmployeeInfo, Principal, DateUtils)
            {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;

                $scope.loggedInUser =   {};
                $scope.getLoggedInUser = function ()
                {
                    Principal.identity().then(function (account)
                    {
                        User.get({login: account.login}, function (result)
                        {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function()
                {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];
                    AlmOnDutyLeaveApp.query(function(result)
                    {
                        $scope.requestEntityCounter++;
                        angular.forEach(result,function(dtoInfo)
                        {
                            if(dtoInfo.applicationLeaveStatus == "Pending"){
                                $scope.newRequestList.push(dtoInfo);
                            }
                        });
                    });
                    $scope.newRequestList.sort($scope.sortById);
                    $scope.loadApprovedRejectList();
                };

                $scope.sortById = function(a,b){
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function ()
                {
                    if($scope.approvalObj.isApproved)
                        $scope.approvalObj.applicationLeaveStatus='Approved';
                    else $scope.approvalObj.applicationLeaveStatus='Reject';
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function ()
                {
                    if($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.applicationLeaveStatus = 'Approved';
                    }
                    else {
                        $scope.approvalObj.applicationLeaveStatus='Reject';
                    }
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj){
                    AlmOnDutyLeaveApp.update(requestObj, function(result)
                    {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        $scope.loadAll();
                    });
                };

                $scope.loadApprovedRejectList = function ()
                {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                    AlmOnDutyLeaveApp.query({}, function(result)
                    {
                        angular.forEach(result,function(requestObj)
                        {
                            if(requestObj.applicationLeaveStatus == 'Approved')
                            {
                                $scope.approvedList.push(requestObj);
                            }
                            if(requestObj.applicationLeaveStatus == 'Reject')
                            {
                                $scope.rejectedList.push(requestObj);
                            }
                        });
                    },function(response)
                    {
                        console.log("data from view load failed");
                    });
                };

                $scope.searchText = "";

                $scope.clearSearchText = function (source)
                {
                    if(source=='request')
                    {
                        $timeout(function(){
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };

                $scope.approvalViewDetail = function (dataObj)
                {
                    $scope.approvalObj = dataObj;
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType){
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        employeeId:null,
                        entityName:null,
                        requestFrom:null,
                        requestSummary: null,
                        requestDate:null,
                        approveState: null,
                        logStatus:null,
                        logComments:null,
                        actionType:'',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function(event, data)
                {
                    $scope.loadAll();
                });


                $scope.loadEmployee = function () {
                    HrEmployeeInfo.get({id: 'my'}, function (result) {
                        $scope.hrEmployeeInfo = result;

                    }, function (response) {
                        $scope.hasProfile = false;
                        $scope.noEmployeeFound = true;
                        $scope.isSaving = false;
                    })
                };

                $scope.sort = function(keyname, source){
                    if(source=='request')
                    {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if(source=='approved')
                    {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if(source=='rejected')
                    {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };
                $scope.loadAll();

            }])
;
