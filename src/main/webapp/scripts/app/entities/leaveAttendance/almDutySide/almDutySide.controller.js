'use strict';

angular.module('stepApp')
    .controller('AlmDutySideController',
   ['$scope', 'AlmDutySide', 'AlmDutySideSearch', 'ParseLinks',
    function ($scope, AlmDutySide, AlmDutySideSearch, ParseLinks) {
        $scope.almDutySides = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmDutySide.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almDutySides = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmDutySide.get({id: id}, function(result) {
                $scope.almDutySide = result;
                $('#deleteAlmDutySideConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmDutySide.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmDutySideConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmDutySideSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almDutySides = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almDutySide = {
                sideName: null,
                description: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almDutySide-dialog.controller.js*/

angular.module('stepApp').controller('AlmDutySideDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'AlmDutySide', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, AlmDutySide, User, Principal, DateUtils) {

            $scope.almDutySide = entity;

            $scope.load = function(id) {
                AlmDutySide.get({id : id}, function(result) {
                    $scope.almDutySide = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almDutySideUpdate', result);
                $scope.isSaving = false;
                $state.go("almDutySide");
            };

            $scope.save = function () {
                $scope.almDutySide.updateBy = $scope.loggedInUser.id;
                $scope.almDutySide.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almDutySide.id != null) {
                    AlmDutySide.update($scope.almDutySide, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almDutySide.updated');
                } else {
                    $scope.almDutySide.createBy = $scope.loggedInUser.id;
                    $scope.almDutySide.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmDutySide.save($scope.almDutySide, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.almDutySide.created');
                }
            };

        }]);


/*almDutySide-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmDutySideDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmDutySide',
            function ($scope, $rootScope, $stateParams, entity, AlmDutySide) {
                $scope.almDutySide = entity;
                $scope.load = function (id) {
                    AlmDutySide.get({id: id}, function(result) {
                        $scope.almDutySide = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almDutySideUpdate', function(event, result) {
                    $scope.almDutySide = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
