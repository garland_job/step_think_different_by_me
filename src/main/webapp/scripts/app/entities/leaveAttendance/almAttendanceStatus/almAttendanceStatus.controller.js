'use strict';

angular.module('stepApp')
    .controller('AlmAttendanceStatusController',
    ['$scope', 'AlmAttendanceStatus', 'AlmAttendanceStatusSearch', 'ParseLinks',
    function ($scope, AlmAttendanceStatus, AlmAttendanceStatusSearch, ParseLinks) {
        $scope.almAttendanceStatuss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmAttendanceStatus.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almAttendanceStatuss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmAttendanceStatus.get({id: id}, function(result) {
                $scope.almAttendanceStatus = result;
                $('#deleteAlmAttendanceStatusConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmAttendanceStatus.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmAttendanceStatusConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmAttendanceStatusSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almAttendanceStatuss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almAttendanceStatus = {
                attendanceStatusName: null,
                description: null,
                attendanceCode: null,
                ShortCode: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almAttendanceStatus-dialog.controller.js*/

angular.module('stepApp').controller('AlmAttendanceStatusDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'AlmAttendanceStatus', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope,$stateParams, $state, entity, AlmAttendanceStatus, User, Principal, DateUtils) {

            $scope.almAttendanceStatus = entity;

            $scope.load = function(id) {
                AlmAttendanceStatus.get({id : id}, function(result) {
                    $scope.almAttendanceStatus = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almAttendanceStatusUpdate', result);
                $scope.isSaving = false;
                $state.go("almAttendanceStatus");
            };

            $scope.save = function () {
                $scope.almAttendanceStatus.updateBy = $scope.loggedInUser.id;
                $scope.almAttendanceStatus.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almAttendanceStatus.id != null) {
                    AlmAttendanceStatus.update($scope.almAttendanceStatus, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almAttendanceStatus.updated');
                } else {
                    $scope.almAttendanceStatus.createBy = $scope.loggedInUser.id;
                    $scope.almAttendanceStatus.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmAttendanceStatus.save($scope.almAttendanceStatus, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.almAttendanceStatus.created');
                }
            };

        }]);
/*almAttendanceStatus-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmAttendanceStatusDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmAttendanceStatus',
            function ($scope, $rootScope, $stateParams, entity, AlmAttendanceStatus) {
                $scope.almAttendanceStatus = entity;
                $scope.load = function (id) {
                    AlmAttendanceStatus.get({id: id}, function(result) {
                        $scope.almAttendanceStatus = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almAttendanceStatusUpdate', function(event, result) {
                    $scope.almAttendanceStatus = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
