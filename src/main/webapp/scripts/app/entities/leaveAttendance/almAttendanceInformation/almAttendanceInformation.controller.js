'use strict';

angular.module('stepApp')
    .controller('AlmAttendanceInformationController',
    ['$scope', 'AlmAttendanceInformation', 'AlmAttendanceInformationSearch', 'ParseLinks',
    function ($scope, AlmAttendanceInformation, AlmAttendanceInformationSearch, ParseLinks) {
        $scope.almAttendanceInformations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmAttendanceInformation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almAttendanceInformations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmAttendanceInformation.get({id: id}, function(result) {
                $scope.almAttendanceInformation = result;
                $('#deleteAlmAttendanceInformationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmAttendanceInformation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmAttendanceInformationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmAttendanceInformationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almAttendanceInformations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almAttendanceInformation = {
                officeIn: null,
                officeOut: null,
                punchIn: null,
                punchInNote: null,
                punchOut: null,
                punchOutNote: null,
                processDate: null,
                isProcessed: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almAttendanceInformation-dialog.controller.js*/

angular.module('stepApp').controller('AlmAttendanceInformationDialogController',
    ['$scope', '$stateParams', '$rootScope', '$state', 'entity', 'AlmAttendanceInformation','User','Principal','DateUtils', 'HrEmployeeInfo', 'AlmAttendanceConfiguration', 'AlmShiftSetup', 'AlmAttendanceStatus',
        function($scope, $stateParams, $rootScope, $state, entity, AlmAttendanceInformation, User, Principal, DateUtils, HrEmployeeInfo, AlmAttendanceConfiguration, AlmShiftSetup, AlmAttendanceStatus) {

            $scope.almAttendanceInformation = entity;
            $scope.hremployeeinfos = [];
            $scope.almattendanceconfigurations = [];
            $scope.almshiftsetups = [];
            $scope.almattendancestatuss = [];

            $scope.predicate = 'id';
            $scope.reverse = true;
            $scope.page = 0;
            // $scope.almAttendanceInformation.punchInTime = new Date(1970, 0, 1, 14, 57, 0);

            HrEmployeeInfo.query({page: $scope.page, size: 10000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                // $scope.links = ParseLinks.parse(headers('link'));
                // $scope.totalItems = headers('X-Total-Count');
                $scope.hremployeeinfos = result;
            });

            AlmAttendanceConfiguration.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almattendanceconfigurations.push(dtoInfo);
                    }
                });
            });

            AlmShiftSetup.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almshiftsetups.push(dtoInfo);
                    }
                });
            });

            AlmAttendanceStatus.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.almattendancestatuss.push(dtoInfo);
                    }
                });
            });

            $scope.load = function(id) {
                AlmAttendanceInformation.get({id : id}, function(result) {
                    $scope.almAttendanceInformation = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almAttendanceInformationUpdate', result);
                $scope.isSaving = false;
                $state.go("almAttendanceInformation");
            };

            $scope.save = function () {
                $scope.almAttendanceInformation.updateBy = $scope.loggedInUser.id;
                $scope.almAttendanceInformation.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almAttendanceInformation.id != null) {
                    AlmAttendanceInformation.update($scope.almAttendanceInformation, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almAttendanceInformation.updated');
                } else {
                    $scope.almAttendanceInformation.createBy = $scope.loggedInUser.id;
                    $scope.almAttendanceInformation.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmAttendanceInformation.save($scope.almAttendanceInformation, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almAttendanceInformation.created');
                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);

/*almAttendanceInformation-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmAttendanceInformationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmAttendanceInformation', 'HrEmployeeInfo', 'AlmAttendanceConfiguration', 'AlmShiftSetup', 'AlmAttendanceStatus',
            function ($scope, $rootScope, $stateParams, entity, AlmAttendanceInformation, HrEmployeeInfo, AlmAttendanceConfiguration, AlmShiftSetup, AlmAttendanceStatus) {
                $scope.almAttendanceInformation = entity;
                $scope.load = function (id) {
                    AlmAttendanceInformation.get({id: id}, function(result) {
                        $scope.almAttendanceInformation = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almAttendanceInformationUpdate', function(event, result) {
                    $scope.almAttendanceInformation = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
