'use strict';

angular.module('stepApp')
    .controller('AlmLeaveTypeController',
    ['$scope', 'AlmLeaveType', 'AlmLeaveTypeSearch', 'ParseLinks',
    function ($scope, AlmLeaveType, AlmLeaveTypeSearch, ParseLinks) {
        $scope.almLeaveTypes = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmLeaveType.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almLeaveTypes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmLeaveType.get({id: id}, function(result) {
                $scope.almLeaveType = result;
                $('#deleteAlmLeaveTypeConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmLeaveType.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmLeaveTypeConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmLeaveTypeSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almLeaveTypes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almLeaveType = {
                leaveTypeName: null,
                description: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almLeaveType-dialog.controller.js*/

angular.module('stepApp').controller('AlmLeaveTypeDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'AlmLeaveType', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, AlmLeaveType, User, Principal, DateUtils) {

            $scope.almLeaveType = entity;

            $scope.load = function(id) {
                AlmLeaveType.get({id : id}, function(result) {
                    $scope.almLeaveType = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almLeaveTypeUpdate', result);
                $scope.isSaving = false;
                $state.go("almLeaveType");
            };

            $scope.save = function () {
                $scope.almLeaveType.updateBy = $scope.loggedInUser.id;
                $scope.almLeaveType.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almLeaveType.id != null) {
                    AlmLeaveType.update($scope.almLeaveType, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.almLeaveType.updated');
                } else {
                    $scope.almLeaveType.createBy = $scope.loggedInUser.id;
                    $scope.almLeaveType.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmLeaveType.save($scope.almLeaveType, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.almLeaveType.created');
                }
            };

        }]);
/*almLeaveType-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmLeaveTypeDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmLeaveType',
            function ($scope, $rootScope, $stateParams, entity, AlmLeaveType) {
                $scope.almLeaveType = entity;
                $scope.load = function (id) {
                    AlmLeaveType.get({id: id}, function(result) {
                        $scope.almLeaveType = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almLeaveTypeUpdate', function(event, result) {
                    $scope.almLeaveType = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
