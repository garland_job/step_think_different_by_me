'use strict';

angular.module('stepApp')
    .controller('AlmLeaveGroupController',
    ['$scope', 'AlmLeaveGroup', 'AlmLeaveGroupSearch', 'ParseLinks',
    function ($scope, AlmLeaveGroup, AlmLeaveGroupSearch, ParseLinks) {
        $scope.almLeaveGroups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmLeaveGroup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almLeaveGroups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmLeaveGroup.get({id: id}, function(result) {
                $scope.almLeaveGroup = result;
                $('#deleteAlmLeaveGroupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmLeaveGroup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmLeaveGroupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmLeaveGroupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almLeaveGroups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almLeaveGroup = {
                leaveGroupName: null,
                description: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*almLeaveGroup-dialog.controller.js*/

angular.module('stepApp').controller('AlmLeaveGroupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'AlmLeaveGroup', 'User', 'Principal', 'DateUtils',
        function($scope, $stateParams, $state, entity, AlmLeaveGroup, User, Principal, DateUtils) {

            $scope.almLeaveGroup = entity;
            $scope.load = function(id) {
                AlmLeaveGroup.get({id : id}, function(result) {
                    $scope.almLeaveGroup = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:almLeaveGroupUpdate', result);
                $scope.isSaving = false;
                $state.go("almLeaveGroup");
            };

            $scope.save = function () {
                $scope.almLeaveGroup.updateBy = $scope.loggedInUser.id;
                $scope.almLeaveGroup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.almLeaveGroup.id != null) {
                    AlmLeaveGroup.update($scope.almLeaveGroup, onSaveFinished);
                } else {
                    $scope.almLeaveGroup.createBy = $scope.loggedInUser.id;
                    $scope.almLeaveGroup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    AlmLeaveGroup.save($scope.almLeaveGroup, onSaveFinished);
                }
            };
        }]);
/*almLeaveGroup-detail.controller.js*/
angular.module('stepApp')
    .controller('AlmLeaveGroupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmLeaveGroup',
            function ($scope, $rootScope, $stateParams, entity, AlmLeaveGroup) {
                $scope.almLeaveGroup = entity;
                $scope.load = function (id) {
                    AlmLeaveGroup.get({id: id}, function(result) {
                        $scope.almLeaveGroup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almLeaveGroupUpdate', function(event, result) {
                    $scope.almLeaveGroup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
