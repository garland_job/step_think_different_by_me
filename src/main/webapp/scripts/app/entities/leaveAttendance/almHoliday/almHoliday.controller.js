'use strict';

angular.module('stepApp')
    .controller('AlmHolidayController',
    ['$scope', 'AlmHoliday', 'AlmHolidaySearch', 'ParseLinks',
    function ($scope, AlmHoliday, AlmHolidaySearch, ParseLinks) {
        $scope.almHolidays = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            AlmHoliday.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.almHolidays = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AlmHoliday.get({id: id}, function(result) {
                $scope.almHoliday = result;
                $('#deleteAlmHolidayConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AlmHoliday.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAlmHolidayConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            AlmHolidaySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.almHolidays = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.almHoliday = {
                typeName: null,
                religion: null,
                occasion: null,
                fromDate: null,
                toDate: null,
                activeStatus: null

            };
        };
    }]);
/*almHoliday-dialog.controller.js*/

angular.module('stepApp').controller('AlmHolidayDialogController',
    ['$scope', '$rootScope','$stateParams', 'entity', 'Principal', 'AlmHoliday', '$state', 'User', 'DateUtils',
        function($scope, $rootScope,$stateParams,  entity, Principal, AlmHoliday, $state, User, DateUtils) {

            $scope.almHoliday = entity;
            $scope.load = function(id) {
                AlmHoliday.get({id : id}, function(result) {
                    $scope.almHoliday = result;
                });
            };


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:almHolidayUpdate', result);
                $scope.isSaving = false;
                $state.go('almHoliday');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.isSaving = true;
                        $scope.almHoliday.updateBy = result.id;
                        $scope.almHoliday.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.almHoliday.id != null)
                        {
                            AlmHoliday.update($scope.almHoliday, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.almHoliday.updated');
                        }
                        else
                        {
                            $scope.almHoliday.createBy = result.id;
                            $scope.almHoliday.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            AlmHoliday.save($scope.almHoliday, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.almHoliday.created');
                        }
                    });
                });
            };


            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
        }]);
/*almHoliday-detail.controller.js*/

angular.module('stepApp')
    .controller('AlmHolidayDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'AlmHoliday',
            function ($scope, $rootScope, $stateParams, entity, AlmHoliday) {
                $scope.almHoliday = entity;
                $scope.load = function (id) {
                    AlmHoliday.get({id: id}, function(result) {
                        $scope.almHoliday = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:almHolidayUpdate', function(event, result) {
                    $scope.almHoliday = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
