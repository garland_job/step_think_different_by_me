'use strict';

angular.module('stepApp')
	.controller('AuditLogDeleteController', function($scope, $modalInstance, entity, AuditLog) {

        $scope.auditLog = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            AuditLog.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });