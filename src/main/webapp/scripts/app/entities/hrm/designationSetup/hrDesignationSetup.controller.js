'use strict';

angular.module('stepApp')
    .controller('HrDesignationSetupController',
    ['$rootScope','$scope', '$state', 'HrDesignationSetup', 'HrDesignationSetupSearch', 'ParseLinks',
        function ($rootScope, $scope, $state, HrDesignationSetup, HrDesignationSetupSearch, ParseLinks) {

            $scope.hrDesignationSetups = [];
            $scope.predicate = 'id';
            $scope.reverse = true;
            $scope.stateName = "hrDesignationSetup";
            $scope.page = 0;
            $scope.loadAll = function()
            {
                if($rootScope.currentStateName == $scope.stateName){
                    $scope.page = $rootScope.pageNumber;
                }
                else {
                    $rootScope.pageNumber = $scope.page;
                    $rootScope.currentStateName = $scope.stateName;
                }
                HrDesignationSetup.query({page: $scope.page, size: 500, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.totalItems = headers('X-Total-Count');
                    $scope.hrDesignationSetups = result;
                });
            };
            $scope.loadPage = function(page)
            {
                $rootScope.currentStateName = $scope.stateName;
                $rootScope.pageNumber = page;
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();


            $scope.search = function () {
                HrDesignationSetupSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.hrDesignationSetups = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.hrDesignationSetup = {
                    elocattedPosition: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
        }]);
/*hrDesignationSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrDesignationSetupDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrDesignationSetup', 'Principal', 'User', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmpWorkAreaDtlInfoByStatus', 'HrGradeSetupByStatus', 'HrDesignationSetupUniqueness', 'HrDesignationHeadSetupByType', 'InstituteByCategory', 'Institute', 'InstCategoryByStatusAndType', 'findAllByDesigMultipleType','InstLevelByCategory','HrDesignationSetupValidateByInstOrg',
        function ($scope, $rootScope, $stateParams, $state, entity, HrDesignationSetup, Principal, User, DateUtils, MiscTypeSetupByCategory, HrEmpWorkAreaDtlInfoByStatus, HrGradeSetupByStatus, HrDesignationSetupUniqueness, HrDesignationHeadSetupByType, InstituteByCategory, Institute, InstCategoryByStatusAndType, findAllByDesigMultipleType,InstLevelByCategory,HrDesignationSetupValidateByInstOrg) {

            $scope.hrDesignationSetup = entity;
            $scope.designationHeadList1 = [];
            $scope.temp=[];
            $scope.temp2=[];
            // InstCategoryByStatus
            //$scope.organizationCategoryList = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            //$scope.designationHeadList  = HrDesignationHeadSetupByStatus.get({stat:'true'});
            $scope.designationHeadList = HrDesignationHeadSetupByType.get({type: 'HRM'});
            $scope.designationHeadList1=findAllByDesigMultipleType.query();
            $scope.tempfn = function () {
                $scope.temp.forEach(function (item) {
                    designationHeadList1.push(item);
                })
                angular.forEach($scope.temp2, function (item) {
                    designationHeadList1.push(item);
                });
            }
            $scope.tempfn();

            if($stateParams.id!=null) {
                HrDesignationSetup.get({id: $stateParams.id}, function (result) {
                    $scope.hrDesignationSetup = result;

                    if(result.organizationType=="Institute")
                    {
                        InstLevelByCategory.query({catId: result.instCategory.id}, function (result) {
                            $scope.instLevels = result;

                        });
                        InstCategoryByStatusAndType.query({activeStatus: 1, instType: 'Government'}, function (result) {
                            $scope.instCategories = result;
                        });
                    }
                });

            }

            $scope.load = function (id) {
                HrDesignationSetup.get({id: id}, function (result) {
                    $scope.hrDesignationSetup = result;
                });
            };

            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.orgCategoryList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.gradeInfoList = HrGradeSetupByStatus.get({stat: 'true'});

            $scope.orgInfoList = HrEmpWorkAreaDtlInfoByStatus.get({stat: 'true'});
            $scope.orgInfoListFltr = $scope.orgInfoList;

            $scope.instGenInfoType = function () {
                InstCategoryByStatusAndType.query({activeStatus: 1, instType: 'Government'}, function (result) {
                    $scope.instCategories = result;
                });
            };

            $scope.instituteListAll = Institute.query({size: 500});
            $scope.instituteList = $scope.instituteListAll;

            $scope.loadOrganizationInfoByCategory = function (orgCategory) {
                $scope.orgInfoListFltr = [];
                angular.forEach($scope.orgInfoList, function (orgInfo) {
                    if (orgCategory.id == orgInfo.workArea.id) {
                        $scope.orgInfoListFltr.push(orgInfo);
                    }
                });
            };

            $scope.loadInstituteByCategory = function (categoryInfo) {
                $scope.instituteList = [];

                InstLevelByCategory.query({catId: categoryInfo.id}, function (result) {
                    $scope.instLevels = result;
                });
                angular.forEach($scope.instituteListAll, function (insitute) {
                    if (insitute.instCategory != null) {
                        if (categoryInfo.id == insitute.instCategory.id) {
                            $scope.instituteList.push(insitute);
                        }
                    }
                });
            };

            $scope.designationAlreadyExist = false;
            $scope.checkDesignationUniqByCategory = function () {
                if($scope.hrDesignationSetup.organizationType=="Organization"){
                    HrDesignationSetupValidateByInstOrg.get({orgType:$scope.hrDesignationSetup.organizationType,orgInstId:$scope.hrDesignationSetup.organizationInfo.id,instCatId:0,instLevelId:0,desigId:$scope.hrDesignationSetup.designationInfo.id},function (result) {
                        $scope.designationAlreadyExist = result.isValid==="FALSE";
                    });
                }
                else {
                    HrDesignationSetupValidateByInstOrg.get({orgType:$scope.hrDesignationSetup.organizationType,orgInstId:0,instCatId:$scope.hrDesignationSetup.instCategory.id,instLevelId:$scope.hrDesignationSetup.instLevel.id,desigId:$scope.hrDesignationSetup.designationInfo.id},function (result) {
                        $scope.designationAlreadyExist = result.isValid==="FALSE";
                    });
                }

            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrDesignationSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrDesignationSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrDesignationSetup.updateBy = $scope.loggedInUser.id;
                $scope.hrDesignationSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrDesignationSetup.id != null) {
                    HrDesignationSetup.update($scope.hrDesignationSetup, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrDesignationSetup.updated');
                }
                else {
                    $scope.hrDesignationSetup.createBy = $scope.loggedInUser.id;
                    $scope.hrDesignationSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrDesignationSetup.save($scope.hrDesignationSetup, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrDesignationSetup.created');
                }
            };

            $scope.clear = function () {

            };
        }]);
/*hrDesignationSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrDesignationSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrDesignationSetup', 'HrClassInfo', 'HrGradeSetup',
            function ($scope, $rootScope, $stateParams, entity, HrDesignationSetup, HrClassInfo, HrGradeSetup) {
                $scope.hrDesignationSetup = entity;
                $scope.load = function (id) {
                    HrDesignationSetup.get({id: id}, function(result) {
                        $scope.hrDesignationSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrDesignationSetupUpdate', function(event, result) {
                    $scope.hrDesignationSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrDesignationSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrDesignationSetupDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrDesignationSetup',
            function($scope, $rootScope, $modalInstance, entity, HrDesignationSetup) {

                $scope.hrDesignationSetup = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrDesignationSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrDesignationSetup.deleted');
                        });
                };

            }]);
/*hrDesignationSetup-mpo.controller.js*/

angular.module('stepApp').controller('HrDesignationSetupMpoController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrDesignationSetup','Principal','User','DateUtils','MiscTypeSetupByCategory','HrEmpWorkAreaDtlInfoByStatus','HrGradeSetupByStatus','HrDesignationSetupMpoUniqueness','HrDesignationHeadSetupByStatus','InstituteByCategory','InstCategoryByStatusAndType','Institute','InstLevel','InstLevelByCategory','HrDesignationSetupValidateByInstOrg',
        function($scope, $rootScope, $stateParams, $state, entity, HrDesignationSetup, Principal, User, DateUtils,MiscTypeSetupByCategory,HrEmpWorkAreaDtlInfoByStatus,HrGradeSetupByStatus,HrDesignationSetupMpoUniqueness,HrDesignationHeadSetupByStatus,InstituteByCategory,InstCategoryByStatusAndType,Institute,InstLevel,InstLevelByCategory,HrDesignationSetupValidateByInstOrg) {

            $scope.hrDesignationSetup = entity;

            //$scope.instLevels = InstLevel.query();
            //$scope.designationHeadList  = HrDesignationHeadSetupByType.get({type:'HRM'});


            $scope.load = function(id) {
                HrDesignationSetup.get({id : id}, function(result) {
                    $scope.hrDesignationSetup = result;
                });
            };

            if($stateParams.id>0) {
                HrDesignationSetup.get({id : $stateParams.id}, function(result) {
                    $scope.hrDesignationSetup = result;
                    $scope.checkDesignationUniqByTypeLevelCat(result.instCategory);
                    $scope.designationHeadListFiltr  = HrDesignationHeadSetupByStatus.get({stat:'true'});
                });
            }

            $scope.checkDesignationUniqByTypeLevelCat = function(select)
            {

                InstLevelByCategory.query({catId: $scope.hrDesignationSetup.instCategory.id}, function (result) {
                    $scope.instLevels = result;
                });

            };

            $scope.checkDesignationUniq = function(select)
            {

                HrDesignationSetupValidateByInstOrg.get({orgType:"Institute",orgInstId:0,instCatId:$scope.hrDesignationSetup.instCategory.id,instLevelId:$scope.hrDesignationSetup.instLevel.id,desigId:select.id},function (result) {
                    $scope.designationAlreadyExist = result.isValid==="FALSE";
                });

            };


            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.designationHeadList  = HrDesignationHeadSetupByStatus.get({stat:'true'});
            $scope.designationHeadListFiltr = $scope.designationHeadList;

            $scope.gradeInfoList    = HrGradeSetupByStatus.get({stat:'true'});


            //$scope.instCategoryList     = InstCategory.query();
            //$scope.instCategoryList     = InstCategoryByStatus.get({stat:'true'});

            $scope.filterDesignationByType = function()
            {
                InstCategoryByStatusAndType.query({activeStatus: 1, instType: 'NonGovernment'}, function (result) {
                    $scope.instCategoryList = result;
                });
                $scope.designationHeadListFiltr = [];
                if($scope.hrDesignationSetup.desigType!=null)
                {
                    angular.forEach($scope.designationHeadList,function(desigInfo)
                    {
                        if(desigInfo.desigType == $scope.hrDesignationSetup.desigType){
                            $scope.designationHeadListFiltr.push(desigInfo);
                        }
                    });
                }

            };
            $scope.filterDesignationByType();
            $scope.designationAlreadyExist = false;



            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrDesignationSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrDesignationSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrDesignationSetup.updateBy = $scope.loggedInUser.id;
                $scope.hrDesignationSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrDesignationSetup.id != null)
                {
                    HrDesignationSetup.update($scope.hrDesignationSetup, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrDesignationSetup.updated');
                }
                else
                {
                    $scope.hrDesignationSetup.createBy = $scope.loggedInUser.id;
                    $scope.hrDesignationSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrDesignationSetup.save($scope.hrDesignationSetup, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrDesignationSetup.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
