'use strict';

angular.module('stepApp')
    .controller('HrEmpPromotionInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpPromotionInfo', 'HrEmpPromotionInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEmpPromotionInfo, HrEmpPromotionInfoSearch, ParseLinks) {

                $scope.hrEmpPromotionInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpPromotionInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpPromotionInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpPromotionInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpPromotionInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpPromotionInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpPromotionInfo = {
                        instituteFrom: null,
                        instituteTo: null,
                        departmentFrom: null,
                        departmentTo: null,
                        designationFrom: null,
                        designationTo: null,
                        payscaleFrom: null,
                        payscaleTo: null,
                        joiningDate: null,
                        officeOrderNo: null,
                        orderDate: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEmpPromotionInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpPromotionInfoDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'Institute', 'GovernmentInstitutes', 'HrDesignationSetupByStatus', 'HrDepartmentSetupByStatus', 'HrEmployeeInfo', 'HrEmployeeInfoByEmployeeId', 'HrEmpPromotionInfo', 'HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'InstCategoryByStatus', 'ParseLinks', 'PrlSalaryStructureInfoListByEmp','hrEmployeeInfosDetailsByDesigTypeList','hrAllPayscaleByDesignationId','hrEmpWorkAreaDtlInfosByWorkingAreaId','DepartmentByInstitute','HrEmployeeOfInstitutes','HrDepartmentSetupByOrgInstType','HrDesignationSetupByOrgOrInst','InstCategoryByStatusAndType','InstLevelByCategory','InstituteByTypeAndLevelAndCategory',
        function ($scope, $rootScope, $sce, $stateParams, $state, DataUtils, entity, Institute, GovernmentInstitutes, HrDesignationSetupByStatus, HrDepartmentSetupByStatus, HrEmployeeInfo, HrEmployeeInfoByEmployeeId, HrEmpPromotionInfo, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, User, Principal, DateUtils, InstCategoryByStatus, ParseLinks, PrlSalaryStructureInfoListByEmp, hrEmployeeInfosDetailsByDesigTypeList, hrAllPayscaleByDesignationId, hrEmpWorkAreaDtlInfosByWorkingAreaId, DepartmentByInstitute,HrEmployeeOfInstitutes,HrDepartmentSetupByOrgInstType,HrDesignationSetupByOrgOrInst,InstCategoryByStatusAndType,InstLevelByCategory,InstituteByTypeAndLevelAndCategory) {

            $scope.hrEmpPromotionInfo = entity;

            InstCategoryByStatusAndType.query({activeStatus: 1, instType: 'Government'}, function (result) {
                $scope.instCategories = result;
            });
            $scope.instituteList = [];
            $scope.loadInstituteByCategory = function (categoryInfo) {
                $scope.instituteList = [];
                InstLevelByCategory.query({catId: categoryInfo.id}, function (result) {
                    $scope.instLevels = result;
                });
            };

            $scope.changeInstitute=function(level,category) {

                InstituteByTypeAndLevelAndCategory.query({level:level,category:category,type:"Govt"},function (result) {
                    $scope.institutes=result;
                });


            };

            if($stateParams.id !=null){
                HrEmpPromotionInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmpPromotionInfo = result;
                    console.log("hr promotion ");
                    console.log($scope.hrEmpPromotionInfo);
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                        $scope.instituteByEmp($scope.hrEmpPromotionInfo);
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = $scope.hrEmpPromotionInfo.employeeInfo.workArea;
                        $scope.organizationByEmp($scope.hrEmpPromotionInfo);
                        $scope.loadModelByWorkAreaDetailIds($scope.hrEmpPromotionInfo.employeeInfo.workArea);
                        $scope.loadPayScaleByDesignationId($scope.hrEmpPromotionInfo.employeeInfo.designationInfo);
                    }
                });
            }


            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });

            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'JobCategory', stat: 'true'});
            $scope.load = function (id) {
                HrEmpPromotionInfo.get({id: id}, function (result) {
                    $scope.hrEmpPromotionInfo = result;
                });
            };

            //$scope.instCategoryList   = InstCategoryByStatus.get({stat:'true'});

            $scope.hremployeeinfos = [];
            $scope.selectedHrEmployeeInfos = [];
            $scope.instituteFroms = [];
            $scope.instituteTos = [];
            $scope.institutes = [];
            $scope.organizationFroms = [];
            $scope.organizationTos = [];
            $scope.departmentFroms = [];
            $scope.departmentTos = [];
            $scope.departments = [];
            $scope.designationFroms = [];
            $scope.designationTos = [];
            $scope.designations = [];
            $scope.designationFromName = "";
            $scope.departmentFromName = "";
            $scope.organizationFromName = "";
            $scope.instituteFromName = "";
            $scope.payScaleToLists = "";
            $scope.organizationAreaDetailIds = [];

            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.promotionTypeList = MiscTypeSetupByCategory.get({cat: 'PromotionType', stat: 'true'});
            /*$scope.institutes = GovernmentInstitutes.query();*/
            $scope.departments = HrDepartmentSetupByStatus.get({stat: 'true'});
            $scope.designations = HrDesignationSetupByStatus.get({stat: 'true'});

            $scope.loadPayScaleByDesignationId = function (designationToId) {
                console.log(designationToId);
                hrAllPayscaleByDesignationId.get({id: designationToId.designationInfo.id}, function (result) {
                    $scope.payScaleToLists = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log($scope.hremployeeinfos);
                    console.log("Hr employee info list " +$scope.hremployeeinfos);
                    console.log("Total record: " + result.length);
                });
            };

            /*$scope.loadDepartmentByInstitute = function (instId) {
                DepartmentByInstitute.query({instId: instId.id}, function (result) {
                    $scope.departments ="";
                    $scope.departments = result;
                });
            };*/

            $scope.loadModelByWorkAreaDetailIds = function (workAreaIds) {
                hrEmpWorkAreaDtlInfosByWorkingAreaId.get({id: workAreaIds.id}, function (result) {
                    $scope.organizationAreaDetailIds = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadModelByWorkAreaInstitute = function (workArea) {
                //alert(workArea);
                /*HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                 $scope.hremployeeinfos = result;
                 console.log("Total record: "+result.length);
                 });*/
                 hrEmployeeInfosDetailsByDesigTypeList.get({}, function (result) {
                     $scope.hremployeeinfos = result;
                     console.log("Total record: " + result.length);
                 });
            };

            $scope.loadHrDesigDept = function (employeeDetails) {
                console.log("emp org");
                console.log(employeeDetails);
                $scope.employeeId = employeeDetails.employeeId;
                $scope.empId = employeeDetails.id;
                if ($scope.employeeId != null && $scope.employeeId.length > 8) {
                    HrEmployeeInfoByEmployeeId.get({id: $scope.employeeId}, function (result) {
                        $scope.selectedHrEmployeeInfos = result;
                        console.log("Total selected HrEmployee Infos");
                        console.log($scope.selectedHrEmployeeInfos);
                        if ($scope.selectedHrEmployeeInfos.organizationType == 'Organization') {

                            $scope.organizationFromName = $scope.selectedHrEmployeeInfos.workArea.typeName;
                            $scope.hrEmpPromotionInfo.organizationFromId = $scope.selectedHrEmployeeInfos.workArea;
                            $scope.hrEmpPromotionInfo.instituteFrom = 'Organization';
                            $scope.hrEmpPromotionInfo.instituteTo = 'Organization';

                        } else if ($scope.selectedHrEmployeeInfos.organizationType == 'Institute') {

                            $scope.instituteFromName = $scope.selectedHrEmployeeInfos.institute.name;
                            $scope.hrEmpPromotionInfo.instituteFromId = $scope.selectedHrEmployeeInfos.institute;
                            $scope.hrEmpPromotionInfo.instituteFrom = 'Institute';
                            $scope.hrEmpPromotionInfo.instituteTo = 'Institute';

                        }
                        $scope.designationFromName = $scope.selectedHrEmployeeInfos.designationInfo.designationInfo.designationName;
                        $scope.hrEmpPromotionInfo.designationFromId = $scope.selectedHrEmployeeInfos.designationInfo;
                        $scope.departmentFromName = $scope.selectedHrEmployeeInfos.departmentInfo.departmentInfo.departmentName;
                        $scope.hrEmpPromotionInfo.departmentFromId = $scope.selectedHrEmployeeInfos.departmentInfo;
                        // $scope.hrEmpPromotionInfo.payscaleFrom = $scope.selectedHrEmployeeInfos.hrEmploymentInfo;
                        PrlSalaryStructureInfoListByEmp.get({empid: $scope.empId}, function (result) {

                            $scope.prlSalaryStructureInfo = result;
                            if (result.length < 1) {
                            }
                            else {
                                $scope.prlSalaryStructureInfo = result[0];
                                $scope.hrEmpPromotionInfo.payscaleFrom = $scope.prlSalaryStructureInfo.basicAmount;

                            }

                        }, function (response) {

                        });
                        $scope.hrEmpPromotionInfo.departmentFrom = 'not access';
                        $scope.hrEmpPromotionInfo.departmentTo = 'not access';
                        $scope.hrEmpPromotionInfo.designationFrom = 'not access';
                        $scope.hrEmpPromotionInfo.designationTo = 'not access';

                    });


                }
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpPromotionInfoUpdate', result);
                $state.go('hrEmpPromotionInfo');
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.instituteByEmp = function(hrEmpPromotionInfo){

                $scope.instituteFromName = hrEmpPromotionInfo.employeeInfo.institute.name;
                $scope.hrEmpPromotionInfo.instituteFromId = hrEmpPromotionInfo.employeeInfo.institute;

                $scope.departmentFromName = hrEmpPromotionInfo.employeeInfo.departmentInfo.departmentInfo.departmentName;
                $scope.hrEmpPromotionInfo.departmentFromId = hrEmpPromotionInfo.employeeInfo.departmentInfo;

                $scope.designationFromName = hrEmpPromotionInfo.employeeInfo.designationInfo.designationInfo.designationName;
                $scope.hrEmpPromotionInfo.designationFromId =  hrEmpPromotionInfo.employeeInfo.designationInfo;

                HrEmployeeInfoByEmployeeId.get({id: hrEmpPromotionInfo.employeeInfo.employeeId}, function (result){

                    PrlSalaryStructureInfoListByEmp.get({empid: result.id}, function (empPayscale) {
                        $scope.prlSalaryStructureInfo = empPayscale[0];
                        $scope.hrEmpPromotionInfo.payscaleFrom = $scope.prlSalaryStructureInfo.basicAmount;


                    }, function (response) {

                    });
                });
            };

            $scope.organizationByEmp = function(promotion){

                HrEmployeeInfoByEmployeeId.get({id: promotion.employeeInfo.employeeId}, function (result){
                    $scope.organizationFromName = result.workArea.typeName;
                    $scope.hrEmpPromotionInfo.organizationFromId = result.workArea;

                    $scope.departmentFromName = result.departmentInfo.departmentInfo.departmentName;
                    $scope.hrEmpPromotionInfo.departmentFromId = result.departmentInfo;

                    $scope.designationFromName = result.designationInfo.designationInfo.designationName;
                    $scope.hrEmpPromotionInfo.designationFromId =  result.designationInfo;

                    PrlSalaryStructureInfoListByEmp.get({empid: result.id}, function (empPayscale) {
                            $scope.prlSalaryStructureInfo = empPayscale[0];
                            $scope.hrEmpPromotionInfo.payscaleFrom = $scope.prlSalaryStructureInfo.basicAmount;


                    }, function (response) {

                    });
                });

            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpPromotionInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpPromotionInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.hrEmpPromotionInfo.payscaleTo=$scope.hrEmpPromotionInfo.prlPayscaleBasicInfo.basicAmount;
                $scope.hrEmpPromotionInfo.fromOrganizationType=$scope.orgOrInst;
                $scope.hrEmpPromotionInfo.toOrganizationType=$scope.orgOrInstTo;


                if ($scope.hrEmpPromotionInfo.id != null) {
                    $scope.hrEmpPromotionInfo.logId = 0;
                    $scope.hrEmpPromotionInfo.logStatus = 6;
                    $rootScope.setWarningMessage('stepApp.hrEmpPromotionInfo.updated');
                    HrEmpPromotionInfo.update($scope.hrEmpPromotionInfo, onSaveSuccess, onSaveError);
                }
                else {
                    $scope.hrEmpPromotionInfo.logId = 0;
                    $scope.hrEmpPromotionInfo.logStatus = 6;
                    $scope.hrEmpPromotionInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpPromotionInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    $rootScope.setSuccessMessage('stepApp.hrEmpPromotionInfo.created');
                    HrEmpPromotionInfo.save($scope.hrEmpPromotionInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpPromotionInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpPromotionInfo.goDoc = base64Data;
                            hrEmpPromotionInfo.goDocContentType = $file.type;
                            if (hrEmpPromotionInfo.goDocName == null) {
                                hrEmpPromotionInfo.goDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Promotion Document : " + modelInfo.instituteTo;
                $rootScope.showPreviewModal();
            };

            $scope.changeTheDepartmentTo=function (id) {
                HrDepartmentSetupByOrgInstType.query({orgType:$scope.orgOrInstTo,orgId:$scope.hrEmpPromotionInfo.instituteToId.id},function(results){
                    $scope.departments=results;
                });
                if($scope.orgOrInstTo==="Organization"){
                    HrDesignationSetupByOrgOrInst.query({orgType:$scope.orgOrInstTo,orgInstId:id,instLevelId:0},function (results) {
                        $scope.designations=results;
                    })
                }
                else {
                    HrDesignationSetupByOrgOrInst.query({orgType:$scope.orgOrInstTo,orgInstId:0,instLevelId:$scope.hrEmpPromotionInfo.instLevel.id},function (results) {
                        $scope.designations=results;
                    })
                }
            }



        }]);
/*hrEmpPromotionInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpPromotionInfoDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpPromotionInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpPromotionInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEmpPromotionInfo = entity;
                console.log("hr emp info id");
                console.log($scope.hrEmpPromotionInfo);

                $scope.load = function (id) {
                    HrEmpPromotionInfo.get({id: id}, function (result) {
                        $scope.hrEmpPromotionInfo = result;

                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpPromotionInfoUpdate', function (event, result) {
                    $scope.hrEmpPromotionInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewImage = function (modelInfo) {
                    var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Promotion Document : " + modelInfo.instituteTo;
                    $rootScope.showPreviewModal();
                };
            }]);
/*hrEmpPromotionInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpPromotionInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpPromotionInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpPromotionInfo) {

                $scope.hrEmpPromotionInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpPromotionInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpPromotionInfo.deleted');
                        });
                };

            }]);
/*hrEmpPromotionInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpPromotionInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpPromotionInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils',
        function ($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpPromotionInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils) {

            $scope.hrEmpPromotionInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'JobCategory', stat: 'true'});
            $scope.promotionTypeList = MiscTypeSetupByCategory.get({cat: 'PromotionType', stat: 'true'});

            console.log("promotion profile info");
            console.log($scope.hrEmpPromotionInfo);
            $scope.load = function (id) {
                HrEmpPromotionInfo.get({id: id}, function (result) {
                    $scope.hrEmpPromotionInfo = result;
                    console.log("promotion profile info");
                    console.log($scope.hrEmpPromotionInfo);
                });
            };
            console.log("promotion profile info");
            console.log($scope.hrEmpPromotionInfo);

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadPublicationProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpPromotionInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpPromotionInfoList = result;
                    if ($scope.hrEmpPromotionInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpPromotionInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpPromotionInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpPromotionInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpPromotionInfoList.length > 1) {
                            var indx = $scope.hrEmpPromotionInfoList.indexOf(modelInfo);
                            $scope.hrEmpPromotionInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpPromotionInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        instituteFrom: null,
                        instituteTo: null,
                        departmentFrom: null,
                        departmentTo: null,
                        designationFrom: null,
                        designationTo: null,
                        payscaleFrom: null,
                        payscaleTo: null,
                        joiningDate: null,
                        promotionType: null,
                        officeOrderNo: null,
                        orderDate: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    instituteFrom: null,
                    instituteTo: null,
                    departmentFrom: null,
                    departmentTo: null,
                    designationFrom: null,
                    designationTo: null,
                    payscaleFrom: null,
                    payscaleTo: null,
                    joiningDate: null,
                    promotionType: null,
                    officeOrderNo: null,
                    orderDate: null,
                    goDate: null,
                    goDoc: null,
                    goDocContentType: null,
                    goDocName: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.calendar_local =
            {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpPromotionInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpPromotionInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if (modelInfo.id != null) {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmpPromotionInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }
                else {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmpPromotionInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
            };

            $scope.clear = function () {

            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                //$rootScope.viewerObject.contentUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Promotion Document : " + modelInfo.instituteTo;
                // call the modal
                $rootScope.showPreviewModal();
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpPromotionInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpPromotionInfo.goDoc = base64Data;
                            hrEmpPromotionInfo.goDocContentType = $file.type;
                            if (hrEmpPromotionInfo.goDocName == null) {
                                hrEmpPromotionInfo.goDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
