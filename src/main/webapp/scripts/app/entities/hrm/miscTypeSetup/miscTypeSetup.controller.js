'use strict';

angular.module('stepApp')
    .controller('MiscTypeSetupController',
        ['$rootScope', '$scope', '$state', 'MiscTypeSetup', 'MiscTypeSetupSearch', 'MiscTypeSetupByCategory', 'ParseLinks',
            function ($rootScope, $scope, $state, MiscTypeSetup, MiscTypeSetupSearch, MiscTypeSetupByCategory, ParseLinks) {

                $scope.miscTypeSetups = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 1;
                $scope.categoryStat = 'all';
                $scope.typeCategory = '';

                $scope.loadAll = function () {
                    MiscTypeSetup.query({
                        page: $scope.page - 1,
                        size: 2000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.miscTypeSetups = result;
                    });
                };

                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                $rootScope.$on('miscTypeSetupUpdateByCategory', function (event, data) {
                    console.log("EmitAndOn-ON" + data.typeCategory);
                    //console.log(JSON.stringify(data));
                    $scope.typeCategory = data.typeCategory;
                    $scope.loadMiscTypes();
                });
                //$scope.loadAll();

                $scope.loadMiscTypes = function () {
                    console.log("category: " + $scope.typeCategory + ", stat: " + $scope.categoryStat);

                    if ($scope.typeCategory != null) {
                        if ($scope.categoryStat != 'all') {
                            MiscTypeSetupByCategory.get({
                                cat: $scope.typeCategory,
                                stat: $scope.categoryStat
                            }, function (result) {
                                $scope.miscTypeSetups = result;
                            }, function (response) {
                                if (response.status === 404) {
                                    $scope.loadAll();
                                }
                            });
                        }
                        else {
                            MiscTypeSetupByCategory.get({cat: $scope.typeCategory}, function (result) {
                                $scope.miscTypeSetups = result;
                            }, function (response) {
                                if (response.status === 404) {
                                    $scope.loadAll();
                                }
                            });
                        }
                    }
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.miscTypeSetup = {
                        typeCategory: null,
                        typeName: null,
                        typeTitle: null,
                        typeTitleBn: null,
                        typeDesc: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);

/*miscTypeSetup-dialog.controller.js*/

angular.module('stepApp').controller('MiscTypeSetupDialogController',
    ['$rootScope', '$scope', '$stateParams', '$modalInstance', 'entity', 'MiscTypeSetup', 'DateUtils', 'Principal', 'User', 'MiscTypeSetupByCatNameOrTitle',
        function ($rootScope, $scope, $stateParams, $modalInstance, entity, MiscTypeSetup, DateUtils, Principal, User, MiscTypeSetupByCatNameOrTitle) {

            $scope.miscTypeSetup = entity;
            $scope.load = function (id) {
                MiscTypeSetup.get({id: id}, function (result) {
                    $scope.miscTypeSetup = result;
                });
            };

            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                        //console.log("loggedInUser: "+JSON.stringify($scope.loggedInUser));
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.typeNameAlreadyExist = false;
            $scope.typeTitleAlreadyExist = false;
            $scope.checkCategoryWiseTypeName = function () {
                console.log("NameCheck=> category: " + $scope.miscTypeSetup.typeCategory + ", value: " + $scope.miscTypeSetup.typeName);
                if ($scope.miscTypeSetup.typeName != null && $scope.miscTypeSetup.typeName.length > 0) {
                    $scope.editForm.typeName.$pending = true;
                    MiscTypeSetupByCatNameOrTitle.get({
                        chktype: 'name',
                        cat: $scope.miscTypeSetup.typeCategory,
                        value: $scope.miscTypeSetup.typeName
                    }, function (result) {
                        console.log(JSON.stringify(result));
                        $scope.editForm.typeName.$pending = false;
                        if (result.isValid) {
                            $scope.typeNameAlreadyExist = false;
                            if ($scope.typeTitleAlreadyExist == false)
                                $scope.isSaving = !result.isValid;
                        }
                        else {
                            $scope.typeNameAlreadyExist = true;
                            $scope.isSaving = !result.isValid;
                        }
                    }, function (response) {
                        console.log("data connection failed");
                        $scope.editForm.typeName.$pending = false;
                    });
                }

            };

            $scope.checkCategoryWiseTypeTitle = function () {
                console.log("TitleCheck=> category: " + $scope.miscTypeSetup.typeCategory + ", value: " + $scope.miscTypeSetup.typeTitle)
                if ($scope.miscTypeSetup.typeTitle != null && $scope.miscTypeSetup.typeTitle.length > 0) {
                    $scope.editForm.typeTitle.$pending = true;
                    MiscTypeSetupByCatNameOrTitle.get({
                        chktype: 'title',
                        cat: $scope.miscTypeSetup.typeCategory,
                        value: $scope.miscTypeSetup.typeTitle
                    }, function (result) {
                        console.log(JSON.stringify(result));
                        $scope.editForm.typeTitle.$pending = false;
                        if (result.isValid) {
                            if ($scope.typeNameAlreadyExist == false)
                                $scope.isSaving = !result.isValid;
                            $scope.typeTitleAlreadyExist = false;
                        }
                        else {
                            $scope.typeTitleAlreadyExist = true;
                            $scope.isSaving = !result.isValid;
                        }
                    }, function (response) {
                        console.log("data connection failed");
                        $scope.editForm.typeTitle.$pending = false;
                    });
                }
            };

            var onSaveSuccess = function (result) {
                $modalInstance.dismiss('cancel');
                $scope.isSaving = false;
                console.log("OnSaveSuccess: emit to miscTypeSetupUpdateByCategory");
                $rootScope.$emit('miscTypeSetupUpdateByCategory', result);
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.miscTypeSetup.updateBy = $scope.loggedInUser.id;
                $scope.miscTypeSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.preCondition()) {
                    if ($scope.miscTypeSetup.id != null) {
                        MiscTypeSetup.update($scope.miscTypeSetup, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.miscTypeSetup.updated');
                    }
                    else {
                        $scope.miscTypeSetup.createBy = $scope.loggedInUser.id;
                        $scope.miscTypeSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        MiscTypeSetup.save($scope.miscTypeSetup, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.miscTypeSetup.created');
                    }
                } else {
                    $rootScope.setErrorMessage("Please enter correct information");
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('miscTypeSetup.typeName', $scope.miscTypeSetup.typeName, 'text', 50, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('miscTypeSetup.typeTitle', $scope.miscTypeSetup.typeTitle, 'text', 50, 2, 'atozAndAtoZ', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };


            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*miscTypeSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('MiscTypeSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, MiscTypeSetup) {
                $scope.miscTypeSetup = entity;
                $scope.load = function (id) {
                    MiscTypeSetup.get({id: id}, function (result) {
                        $scope.miscTypeSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:miscTypeSetupUpdate', function (event, result) {
                    $scope.miscTypeSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*miscTypeSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('MiscTypeSetupDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'MiscTypeSetup',
            function ($scope, $rootScope, $modalInstance, entity, MiscTypeSetup) {

                $scope.miscTypeSetup = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    MiscTypeSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.miscTypeSetup.deleted');
                        });
                };

            }]);
