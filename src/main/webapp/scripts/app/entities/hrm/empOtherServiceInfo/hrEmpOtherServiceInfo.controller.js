'use strict';

angular.module('stepApp')
    .controller('HrEmpOtherServiceInfoController',
    ['$rootScope', '$scope', '$state', 'HrEmpOtherServiceInfo', 'HrEmpOtherServiceInfoSearch', 'ParseLinks',
    function ($rootScope, $scope, $state, HrEmpOtherServiceInfo, HrEmpOtherServiceInfoSearch, ParseLinks) {

        $scope.hrEmpOtherServiceInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmpOtherServiceInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEmpOtherServiceInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmpOtherServiceInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpOtherServiceInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpOtherServiceInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpOtherServiceInfo = {
                companyName: null,
                address: null,
                serviceType: null,
                position: null,
                fromDate: null,
                toDate: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEmpOtherServiceInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpOtherServiceInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpOtherServiceInfo', 'HrEmployeeInfoByWorkArea','User','Principal','DateUtils','MiscTypeSetupByCategory','HrEmployeeOfInstitutes','HrEmployeeInfoByEmployeeId',
        function($scope, $rootScope, $stateParams, $state, entity, HrEmpOtherServiceInfo, HrEmployeeInfoByWorkArea, User, Principal, DateUtils,MiscTypeSetupByCategory,HrEmployeeOfInstitutes,HrEmployeeInfoByEmployeeId)
        {

            $scope.hrEmpOtherServiceInfo = entity;
            $scope.currentDate=new Date();

            if($stateParams.id !=null){
                HrEmpOtherServiceInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmpOtherServiceInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.load = function(id) {
                HrEmpOtherServiceInfo.get({id : id}, function(result) {
                    $scope.hrEmpOtherServiceInfo = result;
                });
            };

            $scope.hremployeeinfos  = [];
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loadHrEmployee = function(otherService)
            {
                console.log("load hr emp");
                HrEmployeeInfoByEmployeeId.get({id: otherService.employeeInfo.employeeId}, function (result){
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpOtherServiceInfo.employeeInfo = result;


                });

            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpOtherServiceInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpOtherServiceInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpOtherServiceInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpOtherServiceInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpOtherServiceInfo.id != null)
                {
                    $scope.hrEmpOtherServiceInfo.logId = 0;
                    $scope.hrEmpOtherServiceInfo.logStatus = 6;
                    HrEmpOtherServiceInfo.update($scope.hrEmpOtherServiceInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpOtherServiceInfo.updated');
                }
                else
                {
                    $scope.hrEmpOtherServiceInfo.logId = 0;
                    $scope.hrEmpOtherServiceInfo.logStatus = 6;
                    $scope.hrEmpOtherServiceInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpOtherServiceInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpOtherServiceInfo.save($scope.hrEmpOtherServiceInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpOtherServiceInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrEmpOtherServiceInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpOtherServiceInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpOtherServiceInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpOtherServiceInfo, HrEmployeeInfo) {
                $scope.hrEmpOtherServiceInfo = entity;
                $scope.load = function (id) {
                    HrEmpOtherServiceInfo.get({id: id}, function(result) {
                        $scope.hrEmpOtherServiceInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpOtherServiceInfoUpdate', function(event, result) {
                    $scope.hrEmpOtherServiceInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpOtherServiceInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpOtherServiceInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpOtherServiceInfo',
            function($scope, $rootScope, $modalInstance, entity, HrEmpOtherServiceInfo) {

                $scope.hrEmpOtherServiceInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpOtherServiceInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpOtherServiceInfo.deleted');
                };

            }]);
/*hrEmpOtherServiceInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpOtherServiceInfoProfileController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', 'HrEmpOtherServiceInfo', 'HrEmployeeInfo','User','Principal','DateUtils',
        function($rootScope, $sce, $scope, $stateParams, $state, HrEmpOtherServiceInfo, HrEmployeeInfo, User, Principal, DateUtils)
        {

            $scope.hrEmpOtherServiceInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                HrEmpOtherServiceInfo.get({id : id}, function(result) {
                    $scope.hrEmpOtherServiceInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadOtherServiceProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmpOtherServiceInfo.query({id: 'my'}, function (result)
                {
                    console.log("result:, len: "+result.length);
                    $scope.hrEmpOtherServiceInfoList = result;
                    if($scope.hrEmpOtherServiceInfoList.length < 1)
                    {
                        $scope.addMode = true;
                        $scope.hrEmpOtherServiceInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else
                    {
                        $scope.hrEmployeeInfo = $scope.hrEmpOtherServiceInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpOtherServiceInfoList,function(modelInfo)
                        {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if(modelInfo.logStatus==0)
                            {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                    {
                        if($scope.hrEmpOtherServiceInfoList.length > 1)
                        {
                            var indx = $scope.hrEmpOtherServiceInfoList.indexOf(modelInfo);
                            $scope.hrEmpOtherServiceInfoList.splice(indx, 1);
                        }
                        else
                        {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function()
            {
                $scope.hrEmpOtherServiceInfoList.push(
                    {
                        viewMode:false,
                        viewModeText:'Cancel',
                        companyName: null,
                        address: null,
                        serviceType: null,
                        position: null,
                        fromDate: null,
                        toDate: null,
                        logId:null,
                        logStatus:null,
                        logComments:null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    companyName: null,
                    address: null,
                    serviceType: null,
                    position: null,
                    fromDate: null,
                    toDate: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpOtherServiceInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpOtherServiceInfoList[$scope.selectedIndex].id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index)
            {
                console.log("selectedIndex: "+index);
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 2;
                    HrEmpOtherServiceInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 1;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmpOtherServiceInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
            };

            $scope.clear = function() {

            };
        }]);
