'use strict';

angular.module('stepApp')
    .controller('HrEmpActingDutyInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpActingDutyInfo', 'HrEmpActingDutyInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEmpActingDutyInfo, HrEmpActingDutyInfoSearch, ParseLinks) {

                $scope.hrEmpActingDutyInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpActingDutyInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpActingDutyInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpActingDutyInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpActingDutyInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpActingDutyInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpActingDutyInfo = {
                        toInstitution: null,
                        designation: null,
                        toDepartment: null,
                        fromDate: null,
                        toDate: null,
                        officeOrderNumber: null,
                        officeOrderDate: null,
                        workOnActingDuty: null,
                        comments: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        goNumber: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEmpActingDutyInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpActingDutyInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$sce', '$state', 'DataUtils', 'entity', 'HrEmpActingDutyInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils', 'HrEmployeeOfInstitutes', 'MiscTypeSetupByCategory', 'HrEmployeeInfoByWorkArea', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $sce, $state, DataUtils, entity, HrEmpActingDutyInfo, HrEmployeeInfo, User, Principal, DateUtils, HrEmployeeOfInstitutes, MiscTypeSetupByCategory, HrEmployeeInfoByWorkArea, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpActingDutyInfo = entity;
            if ($stateParams.id != null) {
                HrEmpActingDutyInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpActingDutyInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (actingDuty) {
                console.log("load hr emp");
                HrEmployeeInfoByEmployeeId.get({id: actingDuty.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpActingDutyInfo.employeeInfo = result;
                });

            };
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function (id) {
                HrEmpActingDutyInfo.get({id: id}, function (result) {
                    $scope.hrEmpActingDutyInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpActingDutyInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpActingDutyInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpActingDutyInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpActingDutyInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpActingDutyInfo.id != null) {
                    $scope.hrEmpActingDutyInfo.logId = 0;
                    $scope.hrEmpActingDutyInfo.logStatus = 2;
                    HrEmpActingDutyInfo.update($scope.hrEmpActingDutyInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpActingDutyInfo.updated');
                }
                else {
                    $scope.hrEmpActingDutyInfo.logId = 0;
                    $scope.hrEmpActingDutyInfo.logStatus = 1;
                    $scope.hrEmpActingDutyInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpActingDutyInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpActingDutyInfo.save($scope.hrEmpActingDutyInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpActingDutyInfo.created');
                }
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpActingDutyInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpActingDutyInfo.goDoc = base64Data;
                            hrEmpActingDutyInfo.goDocContentType = $file.type;
                            if (hrEmpActingDutyInfo.goDocName == null) {
                                hrEmpActingDutyInfo.goDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Acting Duty Document - " + modelInfo.toInstitution;
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpActingDutyInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpActingDutyInfoDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpActingDutyInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpActingDutyInfo, HrEmployeeInfo) {
                $scope.hrEmpActingDutyInfo = entity;
                $scope.load = function (id) {
                    HrEmpActingDutyInfo.get({id: id}, function (result) {
                        $scope.hrEmpActingDutyInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpActingDutyInfoUpdate', function (event, result) {
                    $scope.hrEmpActingDutyInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewImage = function (modelInfo) {
                    var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Acting Duty Document - " + modelInfo.toInstitution;
                    $rootScope.showPreviewModal();
                };
            }]);
/*hrEmpActingDutyInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpActingDutyInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpActingDutyInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpActingDutyInfo) {

                $scope.hrEmpActingDutyInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpActingDutyInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpActingDutyInfo.deleted');
                };

            }]);
/*hrEmpActingDutyInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpActingDutyInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpActingDutyInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils',
        function ($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpActingDutyInfo, HrEmployeeInfo, User, Principal, DateUtils) {

            $scope.hrEmpActingDutyInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function (id) {
                HrEmpActingDutyInfo.get({id: id}, function (result) {
                    $scope.hrEmpActingDutyInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadActingDutyProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpActingDutyInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmpActingDutyInfo = result;
                    if ($scope.hrEmpActingDutyInfo == null) {
                        $scope.addMode = true;
                        $scope.hrEmpActingDutyInfo = $scope.initiateModel();
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpActingDutyInfo.employeeInfo;
                        $scope.hrEmpActingDutyInfo.viewMode = true;
                        $scope.hrEmpActingDutyInfo.viewModeText = "Edit";
                        $scope.hrEmpActingDutyInfo.isLocked = false;
                        if ($scope.hrEmpActingDutyInfo.logStatus == 0) {
                            $scope.hrEmpActingDutyInfo.isLocked = true;
                        }
                    }

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.hrEmpActingDutyInfo.viewMode = true;
                    $scope.hrEmpActingDutyInfo.viewModeText = "Add";

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                    $scope.hrEmpActingDutyInfo.viewMode = true;
                    $scope.hrEmpActingDutyInfo.viewModeText = "Add";
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    toInstitution: null,
                    designation: null,
                    toDepartment: null,
                    fromDate: null,
                    toDate: null,
                    officeOrderNumber: null,
                    officeOrderDate: null,
                    workOnActingDuty: null,
                    comments: null,
                    goDate: null,
                    goDoc: null,
                    goDocContentType: null,
                    goDocName: null,
                    goNumber: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpActingDutyInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpActingDutyInfo.id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo) {
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 2;
                        HrEmpActingDutyInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 1;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEmpActingDutyInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpActingDutyInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.toInstitution', hrEmpActingDutyInfo.toInstitution, 'text', 150, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.goNumber', hrEmpActingDutyInfo.goNumber, 'number', 15, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.designation', hrEmpActingDutyInfo.designation, 'text', 150, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.toDepartment', hrEmpActingDutyInfo.toDepartment, 'text', 150, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.fromDate', DateUtils.convertLocaleDateToDMY(hrEmpActingDutyInfo.fromDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.toDate', DateUtils.convertLocaleDateToDMY(hrEmpActingDutyInfo.toDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.officeOrderNumber', hrEmpActingDutyInfo.officeOrderNumber, 'number', 15, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.officeOrderDate', DateUtils.convertLocaleDateToDMY(hrEmpActingDutyInfo.officeOrderDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpActingDutyInfo.goDate', DateUtils.convertLocaleDateToDMY(hrEmpActingDutyInfo.goDate), 'date', 60, 5, 'date', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                //$rootScope.viewerObject.contentUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Acting Duty Document - " + modelInfo.toInstitution;
                // call the modal
                $rootScope.showPreviewModal();
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpActingDutyInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpActingDutyInfo.goDoc = base64Data;
                            hrEmpActingDutyInfo.goDocContentType = $file.type;
                            if (hrEmpActingDutyInfo.goDocName == null) {
                                hrEmpActingDutyInfo.goDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
