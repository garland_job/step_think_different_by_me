'use strict';

angular.module('stepApp')
    .controller('HrEmpAuditObjectionInfoController',
    ['$rootScope','$scope', '$state', 'HrEmpAuditObjectionInfo', 'HrEmpAuditObjectionInfoSearch', 'ParseLinks',
    function ($rootScope,$scope, $state, HrEmpAuditObjectionInfo, HrEmpAuditObjectionInfoSearch, ParseLinks) {

        $scope.hrEmpAuditObjectionInfos = [];

        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmpAuditObjectionInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEmpAuditObjectionInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmpAuditObjectionInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpAuditObjectionInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpAuditObjectionInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpAuditObjectionInfo = {
                organizationName: null,
                auditYear: null,
                paragraphNumber: null,
                objectionHeadliine: null,
                objectionAmount: null,
                officeReplyNumber: null,
                replyDate: null,
                jointMeetingNumber: null,
                jointMeetingDate: null,
                isSettled: null,
                remarks: null,
                logId: null,
                logStatus: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEmpAuditObjectionInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpAuditObjectionInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpAuditObjectionInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory','User','Principal','DateUtils','HrEmployeeInfoByWorkArea','HrEmployeeOfInstitutes',
        function($scope, $rootScope, $stateParams, $state, entity, HrEmpAuditObjectionInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils,HrEmployeeInfoByWorkArea,HrEmployeeOfInstitutes) {

            $scope.hrEmpAuditObjectionInfo = entity;

            $scope.currentDate=new Date();

            if($stateParams.id !=null){
                HrEmpAuditObjectionInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmpAuditObjectionInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat:'ObjectionType',stat:'true'});

            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.load = function(id) {
                HrEmpAuditObjectionInfo.get({id : id}, function(result) {
                    $scope.hrEmpAuditObjectionInfo = result;
                });
            };

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAuditObjectionInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpAuditObjectionInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpAuditObjectionInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpAuditObjectionInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpAuditObjectionInfo.id != null)
                {
                    $scope.hrEmpAuditObjectionInfo.logId = 0;
                    $scope.hrEmpAuditObjectionInfo.logStatus = 0;
                    HrEmpAuditObjectionInfo.update($scope.hrEmpAuditObjectionInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpAuditObjectionInfo.updated');
                }
                else
                {
                    $scope.hrEmpAuditObjectionInfo.logId = 0;
                    $scope.hrEmpAuditObjectionInfo.logStatus = 0;
                    $scope.hrEmpAuditObjectionInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpAuditObjectionInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpAuditObjectionInfo.save($scope.hrEmpAuditObjectionInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpAuditObjectionInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrEmpAuditObjectionInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAuditObjectionInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpAuditObjectionInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, HrEmpAuditObjectionInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEmpAuditObjectionInfo = entity;
                $scope.load = function (id) {
                    HrEmpAuditObjectionInfo.get({id: id}, function(result) {
                        $scope.hrEmpAuditObjectionInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpAuditObjectionInfoUpdate', function(event, result) {
                    $scope.hrEmpAuditObjectionInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpAuditObjectionInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAuditObjectionInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpAuditObjectionInfo',
            function($scope, $rootScope, $modalInstance, entity, HrEmpAuditObjectionInfo) {

                $scope.hrEmpAuditObjectionInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpAuditObjectionInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpAuditObjectionInfo.deleted');
                };

            }]);
