'use strict';

angular.module('stepApp')
    .controller('HrEmpPublicationInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpPublicationInfo', 'HrEmpPublicationInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEmpPublicationInfo, HrEmpPublicationInfoSearch, ParseLinks) {

                $scope.hrEmpPublicationInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpPublicationInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpPublicationInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpPublicationInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpPublicationInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpPublicationInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpPublicationInfo = {
                        publicationTitle: null,
                        publicationDate: null,
                        remarks: null,
                        publicationDoc: null,
                        publicationDocContentType: null,
                        publicationDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEmpPublicationInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpPublicationInfoDialogController',
    ['$rootScope', '$scope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'HrEmpPublicationInfo', 'HrEmployeeInfoByWorkArea', 'User', 'Principal', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($rootScope, $scope, $sce, $stateParams, $state, DataUtils, entity, HrEmpPublicationInfo, HrEmployeeInfoByWorkArea, User, Principal, DateUtils, MiscTypeSetupByCategory, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpPublicationInfo = entity;
            if ($stateParams.id != null) {
                HrEmpPublicationInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpPublicationInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.load = function (id) {
                HrEmpPublicationInfo.get({id: id}, function (result) {
                    $scope.hrEmpPublicationInfo = result;
                });
            };

            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.publicationTypeList = MiscTypeSetupByCategory.get({cat: 'PublicationType', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };
            $scope.loadHrEmployee = function (publication) {
                HrEmployeeInfoByEmployeeId.get({id: publication.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpPublicationInfo.employeeInfo = result;
                });

            }

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpPublicationInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpPublicationInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpPublicationInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpPublicationInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpPublicationInfo.id != null) {
                    $scope.hrEmpPublicationInfo.logId = 0;
                    $scope.hrEmpPublicationInfo.logStatus = 6;
                    HrEmpPublicationInfo.update($scope.hrEmpPublicationInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpPublicationInfo.updated');
                }
                else {
                    $scope.hrEmpPublicationInfo.logId = 0;
                    $scope.hrEmpPublicationInfo.logStatus = 6;
                    $scope.hrEmpPublicationInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpPublicationInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpPublicationInfo.save($scope.hrEmpPublicationInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpPublicationInfo.created');
                }
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setPublicationDoc = function ($file, hrEmpPublicationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpPublicationInfo.publicationDoc = base64Data;
                            hrEmpPublicationInfo.publicationDocContentType = $file.type;
                            if (hrEmpPublicationInfo.publicationDocName == null) {
                                hrEmpPublicationInfo.publicationDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.publicationDoc, modelInfo.publicationDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.publicationDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Publication Document";
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpPublicationInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpPublicationInfoDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpPublicationInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpPublicationInfo, HrEmployeeInfo) {
                $scope.hrEmpPublicationInfo = entity;
                $scope.load = function (id) {
                    HrEmpPublicationInfo.get({id: id}, function (result) {
                        $scope.hrEmpPublicationInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpPublicationInfoUpdate', function (event, result) {
                    $scope.hrEmpPublicationInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewImage = function (modelInfo) {
                    var blob = $rootScope.b64toBlob(modelInfo.publicationDoc, modelInfo.publicationDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.publicationDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Publication Document";
                    $rootScope.showPreviewModal();
                };
            }]);
/*hrEmpPublicationInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpPublicationInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpPublicationInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpPublicationInfo) {

                $scope.hrEmpPublicationInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpPublicationInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpPublicationInfo.deleted');
                        });
                };

            }]);
/*hrEmpPublicationInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpPublicationInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', '$modal', 'DataUtils', 'HrEmpPublicationInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils', 'MiscTypeSetupByCategory',
        function ($rootScope, $sce, $scope, $stateParams, $state, $modal, DataUtils, HrEmpPublicationInfo, HrEmployeeInfo, User, Principal, DateUtils, MiscTypeSetupByCategory) {

            $scope.hrEmpPublicationInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.publicationTypeList = MiscTypeSetupByCategory.get({cat: 'PublicationType', stat: 'true'});
            $scope.load = function (id) {
                HrEmpPublicationInfo.get({id: id}, function (result) {
                    $scope.hrEmpPublicationInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadPublicationProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpPublicationInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpPublicationInfoList = result;
                    if ($scope.hrEmpPublicationInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpPublicationInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpPublicationInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpPublicationInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpPublicationInfoList.length > 1) {
                            var indx = $scope.hrEmpPublicationInfoList.indexOf(modelInfo);
                            $scope.hrEmpPublicationInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpPublicationInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        publicationTitle: null,
                        publicationDate: null,
                        remarks: null,
                        publicationLink: null,
                        publicationDoc: null,
                        publicationDocContentType: null,
                        publicationDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    publicationTitle: null,
                    publicationDate: null,
                    remarks: null,
                    publicationLink: null,
                    publicationDoc: null,
                    publicationDocContentType: null,
                    publicationDocName: null,
                    logId: null,
                    logStatus: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpPublicationInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpPublicationInfoList[$scope.selectedIndex].id = result.id;
                //console.log("result: "+JSON.stringify($scope.hrEmpPublicationInfoList[$scope.selectedIndex]));

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                console.log("selectedIndex: " + index);
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpPublicationInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEmpPublicationInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.publicationDoc, modelInfo.publicationDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                //$rootScope.viewerObject.contentUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = modelInfo.publicationDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Publication Document";
                // call the modal
                $rootScope.showPreviewModal();
            };


            $scope.preCondition = function (hrEmpPublicationInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPublicationInfo.publicationTitle', hrEmpPublicationInfo.publicationTitle, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPublicationInfo.publicationDate', DateUtils.convertLocaleDateToDMY(hrEmpPublicationInfo.publicationDate), 'date', 60, 5, 'date', true, '', ''));
                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setPublicationDoc = function ($file, hrEmpPublicationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpPublicationInfo.publicationDoc = base64Data;
                            hrEmpPublicationInfo.publicationDocContentType = $file.type;
                            if (hrEmpPublicationInfo.publicationDocName == null) {
                                hrEmpPublicationInfo.publicationDocName = $file.name;
                            }
                            var blob = $rootScope.b64toBlob(hrEmpPublicationInfo.publicationDoc, hrEmpPublicationInfo.publicationDocContentType);
                            hrEmpPublicationInfo.publFileContentUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };
        }])
;
