'use strict';

angular.module('stepApp')
    .controller('HrEmpIncrementInfoController',
     ['$rootScope','$scope', '$state', 'HrEmpIncrementInfo', 'HrEmpIncrementInfoSearch', 'ParseLinks',
     function ($rootScope,$scope, $state, HrEmpIncrementInfo, HrEmpIncrementInfoSearch, ParseLinks) {

        $scope.hrEmpIncrementInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmpIncrementInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEmpIncrementInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmpIncrementInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpIncrementInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpIncrementInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpIncrementInfo = {
                incrementAmount: null,
                incrementDate: null,
                basic: null,
                basicDate: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEmpIncrementInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpIncrementInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', '$q', 'entity', 'HrEmpIncrementInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'HrPayScaleSetup','User','Principal','DateUtils','PrlSalaryStructureInfoBasicListByEmp','HrEmployeeOfInstitutes','HrEmployeeInfoByWorkArea','HrEmployeeInfoByEmployeeId',
        function($scope,$rootScope, $stateParams, $state, $q, entity, HrEmpIncrementInfo, HrEmployeeInfo, MiscTypeSetupByCategory, HrPayScaleSetup, User, Principal, DateUtils,PrlSalaryStructureInfoBasicListByEmp,HrEmployeeOfInstitutes,HrEmployeeInfoByWorkArea,HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpIncrementInfo = entity;

            if($stateParams.id !=null){
                HrEmpIncrementInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmpIncrementInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loadHrEmployee = function(empIncrement)
            {
                HrEmployeeInfoByEmployeeId.get({id: empIncrement.employeeInfo.employeeId}, function (result){
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpIncrementInfo.employeeInfo = result;


                });

            }
            $scope.employeeinfos = HrEmployeeInfo.query({filter: 'hrempincrementinfo-is-null'});
            $scope.hrEmpIncrementInfo = HrEmpIncrementInfo.query();
            $q.all([$scope.hrEmpIncrementInfo.$promise, $scope.employeeinfos.$promise]).then(function() {
                if (!$scope.hrEmpIncrementInfo.employeeInfo || !$scope.hrEmpIncrementInfo.employeeInfo.id) {
                    return $q.reject();
                }
                return HrEmployeeInfo.get({id : $scope.hrEmpIncrementInfo.employeeInfo.id}).$promise;
            }).then(function(employeeInfo) {
                $scope.employeeinfos.push(employeeInfo);
            });

            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat:'JobCategory',stat:'true'});
            $scope.hrpayscalesetups = HrPayScaleSetup.query();
            $scope.load = function(id) {
                HrEmpIncrementInfo.get({id : id}, function(result) {
                    $scope.hrEmpIncrementInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpIncrementInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpIncrementInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpIncrementInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpIncrementInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpIncrementInfo.id != null)
                {
                    $scope.hrEmpIncrementInfo.logId = 0;
                    $scope.hrEmpIncrementInfo.logStatus = 0;
                    HrEmpIncrementInfo.update($scope.hrEmpIncrementInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpIncrementInfo.updated');
                }
                else
                {
                    $scope.hrEmpIncrementInfo.logId = 0;
                    $scope.hrEmpIncrementInfo.logStatus = 0;
                    $scope.hrEmpIncrementInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpIncrementInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpIncrementInfo.save($scope.hrEmpIncrementInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpIncrementInfo.created');
                }
            };

            $scope.basicInfoList = [];
            $scope.loadEmployeePayScale = function ()
            {
                console.log("loadEmployeePayScale id: "+$scope.hrEmployeeInfo.id);
                if($scope.hrEmployeeInfo && $scope.hrEmployeeInfo.id)
                {
                    PrlSalaryStructureInfoBasicListByEmp.get({empid: $scope.hrEmployeeInfo.id}, function (result)
                    {
                       console.log("loadEmployeePayScaleBasics:, len: "+JSON.stringify(result));
                       $scope.basicInfoList = result;

                    }, function (response) {
                        console.log("loadEmployeePayScale Error: "+response);
                    })
                }
            };

            $scope.$watch('hrEmpIncrementInfo.employeeInfo', function()
            {
                $scope.counter=$scope.counter+1;
                var salStrucId = 0;
                console.log("ngwatch - "+$scope.counter);
                if($scope.hrEmpIncrementInfo.employeeInfo!=null && $scope.hrEmpIncrementInfo.employeeInfo.id!=null)
                {
                    console.log("ngwatch - "+$scope.counter+", empId: "+$scope.hrEmpIncrementInfo.employeeInfo.fullName);
                    $scope.hrEmployeeInfo = $scope.hrEmpIncrementInfo.employeeInfo;
                    $scope.loadEmployeePayScale();
                }
            });


            $scope.clear = function() {
            };
        }]);
/*hrEmpIncrementInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpIncrementInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpIncrementInfo', 'HrEmployeeInfo', 'MiscTypeSetup', 'HrPayScaleSetup',
            function ($scope, $rootScope, $stateParams, entity, HrEmpIncrementInfo, HrEmployeeInfo, MiscTypeSetup, HrPayScaleSetup) {
                $scope.hrEmpIncrementInfo = entity;
                $scope.load = function (id) {
                    HrEmpIncrementInfo.get({id: id}, function(result) {
                        $scope.hrEmpIncrementInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpIncrementInfoUpdate', function(event, result) {
                    $scope.hrEmpIncrementInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpIncrementInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpIncrementInfoDeleteController',
        ['$scope','$rootScope', '$modalInstance', 'entity', 'HrEmpIncrementInfo',
            function($scope,$rootScope, $modalInstance, entity, HrEmpIncrementInfo) {

                $scope.hrEmpIncrementInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpIncrementInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpIncrementInfo.deleted');
                        });
                };

            }]);
/*hrEmpIncrementInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpIncrementInfoProfileController',
    ['$scope', '$stateParams', '$state', '$q', 'HrEmpIncrementInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'HrPayScaleSetup','User','Principal','DateUtils','PrlSalaryStructureInfoBasicListByEmp',
        function($scope, $stateParams, $state, $q, HrEmpIncrementInfo, HrEmployeeInfo, MiscTypeSetupByCategory, HrPayScaleSetup, User, Principal, DateUtils,PrlSalaryStructureInfoBasicListByEmp) {

            $scope.hrEmpIncrementInfo = {};
            $scope.employeeinfos = HrEmployeeInfo.query({filter: 'hrempincrementinfo-is-null'});
            $q.all([$scope.hrEmpIncrementInfo.$promise, $scope.employeeinfos.$promise]).then(function() {
                if (!$scope.hrEmpIncrementInfo.employeeInfo || !$scope.hrEmpIncrementInfo.employeeInfo.id) {
                    return $q.reject();
                }
                return HrEmployeeInfo.get({id : $scope.hrEmpIncrementInfo.employeeInfo.id}).$promise;
            }).then(function(employeeInfo) {
                $scope.employeeinfos.push(employeeInfo);
            });

            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat:'JobCategory',stat:'true'});
            $scope.hrpayscalesetups = HrPayScaleSetup.query();
            $scope.load = function(id) {
                HrEmpIncrementInfo.get({id : id}, function(result) {
                    $scope.hrEmpIncrementInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadPublicationProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmpIncrementInfo.get({id: 'my'}, function (result)
                {
                    $scope.hrEmpIncrementInfo = result;
                    if($scope.hrEmpIncrementInfo== null)
                    {
                        $scope.addMode = true;
                        $scope.hrEmpIncrementInfo = $scope.initiateModel();
                        $scope.loadEmployee();
                    }
                    else{
                        $scope.hrEmployeeInfo = $scope.hrEmpIncrementInfo.employeeInfo;
                        $scope.hrEmpIncrementInfo.viewMode = true;
                        $scope.hrEmpIncrementInfo.viewModeText = "Edit";
                        $scope.loadEmployeePayScaleBasics();
                    }

                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.hrEmpIncrementInfo.viewMode = true;
                    $scope.hrEmpIncrementInfo.viewModeText = "Add";
                    $scope.loadEmployeePayScaleBasics();

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                    $scope.hrEmpIncrementInfo.viewMode = true;
                    $scope.hrEmpIncrementInfo.viewModeText = "Add";
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.basicInfoList = [];
            $scope.loadEmployeePayScaleBasics = function ()
            {
                console.log("loadEmployeePayScaleBasics INIT id: "+$scope.hrEmployeeInfo.id);
                if($scope.hrEmployeeInfo && $scope.hrEmployeeInfo.id)
                {
                    PrlSalaryStructureInfoBasicListByEmp.get({empid: $scope.hrEmployeeInfo.id}, function (result)
                    {
                       //console.log("loadEmployeePayScaleBasics:, len: "+JSON.stringify(result));
                       console.log("loadEmployeePayScaleBasics:, len: "+result.length);
                       $scope.basicInfoList = result;

                    }, function (response) {
                        console.log("loadEmployeePayScale Error: "+response);
                    })
                }
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    incrementAmount: null,
                    incrementDate: null,
                    basic: null,
                    basicDate: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpIncrementInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpIncrementInfo.id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo)
            {
                $scope.isSaving = true;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmpIncrementInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmpIncrementInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                }
            };

            $scope.clear = function() {
            };
        }]);
