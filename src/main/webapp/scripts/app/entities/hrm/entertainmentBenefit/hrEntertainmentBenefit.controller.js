'use strict';

angular.module('stepApp')
    .controller('HrEntertainmentBenefitController',
     ['$rootScope', '$scope', '$state', 'HrEntertainmentBenefit', 'HrEntertainmentBenefitSearch','ParseLinks',
     function ($rootScope, $scope, $state, HrEntertainmentBenefit, HrEntertainmentBenefitSearch, ParseLinks) {

        $scope.hrEntertainmentBenefits = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEntertainmentBenefit";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEntertainmentBenefit.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEntertainmentBenefits = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEntertainmentBenefitSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEntertainmentBenefits = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEntertainmentBenefit = {
                eligibilityDate: null,
                amount: null,
                totalDays: null,
                memoNumber: null,
                officeOrderDate: null,
                leaveFromDate: null,
                leaveToDate: null,
                notTakenReason: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEntertainmentBenefit-dialog.controller.js*/

angular.module('stepApp').controller('HrEntertainmentBenefitDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEntertainmentBenefit', 'HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory','User','Principal','DateUtils','HrEmployeeOfInstitutes',
        function($scope, $rootScope, $stateParams, $state, entity, HrEntertainmentBenefit, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, User, Principal, DateUtils,HrEmployeeOfInstitutes) {

            $scope.hrEntertainmentBenefit = entity;

            if($stateParams.id !=null){
                HrEntertainmentBenefit.get({id : $stateParams.id},function(result){
                    $scope.hrEntertainmentBenefit = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat:'JobCategory'});
            $scope.load = function(id) {
                HrEntertainmentBenefit.get({id : id}, function(result) {
                    $scope.hrEntertainmentBenefit = result;
                });
            };

            $scope.hremployeeinfos  = [];
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };
            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEntertainmentBenefitUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEntertainmentBenefit');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEntertainmentBenefit.updateBy = $scope.loggedInUser.id;
                $scope.hrEntertainmentBenefit.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEntertainmentBenefit.id != null)
                {
                    $scope.hrEntertainmentBenefit.logId = 0;
                    $scope.hrEntertainmentBenefit.logStatus = 6;
                    HrEntertainmentBenefit.update($scope.hrEntertainmentBenefit, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEntertainmentBenefit.updated');
                }
                else
                {
                    $scope.hrEntertainmentBenefit.logId = 0;
                    $scope.hrEntertainmentBenefit.logStatus = 6;
                    $scope.hrEntertainmentBenefit.createBy = $scope.loggedInUser.id;
                    $scope.hrEntertainmentBenefit.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEntertainmentBenefit.save($scope.hrEntertainmentBenefit, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEntertainmentBenefit.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrEntertainmentBenefit-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEntertainmentBenefitDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEntertainmentBenefit', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, HrEntertainmentBenefit, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEntertainmentBenefit = entity;
                $scope.load = function (id) {
                    HrEntertainmentBenefit.get({id: id}, function(result) {
                        $scope.hrEntertainmentBenefit = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEntertainmentBenefitUpdate', function(event, result) {
                    $scope.hrEntertainmentBenefit = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEntertainmentBenefit-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEntertainmentBenefitDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEntertainmentBenefit',
            function($scope, $rootScope, $modalInstance, entity, HrEntertainmentBenefit) {

                $scope.hrEntertainmentBenefit = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEntertainmentBenefit.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEntertainmentBenefit.deleted');
                };

            }]);
/*hrEntertainmentBenefit-profile.controller.js*/

angular.module('stepApp').controller('HrEntertainmentBenefitProfileController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'HrEntertainmentBenefit', 'HrEmployeeInfo', 'MiscTypeSetupByCategory','User','Principal','DateUtils',
        function($scope, $rootScope, $stateParams, $state, HrEntertainmentBenefit, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils) {

            $scope.hrEntertainmentBenefit = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat:'JobCategory',stat:'true'});
            $scope.load = function(id) {
                HrEntertainmentBenefit.get({id : id}, function(result) {
                    $scope.hrEntertainmentBenefit = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadEntertainmentBenefitProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEntertainmentBenefit.query({id: 'my'}, function (result)
                {
                    console.log("result:, len: "+result.length);
                    $scope.hrEntertainmentBenefitList = result;
                    if($scope.hrEntertainmentBenefitList.length < 1)
                    {
                        $scope.addMode = true;
                        $scope.hrEntertainmentBenefitList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else
                    {
                        $scope.hrEmployeeInfo = $scope.hrEntertainmentBenefitList[0].employeeInfo;
                        angular.forEach($scope.hrEntertainmentBenefitList,function(modelInfo)
                        {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if(modelInfo.logStatus==0)
                            {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                    {
                        if($scope.hrEntertainmentBenefitList.length > 1)
                        {
                            var indx = $scope.hrEntertainmentBenefitList.indexOf(modelInfo);
                            $scope.hrEntertainmentBenefitList.splice(indx, 1);
                        }
                        else
                        {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function()
            {
                $scope.hrEntertainmentBenefitList.push(
                    {
                        viewMode:false,
                        viewModeText:'Cancel',
                        eligibilityDate: null,
                        amount: null,
                        totalDays: null,
                        memoNumber: null,
                        officeOrderDate: null,
                        leaveFromDate: null,
                        leaveToDate: null,
                        notTakenReason: null,
                        logId:null,
                        logStatus:null,
                        logComments:null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    eligibilityDate: null,
                    amount: null,
                    totalDays: null,
                    memoNumber: null,
                    officeOrderDate: null,
                    leaveFromDate: null,
                    leaveToDate: null,
                    notTakenReason: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEntertainmentBenefitUpdate', result);
                $scope.isSaving = false;
                $scope.hrEntertainmentBenefitList[$scope.selectedIndex].id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index)
            {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if($scope.preCondition(modelInfo)){
                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEntertainmentBenefit.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEntertainmentBenefit.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }}else{
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEntertainmentBenefit) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEntertainmentBenefit.amount', hrEntertainmentBenefit.amount, 'number', 15, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEntertainmentBenefit.totalDays', hrEntertainmentBenefit.totalDays, 'number', 15, 2, '0to9', true, '15', '2'));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEntertainmentBenefit.eligibilityDate', DateUtils.convertLocaleDateToDMY(hrEntertainmentBenefit.eligibilityDate), 'date', 60, 5, 'date', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function() {

            };
        }]);
