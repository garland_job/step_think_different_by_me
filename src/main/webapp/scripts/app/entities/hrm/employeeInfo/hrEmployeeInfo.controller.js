'use strict';

angular.module('stepApp')
    .controller('HrEmployeeInfoController',
    ['$rootScope', '$scope', '$state', 'HrEmployeeInfo', 'HrEmployeeInfoSearch', 'ParseLinks','GetAllHrEmployeeInfo',
        function ($rootScope, $scope, $state, HrEmployeeInfo, HrEmployeeInfoSearch, ParseLinks,GetAllHrEmployeeInfo) {

            $scope.hrEmployeeInfos = [];
            $scope.predicate = 'id';
            $scope.reverse = true;
            $scope.page = 0;
            $scope.stateName = "hrEmployeeInfo";
            $scope.loadAll = function()
            {
                if($rootScope.currentStateName == $scope.stateName){
                    $scope.page = $rootScope.pageNumber;
                }
                else {
                    $rootScope.pageNumber = $scope.page;
                    $rootScope.currentStateName = $scope.stateName;
                }
                //console.log("pg: "+$scope.page+", pred:"+$scope.predicate+", rootPage: "+$rootScope.pageNumber+", rootSc: "+$rootScope.currentStateName+", lcst:"+$scope.stateName);

                // HrEmployeeInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                //     $scope.links = ParseLinks.parse(headers('link'));
                //     $scope.totalItems = headers('X-Total-Count');
                //     $scope.hrEmployeeInfos = result;
                // });

                GetAllHrEmployeeInfo.query({},function(result){
                    $scope.hrEmployeeInfos = result;
                });
            };
            $scope.loadPage = function(page)
            {
                $rootScope.currentStateName = $scope.stateName;
                $rootScope.pageNumber = page;
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            $scope.search = function () {
                HrEmployeeInfoSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.hrEmployeeInfos = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.hrEmployeeInfo = {
                    fullName: null,
                    fullNameBn: null,
                    fatherName: null,
                    fatherNameBn: null,
                    motherName: null,
                    motherNameBn: null,
                    birthDate: null,
                    apointmentGoDate: null,
                    presentId: null,
                    nationalId: null,
                    emailAddress: null,
                    mobileNumber: null,
                    gender: null,
                    birthPlace: null,
                    anyDisease: null,
                    officerStuff: null,
                    tinNumber: null,
                    maritalStatus: null,
                    bloodGroup: null,
                    nationality: null,
                    quota: null,
                    birthCertificateNo: null,
                    religion: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
        }]);
/*hrEmployeeInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmployeeInfoDialogController',
    ['$rootScope','$scope', '$stateParams', '$state', '$q','$timeout', '$sce','entity', 'HrEmployeeInfo', 'HrDepartmentSetupByStatus', 'HrDesignationSetupByType', 'User','Principal','DateUtils','$translate','DataUtils','MiscTypeSetupByCategory','HrGradeSetupByStatus','HrEmplTypeInfoByStatus','HrEmpWorkAreaDtlInfoByStatus','InstituteByCategory','InstCategoryByStatus','HrEmployeeInfoDesigLimitCheck','Institute','District','GovernmentInstitutes',
        function($rootScope, $scope, $stateParams, $state, $q, $timeout, $sce, entity, HrEmployeeInfo, HrDepartmentSetupByStatus, HrDesignationSetupByType, User, Principal, DateUtils, $translate, DataUtils, MiscTypeSetupByCategory,HrGradeSetupByStatus,HrEmplTypeInfoByStatus, HrEmpWorkAreaDtlInfoByStatus,InstituteByCategory,InstCategoryByStatus,HrEmployeeInfoDesigLimitCheck,Institute,District,GovernmentInstitutes)
        {
            console.log("Amanur Rahman")
            $scope.success = null;
            $scope.error = null;
            $scope.doNotMatch = null;
            $scope.errorUserExists = null;
            $scope.registerAccount = {};
            $scope.regiUser = {};
            $scope.loggedInUser =   {};
            $scope.currentDate = new Date();

            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                        console.log("loggedInUser: "+JSON.stringify($scope.loggedInUser));
                    });
                });
            };

            $scope.getLoggedInUser();

            $timeout(function (){angular.element('[ng-model="registerAccount.login"]').focus();});

            $scope.hrEmployeeInfo       = entity;
            $scope.hrdepartmentsetups   = HrDepartmentSetupByStatus.get({stat:'true'});
            $scope.hrdepartmentsetupsFltr   = $scope.hrdepartmentsetups;

            $scope.hrdesignationsetups  = HrDesignationSetupByType.get({type:'HRM'});
            $scope.hrDesigSetupListFltr = $scope.hrdesignationsetups;

            $scope.workAreaList         = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.workAreaDtlList      = HrEmpWorkAreaDtlInfoByStatus.get({stat:'true'});
            $scope.workAreaDtlListFiltr = $scope.workAreaDtlList;
            //$scope.employeeBasicTypes   = MiscTypeSetupByCategory.get({cat:'Employee',stat:'true'});
            $scope.encadrementList      = MiscTypeSetupByCategory.get({cat:'Encadrement',stat:'true'});
            $scope.employementTypeList  = HrEmplTypeInfoByStatus.get({stat:'true'});
            $scope.gradeInfoList        = HrGradeSetupByStatus.get({stat:'true'});
            //$scope.instCategoryList     = InstCategory.query();
            $scope.instCategoryList   = InstCategoryByStatus.get({stat:'true'});
            $scope.instituteListAll     = Institute.query({size:500});
            $scope.instituteList        = $scope.instituteListAll;
            $scope.districtList         = District.query({size:500});

            $scope.generateEmployeeId = function ()
            {
                var curDt = new Date();
                //console.log(curDt);
                var uniqCode = ""+curDt.getFullYear().toString()+""+(curDt.getMonth()+1).toString()+""+curDt.getDate().toString()+""+curDt.getHours().toString()+""+curDt.getMinutes()+""+curDt.getSeconds();
                //console.log(uniqCode);
                $scope.hrEmployeeInfo.employeeId = ""+uniqCode;
            };
            //$scope.generateEmployeeId();

            $scope.users = User.query({filter: 'hremployeeinfo-is-null'});
            $q.all([$scope.hrEmployeeInfo.$promise, $scope.users.$promise]).then(function() {
                if (!$scope.hrEmployeeInfo.user || !$scope.hrEmployeeInfo.user.id) {
                    return $q.reject();
                }
                return User.get({id : $scope.hrEmployeeInfo.user.id}).$promise;
            }).then(function(user) {
                $scope.users.push(user);
            });

            $scope.load = function(id) {
                HrEmployeeInfo.get({id : id}, function(result) {
                    $scope.hrEmployeeInfo = result;
                });
            };

            $scope.loadWorkAreaDetailByWork = function(workArea)
            {
                $scope.workAreaDtlListFiltr = [];
                console.log("WorkAreaOrOrg:"+JSON.stringify(workArea)+"");
                angular.forEach($scope.workAreaDtlList,function(workAreaDtl)
                {
                    if(workArea.id == workAreaDtl.workArea.id){
                        $scope.workAreaDtlListFiltr.push(workAreaDtl);
                    }
                });
            };

            $scope.loadInstituteByCategory = function(categoryInfo)
            {
                //console.log(JSON.stringify(categoryInfo)+"");
                /*InstituteByCategory.get({cat : categoryInfo.id}, function(result) {
                 $scope.instituteList = result;
                 });*/
                //console.log("totalInst: "+$scope.instituteListAll.length);
                $scope.instituteList = [];
                angular.forEach($scope.instituteListAll,function(insitute)
                {
                    //console.log("InstRow: "+JSON.stringify(insitute));
                    if(insitute.instCategory != null)
                    {
                        if(categoryInfo.id == insitute.instCategory.id){
                            $scope.instituteList.push(insitute);
                        }
                    }
                });
            };

            $scope.loadDesigDeptByOrganization = function (orgnization)
            {
                $scope.hrDesigSetupListFltr = [];
                console.log("Total Desig: "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    if(desigInfo.organizationType=='Organization')
                    {
                        if(desigInfo.organizationInfo != null)
                        {
                            //console.log("orgId: "+desigInfo.organizationInfo.id + ", Srcorgid; "+orgnization.id);
                            if(orgnization.id === desigInfo.organizationInfo.id)
                            {
                                $scope.hrDesigSetupListFltr.push(desigInfo);
                            }
                        }
                    }
                });

                console.log("Total Section: "+$scope.hrdepartmentsetups.length);
                $scope.hrdepartmentsetupsFltr = [];
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Organization')
                    {
                        if(sectionInfo.organizationInfo != null)
                        {
                            //console.log("sectionId: "+sectionInfo.organizationInfo.id+", SrcsecId"+sectionInfo);
                            if(orgnization.id === sectionInfo.organizationInfo.id)
                            {
                                $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                            }
                        }
                    }
                });
            };

            $scope.loadDesigDeptByInstitute = function (institute)
            {
                console.log("FilterByInst: "+JSON.stringify(institute));
                $scope.hrDesigSetupListFltr = [];
                console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    //console.log("desigId: "+JSON.stringify(desigInfo));
                    //console.log("desigId: "+JSON.stringify(desigInfo.institute));
                    if(desigInfo.organizationType=='Institute')
                    {
                        if(institute.id == desigInfo.institute.id){
                            $scope.hrDesigSetupListFltr.push(desigInfo);
                        }
                    }
                });

                $scope.hrdepartmentsetupsFltr = [];
                console.log("totalSection: "+$scope.hrdepartmentsetups.length);
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Institute')
                    {
                        //console.log("sectionId1: "+JSON.stringify(sectionInfo));
                        //console.log("sectionId2: "+JSON.stringify(sectionInfo.institute));
                        if(institute.id == sectionInfo.institute.id){
                            $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                        }
                    }
                });
            };

            $scope.noOfTotalEmployeeInDesig = 0;
            $scope.noOfEmployeeInDesig = 0;
            $scope.employeeDesigLimitAllowed = true;
            $scope.checkDesignationLimit = function(desigInfo)
            {
                var refId = 0;
                if($scope.hrEmployeeInfo.organizationType=='Organization')
                {
                    refId = $scope.hrEmployeeInfo.workAreaDtl.id;
                }
                else{
                    refId = $scope.hrEmployeeInfo.institute.id;
                }
                if(desigInfo != null)
                {
                    console.log("Type: "+$scope.hrEmployeeInfo.organizationType+", desigId: "+desigInfo.id+", refId: "+refId);
                    HrEmployeeInfoDesigLimitCheck.get({orgtype:$scope.hrEmployeeInfo.organizationType, desigId: desigInfo.id, refid:refId}, function (result)
                    {
                        console.log("DesigResult: "+JSON.stringify(result));
                        $scope.employeeDesigLimitAllowed = result.isValid;
                        $scope.noOfTotalEmployeeInDesig = result.totalEmployee;
                        $scope.noOfEmployeeInDesig = desigInfo.elocattedPosition;
                        $scope.isSaving = !result.isValid;
                        //console.log("Total: "+result.totalEmployee+", DesigVal: "+desigInfo.elocattedPosition);
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.departmentInfo.$pending = false;
                    });

                    $scope.hrEmployeeInfo.gradeInfo = desigInfo.gradeInfo;
                }
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.updatePrlDate = function()
            {
                if($scope.hrEmployeeInfo.birthDate && $scope.hrEmployeeInfo.quota)
                {
                    var prlYear = 60;
                    if($scope.hrEmployeeInfo.quota=='Others' || $scope.hrEmployeeInfo.quota == 'Other' || $scope.hrEmployeeInfo.quota == 'others' || $scope.hrEmployeeInfo.quota == 'other' || $scope.hrEmployeeInfo.quota == 'OTHERS')
                    {
                        prlYear = 59;
                    }
                    var brDt = new Date($scope.hrEmployeeInfo.birthDate);
                    brDt.setDate(brDt.getDate() - 1);

                    var prlDt = new Date(brDt.getFullYear() + prlYear, brDt.getMonth(), brDt.getDate());
                    var retirementDt = new Date(brDt.getFullYear() + prlYear + 1, brDt.getMonth(), brDt.getDate());
                    $scope.hrEmployeeInfo.prlDate = prlDt;
                    $scope.hrEmployeeInfo.retirementDate = retirementDt;
                    $scope.hrEmployeeInfo.lastWorkingDay = retirementDt;
                }else {console.log("BirthDate and Quota is needed for PRL Calculation");}
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmployeeInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmployeeInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmployeeInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmployeeInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrEmployeeInfo.id != null)
                {
                    HrEmployeeInfo.update($scope.hrEmployeeInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmployeeInfo.updated');
                }
            };

            $scope.byteSize = DataUtils.byteSize;

            $scope.previewEmpPhoto = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.empPhoto, modelInfo.empPhotoContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.empPhotoContentType;
                $rootScope.viewerObject.pageTitle = "Employee Photo : "+modelInfo.fullName;

                $rootScope.showPreviewModal();
            };

            $scope.previewQuotaCertDoc = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.quotaCert, modelInfo.quotaCertContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.quotaCertContentType;
                $rootScope.viewerObject.pageTitle = "Quota Certificate : "+modelInfo.fullName;

                $rootScope.showPreviewModal();
            };

            $scope.setEmpPhoto = function ($file, modelInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            modelInfo.empPhoto = base64Data;
                            modelInfo.empPhotoContentType = $file.type;
                            if (modelInfo.imageName == null)
                            {
                                modelInfo.imageName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setQuotaCertDoc = function ($file, modelInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            modelInfo.quotaCert = base64Data;
                            modelInfo.quotaCertContentType = $file.type;
                            if (modelInfo.quotaCertName == null)
                            {
                                modelInfo.quotaCertName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.clear = function()
            {
            };

        }]);
/*hrEmployeeInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmployeeInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmployeeInfo) {
                $scope.hrEmployeeInfo = entity;
                $scope.load = function (id) {
                    HrEmployeeInfo.get({id: id}, function(result) {
                        $scope.hrEmployeeInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmployeeInfoUpdate', function(event, result) {
                    $scope.hrEmployeeInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmployeeInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmployeeInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmployeeInfo',
            function($scope, $rootScope, $modalInstance, entity, HrEmployeeInfo) {

                $scope.hrEmployeeInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmployeeInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmployeeInfo.deleted');
                        });
                };

            }]);
/*hrEmployeeInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmployeeInfoProfileController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', '$q', 'DataUtils', 'HrEmployeeInfo', 'HrDepartmentSetup', 'HrDesignationSetup', 'User','Principal','DateUtils','MiscTypeSetupByCategory',
        function($rootScope, $sce, $scope, $stateParams, $state, $q, DataUtils, HrEmployeeInfo, HrDepartmentSetup, HrDesignationSetup, User, Principal,DateUtils,MiscTypeSetupByCategory) {

            $scope.hrEmployeeInfo = {};
            $scope.encadrementList = MiscTypeSetupByCategory.get({cat:'Encadrement',stat:'true'});

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };

            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadProfile = function ()
            {
                console.log("loadProfile");
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.hrEmployeeInfo.viewMode = true;
                    $scope.hrEmployeeInfo.viewModeText = "Edit";
                    $scope.hrEmployeeInfo.isLocked = false;
                    if($scope.hrEmployeeInfo.logStatus==0 || $scope.hrEmployeeInfo.logStatus==5)
                    {
                        $scope.hrEmployeeInfo.isLocked = true;
                    }

                }, function (response) {
                    $scope.hasProfile = false;
                    $scope.clearForAdd();
                    $scope.noEmployeeFound = true;
                    $scope.addMode = true;
                    $scope.hrEmployeeInfo.viewMode = true;
                    $scope.hrEmployeeInfo.viewModeText = "Add";
                })
            };

            $scope.loadProfile();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode){
                    console.log('*************');
                    console.log(modelInfo.viewMode);
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }else{

                    $scope.loadProfile();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.changeProfileToAdd = function ()
            {
                $scope.viewMode = false;
                $scope.clearForAdd();
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            //$scope.hrdepartmentsetups = HrDepartmentSetup.query({id:'bystat'});
            //$scope.hrdesignationsetups = HrDesignationSetup.query({id:'bystat'});
            //$scope.workAreaList = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.users = User.query({filter: 'hremployeeinfo-is-null'});
            $q.all([$scope.hrEmployeeInfo.$promise, $scope.users.$promise]).then(function()
            {
                if (!$scope.hrEmployeeInfo.user || !$scope.hrEmployeeInfo.user.id) {
                    return $q.reject();
                }
                return User.get({id : $scope.hrEmployeeInfo.user.id}).$promise;
            }).then(function(user) {
                $scope.users.push(user);
            });
            $scope.load = function(id) {
                HrEmployeeInfo.get({id : id}, function(result) {
                    $scope.hrEmployeeInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmployeeInfoUpdate', result);
                $scope.isSaving = false;
                $scope.viewMode = true;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.nationalitySelection = "Bangladesh";

            $scope.updateProfile = function (modelInfo)
            {
                console.log("update employee profile");
                $scope.isSaving = true;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if($scope.nationalitySelection == 'Bangladesh')
                    modelInfo.nationality = $scope.nationalitySelection;

                if (modelInfo.id != null && $scope.preCondition()){
                    modelInfo.logStatus = 0;
                    HrEmployeeInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }else if (modelInfo.id == null && $scope.preCondition()){
                    modelInfo.logStatus = 0;
                    modelInfo.user = $scope.loggedInUser;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmployeeInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
            };
            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.fullName', $scope.hrEmployeeInfo.fullName, 'text', 100, 2, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.mobileNumber', $scope.hrEmployeeInfo.mobileNumber, 'number', 18, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.fatherName', $scope.hrEmployeeInfo.fatherName, 'text', 100, 2, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.motherName', $scope.hrEmployeeInfo.motherName, 'text', 100, 2, 'atozAndAtoZ0-9.-', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.nationalId', $scope.hrEmployeeInfo.nationalId, 'number', 17, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.birthPlace', $scope.hrEmployeeInfo.birthPlace, 'text', 100, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.anyDisease', $scope.hrEmployeeInfo.anyDisease, 'text', 100, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.officerStuff', $scope.hrEmployeeInfo.officerStuff, 'text', 100, 4, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.tinNumber', $scope.hrEmployeeInfo.tinNumber, 'number', 15, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmployeeInfo.birthCertificateNo', $scope.hrEmployeeInfo.birthCertificateNo, 'number', 17, 10, '0to9', true, '', ''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function() {
            };

            $scope.clearForAdd = function () {
                $scope.hrEmployeeInfo = {
                    fullName: null,
                    fullNameBn: null,
                    fatherName: null,
                    fatherNameBn: null,
                    motherName: null,
                    motherNameBn: null,
                    birthDate: null,
                    apointmentGoDate: null,
                    lastWorkingDay:null,
                    presentId: null,
                    nationalId: null,
                    emailAddress: null,
                    mobileNumber: null,
                    gender: null,
                    birthPlace: null,
                    anyDisease: null,
                    officerStuff: null,
                    tinNumber: null,
                    maritalStatus: null,
                    bloodGroup: null,
                    nationality: null,
                    quota: null,
                    birthCertificateNo: null,
                    religion: null,
                    empPhoto: null,
                    empPhotoContentType: null,
                    imageName: null,
                    quotaCert: null,
                    quotaCertContentType: null,
                    quotaCertName: null,
                    employeeId:null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    activeAccount:true,
                    organizationType:'Organization',
                    id: null
                };
            };

            $scope.byteSize = DataUtils.byteSize;

            $scope.previewEmpPhoto = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.empPhoto, modelInfo.empPhotoContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.empPhotoContentType;
                $rootScope.viewerObject.pageTitle = "Employee Photo : "+modelInfo.fullName;

                $rootScope.showPreviewModal();
            };

            $scope.previewQuotaCertDoc = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.quotaCert, modelInfo.quotaCertContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.quotaCertContentType;
                $rootScope.viewerObject.pageTitle = "Quota Certificate : "+modelInfo.fullName;

                $rootScope.showPreviewModal();
            };

            $scope.setEmpPhoto = function ($file, modelInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            modelInfo.empPhoto = base64Data;
                            modelInfo.empPhotoContentType = $file.type;
                            if (modelInfo.imageName == null)
                            {
                                modelInfo.imageName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setQuotaCertDoc = function ($file, modelInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            modelInfo.quotaCert = base64Data;
                            modelInfo.quotaCertContentType = $file.type;
                            if (modelInfo.quotaCertName == null)
                            {
                                modelInfo.quotaCertName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
/*hrEmployeeInfo-register.controller.js HrEmployeeInfoRegisterController  */

angular.module('stepApp').controller('HrEmployeeInfoRegisterController',
    ['$rootScope','$scope', '$stateParams', '$state', '$q', 'entity', 'HrEmployeeInfo', 'HrDepartmentSetupByStatus', 'HrDesignationSetupByType', 'User','Principal','DateUtils','$translate','$timeout','Auth','MiscTypeSetupByCategory','HrGradeSetupByStatus','HrEmplTypeInfoByStatus','HrEmpWorkAreaDtlInfoByStatus','InstituteByCategory','InstCategoryByStatus','HrEmployeeInfoDesigLimitCheck','Institute','District',
        function($rootScope, $scope, $stateParams, $state, $q, entity, HrEmployeeInfo, HrDepartmentSetupByStatus, HrDesignationSetupByType, User, Principal, DateUtils, $translate, $timeout, Auth, MiscTypeSetupByCategory,HrGradeSetupByStatus,HrEmplTypeInfoByStatus, HrEmpWorkAreaDtlInfoByStatus,InstituteByCategory,InstCategoryByStatus,HrEmployeeInfoDesigLimitCheck,Institute,District)
        {

            $scope.success = null;
            $scope.error = null;
            $scope.doNotMatch = null;
            $scope.errorUserExists = null;
            $scope.registerAccount = {};
            $scope.regiUser = {};
            $scope.loggedInUser =   {};
            $scope.currentDate = new Date();

            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                        console.log("loggedInUser: "+JSON.stringify($scope.loggedInUser));
                    });
                });
            };

            $scope.getLoggedInUser();

            $timeout(function (){angular.element('[ng-model="registerAccount.login"]').focus();});

            $scope.hrEmployeeInfo       = entity;
            $scope.hrdepartmentsetups   = HrDepartmentSetupByStatus.get({stat:'true'});
            $scope.hrdepartmentsetupsFltr   = $scope.hrdepartmentsetups;

            $scope.hrdesignationsetups  = HrDesignationSetupByType.get({type:'HRM'});
            $scope.hrDesigSetupListFltr = $scope.hrdesignationsetups;

            $scope.workAreaList         = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.workAreaDtlList      = HrEmpWorkAreaDtlInfoByStatus.get({stat:'true'});
            $scope.workAreaDtlListFiltr = $scope.workAreaDtlList;
            //$scope.employeeBasicTypes   = MiscTypeSetupByCategory.get({cat:'Employee',stat:'true'});
            $scope.encadrementList      = MiscTypeSetupByCategory.get({cat:'Encadrement',stat:'true'});
            $scope.employementTypeList  = HrEmplTypeInfoByStatus.get({stat:'true'});
            $scope.gradeInfoList        = HrGradeSetupByStatus.get({stat:'true'});
            $scope.districtList         = District.query({size:500});
            //$scope.instCategoryList     = InstCategory.query();
            //$scope.instCategoryList   = InstCategoryByStatus.get({stat:'true'});
            //$scope.instituteListAll     = Institute.query({size:500});
            //$scope.instituteList        = $scope.instituteListAll;

            $scope.generateEmployeeId = function ()
            {
                var curDt = new Date();
                //console.log(curDt);
                var uniqCode = ""+curDt.getFullYear().toString()+""+(curDt.getMonth()+1).toString()+""+curDt.getDate().toString()+""+curDt.getHours().toString()+""+curDt.getMinutes()+""+curDt.getSeconds();
                //console.log(uniqCode);
                $scope.hrEmployeeInfo.employeeId = ""+uniqCode;
            };
            //$scope.generateEmployeeId();

            $scope.users = User.query({filter: 'hremployeeinfo-is-null'});
            $q.all([$scope.hrEmployeeInfo.$promise, $scope.users.$promise]).then(function() {
                if (!$scope.hrEmployeeInfo.user || !$scope.hrEmployeeInfo.user.id) {
                    return $q.reject();
                }
                return User.get({id : $scope.hrEmployeeInfo.user.id}).$promise;
            }).then(function(user) {
                $scope.users.push(user);
            });

            $scope.load = function(id) {
                HrEmployeeInfo.get({id : id}, function(result) {
                    $scope.hrEmployeeInfo = result;
                });
            };

            $scope.loadWorkAreaDetailByWork = function(workArea)
            {
                $scope.workAreaDtlListFiltr = [];
                console.log("WorkAreaOrOrg:"+JSON.stringify(workArea)+"");
                angular.forEach($scope.workAreaDtlList,function(workAreaDtl)
                {
                    if(workArea.id == workAreaDtl.workArea.id){
                        $scope.workAreaDtlListFiltr.push(workAreaDtl);
                    }
                });
            };

            $scope.loadInstituteByCategory = function(categoryInfo)
            {
                //console.log(JSON.stringify(categoryInfo)+"");
                /*InstituteByCategory.get({cat : categoryInfo.id}, function(result) {
                 $scope.instituteList = result;
                 });*/
                //console.log("totalInst: "+$scope.instituteListAll.length);
                $scope.instituteList = [];
                angular.forEach($scope.instituteListAll,function(insitute)
                {
                    //console.log("InstRow: "+JSON.stringify(insitute));
                    if(insitute.instCategory != null)
                    {
                        if(categoryInfo.id == insitute.instCategory.id){
                            $scope.instituteList.push(insitute);
                        }
                    }
                });
            };

            $scope.loadDesigDeptByOrganization = function (orgnization)
            {
                $scope.hrDesigSetupListFltr = [];
                console.log("Total Desig: "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    if(desigInfo.organizationType=='Organization')
                    {
                        if(desigInfo.organizationInfo != null)
                        {
                            //console.log("orgId: "+desigInfo.organizationInfo.id + ", Srcorgid; "+orgnization.id);
                            if(orgnization.id === desigInfo.organizationInfo.id)
                            {
                                $scope.hrDesigSetupListFltr.push(desigInfo);
                            }
                        }
                    }
                });

                console.log("Total Section: "+$scope.hrdepartmentsetups.length);
                $scope.hrdepartmentsetupsFltr = [];
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Organization')
                    {
                        if(sectionInfo.organizationInfo != null)
                        {
                            //console.log("sectionId: "+sectionInfo.organizationInfo.id+", SrcsecId"+sectionInfo);
                            if(orgnization.id === sectionInfo.organizationInfo.id)
                            {
                                $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                            }
                        }
                    }
                });
            };

            $scope.loadDesigDeptByInstitute = function (institute)
            {
                console.log("FilterByInst: "+JSON.stringify(institute));
                $scope.hrDesigSetupListFltr = [];
                console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    //console.log("desigId: "+JSON.stringify(desigInfo));
                    //console.log("desigId: "+JSON.stringify(desigInfo.institute));
                    if(desigInfo.organizationType=='Institute')
                    {
                        if(institute.id == desigInfo.institute.id){
                            $scope.hrDesigSetupListFltr.push(desigInfo);
                        }
                    }
                });

                $scope.hrdepartmentsetupsFltr = [];
                console.log("totalSection: "+$scope.hrdepartmentsetups.length);
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Institute')
                    {
                        //console.log("sectionId1: "+JSON.stringify(sectionInfo));
                        //console.log("sectionId2: "+JSON.stringify(sectionInfo.institute));
                        if(institute.id == sectionInfo.institute.id){
                            $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                        }
                    }
                });
            };

            $scope.noOfTotalEmployeeInDesig = 0;
            $scope.noOfEmployeeInDesig = 0;
            $scope.employeeDesigLimitAllowed = true;
            $scope.checkDesignationLimit = function(desigInfo)
            {
                var refId = 0;
                if($scope.hrEmployeeInfo.organizationType=='Organization')
                {
                    refId = $scope.hrEmployeeInfo.workAreaDtl.id;
                }
                else{
                    refId = $scope.hrEmployeeInfo.institute.id;
                }
                if(desigInfo != null)
                {
                    console.log("Type: "+$scope.hrEmployeeInfo.organizationType+", desigId: "+desigInfo.id+", refId: "+refId);
                    HrEmployeeInfoDesigLimitCheck.get({orgtype:$scope.hrEmployeeInfo.organizationType, desigId: desigInfo.id, refid:refId}, function (result)
                    {
                        console.log("DesigResult: "+JSON.stringify(result));
                        $scope.employeeDesigLimitAllowed = result.isValid;
                        $scope.noOfTotalEmployeeInDesig = result.totalEmployee;
                        $scope.noOfEmployeeInDesig = desigInfo.elocattedPosition;
                        $scope.isSaving = !result.isValid;
                        //console.log("Total: "+result.totalEmployee+", DesigVal: "+desigInfo.elocattedPosition);
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.departmentInfo.$pending = false;
                    });

                    $scope.hrEmployeeInfo.gradeInfo = desigInfo.gradeInfo;
                }
            };
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmployeeInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmployeeInfo = result;
                $('#employeeRegistrationConfirmModal').modal('show');
                //$state.go('hrEmployeeInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            $scope.save = function ()
            {
                console.log("Register User and Saving Employee ");
                //$('#employeeRegistrationConfirmModal').modal('show');
                $scope.registerEmployeeUser();
                $state.go('hrEmployeeInfo',{reload:true});
                // for testing purpose.
                //$scope.registerAccount.login = 'admin';
                //$scope.saveEmployeeInfo();
            };

            $scope.updatePrlDate = function()
            {
                if($scope.hrEmployeeInfo.birthDate && $scope.hrEmployeeInfo.quota)
                {
                    var prlYear = 60;
                    if($scope.hrEmployeeInfo.quota=='Others' || $scope.hrEmployeeInfo.quota == 'Other' || $scope.hrEmployeeInfo.quota == 'others' || $scope.hrEmployeeInfo.quota == 'other' || $scope.hrEmployeeInfo.quota == 'OTHERS')
                    {
                        prlYear = 59;
                    }
                    var brDt = new Date($scope.hrEmployeeInfo.birthDate);
                    brDt.setDate(brDt.getDate() - 1);

                    var prlDt = new Date(brDt.getFullYear() + prlYear, brDt.getMonth(), brDt.getDate());
                    var retirementDt = new Date(brDt.getFullYear() + prlYear + 1, brDt.getMonth(), brDt.getDate());
                    $scope.hrEmployeeInfo.prlDate = prlDt;
                    $scope.hrEmployeeInfo.retirementDate = retirementDt;
                    $scope.hrEmployeeInfo.lastWorkingDay = retirementDt;
                }else {console.log("BirthDate and Quota is needed for PRL Calculation");}
            };

            $scope.saveEmployeeInfo = function()
            {
                console.log("login: "+$scope.registerAccount.login);
                User.get({login: $scope.registerAccount.login}, function (usrResult)
                {
                    $scope.isSaving = true;
                    $scope.hrEmployeeInfo.emailAddress  = $scope.registerAccount.email;
                    $scope.regiUser = usrResult;
                    $scope.regiUser.password = $scope.registerAccount.password;
                    $scope.hrEmployeeInfo.updateBy = $scope.loggedInUser.id;
                    $scope.hrEmployeeInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                    console.log("registerdUserInner: "+JSON.stringify($scope.regiUser)+"\n, email: "+$scope.hrEmployeeInfo.emailAddress);
                    if ($scope.hrEmployeeInfo.id != null)
                    {
                        $scope.hrEmployeeInfo.logId = 0;
                        $scope.hrEmployeeInfo.logStatus = 1;
                        HrEmployeeInfo.update($scope.hrEmployeeInfo, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.hrEmployeeInfo.updated');
                    }
                    else
                    {
                        $scope.hrEmployeeInfo.logId = 0;
                        $scope.hrEmployeeInfo.logStatus = 1;
                        $scope.hrEmployeeInfo.user = $scope.regiUser;
                        $scope.hrEmployeeInfo.user.password = $scope.registerAccount.password;
                        $scope.hrEmployeeInfo.createBy = $scope.loggedInUser.id;
                        $scope.hrEmployeeInfo.logComments = $scope.registerAccount.password;
                        $scope.hrEmployeeInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEmployeeInfo.save($scope.hrEmployeeInfo, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.hrEmployeeInfo.created');
                    }
                });
            };

            $scope.registerEmployeeUser = function ()
            {
                $scope.registerAccount.langKey = $translate.use();
                $scope.doNotMatch = null;
                $scope.error = null;
                $scope.errorUserExists = null;
                $scope.errorEmailExists = null;

                $scope.hrEmployeeInfo.employeeType = 'DTE_Employee';
                //$scope.registerAccount.authorities = ['ROLE_HRM_USER','ROLE_DTE_EMPLOYEE'];
                $scope.registerAccount.authorities = ['ROLE_DTE_EMPLOYEE'];

                /*
                if($scope.hrEmployeeInfo != null && $scope.hrEmployeeInfo.employeeType=='DTE_Employee')
                {
                    $scope.registerAccount.authorities = ["ROLE_DTE_EMPLOYEE"];
                }
                else
                {
                    $scope.registerAccount.authorities = ["ROLE_INSTEMP"];
                } */

                $scope.registerAccount.activated = true;
                $scope.registerAccount.firstName = $scope.hrEmployeeInfo.fullName;
                $scope.hrEmployeeInfo.emailAddress  = $scope.registerAccount.email;
                console.log("FirstName; "+$scope.registerAccount.firstName+", login: "+$scope.registerAccount.login);

                console.log(JSON.stringify($scope.registerAccount.authorities));

                Auth.createHrmAccount($scope.registerAccount).then(function ()
                {
                    $scope.success = 'OK';
                    console.log("User Login Success...");
                    $scope.saveEmployeeInfo(); // call employee saving
                }).catch(function (response)
                {
                    $scope.success = null;
                    if (response.status === 400 && response.data === 'login already in use') {
                        $scope.errorUserExists = 'ERROR';
                    } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                        $scope.errorEmailExists = 'ERROR';
                    } else {
                        $scope.error = 'ERROR';
                    }
                });

            };

            $scope.clear = function()
            {
                $('#employeeRegistrationConfirmModal').modal('hide');
                $timeout(function() {
                    $state.go('hrEmployeeInfo');
                }, 1000);
            };

            $scope.setDefaultValue = function () {
                $scope.hrEmployeeInfo = {
                    fullName: 'Yousuf Zaman',
                    fullNameBn: 'Yousuf Bn',
                    birthDate: '2016-01-01',
                    apointmentGoDate: '2016-01-01',
                    presentId: 'PresId',
                    nationalId: '9999999999999999',
                    emailAddress: 'redikod@yahoo.com',
                    mobileNumber: '01972419696',
                    logId:0,
                    logStatus:1,
                    logComments:'',
                    activeStatus: false,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
        }]);

/*hrEmployeeInfo-transfer.controller.js*/

angular.module('stepApp').controller('HrEmployeeInfoTransferController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', '$q', 'DataUtils', 'HrEmployeeInfo', 'hrEmployeeInfosByDesigType', 'HrDesignationSetupByType', 'User','Principal','DateUtils','MiscTypeSetupByCategory','HrEmpWorkAreaDtlInfoByStatus','HrEmployeeInfoDesigLimitCheck','HrEmployeeInfoTransferToDte','HrEmployeeInfoTransferToTeacher','HrDepartmentSetupByStatus','InstCategoryByStatus','Institute','InstCategoryByStatusAndType','InstLevelByCategory','GovernmentInstitutes',
        function($rootScope, $sce, $scope, $stateParams, $state, $q, DataUtils, HrEmployeeInfo, hrEmployeeInfosByDesigType, HrDesignationSetupByType, User, Principal,DateUtils,MiscTypeSetupByCategory,HrEmpWorkAreaDtlInfoByStatus,HrEmployeeInfoDesigLimitCheck,HrEmployeeInfoTransferToDte,HrEmployeeInfoTransferToTeacher,HrDepartmentSetupByStatus,InstCategoryByStatus,Institute,InstCategoryByStatusAndType,InstLevelByCategory,GovernmentInstitutes) {

            //$scope.hrEmployeeInfo = {};

            $scope.loggedInUser =   {};

            $scope.hrdesignationsetups  = HrDesignationSetupByType.get({type:'HRM'});
            $scope.hrDesigSetupListFltr = $scope.hrdesignationsetups;
            $scope.employeeInfoList     = [];

            $scope.hrdepartmentsetups   = HrDepartmentSetupByStatus.get({stat:'true'});
            $scope.hrdepartmentsetupsFltr   = $scope.hrdepartmentsetups;

            $scope.employeeInfoTeacherList  = hrEmployeeInfosByDesigType.get({desigType:'Teacher'});
            $scope.employeeInfoStaffList  = hrEmployeeInfosByDesigType.get({desigType:'Staff'});
            $scope.employeeInfoDteList  = hrEmployeeInfosByDesigType.get({desigType:'DTE_Employee'});
            $scope.employeeInfoList     = $scope.employeeInfoTeacherList;
            $scope.transferTypeList = MiscTypeSetupByCategory.get({cat:'TransferType',stat:'true'});
            $scope.workAreaList         = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.workAreaDtlList      = HrEmpWorkAreaDtlInfoByStatus.get({stat:'true'});
            $scope.workAreaDtlListFiltr = $scope.workAreaDtlList;
            $scope.transferType = "teacher2dte";
            $scope.organizationType = "Organization";

            //$scope.instCategoryList     = InstCategory.query();
            // Institute category is actually institute level.

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            InstCategoryByStatusAndType.query({activeStatus: 1, instType: 'Government'}, function (result) {
                $scope.instCategories = result;
                console.log("categories");
                console.log($scope.instCategories);
            });

            //$scope.instCategoryList   = InstCategoryByStatus.get({stat:'true'});
            $scope.instituteListAll     = GovernmentInstitutes.query({size:500});
            $scope.instituteList        = $scope.instituteListAll;

            $scope.loadWorkAreaDetailByWork = function(workArea)
            {
                $scope.workAreaDtlListFiltr = [];
                console.log("WorkAreaOrOrg:"+JSON.stringify(workArea)+"");
                angular.forEach($scope.workAreaDtlList,function(workAreaDtl)
                {
                    if(workArea.id == workAreaDtl.workArea.id){
                        $scope.workAreaDtlListFiltr.push(workAreaDtl);
                    }
                });
            };

            $scope.loadInstituteCategoryByLevel = function (levelInfo) {
                console.log("loadInstituteByLevel: "+$scope.instituteListAll.length);
                InstLevelByCategory.query({catId: levelInfo.id}, function (result) {
                    $scope.instLevels = result;
                    console.log("levels");
                    console.log($scope.instLevels);
                });

            };

             $scope.loadInstituteByCategory = function (categoryInfo)
             {
                console.log("loadInstituteByLevel: "+$scope.instituteListAll.length);
                $scope.instituteList = [];

                angular.forEach($scope.instituteListAll, function (insitute)
                {
                    if (insitute.instLevel != null)
                    {
                        //console.log("== instLevel: "+insitute.instLevel.id +", parentLev: "+categoryInfo.id);
                        if (categoryInfo.id == insitute.instLevel.id)
                        {
                            $scope.instituteList.push(insitute);
                        }
                    }
                });

                $scope.hrDesigSetupListFltr = [];
                console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    console.log("desigId: "+desigInfo.id);
                    if(desigInfo.organizationType=='Institute')
                    {
                        if(desigInfo.instLevel !=null && desigInfo.instLevel.id!= null && categoryInfo.id != null)
                        {
                            if(categoryInfo.id == desigInfo.instLevel.id){
                                //console.log("FoundDesig: DesigId: "+desigInfo.id);
                                $scope.hrDesigSetupListFltr.push(desigInfo);
                            }
                        }
                    }
                });

            };

            $scope.loadDesigDeptByOrganization = function (orgnization)
            {
                $scope.hrDesigSetupListFltr = [];
                console.log("Total Desig: "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    if(desigInfo.organizationType=='Organization')
                    {
                        if(desigInfo.organizationInfo != null)
                        {
                            //console.log("orgId: "+desigInfo.organizationInfo.id + ", Srcorgid; "+orgnization.id);
                            if(orgnization.id === desigInfo.organizationInfo.id)
                            {
                                $scope.hrDesigSetupListFltr.push(desigInfo);
                            }
                        }
                    }
                });


                console.log("Total Section: "+$scope.hrdepartmentsetups.length);
                $scope.hrdepartmentsetupsFltr = [];
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Organization')
                    {
                        if(sectionInfo.organizationInfo != null)
                        {
                            //console.log("sectionId: "+sectionInfo.organizationInfo.id+", SrcsecId"+sectionInfo);
                            if(orgnization.id === sectionInfo.organizationInfo.id)
                            {
                                $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                            }
                        }
                    }
                });
            };

            $scope.loadDesigDeptByInstitute = function (institute)
            {
                console.log("loadDesigDeptByInstitute => ");
                if(institute!=null && institute.id!=null)
                {
                    /*
                    console.log("FilterInstId: "+institute.id+", nmae: "+institute.name);
                    $scope.hrDesigSetupListFltr = [];
                    console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                    angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                    {
                        console.log("desigId: "+desigInfo.id);
                        if(desigInfo.organizationType=='Institute')
                        {
                            if(desigInfo.institute !=null && institute!= null && institute.id != null)
                            {
                                if(institute.id == desigInfo.institute.id){
                                    console.log("FoundDesig: DesigId: "+desigInfo.id);
                                    $scope.hrDesigSetupListFltr.push(desigInfo);
                                }
                            }

                        }
                    });*/

                    $scope.hrdepartmentsetupsFltr = [];
                    console.log("totalSection: "+$scope.hrdepartmentsetups.length);
                    angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                    {
                        if(sectionInfo.organizationType=='Institute')
                        {
                            //console.log("sectionId1: "+sectionInfo.id);
                            if(sectionInfo.institute !=null && institute!= null && institute.id != null)
                            {

                                if(institute.id == sectionInfo.institute.id){
                                    //console.log("FoundSection: SecId: "+sectionInfo.id);
                                    $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                                }
                            }

                        }
                    });
                }

            };

            $scope.noOfTotalEmployeeInDesig = 0;
            $scope.noOfEmployeeInDesig = 0;
            $scope.employeeDesigLimitAllowed = true;
            $scope.checkDesignationLimit = function(desigInfo)
            {
                var refId = 0;
                if(desigInfo != null && $scope.organizationType != null
                    && $scope.organizationType=='Organization')
                {

                    if($scope.hrEmployeeInfo.organizationType=='Organization')
                    {
                        refId = $scope.hrEmployeeInfo.workAreaDtl.id;
                    }
                    else{
                        refId = $scope.hrEmployeeInfo.institute.id;
                    }

                    console.log("Type: "+$scope.hrEmployeeInfo.organizationType+", desigId: "+desigInfo.id+", refId: "+refId);
                    HrEmployeeInfoDesigLimitCheck.get({orgtype:$scope.hrEmployeeInfo.organizationType, desigId: desigInfo.id, refid:refId}, function (result)
                    {
                        console.log("DesigResult: "+JSON.stringify(result));
                        $scope.employeeDesigLimitAllowed = result.isValid;
                        $scope.noOfTotalEmployeeInDesig = result.totalEmployee;
                        $scope.noOfEmployeeInDesig = desigInfo.elocattedPosition;
                        $scope.isSaving = !result.isValid;
                        //console.log("Total: "+result.totalEmployee+", DesigVal: "+desigInfo.elocattedPosition);
                    },function(response)
                    {
                        console.log("data connection failed");
                    });

                    $scope.hrEmployeeInfo.gradeInfo = desigInfo.gradeInfo;
                }
            };

            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };

            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmployeeInfoUpdate', result);
                $scope.isSaving = false;
                $scope.transferOperationCompleted = true;
                $scope.responseMsg = result;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
                $scope.transferOperationCompleted = true;
            };
            $scope.transferOperationCompleted = false;
            $scope.transferEmployee = function (modelInfo)
            {
                console.log("transfer employee SourceType: "+$scope.employeeType+", orgType: "+$scope.organizationType);
                $scope.isSaving = true;
                $scope.transferOperationCompleted = false;
                modelInfo.organizationType  = $scope.organizationType;
                modelInfo.activeStatus = true;
                //console.log(JSON.stringify(modelInfo));

                if (modelInfo.id != null)
                {
                    modelInfo.logStatus = 1;

                    if($scope.organizationType=='Organization')
                    {
                        modelInfo.employeeType      = 'DTE_Employee';
                        HrEmployeeInfoTransferToDte.update(modelInfo, onSaveSuccess, onSaveError);
                    }
                    else
                    {
                        modelInfo.employeeType      = $scope.employeeType;
                        //modelInfo.organizationType  = 'Institute';
                        HrEmployeeInfoTransferToTeacher.update(modelInfo, onSaveSuccess, onSaveError);
                    }
                }
            };

            $scope.changeEmployeeList = function ()
            {
                console.log("Employee type : "+$scope.employeeType);
                //$scope.hrEmployeeInfo = {};
                $("#field_employeeInfo").val("");
                //$("#field_employeeInfo").typeahead('val','');
                if($scope.employeeType=='DTE_Employee')
                {
                    $scope.employeeInfoList = $scope.employeeInfoDteList;
                }
                else if($scope.employeeType=='Teacher')
                {
                    $scope.employeeInfoList = $scope.employeeInfoTeacherList;
                }
                else if($scope.employeeType=='Staff')
                {
                    $scope.employeeInfoList = $scope.employeeInfoStaffList;
                }
            };

            $scope.workArea = {};
            $scope.workAreaDtl = {};
            $scope.instCategory = {};
            $scope.institute = {};
            $scope.designationInfo = {};
            $scope.counter=0;
            $scope.$watch('hrEmployeeInfo', function()
            {
                $scope.counter=$scope.counter+1;
                //console.log("ngwatch - "+$scope.counter);
                if($scope.hrEmployeeInfo!=null && $scope.hrEmployeeInfo.organizationType!=null)
                {
                    //console.log("ngwatch - "+$scope.counter+", empId: "+$scope.hrEmployeeInfo.fullName);
                    $scope.workArea = angular.copy($scope.hrEmployeeInfo.workArea);
                    $scope.workAreaDtl = angular.copy($scope.hrEmployeeInfo.workAreaDtl);
                    $scope.instCategory = angular.copy($scope.hrEmployeeInfo.instCategory);
                    $scope.institute = angular.copy($scope.hrEmployeeInfo.institute);
                    $scope.designationInfo = angular.copy($scope.hrEmployeeInfo.designationInfo);

                    //console.log("ng desigid - "+$scope.designationInfo.id);
                }
            });


            $scope.clear = function() {
            };

        }])
    .controller('HrEmployeeInfoByOrganizationController',
        ['$rootScope', '$scope', '$state', 'HrEmployeeInfo',  'ParseLinks','HrEmployeeInfoByWorkAreaDetails','HrEmpWorkAreaDtlInfoByLogin',
            function ($rootScope, $scope, $state, HrEmployeeInfo, ParseLinks,HrEmployeeInfoByWorkAreaDetails,HrEmpWorkAreaDtlInfoByLogin) {

                $scope.hrEmployeeInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmployeeInfo";
                $scope.loadAll = function()
                {
                    if($rootScope.currentStateName == $scope.stateName){
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpWorkAreaDtlInfoByLogin.query({},function(result){
                        console.log('&&&&&&&&&&&&&77');
                        console.log(result);
                        HrEmployeeInfoByWorkAreaDetails.query({workAreaDtlId:result.id},function(results){
                            $scope.hrEmployeeInfos = results;
                        });
                    });
                };
                //$scope.loadPage = function(page)
                //{
                //    $rootScope.currentStateName = $scope.stateName;
                //    $rootScope.pageNumber = page;
                //    $scope.page = page;
                //    $scope.loadAll();
                //};
                $scope.loadAll();
                //
                //$scope.refresh = function () {
                //    $scope.loadAll();
                //    $scope.clear();
                //};

                //$scope.clear = function () {
                //    $scope.hrEmployeeInfo = {
                //        fullName: null,
                //        fullNameBn: null,
                //        fatherName: null,
                //        fatherNameBn: null,
                //        motherName: null,
                //        motherNameBn: null,
                //        birthDate: null,
                //        apointmentGoDate: null,
                //        presentId: null,
                //        nationalId: null,
                //        emailAddress: null,
                //        mobileNumber: null,
                //        gender: null,
                //        birthPlace: null,
                //        anyDisease: null,
                //        officerStuff: null,
                //        tinNumber: null,
                //        maritalStatus: null,
                //        bloodGroup: null,
                //        nationality: null,
                //        quota: null,
                //        birthCertificateNo: null,
                //        religion: null,
                //        logId:null,
                //        logStatus:null,
                //        logComments:null,
                //        activeStatus: true,
                //        createDate: null,
                //        createBy: null,
                //        updateDate: null,
                //        updateBy: null,
                //        id: null
                //    };
                //};
            }]);
