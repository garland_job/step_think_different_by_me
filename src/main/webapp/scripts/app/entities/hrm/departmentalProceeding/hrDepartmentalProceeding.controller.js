'use strict';

angular.module('stepApp')
    .controller('HrDepartmentalProceedingController',
    ['$rootScope','$scope', '$state','DataUtils', 'HrDepartmentalProceeding', 'HrDepartmentalProceedingSearch', 'ParseLinks',
    function ($rootScope,$scope, $state, DataUtils, HrDepartmentalProceeding, HrDepartmentalProceedingSearch, ParseLinks) {

        $scope.hrDepartmentalProceedings = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrDepartmentalProceeding";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrDepartmentalProceeding.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrDepartmentalProceedings = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrDepartmentalProceedingSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrDepartmentalProceedings = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrDepartmentalProceeding = {
                crimeNature: null,
                nature: null,
                amount: null,
                formDate: null,
                toDate: null,
                period: null,
                dudakCaseDetail: null,
                dudakPunishment: null,
                goDate: null,
                goDoc: null,
                goDocContentType: null,
                goDocName: null,
                remarks: null,
                logId: null,
                logStatus: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;
    }]);
/*hrDepartmentalProceeding-dialog.controller.js*/

angular.module('stepApp').controller('HrDepartmentalProceedingDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'DataUtils', 'entity', 'HrDepartmentalProceeding', 'HrEmployeeInfo','User','Principal','DateUtils','HrEmployeeInfoByWorkArea','MiscTypeSetupByCategory','HrEmployeeOfInstitutes',
        function($scope, $rootScope, $stateParams, $state, DataUtils, entity, HrDepartmentalProceeding, HrEmployeeInfo, User, Principal, DateUtils,HrEmployeeInfoByWorkArea,MiscTypeSetupByCategory,HrEmployeeOfInstitutes) {

            $scope.hrDepartmentalProceeding = entity;
            $scope.currentDate=new Date();

            if($stateParams.id !=null){
                HrDepartmentalProceeding.get({id : $stateParams.id},function(result){
                    $scope.hrDepartmentalProceeding = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                HrDepartmentalProceeding.get({id : id}, function(result) {
                    $scope.hrDepartmentalProceeding = result;
                });
            };

            $scope.punisTypeList    = MiscTypeSetupByCategory.get({cat:'PunishmentType',stat:'true'});
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result)
            {
                $scope.$emit('stepApp:hrDepartmentalProceedingUpdate', result);
                $scope.isSaving = false;
                $state.go('hrDepartmentalProceeding');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrDepartmentalProceeding.updateBy = $scope.loggedInUser.id;
                $scope.hrDepartmentalProceeding.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrDepartmentalProceeding.id != null)
                {
                    $scope.hrDepartmentalProceeding.logId = 0;
                    $scope.hrDepartmentalProceeding.logStatus = 2;
                    HrDepartmentalProceeding.update($scope.hrDepartmentalProceeding, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrDepartmentalProceeding.updated');
                }
                else
                {
                    $scope.hrDepartmentalProceeding.logId = 0;
                    $scope.hrDepartmentalProceeding.logStatus = 1;
                    $scope.hrDepartmentalProceeding.createBy = $scope.loggedInUser.id;
                    $scope.hrDepartmentalProceeding.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrDepartmentalProceeding.save($scope.hrDepartmentalProceeding, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrDepartmentalProceeding.created');
                }
            };

            $scope.clear = function() {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrDepartmentalProceeding)
            {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            hrDepartmentalProceeding.goDoc = base64Data;
                            hrDepartmentalProceeding.goDocContentType = $file.type;
                            if (hrDepartmentalProceeding.goDocName == null)
                            {
                                hrDepartmentalProceeding.goDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
/*hrDepartmentalProceeding-detail.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentalProceedingDetailController',
        ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'HrDepartmentalProceeding', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, DataUtils, entity, HrDepartmentalProceeding, HrEmployeeInfo) {
                $scope.hrDepartmentalProceeding = entity;
                $scope.load = function (id) {
                    HrDepartmentalProceeding.get({id: id}, function(result) {
                        $scope.hrDepartmentalProceeding = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrDepartmentalProceedingUpdate', function(event, result) {
                    $scope.hrDepartmentalProceeding = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrDepartmentalProceeding-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentalProceedingDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrDepartmentalProceeding',
            function($scope, $rootScope, $modalInstance, entity, HrDepartmentalProceeding) {

                $scope.hrDepartmentalProceeding = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrDepartmentalProceeding.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrDepartmentalProceeding.deleted');
                };

            }]);
