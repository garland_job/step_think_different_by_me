'use strict';

angular.module('stepApp')
    .controller('HrEmpTrainingInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpTrainingInfo', 'HrEmpTrainingInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEmpTrainingInfo, HrEmpTrainingInfoSearch, ParseLinks) {

                $scope.hrEmpTrainingInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpTrainingInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpTrainingInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpTrainingInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpTrainingInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpTrainingInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpTrainingInfo = {
                        courseTitle: null,
                        durationFrom: null,
                        durationTo: null,
                        result: null,
                        address: null,
                        officeOrderNo: null,
                        jobCategory: null,
                        goOrderDoc: null,
                        goOrderDocContentType: null,
                        goOrderDocName: null,
                        certDoc: null,
                        certDocContentType: null,
                        certDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEmpTrainingInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpTrainingInfoDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'HrEmpTrainingInfo', 'HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'CountrysByName', 'Country', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $sce, $stateParams, $state, DataUtils, entity, HrEmpTrainingInfo, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, User, Principal, DateUtils, CountrysByName, Country, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpTrainingInfo = entity;

            if ($stateParams.id != null) {
                HrEmpTrainingInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpTrainingInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.trainingTypes = MiscTypeSetupByCategory.get({cat: 'TrainingType', stat: 'true'});
            $scope.trainingInstNames = MiscTypeSetupByCategory.get({cat: 'TrainingInstituteName', stat: 'true'});
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'JobCategory', stat: 'true'});
            $scope.countryList = CountrysByName.query();

            $scope.load = function (id) {
                HrEmpTrainingInfo.get({id: id}, function (result) {
                    $scope.hrEmpTrainingInfo = result;
                });
            };

            $scope.callCountry = function (type) {
                console.log('*************** ' + type);
                if (type == 'Local') {
                    $scope.countryList = [];
                    // $scope.hrEmpTrainingInfo.country = result;
                    Country.get({id: 1}, function (result) {
                        console.log(result);
                        $scope.hrEmpTrainingInfo.country = result;
                        // $scope.countryList.push(result);
                    });
                } else {
                    $scope.countryList = CountrysByName.query();
                }
            }

            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };
            $scope.loadHrEmployee = function (training) {
                HrEmployeeInfoByEmployeeId.get({id: training.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpTrainingInfo.employeeInfo = result;
                });
            }

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTrainingInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpTrainingInfo');
                //console.log("TrainingSave: "+JSON.stringify(result));
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpTrainingInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpTrainingInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.hrEmpTrainingInfo.logId = 0;
                $scope.hrEmpTrainingInfo.logStatus = 6;
                if ($scope.hrEmpTrainingInfo.id != null) {
                    HrEmpTrainingInfo.update($scope.hrEmpTrainingInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpTrainingInfo.updated');
                }
                else {
                    $scope.hrEmpTrainingInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpTrainingInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpTrainingInfo.save($scope.hrEmpTrainingInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpTrainingInfo.created');
                }
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.setGoOrderDoc = function ($file, hrEmpTrainingInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpTrainingInfo.goOrderDoc = base64Data;
                            hrEmpTrainingInfo.goOrderDocContentType = $file.type;
                            if (hrEmpTrainingInfo.goOrderDocName == null) {
                                hrEmpTrainingInfo.goOrderDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setCertDoc = function ($file, hrEmpTrainingInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpTrainingInfo.certDoc = base64Data;
                            hrEmpTrainingInfo.certDocContentType = $file.type;
                            if (hrEmpTrainingInfo.certDocName == null) {
                                hrEmpTrainingInfo.certDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewGODoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goOrderDoc, modelInfo.goOrderDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goOrderDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Training GO Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.certDoc, modelInfo.certDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.certDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Training Certificate Document";
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpTrainingInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpTrainingInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'HrEmpTrainingInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, DataUtils, entity, HrEmpTrainingInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEmpTrainingInfo = entity;
                $scope.load = function (id) {
                    HrEmpTrainingInfo.get({id: id}, function (result) {
                        $scope.hrEmpTrainingInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpTrainingInfoUpdate', function (event, result) {
                    $scope.hrEmpTrainingInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;
                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

            }]);
/*hrEmpTrainingInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpTrainingInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpTrainingInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpTrainingInfo) {

                $scope.hrEmpTrainingInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpTrainingInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpTrainingInfo.deleted');
                        });
                };

            }]);
/*hrEmpTrainingInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpTrainingInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpTrainingInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'CountrysByName',
        function ($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpTrainingInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils, CountrysByName) {

            //$scope.hrEmpTrainingInfo = entity;
            $scope.trainingTypes = MiscTypeSetupByCategory.get({cat: 'TrainingType', stat: 'true'});
            $scope.trainingInstNames = MiscTypeSetupByCategory.get({cat: 'TrainingInstituteName', stat: 'true'});
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'JobCategory', stat: 'true'});
            $scope.countryList = CountrysByName.query();
            $scope.loggedInUser = {};
            $scope.hrEmpTrainingInfoList = [];
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadTrainingProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpTrainingInfo.query({id: 'my'}, function (result) {
                    console.log('%%%%%%%%%%%%%%%%');
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpTrainingInfoList = result;
                    if ($scope.hrEmpTrainingInfoList.length < 1) {
                        console.log('***********');
                        $scope.addMode = true;
                        $scope.hrEmpTrainingInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        console.log('^^^^^^');
                        console.log($scope.hrEmpTrainingInfoList[0]);
                        $scope.hrEmployeeInfo = $scope.hrEmpTrainingInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpTrainingInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpTrainingInfoList.length > 1) {
                            var indx = $scope.hrEmpTrainingInfoList.indexOf(modelInfo);
                            $scope.hrEmpTrainingInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpTrainingInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        instituteName: null,
                        courseTitle: null,
                        durationFrom: null,
                        durationTo: null,
                        result: null,
                        officeOrderNo: null,
                        jobCategory: null,
                        goOrderDoc: null,
                        goOrderDocContentType: null,
                        goOrderDocName: null,
                        certDoc: null,
                        certDocContentType: null,
                        certDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    instituteName: null,
                    courseTitle: null,
                    durationFrom: null,
                    durationTo: null,
                    result: null,
                    officeOrderNo: null,
                    jobCategory: null,
                    goOrderDoc: null,
                    goOrderDocContentType: null,
                    goOrderDocName: null,
                    certDoc: null,
                    certDocContentType: null,
                    certDocName: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTrainingInfoUpdate', result);
                $scope.isSaving = false;
                $scope.viewMode = true;
                $scope.addMode = false;
                $scope.hrEmpTrainingInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpTrainingInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                    else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEmpTrainingInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpTrainingInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.courseTitle', hrEmpTrainingInfo.courseTitle, 'text', 50, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.durationFrom', DateUtils.convertLocaleDateToDMY(hrEmpTrainingInfo.durationFrom), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.durationTo', DateUtils.convertLocaleDateToDMY(hrEmpTrainingInfo.durationTo), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.result', hrEmpTrainingInfo.result, 'text', 5, 1, '0to9.', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.officeOrderNo', hrEmpTrainingInfo.officeOrderNo, 'number', 30, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.certNumber', hrEmpTrainingInfo.certNumber, 'number', 30, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTrainingInfo.certDate', DateUtils.convertLocaleDateToDMY(hrEmpTrainingInfo.certDate), 'date', 60, 5, 'date', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.previewGODoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goOrderDoc, modelInfo.goOrderDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goOrderDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Training GO Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.certDoc, modelInfo.certDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.certDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Training Certificate Document";
                $rootScope.showPreviewModal();
            };

            $scope.setGoOrderDoc = function ($file, hrEmpTrainingInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpTrainingInfo.goOrderDoc = base64Data;
                            hrEmpTrainingInfo.goOrderDocContentType = $file.type;
                            if (hrEmpTrainingInfo.goOrderDocName == null || hrEmpTrainingInfo.goOrderDocName == '') {
                                hrEmpTrainingInfo.goOrderDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setCertDoc = function ($file, hrEmpTrainingInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpTrainingInfo.certDoc = base64Data;
                            hrEmpTrainingInfo.certDocContentType = $file.type;
                            if (hrEmpTrainingInfo.certDocName == null || hrEmpTrainingInfo.certDocName == '') {
                                hrEmpTrainingInfo.certDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
