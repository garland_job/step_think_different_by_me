'use strict';

angular.module('stepApp')
    .controller('HrEmpTransferApplInfoController',
     ['$scope', '$state', 'HrEmpTransferApplInfo', 'HrEmpTransferApplInfoSearch', 'ParseLinks',
     function ($scope, $state, HrEmpTransferApplInfo, HrEmpTransferApplInfoSearch, ParseLinks) {

        $scope.hrEmpTransferApplInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            HrEmpTransferApplInfo.query({page: $scope.page - 1, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmpTransferApplInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpTransferApplInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpTransferApplInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpTransferApplInfo = {
                transferReason: null,
                officeOrderNumber: null,
                officeOrderDate: null,
                activeStatus: true,
                selectedOption: null,
                logId: null,
                logStatus: null,
                logComments: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEmpTransferApplInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpTransferApplInfoDialogController',
    ['$scope','$rootScope', '$state', '$stateParams', 'entity', 'HrEmpTransferApplInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'HrEmpWorkAreaDtlInfo', 'HrDesignationSetup','HrEmployeeInfoByWorkArea','Principal','User','DateUtils','HrEmployeeOfInstitutes','HrDepartmentSetupByStatus',
        function($scope,$rootScope, $state, $stateParams, entity, HrEmpTransferApplInfo, HrEmployeeInfo, MiscTypeSetupByCategory, HrEmpWorkAreaDtlInfo, HrDesignationSetup,HrEmployeeInfoByWorkArea, Principal,User,DateUtils,HrEmployeeOfInstitutes,HrDepartmentSetupByStatus) {

            $scope.hrEmpTransferApplInfo = entity;

            if($stateParams.id !=null){
                HrEmpTransferApplInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmpTransferApplInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.hrempworkareadtlinfos = HrEmpWorkAreaDtlInfo.query();
            $scope.orgNameFilterListOne = $scope.hrempworkareadtlinfos;
            $scope.orgNameFilterListTwo = $scope.hrempworkareadtlinfos;
            $scope.orgNameFilterListThree = $scope.hrempworkareadtlinfos;
            $scope.hrdesignationsetups = HrDesignationSetup.query();

            $scope.load = function(id) {
                HrEmpTransferApplInfo.get({id : id}, function(result) {
                    $scope.hrEmpTransferApplInfo = result;
                });
            };

            $scope.hremployeeinfos  = [];
            $scope.workAreaList  = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                });
            };

            $scope.loadOrganizationNameByCategoryOne = function(organizationCategory)
            {
                $scope.orgNameFilterListOne = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListOne.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryTwo = function(organizationCategory)
            {
                $scope.orgNameFilterListTwo = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListTwo.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryThree = function(organizationCategory)
            {
                $scope.orgNameFilterListThree = [];
                angular.forEach($scope.workAreaList,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListThree.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryFour = function(organizationCategory)
            {
                $scope.orgNameFilterListFour = [];
                angular.forEach($scope.workAreaList,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListFour.push(orgNameObj);
                    }
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTransferApplInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpTransferApplInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpTransferApplInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpTransferApplInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpTransferApplInfo.id != null)
                {
                    $scope.hrEmpTransferApplInfo.logId = 0;
                    $scope.hrEmpTransferApplInfo.logStatus = 6;
                    HrEmpTransferApplInfo.update($scope.hrEmpTransferApplInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpTransferApplInfo.update');
                }
                else
                {
                    $scope.hrEmpTransferApplInfo.selectedOption = 0;
                    $scope.hrEmpTransferApplInfo.logId = 0;
                    $scope.hrEmpTransferApplInfo.logStatus = 6;
                    $scope.hrEmpTransferApplInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpTransferApplInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpTransferApplInfo.save($scope.hrEmpTransferApplInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpTransferApplInfo.save');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrEmpTransferApplInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpTransferApplInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpTransferApplInfo', 'HrEmployeeInfo', 'MiscTypeSetup', 'HrEmpWorkAreaDtlInfo', 'HrDesignationSetup',
            function ($scope, $rootScope, $stateParams, entity, HrEmpTransferApplInfo, HrEmployeeInfo, MiscTypeSetup, HrEmpWorkAreaDtlInfo, HrDesignationSetup) {
                $scope.hrEmpTransferApplInfo = entity;
                $scope.load = function (id) {
                    HrEmpTransferApplInfo.get({id: id}, function(result) {
                        $scope.hrEmpTransferApplInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpTransferApplInfoUpdate', function(event, result) {
                    $scope.hrEmpTransferApplInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpTransferApplInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpTransferApplInfoDeleteController',
        ['$scope','$rootScope', '$modalInstance', 'entity', 'HrEmpTransferApplInfo',
            function($scope,$rootScope, $modalInstance, entity, HrEmpTransferApplInfo) {

                $scope.hrEmpTransferApplInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpTransferApplInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpTransferApplInfo.deleted');
                        });
                };

            }]);
/*hrEmpTransferApplInfo-approval.controller.js*/

angular.module('stepApp').controller('HrEmpTransferApplInfoApprovalController',
    ['$scope', '$state', '$stateParams', 'entity','$modalInstance', '$timeout', 'HrEmpTransferApplInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'HrEmpWorkAreaDtlInfo', 'HrDesignationSetup','HrEmployeeInfoByWorkArea','Principal','User','DateUtils','HrEmpServiceHistoryByEmployee',
        function($scope, $state, $stateParams, entity, $modalInstance, $timeout, HrEmpTransferApplInfo, HrEmployeeInfo, MiscTypeSetupByCategory, HrEmpWorkAreaDtlInfo, HrDesignationSetup,HrEmployeeInfoByWorkArea, Principal,User,DateUtils,HrEmpServiceHistoryByEmployee) {

            $scope.hrEmpTransferApplInfo = entity;
            $scope.hrEmpTransferApplInfoTemp = $scope.hrEmpTransferApplInfo;
            $scope.hremployeeinfos = [];//HrEmployeeInfo.query();
            $scope.hrempworkareadtlinfos = [];//HrEmpWorkAreaDtlInfo.query();
            $scope.orgNameFilterListOne = $scope.hrempworkareadtlinfos;
            $scope.orgNameFilterListTwo = $scope.hrempworkareadtlinfos;
            $scope.orgNameFilterListThree = $scope.hrempworkareadtlinfos;
            $scope.empServiceHistoryList = [];

            $timeout(function()
            {
                console.log('EmpId '+$scope.hrEmpTransferApplInfo.employeeInfo.id);
                HrEmpServiceHistoryByEmployee.query({empId:$scope.hrEmpTransferApplInfo.employeeInfo.id}, function(result) {
                    $scope.empServiceHistoryList = result;
                    $scope.hrEmpTransferApplInfo.isApproved = true;
                });
            }, 2000);

            $scope.hrdesignationsetups = HrDesignationSetup.query();
            $scope.load = function(id) {
                HrEmpTransferApplInfo.get({id : id}, function(result) {
                    $scope.hrEmpTransferApplInfo = result;
                });
            };

            $scope.hremployeeinfos  = [];
            $scope.workAreaList  = [];//MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                });
            };

            $scope.loadOrganizationNameByCategoryOne = function(organizationCategory)
            {
                $scope.orgNameFilterListOne = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListOne.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryTwo = function(organizationCategory)
            {
                $scope.orgNameFilterListTwo = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListTwo.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryThree = function(organizationCategory)
            {
                $scope.orgNameFilterListThree = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListThree.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryFour = function(organizationCategory)
            {
                $scope.orgNameFilterListFour = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListFour.push(orgNameObj);
                    }
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTransferApplInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrm', {}, { reload: true });
                $modalInstance.dismiss('cancel');
                //$state.transitionTo('hrm', null, {'reload':true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpTransferApplInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpTransferApplInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                //console.log(JSON.stringify($scope.hrEmpTransferApplInfo));

                if($scope.hrEmpTransferApplInfo.selectedOption=='1')
                {
                    console.log("One selected");
                }
                else if($scope.hrEmpTransferApplInfo.selectedOption=='2')
                {
                    console.log("Two selected");
                }
                else if($scope.hrEmpTransferApplInfo.selectedOption=='3')
                {
                    console.log("Three selected");
                }

                $scope.hrEmpTransferApplInfo.logStatus = 8;
                if($scope.hrEmpTransferApplInfo.isApproved)
                {
                    $scope.hrEmpTransferApplInfo.logStatus = 7;
                }
                else
                {
                    $scope.hrEmpTransferApplInfo.selectedOption = 0;
                }
                console.log("Selected: "+$scope.hrEmpTransferApplInfo.selectedOption+", approved: "+$scope.hrEmpTransferApplInfo.isApproved+", logStat: "+$scope.hrEmpTransferApplInfo.logStatus);
                if ($scope.hrEmpTransferApplInfo.id != null)
                {
                    //$scope.hrEmpTransferApplInfo.logId = 0;
                    //$scope.hrEmpTransferApplInfo.logStatus = 6;
                    HrEmpTransferApplInfo.update($scope.hrEmpTransferApplInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*hrEmpTransferApplInfo-appl.controller.js*/

angular.module('stepApp').controller('HrEmpTransferApplInfoApplicationController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'HrEmpTransferApplInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'HrEmpWorkAreaDtlInfo', 'HrDesignationSetup','HrEmployeeInfoByWorkArea','Principal','User','DateUtils','HrDepartmentSetupByStatus','GovernmentInstitutes',
        function($scope, $rootScope, $state, $stateParams, entity, HrEmpTransferApplInfo, HrEmployeeInfo, MiscTypeSetupByCategory, HrEmpWorkAreaDtlInfo, HrDesignationSetup,HrEmployeeInfoByWorkArea, Principal,User,DateUtils,HrDepartmentSetupByStatus,GovernmentInstitutes) {

            $scope.hrEmpTransferApplInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.hrempworkareadtlinfos = HrEmpWorkAreaDtlInfo.query();
            $scope.orgNameFilterListOne = $scope.hrempworkareadtlinfos;
            $scope.orgNameFilterListTwo = $scope.hrempworkareadtlinfos;
            $scope.orgNameFilterListThree = $scope.hrempworkareadtlinfos;
            $scope.hrdesignationsetups = HrDesignationSetup.query({size: 2000});
            $scope.hrDesigSetupListFltr1 = $scope.hrdesignationsetups;
            $scope.hrDesigSetupListFltr2 = $scope.hrdesignationsetups;
            $scope.hrDesigSetupListFltr3 = $scope.hrdesignationsetups;
            $scope.hrDesigSetupListFltr4 = $scope.hrdesignationsetups;

            $scope.hrdepartmentsetups   = HrDepartmentSetupByStatus.get({stat:'true'});
            $scope.hrdepartmentsetupsFltr1   = $scope.hrdepartmentsetups;
            $scope.hrdepartmentsetupsFltr2   = $scope.hrdepartmentsetups;
            $scope.hrdepartmentsetupsFltr3   = $scope.hrdepartmentsetups;
            $scope.hrdepartmentsetupsFltr4   = $scope.hrdepartmentsetups;

            $scope.instituteListAll = GovernmentInstitutes.query({size: 2000});

            $scope.transferTypeList     = MiscTypeSetupByCategory.get({cat:'TransferType',stat:'true'});
            $scope.workAreaList  = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadDesigDeptByOrganization = function (orgnization, option)
            {
                console.log('^^^^^^^^^^^^^^^^^^');
                console.log(orgnization);
                $scope.hrDesigSetupListFltrTemp = [];
                console.log("Total Desig: "+$scope.hrdesignationsetups.length+", option: "+option);
                console.log($scope.hrdesignationsetups);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    if(desigInfo.organizationType =='Organization')
                    {
                        console.log('*********************************8*');
                        if(desigInfo.organizationInfo != null)
                        {
                            console.log('&&&&&&&&&&&&');
                            console.log(orgnization);
                            console.log(desigInfo.organizationInfo);
                            //console.log("orgId: "+desigInfo.organizationInfo.id + ", Srcorgid; "+orgnization.id);
                            if(orgnization.id === desigInfo.organizationInfo.id)
                            {
                                console.log('+++++++++++++++=');
                                $scope.hrDesigSetupListFltrTemp.push(desigInfo);
                            }
                        }
                    }
                });

                console.log("Total Section: "+$scope.hrdepartmentsetups.length);
                $scope.hrdepartmentsetupsFltrTemp = [];
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Organization')
                    {
                        if(sectionInfo.organizationInfo != null)
                        {
                            //console.log("sectionId: "+sectionInfo.organizationInfo.id+", SrcsecId"+sectionInfo);
                            if(orgnization.id === sectionInfo.organizationInfo.id)
                            {
                                $scope.hrdepartmentsetupsFltrTemp.push(sectionInfo);
                            }
                        }
                    }
                });

                if(option==1)
                {
                    $scope.hrDesigSetupListFltr1 = $scope.hrDesigSetupListFltrTemp;
                    $scope.hrdepartmentsetupsFltr1 = $scope.hrdepartmentsetupsFltrTemp;
                }
                else if(option==2)
                {
                    $scope.hrDesigSetupListFltr2 = $scope.hrDesigSetupListFltrTemp;
                    $scope.hrdepartmentsetupsFltr2 = $scope.hrdepartmentsetupsFltrTemp;
                }
                else if(option==3)
                {
                    $scope.hrDesigSetupListFltr3 = $scope.hrDesigSetupListFltrTemp;
                    $scope.hrdepartmentsetupsFltr3 = $scope.hrdepartmentsetupsFltrTemp;
                }else if(option==4)
                {
                    $scope.hrDesigSetupListFltr4 = $scope.hrDesigSetupListFltrTemp;
                    $scope.hrdepartmentsetupsFltr4 = $scope.hrdepartmentsetupsFltrTemp;
                }
            };


            $scope.loadDesigDeptByInstitute = function (institute, option)
            {
                console.log("loadDesigDeptByInstitute => option:"+option);
                console.log(institute);

                if(institute!=null && institute.id!=null)
                {

                    console.log("FilterInstId: "+institute.id+", nmae: "+institute.name);
                    $scope.hrDesigSetupListFltrTemp = [];
                    console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                    angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                    {
                        console.log("desigId: "+desigInfo.id);
                        if(desigInfo.organizationType=='Institute')
                        {
                            //if(desigInfo.institute !=null && institute!= null && institute.id != null)
                            //{
                            //    if(institute.id == desigInfo.institute.id){
                                    console.log("FoundDesig: DesigId: "+desigInfo.id);
                                    $scope.hrDesigSetupListFltrTemp.push(desigInfo);
                            //    }
                            //}

                        }
                    });

                    $scope.hrdepartmentsetupsFltrTemp = [];
                    console.log("totalSection: "+$scope.hrdepartmentsetups.length);
                    angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                    {
                        if(sectionInfo.organizationType=='Institute')
                        {
                            //console.log("sectionId1: "+sectionInfo.id);
                            if(sectionInfo.institute !=null && institute!= null && institute.id != null)
                            {

                                if(institute.id == sectionInfo.institute.id){
                                    //console.log("FoundSection: SecId: "+sectionInfo.id);
                                    $scope.hrdepartmentsetupsFltrTemp.push(sectionInfo);
                                }
                            }

                        }
                    });

                    if(option==1)
                    {
                        $scope.hrDesigSetupListFltr1 = $scope.hrDesigSetupListFltrTemp;
                        $scope.hrdepartmentsetupsFltr1 = $scope.hrdepartmentsetupsFltrTemp;
                    }
                    else if(option==2)
                    {
                        $scope.hrDesigSetupListFltr2 = $scope.hrDesigSetupListFltrTemp;
                        $scope.hrdepartmentsetupsFltr2 = $scope.hrdepartmentsetupsFltrTemp;
                    }
                    else if(option==3)
                    {
                        $scope.hrDesigSetupListFltr3 = $scope.hrDesigSetupListFltrTemp;
                        $scope.hrdepartmentsetupsFltr3 = $scope.hrdepartmentsetupsFltrTemp;
                    }
                    else if(option==4)
                    {
                        $scope.hrDesigSetupListFltr4 = $scope.hrDesigSetupListFltrTemp;
                        $scope.hrdepartmentsetupsFltr4 = $scope.hrdepartmentsetupsFltrTemp;
                    }
                }

            };

            $scope.loadOrganizationNameByCategoryOne = function(organizationCategory)
            {
                $scope.orgNameFilterListOne = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListOne.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryTwo = function(organizationCategory)
            {
                $scope.orgNameFilterListTwo = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListTwo.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryThree = function(organizationCategory)
            {
                $scope.orgNameFilterListThree = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListThree.push(orgNameObj);
                    }
                });
            };

            $scope.loadOrganizationNameByCategoryFour = function(organizationCategory)
            {
                $scope.orgNameFilterListFour = [];
                angular.forEach($scope.hrempworkareadtlinfos,function(orgNameObj)
                {
                    if(organizationCategory.id == orgNameObj.workArea.id){
                        $scope.orgNameFilterListFour.push(orgNameObj);
                    }
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadTransferApplicationProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmpTransferApplInfo.query({id: 'my'}, function (result)
                {
                    //console.log(JSON.stringify(result));
                    $scope.hrEmpTransferApplInfoList = result;
                    if($scope.hrEmpTransferApplInfoList.length < 1)
                    {
                        $scope.addMode = true;
                        //$scope.hrEmpTransferApplInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else
                    {
                        $scope.hrEmployeeInfo = $scope.hrEmpTransferApplInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpTransferApplInfoList,function(modelInfo)
                        {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if(modelInfo.logStatus==0)
                            {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                    {
                        if($scope.hrEmpTransferApplInfoList.length > 1)
                        {
                            var indx = $scope.hrEmpTransferApplInfoList.indexOf(modelInfo);
                            $scope.hrEmpTransferApplInfoList.splice(indx, 1);
                        }
                        else
                        {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function()
            {
                $scope.hrEmpTransferApplInfoList.push(
                    {
                        viewMode:false,
                        viewModeText:'Cancel',
                        orgTypeOptOne:'Organization',
                        orgTypeOptTwo:'Organization',
                        orgTypeOptThree:'Organization',
                        orgTypeOptFour:'Organization',
                        transferReason: null,
                        officeOrderNumber: null,
                        officeOrderDate: null,
                        activeStatus: true,
                        selectedOption: 0,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    orgTypeOptOne:'Organization',
                    orgTypeOptTwo:'Organization',
                    orgTypeOptThree:'Organization',
                    orgTypeOptFour:'Organization',
                    transferReason: null,
                    officeOrderNumber: null,
                    officeOrderDate: null,
                    activeStatus: true,
                    selectedOption: 0,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };


            $scope.calendar_local = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTransferApplInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpTransferApplInfoList[$scope.selectedIndex].id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            $scope.updateProfile = function (modelInfo, index)
            {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                console.log('*************');
                console.log(modelInfo);
                if($scope.preCondition(modelInfo)){
                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmpTransferApplInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    modelInfo.selectedOption = 0;
                    HrEmpTransferApplInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }}else{
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };
            $scope.preCondition = function (hrEmpTransferApplInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpTransferApplInfo.officeOrderNumber', hrEmpTransferApplInfo.officeOrderNumber, 'number', 30, 1, '0to9', true, '', ''));
                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function() {

            };
        }]);
