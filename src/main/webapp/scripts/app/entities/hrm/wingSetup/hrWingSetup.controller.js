'use strict';

angular.module('stepApp')
    .controller('HrWingSetupController', function ($scope, $state, $modal, HrWingSetup, HrWingSetupSearch, ParseLinks) {

        $scope.hrWingSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            HrWingSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.hrWingSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrWingSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrWingSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrWingSetup = {
                wingName: null,
                wingDesc: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*hrWingSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrWingSetupDialogController',
    ['$rootScope','$scope', '$state', '$stateParams', 'entity', 'HrWingSetup', 'HrEmployeeInfosByDesigLevel','Principal','User','DateUtils','HrWingHeadSetupByWing',
        function($rootScope, $scope, $state, $stateParams, entity, HrWingSetup, HrEmployeeInfosByDesigLevel, Principal,User,DateUtils,HrWingHeadSetupByWing) {

            $scope.hrWingSetup = entity;
            $scope.hremployeeinfos = [];
            $scope.hrWingHeadSetups = [];

            HrEmployeeInfosByDesigLevel.get({desigLevel : 3}, function(result) {
                $scope.hremployeeinfos = result;
            });

            $scope.load = function(id) {
                HrWingSetup.get({id : id}, function(result) {
                    $scope.hrWingSetup = result;
                });
            };

            $scope.parentWingId = 0;
            $scope.editWhen=false;
            $scope.getWingHeadList = function ()
            {
                if($stateParams.id)
                {
                    console.log("Load head info list parent id: "+$stateParams.id);
                    $scope.parentWingId = $stateParams.id;
                    HrWingHeadSetupByWing.get({wingId : $stateParams.id}, function(result) {
                        console.log("totalResult: "+result.length);
                        $scope.hrWingHeadSetups = result;
                    });
                    $scope.editWhen=true;
                }
                else
                {
                    $scope.parentWingId = 0;
                }
                console.log("list parentWingId: "+$scope.parentWingId);
            };


            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                        $scope.getWingHeadList();
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrWingSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrWingSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrWingSetup.updateBy = $scope.loggedInUser.id;
                $scope.hrWingSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrWingSetup.id != null)
                {
                    HrWingSetup.update($scope.hrWingSetup, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrWingSetup.updated');
                }
                else
                {
                    $scope.hrWingSetup.createBy = $scope.loggedInUser.id;
                    $scope.hrWingSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrWingSetup.save($scope.hrWingSetup, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrWingSetup.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrWingSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrWingSetupDetailController', function ($scope, $rootScope, $stateParams, entity, HrWingSetup, HrEmployeeInfo) {
        $scope.hrWingSetup = entity;
        $scope.load = function (id) {
            HrWingSetup.get({id: id}, function(result) {
                $scope.hrWingSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:hrWingSetupUpdate', function(event, result) {
            $scope.hrWingSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*hrWingSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrWingSetupDeleteController', function($scope, $modalInstance, entity, HrWingSetup) {

        $scope.hrWingSetup = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            HrWingSetup.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
