'use strict';

angular.module('stepApp')
    .controller('HrEmpAwardInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpAwardInfo', 'HrEmpAwardInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEmpAwardInfo, HrEmpAwardInfoSearch, ParseLinks) {

                $scope.hrEmpAwardInfos = [];

                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpAwardInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpAwardInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpAwardInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpAwardInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpAwardInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpAwardInfo = {
                        awardName: null,
                        awardArea: null,
                        awardDate: null,
                        remarks: null,
                        awardReceivedFrom: null,
                        goOrderDoc: null,
                        goOrderDocContentType: null,
                        goOrderDocName: null,
                        certDoc: null,
                        certDocContentType: null,
                        certDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEmpAwardInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpAwardInfoDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'HrEmpAwardInfo', 'HrEmployeeInfoByWorkArea', 'User', 'Principal', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $sce, $stateParams, $state, DataUtils, entity, HrEmpAwardInfo, HrEmployeeInfoByWorkArea, User, Principal, DateUtils, MiscTypeSetupByCategory, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpAwardInfo = entity;
            if ($stateParams.id != null) {
                HrEmpAwardInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpAwardInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                console.log("emp by inst....");
                console.log($scope.hremployeeinfosOfInstitute);
                //console.log("Total record: "+result.length);
            });
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.hremployeeinfos = [];
            $scope.load = function (id) {
                HrEmpAwardInfo.get({id: id}, function (result) {
                    $scope.hrEmpAwardInfo = result;
                });
            };

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (awardInfo) {
                HrEmployeeInfoByEmployeeId.get({id: awardInfo.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpAwardInfo.employeeInfo = result;
                });
            }

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAwardInfoUpdate', result);
                $scope.isSaving = false;
                console.log("TrainingSave: " + JSON.stringify(result));
                $state.go('hrEmpAwardInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpAwardInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpAwardInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpAwardInfo.id != null) {
                    $scope.hrEmpAwardInfo.logId = 0;
                    $scope.hrEmpAwardInfo.logStatus = 6;
                    HrEmpAwardInfo.update($scope.hrEmpAwardInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpAwardInfo.updated');
                }
                else {
                    $scope.hrEmpAwardInfo.logId = 0;
                    $scope.hrEmpAwardInfo.logStatus = 6;
                    $scope.hrEmpAwardInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpAwardInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpAwardInfo.save($scope.hrEmpAwardInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpAwardInfo.created');
                }
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoOrderDoc = function ($file, hrEmpAwardInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpAwardInfo.goOrderDoc = base64Data;
                            hrEmpAwardInfo.goOrderDocContentType = $file.type;
                            if (hrEmpAwardInfo.goOrderDocName == null) {
                                hrEmpAwardInfo.goOrderDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setCertDoc = function ($file, hrEmpAwardInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpAwardInfo.certDoc = base64Data;
                            hrEmpAwardInfo.certDocContentType = $file.type;
                            if (hrEmpAwardInfo.certDocName == null) {
                                hrEmpAwardInfo.certDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewGODoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goOrderDoc, modelInfo.goOrderDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goOrderDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Award GO Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.certDoc, modelInfo.certDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.certDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Award Certificate Document";
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpAwardInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAwardInfoDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpAwardInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpAwardInfo, HrEmployeeInfo) {
                $scope.hrEmpAwardInfo = entity;
                $scope.load = function (id) {
                    HrEmpAwardInfo.get({id: id}, function (result) {
                        $scope.hrEmpAwardInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpAwardInfoUpdate', function (event, result) {
                    $scope.hrEmpAwardInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewGODoc = function (modelInfo) {
                    var blob = $rootScope.b64toBlob(modelInfo.goOrderDoc, modelInfo.goOrderDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.goOrderDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Award GO Order Document";
                    $rootScope.showPreviewModal();
                };

                $scope.previewCertDoc = function (modelInfo) {
                    var blob = $rootScope.b64toBlob(modelInfo.certDoc, modelInfo.certDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.certDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Award Certificate Document";
                    $rootScope.showPreviewModal();
                };

            }]);
/*hrEmpAwardInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAwardInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpAwardInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpAwardInfo) {

                $scope.hrEmpAwardInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpAwardInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpAwardInfo.deleted');
                        });
                };

            }]);
/*hrEmpAwardInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpAwardInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpAwardInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils',
        function ($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpAwardInfo, HrEmployeeInfo, User, Principal, DateUtils) {

            $scope.hrEmpAwardInfo = {};

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadAwarddProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpAwardInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpAwardInfoList = result;
                    if ($scope.hrEmpAwardInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpAwardInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpAwardInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpAwardInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpAwardInfoList.length > 1) {
                            var indx = $scope.hrEmpAwardInfoList.indexOf(modelInfo);
                            $scope.hrEmpAwardInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpAwardInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        awardName: null,
                        awardArea: null,
                        awardReceivedFrom: null,
                        awardDate: null,
                        remarks: null,
                        certNumber: null,
                        goOrderDoc: null,
                        goOrderDocContentType: null,
                        goOrderDocName: null,
                        certDoc: null,
                        certDocContentType: null,
                        certDocName: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    awardName: null,
                    awardArea: null,
                    awardDate: null,
                    remarks: null,
                    certNumber: null,
                    goOrderDoc: null,
                    goOrderDocContentType: null,
                    goOrderDocName: null,
                    certDoc: null,
                    certDocContentType: null,
                    certDocName: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAwardInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpAwardInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpAwardInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEmpAwardInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpAwardInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAwardInfo.awardName', hrEmpAwardInfo.awardName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAwardInfo.awardArea', hrEmpAwardInfo.awardArea, 'text', 50, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAwardInfo.awardReceivedFrom', hrEmpAwardInfo.awardReceivedFrom, 'text', 100, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAwardInfo.certNumber', hrEmpAwardInfo.certNumber, 'number', 30, 6, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAwardInfo.awardDate', DateUtils.convertLocaleDateToDMY(hrEmpAwardInfo.awardDate), 'date', 60, 5, 'date', true, '', ''));
                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.previewGODoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goOrderDoc, modelInfo.goOrderDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goOrderDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Award GO Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.certDoc, modelInfo.certDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.certDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Award Certificate Document";
                $rootScope.showPreviewModal();
            };

            $scope.setGoOrderDoc = function ($file, hrEmpAwardInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpAwardInfo.goOrderDoc = base64Data;
                            hrEmpAwardInfo.goOrderDocContentType = $file.type;
                            if (hrEmpAwardInfo.goOrderDocName == null || hrEmpAwardInfo.goOrderDocName == '') {
                                hrEmpAwardInfo.goOrderDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setCertDoc = function ($file, hrEmpAwardInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpAwardInfo.certDoc = base64Data;
                            hrEmpAwardInfo.certDocContentType = $file.type;
                            if (hrEmpAwardInfo.certDocName == null || hrEmpAwardInfo.certDocName == '') {
                                hrEmpAwardInfo.certDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
