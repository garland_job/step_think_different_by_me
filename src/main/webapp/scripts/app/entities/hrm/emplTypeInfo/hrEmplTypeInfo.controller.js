'use strict';

angular.module('stepApp')
    .controller('HrEmplTypeInfoController',
        ['$scope', '$state', 'HrEmplTypeInfo', 'HrEmplTypeInfoSearch', 'ParseLinks',
            function ($scope, $state, HrEmplTypeInfo, HrEmplTypeInfoSearch, ParseLinks) {

                $scope.hrEmplTypeInfos = [];
                $scope.predicate = 'typeName';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.loadAll = function () {
                    HrEmplTypeInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmplTypeInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmplTypeInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmplTypeInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmplTypeInfo = {
                        typeCode: null,
                        typeName: null,
                        typeDetail: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);

/*hrEmplTypeInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmplTypeInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmplTypeInfo', 'Principal', 'User', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmplTypeInfo, Principal, User, DateUtils) {

            $scope.hrEmplTypeInfo = entity;
            $scope.load = function (id) {
                HrEmplTypeInfo.get({id: id}, function (result) {
                    $scope.hrEmplTypeInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmplTypeInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmplTypeInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            $scope.save = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.isSaving = true;
                        $scope.hrEmplTypeInfo.updateBy = result.id;
                        $scope.hrEmplTypeInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.preCondition()) {
                            if ($scope.hrEmplTypeInfo.id != null) {
                                HrEmplTypeInfo.update($scope.hrEmplTypeInfo, onSaveSuccess, onSaveError);
                                $rootScope.setWarningMessage('stepApp.hrEmplTypeInfo.updated');
                            }
                            else {
                                $scope.hrEmplTypeInfo.createBy = result.id;
                                $scope.hrEmplTypeInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                                HrEmplTypeInfo.save($scope.hrEmplTypeInfo, onSaveSuccess, onSaveError);
                                $rootScope.setSuccessMessage('stepApp.hrEmplTypeInfo.created');
                            }
                        } else {
                            $rootScope.setErrorMessage("Please input valid data");
                        }
                    });
                });
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmplTypeInfo.typeCode', $scope.hrEmplTypeInfo.typeCode, 'text', 50, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmplTypeInfo.typeName', $scope.hrEmplTypeInfo.typeName, 'number', 12, 1, 'atozAndAtoZ0-9-/', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {
            };
        }]);

/*hrEmplTypeInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmplTypeInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmplTypeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmplTypeInfo) {
                $scope.hrEmplTypeInfo = entity;
                $scope.load = function (id) {
                    HrEmplTypeInfo.get({id: id}, function (result) {
                        $scope.hrEmplTypeInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmplTypeInfoUpdate', function (event, result) {
                    $scope.hrEmplTypeInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*hrEmplTypeInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmplTypeInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'HrEmplTypeInfo',
            function ($scope, $modalInstance, entity, HrEmplTypeInfo) {

                $scope.hrEmplTypeInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmplTypeInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);
