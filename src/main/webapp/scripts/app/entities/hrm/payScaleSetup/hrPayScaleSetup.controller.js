'use strict';

angular.module('stepApp')
    .controller('HrPayScaleSetupController',
     ['$scope', '$state', 'HrPayScaleSetup', 'HrPayScaleSetupSearch', 'ParseLinks',
     function ($scope, $state, HrPayScaleSetup, HrPayScaleSetupSearch, ParseLinks) {

        $scope.hrPayScaleSetups = [];
        $scope.predicate = 'payScaleCode';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.loadAll = function() {
            HrPayScaleSetup.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrPayScaleSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrPayScaleSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrPayScaleSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrPayScaleSetup = {
                payScaleCode: null,
                basicPayScale: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrPayScaleSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrPayScaleSetupDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrPayScaleSetup', 'HrGradeSetup','Principal','User','DateUtils','HrGazetteSetup',
        function($scope, $rootScope, $stateParams, $state, entity, HrPayScaleSetup, HrGradeSetup, Principal, User, DateUtils, HrGazetteSetup) {

            $scope.hrPayScaleSetup = entity;
            $scope.hrgradesetups = HrGradeSetup.query({id:'bystat', size: 500});
            $scope.hrgazettesetups = HrGazetteSetup.query({size:500});
            $scope.load = function(id)
            {
                HrPayScaleSetup.get({id : id}, function(result) {
                    $scope.hrPayScaleSetup = result;
                });
            };

            var onSaveSuccess = function (result)
            {
                $scope.$emit('stepApp:hrPayScaleSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrPayScaleSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.isSaving = true;
                        $scope.hrPayScaleSetup.updateBy = result.id;
                        $scope.hrPayScaleSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.hrPayScaleSetup.id != null)
                        {
                            HrPayScaleSetup.update($scope.hrPayScaleSetup, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrPayScaleSetup.updated');
                        }
                        else
                        {
                            $scope.hrPayScaleSetup.createBy = result.id;
                            $scope.hrPayScaleSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrPayScaleSetup.save($scope.hrPayScaleSetup, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrPayScaleSetup.created');
                        }
                    });
                });
            };

            $scope.clear = function() {
            };
        }]);
/*hrPayScaleSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrPayScaleSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrPayScaleSetup', 'HrGradeSetup',
            function ($scope, $rootScope, $stateParams, entity, HrPayScaleSetup, HrGradeSetup) {
                $scope.hrPayScaleSetup = entity;
                $scope.load = function (id) {
                    HrPayScaleSetup.get({id: id}, function(result) {
                        $scope.hrPayScaleSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrPayScaleSetupUpdate', function(event, result) {
                    $scope.hrPayScaleSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrPayScaleSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrPayScaleSetupDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrPayScaleSetup',
            function($scope, $rootScope, $modalInstance, entity, HrPayScaleSetup) {

                $scope.hrPayScaleSetup = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrPayScaleSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrPayScaleSetup.deleted');
                        });
                };

            }]);
