'use strict';

angular.module('stepApp')
    .controller('HrEmpPreGovtJobInfoController',
        ['$rootScope', '$scope', '$state', 'HrEmpPreGovtJobInfo', 'HrEmpPreGovtJobInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrEmpPreGovtJobInfo, HrEmpPreGovtJobInfoSearch, ParseLinks) {

                $scope.hrEmpPreGovtJobInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpPreGovtJobInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpPreGovtJobInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpPreGovtJobInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpPreGovtJobInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpPreGovtJobInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpPreGovtJobInfo = {
                        organizationName: null,
                        postName: null,
                        address: null,
                        fromDate: null,
                        toDate: null,
                        salary: null,
                        comments: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpPreGovtJobInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpPreGovtJobInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpPreGovtJobInfo', 'HrEmployeeInfoByWorkArea', 'User', 'Principal', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmpPreGovtJobInfo, HrEmployeeInfoByWorkArea, User, Principal, DateUtils, MiscTypeSetupByCategory, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpPreGovtJobInfo = entity;
            if ($stateParams.id != null) {
                HrEmpPreGovtJobInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpPreGovtJobInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.load = function (id) {
                HrEmpPreGovtJobInfo.get({id: id}, function (result) {
                    $scope.hrEmpPreGovtJobInfo = result;
                });
            };

            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };


            // $scope.loadHrEmployee = function(proMem)
            // {
            //     console.log("pre gosh");
            //     console.log(proMem);
            //     HrEmployeeInfoByEmployeeId.get({id: proMem.employeeId}, function (result){
            //         $scope.hrEmpPreGovtJobInfo = result;
            //     });
            //
            // };
            $scope.loadHrEmployee = function (proMem) {
                HrEmployeeInfoByEmployeeId.get({id: proMem.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpPreGovtJobInfo.employeeInfo = result;
                });
            }

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpPreGovtJobInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpPreGovtJobInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpPreGovtJobInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpPreGovtJobInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpPreGovtJobInfo.id != null) {
                    $scope.hrEmpPreGovtJobInfo.logId = 0;
                    $scope.hrEmpPreGovtJobInfo.logStatus = 6;
                    HrEmpPreGovtJobInfo.update($scope.hrEmpPreGovtJobInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpPreGovtJobInfo.updated');
                }
                else {
                    $scope.hrEmpPreGovtJobInfo.logId = 0;
                    $scope.hrEmpPreGovtJobInfo.logStatus = 6;
                    $scope.hrEmpPreGovtJobInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpPreGovtJobInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpPreGovtJobInfo.save($scope.hrEmpPreGovtJobInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpPreGovtJobInfo.created');
                }
            };

            $scope.clear = function () {

            };
        }]);
/*hrEmpPreGovtJobInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpPreGovtJobInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpPreGovtJobInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpPreGovtJobInfo, HrEmployeeInfo) {
                $scope.hrEmpPreGovtJobInfo = entity;
                $scope.load = function (id) {
                    HrEmpPreGovtJobInfo.get({id: id}, function (result) {
                        $scope.hrEmpPreGovtJobInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpPreGovtJobInfoUpdate', function (event, result) {
                    $scope.hrEmpPreGovtJobInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpPreGovtJobInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpPreGovtJobInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpPreGovtJobInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpPreGovtJobInfo) {

                $scope.hrEmpPreGovtJobInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpPreGovtJobInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpPreGovtJobInfo.deleted');
                };

            }]);
/*hrEmpPreGovtJobInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpPreGovtJobInfoProfileController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'HrEmpPreGovtJobInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, HrEmpPreGovtJobInfo, HrEmployeeInfo, User, Principal, DateUtils) {

            $scope.hrEmpPreGovtJobInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function (id) {
                HrEmpPreGovtJobInfo.get({id: id}, function (result) {
                    $scope.hrEmpPreGovtJobInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadPreGovtJobProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpPreGovtJobInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpPreGovtJobInfoList = result;
                    if ($scope.hrEmpPreGovtJobInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpPreGovtJobInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpPreGovtJobInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpPreGovtJobInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpPreGovtJobInfoList.length > 1) {
                            var indx = $scope.hrEmpPreGovtJobInfoList.indexOf(modelInfo);
                            $scope.hrEmpPreGovtJobInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpPreGovtJobInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        organizationName: null,
                        postName: null,
                        address: null,
                        fromDate: null,
                        toDate: null,
                        salary: null,
                        comments: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    organizationName: null,
                    postName: null,
                    address: null,
                    fromDate: null,
                    toDate: null,
                    salary: null,
                    comments: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                console.log(JSON.stringify("success: " + result));
                $scope.$emit('stepApp:hrEmpPreGovtJobInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpPreGovtJobInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                console.log(JSON.stringify("failed: " + result));
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                //console.log(JSON.stringify(modelInfo));
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpPreGovtJobInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                    else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                        HrEmpPreGovtJobInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpPreGovtJobInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPreGovtJobInfo.organizationName', hrEmpPreGovtJobInfo.organizationName, 'text', 150, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPreGovtJobInfo.postName', hrEmpPreGovtJobInfo.postName, 'text', 150, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPreGovtJobInfo.salary', hrEmpPreGovtJobInfo.salary, 'text', 150, 2, '0to9.', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPreGovtJobInfo.salary', hrEmpPreGovtJobInfo.salary, 'text', 150, 2, '0to9.', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPreGovtJobInfo.fromDate', DateUtils.convertLocaleDateToDMY(hrEmpPreGovtJobInfo.fromDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpPreGovtJobInfo.toDate', DateUtils.convertLocaleDateToDMY(hrEmpPreGovtJobInfo.toDate), 'date', 60, 5, 'date', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };
        }]);
