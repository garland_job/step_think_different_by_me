'use strict';

angular.module('stepApp')
    .controller('HrEmpForeignTourInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpForeignTourInfo', 'HrEmpForeignTourInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEmpForeignTourInfo, HrEmpForeignTourInfoSearch, ParseLinks) {

                $scope.hrEmpForeignTourInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpForeignTourInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpForeignTourInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpForeignTourInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpForeignTourInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpForeignTourInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpForeignTourInfo = {
                        purpose: null,
                        fromDate: null,
                        toDate: null,
                        countryName: null,
                        officeOrderNumber: null,
                        fundSource: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        goNumber: null,
                        officeOrderDate: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEmpForeignTourInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpForeignTourInfoDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'HrEmpForeignTourInfo', 'HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $sce, $stateParams, $state, DataUtils, entity, HrEmpForeignTourInfo, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, User, Principal, DateUtils, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpForeignTourInfo = entity;
            if ($stateParams.id != null) {
                HrEmpForeignTourInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpForeignTourInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'JobCategory', stat: 'true'});
            $scope.load = function (id) {
                HrEmpForeignTourInfo.get({id: id}, function (result) {
                    $scope.hrEmpForeignTourInfo = result;
                });
            };

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (entertainment) {
                console.log("load hr emp");
                HrEmployeeInfoByEmployeeId.get({id: entertainment.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpForeignTourInfo.employeeInfo = result;


                });

            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();
            $scope.todate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpForeignTourInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpForeignTourInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpForeignTourInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpForeignTourInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpForeignTourInfo.id != null) {
                    $scope.hrEmpForeignTourInfo.logId = 0;
                    $scope.hrEmpForeignTourInfo.logStatus = 6;
                    HrEmpForeignTourInfo.update($scope.hrEmpForeignTourInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpForeignTourInfo.updated');
                }
                else {
                    $scope.hrEmpForeignTourInfo.logId = 0;
                    $scope.hrEmpForeignTourInfo.logStatus = 6;
                    $scope.hrEmpForeignTourInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpForeignTourInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpForeignTourInfo.save($scope.hrEmpForeignTourInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpForeignTourInfo.created');
                }
            };

            $scope.clear = function () {
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpForeignTourInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpForeignTourInfo.goDoc = base64Data;
                            hrEmpForeignTourInfo.goDocContentType = $file.type;
                            if (hrEmpForeignTourInfo.goDocName == null) {
                                hrEmpForeignTourInfo.goDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Foreign Tour Document : " + modelInfo.countryName;

                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpForeignTourInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpForeignTourInfoDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpForeignTourInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpForeignTourInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEmpForeignTourInfo = entity;
                $scope.load = function (id) {
                    HrEmpForeignTourInfo.get({id: id}, function (result) {
                        $scope.hrEmpForeignTourInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpForeignTourInfoUpdate', function (event, result) {
                    $scope.hrEmpForeignTourInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewImage = function (modelInfo) {
                    var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Foreign Tour Document : " + modelInfo.countryName;
                    $rootScope.showPreviewModal();
                };
            }]);
/*hrEmpForeignTourInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpForeignTourInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpForeignTourInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpForeignTourInfo) {

                $scope.hrEmpForeignTourInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpForeignTourInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpForeignTourInfo.deleted');
                };

            }]);
/*hrEmpForeignTourInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpForeignTourInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpForeignTourInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils',
        function ($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpForeignTourInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils) {

            $scope.hrEmpForeignTourInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'JobCategory', stat: 'true'});
            $scope.load = function (id) {
                HrEmpForeignTourInfo.get({id: id}, function (result) {
                    $scope.hrEmpForeignTourInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadPreGovtJobProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpForeignTourInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpForeignTourInfoList = result;
                    if ($scope.hrEmpForeignTourInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpForeignTourInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpForeignTourInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpForeignTourInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpForeignTourInfoList.length > 1) {
                            var indx = $scope.hrEmpForeignTourInfoList.indexOf(modelInfo);
                            $scope.hrEmpForeignTourInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpForeignTourInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        purpose: null,
                        fromDate: null,
                        toDate: null,
                        countryName: null,
                        officeOrderNumber: null,
                        fundSource: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        goNumber: null,
                        officeOrderDate: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    purpose: null,
                    fromDate: null,
                    toDate: null,
                    countryName: null,
                    officeOrderNumber: null,
                    fundSource: null,
                    goDate: null,
                    goDoc: null,
                    goDocContentType: null,
                    goDocName: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpForeignTourInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpForeignTourInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpForeignTourInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                    else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                        HrEmpForeignTourInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };
            $scope.preCondition = function (hrEmpForeignTourInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.countryName', hrEmpForeignTourInfo.countryName, 'text', 50, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.purpose', hrEmpForeignTourInfo.purpose, 'text', 150, 10, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.fromDate', DateUtils.convertLocaleDateToDMY(hrEmpForeignTourInfo.fromDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.toDate', DateUtils.convertLocaleDateToDMY(hrEmpForeignTourInfo.toDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.officeOrderNumber', hrEmpForeignTourInfo.officeOrderNumber, 'number', 30, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.fundSource', hrEmpForeignTourInfo.fundSource, 'text', 50, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpForeignTourInfo.goNumber', hrEmpForeignTourInfo.goNumber, 'number', 30, 2, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {
            };

            $scope.previewImage = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Foreign Tour Document : " + modelInfo.countryName;

                $rootScope.showPreviewModal();
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpForeignTourInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEmpForeignTourInfo.goDoc = base64Data;
                            hrEmpForeignTourInfo.goDocContentType = $file.type;
                            if (hrEmpForeignTourInfo.goDocName == null) {
                                hrEmpForeignTourInfo.goDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);
