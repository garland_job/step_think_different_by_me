'use strict';

angular.module('stepApp')
    .controller('HrEmpBankAccountInfoController',
        ['$rootScope', '$scope', '$state', 'HrEmpBankAccountInfo', 'HrEmpBankAccountInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrEmpBankAccountInfo, HrEmpBankAccountInfoSearch, ParseLinks) {

                $scope.hrEmpBankAccountInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 1;
                $scope.stateName = "hrEmpBankAccountInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpBankAccountInfo.query({
                        page: $scope.page - 1,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpBankAccountInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpBankAccountInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpBankAccountInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpBankAccountInfo = {
                        accountName: null,
                        accountNumber: null,
                        branchName: null,
                        description: null,
                        salaryAccount: false,
                        activeStatus: false,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpBankAccountInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpBankAccountInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpBankAccountInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfoByWorkArea', 'HrEmpBankSalaryAccountInfo', 'HrEmployeeOfInstitutes',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmpBankAccountInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils, HrEmployeeInfoByWorkArea, HrEmpBankSalaryAccountInfo, HrEmployeeOfInstitutes) {

            $scope.hrEmpBankAccountInfo = entity;
            if ($stateParams.id != null) {
                HrEmpBankAccountInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpBankAccountInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hremployeeinfos = [];//HrEmployeeInfo.query();
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'BankName', stat: 'true'});
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.load = function (id) {
                HrEmpBankAccountInfo.get({id: id}, function (result) {
                    $scope.hrEmpBankAccountInfo = result;
                });
            };

            $scope.loadEmployeeByWorkArea = function (workArea) {
                //console.log(JSON.stringify(workArea));
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.salaryAccountCounter = 0;
            $scope.loadSalaryAccount = function (emplInfo) {
                //console.log(JSON.stringify(emplInfo.id))
                HrEmpBankSalaryAccountInfo.query({emplId: emplInfo.id}, function (result) {
                    $scope.salaryAccountCounter = result.value;
                    //console.log(JSON.stringify(result)+", count: "+$scope.salaryAccountCounter);
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpBankAccountInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpBankAccountInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpBankAccountInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpBankAccountInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrEmpBankAccountInfo.id != null) {
                    $scope.hrEmpBankAccountInfo.logId = 0;
                    $scope.hrEmpBankAccountInfo.logStatus = 6;
                    HrEmpBankAccountInfo.update($scope.hrEmpBankAccountInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpBankAccountInfo.updated');
                } else {
                    $scope.hrEmpBankAccountInfo.logId = 0;
                    $scope.hrEmpBankAccountInfo.logStatus = 6;
                    $scope.hrEmpBankAccountInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpBankAccountInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpBankAccountInfo.save($scope.hrEmpBankAccountInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpBankAccountInfo.created');
                }
            };

            $scope.clear = function () {

            };
        }]);
/*hrEmpBankAccountInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpBankAccountInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpBankAccountInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, HrEmpBankAccountInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEmpBankAccountInfo = entity;
                $scope.load = function (id) {
                    HrEmpBankAccountInfo.get({id: id}, function (result) {
                        $scope.hrEmpBankAccountInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpBankAccountInfoUpdate', function (event, result) {
                    $scope.hrEmpBankAccountInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpBankAccountInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpBankAccountInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpBankAccountInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpBankAccountInfo) {

                $scope.hrEmpBankAccountInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpBankAccountInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpBankAccountInfo.deleted');
                };

            }]);
/*hrEmpBankAccountInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpBankAccountInfoProfileController',
    ['$scope', '$rootScope', '$stateParams', 'HrEmpBankAccountInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'HrEmpBankSalaryAccountInfo',
        function ($scope, $rootScope, $stateParams, HrEmpBankAccountInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils, HrEmpBankSalaryAccountInfo) {

            $scope.hrEmpBankAccountInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'BankName', stat: 'true'});
            $scope.load = function (id) {
                HrEmpBankAccountInfo.get({id: id}, function (result) {
                    $scope.hrEmpBankAccountInfo = result;
                });
            };

            $scope.salaryAccountCounter = 0;
            $scope.loadSalaryAccount = function () {
                HrEmpBankSalaryAccountInfo.query({emplId: $scope.hrEmployeeInfo.id}, function (result) {
                    $scope.salaryAccountCounter = result.value;
                    //console.log(JSON.stringify(result)+", count: "+$scope.salaryAccountCounter);
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadBankAccountProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpBankAccountInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpBankAccountInfoList = result;
                    if ($scope.hrEmpBankAccountInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpBankAccountInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpBankAccountInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpBankAccountInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                            if (modelInfo.salaryAccount == true) {
                                $scope.salaryAccountCounter++;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpBankAccountInfoList.length > 1) {
                            var indx = $scope.hrEmpBankAccountInfoList.indexOf(modelInfo);
                            $scope.hrEmpBankAccountInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpBankAccountInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        accountName: null,
                        accountNumber: null,
                        branchName: null,
                        description: null,
                        salaryAccount: false,
                        activeStatus: true,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    accountName: null,
                    accountNumber: null,
                    branchName: null,
                    description: null,
                    salaryAccount: false,
                    activeStatus: true,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpBankAccountInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpBankAccountInfoList[$scope.selectedIndex].id = result.id;
                $scope.loadSalaryAccount();
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpBankAccountInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                        HrEmpBankAccountInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpBankAccountInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpBankAccountInfo.accountName', hrEmpBankAccountInfo.accountName, 'text', 100, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpBankAccountInfo.accountNumber', hrEmpBankAccountInfo.accountNumber, 'text', 50, 4, '0to9-', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {

            };
        }]);
