'use strict';

angular.module('stepApp')
    .controller('HrEmpGovtDuesInfoController',
     ['$rootScope','$scope', '$state', 'HrEmpGovtDuesInfo', 'HrEmpGovtDuesInfoSearch', 'ParseLinks',
     function ($rootScope,$scope, $state, HrEmpGovtDuesInfo, HrEmpGovtDuesInfoSearch, ParseLinks) {

        $scope.hrEmpGovtDuesInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmpGovtDuesInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEmpGovtDuesInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmpGovtDuesInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpGovtDuesInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpGovtDuesInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpGovtDuesInfo = {
                description: null,
                dueAmount: null,
                claimerAuthority: null,
                comments: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEmpGovtDuesInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpGovtDuesInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpGovtDuesInfo', 'HrEmployeeInfo','User','Principal','DateUtils','HrEmployeeOfInstitutes','MiscTypeSetupByCategory','HrEmployeeInfoByWorkArea','HrEmployeeInfoByEmployeeId',
        function($scope, $rootScope, $stateParams, $state, entity, HrEmpGovtDuesInfo, HrEmployeeInfo, User, Principal, DateUtils,HrEmployeeOfInstitutes,MiscTypeSetupByCategory,HrEmployeeInfoByWorkArea,HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpGovtDuesInfo = entity;
            if($stateParams.id !=null){
                HrEmpGovtDuesInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmpGovtDuesInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                HrEmpGovtDuesInfo.get({id : id}, function(result) {
                    $scope.hrEmpGovtDuesInfo = result;
                });
            };
            $scope.loadHrEmployee = function(empGovtDuos)
            {
                HrEmployeeInfoByEmployeeId.get({id: empGovtDuos.employeeInfo.employeeId}, function (result){
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpGovtDuesInfo.employeeInfo = result;


                });

            }

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpGovtDuesInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpGovtDuesInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpGovtDuesInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpGovtDuesInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpGovtDuesInfo.id != null)
                {
                    $scope.hrEmpGovtDuesInfo.logId = 0;
                    $scope.hrEmpGovtDuesInfo.logStatus = 0;
                    HrEmpGovtDuesInfo.update($scope.hrEmpGovtDuesInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpGovtDuesInfo.updated');
                }
                else
                {
                    $scope.hrEmpGovtDuesInfo.logId = 0;
                    $scope.hrEmpGovtDuesInfo.logStatus = 0;
                    $scope.hrEmpGovtDuesInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpGovtDuesInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpGovtDuesInfo.save($scope.hrEmpGovtDuesInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpGovtDuesInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrEmpGovtDuesInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpGovtDuesInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpGovtDuesInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpGovtDuesInfo, HrEmployeeInfo) {
                $scope.hrEmpGovtDuesInfo = entity;
                $scope.load = function (id) {
                    HrEmpGovtDuesInfo.get({id: id}, function(result) {
                        $scope.hrEmpGovtDuesInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpGovtDuesInfoUpdate', function(event, result) {
                    $scope.hrEmpGovtDuesInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpGovtDuesInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpGovtDuesInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'HrEmpGovtDuesInfo',
            function($scope, $modalInstance, entity, HrEmpGovtDuesInfo) {

                $scope.hrEmpGovtDuesInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpGovtDuesInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpGovtDuesInfo.deleted');
                        });
                };

            }]);
/*hrEmpGovtDuesInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpGovtDuesInfoProfileController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'HrEmpGovtDuesInfo', 'HrEmployeeInfo','User','Principal','DateUtils',
        function($scope, $rootScope, $stateParams, $state, HrEmpGovtDuesInfo, HrEmployeeInfo, User, Principal, DateUtils) {

            $scope.hrEmpGovtDuesInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                HrEmpGovtDuesInfo.get({id : id}, function(result) {
                    $scope.hrEmpGovtDuesInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadGovtDuesProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmpGovtDuesInfo.get({id: 'my'}, function (result)
                {
                    $scope.hrEmpGovtDuesInfo = result;
                    if($scope.hrEmpGovtDuesInfo== null)
                    {
                        $scope.addMode = true;
                        $scope.hrEmpGovtDuesInfo = $scope.initiateModel();
                        $scope.loadEmployee();
                    }
                    else{
                        $scope.hrEmployeeInfo = $scope.hrEmpGovtDuesInfo.employeeInfo;
                        $scope.hrEmpGovtDuesInfo.viewMode = true;
                        $scope.hrEmpGovtDuesInfo.viewModeText = "Edit";
                    }

                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.hrEmpGovtDuesInfo.viewMode = true;
                    $scope.hrEmpGovtDuesInfo.viewModeText = "Add";

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                    $scope.hrEmpGovtDuesInfo.viewMode = true;
                    $scope.hrEmpGovtDuesInfo.viewModeText = "Add";
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    console.log('===============fff================');
                    $scope.loadModel();
                    console.log('===============fff================');
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    description: null,
                    dueAmount: null,
                    claimerAuthority: null,
                    comments: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpGovtDuesInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpGovtDuesInfo.id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo)
            {
                $scope.isSaving = true;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if($scope.preCondition(modelInfo)){
                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmpGovtDuesInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmpGovtDuesInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                }}else{
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpGovtDuesInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpGovtDuesInfo.dueAmount', hrEmpGovtDuesInfo.dueAmount, 'number', 12, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpGovtDuesInfo.claimerAuthority',hrEmpGovtDuesInfo.claimerAuthority, 'text', 120, 2, 'atozAndAtoZ', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function() {

            };
        }]);
