'use strict';

angular.module('stepApp')
    .controller('HrEmpTransferInfoController',
    ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpTransferInfo', 'HrEmpTransferInfoSearch', 'ParseLinks','GetAllHrmEmployeeListByStatusLog','HrEmployeeInfoTransferHistory',
    function ($rootScope, $scope, $state, DataUtils, HrEmpTransferInfo, HrEmpTransferInfoSearch, ParseLinks,GetAllHrmEmployeeListByStatusLog,HrEmployeeInfoTransferHistory) {

        $scope.hrEmpTransferInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmpTransferInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
             HrEmpTransferInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                 $scope.links = ParseLinks.parse(headers('link'));
                 $scope.totalItems = headers('X-Total-Count');
                 $scope.hrEmpTransferInfos = result;
                 console.log($scope.hrEmpTransferInfos);
                 console.log("trans fer Infuuuuuuuu.");
             });

            /*
            GetAllHrmEmployeeListByStatusLog.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result) {
                $scope.hrEmpTransferInfos = result;

                $scope.viewTransferLog = function(empId)
                {console.log(empId);
                    HrEmployeeInfoTransferHistory.get({emplId: empId}, function (result)
                    {
                        $scope.hrEmployeeInfosss = result;

                        console.log($scope.hrEmployeeInfosss);
                        console.log("infosssssssss");
                    });
                };
            });
            */

        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpTransferInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpTransferInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpTransferInfo = {
                locationFrom: null,
                locationTo: null,
                designation: null,
                departmentFrom: null,
                departmentTo: null,
                fromDate: null,
                toDate: null,
                officeOrderNo: null,
                goDate: null,
                goDoc: null,
                goDocContentType: null,
                goDocName: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;
    }]);
/*hrEmpTransferInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpTransferInfoDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'HrEmployeeInfo','DataUtils', 'entity', 'HrEmpTransferInfo', 'InstCategoryByStatus','HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory','HrEmployeeInfoDesigLimitCheck','User','Principal','DateUtils','HrDepartmentSetupByStatus','HrDesignationSetupByType','HrEmpWorkAreaDtlInfoByStatus','GovernmentInstitutes','InstCategoryByStatusAndType','InstLevelByCategory','Institute','HrEmployeeInfosByInstitute',
        function($scope, $rootScope, $sce, $stateParams, $state, HrEmployeeInfo, DataUtils, entity, HrEmpTransferInfo, InstCategoryByStatus, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, HrEmployeeInfoDesigLimitCheck, User, Principal, DateUtils,HrDepartmentSetupByStatus,HrDesignationSetupByType,HrEmpWorkAreaDtlInfoByStatus,GovernmentInstitutes,InstCategoryByStatusAndType,InstLevelByCategory,Institute,HrEmployeeInfosByInstitute) {

            $scope.hrEmpTransferInfo = entity;
            $scope.load = function(id) {
                HrEmpTransferInfo.get({id : id}, function(result) {
                    $scope.hrEmpTransferInfo = result;
                });
            };
            $scope.instituteListAll = GovernmentInstitutes.query({size: 500});
            $scope.instituteList = $scope.instituteListAll;
            $scope.instituteListByLevel = $scope.instituteListAll;
            InstCategoryByStatusAndType.query({activeStatus: 1, instType: 'Government'}, function (result) {
                $scope.instCategories = result;
            });

            $scope.loadInstituteByCategory1 = function (categoryInfo) {
                $scope.instituteList = [];
                InstLevelByCategory.query({catId: categoryInfo.id}, function (result) {
                    $scope.instLevels = result;
                });
                angular.forEach($scope.instituteListAll, function (insitute) {
                    if (insitute.instCategory != null) {
                        if (categoryInfo.id == insitute.instCategory.id) {
                            $scope.instituteList.push(insitute);
                        }
                    }
                });
            };
            $scope.loadInstituteBylevel = function (levelInfo) {
                $scope.instituteListByLevel = [];
                angular.forEach($scope.instituteListAll, function (insitute) {
                    if (insitute.instLevel != null) {
                        if (levelInfo.id == insitute.instLevel.id) {
                            $scope.instituteListByLevel.push(insitute);
                        }
                    }
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();
            $scope.hremployeeinfos      = [];

            $scope.transferTypeList = MiscTypeSetupByCategory.get({cat:'TransferType',stat:'true'});
            $scope.workAreaList         = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.hrdepartmentsetups   = HrDepartmentSetupByStatus.get({stat:'true'});
            $scope.hrdepartmentsetupsFltr   = $scope.hrdepartmentsetups;
            $scope.hrdesignationsetups  = HrDesignationSetupByType.get({type:'HRM'});
            $scope.hrDesigSetupListFltr = $scope.hrdesignationsetups;
            $scope.workAreaDtlList      = HrEmpWorkAreaDtlInfoByStatus.get({stat:'true'});
            $scope.workAreaDtlListFiltr = $scope.workAreaDtlList;
            $scope.instCategoryList   = InstCategoryByStatus.get({stat:'true'});
            // $scope.instituteListAll     = GovernmentInstitutes.query({size:500});
            // $scope.instituteList        = $scope.instituteListAll;

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            //HrEmployeeOfInstitutes.get({}, function(result) {
            //    $scope.hremployeeinfosOfInstitute = result;
            //});
            $scope.getHrEmpByInstitute = function(instituteId){
                HrEmployeeInfosByInstitute.get({instid:instituteId},function(results){
                    $scope.hremployeeinfosOfInstitute = results;
                });
            }

            $scope.loadWorkAreaDetailByWork = function(workArea)
            {
                $scope.workAreaDtlListFiltr = [];
                console.log("WorkAreaOrOrg:"+JSON.stringify(workArea)+"");
                angular.forEach($scope.workAreaDtlList,function(workAreaDtl)
                {
                    if(workArea.id == workAreaDtl.workArea.id){
                        $scope.workAreaDtlListFiltr.push(workAreaDtl);
                    }
                });
            };

            $scope.loadDesigDeptByOrganization = function (orgnization)
            {
                $scope.hrDesigSetupListFltr = [];
                console.log("Total Desig: "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    if(desigInfo.organizationType=='Organization')
                    {
                        if(desigInfo.organizationInfo != null)
                        {
                            //console.log("orgId: "+desigInfo.organizationInfo.id + ", Srcorgid; "+orgnization.id);
                            if(orgnization.id === desigInfo.organizationInfo.id)
                            {
                                $scope.hrDesigSetupListFltr.push(desigInfo);
                            }
                        }
                    }
                });

                console.log("Total Section: "+$scope.hrdepartmentsetups.length);
                $scope.hrdepartmentsetupsFltr = [];
                angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                {
                    if(sectionInfo.organizationType=='Organization')
                    {
                        if(sectionInfo.organizationInfo != null)
                        {
                            //console.log("sectionId: "+sectionInfo.organizationInfo.id+", SrcsecId"+sectionInfo);
                            if(orgnization.id === sectionInfo.organizationInfo.id)
                            {
                                $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                            }
                        }
                    }
                });
            };

            $scope.loadInstituteByCategoryyy = function (categoryInfo)
             {
                console.log("loadInstituteByLevel: "+$scope.instituteListAll.length);
                $scope.instituteList = [];

                angular.forEach($scope.instituteListAll, function (insitute)
                {
                    if (insitute.instLevel != null)
                    {
                        //console.log("== instLevel: "+insitute.instLevel.id +", parentLev: "+categoryInfo.id);
                        if (categoryInfo.id == insitute.instLevel.id)
                        {
                            $scope.instituteList.push(insitute);
                        }
                    }
                });

                $scope.hrDesigSetupListFltr = [];
                console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                {
                    console.log("desigId: "+desigInfo.id);
                    if(desigInfo.organizationType=='Institute')
                    {
                        if(desigInfo.instLevel !=null && desigInfo.instLevel.id!= null && categoryInfo.id != null)
                        {
                            if(categoryInfo.id == desigInfo.instLevel.id){
                                //console.log("FoundDesig: DesigId: "+desigInfo.id);
                                $scope.hrDesigSetupListFltr.push(desigInfo);
                            }
                        }
                    }
                });

            };

            $scope.loadDesigDeptByInstitute = function (institute)
            {
                console.log("loadDesigDeptByInstitute => ");
                if(institute!=null && institute.id!=null)
                {
                    /*
                    console.log("FilterInstId: "+institute.id+", nmae: "+institute.name);
                    $scope.hrDesigSetupListFltr = [];
                    console.log("totalDeisg: : "+$scope.hrdesignationsetups.length);
                    angular.forEach($scope.hrdesignationsetups,function(desigInfo)
                    {
                        console.log("desigId: "+desigInfo.id);
                        if(desigInfo.organizationType=='Institute')
                        {
                            if(desigInfo.institute !=null && institute!= null && institute.id != null)
                            {
                                if(institute.id == desigInfo.institute.id){
                                    console.log("FoundDesig: DesigId: "+desigInfo.id);
                                    $scope.hrDesigSetupListFltr.push(desigInfo);
                                }
                            }

                        }
                    });*/

                    $scope.hrdepartmentsetupsFltr = [];
                    console.log("totalSection: "+$scope.hrdepartmentsetups.length);
                    angular.forEach($scope.hrdepartmentsetups,function(sectionInfo)
                    {
                        if(sectionInfo.organizationType=='Institute')
                        {
                            //console.log("sectionId1: "+sectionInfo.id);
                            if(sectionInfo.institute !=null && institute!= null && institute.id != null)
                            {

                                if(institute.id == sectionInfo.institute.id){
                                    //console.log("FoundSection: SecId: "+sectionInfo.id);
                                    $scope.hrdepartmentsetupsFltr.push(sectionInfo);
                                }
                            }

                        }
                    });
                }

            };

            $scope.noOfTotalEmployeeInDesig = 0;
            $scope.noOfEmployeeInDesig = 0;
            $scope.employeeDesigLimitAllowed = true;
            $scope.checkDesignationLimit = function(desigInfo)
            {
                var refId = 0;
                if($scope.hrEmpTransferInfo.toOrganizationType=='Organization')
                {
                    refId = $scope.hrEmpTransferInfo.toOrganization.id;
                }
                else{
                    refId = $scope.hrEmpTransferInfo.toInstitute.id;
                }
                if(desigInfo != null)
                {
                    console.log("Type: "+$scope.hrEmpTransferInfo.toOrganizationType+", desigId: "+desigInfo.id+", refId: "+refId);
                    HrEmployeeInfoDesigLimitCheck.get({orgtype:$scope.hrEmpTransferInfo.toOrganizationType, desigId: desigInfo.id, refid:refId}, function (result)
                    {
                        console.log("DesigResult: "+JSON.stringify(result));
                        $scope.employeeDesigLimitAllowed = result.isValid;
                        $scope.noOfTotalEmployeeInDesig = result.totalEmployee;
                        $scope.noOfEmployeeInDesig = desigInfo.elocattedPosition;
                        $scope.isSaving = !result.isValid;
                        //console.log("Total: "+result.totalEmployee+", DesigVal: "+desigInfo.elocattedPosition);
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.toDesignationInfo.$pending = false;
                    });
                }
            };

            $scope.selectedEmplId = 0; $scope.counter= 0;
            $scope.$watch('hrEmployeeInfo', function()
            {
                $scope.counter=$scope.counter+1;
                console.log("ngwatch - "+$scope.counter+", selected: "+$scope.selectedEmplId);
                if($scope.hrEmployeeInfo!=null && $scope.hrEmployeeInfo.id != $scope.selectedEmplId)
                {

                    console.log("From OrgType: "+$scope.hrEmployeeInfo.fullName+", employeeType: "+$scope.hrEmployeeInfo.employeeId);

                    HrEmployeeInfo.get({id : $scope.hrEmployeeInfo.id}, function(result)
                    {
                        $scope.selectedEmplId = result.id;

                        $scope.hrEmpTransferInfo.employeeInfo = result;

                        console.log(JSON.stringify($scope.hrEmpTransferInfo.employeeInfo));

                        $scope.hrEmpTransferInfo.fromDesignationInfo = $scope.hrEmpTransferInfo.employeeInfo.designationInfo;
                        $scope.hrEmpTransferInfo.fromDepartmentInfo = $scope.hrEmpTransferInfo.employeeInfo.departmentInfo;

                        $scope.hrEmpTransferInfo.fromOrganizationType = $scope.hrEmpTransferInfo.employeeInfo.organizationType;
                        $scope.hrEmpTransferInfo.fromEmployeeType = $scope.hrEmpTransferInfo.employeeInfo.employeeType;

                        if($scope.hrEmpTransferInfo.employeeInfo.organizationType=='Institute')
                        {
                            $scope.hrEmpTransferInfo.fromInstCategory = $scope.hrEmpTransferInfo.employeeInfo.instCategory;
                            $scope.hrEmpTransferInfo.fromInstitute = $scope.hrEmpTransferInfo.employeeInfo.institute;
                        }
                        else if($scope.hrEmpTransferInfo.employeeInfo.organizationType=='Organization')
                        {
                            $scope.hrEmpTransferInfo.fromOrgCategory = $scope.hrEmpTransferInfo.employeeInfo.workArea;
                            $scope.hrEmpTransferInfo.fromOrganization = $scope.hrEmpTransferInfo.employeeInfo.workAreaDtl;
                        }

                        console.log("From last: "+$scope.hrEmpTransferInfo.fromOrganizationType+", employeeType: "+$scope.hrEmpTransferInfo.fromEmployeeType);
                        $scope.hrEmployeeInfo = result;
                    });
                }
            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTransferInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpTransferInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpTransferInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpTransferInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpTransferInfo.id != null)
                {
                    $scope.hrEmpTransferInfo.logId = 0;
                    $scope.hrEmpTransferInfo.logStatus = 6;
                    HrEmpTransferInfo.update($scope.hrEmpTransferInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpTransferInfo.updated');
                }
                else
                {
                    $scope.hrEmpTransferInfo.logId = 0;
                    $scope.hrEmpTransferInfo.logStatus = 6;
                    $scope.hrEmpTransferInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpTransferInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpTransferInfo.save($scope.hrEmpTransferInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpTransferInfo.created');
                }
            };

            $scope.clear = function() {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpTransferInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            hrEmpTransferInfo.releaseLetterDoc = base64Data;
                            hrEmpTransferInfo.releaseLetterContentType = $file.type;
                            if (hrEmpTransferInfo.releaseLetterDocName == null)
                            {
                                hrEmpTransferInfo.releaseLetterDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewImage = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.releaseLetterDoc, modelInfo.releaseLetterContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.releaseLetterContentType;
                $rootScope.viewerObject.pageTitle = "Employee Transfer Release Letter Document";
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpTransferInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpTransferInfoDetailController',
        ['$scope', '$rootScope','$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpTransferInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpTransferInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEmpTransferInfo = entity;
                $scope.load = function (id) {
                    HrEmpTransferInfo.get({id: id}, function(result) {
                        $scope.hrEmpTransferInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpTransferInfoUpdate', function(event, result) {
                    $scope.hrEmpTransferInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewImage = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.releaseLetterDoc, modelInfo.releaseLetterContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.releaseLetterContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Transfer Release Letter";
                    $rootScope.showPreviewModal();
                };
            }]);
/*hrEmpTransferInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpTransferInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpTransferInfo',
            function($scope, $rootScope, $modalInstance, entity, HrEmpTransferInfo) {

                $scope.hrEmpTransferInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpTransferInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpTransferInfo.deleted');
                        });
                };

            }]);
/*hrEmpTransferInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpTransferInfoProfileController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpTransferInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory','User','Principal','DateUtils','HrEmployeeInfoTransferHistory','HrEmpTransferInfoReleaseDataUpdate','HrEmploymentInfo',
        function($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpTransferInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils,HrEmployeeInfoTransferHistory,HrEmpTransferInfoReleaseDataUpdate,HrEmploymentInfo) {

            $scope.hrEmpTransferInfo = {};
            $scope.hrEmployeeInfos = [];
            //$scope.hremployeeinfos = HrEmployeeInfo.query();
            //$scope.transferTypeList = MiscTypeSetupByCategory.get({cat:'TransferType',stat:'true'});
            //$scope.jobCategoryList = MiscTypeSetupByCategory.get({cat:'JobCategory',stat:'true'});
            $scope.load = function(id) {
                HrEmpTransferInfo.get({id : id}, function(result) {
                    $scope.hrEmpTransferInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            var onSaveReleaseSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTransferInfoUpdate', result);
                $scope.isSaving = false;
                $('#employeeJobDocUpdateForm').modal('hide');
                $scope.responseMsg = result;
                $scope.msg = 'Successfully saved, Please wait for your department/section head approval'
            };

            var onSaveReleaseError = function (result) {
                $scope.isSaving = false;
                $scope.responseMsg = result;
            };

            $scope.viewEmployeeJobDocUpdateForm = function (hrEmpTransferInfo)
            {
                $scope.hrEmpTransferInfo = hrEmpTransferInfo;
                $('#employeeJobDocUpdateForm').modal('show');
            };

            $scope.jobDocUpdate = function()
            {

                if (confirm("Are you sure to update release data ?") == true)
                {
                    HrEmpTransferInfoReleaseDataUpdate.update($scope.hrEmpTransferInfo, onSaveReleaseSuccess, onSaveReleaseError);
                }
            };

            $scope.previewReleaseLetterDoc = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.releaseLetterDoc, modelInfo.releaseLetterContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.releaseLetterContentType;
                $rootScope.viewerObject.pageTitle = "Release Letter Application Document : "+modelInfo.fullName;

                $rootScope.showPreviewModal();
            };

            $scope.setReleaseLetterDoc = function ($file, modelInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            modelInfo.releaseLetterDoc = base64Data;
                            modelInfo.releaseLetterContentType = $file.type;
                            if (modelInfo.releaseLetterDocName == null)
                            {
                                modelInfo.releaseLetterDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.loadEmploymentModel = function()
            {
                HrEmploymentInfo.query({id: 'my'}, function (result)
                {
                    //console.log("result: "+JSON.stringify(result));
                    if(result.length > 0)
                    {
                        $scope.hrEmploymentInfo = result[0];
                    }
                }, function (response)
                {

                })
            };

            $scope.loadModel = function()
            {
                HrEmpTransferInfo.query({id: 'my'}, function (result)
                {

                    $scope.hrEmpTransferInfoList = result;
                    if($scope.hrEmpTransferInfoList.length < 1)
                    {
                        $scope.addMode = true;
                        $scope.loadEmployee();
                    }
                    else
                    {
                        $scope.hrEmployeeInfo = $scope.hrEmpTransferInfoList[0].employeeInfo;
                    }
                }, function (response)
                {

                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };
            $scope.loadModel();
            $scope.loadEmploymentModel();

            $scope.loadTransferHistory = function()
            {
                HrEmployeeInfoTransferHistory.get({emplId: $scope.hrEmployeeInfo.id}, function (result)
                {
                    $scope.hrEmployeeInfos = result;
                }, function (response)
                {

                    $scope.hasProfile = false;
                    $scope.addMode = false;
                })
            };

            $scope.loadEmployee = function ()
            {
                HrEmployeeInfo.get({id: 'my'}, function (result)
                {
                    $scope.hrEmployeeInfo = result;
                    $scope.loadTransferHistory();
                }, function (response) {
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            //$scope.loadEmployee();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                    {
                        if($scope.hrEmpTransferInfoList.length > 1)
                        {
                            var indx = $scope.hrEmpTransferInfoList.indexOf(modelInfo);
                            $scope.hrEmpTransferInfoList.splice(indx, 1);
                        }
                        else
                        {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function()
            {
                $scope.hrEmpTransferInfoList.push(
                    {
                        viewMode:false,
                        viewModeText:'Cancel',
                        locationFrom: null,
                        locationTo: null,
                        designation: null,
                        departmentFrom: null,
                        departmentTo: null,
                        fromDate: null,
                        toDate: null,
                        officeOrderNo: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        logId:null,
                        logStatus:null,
                        logComments:null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    locationFrom: null,
                    locationTo: null,
                    designation: null,
                    departmentFrom: null,
                    departmentTo: null,
                    fromDate: null,
                    toDate: null,
                    officeOrderNo: null,
                    goDate: null,
                    goDoc: null,
                    goDocContentType: null,
                    goDocName: null,
                    logId: null,
                    logStatus: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpTransferInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpTransferInfoList[$scope.selectedIndex].id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index)
            {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmpTransferInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmpTransferInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
            };

            $scope.clear = function() {

            };

            $scope.previewImage = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.releaseLetterDoc, modelInfo.releaseLetterContentType);
                //$rootScope.viewerObject.contentUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.releaseLetterContentType;
                $rootScope.viewerObject.pageTitle = "Employee Transfer Release Document";
                // call the modal
                $rootScope.showPreviewModal();
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpTransferInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            hrEmpTransferInfo.releaseLetterDoc = base64Data;
                            hrEmpTransferInfo.releaseLetterContentType = $file.type;
                            if (hrEmpTransferInfo.releaseLetterDocName == null)
                            {
                                hrEmpTransferInfo.releaseLetterDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);

angular.module('stepApp')
    .controller('HrEmpTransferInfoDepartmentHeadApprovalController',
        ['$scope', '$rootScope', '$sce', 'HrEmpTransferInfo','HrEmpTransferInfoApprovalPendingList','HrEmpTransferInfoReleaseDataApprove',
            function($scope, $rootScope, $sce, HrEmpTransferInfo,HrEmpTransferInfoApprovalPendingList,HrEmpTransferInfoReleaseDataApprove) {

                $scope.hrEmpTransferInfos = [];
                $scope.approvalObject = {};

                $scope.hrEmpTransferInfo = {};
                $scope.loadApprovalList = function ()
                {
                    $scope.hrEmpTransferInfos= [];
                    HrEmpTransferInfoApprovalPendingList.get(function(result)
                    {
                        //console.log(JSON.stringify(result));
                        $scope.approvalObject = result;
                        if($scope.approvalObject.isHead==true || $scope.approvalObject.isHead=='true')
                        {
                            $scope.hrEmpTransferInfos = $scope.approvalObject.pendingList;
                        }

                    });
                };

                $scope.loadApprovalList();

                $scope.clear = function() {
                    $('#transferReleaseDocumentApprovalForm').modal('hide');
                };

                $scope.viewTransferReleaseDocumentApprovalForm = function (hrEmpTransferInfo)
                {
                    $scope.hrEmpTransferInfo = hrEmpTransferInfo;
                    $('#transferReleaseDocumentApprovalForm').modal('show');
                };

                $scope.approveReleaseDocument = function()
                {
                    console.log("release document approval: "+$scope.hrEmpTransferInfo.jobReleaseDate);

                    if (confirm("Are you sure to approve release data ?") == true)
                    {
                        $scope.hrEmpTransferInfo.logComments = "APPROVED";
                        HrEmpTransferInfoReleaseDataApprove.update($scope.hrEmpTransferInfo, onSaveReleaseSuccess, onSaveReleaseError);
                    }
                };

                $scope.rejectReleaseDocument = function()
                {
                    console.log("release document rejection: "+$scope.hrEmpTransferInfo.jobReleaseDate);

                    if (confirm("Are you sure to reject release data ?") == true)
                    {
                        $scope.hrEmpTransferInfo.logComments = "Application Rejected from department head";
                        HrEmpTransferInfoReleaseDataApprove.update($scope.hrEmpTransferInfo, onSaveReleaseSuccess, onSaveReleaseError);
                    }
                };

                var onSaveReleaseSuccess = function (result) {
                    $scope.$emit('stepApp:hrEmpTransferInfoUpdate', result);
                    $scope.isSaving = false;
                    $('#transferReleaseDocumentApprovalForm').modal('hide');
                    $scope.responseMsg = result;
                    $scope.loadApprovalList();
                };

                var onSaveReleaseError = function (result) {
                    $scope.isSaving = false;
                    $scope.responseMsg = result;
                };

                $scope.previewReleaseLetterDoc = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.releaseLetterDoc, modelInfo.releaseLetterContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.releaseLetterContentType;
                    $rootScope.viewerObject.pageTitle = "Release Letter Application Document : "+modelInfo.fullName;

                    $rootScope.showPreviewModal();
                };

            }]);

angular.module('stepApp')
    .controller('HrEmpTransferInfoInstituteHeadApprovalController',
        ['$scope', '$rootScope', '$sce', 'HrEmpTransferInfo','HrEmpTransferInfoInstHeadApprovalPendingList','HrEmpTransferInfoReleaseDataApprove',
            function($scope, $rootScope, $sce, HrEmpTransferInfo,HrEmpTransferInfoInstHeadApprovalPendingList,HrEmpTransferInfoReleaseDataApprove) {

                $scope.hrEmpTransferInfos = [];
                $scope.approvalObject = {};

                $scope.hrEmpTransferInfo = {};
                $scope.loadApprovalList = function ()
                {
                    /*HrEmpTransferInfoInstHeadApprovalPendingList.get(function(result)
                    {
                        console.log(JSON.stringify(result));
                        $scope.hrEmpTransferInfos = result;
                    });*/

                    $scope.hrEmpTransferInfos = HrEmpTransferInfoInstHeadApprovalPendingList.get();
                };

                $scope.loadApprovalList();

                $scope.clear = function() {
                    $('#transferReleaseDocumentApprovalForm').modal('hide');
                };

                $scope.viewTransferReleaseDocumentApprovalForm = function (hrEmpTransferInfo)
                {
                    $scope.hrEmpTransferInfo = hrEmpTransferInfo;
                    $('#transferReleaseDocumentApprovalForm').modal('show');
                };

                $scope.approveReleaseDocument = function()
                {
                    console.log("release document approval: "+$scope.hrEmpTransferInfo.jobReleaseDate);

                    if (confirm("Are you sure to approve release data ?") == true)
                    {
                        $scope.hrEmpTransferInfo.logComments = "APPROVED";
                        HrEmpTransferInfoReleaseDataApprove.update($scope.hrEmpTransferInfo, onSaveReleaseSuccess, onSaveReleaseError);
                    }
                };

                $scope.rejectReleaseDocument = function()
                {
                    console.log("release document rejection: "+$scope.hrEmpTransferInfo.jobReleaseDate);

                    if (confirm("Are you sure to reject release data ?") == true)
                    {
                        $scope.hrEmpTransferInfo.logComments = "Application Rejected from institute admin";
                        HrEmpTransferInfoReleaseDataApprove.update($scope.hrEmpTransferInfo, onSaveReleaseSuccess, onSaveReleaseError);
                    }
                };

                var onSaveReleaseSuccess = function (result) {
                    $scope.$emit('stepApp:hrEmpTransferInfoUpdate', result);
                    $scope.isSaving = false;
                    $('#transferReleaseDocumentApprovalForm').modal('hide');
                    $scope.responseMsg = result;
                    $scope.hrEmpTransferInfos = [];
                    $scope.hrEmpTransferInfos = HrEmpTransferInfoInstHeadApprovalPendingList.get();
                };

                var onSaveReleaseError = function (result) {
                    $scope.isSaving = false;
                    $scope.responseMsg = result;
                };

                $scope.previewReleaseLetterDoc = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.releaseLetterDoc, modelInfo.releaseLetterContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.releaseLetterContentType;
                    $rootScope.viewerObject.pageTitle = "Release Letter Application Document : "+modelInfo.fullName;

                    $rootScope.showPreviewModal();
                };

            }]);
