'use strict';

angular.module('stepApp')
    .controller('HrDesignationHeadSetupController', function ($scope, $state, HrDesignationHeadSetup, HrDesignationHeadSetupSearch, ParseLinks) {

        $scope.hrDesignationHeadSetups = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function () {
            HrDesignationHeadSetup.query({
                page: $scope.page - 1,
                size: 500,
                sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
            }, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrDesignationHeadSetups = result;
            });
        };
        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrDesignationHeadSetupSearch.query({query: $scope.searchQuery}, function (result) {
                $scope.hrDesignationHeadSetups = result;
            }, function (response) {
                if (response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrDesignationHeadSetup = {
                designationCode: null,
                designationName: null,
                designationDetail: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*hrDesignationHeadSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrDesignationHeadSetupDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrDesignationHeadSetup', 'Principal', 'User', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, entity, HrDesignationHeadSetup, Principal, User, DateUtils) {

            $scope.hrDesignationHeadSetup = entity;
            $scope.load = function (id) {
                HrDesignationHeadSetup.get({id: id}, function (result) {
                    $scope.hrDesignationHeadSetup = result;
                });
            };

            $scope.levelList = $rootScope.generateYearList(0, 15);

            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                        console.log("loggedInUser: " + JSON.stringify($scope.loggedInUser));
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrDesignationHeadSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrDesignationHeadSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrDesignationHeadSetup.updateBy = $scope.loggedInUser.id;
                $scope.hrDesignationHeadSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.preCondition()) {
                    if ($scope.hrDesignationHeadSetup.id != null) {
                        HrDesignationHeadSetup.update($scope.hrDesignationHeadSetup, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.hrDesignationHeadSetup.updated');
                    } else {
                        $scope.hrDesignationHeadSetup.createBy = $scope.loggedInUser.id;
                        $scope.hrDesignationHeadSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrDesignationHeadSetup.save($scope.hrDesignationHeadSetup, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.hrDesignationHeadSetup.created');
                    }
                } else {
                    $rootScope.setErrorMessage("Please enter correct information");
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrDesignationHeadSetup.designationName', $scope.hrDesignationHeadSetup.designationName, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {
            };
        }]);
/*hrDesignationHeadSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrDesignationHeadSetupDetailController', function ($scope, $rootScope, $stateParams, entity, HrDesignationHeadSetup) {
        $scope.hrDesignationHeadSetup = entity;
        $scope.load = function (id) {
            HrDesignationHeadSetup.get({id: id}, function (result) {
                $scope.hrDesignationHeadSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:hrDesignationHeadSetupUpdate', function (event, result) {
            $scope.hrDesignationHeadSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*hrDesignationHeadSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrDesignationHeadSetupDeleteController', function ($scope, $rootScope, $modalInstance, entity, HrDesignationHeadSetup) {

        $scope.hrDesignationHeadSetup = entity;
        $scope.clear = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            HrDesignationHeadSetup.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
            $rootScope.setErrorMessage('stepApp.hrDesignationHeadSetup.deleted');
        };

    });
