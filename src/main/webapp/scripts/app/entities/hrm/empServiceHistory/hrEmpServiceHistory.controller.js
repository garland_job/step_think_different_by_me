'use strict';

angular.module('stepApp')
    .controller('HrEmpServiceHistoryController',
    ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEmpServiceHistory','HrEmpServiceHistorySearch', 'ParseLinks',
    function ($rootScope, $scope, $state, DataUtils, HrEmpServiceHistory, HrEmpServiceHistorySearch, ParseLinks) {

        $scope.hrEmpServiceHistorys = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmpServiceHistory";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEmpServiceHistory.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmpServiceHistorys = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmpServiceHistorySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmpServiceHistorys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmpServiceHistory = {
                serviceDate: null,
                gazettedDate: null,
                encadrementDate: null,
                nationalSeniority: null,
                cadreNumber: null,
                goDate: null,
                goDoc: null,
                goDocContentType: null,
                goDocName: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;
    }]);
/*hrEmpServiceHistory-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpServiceHistoryDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'HrEmpServiceHistory', 'HrEmployeeInfoByWorkArea','User','Principal','DateUtils','MiscTypeSetupByCategory','HrEmployeeOfInstitutes','HrEmployeeInfoByEmployeeId',
        function($scope, $rootScope, $sce, $stateParams, $state, DataUtils, entity, HrEmpServiceHistory, HrEmployeeInfoByWorkArea, User, Principal, DateUtils, MiscTypeSetupByCategory,HrEmployeeOfInstitutes,HrEmployeeInfoByEmployeeId)
        {

            $scope.hrEmpServiceHistory = entity;
            $scope.currentDate=new Date();
            if($stateParams.id !=null){
                HrEmpServiceHistory.get({id : $stateParams.id},function(result){
                    $scope.hrEmpServiceHistory = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.load = function(id) {
                HrEmpServiceHistory.get({id : id}, function(result) {
                    $scope.hrEmpServiceHistory = result;
                });
            };

            $scope.hremployeeinfos  = [];
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loadHrEmployee = function(serviceHistory)
            {
                console.log("load hr emp");
                console.log(serviceHistory);
                HrEmployeeInfoByEmployeeId.get({id: serviceHistory.employeeInfo.employeeId}, function (result){
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpServiceHistory.employeeInfo = result;


                });

            }



                $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpServiceHistoryUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpServiceHistory');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmpServiceHistory.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpServiceHistory.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpServiceHistory.id != null)
                {
                    $scope.hrEmpServiceHistory.logId = 0;
                    $scope.hrEmpServiceHistory.logStatus = 6;
                    HrEmpServiceHistory.update($scope.hrEmpServiceHistory, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpServiceHistory.updated');
                }
                else
                {
                    $scope.hrEmpServiceHistory.logId = 0;
                    $scope.hrEmpServiceHistory.logStatus = 6;
                    $scope.hrEmpServiceHistory.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpServiceHistory.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpServiceHistory.save($scope.hrEmpServiceHistory, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpServiceHistory.created');
                }
            };

            $scope.clear = function() {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpServiceHistory)
            {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            hrEmpServiceHistory.goDoc = base64Data;
                            hrEmpServiceHistory.goDocContentType = $file.type;
                            if (hrEmpServiceHistory.goDocName == null)
                            {
                                hrEmpServiceHistory.goDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewImage = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Service History Document";
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEmpServiceHistory-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpServiceHistoryDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'HrEmpServiceHistory', 'HrEmployeeInfo',
            function ($scope, $rootScope, $sce, $stateParams, DataUtils, entity, HrEmpServiceHistory, HrEmployeeInfo) {
                $scope.hrEmpServiceHistory = entity;
                $scope.load = function (id) {
                    HrEmpServiceHistory.get({id: id}, function(result) {
                        $scope.hrEmpServiceHistory = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpServiceHistoryUpdate', function(event, result) {
                    $scope.hrEmpServiceHistory = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.previewImage = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                    $rootScope.viewerObject.pageTitle = "Employee Service History Document";
                    $rootScope.showPreviewModal();
                };
            }]);
/*hrEmpServiceHistory-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpServiceHistoryDeleteController',
        ['$scope', '$rootScope', '$uibModalInstance', 'entity', 'HrEmpServiceHistory',
            function($scope, $rootScope, $uibModalInstance, entity, HrEmpServiceHistory) {

                $scope.hrEmpServiceHistory = entity;
                $scope.clear = function() {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpServiceHistory.delete({id: id},
                        function () {
                            $uibModalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpServiceHistory.deleted');
                };

            }]);
/*hrEmpServiceHistory-profile.controller.js*/

angular.module('stepApp').controller('HrEmpServiceHistoryProfileController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', 'DataUtils', 'HrEmpServiceHistory', 'HrEmployeeInfo','User','Principal','DateUtils',
        function($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEmpServiceHistory, HrEmployeeInfo, User, Principal, DateUtils)
        {

            $scope.hrEmpServiceHistory = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                HrEmpServiceHistory.get({id : id}, function(result) {
                    $scope.hrEmpServiceHistory = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadHistoryProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmpServiceHistory.query({id: 'my'}, function (result)
                {
                    console.log("result:, len: "+result.length);
                    $scope.hrEmpServiceHistoryList = result;
                    if($scope.hrEmpServiceHistoryList.length < 1)
                    {
                        $scope.addMode = true;
                        $scope.hrEmpServiceHistoryList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else
                    {
                        $scope.hrEmployeeInfo = $scope.hrEmpServiceHistoryList[0].employeeInfo;
                        angular.forEach($scope.hrEmpServiceHistoryList,function(modelInfo)
                        {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if(modelInfo.logStatus==0)
                            {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                    {
                        if($scope.hrEmpServiceHistoryList.length > 1)
                        {
                            var indx = $scope.hrEmpServiceHistoryList.indexOf(modelInfo);
                            $scope.hrEmpServiceHistoryList.splice(indx, 1);
                        }
                        else
                        {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function()
            {
                $scope.hrEmpServiceHistoryList.push(
                    {
                        viewMode:false,
                        viewModeText:'Cancel',
                        serviceDate: null,
                        gazettedDate: null,
                        encadrementDate: null,
                        nationalSeniority: null,
                        cadreNumber: null,
                        goDate: null,
                        goDoc: null,
                        goDocContentType: null,
                        goDocName: null,
                        logId:null,
                        logStatus:null,
                        logComments:null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    serviceDate: null,
                    gazettedDate: null,
                    encadrementDate: null,
                    nationalSeniority: null,
                    cadreNumber: null,
                    goDate: null,
                    goDoc: null,
                    goDocContentType: null,
                    goDocName: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };


            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpServiceHistoryUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpServiceHistoryList[$scope.selectedIndex].id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index)
            {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmpServiceHistory.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmpServiceHistory.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
            };

            $scope.previewImage = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.goDoc, modelInfo.goDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                //$rootScope.viewerObject.contentUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = modelInfo.goDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Service History Document";
                $rootScope.showPreviewModal();
            };

            $scope.clear = function() {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setGoDoc = function ($file, hrEmpServiceHistory)
            {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            hrEmpServiceHistory.goDoc = base64Data;
                            hrEmpServiceHistory.goDocContentType = $file.type;
                            if (hrEmpServiceHistory.goDocName == null)
                            {
                                hrEmpServiceHistory.goDocName = $file.name;
                            }
                        });
                    };
                }
            };
        }]);

