'use strict';

angular.module('stepApp')
    .controller('HrEmpWorkAreaDtlInfoController',
        ['$scope', '$state', 'HrEmpWorkAreaDtlInfo', 'HrEmpWorkAreaDtlInfoSearch', 'ParseLinks',
            function ($scope, $state, HrEmpWorkAreaDtlInfo, HrEmpWorkAreaDtlInfoSearch, ParseLinks) {

                $scope.hrEmpWorkAreaDtlInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 1;
                $scope.loadAll = function () {
                    HrEmpWorkAreaDtlInfo.query({
                        page: $scope.page - 1,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpWorkAreaDtlInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpWorkAreaDtlInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpWorkAreaDtlInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpWorkAreaDtlInfo = {
                        name: null,
                        establishmentDate: null,
                        contactNumber: null,
                        address: null,
                        telephoneNumber: null,
                        faxNumber: null,
                        emailAddress: null,
                        activeStatus: false,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpWorkAreaDtlInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpWorkAreaDtlInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', '$state', 'HrEmpWorkAreaDtlInfo', 'MiscTypeSetupByCategory', 'Division', 'District', 'Upazila', 'User', 'Principal', 'DateUtils', '$translate', '$timeout', 'Auth',
        function ($scope, $rootScope, $stateParams, entity, $state, HrEmpWorkAreaDtlInfo, MiscTypeSetupByCategory, Division, District, Upazila, User, Principal, DateUtils, $translate, $timeout, Auth) {

            $scope.hrEmpWorkAreaDtlInfo = entity;
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.divisions = Division.query();
            $scope.districtList = District.query({size: 500});
            $scope.districtListFilter = $scope.districtList;
            $scope.upazilaList = Upazila.query({size: 500});
            $scope.upazilaListFilter = $scope.upazilaList;
            $scope.registerAccount = {};

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };

            $scope.loadDistrictByDivision = function (divisionObj) {
                $scope.districtListFilter = [];
                angular.forEach($scope.districtList, function (districtObj) {
                    if (divisionObj.id == districtObj.division.id) {
                        $scope.districtListFilter.push(districtObj);
                    }
                });
            };

            $scope.loadUpazilaByDistrict = function (districtObj) {
                $scope.upazilaListFilter = [];
                angular.forEach($scope.upazilaList, function (upazilaObj) {
                    if (districtObj.id == upazilaObj.district.id) {
                        $scope.upazilaListFilter.push(upazilaObj);
                    }
                });
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.getLoggedInUser();

            $scope.load = function (id) {
                HrEmpWorkAreaDtlInfo.get({id: id}, function (result) {
                    $scope.hrEmpWorkAreaDtlInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpWorkAreaDtlInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpWorkAreaDtlInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.editOrgazationDetail = function () {
                $scope.errorMsg = "";
                $scope.isSaving = true;
                $scope.hrEmpWorkAreaDtlInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpWorkAreaDtlInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrEmpWorkAreaDtlInfo.id != null) {
                    HrEmpWorkAreaDtlInfo.update($scope.hrEmpWorkAreaDtlInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpWorkAreaDtlInfo.updated');
                } else {
                    $scope.hrEmpWorkAreaDtlInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpWorkAreaDtlInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpWorkAreaDtlInfo.save($scope.hrEmpWorkAreaDtlInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpWorkAreaDtlInfo.created');
                }
            };

            $scope.errorMsg = "";
            $scope.saveOrganizationInfo = function () {
                console.log("login: " + $scope.registerAccount.login);
                User.get({login: $scope.registerAccount.login}, function (usrResult) {
                    $scope.isSaving = true;
                    $scope.hrEmpWorkAreaDtlInfo.emailAddress = $scope.registerAccount.email;
                    $scope.regiUser = usrResult;
                    $scope.regiUser.password = $scope.registerAccount.password;
                    $scope.hrEmpWorkAreaDtlInfo.updateBy = $scope.loggedInUser.id;
                    $scope.hrEmpWorkAreaDtlInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                    console.log("registerdUserInner: " + JSON.stringify($scope.regiUser) + "\n, email: " + $scope.hrEmpWorkAreaDtlInfo.emailAddress);
                    if ($scope.preCondition()) {

                        if ($scope.hrEmpWorkAreaDtlInfo.id != null) {
                            $scope.hrEmpWorkAreaDtlInfo.logId = 0;
                            $scope.hrEmpWorkAreaDtlInfo.logStatus = 1;
                            HrEmpWorkAreaDtlInfo.update($scope.hrEmpWorkAreaDtlInfo, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrEmpWorkAreaDtlInfo.updated');
                        }
                        else {
                            $scope.hrEmpWorkAreaDtlInfo.logId = 0;
                            $scope.hrEmpWorkAreaDtlInfo.logStatus = 1;
                            $scope.hrEmpWorkAreaDtlInfo.user = $scope.regiUser;
                            $scope.hrEmpWorkAreaDtlInfo.user.password = $scope.registerAccount.password;
                            $scope.hrEmpWorkAreaDtlInfo.createBy = $scope.loggedInUser.id;
                            $scope.hrEmpWorkAreaDtlInfo.logComments = $scope.registerAccount.password;
                            $scope.hrEmpWorkAreaDtlInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrEmpWorkAreaDtlInfo.save($scope.hrEmpWorkAreaDtlInfo, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrEmpWorkAreaDtlInfo.created');
                        }
                    } else {
                        $rootScope.setErrorMessage("Please enter correct information");
                    }
                });
            };

            $scope.registerOrganizationUser = function () {
                if ($scope.hrEmpWorkAreaDtlInfo.id != null) {
                    $scope.editOrgazationDetail();
                }
                else {
                    $scope.errorMsg = "";
                    $scope.registerAccount.langKey = $translate.use();
                    $scope.doNotMatch = null;
                    $scope.error = null;
                    $scope.errorUserExists = null;
                    $scope.errorEmailExists = null;
                    $scope.registerAccount.authorities = ["ROLE_ORG_USER"];
                    $scope.registerAccount.activated = true;
                    $scope.registerAccount.firstName = $scope.hrEmpWorkAreaDtlInfo.name;
                    $scope.hrEmpWorkAreaDtlInfo.emailAddress = $scope.registerAccount.email;
                    console.log("FirstName; " + $scope.registerAccount.firstName + ", login: " + $scope.registerAccount.login);

                    Auth.createOrganizationAccount($scope.registerAccount).then(function () {
                        $scope.success = 'OK';
                        $scope.errorMsg = "";
                        console.log("User Login Success...");
                        $scope.saveOrganizationInfo(); // call employee saving
                    }).catch(function (response) {
                        $scope.success = null;
                        if (response.status === 400 && response.data === 'LOGIN_ALREADY_IN_USE') {
                            $scope.errorUserExists = 'ERROR';
                            $scope.errorMsg = "Login already in user";
                        } else if (response.status === 400 && response.data === 'EMAIL_ALREADY_IN_USE') {
                            $scope.errorEmailExists = 'ERROR';
                            $scope.errorMsg = "Email already in user";
                        } else {
                            $scope.error = 'ERROR';
                        }
                    });
                }

            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpWorkAreaDtlInfo.name', $scope.hrEmpWorkAreaDtlInfo.name, 'text', 50, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpWorkAreaDtlInfo.contactNumber', $scope.hrEmpWorkAreaDtlInfo.contactNumber, 'number', 18, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpWorkAreaDtlInfo.telephoneNumber', $scope.hrEmpWorkAreaDtlInfo.telephoneNumber, 'number', 18, 8, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpWorkAreaDtlInfo.faxNumber', $scope.hrEmpWorkAreaDtlInfo.faxNumber, 'number', 18, 8, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpWorkAreaDtlInfo.emailAddress', $scope.hrEmpWorkAreaDtlInfo.emailAddress, 'email', 100, 5, 'email', true, '', ''));
                /*        $scope.preConditionStatus.push($rootScope.layerDataCheck('registerAccount.email', $scope.registerAccount.email, 'email', 100, 5, 'email', true, '', ''));
                 $scope.preConditionStatus.push($rootScope.layerDataCheck('registerAccount.login', $scope.registerAccount.login, 'text', 100, 2, 'atozAndAtoZ0-9', true, '', ''));
                 */
                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {

            };

            $scope.toDay = new Date();
            $scope.registerAccount.login="";

        }]);
/*hrEmpWorkAreaDtlInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpWorkAreaDtlInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpWorkAreaDtlInfo', 'MiscTypeSetup', 'Division', 'District', 'Upazila',
            function ($scope, $rootScope, $stateParams, entity, HrEmpWorkAreaDtlInfo, MiscTypeSetup, Division, District, Upazila) {
                $scope.hrEmpWorkAreaDtlInfo = entity;
                $scope.load = function (id) {
                    HrEmpWorkAreaDtlInfo.get({id: id}, function (result) {
                        $scope.hrEmpWorkAreaDtlInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpWorkAreaDtlInfoUpdate', function (event, result) {
                    $scope.hrEmpWorkAreaDtlInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpWorkAreaDtlInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpWorkAreaDtlInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpWorkAreaDtlInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpWorkAreaDtlInfo) {

                $scope.hrEmpWorkAreaDtlInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpWorkAreaDtlInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpWorkAreaDtlInfo.deleted');
                        });
                };

            }]);
