'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('hrm-welcome', {
                parent: 'entity',
                url: '/hrmhome',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/hrm/hrm-welcome.html',
                        controller: 'ContentUploadController'
                    },

                    'hrmView@hrm': {
                        templateUrl: 'scripts/app/entities/contentUpload/contentUpload-detail.html',
                        controller: 'ContentUploadDetailController'
                    }

                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('hrm-Notice',{
                parent: 'hrm-welcome',
                url: '/notice-detail/{id}',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/hrm/hrm-Notice.html',
                        controller: 'ContentUploadController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('contentUpload');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ContentUpload', function($stateParams, ContentUpload) {
                        return ContentUpload.get({id : $stateParams.id});
                    }]
                }
            })

            .state('hrm', {
                parent: 'entity',
                url: '/hrm',
                data: {
                    authorities: ['ROLE_HRM_ADMIN','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE','ROLE_ADMIN'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'HRM'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/hrm/hrm.html',
                        controller: 'hrmWelcomeHomeController'
                    },
                   'hrmManagementView@hrm':{
                        templateUrl: 'scripts/app/entities/hrm/hrm-dashboard.html',
                        controller: 'hrmHomeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('hrEmpChildrenInfo');
                        $translatePartialLoader.addPart('hrEmpTransferApplInfo');
                        $translatePartialLoader.addPart('gender');
                        return $translate.refresh();
                    }]
                }
            })

            .state('orghome', {
                parent: 'entity',
                url: '/orghome',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_ORG_USER'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'Organization'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/hrm/org-home.html',
                        controller: 'hrmWelcomeHomeController'
                    },
                   'hrmOrgManagementView@orghome':{
                        templateUrl: 'scripts/app/entities/hrm/hrm-dashboard.html',
                        controller: 'hrmHomeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('hrEmpWorkAreaDtlInfo');
                        $translatePartialLoader.addPart('hrEmpTransferApplInfo');
                        return $translate.refresh();
                    }]
                }
            })

            .state('hrm.dashboard', {
                parent: 'entity',
                url: '/admin-dashboard',
                data: {
                    /*authorities: ['HRM_ADMIN','ROLE_ADMIN','ROLE_INSTEMP','ROLE_HRM_USER'],*/
                    authorities: [],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'HRM Dashboard'
                },
                views: {
                    'content@':{
                        templateUrl: 'scripts/app/entities/hrm/hr-dashboard.html',
                        controller: 'hrmHomeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('hrEmpChildrenInfo');
                        $translatePartialLoader.addPart('hrEmpTransferApplInfo');
                        $translatePartialLoader.addPart('gender');
                        return $translate.refresh();
                    }]
                }
            })

            .state('hrm.depthead', {
                parent: 'hrm',
                url: '/depthead',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_ADMIN','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'Department Head Dashboard'
                },
                views: {
                    'hrmManagementView@hrm':{
                        templateUrl: 'scripts/app/entities/hrm/deptHead-dashboard.html',
                        controller: 'hrmDepartmentHeadController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('hrEmpChildrenInfo');
                        $translatePartialLoader.addPart('hrEmpTransferApplInfo');
                        $translatePartialLoader.addPart('hrEmpServiceHistory');
                        $translatePartialLoader.addPart('gender');
                        return $translate.refresh();
                    }]
                }
            })
            .state('hrm.maindashboard', {
                parent: 'hrm',
                url: '/dashboard',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_ADMIN','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'Department Head Dashboard'
                },
                views: {
                    'hrmManagementView@hrm':{
                        templateUrl: 'scripts/app/entities/hrm/dashboard.html',
                        controller: 'hrmHomeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('hrEmpChildrenInfo');
                        $translatePartialLoader.addPart('hrEmpTransferApplInfo');
                        $translatePartialLoader.addPart('hrEmpServiceHistory');
                        $translatePartialLoader.addPart('gender');
                        return $translate.refresh();
                    }]
                }
            })

            .state('instituteInfo.instadminapproval', {
                parent: 'instituteInfo',
                url: '/instadminapproval',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_INSTITUTE','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE'],
                },
                views: {
                    'instituteView@instituteInfo': {
                        templateUrl: 'scripts/app/entities/hrm/instHead-dashboard.html',
                        controller: 'hrmInstituteHeadController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('designationType');
                        $translatePartialLoader.addPart('hrEmpTransferApplInfo');
                        $translatePartialLoader.addPart('instGenInfo');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('gender');
                        return $translate.refresh();
                    }]
                }
            })

            .state('shortcut-access', {
                parent: 'entity',
                url: '/shortcut-access',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_ADMIN'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'HRM Dashboard'
                },
                views: {
                    'content@':{
                        templateUrl: 'scripts/app/entities/hrm/shortcut-access.html',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('hrEmpChildrenInfo');
                        $translatePartialLoader.addPart('gender');
                        return $translate.refresh();
                    }]
                }
            })

            .state('hrm.employees', {
                parent: 'hrm',
                url: '/employees',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER','ROLE_DTE_EMPLOYEE','ROLE_ORG_USER'],
                    pageTitle: 'stepApp.hrmHome.home.generalHomeTitle',
                    displayName:'Employee List'
                },
                views: {

                    'hrmManagementView@hrm': {
                        templateUrl: 'scripts/app/entities/hrm/admin-employeelist.html',
                        controller: 'HrEmployeeInfoListController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('gender');
                        $translatePartialLoader.addPart('hrmHome');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('leftmenu');
                        $translatePartialLoader.addPart('designationType');
                        return $translate.refresh();
                    }]
                }
            })
            .state('orghome.employeeInfoByOrg', {
                parent: 'orghome',
                url: '/employeeList',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER','ROLE_HRM_ADMIN','ROLE_ORG_USER']
                },
                views: {
                    'hrmOrgManagementView@orghome': {
                        templateUrl: 'scripts/app/entities/hrm/employeeInfo/hrEmployeeInfos.html',
                        controller: 'HrEmployeeInfoByOrganizationController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrEmployeeInfo');
                        $translatePartialLoader.addPart('gender');
                        $translatePartialLoader.addPart('maritalStatus');
                        $translatePartialLoader.addPart('bloodGroup');
                        $translatePartialLoader.addPart('jobQuota');
                        $translatePartialLoader.addPart('religions');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('designationType');
                        return $translate.refresh();
                    }]
                }
            });
    });
