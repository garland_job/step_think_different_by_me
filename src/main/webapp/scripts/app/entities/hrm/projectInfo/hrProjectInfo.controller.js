'use strict';

angular.module('stepApp')
    .controller('HrProjectInfoController',
    ['$scope', '$state', 'HrProjectInfo', 'HrProjectInfoSearch', 'ParseLinks',
    function ($scope, $state, HrProjectInfo, HrProjectInfoSearch, ParseLinks) {

        $scope.hrProjectInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            HrProjectInfo.query({page: $scope.page - 1, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrProjectInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrProjectInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrProjectInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrProjectInfo = {
                projectName: null,
                projectDetail: null,
                directorName: null,
                startDate: null,
                endDate: null,
                projectValue: null,
                projectStatus: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrProjectInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrProjectInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrProjectInfo', 'MiscTypeSetupByCategory','User','Principal','DateUtils','HrEmployeeOfInstitutes',
        function($scope, $rootScope, $stateParams, $state, entity, HrProjectInfo, MiscTypeSetupByCategory, User, Principal, DateUtils,HrEmployeeOfInstitutes) {

            $scope.hrProjectInfo = entity;

            if($stateParams.id !=null){
                HrProjectInfo.get({id : $stateParams.id},function(result){
                    $scope.hrProjectInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.fundSourceList = MiscTypeSetupByCategory.get({cat:'SourceOfFund',stat:'true'});
            $scope.load = function(id) {
                HrProjectInfo.get({id : id}, function(result) {
                    $scope.hrProjectInfo = result;
                });
            };
            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrProjectInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrProjectInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrProjectInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrProjectInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrProjectInfo.id != null)
                {
                    HrProjectInfo.update($scope.hrProjectInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrProjectInfo.updated');
                }
                else
                {
                    $scope.hrProjectInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrProjectInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrProjectInfo.save($scope.hrProjectInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrProjectInfo.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*hrProjectInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrProjectInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrProjectInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, entity, HrProjectInfo, MiscTypeSetup) {
                $scope.hrProjectInfo = entity;
                $scope.load = function (id) {
                    HrProjectInfo.get({id: id}, function(result) {
                        $scope.hrProjectInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrProjectInfoUpdate', function(event, result) {
                    $scope.hrProjectInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrProjectInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrProjectInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrProjectInfo',
            function($scope, $rootScope, $modalInstance, entity, HrProjectInfo) {

                $scope.hrProjectInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrProjectInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrProjectInfo.deleted');
                };

            }]);
