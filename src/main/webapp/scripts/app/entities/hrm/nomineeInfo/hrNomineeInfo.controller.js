'use strict';

angular.module('stepApp')
    .controller('HrNomineeInfoController',
    ['$rootScope', '$scope', '$state', 'HrNomineeInfo', 'HrNomineeInfoSearch', 'ParseLinks',
    function ($rootScope, $scope, $state, HrNomineeInfo, HrNomineeInfoSearch, ParseLinks) {

        $scope.hrNomineeInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrNomineeInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrNomineeInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrNomineeInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrNomineeInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrNomineeInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrNomineeInfo = {
                nomineeName: null,
                nomineeNameBn: null,
                birthDate: null,
                gender: null,
                relationship: null,
                occupation: null,
                designation: null,
                nationalId: null,
                mobileNumber: null,
                address: null,
                logId:null,
                logStatus:null,
                logComments:null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrNomineeInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrNomineeInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrNomineeInfo', 'HrEmployeeInfoByWorkArea','Principal','User','DateUtils','MiscTypeSetupByCategory','HrEmployeeOfInstitutes','HrEmployeeInfoByEmployeeId',
        function($scope, $rootScope, $stateParams, $state, entity, HrNomineeInfo, HrEmployeeInfoByWorkArea, Principal, User, DateUtils, MiscTypeSetupByCategory,HrEmployeeOfInstitutes,HrEmployeeInfoByEmployeeId) {

            $scope.hrNomineeInfo = entity;

            if($stateParams.id !=null){
                HrNomineeInfo.get({id : $stateParams.id},function(result){
                    $scope.hrNomineeInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });

            $scope.nomineeRelationships = MiscTypeSetupByCategory.get({cat:'NomineeRelationship'});
            $scope.load = function(id) {
                HrNomineeInfo.get({id : id}, function(result) {
                    $scope.hrNomineeInfo = result;
                });
            };

            $scope.hremployeeinfos  = [];
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loadHrEmployee = function(nominee)
            {
                HrEmployeeInfoByEmployeeId.get({id: nominee.employeeInfo.employeeId}, function (result){
                    console.log("result");
                    console.log(result);
                    $scope.hrNomineeInfo.employeeInfo = result;
                });
            }
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrNomineeInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrNomineeInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.isSaving = true;
                        $scope.hrNomineeInfo.updateBy = result.id;
                        $scope.hrNomineeInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                        if ($scope.hrNomineeInfo.id != null)
                        {
                            $scope.hrNomineeInfo.logId = 0;
                            $scope.hrNomineeInfo.logStatus = 6;
                            HrNomineeInfo.update($scope.hrNomineeInfo, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrNomineeInfo.updated');
                        }
                        else
                        {
                            $scope.hrNomineeInfo.logId = 0;
                            $scope.hrNomineeInfo.logStatus = 6;
                            $scope.hrNomineeInfo.createBy = result.id;
                            $scope.hrNomineeInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrNomineeInfo.save($scope.hrNomineeInfo, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrNomineeInfo.created');
                        }
                    });
                });
            };

            $scope.clear = function() {

            };
        }]);
/*hrNomineeInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrNomineeInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrNomineeInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrNomineeInfo, HrEmployeeInfo) {
                $scope.hrNomineeInfo = entity;
                $scope.load = function (id) {
                    HrNomineeInfo.get({id: id}, function(result) {
                        $scope.hrNomineeInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrNomineeInfoUpdate', function(event, result) {
                    $scope.hrNomineeInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrNomineeInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrNomineeInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrNomineeInfo',
            function($scope, $rootScope, $modalInstance, entity, HrNomineeInfo) {

                $scope.hrNomineeInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrNomineeInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrNomineeInfo.deleted');
                };

            }]);
/*hrNomineeInfo-profile.controller.js*/

angular.module('stepApp').controller('HrNomineeInfoProfileController',
    ['$scope', '$stateParams', '$state', '$rootScope', 'HrNomineeInfo', 'HrEmployeeInfo','Principal','User','DateUtils','MiscTypeSetupByCategory',
        function($scope, $stateParams, $state, $rootScope, HrNomineeInfo, HrEmployeeInfo, Principal, User, DateUtils, MiscTypeSetupByCategory) {

            $scope.hrNomineeInfo = {};
            $scope.isPreCondition = true;
            $scope.preConditionStatus = [];
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.nomineeRelationships = MiscTypeSetupByCategory.get({cat:'NomineeRelationship'});
            $scope.load = function(id) {
                HrNomineeInfo.get({id : id}, function(result) {
                    $scope.hrNomineeInfo = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();


            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadNomineeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrNomineeInfo.query({id: 'my'}, function (result)
                {
                    //console.log("result:, len: "+JSON.stringify(result));
                    $scope.hrNomineeInfoList = result;
                    if($scope.hrNomineeInfoList.length < 1)
                    {
                        $scope.addMode = true;
                        $scope.hrNomineeInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else
                    {
                        $scope.hrEmployeeInfo = $scope.hrNomineeInfoList[0].employeeInfo;
                        angular.forEach($scope.hrNomineeInfoList,function(modelInfo)
                        {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if(modelInfo.logStatus==0)
                            {
                                modelInfo.isLocked = true;
                            }
                        });
                    }

                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.hrNomineeInfo.viewMode = true;
                    $scope.hrNomineeInfo.viewModeText = "Add";

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                    $scope.hrNomineeInfo.viewMode = true;
                    $scope.hrNomineeInfo.viewModeText = "Add";
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                    {
                        if($scope.hrNomineeInfoList.length > 1)
                        {
                            var indx = $scope.hrNomineeInfoList.indexOf(modelInfo);
                            $scope.hrNomineeInfoList.splice(indx, 1);
                        }
                        else
                        {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function()
            {
                $scope.hrNomineeInfoList.push(
                    {
                        viewMode:false,
                        viewModeText:'Cancel',
                        nomineeName: null,
                        nomineeNameBn: null,
                        birthDate: null,
                        gender: null,
                        occupation: null,
                        designation: null,
                        nationalId: null,
                        mobileNumber: null,
                        address: null,
                        logId:null,
                        logStatus:null,
                        logComments:null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    nomineeName: null,
                    nomineeNameBn: null,
                    birthDate: null,
                    gender: null,
                    occupation: null,
                    designation: null,
                    nationalId: null,
                    mobileNumber: null,
                    address: null,
                    logId:null,
                    logStatus:null,
                    logComments:null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrNomineeInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrNomineeInfoList[$scope.selectedIndex].id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index)
            {
                $scope.selectedIndex = index;
                $scope.isSaving = true;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if($scope.preCondition(modelInfo)){
                    if (modelInfo.id != null  ){
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrNomineeInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }else{
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrNomineeInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                }else{
                    $rootScope.setErrorMessage("Please input valid information");
                }

            };

            $scope.preCondition = function(hrNomineeInfo){
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.nomineeName', hrNomineeInfo.nomineeName, 'text', 100, 4, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.birthDate', DateUtils.convertLocaleDateToDMY(hrNomineeInfo.birthDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.gender', hrNomineeInfo.gender, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.occupation', hrNomineeInfo.occupation, 'text', 100, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.designation', hrNomineeInfo.designation, 'text', 100, 2, 'atozAndAtoZ', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.nationalId', hrNomineeInfo.nationalId, 'number', 17, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrNomineeInfo.mobileNumber', hrNomineeInfo.mobileNumber, 'number', 18, 11, '0to9', true, '', ''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function() {

            };
        }]);

