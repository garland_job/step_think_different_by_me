'use strict';

angular.module('stepApp')
    .controller('HrDepartmentHeadInfoController', function ($scope, HrDepartmentHeadInfo, HrDepartmentHeadInfoSearch, ParseLinks) {
        $scope.hrDepartmentHeadInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            HrDepartmentHeadInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.hrDepartmentHeadInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            HrDepartmentHeadInfo.get({id: id}, function(result) {
                $scope.hrDepartmentHeadInfo = result;
                $('#deleteHrDepartmentHeadInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            HrDepartmentHeadInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteHrDepartmentHeadInfoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            HrDepartmentHeadInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrDepartmentHeadInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrDepartmentHeadInfo = {
                joinDate: null,
                endDate: null,
                activeHead: false,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*hrDepartmentHeadInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrDepartmentHeadInfoDialogController',
    ['$scope', '$stateParams', '$rootScope', '$state', 'entity', 'HrDepartmentHeadInfo', 'HrDepartmentSetup', 'HrEmployeeInfosByDesigLevel','Principal','User','DateUtils','$window',
        function($scope, $stateParams, $rootScope, $state, entity, HrDepartmentHeadInfo, HrDepartmentSetup, HrEmployeeInfosByDesigLevel,Principal,User,DateUtils,$window) {

            $scope.hrDepartmentHeadInfo = entity;
            $scope.hrdepartmentsetups = HrDepartmentSetup.query();
            $scope.hremployeeinfos = [];//HrEmployeeInfo.query();
            $scope.load = function(id) {
                HrDepartmentHeadInfo.get({id : id}, function(result) {
                    $scope.hrDepartmentHeadInfo = result;
                });
            };

            HrEmployeeInfosByDesigLevel.get({desigLevel : 3}, function(result) {
                $scope.hremployeeinfos = result;
            });

            $scope.loadDepartmentEntity = function ()
            {
                console.log("EntityId: "+entity.id);
                console.log("DeptIdStateParam: "+$stateParams.deptid);
                if($stateParams.deptid)
                {
                    HrDepartmentSetup.get({id : $stateParams.deptid}, function(result) {
                        $scope.hrDepartmentHeadInfo.departmentInfo = result;
                        console.log("HeadDeptLoaded: "+$stateParams.deptid);
                    });
                }
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                        $scope.loadDepartmentEntity();
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:hrDepartmentHeadInfoUpdate', result);
                $scope.isSaving = false;
                $window.history.back();
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrDepartmentHeadInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrDepartmentHeadInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrDepartmentHeadInfo.id != null)
                {
                    HrDepartmentHeadInfo.update($scope.hrDepartmentHeadInfo, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.hrDepartmentHeadInfo.updated');
                } else
                {
                    $scope.hrDepartmentHeadInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrDepartmentHeadInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrDepartmentHeadInfo.save($scope.hrDepartmentHeadInfo, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.hrDepartmentHeadInfo.created');
                }
            };

            $scope.clear = function() {

            };
            $scope.max_date=new Date();
        }]);
/*hrDepartmentHeadInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentHeadInfoDetailController', function ($scope, $rootScope, $stateParams, entity, HrDepartmentHeadInfo, HrDepartmentSetup, HrEmployeeInfo) {
        $scope.hrDepartmentHeadInfo = entity;
        $scope.load = function (id) {
            HrDepartmentHeadInfo.get({id: id}, function(result) {
                $scope.hrDepartmentHeadInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:hrDepartmentHeadInfoUpdate', function(event, result) {
            $scope.hrDepartmentHeadInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

/* new  Controller Add by bappi mazumder*/


angular.module('stepApp').controller('HrDepartmentHeadActiveController',
    ['$scope', '$stateParams', '$rootScope', '$modalInstance','$state', 'entity', 'HrDepartmentHeadInfo','Principal','User','DateUtils',
        function($scope, $stateParams, $rootScope,$modalInstance,$state, entity, HrDepartmentHeadInfo,Principal,User,DateUtils) {

            $scope.hrDepartmentHeadInfo = entity;
            //$scope.load = function(id) {
                HrDepartmentHeadInfo.get({id : $stateParams.id}, function(result) {
                    $scope.hrDepartmentHeadInfo = result;
                });
            //};

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                        $scope.loadDepartmentEntity();
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:hrDepartmentHeadInfoUpdate', result);
                $scope.isSaving = false;
                $modalInstance.close();
                $state.go('hrDepartmentSetup.deptAndSectionInfo', null, { reload: true });
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrDepartmentHeadInfo.activeHead = $stateParams.activeHead;
                $scope.hrDepartmentHeadInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrDepartmentHeadInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.hrDepartmentHeadInfo.id != null){
                    HrDepartmentHeadInfo.update($scope.hrDepartmentHeadInfo, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.hrDepartmentHeadInfo.updated');
                }

            };

            $scope.clear = function() {
                $modalInstance.close();
            };
        }]);
