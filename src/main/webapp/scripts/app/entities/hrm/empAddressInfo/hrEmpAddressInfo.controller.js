'use strict';

angular.module('stepApp')
    .controller('HrEmpAddressInfoController',
        ['$rootScope', '$scope', '$state', 'HrEmpAddressInfo', 'HrEmpAddressInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrEmpAddressInfo, HrEmpAddressInfoSearch, ParseLinks) {

                $scope.hrEmpAddressInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpAddressInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    //console.log("pg: "+$scope.page+", pred:"+$scope.predicate+", rootPage: "+$rootScope.pageNumber+", rootSc: "+$rootScope.currentStateName+", lcst:"+$scope.stateName);

                    HrEmpAddressInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpAddressInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpAddressInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpAddressInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpAddressInfo = {
                        addressType: null,
                        houseNumber: null,
                        houseNumberBn: null,
                        roadNumber: null,
                        roadNumberBn: null,
                        villageName: null,
                        villageNameBn: null,
                        postOffice: null,
                        postCode: null,
                        postOfficeBn: null,
                        policeStation: null,
                        policeStationBn: null,
                        district: null,
                        districtBn: null,
                        contactNumber: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpAddressInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpAddressInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpAddressInfo', 'HrEmployeeInfoByWorkArea', 'Principal', 'User', 'DateUtils', 'District', 'Upazila', 'MiscTypeSetupByCategory', 'Division', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmpAddressInfo, HrEmployeeInfoByWorkArea, Principal, User, DateUtils, District, Upazila, MiscTypeSetupByCategory, Division, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpAddressInfo = entity;

            if ($stateParams.id != null) {
                HrEmpAddressInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpAddressInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });


            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.divisions = Division.query();

            $scope.districtList = District.query({size: 500});
            $scope.districtListFilter = $scope.districtList;

            $scope.upazilaList = Upazila.query({size: 500});
            $scope.upazilaListFilter = $scope.upazilaList;
            $scope.load = function (id) {
                HrEmpAddressInfo.get({id: id}, function (result) {
                    $scope.hrEmpAddressInfo = result;
                });
            };
            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };
            $scope.loadHrEmployee = function (address) {
                HrEmployeeInfoByEmployeeId.get({id: address.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpAddressInfo.employeeInfo = result;
                });
            }

            $scope.loadDistrictByDivision = function (divisionObj) {
                $scope.districtListFilter = [];
                angular.forEach($scope.districtList, function (districtObj) {
                    if (divisionObj.id == districtObj.division.id) {
                        $scope.districtListFilter.push(districtObj);
                    }
                });
            };

            $scope.loadUpazilaByDistrict = function (districtObj) {
                $scope.upazilaListFilter = [];
                angular.forEach($scope.upazilaList, function (upazilaObj) {
                    if (districtObj.id == upazilaObj.district.id) {
                        $scope.upazilaListFilter.push(upazilaObj);
                    }
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAddressInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpAddressInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.isSaving = true;
                        $scope.hrEmpAddressInfo.updateBy = result.id;
                        $scope.hrEmpAddressInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        $scope.hrEmpAddressInfo.logId = 0;
                        $scope.hrEmpAddressInfo.logStatus = 6;
                        if ($scope.hrEmpAddressInfo.id != null) {
                            HrEmpAddressInfo.update($scope.hrEmpAddressInfo, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrEmpAddressInfo.updated');
                        }
                        else {
                            $scope.hrEmpAddressInfo.createBy = result.id;
                            $scope.hrEmpAddressInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrEmpAddressInfo.save($scope.hrEmpAddressInfo, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrEmpAddressInfo.created');
                        }
                    });
                });
            };

            $scope.clear = function () {

            };
        }]);
/*hrEmpAddressInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAddressInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpAddressInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpAddressInfo, HrEmployeeInfo) {
                $scope.hrEmpAddressInfo = entity;
                $scope.load = function (id) {
                    HrEmpAddressInfo.get({id: id}, function (result) {
                        $scope.hrEmpAddressInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpAddressInfoUpdate', function (event, result) {
                    $scope.hrEmpAddressInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpAddressInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAddressInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpAddressInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpAddressInfo) {

                $scope.hrEmpAddressInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpAddressInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmpAddressInfo.deleted');
                        });
                };

            }]);
/*hrEmpAddressInfo-approval.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAddressInfoApprovalController',
        ['$scope', '$rootScope', '$stateParams', 'HrEmpAddressInfoApproverLog', 'HrEmpAddressInfoApprover', '$modalInstance',
            function ($scope, $rootScope, $stateParams, HrEmpAddressInfoApproverLog, HrEmpAddressInfoApprover, $modalInstance) {
                $scope.hrEmpAddressInfo = {};
                $scope.hrEmpAddressInfoLog = {};
                $scope.isApproved = true;
                $scope.load = function () {
                    HrEmpAddressInfoApproverLog.get({entityId: $stateParams.id}, function (result) {
                        console.log("HrEmpAddressInfoApproverLog");
                        $scope.hrEmpAddressInfo = result.entityObject;
                        $scope.hrEmpAddressInfoLog = result.entityLogObject;
                    });
                };


                $scope.applyApproval = function (actionType) {
                    var approvalObj = $scope.initApprovalObject($scope.hrEmpAddressInfo.id, $scope.logComments, actionType);
                    console.log("Address approval processing..." + JSON.stringify(approvalObj));

                    HrEmpAddressInfoApprover.update(approvalObj, function (result) {
                        $modalInstance.dismiss('cancel');
                        $rootScope.$emit('onEntityApprovalProcessCompleted', result);
                    });
                    $modalInstance.dismiss('cancel');
                };

                $scope.initApprovalObject = function (entityId, logComments, actionType) {
                    return {
                        entityId: entityId,
                        logComments: logComments,
                        actionType: actionType
                    };
                };

                $scope.load();

                var unsubscribe = $rootScope.$on('stepApp:hrEmpAddressInfoUpdate', function (event, result) {
                    $scope.hrEmpAddressInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };

            }]);
/*hrEmpAddressInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpAddressInfoProfileController',
    ['$scope', '$stateParams', '$rootScope', '$state', '$filter', 'HrEmpAddressInfo', 'HrEmployeeInfo', 'Principal', 'User', 'DateUtils', 'District', 'Upazila', 'Division',
        function ($scope, $stateParams, $rootScope, $state, $filter, HrEmpAddressInfo, HrEmployeeInfo, Principal, User, DateUtils, District, Upazila, Division) {

            $scope.hrEmpAddressInfo = {};
            //$scope.hremployeeinfos = HrEmployeeInfo.query();

            $scope.divisions = Division.query();

            $scope.districtList = District.query({size: 500});
            $scope.districtListFilter = $scope.districtList;

            $scope.upazilaList = Upazila.query({size: 500});
            $scope.upazilaListFilter = $scope.upazilaList;
            $scope.load = function (id) {
                HrEmpAddressInfo.get({id: id}, function (result) {
                    $scope.hrEmpAddressInfo = result;
                });
            };

            $scope.loadDistrictByDivision = function (divisionObj) {
                $scope.districtListFilter = [];
                angular.forEach($scope.districtList, function (districtObj) {
                    if (divisionObj.id == districtObj.division.id) {
                        $scope.districtListFilter.push(districtObj);
                    }
                });
            };

            $scope.loadUpazilaByDistrict = function (districtObj) {
                $scope.upazilaListFilter = [];
                angular.forEach($scope.upazilaList, function (upazilaObj) {
                    if (districtObj.id == upazilaObj.district.id) {
                        $scope.upazilaListFilter.push(upazilaObj);
                    }
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            var ADDR_TYPE_PERMANENT = 'Permanent';
            var ADDR_TYPE_PRESENT = 'Present';
            var ADDR_TYPE_EMERGENCY = 'Emergency';

            $scope.loadModel = function () {
                console.log("loadChildProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpAddressInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpAddressInfoList = result;
                    if ($scope.hrEmpAddressInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpAddressInfoList.push($scope.initiateAddress(ADDR_TYPE_PERMANENT));
                        $scope.hrEmpAddressInfoList.push($scope.initiateAddress(ADDR_TYPE_PRESENT));
                        $scope.hrEmpAddressInfoList.push($scope.initiateAddress(ADDR_TYPE_EMERGENCY));

                        //$scope.hrEmpAddressInfoList = $scope.shortList($scope.hrEmpAddressInfoList);
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpAddressInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpAddressInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });

                        var permAddObj = $filter('filter')($scope.hrEmpAddressInfoList, {addressType: ADDR_TYPE_PERMANENT})[0];
                        if (!permAddObj) {
                            $scope.hrEmpAddressInfoList.push($scope.initiateAddress(ADDR_TYPE_PERMANENT));
                        }

                        var presAddObj = $filter('filter')($scope.hrEmpAddressInfoList, {addressType: ADDR_TYPE_PRESENT})[0];
                        if (!presAddObj) {
                            $scope.hrEmpAddressInfoList.push($scope.initiateAddress(ADDR_TYPE_PRESENT));
                        }

                        var emerAddObj = $filter('filter')($scope.hrEmpAddressInfoList, {addressType: ADDR_TYPE_EMERGENCY})[0];
                        if (!emerAddObj) {
                            $scope.hrEmpAddressInfoList.push($scope.initiateAddress(ADDR_TYPE_EMERGENCY));
                        }

                        $scope.hrEmpAddressInfoList = $scope.shortList($scope.hrEmpAddressInfoList);
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.initiateAddress = function (addressType) {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    addressType: addressType,
                    houseNumber: null,
                    houseNumberBn: null,
                    roadNumber: null,
                    roadNumberBn: null,
                    villageName: null,
                    villageNameBn: null,
                    postOffice: null,
                    postCode: null,
                    postOfficeBn: null,
                    contactNumber: null,
                    contactName: null,
                    contactNameBn: null,
                    address: null,
                    activeStatus: true,
                    logId: 0,
                    logStatus: 1,
                    logComments: null,
                    id: null
                };
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAddressInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpAddressInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $scope.viewMode = true;
            };

            $scope.shortList = function (modelList) {
                var newModelList = [];
                angular.forEach(modelList, function (modelInfo) {
                    if (modelInfo.addressType == ADDR_TYPE_PERMANENT) {
                        newModelList[0] = modelInfo;
                    }
                    else if (modelInfo.addressType == ADDR_TYPE_PRESENT) {
                        newModelList[1] = modelInfo;
                    }
                    else if (modelInfo.addressType == ADDR_TYPE_EMERGENCY) {
                        newModelList[2] = modelInfo;
                    }
                });
                return newModelList;
            };

            $scope.shortListDefault = function (dataList) {
                dataList.sort(function (a, b) {
                    var nameA = a.addressType.toLowerCase(), nameB = b.addressType.toLowerCase()
                    if (nameA < nameB) //sort string ascending
                        return -1
                    if (nameA > nameB)
                        return 1
                    return 0 //default return value (no sorting)
                });
                return dataList;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpAddressInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                    else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                        HrEmpAddressInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpAddressInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.houseNumber', hrEmpAddressInfo.houseNumber, 'number', 20, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.roadNumber', hrEmpAddressInfo.roadNumber, 'number', 20, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.villageName', hrEmpAddressInfo.villageName, 'text', 20, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.postOffice', hrEmpAddressInfo.postOffice, 'text', 50, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.contactName', hrEmpAddressInfo.contactName, 'text', 50, 4, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.contactNumber', hrEmpAddressInfo.contactNumber, 'number', 18, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAddressInfo.postCode', hrEmpAddressInfo.postCode, 'number', 4, 4, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };

            $scope.copyFromUppper = function (indx) {
                console.log("Current Index: " + indx);
                var topObj = $scope.hrEmpAddressInfoList[indx - 1];
                var curObj = $scope.hrEmpAddressInfoList[indx];
                //console.log(JSON.stringify(topObj));
                //console.log(JSON.stringify(curObj));
                if (topObj.id) {
                    console.log("Yes, top object is valid");
                    curObj.houseNumber = topObj.houseNumber;
                    curObj.houseNumberBn = topObj.houseNumberBn;
                    curObj.roadNumber = topObj.roadNumber;
                    curObj.roadNumberBn = topObj.roadNumberBn;
                    curObj.villageName = topObj.villageName;
                    curObj.villageNameBn = topObj.villageNameBn;
                    curObj.postOffice = topObj.postOffice;
                    curObj.postCode = topObj.postCode;
                    curObj.postOfficeBn = topObj.postOfficeBn;
                    curObj.contactNumber = topObj.contactNumber;
                    curObj.division = topObj.division;
                    curObj.district = topObj.district;
                    curObj.upazila = topObj.upazila;
                }
            };
        }]);
