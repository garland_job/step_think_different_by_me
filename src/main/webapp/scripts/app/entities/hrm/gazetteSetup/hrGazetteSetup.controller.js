'use strict';

angular.module('stepApp')
    .controller('HrGazetteSetupController',
        ['$scope', '$state', 'HrGazetteSetup', 'HrGazetteSetupSearch', 'ParseLinks',
            function ($scope, $state, HrGazetteSetup, HrGazetteSetupSearch, ParseLinks) {

                $scope.hrGazetteSetups = [];
                $scope.predicate = 'gazetteName';
                $scope.reverse = true;
                $scope.page = 1;
                $scope.loadAll = function () {
                    HrGazetteSetup.query({
                        page: $scope.page - 1,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrGazetteSetups = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrGazetteSetupSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrGazetteSetups = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrGazetteSetup = {
                        gazetteCode: null,
                        gazetteName: null,
                        gazetteYear: null,
                        gazetteDetail: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrGazetteSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrGazetteSetupDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrGazetteSetup', 'User', 'Principal', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, entity, HrGazetteSetup, User, Principal, DateUtils) {

            $scope.hrGazetteSetup = entity;
            $scope.load = function (id) {
                HrGazetteSetup.get({id: id}, function (result) {
                    $scope.hrGazetteSetup = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrGazetteSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrGazetteSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrGazetteSetup.updateBy = $scope.loggedInUser.id;
                $scope.hrGazetteSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.preCondition()) {
                    if ($scope.hrGazetteSetup.id != null) {
                        HrGazetteSetup.update($scope.hrGazetteSetup, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.hrGazetteSetup.updated');
                    } else {
                        $scope.hrGazetteSetup.createBy = $scope.loggedInUser.id;
                        $scope.hrGazetteSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrGazetteSetup.save($scope.hrGazetteSetup, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.hrGazetteSetup.created');
                    }
                } else {
                    $rootScope.setErrorMessage("Please enter correct information");
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrGazetteSetup.gazetteName', $scope.hrGazetteSetup.gazetteName, 'text', 50, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrGazetteSetup.gazetteYear', $scope.hrGazetteSetup.gazetteYear, 'text', 4, 4, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {
            };
        }]);
/*hrGazetteSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrGazetteSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrGazetteSetup',
            function ($scope, $rootScope, $stateParams, entity, HrGazetteSetup) {
                $scope.hrGazetteSetup = entity;
                $scope.load = function (id) {
                    HrGazetteSetup.get({id: id}, function (result) {
                        $scope.hrGazetteSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrGazetteSetupUpdate', function (event, result) {
                    $scope.hrGazetteSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrGazetteSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrGazetteSetupDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrGazetteSetup',
            function ($scope, $rootScope, $modalInstance, entity, HrGazetteSetup) {

                $scope.hrGazetteSetup = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrGazetteSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrGazetteSetup.deleted');
                        });
                };

            }]);
