'use strict';

angular.module('stepApp')
    .controller('HrClassInfoController',
        ['$scope', '$state', 'HrClassInfo', 'HrClassInfoSearch', 'ParseLinks',
            function ($scope, $state, HrClassInfo, HrClassInfoSearch, ParseLinks) {

                $scope.hrClassInfos = [];
                $scope.predicate = 'className';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.loadAll = function () {
                    HrClassInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrClassInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

                $scope.search = function () {
                    HrClassInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrClassInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrClassInfo = {
                        classCode: null,
                        className: null,
                        classDetail: null,
                        activeStatus: false,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);

/*hrClassInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrClassInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrClassInfo', 'Principal', 'User', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, entity, HrClassInfo, Principal, User, DateUtils) {

            $scope.hrClassInfo = entity;
            $scope.load = function (id) {
                HrClassInfo.get({id: id}, function (result) {
                    $scope.hrClassInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrClassInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrClassInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.isSaving = true;
                        $scope.hrClassInfo.updateBy = result.id;
                        $scope.hrClassInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.preCondition()) {
                            if ($scope.hrClassInfo.id != null) {
                                HrClassInfo.update($scope.hrClassInfo, onSaveSuccess, onSaveError);
                                $rootScope.setWarningMessage('stepApp.hrClassInfo.updated');
                            }
                            else {
                                $scope.hrClassInfo.createBy = result.id;
                                $scope.hrClassInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                                HrClassInfo.save($scope.hrClassInfo, onSaveSuccess, onSaveError);
                                $rootScope.setSuccessMessage('stepApp.hrClassInfo.created');
                            }
                        } else {
                            $rootScope.setErrorMessage("Please enter correct information");
                        }
                    });
                });
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrClassInfo.classCode', $scope.hrClassInfo.classCode, 'text', 50, 4, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrClassInfo.className', $scope.hrClassInfo.className, 'text', 100, 4, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {

            };
        }]);
/*hrClassInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrClassInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrClassInfo',
            function ($scope, $rootScope, $stateParams, entity, HrClassInfo) {
                $scope.hrClassInfo = entity;
                $scope.load = function (id) {
                    HrClassInfo.get({id: id}, function (result) {
                        $scope.hrClassInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrClassInfoUpdate', function (event, result) {
                    $scope.hrClassInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*hrClassInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrClassInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrClassInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrClassInfo) {

                $scope.hrClassInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrClassInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrClassInfo.deleted');
                        });
                };

            }]);

