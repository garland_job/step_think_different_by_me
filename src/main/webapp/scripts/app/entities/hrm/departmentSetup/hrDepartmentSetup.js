'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('hrDepartmentSetup', {
                parent: 'hrm',
                url: '/hrDepartmentSetups',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER','ROLE_INSTITUTE'],
                    pageTitle: 'stepApp.hrDepartmentSetup.home.title'
                },
                views: {
                    'hrmManagementView@hrm': {
                        templateUrl: 'scripts/app/entities/hrm/departmentSetup/hrDepartmentSetups.html',
                        controller: 'HrDepartmentSetupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrDepartmentSetup');
                        $translatePartialLoader.addPart('hrDepartmentHeadSetup');
                        $translatePartialLoader.addPart('instEmployee');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('hrDepartmentSetup.detail', {
                parent: 'hrm',
                url: '/hrDepartmentSetup/{id}',
                data: {
                    authorities: ['ROLE_HRM_ADMIN','ROLE_HRM_USER','ROLE_ADMIN','ROLE_INSTITUTE'],
                    pageTitle: 'stepApp.hrDepartmentSetup.detail.title'
                },
                views: {
                    'hrmManagementView@hrm': {
                        templateUrl: 'scripts/app/entities/hrm/departmentSetup/hrDepartmentSetup-detail.html',
                        controller: 'HrDepartmentSetupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrDepartmentSetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'HrDepartmentSetup', function($stateParams, HrDepartmentSetup) {
                        return HrDepartmentSetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('hrDepartmentSetup.new', {
                parent: 'hrDepartmentSetup',
                url: '/new',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER'],
                },
                views: {
                    'hrmManagementView@hrm': {
                        templateUrl: 'scripts/app/entities/hrm/departmentSetup/hrDepartmentSetup-dialog.html',
                        controller: 'HrDepartmentSetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrDepartmentSetup');
                        $translatePartialLoader.addPart('hrDepartmentHeadInfo');
                        $translatePartialLoader.addPart('hrDepartmentHeadSetup');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            activeStatus: true,
                            createDate: null,
                            createBy: null,
                            updateDate: null,
                            updateBy: null,
                            id: null
                        };
                    }
                }
            })
            .state('hrDepartmentSetup.edit', {
                parent: 'hrDepartmentSetup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_HRM_ADMIN','ROLE_ADMIN'],
                },
                views: {
                    'hrmManagementView@hrm': {
                        templateUrl: 'scripts/app/entities/hrm/departmentSetup/hrDepartmentSetup-dialog.html',
                        controller: 'HrDepartmentSetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrDepartmentSetup');
                        $translatePartialLoader.addPart('hrDepartmentHeadInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'HrDepartmentSetup', function($stateParams, HrDepartmentSetup) {
                        //return HrDepartmentSetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('hrDepartmentSetup.delete', {
                parent: 'hrDepartmentSetup',
                url: '/{id}/delete',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_HRM_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/hrm/departmentSetup/hrDepartmentSetup-delete-dialog.html',
                        controller: 'HrDepartmentSetupDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['HrDepartmentSetup', function(HrDepartmentSetup) {
                                return HrDepartmentSetup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('hrDepartmentSetup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('hrDepartmentSetup.deptAndSectionInfo', {
                parent: 'hrm',
                url: '/hrDepartmentSetups/deptList',
                data: {
                    authorities: ['HRM_ADMIN','ROLE_ADMIN','ROLE_INSTITUTE'],
                    pageTitle: 'stepApp.hrDepartmentSetup.home.title'
                },
                views: {
                    'hrmManagementView@hrm': {
                        templateUrl: 'scripts/app/entities/hrm/departmentSetup/hrDeptAndSectionList.html',
                        controller: 'HrDepartmentAndSectionListController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('hrDepartmentSetup');
                        $translatePartialLoader.addPart('hrDepartmentHeadSetup');
                        $translatePartialLoader.addPart('instEmployee');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            });
    });
