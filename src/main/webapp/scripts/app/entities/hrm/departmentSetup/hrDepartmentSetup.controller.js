'use strict';

angular.module('stepApp')
    .controller('HrDepartmentSetupController',
    ['$rootScope','$scope', '$state', 'HrDepartmentSetup', 'HrDepartmentSetupSearch', 'ParseLinks',
        function ($rootScope, $scope, $state, HrDepartmentSetup, HrDepartmentSetupSearch, ParseLinks) {

            $scope.hrDepartmentSetups = [];
            $scope.predicate = 'id';
            $scope.reverse = true;
            $scope.page = 0;
            $scope.stateName = "hrDepartmentSetup";

            $scope.loadAll = function()
            {
                if($rootScope.currentStateName == $scope.stateName){
                    $scope.page = $rootScope.pageNumber;
                }
                else {
                    $rootScope.pageNumber = $scope.page;
                    $rootScope.currentStateName = $scope.stateName;
                }
                HrDepartmentSetup.query({page: $scope.page, size: 2000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.totalItems = headers('X-Total-Count');
                    $scope.hrDepartmentSetups = result;
                });
            };
            $scope.loadPage = function(page)
            {
                $rootScope.currentStateName = $scope.stateName;
                $rootScope.pageNumber = page;
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();


            $scope.search = function () {
                HrDepartmentSetupSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.hrDepartmentSetups = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.hrDepartmentSetup = {
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

        }]);
/*hrDepartmentSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrDepartmentSetupDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'HrDepartmentSetup','Principal','User','DateUtils','MiscTypeSetupByCategory','HrEmpWorkAreaDtlInfoByStatus','HrDepartmentHeadSetupBySetupTypeAndStatus','HrDepartmentSetupUniqueness',
        'InstCategoryByStatusAndType','InstituteByCategory','Institute','HrDepartmentHeadInfoByDept','HrEmployeeInfosByDesigLevel','HrWingSetupByStatus','InstLevelByCategory','InstituteByTypeAndLevelAndCategory','HrDepartmentSetupByOrgInstType',
        function($scope, $rootScope, $stateParams, $state, entity, HrDepartmentSetup, Principal, User, DateUtils, MiscTypeSetupByCategory,HrEmpWorkAreaDtlInfoByStatus,HrDepartmentHeadSetupBySetupTypeAndStatus,HrDepartmentSetupUniqueness,
                 InstCategoryByStatusAndType,InstituteByCategory,Institute,HrDepartmentHeadInfoByDept,HrEmployeeInfosByDesigLevel,HrWingSetupByStatus,InstLevelByCategory,InstituteByTypeAndLevelAndCategory,HrDepartmentSetupByOrgInstType) {

            $scope.hrDepartmentSetup = entity;
            //$scope.hrDepartmentHeadList = HrDepartmentHeadSetupByStatus.get({stat:'true'});
            $scope.hrWingSetups = HrWingSetupByStatus.get({page: $scope.page, size: 2000,stat:'true'});
            $scope.instituteList = [];
            $scope.sec = false;
            $scope.dept = true;
            $scope.deptMappingList = [];
            $scope.removeItems = [];
            $scope.hrDepartmentHeadList =[];

            $scope.loadInstLevel = function(instCategory){
                $scope.instLevels = InstLevelByCategory.query({page: $scope.page, size: 2000,catId:instCategory.id});
            };
            InstCategoryByStatusAndType.query({page: $scope.page, size: 2000,activeStatus:true,instType:'Government'}, function (result) {
                $scope.instCategories =result;
            });
            $scope.load = function(id) {
                HrDepartmentSetup.get({id : id}, function(result) {
                    $scope.hrDepartmentSetup = result;
                });
            };

            $scope.getDeptList =  function(setupType,index){
                HrDepartmentHeadSetupBySetupTypeAndStatus.query({page: $scope.page, size: 2000,stat:'true',setupType:setupType},function(result){
                    $scope.hrDepartmentHeadList[index] = result;
                });
            }


            $scope.loadInstituteInfo = function(category,level){
                console.log("level and cat id");
                console.log(category);
                console.log(level);
                $scope.instituteList = [];
                if(level != null || category != null){
                    $scope.instituteList = [];
                    InstituteByTypeAndLevelAndCategory.query({page: $scope.page, size: 2000,level:level.id,category:category.id,type:'Govt'},function(result){
                        $scope.instituteList = result;
                        console.log("inst listttt...");
                        console.log($scope.instituteList);

                    });
                }else{
                    $scope.instituteList = [];
                }
            }

            $scope.clearAllList=function() {
                $scope.deptMappingList=[];
                $scope.hrDepartmentSetup.organizationCategory=null;
                $scope.hrDepartmentSetup.organizationInfo=null;
                $scope.hrDepartmentSetup.instCategory=null;
                $scope.hrDepartmentSetup.instlevel=null;
                $scope.hrDepartmentSetup.institute=null;
            };

            $scope.instituteChange=function (hrDeptSetup) {
                $scope.deptMappingList=[];
                $scope.instituteOrganizationChange(hrDeptSetup);
            };

            $scope.instituteOrganizationChange = function(hrDeptSetup) {
                var orgInstId=($scope.hrDepartmentSetup.organizationType==="Organization")?hrDeptSetup.organizationInfo.id:hrDeptSetup.institute.id;
                $scope.deptMappingList=[];

                HrDepartmentSetupByOrgInstType.query({orgType:$scope.hrDepartmentSetup.organizationType,orgId:orgInstId},function (results) {
                    $scope.hrDepartmentSetupList =results;
                    console.log($scope.hrDepartmentSetupList);

                    if($scope.hrDepartmentSetupList.length >0){
                        $scope.hrDepartmentSetup.wingInfo=$scope.hrDepartmentSetupList[0].wingInfo;
                        angular.forEach($scope.hrDepartmentSetupList,function (listResult,key) {
                            $scope.deptMappingList.push(
                                {   setupType:listResult.departmentInfo.setupType,
                                    activeStatus: listResult.departmentInfo.activeStatus,
                                    id: listResult.id,
                                    departmentInfo: listResult.departmentInfo
                                }
                            );
                            $scope.pgsType(listResult.departmentInfo.setupType,key);
                        });
                    }


                });
            }

            if($stateParams.id !=null){
                HrDepartmentSetup.get({id : $stateParams.id}, function(result) {
                    $scope.hrDepartmentSetup = result;

                    $scope.deptMappingList.push(
                        {   setupType:$scope.hrDepartmentSetup.departmentInfo.setupType,
                            activeStatus: $scope.hrDepartmentSetup.departmentInfo.activeStatus,
                            id: $scope.hrDepartmentSetup.id,
                            departmentInfo: $scope.hrDepartmentSetup.departmentInfo
                        }
                    );

                    $scope.pgsType($scope.hrDepartmentSetup.departmentInfo.setupType,0);
                });


                // $scope.dept = {};
                // $scope.dept.setupType = result.departmentInfo.setupType;
                // $scope.deptMappingList.push($scope.dept);
            }

            // {   setupType: $scope.hrDepartmentSetup.departmentInfo.setupType,
            //     activeStatus: $scope.hrDepartmentSetup.activeStatus,
            //     id: $scope.hrDepartmentSetup.id
            // }
            $scope.hremployeeinfos = [];
            HrEmployeeInfosByDesigLevel.get({desigLevel : 4}, function(result) {
                $scope.hremployeeinfos = result;
            });

            // if($stateParams.id != null){
            //     console.log($stateParams.id);
            // }else{
            //     $scope.deptMappingList.push(
            //         {   setupType:null,
            //             activeStatus: null,
            //             id: null
            //         }
            //     );
            // }
            $scope.addMore = function()
            {
                $scope.deptMappingList.push(
                    {   setupType:null,
                        activeStatus: null,
                        id: null
                    }
                );
            };

            var removeList=[];
            $scope.remove = function () {
                removeList.push($scope.deptMappingList.pop().id);
            }

            $scope.parentDeptId = 0;
            $scope.getDepartmentHeadList = function ()
            {
                if($stateParams.id)
                {
                    console.log("Load head info list parent id: "+$stateParams.id);
                    $scope.parentDeptId = $stateParams.id;
                    HrDepartmentHeadInfoByDept.get({deptId : $stateParams.id}, function(result) {
                        console.log("totalResult: "+result.length);
                        $scope.hrDepartmentHeadInfos = result;
                    });
                }
                else
                {
                    $scope.parentDeptId = 0;
                }
                console.log("list parentWingId: "+$scope.parentDeptId);
            };

            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                        console.log("loggedInUser: "+JSON.stringify($scope.loggedInUser));
                        $scope.getDepartmentHeadList();
                    });
                });
            };

            $scope.getLoggedInUser();

            $scope.orgCategoryList  = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});
            $scope.orgInfoList      = HrEmpWorkAreaDtlInfoByStatus.get({stat:'true'});
            $scope.orgInfoListFltr  = $scope.orgInfoList;

            //$scope.instCategoryList     = InstCategory.query();
            //$scope.instituteListAll     = Institute.query({size:2000});
            //$scope.instituteList        = $scope.instituteListAll;

            $scope.loadOrganizationInfoByCategory = function(orgCategory)
            {
                console.log(JSON.stringify(orgCategory)+", len: "+$scope.orgInfoList.length);
                $scope.orgInfoListFltr = [];
                angular.forEach($scope.orgInfoList,function(orgInfo)
                {
                    if(orgCategory.id == orgInfo.workArea.id){
                        $scope.orgInfoListFltr.push(orgInfo);
                    }
                });
            };

            //$scope.loadInstituteByCategory = function(categoryInfo)
            //{
            //    console.log(JSON.stringify(categoryInfo));
            //    /*InstituteByCategory.get({cat : categoryInfo.id}, function(result) {
            //     $scope.instituteList = result;
            //     });*/
            //
            //    $scope.instituteList = [];
            //    angular.forEach($scope.instituteListAll,function(institute)
            //    {
            //
            //        if(institute.instCategory != null)
            //        {
            //            console.log('$$$$$$$$$$');
            //            console.log(institute);
            //            console.log(categoryInfo.id);
            //            if(categoryInfo.id == institute.instCategory.id){
            //                console.log('***********');
            //                console.log(institute);
            //                $scope.instituteList.push(institute);
            //            }
            //        }
            //    });
            //};

            $scope.departmentAlreadyExist = false;
            $scope.checkDepartmentUniqByCategory = function()
            {

                var refId = 0; var isValidData = false;
                if($scope.hrDepartmentSetup.organizationType=='Organization')
                {
                    if($scope.hrDepartmentSetup.organizationInfo!=null)
                    {
                        refId = $scope.hrDepartmentSetup.organizationInfo.id;
                        isValidData = true;
                    }
                }
                else{
                    if($scope.hrDepartmentSetup.institute!=null)
                    {
                        refId = $scope.hrDepartmentSetup.institute.id;
                        isValidData = true;
                    }
                }
                if($scope.hrDepartmentSetup.departmentInfo != null && isValidData == true)
                {
                    $scope.editForm.departmentInfo.$pending = true;
                    console.log("OrgId: "+$scope.hrDepartmentSetup.organizationType+", deptid: "+$scope.hrDepartmentSetup.departmentInfo.id+", refId: "+refId);

                    HrDepartmentSetupUniqueness.get({orgtype:$scope.hrDepartmentSetup.organizationType, deptid: $scope.hrDepartmentSetup.departmentInfo.id, refid:refId}, function(result)
                    {
                        console.log(JSON.stringify(result));
                        $scope.isSaving = !result.isValid;
                        $scope.editForm.departmentInfo.$pending = false;
                        if(result.isValid)
                        {
                            console.log("valid");
                            $scope.departmentAlreadyExist = false;
                        }
                        else
                        {
                            console.log("not valid");
                            $scope.departmentAlreadyExist = true;
                        }
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.departmentInfo.$pending = false;
                    });
                }
                $scope.instituteOrganizationChange($scope.hrDepartmentSetup);

            };

            $scope.pgsType = function(stype,index)
            {
                $scope.getDeptList(stype,index);
                if(stype == 'Department'){
                    $scope.dept = true;
                    $scope.sec = false;
                }else if(stype == 'Section'){
                    $scope.sec = true;
                    $scope.dept = false;
                }else {
                    $scope.sec = false;
                    $scope.dept = false;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrDepartmentSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrDepartmentSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            var objList=[];
            $scope.save = function ()
            {
                $scope.isSaving = true;

                angular.forEach($scope.deptMappingList,function(obj){
                    console.log('^^^^^^^^^^^^^^^^^6');
                    console.log(obj);
                    obj.organizationType = $scope.hrDepartmentSetup.organizationType;
                    obj.organizationCategory = $scope.hrDepartmentSetup.organizationCategory;
                    obj.organizationInfo = $scope.hrDepartmentSetup.organizationInfo;
                    obj.institute = $scope.hrDepartmentSetup.institute;
                    obj.organizationType = $scope.hrDepartmentSetup.organizationType;
                    obj.wingInfo = $scope.hrDepartmentSetup.wingInfo;
                    obj.activeStatus =  true;
                    obj.instCategory = $scope.hrDepartmentSetup.instCategory;

                    if (obj.id != null){
                        obj.updateBy = $scope.loggedInUser.id;
                        obj.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                          HrDepartmentSetup.update(obj);
                        //$rootScope.setWarningMessage('stepApp.hrDepartmentSetup.updated');
                    }else{
                        obj.createBy = $scope.loggedInUser.id;
                        obj.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        //obj = $scope.hrDepartmentSetup;
                          HrDepartmentSetup.save(obj);
                        //$rootScope.setSuccessMessage('stepApp.hrDepartmentSetup.created');
                    }
                });
                angular.forEach(removeList,function(value,key){
                   HrDepartmentSetup.delete({id:value});
                });
                $state.go('hrDepartmentSetup',{},{reload:true});

            };

            $scope.clear = function() {
                $state.go('hrDepartmentSetup',{},{reload:true});
            };
        }]);
/*hrDepartmentSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrDepartmentSetup','hrDepartmentHeadInfoByDeptSetupId',
            function ($scope, $rootScope, $stateParams, entity, HrDepartmentSetup,hrDepartmentHeadInfoByDeptSetupId) {
                $scope.hrDepartmentSetup = entity;
                $scope.load = function (id) {
                    HrDepartmentSetup.get({id: id}, function(result) {
                        $scope.hrDepartmentSetup = result;
                    });
                    console.log($stateParams.id);
                    $scope.hrDeptHeadInfo=hrDepartmentHeadInfoByDeptSetupId.get({deptSetupId:$stateParams.id},function(result){
                        $scope.hrDeptHeadInfo=result;
                    });
                };

                $scope.load();
                var unsubscribe = $rootScope.$on('stepApp:hrDepartmentSetupUpdate', function(event, result) {
                    $scope.hrDepartmentSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrDepartmentSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentSetupDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrDepartmentSetup',
            function($scope, $rootScope, $modalInstance, entity, HrDepartmentSetup) {

                $scope.hrDepartmentSetup = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrDepartmentSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrDepartmentSetup.deleted');
                        });
                };

            }]);

/*New  Controller Added by Bappi*/
angular.module('stepApp')
    .controller('HrDepartmentAndSectionListController',
        ['$rootScope','$scope', '$state', 'HrDepartmentSetup', 'HrDepartmentSetupSearch', 'ParseLinks','DepartmentByInstituteAndType','HrDepartmentHeadInfoList',
            function ($rootScope, $scope, $state, HrDepartmentSetup, HrDepartmentSetupSearch, ParseLinks,DepartmentByInstituteAndType,HrDepartmentHeadInfoList) {

                $scope.hrDepartmentSetups = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrDepartmentSetup";
                $scope.InstEmployeeList = [];

                $scope.loadAll = function()
                {
                    if($rootScope.currentStateName == $scope.stateName){
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrDepartmentSetup.query({page: $scope.page, size: 2000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrDepartmentSetups = result;
                    });
                };
                $scope.loadPage = function(page)
                {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                //$scope.loadAll();


                $scope.search = function () {
                    HrDepartmentSetupSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.hrDepartmentSetups = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrDepartmentSetup = {
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.getDeptList =  function(setupType){
                    DepartmentByInstituteAndType.query({page: $scope.page, size: 2000,setupType:setupType},function(result){
                        console.log(result);
                        $scope.hrDepartmentSetups = result;
                    });
                }
                $scope.getHeadOfInstitute = function(){

                    HrDepartmentHeadInfoList.query({page: $scope.page, size: 2000,headStatus:true},function(result){
                        $scope.headInfoList = result;
                        console.log(result);
                    });
                }
                $scope.getDeptList('Department');
        }]);
