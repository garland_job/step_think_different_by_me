'use strict';

angular.module('stepApp')
    .controller('HrEmpChildrenInfoController',
        ['$rootScope', '$scope', '$state', 'HrEmpChildrenInfo', 'HrEmpChildrenInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrEmpChildrenInfo, HrEmpChildrenInfoSearch, ParseLinks) {

                $scope.hrEmpChildrenInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpChildrenInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpChildrenInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpChildrenInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

                $scope.search = function () {
                    HrEmpChildrenInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpChildrenInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpChildrenInfo = {
                        childrenName: null,
                        childrenNameBn: null,
                        dateOfBirth: null,
                        gender: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpChildrenInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpChildrenInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpChildrenInfo', 'HrEmployeeInfo', 'Principal', 'User', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmployeeInfoByWorkArea', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmpChildrenInfo, HrEmployeeInfo, Principal, User, DateUtils, MiscTypeSetupByCategory, HrEmployeeInfoByWorkArea, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpChildrenInfo = entity;
            if ($stateParams.id != null) {
                HrEmpChildrenInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpChildrenInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });

            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.load = function (id) {
                HrEmpChildrenInfo.get({id: id}, function (result) {
                    $scope.hrEmpChildrenInfo = result;
                });
            };

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };
            $scope.loadHrEmployee = function (children) {
                HrEmployeeInfoByEmployeeId.get({id: children.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpChildrenInfo.employeeInfo = result;
                });
            }
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpChildrenInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpChildrenInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.isSaving = true;
                        $scope.hrEmpChildrenInfo.updateBy = result.id;
                        $scope.hrEmpChildrenInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        $scope.hrEmpChildrenInfo.logStatus = 6;
                        if ($scope.hrEmpChildrenInfo.id != null) {
                            HrEmpChildrenInfo.update($scope.hrEmpChildrenInfo, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrEmpChildrenInfo.updated');
                        }
                        else {
                            $scope.hrEmpChildrenInfo.logId = 0;
                            $scope.hrEmpChildrenInfo.createBy = result.id;
                            $scope.hrEmpChildrenInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrEmpChildrenInfo.save($scope.hrEmpChildrenInfo, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrEmpChildrenInfo.created');
                        }
                    });
                });
            };

            $scope.clear = function () {

            };
        }]);
/*hrEmpChildrenInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpChildrenInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpChildrenInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpChildrenInfo, HrEmployeeInfo) {
                $scope.hrEmpChildrenInfo = entity;
                $scope.load = function (id) {
                    HrEmpChildrenInfo.get({id: id}, function (result) {
                        $scope.hrEmpChildrenInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpChildrenInfoUpdate', function (event, result) {
                    $scope.hrEmpChildrenInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpChildrenInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpChildrenInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpChildrenInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpChildrenInfo) {

                $scope.hrEmpChildrenInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpChildrenInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpChildrenInfo.deleted');
                };

            }]);
/*hrEmpChildrenInfo-approval.controller.js*/

angular.module('stepApp')
    .controller('HrEmpChildrenInfoApprovalController',
        ['$scope', '$rootScope', '$stateParams', '$modalInstance', 'HrEmpChildrenInfoApprover', 'HrEmpChildrenInfoApproverLog',
            function ($scope, $rootScope, $stateParams, $modalInstance, HrEmpChildrenInfoApprover, HrEmpChildrenInfoApproverLog) {
                $scope.hrEmpChildrenInfo = {};
                $scope.hrEmpChildrenInfoLog = {};
                $scope.isApproved = true;

                $scope.load = function (id) {
                    HrEmpChildrenInfoApproverLog.get({entityId: $stateParams.id}, function (result) {
                        console.log("HrEmpChildrenInfoApproverLog");
                        $scope.hrEmpChildrenInfo = result.entityObject;
                        $scope.hrEmpChildrenInfoLog = result.entityLogObject;
                    });
                };


                $scope.applyApproval = function (actionType) {
                    var approvalObj = $scope.initApprovalObject($scope.hrEmpChildrenInfo.id, $scope.logComments, actionType);
                    console.log("Children approval processing..." + JSON.stringify(approvalObj));
                    HrEmpChildrenInfoApprover.update(approvalObj, function (result) {
                        $modalInstance.dismiss('cancel');
                        $rootScope.$emit('onEntityApprovalProcessCompleted', result);
                    });
                    $modalInstance.dismiss('cancel');
                };

                $scope.initApprovalObject = function (entityId, logComments, actionType) {
                    return {
                        entityId: entityId,
                        logComments: logComments,
                        actionType: actionType
                    };
                };

                $scope.load();

                var unsubscribe = $rootScope.$on('stepApp:hrEmpChildrenInfoUpdate', function (event, result) {
                    $scope.hrEmpChildrenInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };

            }]);
/*hrEmpChildrenInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpChildrenInfoProfileController',
    ['$scope', '$stateParams', '$rootScope', '$state', 'HrEmpChildrenInfo', 'HrEmployeeInfo', 'Principal', 'User', 'DateUtils',
        function ($scope, $stateParams, $rootScope, $state, HrEmpChildrenInfo, HrEmployeeInfo, Principal, User, DateUtils) {

            $scope.hrEmpChildrenInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function (id) {
                HrEmpChildrenInfo.get({id: id}, function (result) {
                    $scope.hrEmpChildrenInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadChildProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpChildrenInfo.query({id: 'my'}, function (result) {
                    //console.log("result: "+JSON.stringify(result));
                    $scope.hrEmpChildrenInfoList = result;
                    if ($scope.hrEmpChildrenInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpChildrenInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpChildrenInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpChildrenInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpChildrenInfoList.length > 1) {
                            var indx = $scope.hrEmpChildrenInfoList.indexOf(modelInfo);
                            $scope.hrEmpChildrenInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpChildrenInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        childrenName: null,
                        childrenNameBn: null,
                        dateOfBirth: null,
                        gender: null,
                        activeStatus: true,
                        logId: 0,
                        logStatus: 1,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    childrenName: null,
                    childrenNameBn: null,
                    dateOfBirth: null,
                    gender: null,
                    activeStatus: true,
                    logId: 0,
                    logStatus: 1,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpChildrenInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpChildrenInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $scope.viewMode = true;
            };


            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpChildrenInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEmpChildrenInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                }else{
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpChildrenInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpChildrenInfo.childrenName', hrEmpChildrenInfo.childrenName, 'text', 150, 5, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpChildrenInfo.childrenNameBn', hrEmpChildrenInfo.childrenNameBn, 'text', 150, 5, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpChildrenInfo.dateOfBirth', DateUtils.convertLocaleDateToDMY(hrEmpChildrenInfo.dateOfBirth), 'date', 60, 5, 'date', true, '', ''));
                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };
        }]);
