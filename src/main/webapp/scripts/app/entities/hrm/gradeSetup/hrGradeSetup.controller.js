'use strict';

angular.module('stepApp')
    .controller('HrGradeSetupController',
     ['$scope', '$state', 'HrGradeSetup', 'HrGradeSetupSearch', 'ParseLinks',
     function ($scope, $state, HrGradeSetup, HrGradeSetupSearch, ParseLinks) {

        $scope.hrGradeSetups = [];
        $scope.predicate = 'gradeCode';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.loadAll = function()
        {
            console.log("pg: "+$scope.page+", pred:"+$scope.predicate+", rev: "+$scope.reverse);
            HrGradeSetup.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrGradeSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrGradeSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrGradeSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrGradeSetup = {
                gradeCode: null,
                gradeDetail: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);

/*hrGradeSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrGradeSetupDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrGradeSetup', 'HrClassInfo','Principal','User','DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, HrGradeSetup, HrClassInfo, Principal, User, DateUtils) {

            $scope.hrGradeSetup = entity;
            $scope.hrclassinfos = HrClassInfo.query({id:'bystat'});
            $scope.load = function(id)
            {
                HrGradeSetup.get({id : id}, function(result) {
                    $scope.hrGradeSetup = result;
                });
            };

            var onSaveSuccess = function (result)
            {
                $scope.$emit('stepApp:hrGradeSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrGradeSetup');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.isSaving = true;
                        $scope.hrGradeSetup.updateBy = result.id;
                        $scope.hrGradeSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.hrGradeSetup.id != null)
                        {
                            HrGradeSetup.update($scope.hrGradeSetup, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrGradeSetup.updated');
                        }
                        else
                        {
                            $scope.hrGradeSetup.createBy = result.id;
                            $scope.hrGradeSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrGradeSetup.save($scope.hrGradeSetup, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrGradeSetup.created');
                        }
                    });
                });
            };

            $scope.clear = function() {

            };
        }]);

/*hrGradeSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrGradeSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrGradeSetup', 'HrClassInfo',
            function ($scope, $rootScope, $stateParams, entity, HrGradeSetup, HrClassInfo) {
                $scope.hrGradeSetup = entity;
                $scope.load = function (id) {
                    HrGradeSetup.get({id: id}, function(result) {
                        $scope.hrGradeSetup = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrGradeSetupUpdate', function(event, result) {
                    $scope.hrGradeSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrGradeSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrGradeSetupDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrGradeSetup',
            function($scope, $rootScope, $modalInstance, entity, HrGradeSetup)
            {
                $scope.hrGradeSetup = entity;
                $scope.clear = function()
                {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id)
                {
                    HrGradeSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrGradeSetup.deleted');

                        });
                };
            }]);


