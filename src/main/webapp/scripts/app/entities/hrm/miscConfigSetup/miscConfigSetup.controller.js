'use strict';

angular.module('stepApp')
    .controller('MiscConfigSetupController', function ($scope, MiscConfigSetup, MiscConfigSetupSearch, ParseLinks) {
        $scope.miscConfigSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MiscConfigSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.miscConfigSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            MiscConfigSetup.get({id: id}, function(result) {
                $scope.miscConfigSetup = result;
                $('#deleteMiscConfigSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            MiscConfigSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteMiscConfigSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            MiscConfigSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.miscConfigSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.miscConfigSetup = {
                propertyName: null,
                propertyTitle: null,
                propertyValue: null,
                propertyDataType: null,
                propertyValueMax: null,
                propertyDesc: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*miscConfigSetup-dialog.controller.js*/

angular.module('stepApp').controller('MiscConfigSetupDialogController',
    ['$scope', '$state','$stateParams', '$rootScope', 'entity', 'MiscConfigSetup','MiscConfigSetupByName','Principal','User','DateUtils',
        function($scope, $state, $stateParams, $rootScope, entity, MiscConfigSetup,MiscConfigSetupByName,Principal,User,DateUtils) {

            $scope.miscConfigSetup = entity;
            $scope.load = function(id) {
                MiscConfigSetup.get({id : id}, function(result) {
                    $scope.miscConfigSetup = result;
                });
            };

            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:miscConfigSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('miscConfigSetup');
            };


            $scope.propertyNameAlreadyExist = false;
            $scope.checkPropertyByName = function()
            {
                console.log("NameCheck=> property: "+$scope.miscConfigSetup.propertyName);
                if($scope.miscConfigSetup.propertyName != null && $scope.miscConfigSetup.propertyName.length > 0)
                {
                    $scope.editForm.propertyName.$pending = true;
                    MiscConfigSetupByName.get({ name: $scope.miscConfigSetup.propertyName}, function(result)
                    {
                        console.log(JSON.stringify(result));
                        $scope.editForm.propertyName.$pending = false;
                        if(result!=null)
                        {
                            $scope.propertyNameAlreadyExist = true;
                            $scope.isSaving = false;
                        }
                        else
                        {
                            $scope.propertyNameAlreadyExist = false;
                            $scope.isSaving = true;
                        }
                    },function(response)
                    {
                        console.log("data connection failed");
                        $scope.editForm.propertyName.$pending = false;
                    });
                }

            };

            $scope.save = function ()
            {
                $scope.miscConfigSetup.updateBy = $scope.loggedInUser.id;
                $scope.miscConfigSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.miscConfigSetup.id != null)
                {
                    MiscConfigSetup.update($scope.miscConfigSetup, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.miscConfigSetup.updated');
                }
                else
                {
                    $scope.miscConfigSetup.createBy = $scope.loggedInUser.id;
                    $scope.miscConfigSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    MiscConfigSetup.save($scope.miscConfigSetup, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.miscConfigSetup.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*miscConfigSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('MiscConfigSetupDetailController', function ($scope, $rootScope, $stateParams, entity, MiscConfigSetup) {
        $scope.miscConfigSetup = entity;
        $scope.load = function (id) {
            MiscConfigSetup.get({id: id}, function(result) {
                $scope.miscConfigSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:miscConfigSetupUpdate', function(event, result) {
            $scope.miscConfigSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
