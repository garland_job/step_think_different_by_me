'use strict';

angular.module('stepApp')
    .controller('HrEmploymentInfoController',
     ['$rootScope','$scope', '$state', 'HrEmploymentInfo', 'HrEmploymentInfoSearch', 'ParseLinks',
     function ($rootScope, $scope, $state, HrEmploymentInfo, HrEmploymentInfoSearch, ParseLinks) {

        $scope.hrEmploymentInfos = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 0;
        $scope.stateName = "hrEmploymentInfo";
        $scope.loadAll = function()
        {
            if($rootScope.currentStateName == $scope.stateName){
                $scope.page = $rootScope.pageNumber;
            }
            else {
                $rootScope.pageNumber = $scope.page;
                $rootScope.currentStateName = $scope.stateName;
            }
            HrEmploymentInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrEmploymentInfos = result;
            });
        };
        $scope.loadPage = function(page)
        {
            $rootScope.currentStateName = $scope.stateName;
            $rootScope.pageNumber = page;
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrEmploymentInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.hrEmploymentInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrEmploymentInfo = {
                presentInstitute: null,
                joiningDate: null,
                regularizationDate: null,
                jobConfNoticeNo: null,
                confirmationDate: null,
                officeOrderNo: null,
                officeOrderDate: null,
                prlDate: null,
                logId: null,
                logStatus: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*hrEmploymentInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmploymentInfoDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'HrEmploymentInfo', 'HrEmployeeInfoByWorkArea', 'HrEmplTypeInfo', 'HrPayScaleSetup','User','Principal','DateUtils','MiscTypeSetupByCategory','PrlLocalitySetInfo','PrlSalaryStructureInfoListByEmp','HrEmployeeOfInstitutes','HrEmployeeInfoByEmployeeId',
        function($scope, $rootScope, $stateParams, $state, entity, HrEmploymentInfo, HrEmployeeInfoByWorkArea, HrEmplTypeInfo, HrPayScaleSetup, User, Principal, DateUtils, MiscTypeSetupByCategory,PrlLocalitySetInfo,PrlSalaryStructureInfoListByEmp,HrEmployeeOfInstitutes,HrEmployeeInfoByEmployeeId) {

            $scope.hrEmploymentInfo = entity;
            if($stateParams.id !=null){
                HrEmploymentInfo.get({id : $stateParams.id},function(result){
                    $scope.hrEmploymentInfo = result;
                    if(result.employeeInfo.organizationType =='Institute'){
                        $scope.orgOrInst = 'Institute';
                    }else {
                        $scope.orgOrInst ='Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function(result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hrEmployeeInfo   = {};
            $scope.prlSalaryStructureInfo = {};
            $scope.hrempltypeinfos  = HrEmplTypeInfo.query({id:'bystat'});
            $scope.hrpayscalesetups = HrPayScaleSetup.query({id:'bystat'});
            $scope.prllocalitysetinfos = PrlLocalitySetInfo.query();

            $scope.load = function(id) {
                HrEmploymentInfo.get({id : id}, function(result) {
                    $scope.hrEmploymentInfo = result;
                });
            };

            $scope.hremployeeinfos  = [];0.
            $scope.workAreaList     = MiscTypeSetupByCategory.get({cat:'EmployeeWorkArea',stat:'true'});

            $scope.loadModelByWorkArea = function(workArea)
            {
                HrEmployeeInfoByWorkArea.get({areaid : workArea.id}, function(result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: "+result.length);
                });
            };

            $scope.loadHrEmployee = function(hrEmployment)
            {
                HrEmployeeInfoByEmployeeId.get({id: hrEmployment.employeeInfo.employeeId}, function (result){
                    console.log("result");
                    console.log(result);
                    $scope.hrEmploymentInfo.employeeInfo = result;
                });

            }

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.updatePrlDate = function()
            {
                var jnDt = $scope.hrEmploymentInfo.joiningDate;
                var prlDt = new Date(jnDt.getFullYear() + 61, jnDt.getMonth(), jnDt.getDate());

                $scope.hrEmploymentInfo.prlDate = prlDt;
                //console.log("JoinDt: "+jnDt+", PrlDt: "+prlDt);
                console.log("JoinDate: "+$scope.hrEmploymentInfo.joiningDate+", PrlDate: "+$scope.hrEmploymentInfo.prlDate);
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmploymentInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmploymentInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                $scope.isSaving = true;
                $scope.hrEmploymentInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmploymentInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmploymentInfo.id != null)
                {
                    $scope.hrEmploymentInfo.logId = 0;
                    $scope.hrEmploymentInfo.logStatus = 6;
                    HrEmploymentInfo.update($scope.hrEmploymentInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmploymentInfo.updated');
                }
                else
                {
                    $scope.hrEmploymentInfo.logId = 0;
                    $scope.hrEmploymentInfo.logStatus = 6;
                    $scope.hrEmploymentInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmploymentInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmploymentInfo.save($scope.hrEmploymentInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmploymentInfo.created');
                }
            };

            /*
            $scope.$watch('hrEmploymentInfo.employeeInfo', function()
            {
                $scope.counter=$scope.counter+1;
                var salStrucId = 0;
                console.log("ngwatch - "+$scope.counter);
                if($scope.hrEmploymentInfo.employeeInfo!=null && $scope.hrEmploymentInfo.employeeInfo.id!=null)
                {
                    console.log("ngwatch - "+$scope.counter+", empId: "+$scope.hrEmploymentInfo.employeeInfo.fullName);
                    $scope.hrEmployeeInfo = $scope.hrEmploymentInfo.employeeInfo;
                    $scope.loadEmployeePayScale();
                }
            });


            $scope.loadEmployeePayScale = function ()
            {
                console.log("loadEmployeePayScale id: "+$scope.hrEmployeeInfo.id);
                if($scope.hrEmployeeInfo && $scope.hrEmployeeInfo.id)
                {
                    PrlSalaryStructureInfoListByEmp.get({empid: $scope.hrEmployeeInfo.id}, function (result)
                    {
                        $scope.prlSalaryStructureInfo = result;
                        if(result.length < 1)
                        {
                            console.log("loadEmployeePayScale:, len: "+JSON.stringify(result));
                        }
                        else
                        {
                            $scope.prlSalaryStructureInfo = result[0];
                            console.log("loadEmployeePayScale: Found : "+JSON.stringify($scope.prlSalaryStructureInfo));
                        }

                    }, function (response) {
                        console.log("loadEmployeePayScale Error: "+response);
                    })
                }
            };
        */
            $scope.clear = function() {

            };
        }]);
/*hrEmploymentInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmploymentInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmploymentInfo', 'HrEmployeeInfo', 'HrEmplTypeInfo', 'HrPayScaleSetup',
            function ($scope, $rootScope, $stateParams, entity, HrEmploymentInfo, HrEmployeeInfo, HrEmplTypeInfo, HrPayScaleSetup) {
                $scope.hrEmploymentInfo = entity;
                $scope.load = function (id) {
                    HrEmploymentInfo.get({id: id}, function(result) {
                        $scope.hrEmploymentInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmploymentInfoUpdate', function(event, result) {
                    $scope.hrEmploymentInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmploymentInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmploymentInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance','entity', 'HrEmploymentInfo',
            function($scope, $rootScope, $modalInstance, entity, HrEmploymentInfo) {

                $scope.hrEmploymentInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmploymentInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEmploymentInfo.deleted');
                        });
                };

            }]);
/*hrEmploymentInfo-approval.controller.js*/

angular.module('stepApp')
    .controller('HrEmploymentInfoApprovalController',
        ['$scope', '$rootScope', '$stateParams', '$modalInstance', 'HrEmploymentInfoApprover', 'HrEmploymentInfoApproverLog',
            function ($scope, $rootScope, $stateParams, $modalInstance, HrEmploymentInfoApprover, HrEmploymentInfoApproverLog) {
                $scope.hrEmploymentInfo = {};
                $scope.hrEmploymentInfoLog = {};
                $scope.isApproved = true;

                $scope.load = function ()
                {
                    HrEmploymentInfoApproverLog.get({entityId: $stateParams.id}, function(result)
                    {
                        console.log("HrEmploymentInfoApproverLog");
                        $scope.hrEmploymentInfo = result.entityObject;
                        $scope.hrEmploymentInfoLog = result.entityLogObject;
                    });
                };

                $scope.applyApproval = function (actionType)
                {
                    var approvalObj = $scope.initApprovalObject($scope.hrEmploymentInfo.id, $scope.logComments, actionType);
                    console.log("Employment approval processing..."+JSON.stringify(approvalObj));
                    HrEmploymentInfoApprover.update(approvalObj, function(result)
                    {
                        $modalInstance.dismiss('cancel');
                        $rootScope.$emit('onEntityApprovalProcessCompleted', result);
                    });
                    $modalInstance.dismiss('cancel');
                };

                $scope.initApprovalObject = function(entityId, logComments, actionType)
                {
                    return {
                        entityId: entityId,
                        logComments:logComments,
                        actionType:actionType
                    };
                };

                $scope.load();

                var unsubscribe = $rootScope.$on('stepApp:hrEmploymentInfoUpdate', function(event, result) {
                    $scope.hrEmploymentInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };


            }]);
/*hrEmploymentInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmploymentInfoProfileController',
    ['$scope', '$stateParams', '$state', 'HrEmploymentInfo', 'HrEmployeeInfo', 'HrEmplTypeInfo', 'HrPayScaleSetup','User','Principal','DateUtils','PrlLocalitySetInfo','PrlSalaryStructureInfoListByEmp',
        function($scope, $stateParams, $state, HrEmploymentInfo, HrEmployeeInfo, HrEmplTypeInfo, HrPayScaleSetup, User, Principal, DateUtils,PrlLocalitySetInfo,PrlSalaryStructureInfoListByEmp) {

            $scope.hrEmploymentInfo = {};
            $scope.prlSalaryStructureInfo = {};
            $scope.hrempltypeinfos = HrEmplTypeInfo.query({id:'bystat'});
            $scope.hrpayscalesetups = HrPayScaleSetup.query({id:'bystat'});
            $scope.prllocalitysetinfos = PrlLocalitySetInfo.query();
            $scope.load = function(id) {
                HrEmploymentInfo.get({id : id}, function(result) {
                    $scope.hrEmploymentInfo = result;
                    console.log($scope.hrEmploymentInfo);
                    console.log("employment detail");
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();


            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function()
            {
                console.log("loadEmploymentProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmploymentInfo.query({id: 'my'}, function (result)
                {
                    //console.log("result: "+JSON.stringify(result));
                    if(result.length < 1)
                    {
                        //console.log("result:, len: "+JSON.stringify(result));
                        $scope.addMode = true;
                        $scope.hrEmploymentInfo = $scope.initiateModel();
                        $scope.hrEmploymentInfo.viewMode = true;
                        $scope.hrEmploymentInfo.viewModeText = "Add";
                        $scope.loadEmployee();
                    }
                    else
                    {
                        //console.log("result: data found: "+JSON.stringify(result));
                        $scope.hrEmploymentInfo = result[0];
                        $scope.hrEmployeeInfo = $scope.hrEmploymentInfo.employeeInfo;
                        $scope.hrEmploymentInfo.viewMode = true;
                        $scope.hrEmploymentInfo.viewModeText = "Edit";
                        $scope.hrEmploymentInfo.isLocked = false;
                        if($scope.hrEmploymentInfo.logStatus==0)
                        {
                            $scope.hrEmploymentInfo.isLocked = true;
                        }
                        $scope.updatePrlDate();
                        $scope.loadEmployeePayScale();
                    }

                }, function (response)
                {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function ()
            {
                console.log("loadEmployeeProfile addMode: "+$scope.addMode+", viewMode: "+$scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result)
                {
                    $scope.hrEmployeeInfo = result;
                    console.log("hr employee info my");
                    console.log($scope.hrEmployeeInfo);
                    $scope.hrEmploymentInfo.viewMode = true;
                    $scope.hrEmploymentInfo.viewModeText = "Add";
                    $scope.updatePrlDate();
                    $scope.loadEmployeePayScale();

                }, function (response) {
                    console.log("error: "+response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                    $scope.hrEmploymentInfo.viewMode = true;
                    $scope.hrEmploymentInfo.viewModeText = "Add";
                })
            };

            $scope.loadEmployeePayScale = function ()
            {
                console.log("loadEmployeePayScale empId: "+$scope.hrEmployeeInfo.id);
                if($scope.hrEmployeeInfo && $scope.hrEmployeeInfo.id)
                {
                    PrlSalaryStructureInfoListByEmp.get({empid: $scope.hrEmployeeInfo.id}, function (result)
                    {
                        $scope.prlSalaryStructureInfo = result;
                        if(result.length < 1)
                        {
                            console.log("loadEmployeePayScale:, len: "+JSON.stringify(result));
                        }
                        else
                        {
                            $scope.prlSalaryStructureInfo = result[0];
                            //console.log("loadEmployeePayScale: Found : "+JSON.stringify($scope.prlSalaryStructureInfo));
                            console.log("loadEmployeePayScale: Found ");
                        }

                    }, function (response) {
                        console.log("loadEmployeePayScale Error: "+response);
                    })
                }
            };

            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo)
            {
                if(modelInfo.viewMode)
                {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else
                {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if(modelInfo.id==null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.updatePrlDate = function()
            {
                console.log("birthdate: "+$scope.hrEmployeeInfo.birthDate+", nid:"+$scope.hrEmployeeInfo.quota);

                var prlYear = 61;
                if($scope.hrEmployeeInfo.quota=='Others' || $scope.hrEmployeeInfo.quota == 'Other' || $scope.hrEmployeeInfo.quota == 'others' || $scope.hrEmployeeInfo.quota == 'other')
                {
                    prlYear = 59;
                }
                var brDt = new Date($scope.hrEmployeeInfo.birthDate);
                var oldPrl = new Date($scope.hrEmploymentInfo.prlDate);
                console.log("BirthDate: "+brDt+", PrdYear: "+prlYear);
                var prlDt = new Date(brDt.getFullYear() + prlYear, brDt.getMonth(), brDt.getDate());
                $scope.hrEmploymentInfo.prlDate = prlDt;

                $scope.hrEmploymentInfo.prlDateOld = oldPrl;

                if(oldPrl.getFullYear()==prlDt.getFullYear() &&  oldPrl.getMonth()==prlDt.getMonth() && oldPrl.getDate()==prlDt.getDate() )
                {
                    $scope.prlDateChanged = false;
                    console.log("Same Date...");
                }
                else{
                    $scope.prlDateChanged = true;
                    console.log("Not Same Date...");
                }

                console.log("OldPrl: "+$scope.hrEmploymentInfo.prlDate+", GenPrl: "+$scope.hrEmploymentInfo.prlDateOld);
            };

            $scope.initiateModel = function()
            {
                return {
                    viewMode:true,
                    viewModeText:'Add',
                    presentInstitute: null,
                    joiningDate: null,
                    regularizationDate: null,
                    jobConfNoticeNo: null,
                    confirmationDate: null,
                    officeOrderNo: null,
                    officeOrderDate: null,
                    logId: null,
                    logStatus: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmploymentInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmploymentInfo.id=result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo)
            {
                $scope.isSaving = true;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                //console.log("result: data found: "+JSON.stringify(modelInfo));
                if (modelInfo.id != null)
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    HrEmploymentInfo.update(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    modelInfo.isLocked = true;
                }
                else
                {
                    modelInfo.logId = 0;
                    modelInfo.logStatus = 0;
                    modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                    modelInfo.createBy = $scope.loggedInUser.id;
                    modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                    HrEmploymentInfo.save(modelInfo, onSaveSuccess, onSaveError);
                    modelInfo.viewMode = true;
                    modelInfo.viewModeText = "Edit";
                    $scope.addMode = false;
                    modelInfo.isLocked = true;
                }
            };

            $scope.clear = function() {

            };
        }]);
