'use strict';

angular.module('stepApp')
    .controller('HrSpouseInfoController',
        ['$rootScope', '$scope', '$state', 'HrSpouseInfo', 'HrSpouseInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrSpouseInfo, HrSpouseInfoSearch, ParseLinks) {

                $scope.hrSpouseInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrSpouseInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }

                    //console.log("pg: "+$scope.page+", pred:"+$scope.predicate+", rootPage: "+$rootScope.pageNumber+", rootSc: "+$rootScope.currentStateName+", lcst:"+$scope.stateName);
                    HrSpouseInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrSpouseInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrSpouseInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrSpouseInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrSpouseInfo = {
                        spouseName: null,
                        spouseNameBn: null,
                        birthDate: null,
                        gender: null,
                        relationship: null,
                        isNominee: null,
                        occupation: null,
                        organization: null,
                        designation: null,
                        nationalId: null,
                        emailAddress: null,
                        contactNumber: null,
                        address: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrSpouseInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrSpouseInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrSpouseInfo', 'HrEmployeeInfoByWorkArea', 'Principal', 'User', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $state, entity, HrSpouseInfo, HrEmployeeInfoByWorkArea, Principal, User, DateUtils, MiscTypeSetupByCategory, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrSpouseInfo = entity;

            if ($stateParams.id != null) {
                HrSpouseInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrSpouseInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }
            $scope.load = function (id) {
                HrSpouseInfo.get({id: id}, function (result) {
                    $scope.hrSpouseInfo = result;
                });
            };

            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (spouse) {
                HrEmployeeInfoByEmployeeId.get({id: spouse.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrSpouseInfo.employeeInfo = result;
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            //$scope.phoneNumbr = /^\+?\d{11}?\d{13}$/;

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrSpouseInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrSpouseInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.isSaving = true;
                        $scope.hrSpouseInfo.updateBy = result.id;
                        $scope.hrSpouseInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                        if ($scope.hrSpouseInfo.id != null) { /*TODO: && $scope.preCondition()*/
                            $scope.hrSpouseInfo.logId = 0;
                            $scope.hrSpouseInfo.logStatus = 6;
                            HrSpouseInfo.update($scope.hrSpouseInfo, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.hrSpouseInfo.updated');
                        } else { /*TODO: if ($scope.hrSpouseInfo.id == null && $scope.preCondition())*/
                            $scope.hrSpouseInfo.logId = 0;
                            $scope.hrSpouseInfo.logStatus = 6;
                            $scope.hrSpouseInfo.createBy = result.id;
                            $scope.hrSpouseInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            HrSpouseInfo.save($scope.hrSpouseInfo, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.hrSpouseInfo.created');
                        }
                    });
                });
            };


            /*$scope.preCondition = function(){
             $scope.isPreCondition = true;
             $scope.preConditionStatus = [];
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.spouseName', $scope.hrSpouseInfo.spouseName, 'text', 150, 5, 'atozAndAtoZ0-9.-', true, '', ''));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.birthDate', DateUtils.convertLocaleDateToDMY($scope.hrSpouseInfo.birthDate), 'date', 60, 5, 'date', true, '', ''));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.gender', $scope.hrSpouseInfo.gender, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.relationship', $scope.hrSpouseInfo.relationship, 'text', 8, 4, 'atozAndAtoZ', true, '', ''));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.occupation', $scope.hrSpouseInfo.occupation, 'text', 50, 5, 'atozAndAtoZ', true, '','' ));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.organization', $scope.hrSpouseInfo.organization, 'text', 100, 5, 'atozAndAtoZ', true, '','' ));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.designation', $scope.hrSpouseInfo.designation, 'text', 50, 5, 'atozAndAtoZ', true, '','' ));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.nationalId', $scope.hrSpouseInfo.nationalId, 'number', 17, 10, '0to9', true, '', ''));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.emailAddress', $scope.hrSpouseInfo.emailAddress, 'email', 50, 5, 'email', true, '', ''));
             $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.contactNumber', $scope.hrSpouseInfo.contactNumber, 'number', 11, 11, '0to9', true, '', ''));

             if($scope.preConditionStatus.indexOf(false) !== -1){
             $scope.isPreCondition = false;
             }else {
             $scope.isPreCondition = true;
             }
             console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
             console.log($scope.preConditionStatus);
             return $scope.isPreCondition;
             };*/

            $scope.clear = function () {

            };
        }]);
/*hrSpouseInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrSpouseInfoDetailController', function ($scope, $rootScope, $stateParams, entity, HrSpouseInfo, HrEmployeeInfo) {
        $scope.hrSpouseInfo = entity;
        $scope.load = function (id) {
            HrSpouseInfo.get({id: id}, function (result) {
                $scope.hrSpouseInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:hrSpouseInfoUpdate', function (event, result) {
            $scope.hrSpouseInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*hrSpouseInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrSpouseInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrSpouseInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrSpouseInfo) {

                $scope.hrSpouseInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrSpouseInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrSpouseInfo.deleted');
                };

            }]);
/*hrSpouseInfo-profile.controller.js*/

angular.module('stepApp').controller('HrSpouseInfoProfileController',
    ['$scope', '$stateParams', '$rootScope', '$state', 'HrSpouseInfo', 'HrEmployeeInfo', 'Principal', 'User', 'DateUtils',
        function ($scope, $stateParams, $rootScope, $state, HrSpouseInfo, HrEmployeeInfo, Principal, User, DateUtils) {

            $scope.hrSpouseInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();


            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadSpouseProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrSpouseInfo.query({id: 'my'}, function (result) {
                    //console.log("result:, len: "+JSON.stringify(result));

                    if (result.length < 1) {
                        //console.log("spouse is null");
                        $scope.addMode = true;
                        $scope.hrSpouseInfo = $scope.initiateModel();
                        $scope.hrSpouseInfo.viewMode = true;
                        $scope.hrSpouseInfo.viewModeText = "Edit";
                        console.log("InitSpouse:, len: " + JSON.stringify($scope.hrSpouseInfo));
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrSpouseInfo = result[0];
                        $scope.hrEmployeeInfo = $scope.hrSpouseInfo.employeeInfo;

                        //console.log("spouse is not null"+JSON.stringify($scope.hrSpouseInfo));
                        $scope.hrEmployeeInfo = $scope.hrSpouseInfo.employeeInfo;
                        $scope.hrSpouseInfo.viewMode = true;
                        $scope.hrSpouseInfo.viewModeText = "Edit";
                        $scope.hrSpouseInfo.isLocked = false;
                        if ($scope.hrSpouseInfo.logStatus == 0) {
                            $scope.hrSpouseInfo.isLocked = true;
                        }
                    }

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.hrSpouseInfo.viewMode = true;
                    $scope.hrSpouseInfo.viewModeText = "Add";

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                    $scope.hrSpouseInfo.viewMode = true;
                    $scope.hrSpouseInfo.viewModeText = "Add";
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null)
                        modelInfo.viewModeText = "Add";
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    spouseName: null,
                    spouseNameBn: null,
                    birthDate: null,
                    gender: null,
                    relationship: null,
                    isNominee: null,
                    occupation: null,
                    organization: null,
                    designation: null,
                    nationalId: null,
                    emailAddress: null,
                    contactNumber: null,
                    address: null,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.toDay = new Date();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrSpouseInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrSpouseInfo.id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            $scope.updateProfile = function (modelInfo) {

                $scope.isSaving = true;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;

                //console.log("model: "+JSON.stringify(modelInfo));
                if ($scope.preCondition()) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrSpouseInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrSpouseInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.spouseName', $scope.hrSpouseInfo.spouseName, 'text', 150, 5, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.birthDate', DateUtils.convertLocaleDateToDMY($scope.hrSpouseInfo.birthDate), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.gender', $scope.hrSpouseInfo.gender, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.relationship', $scope.hrSpouseInfo.relationship, 'text', 8, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.occupation', $scope.hrSpouseInfo.occupation, 'text', 50, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.organization', $scope.hrSpouseInfo.organization, 'text', 100, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.designation', $scope.hrSpouseInfo.designation, 'text', 50, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.nationalId', $scope.hrSpouseInfo.nationalId, 'number', 17, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.emailAddress', $scope.hrSpouseInfo.emailAddress, 'email', 50, 5, 'email', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrSpouseInfo.contactNumber', $scope.hrSpouseInfo.contactNumber, 'number', 11, 11, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {
            };
        }]);
