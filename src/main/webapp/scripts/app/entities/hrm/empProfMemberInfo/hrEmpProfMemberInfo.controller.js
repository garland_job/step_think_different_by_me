'use strict';

angular.module('stepApp')
    .controller('HrEmpProfMemberInfoController',
        ['$rootScope', '$scope', '$state', 'HrEmpProfMemberInfo', 'HrEmpProfMemberInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrEmpProfMemberInfo, HrEmpProfMemberInfoSearch, ParseLinks) {

                $scope.hrEmpProfMemberInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpProfMemberInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpProfMemberInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpProfMemberInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpProfMemberInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpProfMemberInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpProfMemberInfo = {
                        organizationName: null,
                        membershipNumber: null,
                        membershipDate: null,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpProfMemberInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpProfMemberInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpProfMemberInfo', 'HrEmployeeInfoByWorkArea', 'User', 'Principal', 'DateUtils', 'MiscTypeSetupByCategory', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmpProfMemberInfo, HrEmployeeInfoByWorkArea, User, Principal, DateUtils, MiscTypeSetupByCategory, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpProfMemberInfo = entity;

            if ($stateParams.id != null) {
                HrEmpProfMemberInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpProfMemberInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.load = function (id) {
                HrEmpProfMemberInfo.get({id: id}, function (result) {
                    $scope.hrEmpProfMemberInfo = result;
                });
            };

            $scope.hremployeeinfos = [];
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.memberCatList = MiscTypeSetupByCategory.get({cat: 'MembershipCategory', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (proMem) {
                console.log("load hr emp");
                HrEmployeeInfoByEmployeeId.get({id: proMem.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpProfMemberInfo.employeeInfo = result;
                });

            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpProfMemberInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpProfMemberInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpProfMemberInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpProfMemberInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpProfMemberInfo.id != null) {
                    $scope.hrEmpProfMemberInfo.logId = 0;
                    $scope.hrEmpProfMemberInfo.logStatus = 6;
                    HrEmpProfMemberInfo.update($scope.hrEmpProfMemberInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpProfMemberInfo.updated');
                }
                else {
                    $scope.hrEmpProfMemberInfo.logId = 0;
                    $scope.hrEmpProfMemberInfo.logStatus = 6;
                    $scope.hrEmpProfMemberInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpProfMemberInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpProfMemberInfo.save($scope.hrEmpProfMemberInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpProfMemberInfo.created');
                }
            };

            $scope.clear = function () {

            };
        }]);
/*hrEmpProfMemberInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpProfMemberInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpProfMemberInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpProfMemberInfo, HrEmployeeInfo) {
                $scope.hrEmpProfMemberInfo = entity;
                $scope.load = function (id) {
                    HrEmpProfMemberInfo.get({id: id}, function (result) {
                        $scope.hrEmpProfMemberInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpProfMemberInfoUpdate', function (event, result) {
                    $scope.hrEmpProfMemberInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpProfMemberInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpProfMemberInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpProfMemberInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpProfMemberInfo) {

                $scope.hrEmpProfMemberInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpProfMemberInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpProfMemberInfo.deleted');
                };

            }]);
/*hrEmpProfMemberInfo-approval.controller.js*/

angular.module('stepApp')
    .controller('HrEmpProfMemberInfoApprovalController',
        ['$scope', '$rootScope', '$stateParams', '$modalInstance', 'HrEmpProfMemberInfoApproverLog', 'HrEmpProfMemberInfoApprover',
            function ($scope, $rootScope, $stateParams, $modalInstance, HrEmpProfMemberInfoApproverLog, HrEmpProfMemberInfoApprover) {
                $scope.hrEmpProfMemberInfo = {};
                $scope.hrEmpProfMemberInfoLog = {};
                $scope.isApproved = true;

                $scope.load = function () {
                    HrEmpProfMemberInfoApproverLog.get({entityId: $stateParams.id}, function (result) {
                        console.log("HrEmpProfMemberInfoApproverLog");
                        $scope.hrEmpProfMemberInfo = result.entityObject;
                        $scope.hrEmpProfMemberInfoLog = result.entityLogObject;
                    });
                };


                $scope.applyApproval = function (actionType) {
                    var approvalObj = $scope.initApprovalObject($scope.hrEmpProfMemberInfo.id, $scope.logComments, actionType);
                    console.log("Professional membership approval processing..." + JSON.stringify(approvalObj));

                    HrEmpProfMemberInfoApprover.update(approvalObj, function (result) {
                        $modalInstance.dismiss('cancel');
                        $rootScope.$emit('onEntityApprovalProcessCompleted', result);
                    });
                    $modalInstance.dismiss('cancel');
                };

                $scope.initApprovalObject = function (entityId, logComments, actionType) {
                    return {
                        entityId: entityId,
                        logComments: logComments,
                        actionType: actionType
                    };
                };

                $scope.load();


                var unsubscribe = $rootScope.$on('stepApp:hrEmpProfMemberInfoUpdate', function (event, result) {
                    $scope.hrEmpProfMemberInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };

            }]);
/*hrEmpProfMemberInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpProfMemberInfoProfileController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'HrEmpProfMemberInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils', 'MiscTypeSetupByCategory',
        function ($scope, $rootScope, $stateParams, $state, HrEmpProfMemberInfo, HrEmployeeInfo, User, Principal, DateUtils, MiscTypeSetupByCategory) {

            $scope.hrEmpProfMemberInfo = {};
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.memberCatList = MiscTypeSetupByCategory.get({cat: 'MembershipCategory', stat: 'true'});

            $scope.load = function (id) {
                HrEmpProfMemberInfo.get({id: id}, function (result) {
                    $scope.hrEmpProfMemberInfo = result;
                });
            };

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadPreGovtJobProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpProfMemberInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpProfMemberInfoList = result;
                    if ($scope.hrEmpProfMemberInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpProfMemberInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpProfMemberInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpProfMemberInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpProfMemberInfoList.length > 1) {
                            var indx = $scope.hrEmpProfMemberInfoList.indexOf(modelInfo);
                            $scope.hrEmpProfMemberInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpProfMemberInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        organizationName: null,
                        membershipNumber: null,
                        membershipDate: null,
                        logId: null,
                        logStatus: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    organizationName: null,
                    membershipNumber: null,
                    membershipDate: null,
                    logId: null,
                    logStatus: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
            $scope.toDay = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpProfMemberInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpProfMemberInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 2;
                        HrEmpProfMemberInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                    else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 1;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                        HrEmpProfMemberInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpProfMemberInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpProfMemberInfo.organizationName', hrEmpProfMemberInfo.organizationName, 'text', 150, 4, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpProfMemberInfo.membershipNumber', hrEmpProfMemberInfo.membershipNumber, 'text', 150, 2, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpProfMemberInfo.membershipDate', DateUtils.convertLocaleDateToDMY(hrEmpProfMemberInfo.membershipDate), 'date', 60, 5, 'date', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };
        }]);
