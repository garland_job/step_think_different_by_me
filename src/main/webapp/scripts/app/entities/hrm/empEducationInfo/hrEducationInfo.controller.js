'use strict';

angular.module('stepApp')
    .controller('HrEducationInfoController',
        ['$rootScope', '$scope', '$state', 'DataUtils', 'HrEducationInfo', 'HrEducationInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, DataUtils, HrEducationInfo, HrEducationInfoSearch, ParseLinks) {

                $scope.hrEducationInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEducationInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEducationInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEducationInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEducationInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEducationInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEducationInfo = {
                        examTitle: null,
                        majorSubject: null,
                        certSlNumber: null,
                        sessionYear: null,
                        rollNumber: null,
                        instituteName: null,
                        gpaOrCgpa: null,
                        gpaScale: null,
                        passingYear: null,
                        certificateDoc: null,
                        certificateDocContentType: null,
                        certificateDocName: null,
                        transcriptDoc: null,
                        transcriptDocContentType: null,
                        transcriptDocName: null,
                        activeStatus: true,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEducationInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEducationInfoDialogController',
    ['$scope', '$rootScope', '$sce', '$stateParams', '$state', 'DataUtils', 'entity', 'HrEducationInfo', 'HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $sce, $stateParams, $state, DataUtils, entity, HrEducationInfo, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, User, Principal, DateUtils, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEducationInfo = entity;

            if ($stateParams.id != null) {
                HrEducationInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEducationInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});
            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (education) {
                HrEmployeeInfoByEmployeeId.get({id: education.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEducationInfo.employeeInfo = result;
                });
            }

            $scope.hremployeeinfos = [];
            $scope.educationBoardNames = MiscTypeSetupByCategory.get({cat: 'EducationBoard', stat: 'true'});
            $scope.educationLevelNames = MiscTypeSetupByCategory.get({cat: 'EducationLevel', stat: 'true'});


            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                        //console.log("loggedInUser: "+JSON.stringify($scope.loggedInUser));
                    });
                });
            };

            $scope.getLoggedInUser();

            $scope.load = function (id) {
                HrEducationInfo.get({id: id}, function (result) {
                    $scope.hrEducationInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEducationInfoUpdate', result);
                $state.go('hrEducationInfo');
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEducationInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEducationInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEducationInfo.id != null && $scope.preCondition()) {
                    $scope.hrEducationInfo.logId = 0;
                    $scope.hrEducationInfo.logStatus = 6;
                    HrEducationInfo.update($scope.hrEducationInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEducationInfo.updated');
                } else if ($scope.hrEducationInfo.id == null && $scope.preCondition()) {
                    $scope.hrEducationInfo.logId = 0;
                    $scope.hrEducationInfo.logStatus = 6;
                    $scope.hrEducationInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEducationInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEducationInfo.save($scope.hrEducationInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEducationInfo.created');
                }
            };


            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.instituteName', $scope.hrEducationInfo.instituteName, 'text', 100, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.examTitle', $scope.hrEducationInfo.examTitle, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.majorSubject', $scope.hrEducationInfo.majorSubject, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.certSlNumber', $scope.hrEducationInfo.certSlNumber, 'number', 15, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.sessionYear', $scope.hrEducationInfo.sessionYear, 'text', 9, 4, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.rollNumber', $scope.hrEducationInfo.rollNumber, 'number', 10, 6, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.gpaScale', $scope.hrEducationInfo.gpaScale, 'number', 1, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.passingYear', $scope.hrEducationInfo.passingYear, 'number', 4, 4, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {
                //$modalInstance.dismiss('cancel');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setCertificateDoc = function ($file, hrEducationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEducationInfo.certificateDoc = base64Data;
                            hrEducationInfo.certificateDocContentType = $file.type;
                            if (hrEducationInfo.id == null) {
                                hrEducationInfo.certificateDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setTranscriptDoc = function ($file, hrEducationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEducationInfo.transcriptDoc = base64Data;
                            hrEducationInfo.transcriptDocContentType = $file.type;
                            if (hrEducationInfo.id == null) {
                                hrEducationInfo.transcriptDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setTestimonialDoc = function ($file, hrEducationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEducationInfo.testimonialDoc = base64Data;
                            hrEducationInfo.testimonialDocContentType = $file.type;
                            if (hrEducationInfo.id == null) {
                                hrEducationInfo.testimonialDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.previewTransDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.transcriptDoc, modelInfo.transcriptDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.transcriptDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Education Transcript Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.certificateDoc, modelInfo.certificateDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.certificateDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Education Certificate Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewTestimonialDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.testimonialDoc, modelInfo.testimonialDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.testimonialDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Education Testimonial Document";
                $rootScope.showPreviewModal();
            };
        }]);
/*hrEducationInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEducationInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'HrEducationInfo', 'HrEmployeeInfo', 'MiscTypeSetup',
            function ($scope, $rootScope, $stateParams, DataUtils, entity, HrEducationInfo, HrEmployeeInfo, MiscTypeSetup) {
                $scope.hrEducationInfo = entity;
                $scope.load = function (id) {
                    HrEducationInfo.get({id: id}, function (result) {
                        $scope.hrEducationInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEducationInfoUpdate', function (event, result) {
                    $scope.hrEducationInfo = result;
                });
                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = " " + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };
                $scope.$on('$destroy', unsubscribe);
                $scope.byteSize = DataUtils.byteSize;
            }]);
/*hrEducationInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEducationInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEducationInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEducationInfo) {

                $scope.hrEducationInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEducationInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.hrEducationInfo.deleted');
                        });
                };

            }]);
/*hrEducationInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEducationInfoProfileController',
    ['$rootScope', '$sce', '$scope', '$stateParams', '$state', 'DataUtils', 'HrEducationInfo', 'HrEmployeeInfo', 'MiscTypeSetupByCategory', 'User', 'Principal', 'DateUtils',
        function ($rootScope, $sce, $scope, $stateParams, $state, DataUtils, HrEducationInfo, HrEmployeeInfo, MiscTypeSetupByCategory, User, Principal, DateUtils) {

            //$scope.hrEducationInfo = entity;
            $scope.hremployeeinfos = HrEmployeeInfo.query();

            $scope.educationBoardNames = MiscTypeSetupByCategory.get({cat: 'EducationBoard', stat: 'true'});
            $scope.educationLevelNames = MiscTypeSetupByCategory.get({cat: 'EducationLevel', stat: 'true'});

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                        //console.log("loggedInUser: "+JSON.stringify($scope.loggedInUser));
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadEducationProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEducationInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEducationInfoList = result;
                    if ($scope.hrEducationInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEducationInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEducationInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEducationInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.viewModeIcon = "fa fa-pencil-square";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }

                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };

            $scope.loadModel();


            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                    modelInfo.viewModeIcon = "fa fa-ban";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEducationInfoList.length > 1) {
                            var indx = $scope.hrEducationInfoList.indexOf(modelInfo);
                            $scope.hrEducationInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else {
                        modelInfo.viewModeText = "Edit";
                        modelInfo.viewModeIcon = "fa fa-pencil-square";
                    }
                }
            };

            $scope.addMore = function () {
                $scope.hrEducationInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        examTitle: null,
                        majorSubject: null,
                        certSlNumber: null,
                        sessionYear: null,
                        rollNumber: null,
                        instituteName: null,
                        gpaOrCgpa: null,
                        gpaScale: null,
                        passingYear: null,
                        certificateDoc: null,
                        certificateDocContentType: null,
                        certificateDocName: null,
                        transcriptDoc: null,
                        transcriptDocContentType: null,
                        transcriptDocName: null,
                        activeStatus: true,
                        logId: null,
                        logStatus: null,
                        logComments: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    examTitle: null,
                    majorSubject: null,
                    certSlNumber: null,
                    sessionYear: null,
                    rollNumber: null,
                    instituteName: null,
                    gpaOrCgpa: null,
                    gpaScale: null,
                    passingYear: null,
                    certificateDoc: null,
                    certificateDocContentType: null,
                    certificateDocName: null,
                    transcriptDoc: null,
                    transcriptDocContentType: null,
                    transcriptDocName: null,
                    activeStatus: true,
                    logId: null,
                    logStatus: null,
                    logComments: null,
                    id: null
                };
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEducationInfoUpdate', result);
                $scope.isSaving = false;
                $scope.viewMode = true;
                $scope.addMode = false;
                $scope.hrEducationInfoList[$scope.selectedIndex].id = result.id;

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $scope.viewMode = true;
            };

            $scope.updateProfile = function (modelInfo, index) {
                console.log("Education Info ");
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEducationInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    } else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrEducationInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };
            $scope.preCondition = function (hrEducationInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.instituteName', hrEducationInfo.instituteName, 'text', 100, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.examTitle', hrEducationInfo.examTitle, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.majorSubject', hrEducationInfo.majorSubject, 'text', 100, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.certSlNumber', hrEducationInfo.certSlNumber, 'number', 15, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.sessionYear', hrEducationInfo.sessionYear, 'text', 9, 4, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.rollNumber', hrEducationInfo.rollNumber, 'number', 10, 6, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.gpaScale', hrEducationInfo.gpaScale, 'number', 1, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEducationInfo.passingYear', hrEducationInfo.passingYear, 'number', 4, 4, '0to9', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.clear = function () {

            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.previewTransDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.transcriptDoc, modelInfo.transcriptDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.transcriptDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Education Transcript Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewCertDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.certificateDoc, modelInfo.certificateDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.certificateDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Education Certificate Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewTestimonialDoc = function (modelInfo) {
                var blob = $rootScope.b64toBlob(modelInfo.testimonialDoc, modelInfo.testimonialDocContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.testimonialDocContentType;
                $rootScope.viewerObject.pageTitle = "Employee Education Testimonial Document";
                $rootScope.showPreviewModal();
            };

            $scope.setCertificateDoc = function ($file, hrEducationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEducationInfo.certificateDoc = base64Data;
                            hrEducationInfo.certificateDocContentType = $file.type;
                            if (hrEducationInfo.id == null) {
                                hrEducationInfo.certificateDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setTranscriptDoc = function ($file, hrEducationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEducationInfo.transcriptDoc = base64Data;
                            hrEducationInfo.transcriptDocContentType = $file.type;
                            if (hrEducationInfo.id == null) {
                                hrEducationInfo.transcriptDocName = $file.name;
                            }
                        });
                    };
                }
            };

            $scope.setTestimonialDoc = function ($file, hrEducationInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            hrEducationInfo.testimonialDoc = base64Data;
                            hrEducationInfo.testimonialDocContentType = $file.type;
                            if (hrEducationInfo.id == null) {
                                hrEducationInfo.testimonialDocName = $file.name;
                            }
                        });
                    };
                }
            };


        }]);
