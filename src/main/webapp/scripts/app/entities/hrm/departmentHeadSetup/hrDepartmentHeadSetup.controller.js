'use strict';

angular.module('stepApp')
    .controller('HrDepartmentHeadSetupController', function ($scope, $state, HrDepartmentHeadSetup, HrDepartmentHeadSetupSearch, ParseLinks) {

        $scope.hrDepartmentHeadSetups = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.loadAll = function () {
            HrDepartmentHeadSetup.query({
                page: $scope.page - 1,
                size: 2000,
                sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
            }, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.hrDepartmentHeadSetups = result;
            });
        };
        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            HrDepartmentHeadSetupSearch.query({query: $scope.searchQuery}, function (result) {
                $scope.hrDepartmentHeadSetups = result;
            }, function (response) {
                if (response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.hrDepartmentHeadSetup = {
                departmentCode: null,
                departmentName: null,
                departmentDetail: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*hrDepartmentHeadSetup-dialog.controller.js*/

angular.module('stepApp').controller('HrDepartmentHeadSetupDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrDepartmentHeadSetup', 'Principal', 'User', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, entity, HrDepartmentHeadSetup, Principal, User, DateUtils) {

            $scope.hrDepartmentHeadSetup = entity;
            $scope.sec = false;
            $scope.dept = true;
            $scope.load = function (id) {
                HrDepartmentHeadSetup.get({id: id}, function (result) {
                    $scope.hrDepartmentHeadSetup = result;
                });
            };
            if ($stateParams.id != null) {
                HrDepartmentHeadSetup.get({id: $stateParams.id}, function (result) {
                    $scope.hrDepartmentHeadSetup = result;
                    if (result.setupType == 'Department') {
                        $scope.dept = true;
                        $scope.sec = false;
                    } else if (result.setupType == 'Section') {
                        $scope.sec = true;
                        $scope.dept = false;
                    }

                });
            }


            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                        console.log("loggedInUser: " + JSON.stringify($scope.loggedInUser));
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrDepartmentHeadSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('hrDepartmentHeadSetup', {}, {reload: true});
                $rootScope.setSuccessMessage('stepApp.hrDepartmentHeadSetup.created');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrDepartmentHeadSetup.updateBy = $scope.loggedInUser.id;
                $scope.hrDepartmentHeadSetup.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                $scope.hrDepartmentHeadSetup.setupType = $scope.setupType;
                if ($scope.preCondition()) {
                    if ($scope.hrDepartmentHeadSetup.id != null) {
                        if ($scope.hrDepartmentHeadSetup.setupType == null) {
                            $scope.hrDepartmentHeadSetup.setupType = 'Department';
                        }
                        HrDepartmentHeadSetup.update($scope.hrDepartmentHeadSetup, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.hrDepartmentHeadSetup.updated');
                    } else {
                        if ($scope.hrDepartmentHeadSetup.setupType == null) {
                            $scope.hrDepartmentHeadSetup.setupType = 'Department';
                        }
                        $scope.hrDepartmentHeadSetup.createBy = $scope.loggedInUser.id;
                        $scope.hrDepartmentHeadSetup.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        HrDepartmentHeadSetup.save($scope.hrDepartmentHeadSetup, onSaveSuccess, onSaveError);
                        //$rootScope.setSuccessMessage('stepApp.hrDepartmentHeadSetup.created');
                    }
                } else {
                    $rootScope.setErrorMessage("Please enter correct information");
                }
            };
            $scope.pgsType = function (stype) {
                if (stype == 'Department') {
                    $scope.setupType = "Department";
                    $scope.dept = true;
                    $scope.sec = false;
                } else if (stype == 'Section') {
                    $scope.setupType = "Section";
                    $scope.sec = true;
                    $scope.dept = false;
                } else {
                    $scope.sec = false;
                    $scope.dept = false;
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrDepartmentHeadSetup.departmentName', $scope.hrDepartmentHeadSetup.departmentName, 'text', 100, 4, 'atozAndAtoZ', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {

            };
        }]);
/*hrDepartmentHeadSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentHeadSetupDetailController', function ($scope, $rootScope, $stateParams, entity, HrDepartmentHeadSetup) {
        $scope.hrDepartmentHeadSetup = entity;
        $scope.dept = false;
        $scope.sec = false;
        $scope.load = function (id) {
            HrDepartmentHeadSetup.get({id: id}, function (result) {
                $scope.hrDepartmentHeadSetup = result;
            });
        };

        HrDepartmentHeadSetup.get({id: $stateParams.id}, function (result) {
            console.log('*********')
            $scope.hrDepartmentHeadSetup = result;
            $scope.pgsType(result.setupType);

        });
        $scope.pgsType = function (stype) {
            if (stype == 'Department') {
                //$scope.setupType = "Deparment";
                $scope.dept = true;
                $scope.sec = false;
            } else if (stype == 'Section') {
                //$scope.setupType = "Section";
                $scope.sec = true;
                $scope.dept = false;
            } else {
                $scope.sec = false;
                $scope.dept = false;
            }
        };

        var unsubscribe = $rootScope.$on('stepApp:hrDepartmentHeadSetupUpdate', function (event, result) {
            $scope.hrDepartmentHeadSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*hrDepartmentHeadSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrDepartmentHeadSetupDeleteController', function ($scope, $rootScope, $modalInstance, entity, HrDepartmentHeadSetup) {

        $scope.hrDepartmentHeadSetup = entity;
        $scope.clear = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            HrDepartmentHeadSetup.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
            $rootScope.setErrorMessage('stepApp.HrDepartmentHeadSetup.deleted');
        };

    });
