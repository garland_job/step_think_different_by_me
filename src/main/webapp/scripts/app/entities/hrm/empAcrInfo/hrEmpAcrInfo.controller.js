'use strict';

angular.module('stepApp')
    .controller('HrEmpAcrInfoController',
        ['$rootScope', '$scope', '$state', 'HrEmpAcrInfo', 'HrEmpAcrInfoSearch', 'ParseLinks',
            function ($rootScope, $scope, $state, HrEmpAcrInfo, HrEmpAcrInfoSearch, ParseLinks) {

                $scope.hrEmpAcrInfos = [];
                $scope.predicate = 'id';
                $scope.reverse = true;
                $scope.page = 0;
                $scope.stateName = "hrEmpAcrInfo";
                $scope.loadAll = function () {
                    if ($rootScope.currentStateName == $scope.stateName) {
                        $scope.page = $rootScope.pageNumber;
                    }
                    else {
                        $rootScope.pageNumber = $scope.page;
                        $rootScope.currentStateName = $scope.stateName;
                    }
                    HrEmpAcrInfo.query({
                        page: $scope.page,
                        size: 5000,
                        sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.totalItems = headers('X-Total-Count');
                        $scope.hrEmpAcrInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $rootScope.currentStateName = $scope.stateName;
                    $rootScope.pageNumber = page;
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    HrEmpAcrInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.hrEmpAcrInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.hrEmpAcrInfo = {
                        acrYear: null,
                        totalMarks: null,
                        overallEvaluation: null,
                        promotionStatus: null,
                        currentYearAcrInd: null,
                        noAcrReason: null,
                        acrDate: null,
                        logId: null,
                        logStatus: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };
            }]);
/*hrEmpAcrInfo-dialog.controller.js*/

angular.module('stepApp').controller('HrEmpAcrInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'HrEmpAcrInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfoByWorkArea', 'MiscTypeSetupByCategory', 'HrEmployeeOfInstitutes', 'HrEmployeeInfoByEmployeeId',
        function ($scope, $rootScope, $stateParams, $state, entity, HrEmpAcrInfo, HrEmployeeInfo, User, Principal, DateUtils, HrEmployeeInfoByWorkArea, MiscTypeSetupByCategory, HrEmployeeOfInstitutes, HrEmployeeInfoByEmployeeId) {

            $scope.hrEmpAcrInfo = entity;
            if ($stateParams.id != null) {
                HrEmpAcrInfo.get({id: $stateParams.id}, function (result) {
                    $scope.hrEmpAcrInfo = result;
                    if (result.employeeInfo.organizationType == 'Institute') {
                        $scope.orgOrInst = 'Institute';
                    } else {
                        $scope.orgOrInst = 'Organization';
                        $scope.workArea = result.employeeInfo.workArea;
                    }
                });
            }

            HrEmployeeOfInstitutes.get({}, function (result) {
                $scope.hremployeeinfosOfInstitute = result;
                //console.log("Total record: "+result.length);
            });
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function (id) {
                HrEmpAcrInfo.get({id: id}, function (result) {
                    $scope.hrEmpAcrInfo = result;
                });
            };

            $scope.workAreaList = MiscTypeSetupByCategory.get({cat: 'EmployeeWorkArea', stat: 'true'});

            $scope.loadModelByWorkArea = function (workArea) {
                HrEmployeeInfoByWorkArea.get({areaid: workArea.id}, function (result) {
                    $scope.hremployeeinfos = result;
                    console.log("Total record: " + result.length);
                });
            };

            $scope.loadHrEmployee = function (acrInfo) {
                console.log("load hr emp");
                console.log(acrInfo.employeeInfo.employeeId);
                HrEmployeeInfoByEmployeeId.get({id: acrInfo.employeeInfo.employeeId}, function (result) {
                    console.log("result");
                    console.log(result);
                    $scope.hrEmpAcrInfo.employeeInfo = result;


                });

            }

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();
            $scope.toDate = new Date();
            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAcrInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('hrEmpAcrInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.hrEmpAcrInfo.updateBy = $scope.loggedInUser.id;
                $scope.hrEmpAcrInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());

                if ($scope.hrEmpAcrInfo.id != null) {
                    $scope.hrEmpAcrInfo.logId = 0;
                    $scope.hrEmpAcrInfo.logStatus = 2;
                    HrEmpAcrInfo.update($scope.hrEmpAcrInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.hrEmpAcrInfo.updated');
                }
                else {
                    $scope.hrEmpAcrInfo.logId = 0;
                    $scope.hrEmpAcrInfo.logStatus = 1;
                    $scope.hrEmpAcrInfo.createBy = $scope.loggedInUser.id;
                    $scope.hrEmpAcrInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    HrEmpAcrInfo.save($scope.hrEmpAcrInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.hrEmpAcrInfo.created');
                }
            };

            $scope.clear = function () {

            };
        }]);
/*hrEmpAcrInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAcrInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'HrEmpAcrInfo', 'HrEmployeeInfo',
            function ($scope, $rootScope, $stateParams, entity, HrEmpAcrInfo, HrEmployeeInfo) {
                $scope.hrEmpAcrInfo = entity;
                $scope.load = function (id) {
                    HrEmpAcrInfo.get({id: id}, function (result) {
                        $scope.hrEmpAcrInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:hrEmpAcrInfoUpdate', function (event, result) {
                    $scope.hrEmpAcrInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*hrEmpAcrInfo-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('HrEmpAcrInfoDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'HrEmpAcrInfo',
            function ($scope, $rootScope, $modalInstance, entity, HrEmpAcrInfo) {

                $scope.hrEmpAcrInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    HrEmpAcrInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.HrEmpAcrInfo.deleted');
                };

            }]);
/*hrEmpAcrInfo-profile.controller.js*/

angular.module('stepApp').controller('HrEmpAcrInfoProfileController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'HrEmpAcrInfo', 'HrEmployeeInfo', 'User', 'Principal', 'DateUtils',
        function ($scope, $rootScope, $stateParams, $state, HrEmpAcrInfo, HrEmployeeInfo, User, Principal, DateUtils) {

            $scope.hrEmpAcrInfo = {};

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.viewMode = true;
            $scope.addMode = false;
            $scope.noEmployeeFound = false;

            $scope.loadModel = function () {
                console.log("loadAwarddProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmpAcrInfo.query({id: 'my'}, function (result) {
                    console.log("result:, len: " + result.length);
                    $scope.hrEmpAcrInfoList = result;
                    if ($scope.hrEmpAcrInfoList.length < 1) {
                        $scope.addMode = true;
                        $scope.hrEmpAcrInfoList.push($scope.initiateModel());
                        $scope.loadEmployee();
                    }
                    else {
                        $scope.hrEmployeeInfo = $scope.hrEmpAcrInfoList[0].employeeInfo;
                        angular.forEach($scope.hrEmpAcrInfoList, function (modelInfo) {
                            modelInfo.viewMode = true;
                            modelInfo.viewModeText = "Edit";
                            modelInfo.isLocked = false;
                            if (modelInfo.logStatus == 0) {
                                modelInfo.isLocked = true;
                            }
                        });
                    }
                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.addMode = true;
                    $scope.loadEmployee();
                })
            };

            $scope.loadEmployee = function () {
                console.log("loadEmployeeProfile addMode: " + $scope.addMode + ", viewMode: " + $scope.viewMode);
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;

                }, function (response) {
                    console.log("error: " + response);
                    $scope.hasProfile = false;
                    $scope.noEmployeeFound = true;
                    $scope.isSaving = false;
                })
            };
            $scope.loadModel();

            $scope.changeProfileMode = function (modelInfo) {
                if (modelInfo.viewMode) {
                    modelInfo.viewMode = false;
                    modelInfo.viewModeText = "Cancel";
                }
                else {
                    $scope.loadModel();
                    modelInfo.viewMode = true;
                    if (modelInfo.id == null) {
                        if ($scope.hrEmpAcrInfoList.length > 1) {
                            var indx = $scope.hrEmpAcrInfoList.indexOf(modelInfo);
                            $scope.hrEmpAcrInfoList.splice(indx, 1);
                        }
                        else {
                            modelInfo.viewModeText = "Add";
                        }
                    }
                    else
                        modelInfo.viewModeText = "Edit";
                }
            };

            $scope.addMore = function () {
                $scope.hrEmpAcrInfoList.push(
                    {
                        viewMode: false,
                        viewModeText: 'Cancel',
                        acrYear: null,
                        totalMarks: null,
                        overallEvaluation: null,
                        promotionStatus: null,
                        acrDate: null,
                        currentYearAcrInd: null,
                        logId: null,
                        logStatus: null,
                        activeStatus: true,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    }
                );
            };

            $scope.initiateModel = function () {
                return {
                    viewMode: true,
                    viewModeText: 'Add',
                    acrYear: null,
                    totalMarks: null,
                    overallEvaluation: null,
                    promotionStatus: null,
                    currentYearAcrInd: null,
                    acrDate: null,
                    logId: null,
                    logStatus: null,
                    activeStatus: true,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };

            $scope.calendar_local = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:hrEmpAcrInfoUpdate', result);
                $scope.isSaving = false;
                $scope.hrEmpAcrInfoList[$scope.selectedIndex].id = result.id;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            $scope.updateProfile = function (modelInfo, index) {
                $scope.selectedIndex = index;
                modelInfo.updateBy = $scope.loggedInUser.id;
                modelInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                modelInfo.activeStatus = true;
                if ($scope.preCondition(modelInfo)) {
                    if (modelInfo.id != null) {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        HrEmpAcrInfo.update(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        modelInfo.isLocked = true;
                    }
                    else {
                        modelInfo.logId = 0;
                        modelInfo.logStatus = 0;
                        modelInfo.employeeInfo = $scope.hrEmployeeInfo;
                        modelInfo.createBy = $scope.loggedInUser.id;
                        modelInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());

                        HrEmpAcrInfo.save(modelInfo, onSaveSuccess, onSaveError);
                        modelInfo.viewMode = true;
                        modelInfo.viewModeText = "Edit";
                        $scope.addMode = false;
                        modelInfo.isLocked = true;
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid information");
                }
            };

            $scope.preCondition = function (hrEmpAcrInfo) {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAcrInfo.acrYear', hrEmpAcrInfo.acrYear, 'number', 4, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAcrInfo.totalMarks', hrEmpAcrInfo.totalMarks, 'number', 3, 2, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAcrInfo.promotionStatus', hrEmpAcrInfo.promotionStatus, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('hrEmpAcrInfo.overallEvaluation', hrEmpAcrInfo.overallEvaluation, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };
            $scope.clear = function () {

            };
        }]);
