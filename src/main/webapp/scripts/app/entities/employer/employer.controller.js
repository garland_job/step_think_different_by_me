'use strict';

angular.module('stepApp')
    .controller('EmployerController',
    ['$scope', '$state', '$modal', 'DataUtils', 'Employer', 'EmployerSearch', 'ParseLinks',
    function ($scope, $state, $modal, DataUtils, Employer, EmployerSearch, ParseLinks) {

        $scope.employers = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Employer.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employers = result;
                $scope.total = headers('x-total-count');
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.loadAll();

        $scope.search = function () {
            EmployerSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.employers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employer = {
                name: null,
                alternativeCompanyName: null,
                contactPersonName: null,
                personDesignation: null,
                contactNumber: null,
                companyInformation: null,
                address: null,
                city: null,
                zipCode: null,
                companyWebsite: null,
                industryType: null,
                businessDescription: null,
                companyLogo: null,
                companyLogoContentType: null,
                id: null
            };
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;

        // bulk operations start
        $scope.areAllEmployersSelected = false;

        $scope.updateEmployersSelection = function (employerArray, selectionValue) {
            for (var i = 0; i < employerArray.length; i++)
            {
            employerArray[i].isSelected = selectionValue;
            }
        };


        $scope.import = function (){
            for (var i = 0; i < $scope.employers.length; i++){
                var employer = $scope.employers[i];
                if(employer.isSelected){
                    //Employer.update(employer);
                    //TODO: handle bulk export
                }
            }
        };

        $scope.export = function (){
            for (var i = 0; i < $scope.employers.length; i++){
                var employer = $scope.employers[i];
                if(employer.isSelected){
                    //Employer.update(employer);
                    //TODO: handle bulk export
                }
            }
        };

        $scope.deleteSelected = function (){
            for (var i = 0; i < $scope.employers.length; i++){
                var employer = $scope.employers[i];
                if(employer.isSelected){
                    Employer.delete(employer);
                }
            }
        };

        $scope.sync = function (){
            for (var i = 0; i < $scope.employers.length; i++){
                var employer = $scope.employers[i];
                if(employer.isSelected){
                    Employer.update(employer);
                }
            }
        };

        $scope.order = function (predicate, reverse) {
            $scope.predicate = predicate;
            $scope.reverse = reverse;
            Employer.query({page: $scope.page, size: 20}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employers = result;
                $scope.total = headers('x-total-count');
            });
        };
        // bulk operations end

    }]);


/* applicant-filter.controller.js */
angular.module('stepApp').controller('SearchApplicantController',
['$scope', '$stateParams', 'DataUtils','JobApplicationsShortList', 'Jobapplication', '$state',
        function ($scope, $stateParams, DataUtils,JobApplicationsShortList, Jobapplication, $state) {

            /*JobApplicationsShortList.query({id: $stateParams.id}, function (result) {
                console.log(result);
                $scope.applications = result;
            });

            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };*/

        }]);


/* applicant-resume.controller.js */
angular.module('stepApp')
    .controller('ApplicantResumeController',
        ['$scope', '$stateParams', '$rootScope', 'entity', 'JpEmployeeReferenceJpEmployee', 'JpLanguageProfeciencyJpEmployee', 'AcademicQualificationJpEmployee', 'JpEmployeeTrainingJpEmployee', 'JpEmployeeExperienceJpEmployee', 'JpEmploymentHistoryJpEmployee', 'Jobapplication',
            function ($scope, $stateParams, $rootScope, entity, JpEmployeeReferenceJpEmployee, JpLanguageProfeciencyJpEmployee, AcademicQualificationJpEmployee, JpEmployeeTrainingJpEmployee, JpEmployeeExperienceJpEmployee, JpEmploymentHistoryJpEmployee, Jobapplication) {
                //$scope.jobApplication = entity;

                $scope.showInternal = true;
                /*$scope.content = '';*/
                $scope.jobApplication = {};

                Jobapplication.get({id: $stateParams.id}, function(response){
                    $scope.jobApplication = response;
                    response.cvViewed = true;

                    if (response.id != null) {
                        Jobapplication.update(response);
                    }

                    if ($scope.jobApplication.cvType === 'external') {
                        $scope.showInternal = false;
                    }

                    $scope.jpEmployee = $scope.jobApplication.jpEmployee;

                    var blob = $rootScope.b64toBlob($scope.jpEmployee.cv, 'application/pdf');
                    $scope.content = (window.URL || window.webkitURL).createObjectURL(blob);

                    var blob2 = b64toBlob($scope.jpEmployee.picture);
                    $scope.url = (window.URL || window.webkitURL).createObjectURL( blob2 );

                    AcademicQualificationJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.academicQualifications = result;
                    });

                    JpEmploymentHistoryJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.employments = result;
                    });

                    JpEmployeeExperienceJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.skills = result;

                    });

                    JpEmployeeTrainingJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.trainings = result;

                    });

                    JpLanguageProfeciencyJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.langs = result;
                    });
                    JpEmployeeReferenceJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.references = result;

                    });
                });


                $scope.click = function(){
                    window.open($scope.content);
                };

                $scope.print = function(){
                    window.print();
                };

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }

                        var byteArray = new Uint8Array(byteNumbers);

                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }


            }]);


/* change-password.controller.js */
angular.module('stepApp').controller('EmployerChangePasswordController',
    ['Principal', '$scope', '$stateParams', 'entity', 'User', 'Auth', '$state',
        function (Principal, $scope, $stateParams, entity, User, Auth, $state) {

            Principal.identity().then(function (account) {
                $scope.account = account;
                User.get({login: $scope.account.login}, function (result) {
                    $scope.user = result;
                });
                if($scope.isInArray('ROLE_ADMIN', $scope.account.authorities))
                {
                    $scope.userRole = 'ROLE_ADMIN';
                }
                else if($scope.isInArray('ROLE_EMPLOYER', $scope.account.authorities))
                {
                    $scope.userRole = 'ROLE_EMPLOYER';
                }
                else if($scope.isInArray('ROLE_JPADMIN', $scope.account.authorities))
                {
                    $scope.userRole = 'ROLE_JPADMIN';
                }
                else if($scope.isInArray('ROLE_USER', $scope.account.authorities))
                {
                    $scope.userRole = 'ROLE_USER';

                }
            });
            $scope.isInArray = function isInArray(value, array) {
                return array.indexOf(value) > -1;
            };

            $scope.curPass;
            $scope.matchPass = function (pass, conPass) {

                if (pass != conPass) {
                    $scope.notMatched = true;
                } else {
                    $scope.notMatched = false;
                }

            };
            var onSaveSuccess = function (result) {
                $state.go('#');
            };

            var onSaveError = function (result) {
                //$scope.isSaving = false;
            };

            $scope.success = null;
            $scope.error = null;
            $scope.doNotMatch = null;

            $scope.changePassword = function () {
                $scope.curPass = $scope.currentPassword;
                //console.log( $scope.curPass);
                if ($scope.password !== $scope.confirmPassword) {
                    $scope.doNotMatch = 'ERROR';
                } else {
                    $scope.doNotMatch = null;
                    Auth.changePassword($scope.password).then(function () {
                        $scope.error = null;
                        $scope.success = 'OK';
                        Auth.logout();
                        $state.go('home');
                    }).catch(function () {
                        $scope.success = null;
                        $scope.error = 'ERROR';
                    });
                }
            };
            $scope.updatePassword = function () {

            };
        }]);

/* employer-detail.controller.js */

angular.module('stepApp')
    .controller('EmployerDetailController',
        ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'Employer', 'User', 'Country',
            function ($scope, $rootScope, $stateParams, DataUtils, entity, Employer, User, Country) {
                $scope.employer = entity;
                $scope.load = function (id) {
                    Employer.get({id: id}, function(result) {
                        $scope.employer = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:employerUpdate', function(event, result) {
                    $scope.employer = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;

                $scope.setCompanyLogo = function ($file, employer) {
                    if ($file && $file.$error == 'pattern') {
                        return;
                    }
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function() {
                                employer.companyLogo = base64Data;
                                employer.companyLogoContentType = $file.type;
                            });
                        };
                    }
                };
            }]);


/*  employer-dialog.controller.js */
angular.module('stepApp').controller('EmployerDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'DataUtils', 'entity', 'Employer', 'User', 'Country',
        function($scope, $stateParams, $modalInstance, DataUtils, entity, Employer, User, Country) {

            $scope.employer = entity;
            $scope.users = User.query();
            $scope.countrys = Country.query();
            $scope.load = function(id) {
                Employer.get({id : id}, function(result) {
                    $scope.employer = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:employerUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                console.log($scope.employer);
                $scope.isSaving = true;
                if ($scope.employer.id != null) {
                    Employer.update($scope.employer, onSaveSuccess, onSaveError);
                } else {
                    Employer.save($scope.employer, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setCompanyLogo = function ($file, employer) {
                if ($file && $file.$error == 'pattern') {
                    return;
                }
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            employer.companyLogo = base64Data;
                            employer.companyLogoContentType = $file.type;
                        });
                    };
                }
            };
        }]);

/*  employer-profile.controller.js */

angular.module('stepApp').
controller('EmployerProfileController',
    ['$scope', '$stateParams', 'DataUtils','OrganizationCategoryActive', 'Principal','Division','District', 'Upazila','Employer', 'TempEmployer', 'User', 'Country', 'TempEmployerByUserId','OrganizationTypeActive','GovernmentInstitutes',
        function ($scope, $stateParams, DataUtils,OrganizationCategoryActive, Principal,Division,District, Upazila,Employer, TempEmployer, User, Country, TempEmployerByUserId,OrganizationTypeActive,GovernmentInstitutes) {

            $scope.employer = {};
            $scope.tempEmployer = {};
            $scope.isProfilePending = false;
            $scope.isNewProfile = false;
            //$scope.notMatched=false;
            $scope.selectUpazila = null;
            $scope.selectDistrict = null;
            $scope.selectInstitute = null;
            $scope.allDistrict = [];
            $scope.allUpazila = [];
            $scope.divisions = Division.query();
            $scope.institutes = [];
            GovernmentInstitutes.query({}, function(result, headers) {
                $scope.institutes =result;
            });
            $scope.countries = Country.query({size: 200});
            $scope.organizationTypes = OrganizationTypeActive.query({size: 50});
            $scope.organizationCategorys = OrganizationCategoryActive.query({size: 50});

            Principal.identity().then(function (account) {
                $scope.account = account;

                User.get({login: $scope.account.login}, function (result) {
                    $scope.user = result;
                    TempEmployerByUserId.get({id: $scope.user.id}, function (tempEmployer) {
                            $scope.isProfilePending = true;
                        },
                        function (response) {
                            if (response.status === 404) {
                                $scope.isProfilePending = false;
                            }
                        }
                    );
                });

            });

            Employer.get({id: 'my'}, function (result) {
                $scope.isNewProfile = false;
                $scope.employer = result;
                $scope.tempEmployer = result;
                if(result.upazila.id != null){
                    $scope.selectUpazila = result.upazila.name;
                    $scope.selectDistrict = result.upazila.district.name;
                    $scope.selectInstitute = result.institute.name;

                }

            }, function (response) {
                if (response.status == 404) {
                    $scope.isNewProfile = true;
                    console.log($scope.isNewProfile);
                }
            });

            var onSaveSuccess = function (result) {
                //$scope.$emit('stepApp:tempEmployerUpdate', result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {

                $scope.isSaving = true;
                $scope.editEmployer = false;
                $scope.tempEmployer.user = $scope.user;

                if ($scope.tempEmployer.id != null) {
                    $scope.isProfilePending = true;
                    $scope.tempEmployer.id = null;
                    $scope.tempEmployer.status = 'update';
                    TempEmployer.save($scope.tempEmployer, onSaveSuccess, onSaveError);
                } else {
                    $scope.isProfilePending = true;
                    $scope.tempEmployer.id = null;
                    $scope.tempEmployer.status = 'pending';
                    TempEmployer.save($scope.tempEmployer, onSaveSuccess, onSaveError);
                }
            };

            if($scope.allDistrict.length==0){
                District.query({page: $scope.page, size: 65}, function(result, headers) { $scope.allDistrict= result;});
            }
            if( $scope.allUpazila.length==0){
                Upazila.query({page: $scope.page, size: 500}, function(result, headers) { $scope.allUpazila= result;});
            }
            $scope.updatedDistrict=function(select){
                if($scope.allDistrict.length==0){
                    District.query({page: $scope.page, size: 65}, function(result, headers) { $scope.allDistrict= result;
                        console.log(select);
                        console.log($scope.allDistrict);

                        $scope.districts=[];
                        angular.forEach($scope.allDistrict, function(district) {
                            if(select !=undefined && select.id == district.division.id){
                                $scope.districts.push(district);
                            }
                        });
                    });
                }else{
                    console.log(select);
                    console.log($scope.allDistrict);

                    $scope.districts=[];
                    $scope.allUpazila = [];
                    $scope.institutes = [];
                    $scope.selectUpazila = null;
                    $scope.selectDistrict = null;
                    //$scope.selectInstitute = null;
                    angular.forEach($scope.allDistrict, function(district) {
                        if(select != undefined  && select.id == district.division.id){
                            $scope.districts.push(district);
                        }
                    });
                }
            };
            $scope.updatedUpazila=function(select){
                //if( $scope.allUpazila.length==0){
                Upazila.query({page: $scope.page, size: 500}, function(result, headers) { $scope.allUpazila= result;
                    console.log(select);
                    console.log($scope.allUpazila);
                    $scope.upazilas=[];
                    $scope.institutes = [];
                    $scope.selectUpazila = null;
                    // $scope.selectInstitute = null;
                    angular.forEach($scope.allUpazila, function(upazila) {
                        if(select !=undefined && select.id==upazila.district.id){
                            $scope.upazilas.push(upazila);
                        }
                    });
                });
            };

            $scope.updatedInstitute=function(upazilaId){

            };

            $scope.setCompanyLogo = function ($file, employer) {
                if ($file && $file.$error == 'pattern') {
                    return;
                }
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            employer.companyLogo = base64Data;
                            employer.companyLogoContentType = $file.type;
                        });
                    };
                }
            };

            $scope.editEmployer = false;
            $scope.editEmployerProfile = function (value) {
                $scope.editEmployer = value;
            }


        }]);


/*  employer-registration.controller.js */

angular.module('stepApp').controller('EmployerRegistrationController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'InstituteByUpazila','District','Upazila','CountrysByName', 'DataUtils','Division','OrganizationType' ,'OrganizationCategory', 'entity', 'Employer', 'Auth', 'User', 'Country', 'TempEmployer', 'ExistingEmployer','OrganizationCategoryActive','GovernmentInstitutes','OrganizationTypeActive','CountryFindByName',
        function ($scope, $rootScope, $state, $stateParams, InstituteByUpazila,District,Upazila,CountrysByName, DataUtils,Division,OrganizationType ,OrganizationCategory, entity, Employer, Auth, User, Country, TempEmployer, ExistingEmployer,OrganizationCategoryActive,GovernmentInstitutes,OrganizationTypeActive,CountryFindByName) {


            CountrysByName.query({}, function (result) {
                $scope.countries =result;
                angular.forEach(result, function(each) {
                    if(each.name === "Bangladesh"){
                        $scope.employer.country = each;
                    }
                });
            });

            console.log("countrys :"+$scope.countries);
            $scope.organizationTypes = OrganizationTypeActive.query({size: 50});
            $scope.organizationCategorys = OrganizationCategoryActive.query({size: 50});
            $scope.employer = {};
           /* CountryFindByName.get({name:"Bangladesh"},function (result) {
                $scope.employer.country = result;
            });*/

            $scope.success = null;
            $scope.error = null;
            $scope.doNotMatch = null;
            $scope.errorUserExists = null;
            $scope.didNotMatch=null;
            $scope.zipCode=null;
            $scope.notMatched=false;
            $scope.allDistrict = [];
            $scope.allUpazila = [];
            $scope.divisions = Division.query();
            $scope.institutes = [];

            $scope.matchPass=function(pass,conPass){

                if(pass != conPass){
                    $scope.notMatched=true;
                }else{
                    $scope.notMatched=false;
                }

            };

            $scope.getzip=function (zip){
                $scope.zipCode=zip;

            };


            $scope.validateZip=function(zip){
                console.log('zip code : '+zip);
            };

            var onSaveSuccess = function (result) {
                $scope.isSaving = true;

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            $scope.setCompanyLogo = function ($file, employer) {
                console.log("printing file error");
                if ($file && $file.$error == 'pattern') {
                    return;
                }
                if ($file) {
                    if($file.size/1024 > 100){
                        alert("File size should be maximum 100KB!");
                    }else{

                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            /////////////////////////
                            /*var blob = new Blob([base64Data], {type: 'image/png'});
                            var image = new Image();
                            image.src = URL.createObjectURL(blob);
                            // create an off-screen canvas
                            var canvas = document.createElement('canvas'),
                                ctx = canvas.getContext('2d');
                            // set its dimension to target size
                            canvas.width = 300;
                            canvas.height = 300;

                           /!* image.onload = function() {
                                ctx.drawImage(image, 0, 0, 300, 300);
                            };*!/

                                // draw source image into the off-screen canvas:
                            ctx.drawImage(image, 0, 0, 300, 300);
                            console.log(canvas.toDataURL());
*/
                            //var convertedFile = dataURLtoBlob(myCanvas.toDataURL());
                            //console.log(convertedFile);
                            ///////////////////////
                            $scope.$apply(function() {
                                //employer.companyLogo = base64Data;
                                employer.companyLogo = base64Data;
                                employer.companyLogoContentType = $file.type;
                                employer.companyLogoName = $file.name;
                                console.log("ends.......")
                            });
                        };

                    }

                }
            };

            $scope.captcha = function () {
                var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '@', '#', '$', '%', '^', '&', '*');
                var i;
                var code = "";
                for (i = 0; i < 6; i++) {
                    code = code + alpha[Math.floor(Math.random() * alpha.length)] + " ";
                }
                //var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
                $scope.mainCaptcha = code;
            };
            $scope.captcha()

            function dataURLtoBlob(dataurl) {
                var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
                while(n--){
                    u8arr[n] = bstr.charCodeAt(n);
                }
                return new Blob([u8arr], {type:mime});
            }
            $scope.imageToDataUri = function(img, width, height) {

                // create an off-screen canvas
                var canvas = document.createElement('canvas'),
                    ctx = canvas.getContext('2d');

                // set its dimension to target size
                canvas.width = width;
                canvas.height = height;

                // draw source image into the off-screen canvas:
                ctx.drawImage(img, 0, 0, width, height);

                // encode image to data-uri with base64 version of compressed image
                return canvas.toDataURL();
            }
            /*$scope.imageToDataUri = function (img, width, height) {

                // create an off-screen canvas
                var canvas = document.createElement('canvas'),
                    ctx = canvas.getContext('2d');

                // set its dimension to target size
                canvas.width = width;
                canvas.height = height;

                // draw source image into the off-screen canvas:
                ctx.drawImage(img, 0, 0, width, height);

                // encode image to data-uri with base64 version of compressed image
                return canvas.toDataURL();
            }*/
            $scope.save = function () {
                if ($scope.employer.user.password !== $scope.employer.user.confirmPassword) {
                    $scope.doNotMatch = 'ERROR';
                } else {
                    $scope.isSaving = true;
                    $scope.employer.user.langKey = "en";
                    //$scope.employer.user.firstName = $scope.employer.user.login;
                    $scope.employer.user.activated = false;
                    $scope.employer.user.authorities = ["ROLE_EMPLOYER"];

                    Auth.createAccount($scope.employer.user).then(function (newUser) {
                        $scope.employer.user.id = parseInt(newUser.id);
                        $scope.employer.status = "pending";
                        TempEmployer.save($scope.employer, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.employer.created');
                        $scope.success = 'OK';
                    }).catch(function (response) {
                        $scope.success = null;
                        if (response.status === 400 && response.data === 'login already in use') {
                            $scope.errorUserExists = 'ERROR';
                        } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                            $scope.errorEmailExists = 'ERROR';
                        } else {
                            $scope.error = 'ERROR';
                        }
                    });
                }
            };

            if($scope.allDistrict.length==0){
                District.query({page: $scope.page, size: 65}, function(result, headers) { $scope.allDistrict= result;});
            }
            if( $scope.allUpazila.length==0){
                Upazila.query({page: $scope.page, size: 500}, function(result, headers) { $scope.allUpazila= result;});
            }
            $scope.updatedDistrict=function(select){
                if($scope.allDistrict.length==0){
                    District.query({page: $scope.page, size: 65}, function(result, headers) { $scope.allDistrict= result;
                        console.log(select);
                        console.log($scope.allDistrict);

                        $scope.districts=[];
                        angular.forEach($scope.allDistrict, function(district) {
                            if(select !=undefined && select.id == district.division.id){
                                $scope.districts.push(district);
                            }
                        });
                    });
                }else{
                    console.log(select);
                    console.log($scope.allDistrict);

                    $scope.districts=[];
                    $scope.allUpazila = [];
                    // $scope.institutes = [];
                    angular.forEach($scope.allDistrict, function(district) {
                        if(select != undefined  && select.id == district.division.id){
                            $scope.districts.push(district);
                        }
                    });
                }
            };
            $scope.updatedUpazila=function(select){
                //if( $scope.allUpazila.length==0){
                Upazila.query({page: $scope.page, size: 500}, function(result, headers) { $scope.allUpazila= result;
                    console.log(select);
                    console.log($scope.allUpazila);
                    $scope.upazilas=[];
                    // $scope.institutes = [];
                    angular.forEach($scope.allUpazila, function(upazila) {
                        if(select !=undefined && select.id==upazila.district.id){
                            $scope.upazilas.push(upazila);
                        }
                    });
                });
            };

            $scope.updatedInstitute=function(upazilaId){
                InstituteByUpazila.query({id:upazilaId}, function(result, headers) {
                    $scope.institutes =result;
                });
            };

            $scope.findInstitutes=function(country){
                if(country.name != "Bangladesh"){
                    GovernmentInstitutes.query({}, function(result, headers) {
                        $scope.institutes =result;
                    });
                }

            };

        }]);

/*final-list.controller.js */

angular.module('stepApp').controller('FinalListController',
    ['$scope', '$stateParams', 'DataUtils','JobApplicationsSelectedList', 'Jobapplication', '$state',
        function ($scope, $stateParams, DataUtils,JobApplicationsSelectedList, Jobapplication, $state) {

            $scope.applicantList = "Final Listed";

            JobApplicationsSelectedList.query({id: $stateParams.id}, function (result) {
                console.log(result);
                $scope.applications = result;
            });

            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };

        }]);

/* interview-list.controller.js */
angular.module('stepApp').controller('InterviewListController',
    ['$scope', '$stateParams','JobApplicationsInterviewList', 'Jobapplication', '$state',
        function ($scope, $stateParams,JobApplicationsInterviewList, Jobapplication, $state) {

            JobApplicationsInterviewList.query({id: $stateParams.id}, function (result) {
                console.log(result);
                $scope.applications = result;
            });

            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };

        }]);

/* job-application.controller.js */

angular.module('stepApp')
    .controller('JobApplicationController',
        ['$scope', '$state','ParseLinks','District', '$stateParams', 'entity', 'JobapplicationJob','JobApplicationsDto', 'Jobapplication', 'JobApplicationsTotalExp','JpEmploymentHistoryFirst','JobApplicationsByJob', 'JobapplicationCvSort',
            function ($scope, $state,ParseLinks,District, $stateParams, entity, JobapplicationJob,JobApplicationsDto, Jobapplication, JobApplicationsTotalExp,JpEmploymentHistoryFirst,JobApplicationsByJob, JobapplicationCvSort) {

                $scope.jobApplications = [];
                $scope.districts = [];
                $scope.cvSearchCriteria = {};
                var x = 0;
                District.query({page: $scope.page, size: 65}, function(result, headers) { $scope.districts= result;});
                $scope.page = 0;
                $scope.loadAll = function() {
                    JobApplicationsByJob.get({id:$stateParams.id, page: $scope.page, size: 200}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.jobApplications = result;
                        console.log("job application list");
                        console.log($scope.jobApplications);

                        angular.forEach($scope.jobApplications, function(value, key){
                            console.log('comes to loop');
                            JpEmploymentHistoryFirst.get({id:value.jpEmployee.id}, function(empHist){
                                console.log("experience found");
                                console.log(empHist.stDate);
                                console.log(empHist.stDate);
                                if(empHist.stDate != null){
                                    value.experience= $scope.diffCalculate(empHist.stDate);
                                    console.log('experience :'+$scope.calculateAge(empHist.stDate));
                                }
                            });
                        });
                    });
                };

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();
                JobApplicationsDto.query({id:$stateParams.id},function(result){
                    $scope.applicationDto = result;
                });

                // $scope.calculateAge = function(birthday) {
                //     var ageDifMs = Date.now() - new Date(birthday);
                //     var ageDate = new Date(ageDifMs);
                //     return Math.abs(ageDate.getUTCFullYear() - 1970);
                // };


                $scope.diffCalculate = function calcDate(sdt) {
                    console.log('((((((((((((((((');
                    console.log(sdt);
                     var sdt1 = new Date(sdt);
                    var difdt = new Date(new Date() - sdt1);
                    return (difdt.toISOString().slice(0, 4) - 1970) + "Year: " + (difdt.getMonth()+1) +"Month: " + difdt.getDate() + "Day";
                }


               // a = calcDate(today,past)

                $scope.sortCvs = function() {
                    console.log("cv sorting....");
                    console.log($stateParams.id);
                    $scope.cvSearchCriteria.jobId = $stateParams.id;
                    $scope.jobApplications = JobapplicationCvSort.save($scope.cvSearchCriteria);
                };

                $scope.getExperience = function(id) {
                    var exp = 0;
                    JpEmploymentHistoryFirst.get({id:id}, function(result){
                        exp = 11;
                    });
                    return exp;
                };

                $scope.getAge = function getAge(dateString) {
                    var today = new Date();
                    var birthDate = new Date(dateString);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    return age;
                }

                $scope.jobApplicationStatus = [
                    {'applicantStatus' : 'Awaiting'},
                    {'applicantStatus' : 'Shortlisted'},
                    {'applicantStatus' : 'Interviewed'},
                    {'applicantStatus' : 'Rejected'},
                    {'applicantStatus' : 'Selected'},
                    {'applicantStatus' : 'Offered'}
                ];

                $scope.displayCv = function(applicaiton) {

                    var blob = b64toBlob(applicaiton.cv, applicaiton.cvContentType);
                    $scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
                    window.open($scope.url);
                }

                $scope.showPic = function(jpEmployee) {
                    console.log(jpEmployee.name+' name found');
                    var blob = b64toBlob(jpEmployee.picture);
                    return (window.URL || window.webkitURL).createObjectURL( blob );

                }

                var onSaveFinished = function (result) {
                    $scope.$emit('stepApp:jobapplicationUpdate', result);
                };

                $scope.updateStatus=function (status, id){
                    Jobapplication.get({id : id}, function(result) {
                        $scope.jobapplication = result;
                        $scope.jobapplication.applicantStatus = status;
                        Jobapplication.update($scope.jobapplication, onSaveFinished);
                        $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                    });
                };

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }
                        var byteArray = new Uint8Array(byteNumbers);
                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }

            }]);


/* job-list.controller.js */

angular.module('stepApp').controller('JobListController',
    ['$scope', '$stateParams', 'DataUtils', 'Principal', 'Employer', 'JobInfoEmployer', 'Job', 'JobEmployer', 'User', 'Country', 'JobSearch',
        function ($scope, $stateParams, DataUtils, Principal, Employer, JobInfoEmployer, Job, JobEmployer, User, Country, JobSearch) {

            $scope.isNewProfile = false;

            Employer.get({id: 'my'}, function (result) {
                $scope.employer = result;
                console.log($scope.employer);
                JobEmployer.query({id: $scope.employer.id}, function (result) {
                    $scope.jobs = result;
                });
                JobInfoEmployer.query({id: $scope.employer.id}, function (result) {
                    console.log(result);
                    $scope.jobInfo = result;
                });
            });


            Employer.get({id: 'my'}, function (result) {
                $scope.isNewProfile = false;
                $scope.employer = result;
                $scope.tempEmployer = result;
            }, function (response) {
                if (response.status == 404) {
                    $scope.isNewProfile = true;
                }
            });
        }]);

/* new-job.controller.js */
angular.module('stepApp').controller('NewJobController',
    ['$scope', '$rootScope', '$stateParams', 'DateUtils', 'CountrysByName', 'CatByOrganizationCat', 'JobTypeAllActive', 'entity', 'Job', 'Employer','$state','Cat','ActiveEduLevels','PayScale',
        function ($scope, $rootScope, $stateParams, DateUtils, CountrysByName, CatByOrganizationCat, JobTypeAllActive, entity, Job, Employer, $state,Cat,ActiveEduLevels,PayScale) {

            $scope.job = {};
            $scope.typeMinSalary = null;
            $scope.typeMaxSalary = null;
            $scope.salaryError = false;
            $scope.dateError = false;
            $scope.updateJob = false;
            $scope.organizationCategory = {};
            $scope.createJob = false;
            $scope.category = {};
            $scope.addNewCategory = false;

            if($stateParams.id !=null){
                Job.get({id: $stateParams.id}, function (result) {
                    $scope.job = result;
                });
            }

            ActiveEduLevels.query({}, function (result, headers) {
                $scope.eduLevels = result;
            });
            PayScale.query({page: $scope.page, size: 50}, function(result, headers) {
                $scope.payScales = result;
            });

            $scope.jobTypes = JobTypeAllActive.query({size: 50});

            if ($stateParams.id != null) {
                $scope.updateJob = true;
                $scope.createJob = false;
            } else {
                $scope.job.publishedAt = new Date().toISOString().substring(0,10);
                $scope.createJob = true;
            }

            $scope.isNewProfile = false;
            $scope.cats = [];
            Employer.get({id: 'my'}, function (result) {
                $scope.isNewProfile = false;
                $scope.employer = result;
                $scope.tempEmployer = result;
                $scope.organizationCategory = result.organizationCategory;
                $scope.cats2 = CatByOrganizationCat.query({id: result.organizationCategory.id}, function () {
                    angular.forEach($scope.cats2, function (value, key) {
                        {
                            $scope.cats.push({
                                description: value.description,
                                id: value.id,
                                cat: value.cat,
                                status: value.status
                            });
                        };
                    });
                    $scope.cats.push(
                        {
                            description: 'Other',
                            id: -1,
                            cat: 'Other',
                            status: true
                        }
                    );
                });
            }, function (response) {
                if (response.status == 404) {
                    $scope.isNewProfile = true;
                }
            });

            $scope.salaryValidation = function () {

                if ($scope.job.maximumSalary != null && $scope.job.maximumSalary > $scope.job.minimumSalary) {
                    $scope.salaryError = false;
                } else {
                    $scope.salaryError = true;
                }
            };
            $scope.deadlineValidation = function (deadlineValidation) {
                console.log("date .....");
                console.log(deadlineValidation);
                var d1 = Date.parse($scope.job.publishedAt);
                var d2 = Date.parse($scope.job.applicationDeadline);

                if (d1 >= d2) {
                    $scope.dateError = true;
                }else {
                    $scope.dateError = false;
                }
            };

            // $scope.calendar = {
            //     opened: {},
            //     dateFormat: 'yyyy-MM-dd',
            //     dateOptions: {},
            //     open: function ($event, which) {
            //         $event.preventDefault();
            //         $event.stopPropagation();
            //         $scope.calendar.opened[which] = true;
            //     }
            // };
            CountrysByName.query({}, function (result) {
                $scope.countries = result;
                if($stateParams.id == null){
                    angular.forEach(result, function(value, key){
                        if(value.name === "Bangladesh"){
                            $scope.job.country = value;
                        }
                    })
                }

            });
            $scope.showPreview = false;
            Employer.get({id: 'my'}, function (result) {
                $scope.employer = result;
            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:jobUpdate', result);
                $scope.isSaving = false;
            };

            $scope.job.publishedAt = new Date();

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.changeCat = function () {
                if ($scope.job.cat.id == -1) {
                    console.log("Other found");
                    $scope.addNewCategory = true;
                } else {
                    $scope.addNewCategory = false;
                }
            };

            $scope.save = function () {
                if ($scope.job.cat.id == -1) {
                    $scope.category.id = null;
                    $scope.category.status = true;
                    $scope.category.organizationCategory = $scope.organizationCategory;
                    Cat.save($scope.category, onSaveNewCat);
                } else {
                    $scope.job.location = $scope.job.location + ', ' + $scope.job.country.name;
                    $scope.job.user = {};
                    $scope.isSaving = true;
                    $scope.job.employer = $scope.employer;
                    $scope.job.user.id = $scope.employer.user.id;
                    console.log($scope.job);
                    if ($scope.job.id != null) {

                        Job.update($scope.job, onSaveSuccess, onSaveError);
                        $state.go('employer.job-list');
                    } else {
                        $scope.job.status = 'awaiting';
                        console.log($scope.job);
                        Job.save($scope.job, onSaveSuccess, onSaveError);
                        $state.go('employer.job-list');
                    }
                }

            };

            $scope.previewJob = function (value) {
                $scope.showPreview = value;

            }

            var onSaveNewCat = function (result) {
                $scope.job.cat = result;
                $scope.job.location = $scope.job.location + ', ' + $scope.job.country.name;
                $scope.job.user = {};
                $scope.isSaving = true;
                $scope.job.employer = $scope.employer;
                $scope.job.user.id = $scope.employer.user.id;
                console.log($scope.job);
                if ($scope.job.id != null) {
                    Job.update($scope.job, onSaveSuccess, onSaveError);
                    $state.go('employer.job-list');
                } else {
                    $scope.job.status = 'awaiting';
                    console.log($scope.job);
                    Job.save($scope.job, onSaveSuccess, onSaveError);
                    $state.go('employer.job-list');
                }

            }
        }]);

/* rejected-list.controller.js */
angular.module('stepApp').controller('RejectedListController',
    ['$scope', '$stateParams', 'Jobapplication','JobApplicationsRejectedList','$state',
        function ($scope, $stateParams, Jobapplication,JobApplicationsRejectedList,$state) {

            $scope.applicantList = "Rejected List";

            JobApplicationsRejectedList.query({id: $stateParams.id}, function (result) {
                $scope.applications = result;
            });

            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };

        }]);

/* short-list.controller.js */
angular.module('stepApp').controller('ShortListController',
    ['$scope', '$stateParams','JobApplicationsShortList', 'Jobapplication', '$state',
        function ($scope, $stateParams,JobApplicationsShortList, Jobapplication, $state) {

            $scope.applicantList = "Short Listed";

            JobApplicationsShortList.query({id: $stateParams.id}, function (result) {
                $scope.applications = result;
            });

            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };

        }]);

/* unviewed-list.controller.js */

angular.module('stepApp').controller('UnviewedListController',
    ['$scope', '$stateParams', 'Jobapplication','UniewedJobApplication','$state',
        function ($scope, $stateParams, Jobapplication,UniewedJobApplication,$state) {

            $scope.applicantList = "Unviewed List";

            UniewedJobApplication.query({id: $stateParams.id}, function (result) {
                $scope.applications = result;
            });

            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };
        }]);

/* update-detail.controller.js */
angular.module('stepApp').controller('IndustryDetailDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'DataUtils', 'entity', 'Employer', 'User', 'Country', 'Principal',
        function($scope, $stateParams, $modalInstance, DataUtils, entity, Employer, User, Country, Principal) {

            $scope.employer = entity;
            Principal.identity().then(function (account) {
                $scope.account = account;
                User.get({login: $scope.account.login}, function (result) {
                    $scope.user = result;
                    $scope.employer.user = $scope.user;

                });
            });

            $scope.users = User.query({size: 500});
            $scope.countries = Country.query({size: 500});
            $scope.load = function(id) {
                Employer.get({id : id}, function(result) {
                    $scope.employer = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:employerUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.employer.id != null) {
                    Employer.update($scope.employer, onSaveSuccess, onSaveError);
                } else {
                    Employer.save($scope.employer, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setCompanyLogo = function ($file, employer) {
                if ($file && $file.$error == 'pattern') {
                    return;
                }
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            employer.companyLogo = base64Data;
                            employer.companyLogoContentType = $file.type;
                        });
                    };
                }
            };

            //Resize the image (in the example 640 x 480px)
            function imageToDataUri(img, width, height) {

                // create an off-screen canvas
                var canvas = document.createElement('canvas'),
                    ctx = canvas.getContext('2d');

                // set its dimension to target size
                canvas.width = width;
                canvas.height = height;

                // draw source image into the off-screen canvas:
                ctx.drawImage(img, 0, 0, width, height);

                // encode image to data-uri with base64 version of compressed image
                return canvas.toDataURL();
            }
        }]);

/* viewed-list.controller.js */

angular.module('stepApp').controller('ViewedListController',
    ['$scope','$state', '$stateParams', 'Jobapplication','ViewedJobApplication',
        function ($scope,$state, $stateParams, Jobapplication,ViewedJobApplication) {

            $scope.applicantList = "Viewed List";

            ViewedJobApplication.query({id: $stateParams.id}, function (result) {
                console.log(result);
                $scope.applications = result;
            });
            $scope.updateStatus=function (status, id){
                Jobapplication.get({id : id}, function(result) {
                    $scope.jobapplication = result;
                    $scope.jobapplication.applicantStatus = status;
                    Jobapplication.update($scope.jobapplication);
                    $state.go('job.applied', {id:$stateParams.id}, { reload: true });

                });
            };
        }]);
