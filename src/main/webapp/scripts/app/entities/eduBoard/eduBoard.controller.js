'use strict';

angular.module('stepApp')
    .controller('EduBoardController',
    ['$scope', '$state', '$modal', 'EduBoard', 'EduBoardSearch', 'ParseLinks',
    function ($scope, $state, $modal, EduBoard, EduBoardSearch, ParseLinks) {

        $scope.eduBoards = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EduBoard.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.eduBoards = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            EduBoardSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.eduBoards = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.eduBoard = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);

/* eduBoard-dialog.controller.js */
angular.module('stepApp').controller('EduBoardDialogController',
    ['$scope', '$rootScope', '$stateParams', 'entity', '$state', 'EduBoard',
        function($scope, $rootScope, $stateParams, entity,$state, EduBoard) {

            $scope.eduBoard = entity;
            $scope.load = function(id) {
                EduBoard.get({id : id}, function(result) {
                    $scope.eduBoard = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:eduBoardUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('eduBoard', null, { reload: true });
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.eduBoard.id != null) {
                    EduBoard.update($scope.eduBoard, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.eduBoard.updated');
                } else {
                    EduBoard.save($scope.eduBoard, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.eduBoard.created');
                }
            };

            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
            };
        }]);

/* eduBoard-detail.controller.js */

angular.module('stepApp')
    .controller('EduBoardDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'EduBoard',
            function ($scope, $rootScope, $stateParams, entity, EduBoard) {
                $scope.eduBoard = entity;
                $scope.load = function (id) {
                    EduBoard.get({id: id}, function(result) {
                        $scope.eduBoard = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:eduBoardUpdate', function(event, result) {
                    $scope.eduBoard = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/* eduBoard-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('EduBoardDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'EduBoard',
            function($scope, $rootScope, $modalInstance, entity, EduBoard) {

                $scope.eduBoard = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    EduBoard.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.eduBoard.deleted');
                        });
                };

            }]);
