'use strict';

angular.module('stepApp').controller('InstVacancyTempDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'InstVacancyTemp', 'Institute', 'CmsTrade', 'CmsSubject', 'HrDesignationSetupByType', '$state',
        function ($scope, $stateParams, $modalInstance, entity, InstVacancyTemp, Institute, CmsTrade, CmsSubject, HrDesignationSetupByType, $state) {

            $scope.instVacancyTemp = entity;
            //$scope.institutes = Institute.query();
            //$scope.cmstrades = CmsTrade.query();
            //$scope.cmssubjects = CmsSubject.query();
            //$scope.instempldesignations = InstEmplDesignation.query();
            $scope.institutes = [];
            HrDesignationSetupByType.query({type: 'Non_Govt_Employee', size: 200}, function (result) {
                $scope.hrDesignationSetups = result;
            });

            if ($stateParams.vacancyId != null) {
                InstVacancyTemp.get({id: $stateParams.vacancyId}, function (result) {
                    $scope.instVacancyTemp = result;
                    //$scope.institutes = result.institute;
                    //console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&p');
                    //console.log($scope.instVacancyTemp.institute);
                    //$scope.instVacancyTemp.designationSetup = result.designationSetup;
                });
            } else if ($stateParams.instituteId != null) {
                Institute.get({id: $stateParams.instituteId}, function (instituteData) {
                    $scope.instVacancyTemp.institute = instituteData;
                });
            }

            $scope.load = function (id) {
                InstVacancyTemp.get({id: $stateParams.vacancyId}, function (result) {
                    $scope.instVacancyTemp = result;
                });
            };

            var onSaveFinished = function (result) {
                $state.go('mpo.details',null, {reload: true});
                // $scope.$emit('stepApp:instVacancyTempUpdate', result);
                $modalInstance.close(result);
                $modalInstance.dismiss('cancel');
                //$state.go('mpo.details',{reload: true});
                $('#myModal').hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            };

            $scope.save = function () {
                if ($scope.instVacancyTemp.id != null) {
                    InstVacancyTemp.update($scope.instVacancyTemp, onSaveFinished);
                } else {
                    InstVacancyTemp.save($scope.instVacancyTemp, onSaveFinished);
                }

            };

            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
                $state.go('mpo.details', {reload: true});
                $('#myModal').hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            };
        }]);
