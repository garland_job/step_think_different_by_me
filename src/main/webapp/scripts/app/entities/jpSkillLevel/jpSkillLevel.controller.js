'use strict';

angular.module('stepApp')
    .controller('JpSkillLevelController',
     ['$scope', '$state', '$modal', 'JpSkillLevel', 'JpSkillLevelSearch', 'ParseLinks',
     function ($scope, $state, $modal, JpSkillLevel, JpSkillLevelSearch, ParseLinks) {

        $scope.jpSkillLevels = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            JpSkillLevel.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.jpSkillLevels = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            JpSkillLevelSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.jpSkillLevels = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.jpSkillLevel = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);

/* jpSkillLevel-dialog.controller.js */
angular.module('stepApp').controller('JpSkillLevelDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'JpSkillLevel',
        function($scope, $rootScope, $stateParams, $state, entity, JpSkillLevel) {
            $scope.jpSkillLevel = entity;
            $scope.load = function(id) {
                JpSkillLevel.get({id : id}, function(result) {
                    $scope.jpSkillLevel = result;
                });
            };
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:jpSkillLevelUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('jpSkillLevel', null, { reload: true });
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.jpSkillLevel.id != null) {
                    JpSkillLevel.update($scope.jpSkillLevel, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.jpSkillLevel.updated');
                } else {
                    JpSkillLevel.save($scope.jpSkillLevel, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.jpSkillLevel.created');
                }
            };
            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
            };
        }]);

/* jpSkillLevel-detail.controller.js */
angular.module('stepApp')
    .controller('JpSkillLevelDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'JpSkillLevel',
            function ($scope, $rootScope, $stateParams, entity, JpSkillLevel) {
                $scope.jpSkillLevel = entity;
                $scope.load = function (id) {
                    JpSkillLevel.get({id: id}, function(result) {
                        $scope.jpSkillLevel = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:jpSkillLevelUpdate', function(event, result) {
                    $scope.jpSkillLevel = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/* jpSkillLevel-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('JpSkillLevelDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'JpSkillLevel',
            function($scope, $rootScope, $modalInstance, entity, JpSkillLevel) {

                $scope.jpSkillLevel = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    JpSkillLevel.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.jpSkillLevel.deleted');
                        });
                };

            }]);
