'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('bloodGroupInfo', {
                parent: 'entity',
                url: '/bloodGroupInfos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.bloodGroupInfo.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/bloodGroupInfo/bloodGroupInfos.html',
                        controller: 'BloodGroupInfoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('bloodGroupInfo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('bloodGroupInfo.detail', {
                parent: 'entity',
                url: '/bloodGroupInfo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.bloodGroupInfo.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/bloodGroupInfo/bloodGroupInfo-detail.html',
                        controller: 'BloodGroupInfoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('bloodGroupInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'BloodGroupInfo', function($stateParams, BloodGroupInfo) {
                        return BloodGroupInfo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('bloodGroupInfo.new', {
                parent: 'bloodGroupInfo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/bloodGroupInfo/bloodGroupInfo-dialog.html',
                        controller: 'BloodGroupInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    status: null,
                                    createDate: null,
                                    createBy: null,
                                    updateBy: null,
                                    updateDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('bloodGroupInfo', null, { reload: true });
                    }, function() {
                        $state.go('bloodGroupInfo');
                    })
                }]
            })
            .state('bloodGroupInfo.edit', {
                parent: 'bloodGroupInfo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/bloodGroupInfo/bloodGroupInfo-dialog.html',
                        controller: 'BloodGroupInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['BloodGroupInfo', function(BloodGroupInfo) {
                                return BloodGroupInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('bloodGroupInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('bloodGroupInfo.delete', {
                parent: 'bloodGroupInfo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/bloodGroupInfo/bloodGroupInfo-delete-dialog.html',
                        controller: 'BloodGroupInfoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['BloodGroupInfo', function(BloodGroupInfo) {
                                return BloodGroupInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('bloodGroupInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
