'use strict';

angular.module('stepApp')
    .controller('BloodGroupInfoDetailController', function ($scope, $rootScope, $stateParams, entity, BloodGroupInfo) {
        $scope.bloodGroupInfo = entity;
        $scope.load = function (id) {
            BloodGroupInfo.get({id: id}, function(result) {
                $scope.bloodGroupInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:bloodGroupInfoUpdate', function(event, result) {
            $scope.bloodGroupInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
