'use strict';

angular.module('stepApp').controller('BloodGroupInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'BloodGroupInfo',
        function($scope, $stateParams, $modalInstance, entity, BloodGroupInfo) {

        $scope.bloodGroupInfo = entity;
        $scope.load = function(id) {
            BloodGroupInfo.get({id : id}, function(result) {
                $scope.bloodGroupInfo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:bloodGroupInfoUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.bloodGroupInfo.id != null) {
                BloodGroupInfo.update($scope.bloodGroupInfo, onSaveSuccess, onSaveError);
            } else {
                BloodGroupInfo.save($scope.bloodGroupInfo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
