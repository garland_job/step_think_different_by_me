'use strict';

angular.module('stepApp')
	.controller('BloodGroupInfoDeleteController', function($scope, $modalInstance, entity, BloodGroupInfo) {

        $scope.bloodGroupInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            BloodGroupInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });