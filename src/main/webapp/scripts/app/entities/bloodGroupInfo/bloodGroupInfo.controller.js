'use strict';

angular.module('stepApp')
    .controller('BloodGroupInfoController', function ($scope, $state, $modal, BloodGroupInfo, BloodGroupInfoSearch, ParseLinks) {
      
        $scope.bloodGroupInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            BloodGroupInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.bloodGroupInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BloodGroupInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.bloodGroupInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.bloodGroupInfo = {
                name: null,
                status: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    });
