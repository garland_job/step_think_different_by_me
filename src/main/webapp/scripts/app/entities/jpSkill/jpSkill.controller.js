'use strict';

angular.module('stepApp')
    .controller('JpSkillController',
     ['$scope', '$state', '$modal', 'JpSkill', 'JpSkillSearch', 'ParseLinks',
     function ($scope, $state, $modal, JpSkill, JpSkillSearch, ParseLinks) {
        $scope.jpSkills = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            JpSkill.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.jpSkills = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            JpSkillSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.jpSkills = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.jpSkill = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);

/* jpSkill-dialog.controller.js */
angular.module('stepApp').controller('JpSkillDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'JpSkill',
        function($scope, $rootScope, $stateParams, $state, entity, JpSkill) {

            $scope.jpSkill = entity;
            $scope.load = function(id) {
                JpSkill.get({id : id}, function(result) {
                    $scope.jpSkill = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:jpSkillUpdate', result);
                $scope.isSaving = false;
                $state.go('jpSkill', null, { reload: true });
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.jpSkill.id != null) {
                    JpSkill.update($scope.jpSkill, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.jpSkill.updated');
                } else {
                    JpSkill.save($scope.jpSkill, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.jpSkill.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/* jpSkill-detail.controller.js */

angular.module('stepApp')
    .controller('JpSkillDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'JpSkill',
            function ($scope, $rootScope, $stateParams, entity, JpSkill) {
                $scope.jpSkill = entity;
                $scope.load = function (id) {
                    JpSkill.get({id: id}, function(result) {
                        $scope.jpSkill = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:jpSkillUpdate', function(event, result) {
                    $scope.jpSkill = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/* jpSkill-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('JpSkillDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'JpSkill',
            function($scope, $rootScope, $modalInstance, entity, JpSkill) {

                $scope.jpSkill = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    JpSkill.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.jpSkill.deleted');
                        });
                };

    }]);
