'use strict';

angular.module('stepApp')
    .controller('BankSetupController',
    ['$scope', '$state', '$modal', 'BankSetup', 'BankSetupSearch', 'ParseLinks',
    function ($scope, $state, $modal, BankSetup, BankSetupSearch, ParseLinks) {

        $scope.bankSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            BankSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.bankSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BankSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.bankSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.bankSetup = {
                name: null,
                code: null,
                branchName: null,
                id: null
            };
        };
    }]);


/*bankSetup-dialog.controller.js*/
angular.module('stepApp').controller('BankSetupDialogController',
    ['$scope', '$state', '$stateParams', 'entity', 'BankSetup', 'Upazila',
        function($scope, $state, $stateParams, entity, BankSetup, Upazila) {

            $scope.bankSetup = entity;
            $scope.upazilas = Upazila.query();
            $scope.load = function(id) {
                BankSetup.get({id : id}, function(result) {
                    $scope.bankSetup = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:bankSetupUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('bankSetup',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.bankSetup.id != null) {
                    BankSetup.update($scope.bankSetup, onSaveSuccess, onSaveError);
                } else {
                    BankSetup.save($scope.bankSetup, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
                $scope.bankSetup=null;

            };
        }]);

/*bankSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('BankSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'BankSetup', 'Upazila','BankBranchsByBankSetupId',
            function ($scope, $rootScope, $stateParams, entity, BankSetup, Upazila,BankBranchsByBankSetupId) {
                $scope.bankSetup = entity;
                $scope.load = function (id) {
                    BankSetup.get({id: id}, function(result) {
                        $scope.bankSetup = result;
                    });
                };

                BankBranchsByBankSetupId.query({bankSetupId:$stateParams.id, size: 500}, function(result) {
                    $scope.bankBranchsByBank = result;
                });
                var unsubscribe = $rootScope.$on('stepApp:bankSetupUpdate', function(event, result) {
                    $scope.bankSetup = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*bankSetup-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('BankSetupDeleteController',
        ['$scope', '$modalInstance', 'entity', 'BankSetup',
            function($scope, $modalInstance, entity, BankSetup) {

                $scope.bankSetup = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    BankSetup.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);


/*bankBranch-dialog.controller.js*/
angular.module('stepApp').controller('BankBranchDialogController',
    ['$scope', '$state','$rootScope','$stateParams','$q','District', 'Upazila','Division', 'entity', 'BankBranch', 'BankSetup', 'DistrictsByDivision','UpazilasByDistrict',
        function($scope, $state,$rootScope,$stateParams,$q,District,Upazila, Division, entity, BankBranch, BankSetup,DistrictsByDivision,UpazilasByDistrict) {

            //$scope.bankBranch = entity;
            $scope.banksetups = BankSetup.query();
            //$scope.upazilas = Upazila.query();
            $scope.branches = [];
            $scope.bankBranch2 = {};
            $scope.divisions = Division.query();

            // $scope.load = function(id) {
            //     BankBranch.get({id : $stateParams.id}, function(result) {
            //         $scope.bankBranch = result;
            //     });
            // };
            if($stateParams.id !=null){
                BankBranch.get({id : $stateParams.id}, function(result) {
                    console.log('******************************');
                    console.log(result.district.division);
                    console.log("district");
                    console.log(result.district);
                    console.log(result.upazila);
                    $scope.bankBranch = result;
                    $scope.bankBranch.division = result.district.division;
                    $scope.bankBranch.district = result.district;
                    $scope.bankBranch.upazila = result.upazila;
                    $scope.branches[0].address = result.address;
                    $scope.branches[0].brName = result.brName;

                    // console.log($scope.bankBranch);
                    console.log("Bank Branch edit test");
                });
            }



            // $q.all([entity.$promise]).then(function() {
            //     if(entity.upazila){
            //         $scope.upazila = entity.upazila.name;
            //         if(entity.upazila.district){
            //             $scope.district = entity.upazila.district.name;
            //             if(entity.upazila.district.division){
            //                 $scope.division = entity.upazila.district.division.name;
            //             }
            //             else{
            //                 $scope.division = "Select Division"
            //             }
            //         }
            //         else{
            //             $scope.district = "Select District";
            //             $scope.division = "Select Division"
            //         }
            //     }
            //     else{
            //         $scope.division = "Select Division";
            //         $scope.district = "Select District";
            //         $scope.upazila = "Select Upazilla";
            //     }
            // });


            // var allDistrict= District.query({page: $scope.page, size: 65}, function(result) { return result;});
            // var allUpazila= Upazila.query({page: $scope.page, size: 500}, function(result) { return result;});



            //$scope.districts = District.query();
            //$scope.upazilas = Upazila.query();

            $scope.updatedDistrict=function(select){
                $scope.districts=[];
                // angular.forEach(allDistrict, function(district) {
                //     if(select.id == district.division.id){
                //         $scope.districts.push(district);
                //     }
                // });
                if($stateParams.id != null){
                    $scope.bankBranch.district.name = '';
                    $scope.bankBranch.upazila.name = '';
                }
                DistrictsByDivision.query({id:select.id},function(districtData){
                    $scope.districts = districtData;
                });
            };

            $scope.updatedUpazila=function(select){
                /* console.log("selected district .............");
                 console.log(select);*/
                $scope.upazilas=[];
                // angular.forEach(allUpazila, function(upazila) {
                //     if(select.id==upazila.district.id){
                //         $scope.upazilas.push(upazila);
                //     }
                // });

                UpazilasByDistrict.query({id:select.id},function(upazilaData){
                    $scope.upazilas = upazilaData;
                });

            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:bankBranchUpdate', result);
                $scope.isSaving = false;
                $state.go('bankBranch',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                console.log('^^^^^^^^^^^^^^^');
                console.log($stateParams.id);
                console.log($scope.bankBranch);

                if ($stateParams.id == null) {
                    angular.forEach($scope.branches, function(branch) {
                        $scope.bankBranch2.brName = branch.brName;
                        $scope.bankBranch2.address = branch.address;
                        $scope.bankBranch2.status = $scope.bankBranch.status;
                        $scope.bankBranch2.bankSetup = $scope.bankBranch.bankSetup;
                        $scope.bankBranch2.upazila = $scope.bankBranch.upazila;
                        $scope.bankBranch2.district = $scope.bankBranch.district;
                        console.log("branches");
                        console.log($scope.bankBranch2);
                        console.log($scope.bankBranch);
                        // if ($stateParams.id != null) {
                        //     BankBranch.update($scope.bankBranch2);
                        //     $scope.bankBranch2 = {};
                        // } else {
                            BankBranch.save($scope.bankBranch2);
                            $scope.bankBranch2 = {};
                        // }
                    });
                    }else{
                        $scope.bankBranch.brName = $scope.branches[0].brName;
                        $scope.bankBranch.address = $scope.branches[0].address;
                        BankBranch.update($scope.bankBranch);
                        $scope.bankBranch = {};
                    }
                $rootScope.setSuccessMessage('stepApp.bankBranch.created');
                $state.go('bankBranch', null, { reload: true });
            };



            $scope.branches.push({
                name: '',
                address: '',
                id:''
            });

            $scope.addMoreBranch = function () {
                $scope.branches.push({
                    name: '',
                    address: '',
                    id:''
                });
            };

        }]);


/*bankBranch-detail.controller.js*/
angular.module('stepApp')
    .controller('BankBranchDetailController', function ($scope, $rootScope, $stateParams, entity, BankBranch, BankSetup, Upazila) {
        $scope.bankBranch = entity;
        $scope.load = function (id) {
            BankBranch.get({id: id}, function(result) {
                $scope.bankBranch = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:bankBranchUpdate', function(event, result) {
            $scope.bankBranch = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });


/*bankBranch-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('BankBranchDeleteController', function($scope, $modalInstance, entity, BankBranch) {

        $scope.bankBranch = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            BankBranch.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });


/*bankBranch.controller.js*/
angular.module('stepApp')
    .controller('BankBranchController', function ($scope, $state, $modal, BankBranch, BankBranchSearch, ParseLinks,BankSetup) {

        $scope.bankBranchs = [];
        $scope.page = 0;

        BankSetup.query({page: $scope.page, size: 20}, function(result, headers) {
            $scope.links = ParseLinks.parse(headers('link'));
            $scope.bankSetups = result;
        });
        $scope.loadAll = function() {
            BankBranch.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.bankBranchs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BankBranchSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.bankBranchs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.bankBranch = {
                brName: null,
                address: null,
                status: null,
                id: null
            };
        };
    });


/*bankAssign-dialog.controller.js*/
angular.module('stepApp').controller('BankAssignDialogController',
    ['$scope', '$stateParams', '$q', 'entity', 'BankAssign', 'User', 'BankSetup', 'Upazila','Division','District','$state',
        function($scope, $stateParams, $q, entity, BankAssign, User, BankSetup, Upazila, Division, District, $state) {

            $scope.bankAssign = entity;
            $scope.users = User.query();
            $scope.banksetups = BankSetup.query();
            /*$scope.upazilas = Upazila.query({filter: 'bankassign-is-null'});*/
            /*$q.all([$scope.bankAssign.$promise, $scope.upazilas.$promise]).then(function() {
             if (!$scope.bankAssign.upazila.id) {
             return $q.reject();
             }
             return Upazila.get({id : $scope.bankAssign.upazila.id}).$promise;
             }).then(function(upazila) {
             $scope.upazilas.push(upazila);
             });*/
            $scope.load = function(id) {
                BankAssign.get({id : id}, function(result) {
                    $scope.bankAssign = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:bankAssignUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.bankAssign.id != null) {
                    BankAssign.update($scope.bankAssign, onSaveSuccess, onSaveError);
                } else {
                    for (var i = 0; i < $scope.upazilas.length; i++){

                        //var srmsRegInfo = $scope.srmsRegInfos[i];
                        if($scope.upazilas[i].isSelected){
                            var bankAssign2 = {};
                            bankAssign2.bank = $scope.bankAssign.bank;
                            console.log($scope.upazilas[i].id);
                            bankAssign2.upazila=$scope.upazilas[i];
                            console.log(bankAssign2);
                            BankAssign.save(bankAssign2);
                        }
                    }


                }

                $state.go('bankAssign', null, { reload: true });
            };


            $scope.divisions = Division.query();
            var allDistrict= District.query({page: $scope.page, size: 65}, function(result, headers) { return result;});
            var allUpazila= Upazila.query({page: $scope.page, size: 500}, function(result, headers) { return result;});

            $scope.updatedUpazila=function(select){
                /* console.log("selected district .............");
                 console.log(select);*/
                $scope.upazilas=[];
                angular.forEach(allUpazila, function(upazila) {
                    if(select.id==upazila.district.id){
                        $scope.upazilas.push(upazila);
                    }
                });

            };

            $scope.updatedDistrict=function(select){
                $scope.districts=[];
                angular.forEach(allDistrict, function(district) {
                    if(select.id == district.division.id){
                        $scope.districts.push(district);
                    }
                });
            };

            $scope.areAllSelected = false;

            $scope.updateUpazilaSelection = function (upazilas, selectionValue) {
                for (var i = 0; i < upazilas.length; i++)
                {
                    upazilas[i].isSelected = selectionValue;
                }
            };

            /* $scope.clear = function() {
             $modalInstance.dismiss('cancel');
             };*/
        }]);


/*bankAssign-detail.controller.js*/

angular.module('stepApp')
    .controller('BankAssignDetailController', function ($scope, $rootScope, $stateParams, entity, BankAssign, User, BankSetup, Upazila) {
        $scope.bankAssign = entity;
        $scope.load = function (id) {
            BankAssign.get({id: id}, function(result) {
                $scope.bankAssign = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:bankAssignUpdate', function(event, result) {
            $scope.bankAssign = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });


/*bankAssign-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('BankAssignDeleteController', function($scope, $modalInstance, entity, BankAssign) {

        $scope.bankAssign = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            BankAssign.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
/*bankAssign.controller.js*/
angular.module('stepApp')
    .controller('BankAssignController', function ($scope, $state, $modal, BankAssign, BankAssignSearch, ParseLinks) {

        $scope.bankAssigns = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            BankAssign.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.bankAssigns = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BankAssignSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.bankAssigns = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.bankAssign = {
                createdDate: null,
                modifiedDate: null,
                status: null,
                id: null
            };
        };
    });


