'use strict';

angular.module('stepApp')
    .controller('InstShopInfoController',
    ['$scope', '$state', '$modal', 'InstShopInfo', 'InstShopInfoSearch', 'ParseLinks',
    function ($scope, $state, $modal, InstShopInfo, InstShopInfoSearch, ParseLinks) {

        $scope.instShopInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            InstShopInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.instShopInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            InstShopInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.instShopInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instShopInfo = {
                nameOrNumber: null,
                buildingNameOrNumber: null,
                length: null,
                width: null,
                id: null
            };
        };
    }]);

/*  instShopInfo-dialog.controller.js */
angular.module('stepApp').controller('InstShopInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'InstShopInfo', 'InstInfraInfo',
        function($scope, $stateParams, $modalInstance, entity, InstShopInfo, InstInfraInfo) {

            $scope.instShopInfo = entity;
            $scope.instinfrainfos = InstInfraInfo.query();
            $scope.load = function(id) {
                InstShopInfo.get({id : id}, function(result) {
                    $scope.instShopInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instShopInfoUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instShopInfo.id != null) {
                    InstShopInfo.update($scope.instShopInfo, onSaveSuccess, onSaveError);
                } else {
                    InstShopInfo.save($scope.instShopInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/* instShopInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('InstShopInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstShopInfo', 'InstInfraInfo',
            function ($scope, $rootScope, $stateParams, entity, InstShopInfo, InstInfraInfo) {
                $scope.instShopInfo = entity;
                $scope.load = function (id) {
                    InstShopInfo.get({id: id}, function(result) {
                        $scope.instShopInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instShopInfoUpdate', function(event, result) {
                    $scope.instShopInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*  instShopInfo-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('InstShopInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstShopInfo',
            function($scope, $modalInstance, entity, InstShopInfo) {

                $scope.instShopInfo = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstShopInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);
