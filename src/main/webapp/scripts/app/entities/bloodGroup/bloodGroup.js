'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('bloodGroup', {
                parent: 'entity',
                url: '/bloodGroups',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.bloodGroup.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/bloodGroup/bloodGroups.html',
                        controller: 'BloodGroupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('bloodGroup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('bloodGroup.detail', {
                parent: 'entity',
                url: '/bloodGroup/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.bloodGroup.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/bloodGroup/bloodGroup-detail.html',
                        controller: 'BloodGroupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('bloodGroup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'BloodGroup', function($stateParams, BloodGroup) {
                        return BloodGroup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('bloodGroup.new', {
                parent: 'bloodGroup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/bloodGroup/bloodGroup-dialog.html',
                        controller: 'BloodGroupDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    status: null,
                                    createDate: null,
                                    createBy: null,
                                    updateBy: null,
                                    updateDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('bloodGroup', null, { reload: true });
                    }, function() {
                        $state.go('bloodGroup');
                    })
                }]
            })
            .state('bloodGroup.edit', {
                parent: 'bloodGroup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/bloodGroup/bloodGroup-dialog.html',
                        controller: 'BloodGroupDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['BloodGroup', function(BloodGroup) {
                                return BloodGroup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('bloodGroup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('bloodGroup.delete', {
                parent: 'bloodGroup',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/bloodGroup/bloodGroup-delete-dialog.html',
                        controller: 'BloodGroupDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['BloodGroup', function(BloodGroup) {
                                return BloodGroup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('bloodGroup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
