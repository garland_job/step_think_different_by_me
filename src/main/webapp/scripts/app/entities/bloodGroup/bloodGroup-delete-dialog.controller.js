'use strict';

angular.module('stepApp')
	.controller('BloodGroupDeleteController', function($scope, $modalInstance, entity, BloodGroup) {

        $scope.bloodGroup = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            BloodGroup.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });