'use strict';

angular.module('stepApp').controller('BloodGroupDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'BloodGroup',
        function($scope, $stateParams, $modalInstance, entity, BloodGroup) {

        $scope.bloodGroup = entity;
        $scope.load = function(id) {
            BloodGroup.get({id : id}, function(result) {
                $scope.bloodGroup = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:bloodGroupUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.bloodGroup.id != null) {
                BloodGroup.update($scope.bloodGroup, onSaveSuccess, onSaveError);
            } else {
                BloodGroup.save($scope.bloodGroup, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
