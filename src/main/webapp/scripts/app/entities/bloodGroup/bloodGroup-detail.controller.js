'use strict';

angular.module('stepApp')
    .controller('BloodGroupDetailController', function ($scope, $rootScope, $stateParams, entity, BloodGroup) {
        $scope.bloodGroup = entity;
        $scope.load = function (id) {
            BloodGroup.get({id: id}, function(result) {
                $scope.bloodGroup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:bloodGroupUpdate', function(event, result) {
            $scope.bloodGroup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
