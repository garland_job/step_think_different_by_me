'use strict';

angular.module('stepApp')
    .controller('CatController',
    ['$scope', '$state', '$modal', 'Cat', 'CatSearch', 'ParseLinks',
     function ($scope, $state, $modal, Cat, CatSearch, ParseLinks) {

        $scope.cats = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Cat.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cats = result;
                $scope.total = headers('x-total-count');
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.loadAll();
        $scope.search = function () {

            CatSearch.query({query: $scope.searchQuery}, function(result) {

            }, function(response) {
                if(response.status === 404) {
                     $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cat = {
                cat: null,
                description: null,
                id: null
            };
        };

        // bulk operations start
        $scope.areAllCatsSelected = false;

        $scope.updateCatsSelection = function (catArray, selectionValue) {
            for (var i = 0; i < catArray.length; i++)
            {
            catArray[i].isSelected = selectionValue;
            }
        };

        $scope.sync = function (){
            for (var i = 0; i < $scope.cats.length; i++){
                var cat = $scope.cats[i];
                if(cat.isSelected){
                    Cat.update(cat);
                }
            }
        };

        $scope.order = function (predicate, reverse) {
            $scope.predicate = predicate;
            $scope.reverse = reverse;
            Cat.query({page: $scope.page, size: 5000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cats = result;
                $scope.total = headers('x-total-count');
            });
        };
        // bulk operations end

    }]);

/*cat-form.controller.js*/

angular.module('stepApp').controller('CatFormController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'Cat', 'OrganizationCategory',
        function($scope, $rootScope, $state, $stateParams, entity, Cat, OrganizationCategory) {

            $scope.cat = {};
            $scope.cat.status = true;
            $scope.categorys = OrganizationCategory.query();

            Cat.get({id : $stateParams.id}, function(response){
                $scope.cat = response;
                console.log($scope.cat.status);
            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:catUpdate', result);
                $scope.isSaving = false;
                $state.go('cat');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $rootScope.setErrorMessage('stepApp.cat.error');
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if( typeof $scope.cat.status == 'undefined'){
                    $scope.cat.status = true;
                }

                if ($scope.cat.id != null) {
                    Cat.update($scope.cat, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.cat.updated');
                } else {
                    Cat.save($scope.cat, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.cat.created');
                }
            };



        }]);
/*cat-detail.controller.js*/

angular.module('stepApp')
    .controller('CatDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'Cat', 'Job',
            function ($scope, $rootScope, $stateParams, entity, Cat, Job) {
                $scope.cat = entity;
                $scope.load = function (id) {
                    Cat.get({id: id}, function (result) {
                        $scope.cat = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:catUpdate', function (event, result) {
                    $scope.cat = result;
                });

                $scope.detailJobCat = false;
                $scope.showCatDetail = function (value) {
                    $scope.detailJobCat = value;
                }
                $scope.$on('$destroy', unsubscribe);

            }]);
/*cat-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('CatDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'Cat',
            function($scope, $rootScope, $modalInstance, entity, Cat) {

                $scope.cat = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    Cat.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.cat.deleted');
                        });

                };

            }]);

