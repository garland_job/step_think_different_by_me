'use strict';

angular.module('stepApp')
    .controller('MpoVacancyRoleDesgnationsController', function ($scope, $state, $modal, MpoVacancyRoleDesgnations, MpoVacancyRoleDesgnationsSearch, ParseLinks) {

        $scope.mpoVacancyRoleDesgnationss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MpoVacancyRoleDesgnations.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.mpoVacancyRoleDesgnationss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            MpoVacancyRoleDesgnationsSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.mpoVacancyRoleDesgnationss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.mpoVacancyRoleDesgnations = {
                crateDate: null,
                updateDate: null,
                status: null,
                totalPost: null,
                id: null
            };
        };
    });


/* mpoVacancyRoleDesgnations-dialog.controller.js */

angular.module('stepApp').controller('MpoVacancyRoleDesgnationsDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'MpoVacancyRoleDesgnations', 'MpoVacancyRole', 'User', 'InstEmplDesignation', 'CmsSubject',
        function($scope, $stateParams, $modalInstance, $q, entity, MpoVacancyRoleDesgnations, MpoVacancyRole, User, InstEmplDesignation, CmsSubject) {

            $scope.mpoVacancyRoleDesgnations = entity;
            $scope.mpovacancyroles = MpoVacancyRole.query();
            $scope.users = User.query();
            $scope.instempldesignations = InstEmplDesignation.query();
            $scope.cmssubjects = CmsSubject.query();
            $scope.load = function(id) {
                MpoVacancyRoleDesgnations.get({id : id}, function(result) {
                    $scope.mpoVacancyRoleDesgnations = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:mpoVacancyRoleDesgnationsUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.mpoVacancyRoleDesgnations.id != null) {
                    MpoVacancyRoleDesgnations.update($scope.mpoVacancyRoleDesgnations, onSaveSuccess, onSaveError);
                } else {
                    MpoVacancyRoleDesgnations.save($scope.mpoVacancyRoleDesgnations, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);

/* mpoVacancyRoleDesgnations-detail.controller.js */
angular.module('stepApp')
    .controller('MpoVacancyRoleDesgnationsDetailController', function ($scope, $rootScope, $stateParams, entity, MpoVacancyRoleDesgnations, MpoVacancyRole, User, InstEmplDesignation, CmsSubject) {
        $scope.mpoVacancyRoleDesgnations = entity;
        $scope.load = function (id) {
            MpoVacancyRoleDesgnations.get({id: id}, function(result) {
                $scope.mpoVacancyRoleDesgnations = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:mpoVacancyRoleDesgnationsUpdate', function(event, result) {
            $scope.mpoVacancyRoleDesgnations = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

/*mpoVacancyRoleDesgnations-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('MpoVacancyRoleDesgnationsDeleteController', function($scope, $modalInstance, entity, MpoVacancyRoleDesgnations) {

        $scope.mpoVacancyRoleDesgnations = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            MpoVacancyRoleDesgnations.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
