'use strict';

angular.module('stepApp')
    .controller('MpoVacancyRoleTradeController', function ($scope, $state, $modal, MpoVacancyRoleTrade, MpoVacancyRoleTradeSearch, ParseLinks) {

        $scope.mpoVacancyRoleTrades = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MpoVacancyRoleTrade.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.mpoVacancyRoleTrades = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            MpoVacancyRoleTradeSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.mpoVacancyRoleTrades = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.mpoVacancyRoleTrade = {
                crateDate: null,
                updateDate: null,
                status: null,
                id: null
            };
        };
    });

/* mpoVacancyRoleTrade-dialog.controller.js */
angular.module('stepApp').controller('MpoVacancyRoleTradeDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'MpoVacancyRoleTrade', 'MpoVacancyRole', 'User', 'CmsTrade',
        function($scope, $stateParams, $modalInstance, $q, entity, MpoVacancyRoleTrade, MpoVacancyRole, User, CmsTrade) {

            $scope.mpoVacancyRoleTrade = entity;
            $scope.mpovacancyroles = MpoVacancyRole.query();
            $scope.users = User.query();
            $scope.cmstrades = CmsTrade.query();
            $scope.load = function(id) {
                MpoVacancyRoleTrade.get({id : id}, function(result) {
                    $scope.mpoVacancyRoleTrade = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:mpoVacancyRoleTradeUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.mpoVacancyRoleTrade.id != null) {
                    MpoVacancyRoleTrade.update($scope.mpoVacancyRoleTrade, onSaveSuccess, onSaveError);
                } else {
                    MpoVacancyRoleTrade.save($scope.mpoVacancyRoleTrade, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);


/* mpoVacancyRoleTrade-detail.controller.js */

angular.module('stepApp')
    .controller('MpoVacancyRoleTradeDetailController', function ($scope, $rootScope, $stateParams, entity, MpoVacancyRoleTrade, MpoVacancyRole, User, CmsTrade) {
        $scope.mpoVacancyRoleTrade = entity;
        $scope.load = function (id) {
            MpoVacancyRoleTrade.get({id: id}, function(result) {
                $scope.mpoVacancyRoleTrade = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:mpoVacancyRoleTradeUpdate', function(event, result) {
            $scope.mpoVacancyRoleTrade = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });


/* mpoVacancyRoleTrade-delete-dialog.controller.js */


 angular.module('stepApp')
 .controller('MpoVacancyRoleTradeDeleteController', function($scope, $modalInstance, entity, MpoVacancyRoleTrade) {

        $scope.mpoVacancyRoleTrade = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            MpoVacancyRoleTrade.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
