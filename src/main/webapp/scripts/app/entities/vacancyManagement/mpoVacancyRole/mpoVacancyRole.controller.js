'use strict';

angular.module('stepApp')
    .controller('MpoVacancyRoleController', function ($scope, $state, $modal, MpoVacancyRole, MpoVacancyRoleSearch, ParseLinks) {

        $scope.mpoVacancyRoles = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MpoVacancyRole.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.mpoVacancyRoles = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            MpoVacancyRoleSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.mpoVacancyRoles = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.mpoVacancyRole = {
                crateDate: null,
                updateDate: null,
                status: null,
                totalTrade: null,
                vacancyRoleType: null,
                totalVacancy: null,
                id: null
            };
        };
    });

/* mpoVacancyRole-dialog.controller.js */
angular.module('stepApp').controller('MpoVacancyRoleDialogController',
    ['$scope', '$stateParams', '$q', 'entity', 'MpoVacancyRole', 'CmsCurriculum', 'User', 'CmsTrade','CmsSubject', '$timeout', '$rootScope', 'MpoVacancyRoleTrade', 'MpoVacancyRoleDesgnations', '$state', 'MpoVacancyRoleDesgnationsByRole', 'MpoCmsTradeByCurriculumId', 'MpoVacancyRoleTradeByVacancyRole','FindActivecmsCurriculums','HrDesignationSetupByType','GetCmsSubjectByGeneralStatus','AllCmsSubjectByTrade',
        function ($scope, $stateParams, $q, entity, MpoVacancyRole, CmsCurriculum, User, CmsTrade, CmsSubject, $timeout, $rootScope, MpoVacancyRoleTrade, MpoVacancyRoleDesgnations, $state, MpoVacancyRoleDesgnationsByRole, MpoCmsTradeByCurriculumId, MpoVacancyRoleTradeByVacancyRole,FindActivecmsCurriculums,HrDesignationSetupByType,GetCmsSubjectByGeneralStatus,AllCmsSubjectByTrade) {

            $scope.mpoVacancyRole = {};

            $scope.mpoVacancyRoleTrade = {};
            $scope.mpoTrades = [];
            $scope.checkedCurriculums = [];
            $scope.checkedTrades = [];
            $scope.mpoVacancyRoleDesgnations = [];
            $scope.mpoVacancyRoleDesgnation = {};
            $scope.cmstrades = [];
            $scope.cmstrades2 = [];
            $scope.mpoVacancyTradeRoleDesignations = [];
            $scope.cmsSubjectss= [];
            $scope.cmsSubjects = [];


            $scope.setCmsSubject = function(tradeId,index){
                AllCmsSubjectByTrade.get({tradeId:tradeId},function(result){
                    $scope.cmsSubjects[index] = result;
                });
            }

            HrDesignationSetupByType.query({type: 'Non_Govt_Employee',  size: 200},function(result){
                $scope.designationSetups = result;
            });
            $scope.cmscurriculums = FindActivecmsCurriculums.query({page: 0, size: 200});

            $scope.setTrades = function (curriculum) {
                GetCmsSubjectByGeneralStatus.query({curriculumnId:curriculum.id,status:true},function(result){
                    $scope.cmsSubjectss = result;
                });
                MpoCmsTradeByCurriculumId.query({id: curriculum.id}, function (result) {
                    $scope.cmstrades = result;
                });
            };

            //this to edit mpo role
            if($stateParams.id != null){
                MpoVacancyRole.get({id: $stateParams.id}, function (result) {
                    $scope.mpoVacancyRole = result;
                    $scope.setTrades(result.cmsCurriculum);
                });
                MpoVacancyRoleTradeByVacancyRole.query({id :$stateParams.id}, function(result){
                    $scope.mpoVacancyTradeRoleDesignations = result;
                });
                MpoVacancyRoleDesgnationsByRole.query({id: $stateParams.id}, function (result) {
                    $scope.mpoVacancyRoleDesgnations = result;
                });
            }

            $scope.save = function () {

                if ($scope.mpoVacancyRole.id != null) {
                    MpoVacancyRole.update($scope.mpoVacancyRole, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.mpoVacancyRole.updated');
                } else {
                    MpoVacancyRole.save($scope.mpoVacancyRole, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.mpoVacancyRole.created');
                }
            };

            var onSaveSuccess = function (result) {
                angular.forEach($scope.mpoVacancyRoleDesgnations, function (data) {
                    data.mpoVacancyRole = result;
                    if(data.mpoVacancyRole.id !=null){
                        MpoVacancyRoleDesgnations.update(data);
                    }else{
                        MpoVacancyRoleDesgnations.save(data);
                    }
                });

                //adding cmsTrades to mpoVacancyRoleTrade which is checked
                angular.forEach($scope.mpoVacancyTradeRoleDesignations, function (data) {
                        if(data.id !=null){
                            $scope.mpoVacancyRoleTrade = {};
                            $scope.mpoVacancyRoleTrade.id = data.id;
                            $scope.mpoVacancyRoleTrade.mpoVacancyRole = result;
                            $scope.mpoVacancyRoleTrade.cmsTrade = data.cmsTrade;
                            $scope.mpoVacancyRoleTrade.designationSetup = data.designationSetup;
                            $scope.mpoVacancyRoleTrade.totalPost = data.totalPost;
                            $scope.mpoVacancyRoleTrade.cmsSubject = data.cmsSubject;
                            MpoVacancyRoleTrade.update($scope.mpoVacancyRoleTrade);

                        }else{
                            $scope.mpoVacancyRoleTrade = {};
                            $scope.mpoVacancyRoleTrade.mpoVacancyRole = result;
                            $scope.mpoVacancyRoleTrade.cmsTrade = data.cmsTrade;
                            $scope.mpoVacancyRoleTrade.designationSetup = data.designationSetup;
                            $scope.mpoVacancyRoleTrade.totalPost = data.totalPost;
                            $scope.mpoVacancyRoleTrade.cmsSubject = data.cmsSubject;
                            MpoVacancyRoleTrade.save($scope.mpoVacancyRoleTrade);
                        }
                });
                $state.go('mpoVacancyRole', null, {reload: true});
                $scope.$emit('stepApp:mpoVacancyRoleUpdate', result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            /*$scope.areAllCurriculumSelected = false;

             $scope.updateCurriculumSelection = function (curriculumArray, selectionValue) {
             for (var i = 0; i < curriculumArray.length; i++)
             {
             curriculumArray[i].isSelected = selectionValue;
             }
             console.log(curriculumArray)
             };
             */
            //$scope.areAllTradesSelected = false;

            //$scope.updateTradeSelection = function (tradeArray, selectionValue) {
            //    for (var i = 0; i < tradeArray.length; i++) {
            //        tradeArray[i].isSelected = selectionValue;
            //    }
            //    console.log(tradeArray)
            //};

            //$scope.checkCurriculum = function (value, checked) {
            //    var idx = $scope.checkedCurriculums.indexOf(value);
            //    if (idx >= 0 && !checked) {
            //        console.log('canceled');
            //        $scope.checkedCurriculums.splice(idx, 1);
            //    }
            //    if (idx < 0 && checked) {
            //        console.log('checked');
            //        $scope.checkedCurriculums.push(value);
            //    }
            //};

            $scope.AddMoreVacancyRoleDesgnations = function () {

                $scope.mpoVacancyRoleDesgnations.push(
                    {
                        status: null,
                        totalPost: null,
                        id: null
                    }
                );
                // Start Add this code for showing required * in add more fields
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);
                // End Add this code for showing required * in add more fields

            };
            $scope.mpoVacancyRoleDesgnations.push(
                {
                    status: null,
                    totalPost: null,
                    id: null
                }
            );

            $scope.mpoVacancyTradeRoleDesignations.push(
                {
                    status: null,
                    cmsTrade: null,
                    id: null
                }
            );

            $scope.AddMoreTradeRoleDesgnations = function () {
                $scope.mpoVacancyTradeRoleDesignations.push(
                    {
                        totalPost:null,
                        status: null,
                        cmsTrade: null,
                        id: null
                    }
                );
            };

            $scope.clear = function () {
                window.history.back();
                //$modalInstance.dismiss('cancel');
            };


        }]);


/* mpoVacancyRole-detail.controller.js */

angular.module('stepApp')
    .controller('MpoVacancyRoleDetailController', function ($scope, $rootScope, $stateParams, entity, MpoVacancyRole, MpoVacancyRoleDesgnationsByRole, MpoVacancyRoleTradeByVacancyRole) {
        $scope.mpoVacancyRole = entity;
        $scope.mpoTrades = [];
        $scope.mpoVacancyRoleDesgnations = [];



        //$scope.load = function (id) {
            MpoVacancyRole.get({id: $stateParams.id}, function(result) {
                $scope.mpoVacancyRole = result;
            });
            MpoVacancyRoleDesgnationsByRole.query({id :$stateParams.id}, function(result){
                $scope.mpoVacancyRoleDesgnations = result;
            });

            MpoVacancyRoleTradeByVacancyRole.query({id :$stateParams.id}, function(result){
                $scope.mpoTrades = result;
            });
        //};
        var unsubscribe = $rootScope.$on('stepApp:mpoVacancyRoleUpdate', function(event, result) {
            $scope.mpoVacancyRole = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });


/* mpoVacancyRole-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('MpoVacancyRoleDeleteController', function($scope, $modalInstance, entity, MpoVacancyRole) {

        $scope.mpoVacancyRole = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            MpoVacancyRole.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
