
'use strict';

angular.module('stepApp').controller('JobPlacementInfoDialogController',
    ['$scope','$filter','$rootScope','$state', '$stateParams', 'entity', 'JobPlacementInfo',
        function($scope,$filter,$rootScope,$state, $stateParams, entity, JobPlacementInfo) {

        $scope.jobPlacementInfo = entity;
        $scope.load = function(id) {
            JobPlacementInfo.get({id : id}, function(result) {
                $scope.jobPlacementInfo = result;
            });
        };
        $scope.calendar = {
            opened: {},
            dateFormat: 'dd-MM-yyyy',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };
        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:jobPlacementInfoUpdate', result);
            $scope.isSaving = false;
            $state.go('jobPlacementInfo');
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.UserName = function(){
            $scope.jobPlacementInfo.accountName = $scope.accountName;
        }

        $scope.save=function() {
            $scope.isSaving = true;

            if ($scope.jobPlacementInfo.id != null) {
                JobPlacementInfo.update($scope.jobPlacementInfo, onSaveSuccess, onSaveError);
                $rootScope.setSuccessMessage('stepApp.jobPlacementInfo.updated');
            } else {
                JobPlacementInfo.save($scope.jobPlacementInfo, onSaveSuccess, onSaveError);
                $rootScope.setSuccessMessage('stepApp.jobPlacementInfo.created');
            }
        };
        $scope.clear = function() {
            $state.go('jobPlacementInfo');
        };

        $scope.tillNow = function(fromDate){
            var date = $filter('date')(new Date(fromDate), 'dd-MM-yyyy');
            $scope.jobPlacementInfo.durationExp = date+ '--Till now';
            }
       $scope.duration=function (firstDate,secondDate) {
           var date = $filter('date')(new Date(firstDate), 'yyyy/MM/dd');
           if(secondDate==null){
                $scope.jobPlacementInfo.workTo=null;
                var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                var year = new Date().getFullYear();
                var month = new Date().getMonth() + 1;
                var day = new Date().getDate();
                var time=new Date().getTime();
            }else{
               today =$filter('date')(new Date(secondDate), 'yyyy/MM/dd');
                var year = new Date(secondDate).getFullYear();
                var month = new Date(secondDate).getMonth() + 1;
                var day = new Date(secondDate).getDate();
                var time=  new Date(secondDate).getTime()
           }

            date = date.split('/');
            today=today.split('/');
            var yy = parseInt(date[0]);
            var mm = parseInt(date[1]);
            var dd = parseInt(date[2]);
            var years, months, days;
            // months
            months = month - mm;
            if (day < dd) {
                months = months - 1;
            }
            // years
            years = year - yy;
            if (month * 100 + day < mm * 100 + dd) {
                years = years - 1;
                months = months + 12;
            }
            // days
           $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
           $scope.dura=years+" Years, "+months+" Months, "+$scope.days+" Days"
           $scope.jobPlacementInfo.durationExp=$scope.dura;

        }

}]);

/*-------------------JobPlacementInfoController-------------------------*/

angular.module('stepApp')
    .controller('JobPlacementInfoController',
    ['$scope', '$state', 'Principal', '$modal', 'JobPlacementInfo', 'JobPlacementInfoSearch', 'ParseLinks', 'JobPlacementStudentInfo',
        function ($scope, $state, Principal, $modal, JobPlacementInfo, JobPlacementInfoSearch, ParseLinks ,JobPlacementStudentInfo) {

            $scope.jobPlacementInfos = [];
            $scope.JobPlacementStudentInfo = [];
            $scope.page = 0;
            $scope.loadAll = function() {
                if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                    JobPlacementInfo.query({page: $scope.page, size: 50000000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.jobPlacementInfos = result;
                    });
                }else
                if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_GOVT_STUDENT'])) {
                    JobPlacementStudentInfo.query({page: $scope.page, size: 50000000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.jobPlacementInfos = result;
                    });
                }

            };
            $scope.loadPage = function(page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            $scope.search = function () {
                JobPlacementInfoSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.jobPlacementInfos = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.jobPlacementInfo = {
                    jobId: null,
                    orgName: null,
                    description: null,
                    address: null,
                    designation: null,
                    department: null,
                    responsibility: null,
                    workFrom: null,
                    workTo: null,
                    currWork: null,
                    experience: null,
                    createDate: null,
                    createBy: null,
                    updateBy: null,
                    updateDate: null,
                    id: null
                };
            };
        }]);

/*------------------------------END----------------------*/
/*-----------------------JobPlacementInfoDeleteController---------------------*/
angular.module('stepApp')
    .controller('JobPlacementInfoDeleteController',
    ['$scope', '$modalInstance', 'entity', 'JobPlacementInfo',
        function($scope, $modalInstance, entity, JobPlacementInfo) {

            $scope.jobPlacementInfo = entity;
            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
            $scope.confirmDelete = function (id) {
                JobPlacementInfo.delete({id: id},
                    function () {
                        $modalInstance.close(true);
                    });
            };

        }]);

/*-------------------------------END-------------------------*/
/*------------------------JobPlacementInfoDetailController-----------------------*/
angular.module('stepApp')
    .controller('JobPlacementInfoDetailController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'JobPlacementInfo',
        function ($scope, $rootScope, $stateParams, entity, JobPlacementInfo) {
            $scope.jobPlacementInfo = entity;
            $scope.load = function (id) {
            };

            if($stateParams.id!=null){
                JobPlacementInfo.get({id: $stateParams.id}, function(result) {
                    $scope.jobPlacementInfo = result;
                    $scope.workValue = result.workTo;
                    if($scope.workValue == null){
                        $scope.WorkTo = true;
                    }
                });
            }

            var unsubscribe = $rootScope.$on('stepApp:jobPlacementInfoUpdate', function(event, result) {
                $scope.jobPlacementInfo = result;
            });
            $scope.$on('$destroy', unsubscribe);

        }]);

/*---------------------------------END---------------------*/

/*--------------------JobPostingInfoController-----------------------------------------*/
angular.module('stepApp')
    .controller('JobPostingInfoController',
    ['$scope','$rootScope','$sce', '$state', '$modal', 'DataUtils', 'JobPostingInfo', 'JobPostingInfoSearch', 'ParseLinks','getbyCategory','CatByJobs',
        function ($scope,$rootScope,$sce, $state, $modal, DataUtils, JobPostingInfo, JobPostingInfoSearch, ParseLinks,getbyCategory,CatByJobs) {

            $scope.jobPostingInfos = [];
            $scope.getCatbyCategory=[];
            $scope.getbyJobs =[];
            $scope.page = 0;
            $scope.loadAll = function() {
                JobPostingInfo.query({page: $scope.page, size: 1000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.jobPostingInfos = result;
                });
                getbyCategory.query({page: $scope.page, size:10},function(result, headers) {
                    $scope.cats = result;
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.getCatbyCategory = result;
                });

            };
            $scope.loadPage = function(page) {
                $scope.page = page;
                $scope.loadAll();
            };

            $scope.loadAll();

            $scope.previewCategory = function ()
            {
                getbyCategory.query({}, function(result){
                    $rootScope.showCategoryModal(result);
                });
            };

            $rootScope.showCategoryModal = function(result)
            {
                $modal.open({
                    templateUrl: 'scripts/app/entities/jobPostingInfo/category.html',
                    controller: [
                        '$scope', '$modalInstance', function($scope, $modalInstance) {
                            $scope.cats = result;
                            $scope.ok = function() {
                                $rootScope.viewerObject = {};
                                $modalInstance.close();
                            };
                            $scope.closeModal = function() {
                                $rootScope.viewerObject = {};
                                $modalInstance.dismiss();
                            };
                        }
                    ]
                });
            };

            $rootScope.areAllcatsSelected = false;
            $rootScope.updateCatsSelection = function (catArray, selectionValue) {
                for (var i = 0; i < catArray.length; i++)
                {
                    catArray[i].isSelected = selectionValue;
                }
            };

            $rootScope.jobs = [];
            $rootScope.JobsShow = function(catArray) {
                for (var i = 0; i < catArray.length; i++)
                {
                    if(catArray[i].isSelected){
                        console.log(catArray[i]);
                        CatByJobs.query({catId: catArray[i].id, page: $rootScope.page, size:10},function(result, headers) {
                            $rootScope.links = ParseLinks.parse(headers('link'));
                            if(result.length >0 ){
                                for(var j= 0; j< result.length; j++){
                                    $rootScope.jobs.push(result[j]);
                                }
                            }

                        });
                    }
                }
                $state.go('jobSuggestions');
            };
            $rootScope.NoData = function() {
                $rootScope.jobs = [];
            };

            $rootScope.closeModal = function() {
                $modalInstance.dismiss();
            };

            $scope.previewImage = function (content, contentType, name)
            {
                console.log(content)
                console.log(contentType)
                console.log(name)
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content =$sce.trustAsResourceUrl( (window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                // call the modal
                $rootScope.showFilePreviewModal();
            };

            $scope.search = function () {
                JobPostingInfoSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.jobPostingInfos = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.jobPostingInfo = {
                    jobPostId: null,
                    jobTitle: null,
                    organizatinName: null,
                    jobVacancy: null,
                    salaryRange: null,
                    jobSource: null,
                    publishedDate: null,
                    applicationDateLine: null,
                    jobDescription: null,
                    jobFileName: null,
                    upload: null,
                    uploadContentType: null,
                    createDate: null,
                    createBy: null,
                    updateBy: null,
                    updateDate: null,
                    id: null
                };
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;
        }])

/*-------------------------------------------END--------------------*/
/*-------------------------JobPostingInfoDeleteController-----------------------*/
angular.module('stepApp')
    .controller('JobPostingInfoDeleteController',
    ['$scope', '$modalInstance', 'entity', 'JobPostingInfo',
        function($scope, $modalInstance, entity, JobPostingInfo) {

            $scope.jobPostingInfo = entity;
            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
            $scope.confirmDelete = function (id) {
                JobPostingInfo.delete({id: id},
                    function () {
                        $modalInstance.close(true);
                    });
            };

        }]);

/*-----------------------------------------END--------------------*/
/*---------------------JobPostingInfoDetailController------------JobType----------*/

angular.module('stepApp')
    .controller('JobPostingInfoDetailController',
    ['$scope', '$rootScope','$sce', 'DataUtils', 'entity', 'JobPostingInfo',
        function ($scope, $rootScope,$sce, DataUtils, entity, JobPostingInfo) {
            $scope.jobPostingInfo = entity;

            $scope.load = function (id) {
                JobPostingInfo.get({id: id}, function(result) {
                    $scope.jobPostingInfo = result;
                });
            };

            $scope.previewImage = function (content, contentType, name)
            {
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                // call the modal//
                $rootScope.showFilePreviewModal();
            };

            var unsubscribe = $rootScope.$on('stepApp:jobPostingInfoUpdate', function(event, result) {
                $scope.jobPostingInfo = result;
            });
            $scope.$on('$destroy', unsubscribe);

            $scope.byteSize = DataUtils.byteSize;
        }]);

/*--------------------------------------END-------------------------*/

/*-------------------JobPostingInfoDialogController------------------------*/

angular.module('stepApp').controller('JobPostingInfoDialogController',
    ['$scope','$rootScope','$state', 'DataUtils', 'entity', 'JobPostingInfo', 'JobType','PayScale',
        function($scope,$rootScope,$state,DataUtils, entity, JobPostingInfo, JobType,PayScale) {

            $scope.jobPostingInfo = entity;
            $scope.jobtypes = JobType.query();
            $scope.payScales= PayScale.query();
            $scope.gradeShow=true;
            $scope.load = function(id) {
                JobPostingInfo.get({id : id}, function(result) {
                    $scope.jobPostingInfo = result;
                });
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:jobPostingInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('jobPostingInfo');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.appearGrade=function(nature){
                if(nature.name == 'Govt' ){
                    $scope.gradeShow=false;
                }
                else{
                    $scope.gradeShow=true;
                }
            }

            $scope.applicationDate = function(date1,date2){
                if(moment(date1).isAfter(date2))
                {
                    alert('Publication Date will be greater than Application dateline!')
                    $scope.jobPostingInfo.applicationDateLine=null;
                }
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.jobPostingInfo.id != null) {
                    JobPostingInfo.update($scope.jobPostingInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.jobPostingInfo.updated');
                } else {
                    JobPostingInfo.save($scope.jobPostingInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.jobPostingInfo.created');
                }
            };

            $scope.allowedFileTypes = '';
            $scope.updateContentType = function () {
                $scope.allowedFileTypes = '.pdf,.doc,.docx,.rtf,.png,.jpg,.gif,';

            }

            $scope.setContent = function ($file, jobPostingInfo) {
                if (($file.size / 1024) / 1024 > $scope.appId.limitMb) {
                    jobPostingInfo.uploadContentType = null;
                    jobPostingInfo.contentName = null;
                }
                else {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            jobPostingInfo.upload = base64Data;
                            jobPostingInfo.uploadContentType = $file.type;
                            jobPostingInfo.contentName = $file.name;
                        });
                    };
                }
            };

            $scope.setContentImage = function ($file, jobPostingInfo) {

                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            jobPostingInfo.upload = base64Data;
                            jobPostingInfo.uploadContentType = $file.type;
                            jobPostingInfo.jobFileName = $file.name;
                        });
                    };
                }
            };

            $scope.clear = function() {
                $state.go('jobPostingInfo');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setUpload = function ($file, jobPostingInfo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            jobPostingInfo.upload = base64Data;
                            jobPostingInfo.uploadContentType = $file.type;
                        });
                    };
                }
            };
        }]);

/*------------------------------------END-------------------------------*/
