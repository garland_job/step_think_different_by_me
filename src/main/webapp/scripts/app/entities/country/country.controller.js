'use strict';

angular.module('stepApp')
    .controller('CountryController',
    ['$scope', 'Country', 'CountrySearch', 'ParseLinks',
    function ($scope, Country, CountrySearch, ParseLinks) {
        $scope.countrys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Country.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.countrys = result;
                $scope.total = headers('x-total-count');
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Country.get({id: id}, function(result) {
                $scope.country = result;
                $('#deleteCountryConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Country.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCountryConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CountrySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.countrys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };


        //==================|
        // bulk actions		|
        //==================|
        $scope.areAllCountrysSelected = false;

        $scope.updateCountrysSelection = function (countryArray, selectionValue) {
            for (var i = 0; i < countryArray.length; i++) {
                countryArray[i].isSelected = selectionValue;
            }
        };

        $scope.sync = function () {
            for (var i = 0; i < $scope.countrys.length; i++) {
                var country = $scope.countrys[i];
                if (country.isSelected) {
                    Country.update(country);
                }
            }
        };

        $scope.clear = function () {
            $scope.country = {
                isoCode3: null,
                isoCode2: null,
                name: null,
                continent: null,
                region: null,
                surfaceArea: null,
                indepYear: null,
                population: null,
                lifeExpectancy: null,
                gnp: null,
                gnpOld: null,
                localName: null,
                governmentform: null,
                capital: null,
                headOfState: null,
                callingCode: null,
                id: null
            };
        };
    }]);

/* country-form.controller.js */

angular.module('stepApp').controller('CountryFormController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'Country',
        function($scope, $rootScope, $state, $stateParams, entity, Country) {

            $scope.country = entity;
            $scope.load = function(id) {
                Country.get({id : id}, function(result) {
                    $scope.country = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:countryUpdate', result);
                $scope.isSaving = false;
                $state.go('country');
            };

            $scope.save = function () {
                if ($scope.country.id != null) {
                    Country.update($scope.country, onSaveSuccess);
                    $rootScope.setWarningMessage('stepApp.country.updated');
                } else {
                    Country.save($scope.country, onSaveSuccess);
                    $rootScope.setSuccessMessage('stepApp.country.created');
                }
            };

        }]);

/* country-detail.controller.js */
angular.module('stepApp')
    .controller('CountryDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'Country',
            function ($scope, $rootScope, $stateParams, entity, Country) {
                $scope.country = entity;
                $scope.load = function (id) {
                    Country.get({id: id}, function(result) {
                        $scope.country = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:countryUpdate', function(event, result) {
                    $scope.country = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/* country-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('CountryDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'Country',
            function ($scope, $rootScope, $modalInstance, entity, Country) {

                $scope.country = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    Country.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.country.deleted');
                        });
                };

            }]);
