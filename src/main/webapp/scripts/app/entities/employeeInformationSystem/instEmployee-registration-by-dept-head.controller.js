'use strict';

angular.module('stepApp').controller('InstEmployeeRegistrationByDeptHeadController',
    ['$scope', '$rootScope', '$stateParams', '$state', '$q', 'DataUtils', 'entity', 'InstEmployee', 'Institute', 'MiscTypeSetupByCategory','InstGenInfosByCurrentUser', 'InstLevel', 'CmsSubAssignByTrade', 'HrDesignationSetup', 'HrEmplTypeInfo', 'HrDesignationSetupByType', 'HrDesignationSetupByTypes', 'District', 'IisCourseInfosOfCurrentInst', 'IisCourseInfo', 'IisCurriculumByInstIdForDept', 'GetCmsSubjectByGeneralStatus', 'MpoCmsTradeByCurriculumId', 'AllCmsSubjectByTrade', 'DepartmentByInstituteAndType', 'CmsTradesByCurriculum', 'CurrentInstEmployee', 'IisTradesByInstitute', 'IisCourseInfoByInstituteId', 'IisTradesOfCurrentInst',
        function ($scope, $rootScope, $stateParams, $state, $q, DataUtils, entity, InstEmployee, Institute, MiscTypeSetupByCategory,InstGenInfosByCurrentUser, InstLevel, CmsSubAssignByTrade, HrDesignationSetup, HrEmplTypeInfo, HrDesignationSetupByType, HrDesignationSetupByTypes, District, IisCourseInfosOfCurrentInst, IisCourseInfo, IisCurriculumByInstIdForDept, GetCmsSubjectByGeneralStatus, MpoCmsTradeByCurriculumId, AllCmsSubjectByTrade, DepartmentByInstituteAndType, CmsTradesByCurriculum, CurrentInstEmployee, IisTradesByInstitute, IisCourseInfoByInstituteId, IisTradesOfCurrentInst) {

            $scope.cmsSubAssigns = [];
            $scope.instEmployee = {};
            $scope.courseList = [];
            $scope.subjectActive = true;
            $scope.districtsOwn = District.query({size: 200});
            $scope.hrDepartmentList = [];
            $scope.encadrementList      = MiscTypeSetupByCategory.get({cat:'Encadrement',stat:'true'});
            $scope.dateError = false;

            CurrentInstEmployee.get({}, function (result) {
                $scope.currentEmployee = result;
                IisCurriculumByInstIdForDept.query({
                    instituteId: $scope.currentEmployee.institute.id,
                    page: 0,
                    size: 2000
                }, function (result1) {
                    $scope.cmscurriculums = result1;
                });
                $scope.levelId = $scope.currentEmployee.institute.instLevel.id;
                $scope.levelName = '';
                $scope.instLevels = [];
                InstLevel.query({size: 200}, function (allInst) {
                    angular.forEach(allInst, function (value, key) {
                        if (parseInt($scope.levelId) == parseInt(value.id)) {
                            $scope.levelName = value.name;
                        }
                    });
                    var splitNames = $scope.levelName.split('&');
                    angular.forEach(allInst, function (value, key) {
                        angular.forEach(splitNames, function (spname, spkey) {
                            if (spname.trim() == value.name.trim()) {
                                $scope.instLevels.push(value);
                            }
                        });
                    });
                });
                IisCourseInfoByInstituteId.query({instituteId: $scope.currentEmployee.institute.id}, function (result) {
                    $scope.courseList = result;
                });
                HrDesignationSetupByTypes.query({size: 200}, function (result) {
                    $scope.designationSetupsss = result;
                });
                HrEmplTypeInfo.query({}, function (result) {
                    $scope.employeeTypes = result;
                });
            });
            $scope.sec = false;
            $scope.dept = true;
            $scope.hrDepartmentList = [];

            $scope.pgsType = function (stype) {
                $scope.getDeptList(stype);
                if (stype == 'Department') {
                    $scope.dept = true;
                    $scope.sec = false;
                } else if (stype == 'Section') {
                    $scope.sec = true;
                    $scope.dept = false;
                } else {
                    $scope.sec = false;
                    $scope.dept = false;
                }
            };
            $scope.getDeptList = function (setupType) {
                DepartmentByInstituteAndType.query({setupType: setupType}, function (result) {
                    $scope.hrDepartmentList = result;
                });
            }

            $scope.isRequired = false;
            $scope.updateFormFields = function () {
                if ($scope.instEmployee.category == 'Teacher') {
                    $scope.isVacancy = true;
                    $scope.staffVacancy = false;
                    $scope.isRequired = true;
                    //if ($('#label-level').html().indexOf('*') == -1)
                    //    $('#label-level').append('<strong style="color:red"> * </strong>');
                    //if ($('#label-courseTech').html().indexOf('*') == -1)
                    //    $('#label-courseTech').append('<strong style="color:red"> * </strong>');
                    //if ($('#label-subject').html().indexOf('*') == -1)
                    //    $('#label-subject').append('<strong style="color:red"> * </strong>');
                    //if ($('#label-email').html().indexOf('*') == -1)
                    //    $('#label-email').append('<strong style="color:red"> * </strong>');
                    //if ($('#label-contactNo').html().indexOf('*') == -1)
                    //    $('#label-contactNo').append('<strong style="color:red"> * </strong>');
                }
                else {
                    $scope.staffVacancy = true;
                    $scope.isVacancy = false;
                    $scope.isRequired = false;
                    $('#label-level').html($('#label-level').html().replace("*", ""));
                    $('#label-courseTech').html($('#label-courseTech').html().replace("*", ""));
                    $('#label-subject').html($('#label-subject').html().replace("*", ""));
                    $('#label-email').html($('#label-email').html().replace("*", ""));
                    $('#label-contactNo').html($('#label-contactNo').html().replace("*", ""));
                }
            };
            $scope.tradeChange = function (curriculumId) {
                CmsTradesByCurriculum.query({id: curriculumId}, function (result) {
                    $scope.cmsTrades = result;
                });
            };
            //$scope.courseSubsGlobal = CourseSub.query();
            //$scope.coursetechs = CourseTech.query();
            $scope.coursesubs = [];
            $scope.selectedTech = "Select a Technology";
            $scope.SelectedSubs = "Select a Subject";

            //$scope.CourseTechChange = function (data) {
            //    $scope.coursesubs = [];
            //    angular.forEach($scope.courseSubsGlobal, function (subjectData) {
            //        if (data.id == subjectData.courseTech.id) {
            //            $scope.coursesubs.push(subjectData);
            //        }
            //    });
            //};
            $scope.selectedCourse = {};
            $scope.CourseTradeChange = function (data) {
                CmsSubAssignByTrade.query({id: data}, function (result) {
                    $scope.subjectActive = false;
                    $scope.cmsSubAssigns = result;
                });
            };


            var onSaveSuccess = function (result) {
                $scope.instEmployee.applyDate = new Date();
                $scope.$emit('stepApp:instEmployeeUpdate', result);
                $scope.isSaving = false;
                IisCourseInfo.get({id: result.iisCourseInfo.id}, function (result3) {
                    $scope.iisCourseInfo = result3;
                    if (result.category == 'Teacher' && $scope.iisCourseInfo.noOfPostT != null) {
                        $scope.iisCourseInfo.noOfPostT = parseInt($scope.iisCourseInfo.noOfPostT) - 1;
                    }
                    else if (result.category == 'Staff' && $scope.iisCourseInfo.noOfPostStaff != null) {
                        $scope.iisCourseInfo.noOfPostStaff = parseInt($scope.iisCourseInfo.noOfPostStaff) - 1;
                    }
                    IisCourseInfo.update($scope.iisCourseInfo);
                });

                if (result.category === 'Staff') {
                    $state.go('instituteInfo.staffList', {}, {reload: true});
                    $rootScope.setSuccessMessage('Your request is sent to the Principal. Please wait for your confirmation mail. ');
                } else {
                    $state.go('instituteInfo.employeeList', {}, {reload: true});
                    $rootScope.setSuccessMessage('Your request is sent to the Principal. Please wait for your confirmation mail. ');
                }

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {type: contentType});
                return blob;
            }


            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setQuotaCert = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.quotaCert = base64Data;
                            instEmployee.quotaCertContentType = $file.type;
                            instEmployee.quotaCertName = $file.name;
                            var blob = b64toBlob(instEmployee.quotaCert, instEmployee.quotaCertContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                            $scope.picNotAdd = false;
                        });
                    };
                }

            };


            $scope.setImage = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.image = base64Data;
                            instEmployee.imageContentType = $file.type;
                            instEmployee.imageName = $file.name;
                            var blob = b64toBlob(instEmployee.image, instEmployee.imageContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };


            $scope.save = function () {
                $scope.isSaving = true;
                $scope.instEmployee.status = 2;
                $scope.instEmployee.level = $scope.instEmployee.instLevel.name;
                if (typeof $scope.instEmployee.isJPAdmin == "undefined" || $scope.instEmployee.isJPAdmin == null) {
                    $scope.instEmployee.isJPAdmin = false;
                }
                $scope.instEmployee.institute = $scope.logInInstitute;
                $scope.instEmployee.rollNo = 'NO';
                InstEmployee.save($scope.instEmployee, onSaveSuccess, onSaveError);
                $rootScope.setSuccessMessage('stepApp.instEmployee.created');
            };

            $scope.clear = function () {
                $scope.instEmployee = null;
            };

            $scope.testVal = function (test) {
                $scope.instEmployee.isHeadOfDept = test;
            }

            $scope.setTrades = function (curriculum) {


                GetCmsSubjectByGeneralStatus.query({curriculumnId: curriculum.id, status: true}, function (result) {
                    $scope.cmsSubjects = result;
                });
                IisTradesOfCurrentInst.query({curId: curriculum.id}, function (result) {
                    $scope.cmsTrades = result;

                });
            };

            $scope.setCmsSubject = function (tradeId) {
                $scope.CourseTradeChange(tradeId);
                AllCmsSubjectByTrade.get({tradeId: tradeId}, function (result) {
                    $scope.cmsSubjects = result;
                });

                angular.forEach($scope.courseList, function (courseInfo) {
                    if (courseInfo.cmsTrade.id != null) {
                        if (courseInfo.cmsTrade.id == tradeId) {
                            $scope.selectedCourse = courseInfo;
                            $scope.instEmployee.iisCourseInfo = $scope.selectedCourse;
                            if ($scope.selectedCourse != null) {
                                if ($scope.instEmployee.category == 'Teacher' && $scope.selectedCourse.noOfPostT != '0') {
                                    $scope.isVacancy = false;

                                }
                                else if ($scope.instEmployee.category == 'Staff' && $scope.selectedCourse.noOfPostStaff != '0') {
                                    $scope.staffVacancy = false;

                                }

                            }
                        }
                    }
                });
            };

            $scope.updatePrlDate = function()
            {
                if($scope.instEmployee.dob && $scope.instEmployee.jobQuota)
                {
                    var prlYear = 59;
                    if ($scope.instEmployee.jobQuota != 'General'){
                        prlYear = 61;
                    }
                    var brDt = new Date($scope.instEmployee.dob);
                    brDt.setDate(brDt.getDate() - 1);
                    var prlDt = new Date(brDt.getFullYear() + prlYear, brDt.getMonth(), brDt.getDate());
                    var retirementDt = new Date(brDt.getFullYear() + prlYear + 1, brDt.getMonth(), brDt.getDate());
                    $scope.instEmployee.prlDate = prlDt;
                    $scope.instEmployee.retirementDate = retirementDt;
                }else {
                    console.log("BirthDate and Quota is needed for PRL Calculation");
                }
            };
            $scope.toDate = new Date();
            $scope.dateValidation = function () {
                if($scope.instEmployee.dob != null &&  $scope.instEmployee.joiningDate !=null){
                    var d1 = Date.parse($scope.instEmployee.dob);
                    var d2 = Date.parse($scope.instEmployee.joiningDate);
                    if (d1 >= d2) {
                        $scope.dateError = true;
                    } else {
                        $scope.dateError = false;
                    }
                }else if($scope.instEmployee.dob != null &&  $scope.instEmployee.joiningDate ==null){
                    $scope.dateError = false;
                }else{
                    $scope.dateError = true;
                }
            };

        }]);
