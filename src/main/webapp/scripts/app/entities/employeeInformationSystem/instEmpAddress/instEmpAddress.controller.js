'use strict';

angular.module('stepApp')
    .controller('InstEmpAddressController',
    ['$scope', '$state', '$modal', 'InstEmpAddress', 'InstEmpAddressSearch', 'ParseLinks',
     function ($scope, $state, $modal, InstEmpAddress, InstEmpAddressSearch, ParseLinks) {

        $scope.instEmpAddresss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            InstEmpAddress.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.instEmpAddresss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            InstEmpAddressSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.instEmpAddresss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instEmpAddress = {
                address: null,
                status: null,
                id: null
            };
        };
    }]);

/*instEmpAddress-dialog.controller.js*/
angular.module('stepApp').controller('InstEmpAddressDialogController',
    ['$scope','$rootScope', '$stateParams', 'entity', 'InstEmpAddress', 'Upazila', 'InstEmployee',
        function($scope,$rootScope, $stateParams, entity, InstEmpAddress, Upazila, InstEmployee) {

            $scope.instEmpAddress = entity;
            $scope.upazilas = Upazila.query();
            $scope.instemployees = InstEmployee.query();
            $scope.load = function(id) {
                InstEmpAddress.get({id : id}, function(result) {
                    $scope.instEmpAddress = result;
                });
            };
            CurrentInstEmployee.get({},function(res){
                console.log(res.mpoAppStatus);
                if(res.mpoAppStatus > 1){
                    console.log("Eligible to apply");
                    $rootScope.setErrorMessage('You have applied for mpo.So you are not allowed to edit');
                    $state.go('employeeInfo.personalInfo');
                }
            });
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmpAddressUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instEmpAddress.id != null) {
                    InstEmpAddress.update($scope.instEmpAddress, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmpAddress.updated');
                } else {
                    InstEmpAddress.save($scope.instEmpAddress, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmpAddress.created');
                }
            };

            $scope.clear = function() {
                scope.InstEmpAddress = null;
            };
        }]);

/* instEmpAddress-detail.controller.js */
angular.module('stepApp')
    .controller('InstEmpAddressDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstEmpAddress', 'Upazila', 'InstEmployee',
            function ($scope, $rootScope, $stateParams, entity, InstEmpAddress, Upazila, InstEmployee) {
                $scope.instEmpAddress = entity;
                $scope.load = function (id) {
                    InstEmpAddress.get({id: id}, function(result) {
                        $scope.instEmpAddress = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instEmpAddressUpdate', function(event, result) {
                    $scope.instEmpAddress = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*instEmpAddress-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('InstEmpAddressDeleteController',
        ['$scope', '$rootScope' , '$modalInstance', 'entity', 'InstEmpAddress',
            function($scope, $rootScope , $modalInstance, entity, InstEmpAddress) {

                $scope.instEmpAddress = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmpAddress.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setSuccessMessage('stepApp.instEmpAddress.deleted');
                        });
                };

            }]);
