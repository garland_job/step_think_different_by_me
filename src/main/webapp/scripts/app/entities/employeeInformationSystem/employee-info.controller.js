'use strict';

angular.module('stepApp')
    .controller('EmployeeInfoController',
        ['$scope', '$state', '$modal', 'DataUtils', 'Principal', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks', 'CurrentInstEmployee', 'InstituteByLogin', 'InstEmployeeActiveInactive',
            function ($scope, $state, $modal, DataUtils, Principal, InstEmployee, InstEmployeeSearch, ParseLinks, CurrentInstEmployee, InstituteByLogin, InstEmployeeActiveInactive) {

                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;


                if (Principal.hasAnyAuthority(['ROLE_INSTEMP'])) {
                    CurrentInstEmployee.get({}, function (result) {
                        $scope.CurrentInstEmployee = result;

                    });
                }

                InstEmployeeActiveInactive.query({}, function (result) {
                    $scope.inActiveInstitute = result;

                });

                if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                    InstituteByLogin.query({}, function (result) {
                        $scope.logInInstitute = result;

                    });
                }

            }]).controller('InstEmployeeMpoEnlistedListController',
    ['$scope', '$location', '$state', '$modal', 'DataUtils', 'Principal', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks', 'CurrentInstEmployee', 'InstituteByLogin', 'AllMpoEnlistedEmployeeOfCurrent',
        function ($scope, $location, $state, $modal, DataUtils, Principal, InstEmployee, InstEmployeeSearch, ParseLinks, CurrentInstEmployee, InstituteByLogin, AllMpoEnlistedEmployeeOfCurrent) {
            $scope.abbreviate = DataUtils.abbreviate;
            $scope.byteSize = DataUtils.byteSize;

            if ($location.path() == '/employee-info/mpo-enlisted-employee-list') {
                $scope.showHeadingmpo = '/employee-info/mpo-enlisted-employee-list';
            }
            else {
                $scope.showHeadingmpo = null;
            }

            if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                AllMpoEnlistedEmployeeOfCurrent.query({}, function (result) {
                    $scope.instEmployees = result;

                });
            }

        }]);

/* employee-info-dashboard.controller.js */
angular.module('stepApp')
    .controller('EmployeeDashboardController',
        ['$scope', '$state', 'Principal', '$modal', 'DataUtils', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks', 'CurrentInstEmployee', 'InstituteByLogin',
            function ($scope, $state, Principal, $modal, DataUtils, InstEmployee, InstEmployeeSearch, ParseLinks, CurrentInstEmployee, InstituteByLogin) {

                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;
                if (Principal.hasAnyAuthority(['ROLE_INSTEMP'])) {
                    CurrentInstEmployee.get({}, function (result) {
                        $scope.CurrentInstEmployee = result;

                    });
                }

            }]);
