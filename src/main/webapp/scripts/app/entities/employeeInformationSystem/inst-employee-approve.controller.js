'use strict';

angular.module('stepApp')
    .controller('InstEmployeeInfoApproveController',
        ['$scope', '$stateParams', 'Institute', '$state', 'InstGenInfo', 'InstEmployee', 'InstEmployeeCode', 'Principal', '$rootScope', 'IisCourseInfoOfCurrentInstByTrade', 'InstVacancyCurrentInstForTeacher', 'InstVacancyCurInstForTeacher', 'InstVacancyCurrentInstForStaff', 'AttachmentByEmployeeAndName', 'EligibleForInstVacancy', 'GetInstituteById',
            function ($scope, $stateParams, Institute, $state, InstGenInfo, InstEmployee, InstEmployeeCode, Principal, $rootScope, IisCourseInfoOfCurrentInstByTrade, InstVacancyCurrentInstForTeacher, InstVacancyCurInstForTeacher, InstVacancyCurrentInstForStaff, AttachmentByEmployeeAndName, EligibleForInstVacancy, GetInstituteById) {

                $scope.hideApproveAndDecline = false;
                $scope.showEligible = false;
                Principal.identity().then(function (account) {
                    $scope.account = account;
                    $scope.hideApproveAndDecline = false;
                    $scope.showEligible = false;
                    InstEmployeeCode.get({'code': $stateParams.code}, function (result) {
                        $scope.instEmployee = result.instEmployee;
                        $scope.instEmpAddress = result.instEmpAddress;
                        $scope.instEmplBankInfo = result.instEmplBankInfo;
                        $scope.instEmplExperiences = result.instEmplExperiences;
                        if ($scope.instEmplExperiences == null || $scope.instEmplExperiences.length < 1) {
                            $scope.instEmplExperiences = [{}];
                        }
                        if ($scope.instEmployee.imageName != null) {
                            var blob = $rootScope.b64toBlob($scope.instEmployee.image, $scope.instEmployee.imageContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                        }
                        if ($scope.instEmployee.status > 0) {
                            $scope.hideApproveAndDecline = true;
                        }
                        AttachmentByEmployeeAndName.query({
                            id: $scope.instEmployee.id,
                            applicationName: "Employee-Personal-Info"
                        }, function (result1) {
                            $scope.attachments = result1;
                        });
                        //checking 1.institute mpoEnlisted 2.employee status is 2  3.employee mpoApplicatonStatus less than 2
                        if ($scope.instEmployee.institute.mpoEnlisted && $scope.instEmployee.status === 2 && $scope.instEmployee.mpoAppStatus < 2) {
                          EligibleForInstVacancy.get({instEmployeeId: $scope.instEmployee.id}, function (vacancy) {
                                if (vacancy.totalVacancy > vacancy.filledUpVacancy) {
                                    $scope.showEligible = true;
                                } else {
                                    $scope.showEligible = false;
                                }
                            });
                        }

                        if ($scope.instEmployee.mpoAppStatus >= 2) {
                            $scope.showEligible = false;
                        }
                        $scope.instEmplTrainings = result.instEmplTrainings;
                        if ($scope.instEmplTrainings == null || $scope.instEmplTrainings.length < 1) {
                            $scope.instEmplTrainings = [{}];
                        }
                        $scope.instEmplRecruitInfo = result.instEmplRecruitInfo;
                        $scope.instEmpSpouseInfo = result.instEmpSpouseInfo;
                        $scope.EducationalQualifications = result.instEmpEduQualis;
                        if ($scope.EducationalQualifications == null || $scope.EducationalQualifications.length < 1) {
                            $scope.EducationalQualifications = [{}];
                        }
                    });
                    var onApproveSuccess = function () {
                        $('#appmodal').modal('hide');
                        $('#appmodalForAdmin').modal('hide');
                        $('#declinemodal').modal('hide');
                        $('#declinemodalAdmin').modal('hide');
                        $('.modal-open').css('overflow', 'scroll');
                        $(".modal-backdrop").remove();
                        $state.go('employeeInfo.detail', {code: $scope.instEmployee.code}, {reload: true});
                        $rootScope.setSuccessMessage('Your request is sent to the Admin. Please wait for your confirmation mail. ');

                    }
                    $scope.ApproveApplication = function () {
                        $('#appmodal').modal('show');
                    };
                    $scope.DeclineApplication = function () {
                        $('#declinemodal').modal('show');
                    };
                    $scope.DeclineApplicationByAdmin = function () {
                        $('#declinemodalAdmin').modal('show');
                    };
                    $scope.ApproveApplicationForAdmin = function () {
                        $('#appmodalForAdmin').modal('show');
                    };
                    $scope.confirmApprove = function (id) {
                        GetInstituteById.get({id: id}, function (result) {
                            $scope.instEmployee = result;
                            $scope.instEmployee.status = 1;
                            $scope.instEmployee.entryStatus = 'UPDATE';
                            InstEmployee.update($scope.instEmployee, onApproveSuccess);
                            $rootScope.setSuccessMessage('Successfully Approved. ');
                        });
                    };
                    $scope.setReleaseAttachment = function ($file, instEmplExperience, releaseFile, releaseFileCntType, releaseFileName) {
                        if ($file) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL($file);
                            fileReader.onload = function (e) {
                                var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                                $scope.$apply(function () {
                                    instEmplExperience[releaseFile] = base64Data;
                                    instEmplExperience[releaseFileCntType] = $file.type;
                                    instEmplExperience[releaseFileName] = $file.name;
                                });
                            };
                        }
                    };

                    $scope.previewCertificate = function (content, contentType, name) {
                        var blob = $rootScope.b64toBlob(content, contentType);
                        $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                        $rootScope.viewerObject.contentType = contentType;
                        $rootScope.viewerObject.pageTitle = name;
                        $rootScope.showFilePreviewModal();
                    };

                    $scope.previewappCertificateugc = function (content, contentType, name) {
                        var blob = $rootScope.b64toBlob(content, contentType);
                        $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                        $rootScope.viewerObject.contentType = contentType;
                        $rootScope.viewerObject.pageTitle = name;
                        $rootScope.showFilePreviewModal();
                    };

                    $scope.confirmApproveForAdmin = function (id) {
                        GetInstituteById.get({id: id}, function (result) {
                            $scope.instEmployee = result;
                            $scope.instEmployee.status = 2;
                            $scope.instEmployee.entryStatus = 'APPROVED';
                            InstEmployee.update($scope.instEmployee, onApproveSuccess);
                        });
                    };

                    $scope.declineApp = function (id, delCon) {
                        GetInstituteById.get({id: id}, function (result) {
                            $scope.instEmployee = result;
                            $scope.instEmployee.status = -2;
                            $scope.instEmployee.cosOfDell = delCon;
                            $scope.instEmployee.entryStatus = 'DECLINED';
                            InstEmployee.update($scope.instEmployee, onApproveSuccess);
                        });
                    };
                    $scope.declineAppAdmin = function (id, delCon) {
                        GetInstituteById.get({id: id}, function (result) {
                            $scope.instEmployee = result;
                            $scope.instEmployee.status = -3;
                            $scope.instEmployee.cosOfDell = delCon;
                            $scope.instEmployee.entryStatus = 'DECLINED';
                            InstEmployee.update($scope.instEmployee, onApproveSuccess);
                        });
                    };
                });
                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    $rootScope.showFilePreviewModal();
                };
            }])
    .controller('InstEmployeeDeclineController',
        ['$scope', '$stateParams', '$modalInstance', '$state', 'entity', 'InstEmployeeDecline',
            function ($scope, $stateParams, $modalInstance, $state, entity, InstEmployeeDecline) {
                $scope.decline = function () {
                };
                $scope.confirmApprove = function () {
                    InstEmployeeDecline.update({id: $stateParams.id}, $scope.causeDeny, function (result) {
                        $modalInstance.close();
                        $state.go('instituteInfo.PendingEmployeeList', {}, {reload: true});
                    });
                }
                $scope.clear = function () {
                    $modalInstance.close();
                };
            }])
    .controller('InstEmployeeConfirmApproveController',
        ['$scope', '$stateParams', '$modalInstance', '$state', 'entity', 'InstEmployeeApprove',
            function ($scope, $stateParams, $modalInstance, $state, entity, InstEmployeeApprove) {
                $scope.decline = function () {
                };
                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };
                $scope.confirmApprove = function () {
                    InstEmployeeApprove.approve({id: $stateParams.id}, {}, function () {
                        $state.go('instituteInfo.PendingEmployeeList', {}, {reload: true});
                        $modalInstance.close();
                    });
                }
                $scope.clear = function () {
                    $modalInstance.close();
                };
            }])
    .controller('InstEmployeeEligibleController',
        ['$scope', '$stateParams', '$modalInstance', '$state', 'entity', 'InstEmployeeEligible',
            function ($scope, $stateParams, $modalInstance, $state, entity, InstEmployeeEligible) {
                $scope.decline = function () {
                };
                $scope.confirmApprove = function () {
                    InstEmployeeEligible.update({id: $stateParams.id}, {}, function () {
                        $modalInstance.close();
                        $state.go('instituteInfo.employeeList', {}, {reload: true});
                    });
                }
                $scope.clear = function () {
                    $modalInstance.close();
                };
            }]);
