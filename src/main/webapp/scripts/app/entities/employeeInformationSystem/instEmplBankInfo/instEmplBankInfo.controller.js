'use strict';

angular.module('stepApp')
    .controller('InstEmplBankInfoController',
        ['$scope', '$state', '$modal', 'InstEmplBankInfo', 'InstEmplBankInfoSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstEmplBankInfo, InstEmplBankInfoSearch, ParseLinks) {

                $scope.instEmplBankInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmplBankInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmplBankInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmplBankInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmplBankInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmplBankInfo = {
                        bankName: null,
                        bankBranch: null,
                        bankAccountNo: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/* instEmplBankInfo-dialog.controller.js */

angular.module('stepApp').controller('InstEmplBankInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$modalInstance', 'entity', 'InstEmplBankInfo', 'InstEmployee',
        function ($scope, $rootScope, $stateParams, $modalInstance, entity, InstEmplBankInfo, InstEmployee) {

            $scope.instEmplBankInfo = entity;
            $scope.instemployees = InstEmployee.query();
            CurrentInstEmployee.get({}, function (res) {
                console.log(res.mpoAppStatus);
                if (res.mpoAppStatus > 1) {
                    console.log("Eligible to apply");
                    $rootScope.setErrorMessage('You have applied for mpo.So you are not allowed to edit');
                    $state.go('employeeInfo.personalInfo');
                }
            });
            $scope.load = function (id) {
                InstEmplBankInfo.get({id: id}, function (result) {
                    $scope.instEmplBankInfo = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmplBankInfoUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instEmplBankInfo.id != null) {
                    InstEmplBankInfo.update($scope.instEmplBankInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                } else {
                    InstEmplBankInfo.save($scope.instEmplBankInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                }
            };

            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);


/* instEmplBankInfo-detail.controller.js */
angular.module('stepApp')
    .controller('InstEmplBankInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'InstEmplBankInfo', 'InstEmployee',
            function ($scope, $rootScope, $stateParams, entity, InstEmplBankInfo, InstEmployee) {
                $scope.instEmplBankInfo = entity;
                $scope.load = function (id) {
                    InstEmplBankInfo.get({id: id}, function (result) {
                        $scope.instEmplBankInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instEmplBankInfoUpdate', function (event, result) {
                    $scope.instEmplBankInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/* instEmplBankInfo-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('InstEmplBankInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstEmplBankInfo',
            function ($scope, $modalInstance, entity, InstEmplBankInfo) {

                $scope.instEmplBankInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmplBankInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);
