'use strict';

angular.module('stepApp')
    .controller('InstEmplExperienceController',
        ['$scope', '$state', '$modal', 'DataUtils', 'InstEmplExperience', 'InstEmplExperienceSearch', 'ParseLinks',
            function ($scope, $state, $modal, DataUtils, InstEmplExperience, InstEmplExperienceSearch, ParseLinks) {

                $scope.instEmplExperiences = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmplExperience.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmplExperiences = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmplExperienceSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmplExperiences = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };
                $scope.setAttachment = function ($file, instEmplExperience, resignFile, resignFileCntType, resignFileName) {
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function () {
                                instEmplExperience[resignFile] = base64Data;
                                instEmplExperience[resignFileCntType] = $file.type;
                                instEmplExperience[resignFileName] = $file.name;
                            });
                        };
                    }
                };
                $scope.setReleaseAttachment = function ($file, instEmplExperience, releaseFile, releaseFileCntType, releaseFileName) {
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function () {
                                instEmplExperience[releaseFile] = base64Data;
                                instEmplExperience[releaseFileCntType] = $file.type;
                                instEmplExperience[releaseFileName] = $file.name;
                            });
                        };
                    }
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmplExperience = {
                        instituteName: null,
                        indexNo: null,
                        address: null,
                        designation: null,
                        subject: null,
                        salaryCode: null,
                        joiningDate: null,
                        mpoEnlistingDate: null,
                        resignDate: null,
                        dateOfPaymentReceived: null,
                        startDate: null,
                        endDate: null,
                        vacationFrom: null,
                        vacationTo: null,
                        totalExpFrom: null,
                        totalExpTo: null,
                        current: null,
                        attachment: null,
                        attachmentContentType: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]);


/* instEmplExperience-dialog.controller.js*/

angular.module('stepApp').controller('InstEmplExperienceDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', '$q', 'Principal', 'InstEmployeeCode', 'DataUtils', 'InstEmplExperience', 'InstEmployee', 'InstEmplExperienceCurrent', 'entity', 'CurrentInstEmployee', 'DateUtils', '$timeout',
        function ($scope, $rootScope, $stateParams, $state, $q, Principal, InstEmployeeCode, DataUtils, InstEmplExperience, InstEmployee, InstEmplExperienceCurrent, entity, CurrentInstEmployee, DateUtils, $timeout) {

            $scope.instEmplExperiences = entity;
            $scope.preConditionStatus = [];
             // $scope.instEmplExperience = [];
            /*$scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };*/
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmplExperienceUpdate', result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.isPreCondition = true;

                angular.forEach($scope.instEmplExperiences, function (data) {
                    $scope.preCondition(data);
                });
                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                console.log('#############33');
                console.log($scope.preConditionStatus);
                if($scope.isPreCondition) {
                    angular.forEach($scope.instEmplExperiences, function (data) {
                        console.log('&&&&&&&&&&&');
                        console.log(data);
                        if (data.id != null) {
                            console.log('88888888888');

                            InstEmplExperience.update(data);
                            //$rootScope.setWarningMessage('stepApp.instEmplExperience.updated');
                        } else if (data.id == null) {
                            InstEmplExperience.save(data);
                            //$rootScope.setSuccessMessage('stepApp.instEmplExperience.created');
                        }
                    });
                    $scope.isSaving = false;
                    $rootScope.setSuccessMessage('stepApp.instEmplExperience.created');
                    $state.go('employeeInfo.experienceInfo', {}, {reload: true});
                }else {
                    $rootScope.setErrorMessage( "Not Valid Data");
                }
                //$state.go('employeeInfo.experienceInfo', {}, {reload: true});
            };

            $scope.clear = function () {
            };
            $scope.AddMore = function () {
                $scope.instEmplExperiences.push(
                    {
                        instituteName: null,
                        indexNo: null,
                        address: null,
                        designation: null,
                        subject: null,
                        salaryCode: null,
                        joiningDate: null,
                        mpoEnlistingDate: null,
                        resignDate: null,
                        dateOfPaymentReceived: null,
                        startDate: null,
                        endDate: null,
                        vacationFrom: null,
                        vacationTo: null,
                        totalExpFrom: null,
                        totalExpTo: null,
                        current: null,
                        attachment: null,
                        attachmentContentType: null,
                        id: null
                    }
                );
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);
            }

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setAttachment = function ($file, instEmplExperience) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplExperience.attachment = base64Data;
                            instEmplExperience.attachmentCntType = $file.type;
                            instEmplExperience.attachmentName = $file.name;

                        });
                    };
                }
            };

            $scope.setReleaseAttachment = function ($file, instEmplExperience) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplExperience.releaseFile = base64Data;
                            instEmplExperience.releaseFileCntType = $file.type;
                            instEmplExperience.releaseFileName = $file.name;

                        });
                    };
                }
            };

            $scope.setExperienceAttachment = function ($file, instEmplExperience) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            console.log('^^^^^^^^^^^^^^^^^^^^^666');
                            instEmplExperience.attachment = base64Data;
                            instEmplExperience.attachmentCntType = $file.type;
                            instEmplExperience.attachmentName = $file.name;

                        });
                        console.log($file.type);
                        console.log(instEmplExperience.attachmentCntType);
                    };
                }
            };

            $scope.setResignAttachment = function ($file, instEmplExperience) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplExperience.resignFile = base64Data;
                            instEmplExperience.resignFileCntType = $file.type;
                            instEmplExperience.resignFileName = $file.name;

                        });
                    };
                }
            };

            $scope.deadlineValidation1 = function (instEmplExperience) {
                var d1 = Date.parse(instEmplExperience.joiningDate);
                var d2 = Date.parse(instEmplExperience.resignDate);


                if (d2 <= d1) {
                    $scope.dateError1 = true;
                }
                else {
                    $scope.dateError1 = false;
                }
            };
            var today = new Date();
            $scope.toDay = today;
            $scope.vacationDateValidation = function (instEmplExperience) {
                var d1 = Date.parse(instEmplExperience.vacationFrom);
                var d2 = Date.parse(instEmplExperience.vacationTo);
                if (d2 <= d1) {
                    $scope.vacationError1 = true;
                }
                else {
                    $scope.vacationError1 = false;
                }
            };

            $scope.preCondition = function(instEmplExperience){
                //$scope.isPreCondition = true;

                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.instituteName', instEmplExperience.instituteName, 'text', 150, 4, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.designation', instEmplExperience.designation, 'text', 60, 4, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.subject', instEmplExperience.subject, 'text', 60, 4, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.indexNo', instEmplExperience.indexNo, 'number', 25, 1, '0to9', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.department', instEmplExperience.department, 'text', 60, 2, 'atozAndAtoZ0-9.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.reasonOfResignation', instEmplExperience.reasonOfResignation, 'text', 150, 5, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.mpoEnlistingDate', DateUtils.convertLocaleDateToDMY(instEmplExperience.mpoEnlistingDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.address', instEmplExperience.address, 'text', 150, 5, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.joiningDate', DateUtils.convertLocaleDateToDMY(instEmplExperience.joiningDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.resignDate', DateUtils.convertLocaleDateToDMY(instEmplExperience.resignDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.dateOfPaymentReceived', DateUtils.convertLocaleDateToDMY(instEmplExperience.dateOfPaymentReceived), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.vacationFrom', DateUtils.convertLocaleDateToDMY(instEmplExperience.vacationFrom), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.vacationTo', DateUtils.convertLocaleDateToDMY(instEmplExperience.vacationTo), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.totalExpFrom', DateUtils.convertLocaleDateToDMY(instEmplExperience.totalExpFrom), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplExperience.totalExpTo', DateUtils.convertLocaleDateToDMY(instEmplExperience.totalExpTo), 'date', 60, 5, 'date', true, '',''));

                //if($scope.preConditionStatus.indexOf(false) !== -1){
                //    $scope.isPreCondition = false;
                //}else {
                //    $scope.isPreCondition = true;
                //}
                //console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                //console.log($scope.preConditionStatus);
                //return $scope.isPreCondition;
            }


        }]);

/* instEmplExperience-detail.controller.js */

angular.module('stepApp')
    .controller('InstEmplExperienceDetailController',
        ['$scope', 'Principal', 'InstEmployeeCode', '$rootScope', '$stateParams', 'DataUtils', 'InstEmplExperience', 'InstEmployee', 'InstEmplExperienceCurrent', 'CurrentInstEmployee',
            function ($scope, Principal, InstEmployeeCode, $rootScope, $stateParams, DataUtils, InstEmplExperience, InstEmployee, InstEmplExperienceCurrent, CurrentInstEmployee) {

                $scope.hideEditButton = false;
                $scope.hideAddButton = true;
                InstEmplExperienceCurrent.get({}, function (result) {
                    $scope.instEmplExperiences = result;
                    if (result.length < 1) {
                        $scope.hideAddButton = true;
                        $scope.hideEditButton = false;
                        $scope.instEmplExperiences = [{
                            instituteName: null,
                            indexNo: null,
                            address: null,
                            designation: null,
                            subject: null,
                            salaryCode: null,
                            joiningDate: null,
                            mpoEnlistingDate: null,
                            resignDate: null,
                            dateOfPaymentReceived: null,
                            startDate: null,
                            endDate: null,
                            vacationFrom: null,
                            vacationTo: null,
                            totalExpFrom: null,
                            totalExpTo: null,
                            current: null,
                            attachment: null,
                            attachmentContentType: null,
                            id: null
                        }];
                    }
                    else {
                        $scope.hideAddButton = false;
                        $scope.hideEditButton = true;
                    }

                }, function (result) {

                });
                $scope.previewCertificate = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };
                CurrentInstEmployee.get({}, function (result) {
                    if (!result.mpoAppStatus >= 3) {
                        $scope.hideEditButton = false;
                        $scope.hideAddButton = false;
                    }
                });
                var unsubscribe = $rootScope.$on('stepApp:instEmplExperienceUpdate', function (event, result) {
                    $scope.instEmplExperience = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;
            }]);


/* instEmplExperience-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('InstEmplExperienceDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'InstEmplExperience',
            function ($scope, $rootScope, $modalInstance, entity, InstEmplExperience) {

                $scope.instEmplExperience = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmplExperience.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.instEmplExperience.deleted');
                };

            }]);
