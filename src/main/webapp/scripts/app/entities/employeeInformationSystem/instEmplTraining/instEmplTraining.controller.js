'use strict';

angular.module('stepApp')
    .controller('InstEmplTrainingController',
        ['$scope', '$state', '$modal', 'InstEmplTraining', 'InstEmplTrainingSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstEmplTraining, InstEmplTrainingSearch, ParseLinks) {

                $scope.instEmplTrainings = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmplTraining.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmplTrainings = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmplTrainingSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmplTrainings = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmplTraining = {
                        name: null,
                        location: null,
                        startedDate: null,
                        endedDate: null,
                        result: null,
                        id: null
                    };
                };
            }]);


/* instEmplTraining-dialog.controller.js */

angular.module('stepApp').controller('InstEmplTrainingDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'Principal', 'InstEmployeeCode', 'InstEmplTraining', 'InstEmployee', 'entity', 'DateUtils','DataUtils', '$timeout',
        function ($scope, $rootScope, $stateParams, $state, Principal, InstEmployeeCode, InstEmplTraining, InstEmployee, entity, DateUtils, DataUtils,$timeout) {

            $scope.instEmplTrainings = [];
            $scope.preConditionStatus = [];

            if ($stateParams.instEmplTrainings == null) {
                $state.go('employeeInfo.trainingInfo', {}, {reload: true});
            } else {
                $rootScope.refreshRequiredFields();
                $scope.instEmplTrainings = $stateParams.instEmplTrainings;
            }

            /*$scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };*/
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmplTrainingUpdate', result);
                $scope.isSaving = false;
                $state.go('employeeInfo.trainingInfo', {}, {reload: true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.deadlineValidation = function (startedDate, endedDate) {
                if (startedDate != null && endedDate != null) {
                    var d1 = Date.parse(startedDate);
                    var d2 = Date.parse(endedDate);
                    if (d1 <= d2) {
                        $scope.dateError = true;
                    } else {
                        $scope.dateError = false;
                    }
                }

            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.isPreCondition = true;
                angular.forEach($scope.instEmplTrainings, function (data) {
                     $scope.preCondition(data);
                });

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                if($scope.isPreCondition){
                    angular.forEach($scope.instEmplTrainings, function (data) {

                        if (data.id != null) {
                            InstEmplTraining.update(data);
                            //$rootScope.setWarningMessage('stepApp.instEmplTraining.updated');
                        }else if (data.id == null){
                            if (data.name != null && data.location != null) {
                                console.log('))))))))))))))))))))))))))');
                                InstEmplTraining.save(data);
                            }else{
                                $rootScope.setErrorMessage( "Course Name & Location can't null");
                            }
                        }
                    });
                    $scope.isSaving = false;
                    $rootScope.setSuccessMessage('stepApp.instEmplTraining.created');
                    $state.go('employeeInfo.trainingInfo', {}, {reload: true});
                }else {
                    $rootScope.setErrorMessage( "Not Valid Data");
                }
            };

            $scope.AddMore = function () {
                $scope.instEmplTrainings.push({
                    name: null,
                    location: null,
                    startedDate: null,
                    endedDate: null,
                    result: null,
                    attachment: null,
                    attachmentContentType: null,
                    id: null
                });
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);

            }

            $scope.clear = function () {
                $state.go('employeeInfo.trainingInfo');
            };
            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setAttachment = function ($file, instEmplExperience) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplExperience.attachment = base64Data;
                            instEmplExperience.attachmentContentType = $file.type;
                            instEmplExperience.attachmentName = $file.name;
                        });
                    };
                }
            };

            $scope.preCondition = function(instEmplTraining){
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.name', instEmplTraining.name, 'text', 200, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.courseDuration',instEmplTraining.courseDuration, 'text', 200, 2, '0to9AndatozAndAtoZ', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.result',instEmplTraining.result, 'text', 200, 2, '0to9AndatozAndAtoZ.', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.rollOrReg',instEmplTraining.rollOrReg, 'number', 10, 1, '0to9', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.ofcOrderNo',instEmplTraining.ofcOrderNo, 'number', 10, 1, '0to9', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.startedDate', DateUtils.convertLocaleDateToDMY(instEmplTraining.startedDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplTraining.endedDate', DateUtils.convertLocaleDateToDMY(instEmplTraining.endedDate), 'date', 5, 60, 'date', true, '',''));

            }

        }]);


/*instEmplTraining-detail.controller.js */

angular.module('stepApp')
    .controller('InstEmplTrainingDetailController',
        ['$scope', 'Principal', 'InstEmployeeCode', '$rootScope', '$stateParams', 'InstEmplTraining', 'InstEmployee', 'InstEmplTrainingCurrent', 'CurrentInstEmployee',
            function ($scope, Principal, InstEmployeeCode, $rootScope, $stateParams, InstEmplTraining, InstEmployee, InstEmplTrainingCurrent, CurrentInstEmployee) {


                $scope.instEmplTrainings = [];
                $scope.hideEditButton = false;
                $scope.hideAddButton = true;
                InstEmplTrainingCurrent.get({}, function (result) {
                    $scope.instEmplTrainings = result;
                    if (result.length < 1) {
                        $scope.hideAddButton = true;
                        $scope.hideEditButton = false;
                        $scope.instEmplTrainings = [{
                            name: null,
                            location: null,
                            startedDate: null,
                            endedDate: null,
                            result: null,
                            id: null
                        }];
                        CurrentInstEmployee.get({}, function (result) {
                            if (result.status <= 2 ) {
                                $scope.hideEditbutton = false;
                                $scope.hideAddButton = true;
                            } else {
                                $scope.hideAddButton = false;
                                $scope.hideEditButton = false;
                            }
                            if (result.mpoAppStatus >= 3 &&  result.mpoAppStatus < 5) {
                                $scope.hideEditButton = false;
                                $scope.hideAddButton = false;
                            }
                        });
                    }
                    else {
                        $scope.hideAddButton = false;
                        $scope.hideEditButton = true;
                        if ($scope.instEmplTrainings[0].instEmployee.status < 2) {
                            $scope.hideEditbutton = true;
                        } else {
                            $scope.hideEditbutton = false;
                        }
                        if ($scope.instEmplTrainings[0].instEmployee.mpoAppStatus >= 3 &&  result.mpoAppStatus < 5) {
                            $scope.hideEditButton = false;
                            $scope.hideAddButton = false;
                        }
                    }

                });


                var unsubscribe = $rootScope.$on('stepApp:instEmplTrainingUpdate', function (event, result) {
                    $scope.instEmplTraining = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.previewCertificate = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = "National Id Image of " + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

            }]);


/*  instEmplTraining-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('InstEmplTrainingDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstEmplTraining',
            function ($scope, $modalInstance, entity, InstEmplTraining) {

                $scope.instEmplTraining = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmplTraining.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.instEmplTraining.deleted');

                };

            }]);
