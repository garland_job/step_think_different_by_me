'use strict';

angular.module('stepApp')
    .controller('InstEmpSpouseInfoController',
        ['$scope', '$state', '$modal', 'InstEmpSpouseInfo', 'InstEmpSpouseInfoSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstEmpSpouseInfo, InstEmpSpouseInfoSearch, ParseLinks) {

                $scope.instEmpSpouseInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmpSpouseInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmpSpouseInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmpSpouseInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmpSpouseInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmpSpouseInfo = {
                        name: null,
                        dob: null,
                        isNominee: false,
                        gender: null,
                        relation: null,
                        nomineePercentage: null,
                        occupation: null,
                        tin: null,
                        nid: null,
                        designation: null,
                        govJobId: null,
                        mobile: null,
                        officeContact: null,
                        id: null
                    };
                };
            }]);

/* instEmpSpouseInfo-dialog.controller.js */

angular.module('stepApp').controller('InstEmpSpouseInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$q', 'InstEmpSpouseInfoForCurrent', 'InstEmpSpouseInfo', 'CurrentInstEmployee', 'InstEmployee', 'InstEmpAddress', 'entity', '$state', 'InstEmployeeByInstitute','DateUtils',
        function ($scope, $rootScope, $stateParams, $q, InstEmpSpouseInfoForCurrent, InstEmpSpouseInfo, CurrentInstEmployee, InstEmployee, InstEmpAddress, entity, $state, InstEmployeeByInstitute,DateUtils) {

            $scope.instEmpSpouseInfo = entity;

            CurrentInstEmployee.get({}, function (result) {
                InstEmployeeByInstitute.query({instituteID: result.institute.id}, function (result1) {
                    $scope.instituteList = result1;
                    console.log("===========");
                    console.log($scope.instituteList);
                    console.log("------------------");
                });
                if (!result.mpoAppStatus >= 3) {
                    $scope.hideEditButton = false;
                    $scope.hideAddButton = false;
                }
            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmpSpouseInfoUpdate', result);
                $scope.isSaving = false;
                $state.go('employeeInfo.spouseInfo', {}, {reload: true});
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            $scope.save = function () {
                console.log($scope.instEmpSpouseInfo);
                console.log($scope.instEmpSpouseInfo.nomineeHas);
                $scope.isSaving = true;
                if ($scope.instEmpSpouseInfo.id != null && $scope.preCondition()) {
                    InstEmpSpouseInfo.update($scope.instEmpSpouseInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.instEmpSpouseInfo.updated');
                } else if ($scope.instEmpSpouseInfo.id == null && $scope.preCondition()){
                    InstEmpSpouseInfo.save($scope.instEmpSpouseInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmpSpouseInfo.created');
                }
            };
            $scope.valgender = function () {
            };

            $scope.toDay = new Date();

            $scope.preCondition = function() {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.name', $scope.instEmpSpouseInfo.name, 'text', 150, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.dob', DateUtils.convertLocaleDateToDMY($scope.instEmpSpouseInfo.dob), 'date', 60, 5, 'date', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.occupation', $scope.instEmpSpouseInfo.occupation, 'text', 150, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.tin', $scope.instEmpSpouseInfo.tin, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.nid', $scope.instEmpSpouseInfo.nid, 'number', 17, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.designation', $scope.instEmpSpouseInfo.designation, 'text', 150, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.govJobId', $scope.instEmpSpouseInfo.govJobId, 'number', 17, 3, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.mobile', $scope.instEmpSpouseInfo.mobile, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpSpouseInfo.officeContact', $scope.instEmpSpouseInfo.officeContact, 'number', 11, 11, '0to9', true, '', ''));
                console.log($scope.preConditionStatus);
                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            };
        }]);

/* instEmpSpouseInfo-detail.controller.js */
angular.module('stepApp')
    .controller('InstEmpSpouseInfoDetailController',
        ['$scope', '$rootScope', 'InstEmpSpouseInfoForCurrent', '$stateParams', 'InstEmpSpouseInfo', 'InstEmployee', 'InstEmpAddress', 'CurrentInstEmployee', 'InstEmployeeByInstitute',
            function ($scope, $rootScope, InstEmpSpouseInfoForCurrent, $stateParams, InstEmpSpouseInfo, InstEmployee, InstEmpAddress, CurrentInstEmployee, InstEmployeeByInstitute) {


                $scope.hideEditButton = false;
                $scope.hideAddButton = true;
                InstEmpSpouseInfoForCurrent.get({}, function (result) {
                    $scope.instEmpSpouseInfo = result;
                    if (result.length < 1) {
                        $scope.hideAddButton = true;
                        $scope.hideEditButton = false;
                        $scope.instEmpSpouseInfo = {
                            name: null,
                            dob: null,
                            isNominee: false,
                            gender: null,
                            relation: null,
                            nomineePercentage: null,
                            occupation: null,
                            tin: null,
                            nid: null,
                            designation: null,
                            govJobId: null,
                            mobile: null,
                            officeContact: null,
                            id: null
                        };
                    }
                    else {
                        $scope.hideAddButton = false;
                        $scope.hideEditButton = true;
                    }
                });
                CurrentInstEmployee.get({}, function (result) {

                    InstEmployeeByInstitute.query({instituteID: result.institute.id}, function (result1) {

                        $scope.instituteList = result1;

                    })
                    if (!result.mpoAppStatus >= 3) {
                        $scope.hideEditButton = false;
                        $scope.hideAddButton = false;
                    }
                });

                var unsubscribe = $rootScope.$on('stepApp:instEmpSpouseInfoUpdate', function (event, result) {
                    $scope.instEmpSpouseInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/* instEmpSpouseInfo-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('InstEmpSpouseInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstEmpSpouseInfo',
            function ($scope, $modalInstance, entity, InstEmpSpouseInfo) {

                $scope.instEmpSpouseInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmpSpouseInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.InstEmpSpouseInfo.deleted');
                };

            }]);
