'use strict';

angular.module('stepApp')
    .controller('InstEmployeeDetailController',
        ['$scope', '$rootScope', '$stateParams', 'Principal', 'DataUtils', 'entity', 'InstEmployee',  'InstEmpAddress',  'InstEmployeeCode', 'AttachmentByEmployeeAndName','CurrentInstEmployee',
            function ($scope, $rootScope, $stateParams, Principal, DataUtils, entity, InstEmployee, InstEmpAddress,InstEmployeeCode, AttachmentByEmployeeAndName,CurrentInstEmployee) {

                $scope.hideEditButton = false;
                $scope.hideAddButton = false;
                $scope.allInfo = {};
                $scope.hideRegistration = false;

                Principal.identity().then(function (account) {
                    $scope.account = account;
                    // CurrentInstEmployee
                    CurrentInstEmployee.get(function (instEmp) {
                        InstEmployeeCode.get({'code': instEmp.code}, function (result) {
                            $scope.allInfo = result;
                            $scope.instEmployee = result.instEmployee;
                            if (result.instEmployee.category == 'Teacher') {
                                $scope.hideRegistration = true;
                            }

                            if ($scope.instEmployee.imageName != null) {
                                var blob = $rootScope.b64toBlob($scope.instEmployee.image, $scope.instEmployee.imageContentType);
                                $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                                console.log("image name");
                                console.log($scope.url);
                            }

                            if ($scope.instEmployee.nidImage != null) {

                                var blob = b64toBlob($scope.instEmployee.nidImage, $scope.instEmployee.nidImageContentType);
                                $scope.nidImageNameurl = (window.URL || window.webkitURL).createObjectURL(blob);
                            }

                            AttachmentByEmployeeAndName.query({
                                id: $scope.instEmployee.id,
                                applicationName: "Employee-Personal-Info"
                            }, function (result) {
                                $scope.attachments = result;
                                console.log("Attachments");
                                console.log($scope.attachments);
                            });


                            $scope.instEmpAddress = result.instEmpAddress;
                            $scope.instEmplBankInfo = result.instEmplBankInfo;
                            //if( $scope.instEmployee.mpoAppStatus<=3 && $scope.instEmployee.status!=-1){
                            if ($scope.instEmployee.mpoAppStatus < 3 || $scope.instEmployee.mpoAppStatus == 5) {
                                if ($scope.instEmployee.status == -1) {
                                    $scope.hideAddButton = true;
                                } else {
                                    $scope.hideEditbutton = true;
                                }
                            } else {
                                $scope.hideEditbutton = false;
                            }
                        });
                    })
                });


                $scope.load = function (id) {
                    InstEmployee.get({id: id}, function (result) {
                        $scope.instEmployee = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:instEmployeeUpdate', function (event, result) {
                    $scope.instEmployee = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.byteSize = DataUtils.byteSize;


                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }

                        var byteArray = new Uint8Array(byteNumbers);

                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }

                $scope.previewNidImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = "National Id Image of " + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };
                $scope.previewBirthImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = "" + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = "" + name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

            }]);
