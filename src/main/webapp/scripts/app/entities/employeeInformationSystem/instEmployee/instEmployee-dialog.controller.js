'use strict';

angular.module('stepApp').controller('InstEmployeeDialogController',
    ['$scope', 'BankSetup', '$rootScope', '$stateParams', '$q', '$state', 'DataUtils', 'CourseSub', 'Division', 'District', 'Upazila', 'Principal', 'InstEmployeeCode', 'entity', 'InstEmployee', 'Institute', 'Religion', 'CourseTech', 'InstEmpAddress', 'InstEmpEduQuali', 'InstEmplBankInfo', 'CmsSubAssignByTrade', 'IisTradesOfCurrentInst', 'InstLevel', 'IisTradesByInstitute', 'InstFinancialInfoByInstitute', 'BankBranchsOfADistrictByBank', 'AttachmentCategoryByApplicationName', 'LoanAttachmentByEmployeeAndAppName', 'CurrentInstEmployee', 'Attachment', 'AttachmentByEmployeeAndName', 'AttachmentByEmployeeAndNameAndAttachName',
        function ($scope, BankSetup, $rootScope, $stateParams, $q, $state, DataUtils, CourseSub, Division, District, Upazila, Principal, InstEmployeeCode, entity, InstEmployee, Institute, Religion, CourseTech, InstEmpAddress, InstEmpEduQuali, InstEmplBankInfo, CmsSubAssignByTrade, IisTradesOfCurrentInst, InstLevel, IisTradesByInstitute, InstFinancialInfoByInstitute, BankBranchsOfADistrictByBank, AttachmentCategoryByApplicationName, LoanAttachmentByEmployeeAndAppName, CurrentInstEmployee, Attachment, AttachmentByEmployeeAndName, AttachmentByEmployeeAndNameAndAttachName) {

            $scope.message = '';
            $scope.content = '';
            $scope.bankSetups = BankSetup.query();
            $scope.instEmpAddress = {};
            $scope.upazila = {};
            $scope.bankBranches = [];
            $scope.instEmpAddress.upazila == null;
            $scope.instEmpAddress.roadBlockSector == null;
            $scope.instEmpAddress.villageOrHouse == null;
            $scope.applicantAttachment = [];
            $scope.applicantAttachmentCount = [0];
            $scope.attachmentCategoryList = [];
            $scope.attachments = [];
            $scope.showAddMoreButton = false;
            $scope.loanReqFormHasError = true;
            $scope.currentSelectItem = [];
            $scope.employee = [];
            $scope.errorForInstallment = false;
            $scope.errorForLoanAmount = false;
            $scope.errorApplyForLoan = false;
            $scope.invalidLoanAmount = false;
            $scope.loanReqFormEditMode = false;
            <!--Multiple attachment implementation work is under development:End>>-->
            $scope.hideRegistration = false;
            $scope.registrationField = false;

            $scope.districtsOwn = District.query({size: 200});


            $scope.cmsSubAssigns = [];

            if (!$stateParams.allInfo) {
                $state.go('employeeInfo.personalInfo', {}, {reload: true});
            } else {
                var result = $stateParams.allInfo;
                $scope.instEmployee = result.instEmployee;
                $scope.institute = result.instEmployee.institute;
                $scope.instEmployee.nationality = 'Bangladeshi';
                if ($scope.instEmployee.instCategory == 'Teacher') {
                    $scope.hideRegistration = true;
                    $scope.registrationField = true;
                    if ($scope.instEmployee.registeredCertificateSubject != undefined && $scope.instEmployee.registrationExamYear != undefined && $scope.instEmployee.registeredCertificateNo != undefined) {
                        $scope.registrationField = false;
                    }
                }
                $scope.instEmpAddress = result.instEmpAddress;
                $scope.instEmplBankInfo = result.instEmplBankInfo;
                if ($scope.instEmplBankInfo == null) {
                    $scope.instEmplBankInfo = {};
                    InstFinancialInfoByInstitute.query({instituteId: result.instEmployee.institute.id}, function (infos) {


                        $scope.instEmplBankInfo.bankSetup = infos[0].bankSetup;
                        $scope.bankName = infos[0].bankSetup.name;
                        $scope.bankId = infos[0].bankSetup.id;

                        BankBranchsOfADistrictByBank.query({
                            districtId: $scope.institute.upazila.district.id,
                            bankSetupId: $scope.bankId
                        }, function (branches) {
                            $scope.bankBranchesNew = branches;
                        });
                    });

                } else {
                    if ($scope.instEmplBankInfo.bankSetup == null) {
                        InstFinancialInfoByInstitute.query({instituteId: result.instEmployee.institute.id}, function (infos) {
                            $scope.instEmplBankInfo.bankSetup = infos[0].bankSetup;
                            $scope.bankName = infos[0].bankSetup.name;
                        });
                    }
                    $scope.bankName = $scope.instEmplBankInfo.bankSetup.name;
                    BankBranchsOfADistrictByBank.query({
                        districtId: $scope.institute.upazila.district.id,
                        bankSetupId: $scope.instEmplBankInfo.bankSetup.id
                    }, function (branches) {
                        $scope.bankBranchesNew = branches;
                    });
                }

                if ($scope.instEmployee.image != null) {
                    var blob = b64toBlob($scope.instEmployee.image, $scope.instEmployee.imageContentType);
                    $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                }
                if ($scope.instEmpAddress != null) {
                   // $scope.instEmpAddress.prevPost = $scope.instEmpAddress.post;
                    //$scope.instEmpAddress.prevPostCode = $scope.instEmpAddress.postCode;

                    $scope.parmDivsion = $scope.instEmpAddress.upazila.district.division;
                    $scope.instEmpAddress.district = $scope.instEmpAddress.upazila.district;
                    $scope.instEmpAddress.prevDistrict = $scope.instEmpAddress.prevUpazila.district;
                    $scope.instEmpAddress.prevDivision = $scope.instEmpAddress.prevUpazila.district.division;
                }
                if ($scope.instEmployee.courseSub != null) {
                    $scope.instEmployee.courseTech = $scope.instEmployee.courseSub.courseTech;
                }

                $scope.logInInstitute = result.instEmployee.institute;
                $scope.levelId = result.instEmployee.institute.instLevel.id;
                $scope.levelName = '';
                IisTradesByInstitute.query({id: result.instEmployee.institute.id}, function (result) {
                    $scope.cmsTrades = result;
                });
                $scope.instLevels = [];
                InstLevel.query(function (allInst) {
                    angular.forEach(allInst, function (value, key) {
                        if (parseInt($scope.levelId) == parseInt(value.id)) {
                            $scope.levelName = value.name;
                        }
                    });

                    var splitNames = $scope.levelName.split('&');

                    angular.forEach(allInst, function (value, key) {
                        angular.forEach(splitNames, function (spname, spkey) {
                            if (spname.trim() == value.name.trim()) {
                                $scope.instLevels.push(value);
                            }
                        });

                    });

                });
            }


            $scope.courseSubsGlobal = CourseSub.query();
            $scope.coursetechs = CourseTech.query();

            $scope.coursesubs = $scope.courseSubsGlobal;
            $scope.CourseTechChange = function (data) {
                $scope.Selected = "Select a subject";
                $scope.coursesubs = [];
                console.log(data);
                angular.forEach($scope.courseSubsGlobal, function (subjectData) {
                    if (data.id == subjectData.courseTech.id) {
                        $scope.coursesubs.push(subjectData);
                    }
                });
            }
            $scope.religions = Religion.query();


            Division.query({page: $scope.page, size: 10}, function (result) {
                $scope.divisions = result;
                $scope.prevdivisions = result;
            });
            District.query({page: $scope.page, size: 80}, function (result) {
                $scope.GlobalDistricts = result;
                $scope.districts = result;
                $scope.prevdistricts = result;

            });
            Upazila.query({page: $scope.page, size: 1000}, function (result) {
                $scope.GlobalUpazilas = result;
                $scope.upazilas = result;
                $scope.prevupazilas = result;
            });


            $scope.PreDistrictChange = function (data) {
                $scope.prevupazilas = [];
                angular.forEach($scope.GlobalUpazilas, function (gUpzilla) {
                    if (data.id == gUpzilla.district.id) {
                        $scope.prevupazilas.push(gUpzilla);
                    }
                })
                $scope.upazilaSelect = "Select a Upazila";
            }
            $scope.registrationFieldErrorFunction = function (data) {
                if ($scope.instEmployee.registeredCertificateSubject != undefined && $scope.instEmployee.registrationExamYear != undefined && $scope.instEmployee.registeredCertificateNo != undefined) {
                    $scope.registrationField = false;
                }

            };
            $scope.PreDivisionChange = function (data) {
                $scope.prevdistricts = [];
                angular.forEach($scope.GlobalDistricts, function (gDistrict) {
                    if (data.id == gDistrict.division.id) {
                        $scope.prevdistricts.push(gDistrict);
                    }
                });
                $scope.districtSelect = "Select a district";

                /* Reset pram check */

                $scope.instEmpAddress.sameAddress = false;
            };

            $scope.divisionChange = function (data) {
                $scope.districts = [];
                angular.forEach($scope.GlobalDistricts, function (gDistrict) {
                    if (data.id == gDistrict.division.id) {
                        $scope.districts.push(gDistrict);
                    }
                });
                $scope.PreDistrictSelect = "Select a district";

                /* clear fields */
                $scope.instEmpAddress.prevPost = null;
                $scope.instEmpAddress.prevPostCode = null;
                $scope.instEmpAddress.prevVillageOrHouse = null;
                $scope.instEmpAddress.prevRoadBlockSector = null;
                $scope.instEmpAddress.prevUpazila = null;
                $scope.instEmpAddress.prevDistrict = null;
                $scope.instEmpAddress.prevDivision = null;
                $scope.instEmpAddress.sameAddress = false;
            }

            $scope.districtChange = function (data) {
                $scope.upazilas = [];
                angular.forEach($scope.GlobalUpazilas, function (Districtdata) {
                    if (data.id == Districtdata.district.id) {
                        $scope.upazilas.push(Districtdata);
                    }
                });
                $scope.PreUpzilaSelect = "Select a Upazila";

                /* clear fields */
                $scope.instEmpAddress.prevPost = null;
                $scope.instEmpAddress.prevPostCode = null;
                $scope.instEmpAddress.prevVillageOrHouse = null;
                $scope.instEmpAddress.prevRoadBlockSector = null;
                $scope.instEmpAddress.prevUpazila = null;
                $scope.instEmpAddress.prevDistrict = null;
                $scope.instEmpAddress.prevDivision = null;
                $scope.instEmpAddress.sameAddress = false;
            }

            $scope.upazilaChange = function () {
                $scope.instEmpAddress.prevPost = null;
                $scope.instEmpAddress.prevPostCode = null;

                /* clear fields */
                $scope.instEmpAddress.prevVillageOrHouse = null;
                $scope.instEmpAddress.prevRoadBlockSector = null;
                $scope.instEmpAddress.prevUpazila = null;
                $scope.instEmpAddress.prevDistrict = null;
                $scope.instEmpAddress.prevDivision = null;
                $scope.instEmpAddress.sameAddress = false;
            }

            $scope.load = function (id) {
                InstEmployee.get({id: id}, function (result) {
                    $scope.instEmployee = result;
                });
            };

            $scope.bankInfoSave = function () {
                $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                if ($scope.instEmplBankInfo == null) {
                    InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                        console.log(result);
                    }, function (result) {
                        console.log(result);
                    })
                } else {
                    InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                        console.log(result);
                    }, function (result) {
                        console.log(result);
                    })
                }

            }
            <!--Multiple attachment implementation work is under development:Start>>-->


            CurrentInstEmployee.get({}, function (instEmployee) {
                $scope.employee = instEmployee;
                AttachmentCategoryByApplicationName.query({name: 'Employee-Personal-Info'}, function (result) {
                    $scope.attachmentCategoryList = result;
                });
                //AttachmentByEmployeeAndName.query({id: instEmployee.id,applicationName: "Employee-Personal-Info"}, function (result) {
                //    $scope.attachments = result;
                //    if ($scope.attachments.length > 0) {
                //        console.log("come to attachemnt null or not");
                //        $scope.applicantAttachmentCount = result;
                //        console.log($scope.applicantAttachmentCount);
                //    }
                //});
            });
            <!--Multiple attachment implementation work is under development:End>>-->

            var onSaveFinished = function (result) {
                console.log("come to save finished..........");

                $scope.instEmpAddress.instEmployee = result;
                if ($scope.instEmpAddress == null) {
                    InstEmpAddress.save($scope.instEmpAddress, function (result) {
                        $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                        if ($scope.instEmplBankInfo == null) {
                            InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                                $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            }, function (result) {
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            })
                        } else {
                            InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                                $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            }, function (result) {
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            })
                        }
                    }, function (result) {

                    });
                }
                else {
                    InstEmpAddress.update($scope.instEmpAddress, function (result) {
                        $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                        if ($scope.instEmplBankInfo == null) {
                            InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                                $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            }, function (result) {
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            })
                        } else {
                            InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                                $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            }, function (result) {
                                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                            })
                        }
                    }, function (result) {
                    });
                }
                angular.forEach($scope.applicantAttachmentCount, function (value, key) {
                    if ($scope.applicantAttachment[value].file != '') {
                        $scope.attachmentsByName = {};
                        $scope.applicantAttachment[value].instEmployee = {};
                        $scope.applicantAttachment[value].name = $scope.applicantAttachment[value].attachmentCategory.attachmentName;
                        $scope.applicantAttachment[value].instEmployee.id = $scope.employee.id;
                        AttachmentByEmployeeAndNameAndAttachName.get({
                            id: $scope.instEmployee.id,
                            applicationName: "Employee-Personal-Info",
                            attachmentName: $scope.applicantAttachment[value].name
                        }, function (result) {
                            $scope.attachmentsByName = result;

                            if (result.id != null) {
                                $scope.applicantAttachment[value].id = result.id;
                                Attachment.update($scope.applicantAttachment[value]);
                            }
                        }, function (response) {
                            if (response.status === 404) {
                                if ($scope.applicantAttachment[value].id == null) {
                                    Attachment.save($scope.applicantAttachment[value]);
                                }
                            }
                        });

                    }
                });
                $rootScope.setSuccessMessage('Save successfully');
                $state.go('employeeInfo.personalInfo', {}, {reload: true});
                <!--Multiple attachment implementation work is under development:End>>-->

            }


            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instEmployee.status = -1) {
                    $scope.instEmployee.status = 0;
                }
                if ($scope.instEmployee != null && $scope.preCondition()) {
                    //InstEmployee.update($scope.instEmployee, onSaveSuccess, onSaveError);
                    InstEmployee.update($scope.instEmployee, onSaveFinished, function (result) {
                        $scope.instEmpAddress.instEmployee = result;
                        if ($scope.instEmpAddress == null) {
                            InstEmpAddress.save($scope.instEmpAddress, function (result) {
                                /*$scope.bankInfoSave();*/
                                $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                                if ($scope.instEmplBankInfo == null) {
                                    InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                } else {
                                    InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                }
                            }, function (result) {
                            });
                        }
                        else {
                            InstEmpAddress.update($scope.instEmpAddress, function (result) {
                                $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                                if ($scope.instEmplBankInfo == null) {
                                    InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                } else {
                                    InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                }
                            }, function (result) {
                            });
                        }
                    });


                }
                else if ($scope.instEmployee != null && $scope.preCondition()){
                    InstEmployee.save($scope.instEmployee, function (result) {
                        $scope.instEmpAddress.instEmployee = result;

                        if ($scope.instEmpAddress == null) {
                            InstEmpAddress.save($scope.instEmpAddress, function (result) {
                                $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                                if ($scope.instEmplBankInfo == null) {
                                    InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                } else {
                                    InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});

                                    })
                                }
                            }, function (result) {
                            });
                        }
                        else {
                            InstEmpAddress.update($scope.instEmpAddress, function (result) {
                                $scope.instEmplBankInfo.instEmployee = $scope.instEmployee;
                                if ($scope.instEmplBankInfo == null) {
                                    InstEmplBankInfo.save($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.created');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                } else {
                                    InstEmplBankInfo.update($scope.instEmplBankInfo, function (result) {
                                        $rootScope.setSuccessMessage('stepApp.instEmplBankInfo.updated');
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    }, function (result) {
                                        $state.go('employeeInfo.personalInfo', {}, {reload: true});
                                    })
                                }
                            }, function (result) {
                            });
                        }
                    });
                }


            };

            $scope.clear = function () {
                $state.go('employeeInfo.personalInfo');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;


            <!-- Development yard for Multiple attachment implementation work! :Start>>-->

            $scope.setAttachment = function ($index, attachment, noAttach) {
                if (noAttach) {
                    if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                        $scope.showAddMoreButton = false;
                    } else {
                        $scope.showAddMoreButton = true;
                    }
                    if ($scope.loanReqFormEditMode) $scope.loanReqFormHasError = false;
                }
                try {
                    if (attachment == "") {
                        $scope.loanReqFormHasError = true;
                    } else {
                        if (!noAttach && attachment.file)
                            $scope.showAddMoreButton = true;
                        if (noAttach && (attachment.remarks == undefined || attachment.remarks == ""))
                            $scope.showAddMoreButton = true;
                    }
                    attachment.attachmentCategory = angular.fromJson(attachment.attachment);

                    if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                        $scope.showAddMoreButton = false;
                    }

                    if (attachment.attachmentCategory.id) {
                        $scope.currentSelectItem[$index] = attachment.attachmentCategory.id;
                    }
                    if ($scope.loanReqFormEditMode) $scope.loanReqFormHasError = false;

                } catch (e) {
                    $scope.showAddMoreButton = false;
                    $scope.loanReqFormHasError = true;
                    $scope.currentSelectItem.splice($index, 1);
                }
                if ($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length) {
                    $scope.loanReqFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {

                        if (value.noAttachment && (value.remarks == undefined || value.remarks == "")) {
                            $scope.loanReqFormHasError = true;
                        } else {
                            if (value.fileName) {
                                $scope.loanReqFormHasError = true;
                            }
                        }

                    });
                }
                else {
                    angular.forEach($scope.applicantAttachment, function (value, key) {

                        if (!value.noAttachment && (value.fileName)) {
                            $scope.loanReqFormHasError = true;

                        }
                    });
                }

                if ($scope.loanReqFormEditMode) $scope.loanReqFormHasError = false;

                if (arrayUnique($scope.currentSelectItem).length != $scope.currentSelectItem.length)
                    $scope.duplicateError = true;
                else
                    $scope.duplicateError = false;

                if ($scope.duplicateError) {
                    $scope.showAddMoreButton = false;
                }
            }


            $scope.setFile = function ($file, attachment) {
                $scope.showAddMoreButton = true;
                $scope.loanReqFormHasError = true;
                try {
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {

                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function () {
                                try {
                                    attachment.file = base64Data;
                                    attachment.fileContentType = $file.type;
                                    attachment.fileName = $file.name;
                                } catch (e) {
                                    $scope.showAddMoreButton = false;
                                }
                            });
                        };


                        if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                            $scope.showAddMoreButton = false;
                        }

                        if ($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length)
                            $scope.loanReqFormHasError = false;
                        else
                            $scope.loanReqFormHasError = true;

                        if ($scope.loanReqFormEditMode) $scope.loanReqFormHasError = false;
                    }
                } catch (e) {
                    $scope.showAddMoreButton = false;
                    $scope.loanReqFormHasError = true;
                    if ($scope.loanReqFormEditMode) $scope.loanReqFormHasError = false;
                }
            };

            $scope.setFileForEdit = function ($file, attachment) {
                try {
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {

                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function () {
                                try {
                                    attachment.file = base64Data;
                                    attachment.fileContentType = $file.type;
                                    attachment.fileName = $file.name;
                                } catch (e) {
                                }
                            });
                        };
                    }
                } catch (e) {

                    if ($scope.loanReqFormEditMode) $scope.loanReqFormHasError = false;
                }
            };


            $scope.addMoreAttachment = function () {
                console.log('&&&&&&&&&&&&&&&&&&&&&&&');
                if (!inArray($scope.applicantAttachmentCount.length, $scope.applicantAttachmentCount)) {
                    $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
                }else{
                    $scope.applicantAttachmentCount.length++;
                    $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
                }
                $scope.showAddMoreButton = false;
            }

            $scope.removeAttachment = function (attachment) {
                $scope.showAddMoreButton = true;
                $scope.loanReqFormHasError = true;
                var index = $scope.applicantAttachmentCount.indexOf(attachment);
                $scope.applicantAttachmentCount.splice(index, 1);

                if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length - 1) {
                    $scope.showAddMoreButton = false;
                    $scope.loanReqFormHasError = false;
                }
            }

            function arrayUnique(a) {
                return a.reduce(function (p, c) {
                    if (p.indexOf(c) < 0) p.push(c);
                    return p;
                }, []);
            };

            function inArray(needle, haystack) {
                var length = haystack.length;
                for (var i = 0; i < length; i++) {
                    if (typeof haystack[i] == 'object') {
                        if (arrayCompare(haystack[i], needle)) return true;
                    } else {
                        if (haystack[i] == needle) return true;
                    }
                }
                return false;
            }

            function arrayCompare(a1, a2) {
                if (a1.length != a2.length) return false;
                var length = a2.length;
                for (var i = 0; i < length; i++) {
                    if (a1[i] !== a2[i]) return false;
                }
                return true;
            }

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }

                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }

                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }

                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.selectNoAttachment = function (val, val2, file) {

                if (val && val2) {
                    if ($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length) {

                        $scope.showAddMoreButton = false;
                        $scope.loanReqFormHasError = false;
                        angular.forEach($scope.applicantAttachment, function (value, key) {
                            if (value.noAttachment && (value.remarks == undefined || value.remarks == "")) {
                                $scope.loanReqFormHasError = true;
                            }
                        });

                    } else {
                        $scope.showAddMoreButton = true;
                    }
                } else {
                    if (file == undefined) {
                        $scope.loanReqFormHasError = true;
                    }

                    if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                        $scope.showAddMoreButton = false;
                    }
                }
                if ($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length) {
                    $scope.showAddMoreButton = false;
                }
            }

            $scope.remarksChange = function (val, val2) {

                if ($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length) {
                    $scope.showAddMoreButton = false;
                    $scope.loanReqFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {

                        if (value.noAttachment && (value.remarks == undefined || value.remarks == "")) {
                            $scope.loanReqFormHasError = true;
                        }
                    });
                }
                if ($scope.loanReqFormEditMode) {
                    $scope.loanReqFormHasError = false;
                }
            }

            $scope.previewImage = function (content, contentType, name) {
                console.log('&&&&&&&&&&&&&&&&&&&&&&');
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                // call the modal
                $rootScope.showFilePreviewModal();
            };

            $scope.editAttachment = function () {
                $scope.browseButton = false;
            }
            <!--Multiple attachment implementation work is under development:End>>-->


            if ($scope.instEmployee.quotaCert != null) {
                var quotaCertType = $scope.instEmployee.quotaCertContentType;
                $scope.preview = false;
                if (quotaCertType.indexOf("image") >= 0 || quotaCertType.indexOf("pdf") >= 0) {
                    $scope.preview = true;
                }
                var blob = $rootScope.b64toBlob($scope.instEmployee.quotaCert, quotaCertType);
                $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
            }


            $scope.setImage = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.image = base64Data;
                            instEmployee.imageContentType = $file.type;
                            instEmployee.imageName = $file.name;
                            var blob = b64toBlob(instEmployee.image, instEmployee.imageContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };
            $scope.changeSameAddress = function (data) {
                if (data) {

                    $scope.instEmpAddress.prevVillageOrHouse = $scope.instEmpAddress.villageOrHouse;
                    $scope.instEmpAddress.prevPost = $scope.instEmpAddress.post;
                    $scope.instEmpAddress.prevPostCode = $scope.instEmpAddress.postCode;
                    $scope.instEmpAddress.prevRoadBlockSector = $scope.instEmpAddress.roadBlockSector;
                    $scope.instEmpAddress.prevUpazila = $scope.instEmpAddress.upazila;
                    $scope.instEmpAddress.prevDistrict = $scope.instEmpAddress.district;
                    $scope.instEmpAddress.prevDivision = $scope.instEmpAddress.district.division;
                } else {
                    $scope.instEmpAddress.prevPost = null;
                    $scope.instEmpAddress.prevPostCode = null;
                    $scope.instEmpAddress.prevVillageOrHouse = null;
                    $scope.instEmpAddress.prevRoadBlockSector = null;
                    $scope.instEmpAddress.prevUpazila = null;
                    $scope.instEmpAddress.prevDistrict = null;
                    $scope.instEmpAddress.prevDivision = null;
                }
            };
            $scope.yerExam = function (data, data2) {
                if(data > data2){
                    $scope.showerror= true;
                }else {
                    $scope.showerror=false;
                }
            };
            var today = new Date();
            $scope.toDay = today;
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.setNidImage = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.nidImage = base64Data;
                            instEmployee.nidImageContentType = $file.type;
                            instEmployee.nidImageName = $file.name;
                        });
                    };
                }
            };

            $scope.setBirthCertImage = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.birthCertImage = base64Data;
                            instEmployee.birthCertImageContentType = $file.type;
                            instEmployee.birthCertNoName = $file.name;
                        });
                    };
                }
            };

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {type: contentType});
                return blob;
            }


            $scope.courseSubsGlobal = CourseSub.query();
            $scope.coursetechs = CourseTech.query();
            $scope.coursesubs = [];
            $scope.selectedTech = "Select a Technology";
            $scope.SelectedSubs = "Select a Subject";

            $scope.CourseTechChange = function (data) {
                $scope.coursesubs = [];
                angular.forEach($scope.courseSubsGlobal, function (subjectData) {
                    if (data.id == subjectData.courseTech.id) {
                        $scope.coursesubs.push(subjectData);
                    }
                });
            };

            $scope.CourseTradeChange = function (data) {
                CmsSubAssignByTrade.query({id: data.id}, function (result) {
                    $scope.cmsSubAssigns = result;
                });
            };

            $scope.setDesignation = function (instLevel) {
                $scope.instLevel = instLevel;
                if ($scope.instEmployee.category == 'Teacher') {
                }
            };



            $scope.preCondition = function() {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.fatherName', $scope.instEmployee.fatherName, 'text', 150, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.motherName', $scope.instEmployee.motherName, 'text', 150, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.tin', $scope.instEmployee.tin, 'number', 50, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.nationality', $scope.instEmployee.nationality, 'text', 150, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.nid', $scope.instEmployee.nid, 'number', 17, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.birthCertNo', $scope.instEmployee.birthCertNo, 'number', 17, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.registeredCertificateSubject', $scope.instEmployee.registeredCertificateSubject, 'text', 150, 2, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.rollNo', $scope.instEmployee.rollNo, 'number', 17, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.ordinalNo', $scope.instEmployee.ordinalNo, 'number', 17, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.registrationExamYear', $scope.instEmployee.registrationExamYear, 'number', 4, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.passingYear', $scope.instEmployee.passingYear, 'number', 4, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.registeredCertificateNo', $scope.instEmployee.registeredCertificateNo, 'number', 20, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.contactNo', $scope.instEmployee.contactNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.altContactNo', $scope.instEmployee.altContactNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.conPerName', $scope.instEmployee.conPerName, 'text', 150, 2, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.conPerMobile', $scope.instEmployee.conPerMobile, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpAddress.postCode', $scope.instEmpAddress.postCode, 'number', 4, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpAddress.prevPostCode', $scope.instEmpAddress.prevPostCode, 'number', 4, 4, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplBankInfo.bankAccountNo', $scope.instEmplBankInfo.bankAccountNo, 'number', 30, 2, '0to9', true, '', ''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            };
        }]);
