'use strict';

angular.module('stepApp')
    .controller('InstEmployeeController',
        ['$scope', '$state', '$modal', 'DataUtils', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks',
            function ($scope, $state, $modal, DataUtils, InstEmployee, InstEmployeeSearch, ParseLinks) {

                $scope.instEmployees = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmployee.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmployees = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmployeeSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmployees = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmployee = {
                        name: null,
                        email: null,
                        contactNo: null,
                        fatherName: null,
                        motherName: null,
                        dob: null,
                        category: null,
                        gender: null,
                        maritalStatus: null,
                        bloodGroup: null,
                        tin: null,
                        image: null,
                        imageContentType: null,
                        nationality: null,
                        nid: null,
                        nidImage: null,
                        nidImageContentType: null,
                        birthCertNo: null,
                        birthCertImage: null,
                        birthCertImageContentType: null,
                        conPerName: null,
                        conPerMobile: null,
                        conPerAddress: null,
                        id: null
                    };
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]).controller('InstEmployeeListController',
    ['$scope', '$location', '$state', '$modal', 'Principal', 'DataUtils', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks', 'CurrentInstEmployee', 'InstEmployeeApproveList', 'InstGenInfo', 'GovtInstEmployeeApproveList', 'NonGovtInstEmployeeApproveList', 'FindteacherListByDeptHead', 'FindteacherListByInstitute', 'FindstaffListByDeptHead', 'FindStaffsListByInstitute', 'FindApprovedTeacherList',
        function ($scope, $location, $state, $modal, Principal, DataUtils, InstEmployee, InstEmployeeSearch, ParseLinks, CurrentInstEmployee, InstEmployeeApproveList, InstGenInfo, GovtInstEmployeeApproveList, NonGovtInstEmployeeApproveList, FindteacherListByDeptHead, FindteacherListByInstitute, FindstaffListByDeptHead, FindStaffsListByInstitute, FindApprovedTeacherList) {

            $scope.instEmployees = [];
            $scope.page = 0;

            FindStaffsListByInstitute.query({size: 200}, function (result) {
                $scope.staffListByInstitute = result;
                var instArray = [];
                var instColorArray = ['#ffcccc', '#ffe6e6', '#ccffff', '#f9e2d2', '#d1dffa', '#ccffcc', '#ffe6ff', '#ffcccc', '#ffe6f3', '#ccf2ff', '#fff7cc', '#f3d8d8'];
                var counter = 0;
                var i = 0;
                angular.forEach($scope.staffListByInstitute, function (instObj) {

                    var isColorMatched = false;
                    for (i = 0; i < instArray.length; i++) {
                        if (instArray[i] == instObj.institute.id) {
                            instObj.institute.colorCode = instColorArray[i];
                            isColorMatched = true;
                        }

                    }
                    if (isColorMatched == false) {
                        //instObj.institute.colorCode = $scope.getRandomColor();
                        instObj.institute.colorCode = instColorArray[counter];
                        //instColorArray[counter] = instObj.institute.colorCode;
                        instArray[counter] = instObj.institute.id;
                        counter++;
                    }
                });
            });

            FindteacherListByInstitute.query({size: 200}, function (result) {
                $scope.employeeListByInstitute = result;
                var instArray = [];
                var instColorArray = ['#ffcccc', '#ffe6e6', '#ccffff', '#f9e2d2', '#d1dffa', '#ccffcc', '#ffe6ff', '#ffcccc', '#ffe6f3', '#ccf2ff', '#fff7cc', '#f3d8d8'];
                var counter = 0;
                var i = 0;
                angular.forEach($scope.employeeListByInstitute, function (instObj) {

                    var isColorMatched = false;
                    for (i = 0; i < instArray.length; i++) {
                        if (instArray[i] == instObj.institute.id) {
                            instObj.institute.colorCode = instColorArray[i];
                            isColorMatched = true;
                        }
                    }
                    if (isColorMatched == false) {
                        console.log("color: " + instColorArray[counter] + ", counter: " + counter + ", matched: " + isColorMatched);
                        //instObj.institute.colorCode = $scope.getRandomColor();
                        instObj.institute.colorCode = instColorArray[counter];
                        //instColorArray[counter] = instObj.institute.colorCode;
                        instArray[counter] = instObj.institute.id;
                        counter++;
                    }
                });
            });

            $scope.getRandomColor = function () {
                return '#' + Math.floor(Math.random() * 16777215).toString(16);
            };

            $scope.loadAll = function () {

                if (Principal.hasAnyAuthority(['ROLE_ADMIN','ROLE_HRM_ADMIN'])) {
                    FindApprovedTeacherList.query({size: 2000}, function (result) {
                        $scope.instEmployees = result;
                    });
                }

                if (Principal.hasAnyAuthority(['ROLE_INSTEMP', 'ROLE_HRM_USER'])) {
                    CurrentInstEmployee.get({}, function (result) {
                        $scope.currentEmployee = result;

                        InstEmployeeApproveList.get({
                            id: $scope.currentEmployee.institute.id,
                            size: 2000
                        }, function (result) {
                            $scope.instEmployees = result;

                        });

                    });
                } else if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                    InstGenInfo.get({id: 0}, function (result) {
                        $scope.curInstitute = result;
                        InstEmployeeApproveList.get({id: result.institute.id, size: 2000}, function (result) {
                            $scope.instEmployees = result;
                        });
                        FindstaffListByDeptHead.get({id: result.institute.id, size: 2000}, function (result) {
                            $scope.staffListByDeptHead = result;
                            var instArray = [];
                            var instColorArray = ['#ffcccc', '#ffe6e6', '#ccffff', '#f9e2d2', '#d1dffa', '#ccffcc', '#ffe6ff', '#ffcccc', '#ffe6f3', '#ccf2ff', '#fff7cc', '#f3d8d8'];
                            //var instColorArray = ['#A9CCE3'];
                            var counter = 0;
                            var i = 0;
                            angular.forEach($scope.staffListByDeptHead, function (instObj) {
                                var isColorMatched = false;
                                for (i = 0; i < instArray.length; i++) {
                                    if (instArray[i] == instObj.hrdepartmentSetup.id) {
                                        instObj.hrdepartmentSetup.colorCode = instColorArray[i];
                                        isColorMatched = true;
                                    }
                                }
                                if (isColorMatched == false) {
                                    //instObj.institute.colorCode = $scope.getRandomColor();
                                    instObj.hrdepartmentSetup.colorCode = instColorArray[counter];
                                    //instColorArray[counter] = instObj.institute.colorCode;
                                    instArray[counter] = instObj.hrdepartmentSetup.id;
                                    counter++;
                                }

                            });
                        });

                        FindteacherListByDeptHead.get({id: result.institute.id, size: 200}, function (result) {
                            $scope.employeeListByDeptHead = result;

                            var instArray = [];
                            var instColorArray = ['#ffcccc', '#ffe6e6', '#ccffff', '#f9e2d2', '#d1dffa', '#ccffcc', '#ffe6ff', '#ffcccc', '#ffe6f3', '#ccf2ff', '#fff7cc', '#f3d8d8'];
                            //var instColorArray = ['#A9CCE3'];
                            var counter = 0;
                            var i = 0;

                            angular.forEach($scope.employeeListByDeptHead, function (instObj) {

                                var isColorMatched = false;
                                for (i = 0; i < instArray.length; i++) {
                                    if (instArray[i] == instObj.hrdepartmentSetup.id) {
                                        instObj.hrdepartmentSetup.colorCode = instColorArray[i];
                                        console.log("dept color code");
                                        console.log(instObj.hrdepartmentSetup.colorCode);
                                        isColorMatched = true;
                                    }

                                }
                                if (isColorMatched == false) {
                                    //instObj.institute.colorCode = $scope.getRandomColor();
                                    instObj.hrdepartmentSetup.colorCode = instColorArray[counter];
                                    //instColorArray[counter] = instObj.institute.colorCode;
                                    instArray[counter] = instObj.hrdepartmentSetup.id;
                                    counter++;
                                }

                            });

                        });

                    });
                }

            };
            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            if ($location.path() == '/employee-info/employee-list') {
                $scope.showHeading = '/employee-info/employee-list';
            }
            else {
                $scope.showHeading = null;
            }

            $scope.search = function () {
                InstEmployeeSearch.query({query: $scope.searchQuery}, function (result) {
                    $scope.instEmployees = result;
                }, function (response) {
                    if (response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;
        }])
    .controller('InstStaffListController',
        ['$scope', '$state', '$modal', 'DataUtils', 'InstEmployee', 'Principal', 'InstEmployeeSearch', 'ParseLinks', 'CurrentInstEmployee', 'InstStaffApproveList', 'InstGenInfo', 'FindApprovedStaffList',
            function ($scope, $state, $modal, DataUtils, InstEmployee, Principal, InstEmployeeSearch, ParseLinks, CurrentInstEmployee, InstStaffApproveList, InstGenInfo, FindApprovedStaffList) {

                $scope.instStaffs = [];
                $scope.page = 0;
                $scope.loadAll = function () {

                    if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                        FindApprovedStaffList.query({size: 200}, function (result) {
                            $scope.instStaffs = result;
                        });
                    }
                    if (Principal.hasAnyAuthority(['ROLE_INSTEMP'])) {
                        CurrentInstEmployee.get({}, function (result) {
                            $scope.currentEmployee = result;

                            InstStaffApproveList.get({
                                id: $scope.currentEmployee.institute.id,
                                size: 200
                            }, function (result) {
                                $scope.instStaffs = result;
                            });
                        });
                    } else if (Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                        InstGenInfo.get({id: 0}, function (result) {
                            InstStaffApproveList.get({id: result.institute.id, size: 200}, function (result) {
                                $scope.instStaffs = result;
                            });
                        });
                    }
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();
                $scope.search = function () {
                    InstEmployeeSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmployees = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }])
    .controller('InstEmployeePendingListController',
        ['$scope', '$state', '$modal', 'DataUtils', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks', 'InstEmployeePendingList', 'InstGenInfo',
            function ($scope, $state, $modal, DataUtils, InstEmployee, InstEmployeeSearch, ParseLinks, InstEmployeePendingList, InstGenInfo) {

                $scope.instEmployees = [];
                $scope.page = 0;
                $scope.listType = 'pending';
                $scope.loadAll = function () {
                    InstGenInfo.get({id: 0}, function (result) {
                        InstEmployeePendingList.get({id: result.institute.id}, function (result) {
                            $scope.instEmployees = result;


                        })
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmployeeSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmployees = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };


                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;
            }]).controller('InstEmployeeDeclinedListController',
    ['$scope', '$state', '$modal', 'DataUtils', 'InstEmployee', 'InstEmployeeSearch', 'ParseLinks', 'InstEmployeeDeclineList', 'InstGenInfo',
        function ($scope, $state, $modal, DataUtils, InstEmployee, InstEmployeeSearch, ParseLinks, InstEmployeeDeclineList, InstGenInfo) {

            $scope.instEmployees = [];
            $scope.page = 0;
            $scope.listType = 'declined';
            $scope.loadAll = function () {
                InstGenInfo.get({id: 0}, function (result) {
                    InstEmployeeDeclineList.get({id: result.institute.id}, function (result) {
                        $scope.instEmployees = result;
                    })
                });
            };
            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();


            $scope.search = function () {
                InstEmployeeSearch.query({query: $scope.searchQuery}, function (result) {
                    $scope.instEmployees = result;
                }, function (response) {
                    if (response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

        }]).controller('InstEmployeeAssignDeptHeadController',
    ['$scope', '$state', '$modal', '$rootScope', 'AssignNewHeadOfDept', 'InstEmployee', 'InstituteByLogin', 'DepartmentByInstitute', 'GetAllInstEmployeeByDepSetuptId',
        function ($scope, $state, $modal, $rootScope, AssignNewHeadOfDept, InstEmployee, InstituteByLogin, DepartmentByInstitute, GetAllInstEmployeeByDepSetuptId) {

            $scope.instEmployees = [];
            $scope.page = 0;
            $scope.deptList = [];
            $scope.instEmployees = [];

            $scope.loadAll = function () {
            };
            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            InstituteByLogin.query(function (institute) {
                DepartmentByInstitute.query({instId: institute.id}, function (result) {
                    $scope.deptList = result;
                });
            });

            $scope.instituteEmployeeByDept = function (deptId) {
                GetAllInstEmployeeByDepSetuptId.query({deptSetupId: deptId}, function (emplosyeeList) {
                    $scope.instEmployees = emplosyeeList;
                });
            }

            $scope.save = function () {
                AssignNewHeadOfDept.update($scope.instEmployee, function (onSuccessResult) {
                    if (onSuccessResult.Assign == 'Assigned') {
                        $state.go('employeeInfo.headOfDeptAssign', {}, {reload: true});
                        $rootScope.setSuccessMessage('Successfully Assigned');
                    } else if (onSuccessResult.Assign == 'AlreadyAssigned') {
                        $rootScope.setSuccessMessage('Already Assigned');
                    } else if (onSuccessResult.Assign == 'Failed') {
                        $rootScope.setErrorMessage('Assign Failed');
                    }
                });
            }
            $scope.cancelBtn = function () {
                $state.go('employeeInfo', {}, {reload: true});
            };

        }]);
