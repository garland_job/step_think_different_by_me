'use strict';

angular.module('stepApp')
    .controller('InstEmplRecruitInfoController',
        ['$scope', '$state', '$modal', 'InstEmplRecruitInfo', 'InstEmplRecruitInfoSearch', 'ParseLinks',
            function ($scope, $state, $modal, InstEmplRecruitInfo, InstEmplRecruitInfoSearch, ParseLinks) {

                $scope.instEmplRecruitInfos = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmplRecruitInfo.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmplRecruitInfos = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    InstEmplRecruitInfoSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmplRecruitInfos = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmplRecruitInfo = {
                        salaryScale: null,
                        salaryCode: null,
                        monthlySalaryGovtProvided: null,
                        monthlySalaryInstituteProvided: null,
                        gbResolutionReceiveDate: null,
                        gbResolutionAgendaNo: null,
                        circularPaperName: null,
                        circularPublishedDate: null,
                        recruitExamReceiveDate: null,
                        dgRepresentativeName: null,
                        dgRepresentativeDesignation: null,
                        dgRepresentativeAddress: null,
                        boardRepresentativeName: null,
                        boardRepresentativeDesignation: null,
                        boardRepresentativeAddress: null,
                        recruitApproveGBResolutionDate: null,
                        recruitPermitAgendaNo: null,
                        recruitmentDate: null,
                        presentInstituteJoinDate: null,
                        presentPostJoinDate: null,
                        dgRepresentativeRecordNo: null,
                        boardRepresentativeRecordNo: null,
                        department: null,
                        id: null
                    };
                };
            }]);

/* instEmplRecruitInfo-dialog.controller.js */

angular.module('stepApp').controller('InstEmplRecruitInfoDialogController',
    ['$scope', '$rootScope', '$stateParams', '$q', '$state', 'entity', 'InstEmplRecruitInfo', 'InstEmployee', 'DataUtils', 'PayScalesOfActiveGazette','DateUtils',
        function ($scope, $rootScope, $stateParams, $q, $state, entity, InstEmplRecruitInfo, InstEmployee, DataUtils, PayScalesOfActiveGazette, DateUtils) {


            $scope.instEmplRecruitInfo = entity;
            $scope.payScales = [];
            // $q.all([$scope.instEmplRecruitInfo.$promise]).then(function () {
            //     return $scope.instEmplRecruitInfo.$promise;
            // });

            PayScalesOfActiveGazette.query({size:2000}, function (result) {
                $scope.payScales = result;
            });

            $scope.toDay = new Date();
            //$scope.calendar = {
            //    opened: {},
            //    dateFormat: 'dd-MM-yyyy',
            //    dateOptions: {},
            //    open: function ($event, which) {
            //        console.log('----');
            //        $event.preventDefault();
            //        $event.stopPropagation();
            //        $scope.calendar.opened[which] = true;
            //    }
            //};

            if($scope.instEmplRecruitInfo.id == null){
            }else if($scope.instEmplRecruitInfo.id != null){
                $rootScope.refreshRequiredFields();
            }


            $scope.preCondition = function(){
                console.log($scope.instEmplRecruitInfo.gbResolutionReceiveDate);
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.monthlySalaryGovtProvided', $scope.instEmplRecruitInfo.monthlySalaryGovtProvided, 'number', 7, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.monthlySalaryInstituteProvided', $scope.instEmplRecruitInfo.monthlySalaryInstituteProvided, 'number', 7, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.gbResolutionReceiveDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.gbResolutionReceiveDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.circularPaperName', $scope.instEmplRecruitInfo.circularPaperName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.circularPublishedDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.circularPublishedDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.circularNationalPaperName', $scope.instEmplRecruitInfo.circularNationalPaperName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.circularNationalPubDt', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.circularNationalPubDt), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.recruitExamReceiveDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.recruitExamReceiveDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.boardRepresentativeName', $scope.instEmplRecruitInfo.boardRepresentativeName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.boardRepresentativeDesignation', $scope.instEmplRecruitInfo.boardRepresentativeDesignation, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.recruitApproveGBResolutionDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.recruitApproveGBResolutionDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.recruitmentDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.recruitmentDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.presentInstituteJoinDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.presentInstituteJoinDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.presentPostJoinDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.presentPostJoinDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.dgRepresentativeName', $scope.instEmplRecruitInfo.dgRepresentativeName, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.dgRepresentativeDesignation', $scope.instEmplRecruitInfo.dgRepresentativeDesignation, 'text', 150, 5, 'atozAndAtoZ', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.dgRecordDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.dgRecordDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.committeeForDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.committeeForDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.recommendDateAppl', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.recommendDateAppl), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.approvalDateAppointment', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.approvalDateAppointment), 'date', 5, 60, 'date', true, '',''));


                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.mcDuration', $scope.instEmplRecruitInfo.mcDuration, 'text', 50, 1, 'atozAndAtoZ.-', true, '',''));
                //$scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.mcMemoNo', $scope.instEmplRecruitInfo.mcMemoNo, 'text', 20, 2, 'atozAndAtoZ0-9.-', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmplRecruitInfo.mcMemoDate', DateUtils.convertLocaleDateToDMY($scope.instEmplRecruitInfo.mcMemoDate), 'date', 60, 5, 'date', true, '',''));


                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            };




            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmplRecruitInfoUpdate', result);
                $scope.isSaving = false;
                $state.go("employeeInfo.recruitmentInfo", {}, {reload: true});
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };
            /*&& $scope.preCondition()*/
            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.instEmplRecruitInfo.id != null && $scope.preCondition()) {
                    console.log("Comming Here");
                    InstEmplRecruitInfo.update($scope.instEmplRecruitInfo, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.instEmplRecruitInfo.updated');
                } else if($scope.instEmplRecruitInfo.id == null && $scope.preCondition()){
                    InstEmplRecruitInfo.save($scope.instEmplRecruitInfo, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.instEmplRecruitInfo.created');
                }
            };
            $scope.clear = function () {
                $scope.instEmplRecruitInfo = null;
            };
            $scope.abbreviate = DataUtils.abbreviate;
            $scope.byteSize = DataUtils.byteSize;
            $scope.setAttachment = function ($file, instEmplRecruitInfo, attachment, attachmentContentType, attachmentName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[attachment] = base64Data;
                            instEmplRecruitInfo[attachmentContentType] = $file.type;
                            instEmplRecruitInfo[attachmentName] = $file.name;
                        });
                    };
                }
            };

            $scope.setGoResulaton = function ($file, instEmplRecruitInfo, goResulaton, goResulatonContentType, goResulatonName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[goResulaton] = base64Data;
                            instEmplRecruitInfo[goResulatonContentType] = $file.type;
                            instEmplRecruitInfo[goResulatonName] = $file.name;
                        });
                    };
                }
            };

            $scope.setRecruitNewsLocal = function ($file, instEmplRecruitInfo, recruitNewsLocal, recruitNewsLocalContentType, recruitNewsLocalName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[recruitNewsLocal] = base64Data;
                            instEmplRecruitInfo[recruitNewsLocalContentType] = $file.type;
                            instEmplRecruitInfo[recruitNewsLocalName] = $file.name;
                        });
                    };
                }
            };

            $scope.setRecruitNewsDaily = function ($file, instEmplRecruitInfo, recruitNewsDaily, recruitNewsDailyContentType, recruitNewsDailyName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[recruitNewsDaily] = base64Data;
                            instEmplRecruitInfo[recruitNewsDailyContentType] = $file.type;
                            instEmplRecruitInfo[recruitNewsDailyName] = $file.name;
                        });
                    };
                }
            };

            $scope.setJoiningLetter = function ($file, instEmplRecruitInfo, joiningLetter, joiningLetterContentType, joiningLetterName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[joiningLetter] = base64Data;
                            instEmplRecruitInfo[joiningLetterContentType] = $file.type;
                            instEmplRecruitInfo[joiningLetterName] = $file.name;
                        });
                    };
                }
            };
            $scope.setRecommendation = function ($file, instEmplRecruitInfo, recommendation, recommendationContentType, recommendationName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[recommendation] = base64Data;
                            instEmplRecruitInfo[recommendationContentType] = $file.type;
                            instEmplRecruitInfo[recommendationName] = $file.name;
                        });
                    };
                }
            };

            $scope.setAppoinmentLtr = function ($file, instEmplRecruitInfo, appoinmentLtr, appoinmentLtrCntType, appoinmentLtrName) {
                if ($file) {
                    var fileReader = new FileReader();
                    var strin = 'attachment';
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[appoinmentLtr] = base64Data;
                            instEmplRecruitInfo[appoinmentLtrCntType] = $file.type;
                            instEmplRecruitInfo[appoinmentLtrName] = $file.name;
                        });
                    };
                }
            };
            $scope.setDgAprove = function ($file, instEmplRecruitInfo, dgApproveFile, dgApproveFileContentType, dgApproveFileName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[dgApproveFile] = base64Data;
                            instEmplRecruitInfo[dgApproveFileContentType] = $file.type;
                            instEmplRecruitInfo[dgApproveFileName] = $file.name;
                        });
                    };
                }
            };
            $scope.setSelectionCopy = function ($file, instEmplRecruitInfo, selectionCopy, selectionCopyCntType, selectionCopyName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[selectionCopy] = base64Data;
                            instEmplRecruitInfo[selectionCopyCntType] = $file.type;
                            instEmplRecruitInfo[selectionCopyName] = $file.name;
                        });
                    };
                }
            };

            $scope.setSanction = function ($file, instEmplRecruitInfo, sanction, sanctionContentType, sanctionName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[sanction] = base64Data;
                            instEmplRecruitInfo[sanctionContentType] = $file.type;
                            instEmplRecruitInfo[sanctionName] = $file.name;
                        });
                    };
                }
            };

            $scope.setMcMemoAttach = function ($file, instEmplRecruitInfo, mcMemoAttach, mcMemoAttachCntType, mcMemoAttachName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[mcMemoAttach] = base64Data;
                            instEmplRecruitInfo[mcMemoAttachCntType] = $file.type;
                            instEmplRecruitInfo[mcMemoAttachName] = $file.name;
                        });
                    };
                }
            };
            $scope.setConfirmLetter = function ($file, instEmplRecruitInfo, confirmLetter, confirmLetterCntType, confirmLetterName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[confirmLetter] = base64Data;
                            instEmplRecruitInfo[confirmLetterCntType] = $file.type;
                            instEmplRecruitInfo[confirmLetterName] = $file.name;
                        });
                    };
                }
            };
            $scope.setNtrcCert = function ($file, instEmplRecruitInfo, ntrcaCert, ntrcaCertCntType, ntrcaCertName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[ntrcaCert] = base64Data;
                            instEmplRecruitInfo[ntrcaCertCntType] = $file.type;
                            instEmplRecruitInfo[ntrcaCertName] = $file.name;
                        });
                    };
                }
            };
            $scope.setAppointAprvAtch = function ($file, instEmplRecruitInfo, appointAprvAtch, appointAprvAtchCntType, appointAprvAtchName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[appointAprvAtch] = base64Data;
                            instEmplRecruitInfo[appointAprvAtchCntType] = $file.type;
                            instEmplRecruitInfo[appointAprvAtchName] = $file.name;
                        });
                    };
                }
            };

            $scope.setRecruitResult = function ($file, instEmplRecruitInfo, recruitResult, recruitResultCntType, recruitResultName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[recruitResult] = base64Data;
                            instEmplRecruitInfo[recruitResultCntType] = $file.type;
                            instEmplRecruitInfo[recruitResultName] = $file.name;
                        });
                    };
                }
            };

            $scope.setPblcCircular = function ($file, instEmplRecruitInfo, pblcCircular, pblcCircularCntType, pblcCircularName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[pblcCircular] = base64Data;
                            instEmplRecruitInfo[pblcCircularCntType] = $file.type;
                            instEmplRecruitInfo[pblcCircularName] = $file.name;
                        });
                    };
                }
            };

            $scope.setReuiLetter = function ($file, instEmplRecruitInfo, requiLetter, requiLetterCntType, requiLetterName) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmplRecruitInfo[requiLetter] = base64Data;
                            instEmplRecruitInfo[requiLetterCntType] = $file.type;
                            instEmplRecruitInfo[requiLetterName] = $file.name;
                        });
                    };
                }
            };

        }]);

/*  instEmplRecruitInfo-detail.controller.js */

angular.module('stepApp')
    .controller('InstEmplRecruitInfoDetailController',
        ['$scope', 'Principal', 'InstEmployeeCode', '$rootScope', '$stateParams', 'InstEmplRecruitInfo', 'InstEmployee', 'InstEmplRecruitInfoCurrent', 'CurrentInstEmployee',
            function ($scope, Principal, InstEmployeeCode, $rootScope, $stateParams, InstEmplRecruitInfo, InstEmployee, InstEmplRecruitInfoCurrent, CurrentInstEmployee) {


                $scope.hideEditButton = false;
                $scope.hideAddButton = true;
                InstEmplRecruitInfoCurrent.get({}, function (result) {
                    $scope.instEmplRecruitInfo = result;
                    if (!result.instEmplRecruitInfo) {
                        $scope.hideAddButton = true;
                        $scope.hideEditButton = false;
                        CurrentInstEmployee.get({}, function (result) {

                            if (result.status < 2) {
                                $scope.hideAddButton = true;
                                $scope.hideEditButton = false;
                            } else {
                                $scope.hideAddButton = false;
                                $scope.hideEditButton = true;
                            }
                            if (result.mpoAppStatus >= 3) {
                                $scope.hideEditButton = false;
                                $scope.hideAddButton = false;
                            }
                        });
                    }
                    else {
                        $scope.hideAddButton = false;
                        $scope.hideEditButton = true;
                        if (result.status <= 2) {
                            $scope.hideAddButton = true;
                            $scope.hideEditButton = false;
                        } else {
                            $scope.hideEditButton = true;
                            $scope.hideAddButton = false;
                        }
                        if ($scope.instEmplRecruitInfo.instEmployee.mpoAppStatus != undefined && $scope.instEmplRecruitInfo.instEmployee.mpoAppStatus >= 3) {
                            $scope.hideEditButton = false;
                            $scope.hideAddButton = false;
                        }
                    }

                });

                var unsubscribe = $rootScope.$on('stepApp:instEmplRecruitInfoUpdate', function (event, result) {
                    $scope.instEmplRecruitInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.previewImage = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    //$rootScope.viewerObject.pageTitle = "Birth Certificate  Image of "+name;
                    $rootScope.viewerObject.pageTitle = name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

            }]);

/* instEmplRecruitInfo-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('InstEmplRecruitInfoDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstEmplRecruitInfo',
            function ($scope, $modalInstance, entity, InstEmplRecruitInfo) {

                $scope.instEmplRecruitInfo = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmplRecruitInfo.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.InstEmplRecruitInfo.deleted');
                };

            }]);
