'use strict';

angular.module('stepApp')
    .controller('InstEmpEduQualiController',
        ['$scope', '$state', '$modal', 'DataUtils', 'InstEmpEduQuali', 'InstEmpEduQualiSearch', 'ParseLinks',
            function ($scope, $state, $modal, DataUtils, InstEmpEduQuali, InstEmpEduQualiSearch, ParseLinks) {

                $scope.instEmpEduQualis = [];
                $scope.page = 0;
                $scope.loadAll = function () {
                    InstEmpEduQuali.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.instEmpEduQualis = result;
                    });
                };
                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

                $scope.search = function () {
                    InstEmpEduQualiSearch.query({query: $scope.searchQuery}, function (result) {
                        $scope.instEmpEduQualis = result;
                    }, function (response) {
                        if (response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.instEmpEduQuali = {
                        certificateName: null,
                        board: null,
                        session: null,
                        semester: null,
                        rollNo: null,
                        passingYear: null,
                        cgpa: null,
                        certificateCopy: null,
                        certificateCopyContentType: null,
                        status: null,
                        id: null
                    };
                };
                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;
            }]);

/* instEmpEduQuali-dialog.controller.js */
angular.module('stepApp').controller('InstEmpEduQualiDialogController',
    ['$scope', '$rootScope', '$stateParams', 'Principal', '$state', 'InstEmployeeCode', 'DataUtils', 'InstEmpEduQuali', 'entity', 'InstEmployee', 'InstEmpEduQualisCurrent', 'CurrentInstEmployee', '$q', 'EduBoard', 'EduLevel', '$timeout', 'EduBoardByType', 'ActiveEduLevels',
        function ($scope, $rootScope, $stateParams, Principal, $state, InstEmployeeCode, DataUtils, InstEmpEduQuali, entity, InstEmployee, InstEmpEduQualisCurrent, CurrentInstEmployee, $q, EduBoard, EduLevel, $timeout, EduBoardByType, ActiveEduLevels) {

            $scope.years = [];
            $scope.instEmpEduQua = [];
            $scope.emptyGroup = [];
            $scope.instEmpEduQualiGroup = [];
            var currentYear = new Date().getFullYear();
            $scope.preConditionStatus = [];

            if($stateParams.id !=null){
                InstEmpEduQuali.get({id:$stateParams.id},function(result){
                        $scope.instEmpEduQua = result;
                });
            }
            EduBoardByType.query({boardType: 'board'}, function (result) {
                $scope.eduBoards = result;
            });
            EduBoardByType.query({boardType: 'university'}, function (result) {
                $scope.eduUniversitys = result;
            });
            $scope.eduLevels = ActiveEduLevels.query();

            if ($stateParams.instEmpEduQuali != null) {
                $scope.instEmpEduQualis = $stateParams.instEmpEduQuali;
                $scope.education = 'Education';
            } else {
                $scope.education = '';
                $state.go('employeeInfo.educationalHistory', {}, {reload: true});
            }
            $scope.instEmployee = null;
            CurrentInstEmployee.get({}, function (result) {
                $scope.instEmployee = result;
                if (result.mpoAppStatus > 2 && result.mpoAppStatus < 5) {
                    $rootScope.setErrorMessage('You have applied for mpo.So you are not allowed to edit');
                    $state.go('employeeInfo.personalInfo');
                }
            });
            $scope.refreshField=function () {
                $scope.emptyGroup.push(
                    {
                      group: null,
                      gpaScale:null
                    }
                );
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);
            };

            $scope.AddMore = function () {
                $scope.instEmpEduQualis.push(
                    {
                        isGpaResult: true,
                        fromUniversity: false,
                        certificateName: null,
                        board: null,
                        session: null,
                        semester: null,
                        rollNo: null,
                        passingYear: null,
                        cgpa: null,
                        certificateCopy: null,
                        certificateCopyContentType: null,
                        status: null,
                        id: null,
                        instEmployee: null
                    }
                );
                // Start Add this code for showing required * in add more fields
                $timeout(function () {
                    $rootScope.refreshRequiredFields();
                }, 100);
                // End Add this code for showing required * in add more fields

            }

            $scope.hideField = function (data,index) {
                $scope.data = data;
                if(data.name == 'PSC/Equivalent' || data.name == 'JSC/Equivalent' || data.name == 'SSC/Equivalent' || data.name == 'HSC/Equivelent'){
                    $scope.instEmpEduQualiGroup[index] = false;
                }else{
                    $scope.instEmpEduQualiGroup[index] = true;
                }
            };

            //$scope.hideFieldall = function (data) {
            //    $scope.data = data;
            //    if(data.name == 'SSC/Equivalent' || data.name == 'HSC/Equivelent'){
            //        $scope.hideFieldallAll = false;
            //    }else{
            //        $scope.hideFieldallAll = true;
            //    }
            //};
            //
            //$scope.hideFielphsc = function (data) {
            //    $scope.data = data;
            //    if(data.name == 'PSC/Equivalent' || data.name == 'JSC/Equivalent' || data.name == 'SSC/Equivalent' || data.name == 'HSC/Equivelent'){
            //        $scope.hideFielphscs = false;
            //    }else{
            //        $scope.hideFielphscs = true;
            //    }
            //};

            $scope.k = [3, 2, 3, 5, 4];
            $scope.a = [];


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:instEmpEduQualiUpdate', result);
                $scope.isSaving = false;
                $state.go('employeeInfo.educationalHistory');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                $scope.isPreCondition = false;

                angular.forEach($scope.instEmpEduQualis, function (data) {
                    $scope.preCondition(data);
                });
                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                if($scope.isPreCondition){
                    angular.forEach($scope.instEmpEduQualis, function (data) {
                        if (data.id != null) {
                            InstEmpEduQuali.update(data);
                            $rootScope.setWarningMessage('stepApp.instEmployee.updated');
                        }else if(data.id==null) {
                            data.instEmployee = $scope.instEmployee;
                            InstEmpEduQuali.save(data);
                            $rootScope.setSuccessMessage('stepApp.instEmployee.created');
                        }
                    });
                    $scope.isSaving = false;
                    $state.go('employeeInfo.educationalHistory',{},{reload:true});
                }else {
                    $rootScope.setErrorMessage('Invalid Data Found');
                }

                //$q.all(requests).then(function () {
                //    $state.go('employeeInfo.educationalHistory', {}, {reload: true});
                //});
            };

            $scope.clear = function () {
                $state.go('employeeInfo.educationalHistory');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setCertificateCopy = function ($file, instEmpEduQuali) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmpEduQuali.certificateCopy = base64Data;
                            instEmpEduQuali.certificateCopyContentType = $file.type;
                            instEmpEduQuali.certificateCopyName = $file.name;
                        });
                    };
                }
            };
            $scope.setCourseAtchFile = function ($file, instEmpEduQuali) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmpEduQuali.courseAtchFile = base64Data;
                            instEmpEduQuali.courseAtchFileCntType = $file.type;
                            instEmpEduQuali.courseAtchFileName = $file.name;
                        });
                    };
                }
            };

            $scope.setAttachment = function ($file, instEmpEduQuali) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmpEduQuali.ugcAttachFile = base64Data;
                            instEmpEduQuali.ugcAttachFileContentType = $file.type;
                            instEmpEduQuali.ugcAttachFileName = $file.name;
                        });
                    };
                }
            };

            $scope.initDates = function () {
                var i;
                for (i = currentYear; i >= currentYear - 50; i--) {
                    $scope.years.push(i);
                }
            };
            $scope.initDates();

            $scope.clearCgpa = function(index){
                $scope.instEmpEduQualis[index].cgpa = null;
            };

            $scope.preCondition = function(instEmpEduQua){
                //$scope.isPreCondition = true;
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.subject',instEmpEduQua.subject, 'text', 150, 5, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.uniName', instEmpEduQua.uniName, 'text', 150, 5, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.groupSubject',instEmpEduQua.groupSubject, 'text', 150, 5, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.session', instEmpEduQua.session, 'text', 9, 10, '0to9-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.duration', instEmpEduQua.duration, 'text', 9, 10, 'atozAndAtoZ0-9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.rollNo', instEmpEduQua.rollNo, 'number', 9, 10, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.regNo', instEmpEduQua.regNo, 'number', 9, 10, '0to9', true, '', ''));
                //$scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.cgpa', $scope.instEmpEduQua.cgpa, 'number', 9, 10, '0to9', true, '', ''));
                //$scope.preConditionStatus.push($rootScope.layerDataCheck('instEmpEduQua.gpaScale', $scope.instEmpEduQua.gpaScale, 'number', 9, 10, '0to9', true, '', ''));

                //if($scope.preConditionStatus.indexOf(false) !== -1){
                //    $scope.isPreCondition = false;
                //}else {
                //    $scope.isPreCondition = true;
                //}
                //return $scope.isPreCondition;
            };
        }]);


/* instEmpEduQuali-detail.controller.js */

angular.module('stepApp')
    .controller('InstEmpEduQualiDetailController',
        ['$scope', 'Principal', 'InstEmployeeCode', '$rootScope', '$stateParams', 'DataUtils', 'InstEmpEduQuali', 'InstEmployee', 'InstEmpEduQualisCurrent', 'CurrentInstEmployee',
            function ($scope, Principal, InstEmployeeCode, $rootScope, $stateParams, DataUtils, InstEmpEduQuali, InstEmployee, InstEmpEduQualisCurrent, CurrentInstEmployee) {
                $scope.EducationalQualifications = [];
                $scope.hideEditButton = false;
                $scope.hideAddButton = true;
                InstEmpEduQualisCurrent.get({}, function (result) {
                    if (result) {
                        $scope.EducationalQualifications = result;
                        if ($scope.EducationalQualifications.length > 0) {
                            $scope.hideAddButton = false;
                            $scope.hideEditButton = true;
                            if ($scope.EducationalQualifications[0].instEmployee.status < 2) {
                                $scope.hideEditbutton = true;
                            } else {
                                $scope.hideEditbutton = false;
                            }
                            if ($scope.EducationalQualifications[0].instEmployee.mpoAppStatus >= 3) {
                                $scope.hideEditButton = false;
                                $scope.hideAddButton = false;
                            }
                        } else {
                            $scope.hideAddButton = true;
                            $scope.hideEditButton = false;
                            $scope.EducationalQualifications.push(
                                {
                                    certificateName: null,
                                    board: null,
                                    session: null,
                                    semester: null,
                                    rollNo: null,
                                    passingYear: null,
                                    cgpa: null,
                                    certificateCopy: null,
                                    certificateCopyContentType: null,
                                    status: null,
                                    id: null,
                                    instEmployee: null
                                }
                            );
                            CurrentInstEmployee.get({}, function (result) {
                                if (result.status <= 2) {
                                    $scope.hideEditbutton = false;
                                    $scope.hideAddButton = true;
                                } else {
                                    $scope.hideAddButton = false;
                                    $scope.hideEditButton = false;
                                }
                                if (result.mpoAppStatus >2 && result.mpoAppStatus < 5) {
                                    $scope.hideEditButton = false;
                                    $scope.hideAddButton = false;
                                }
                            });
                        }
                    } else {
                        $scope.hideEditbutton = false;
                        $scope.hideAddButton = true;
                    }
                });

                $scope.displayAttachment = function (attachment, fileContentType) {
                    var blob = $rootScope.b64toBlob(attachment, fileContentType);
                    $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                }

                $scope.previewCertificate = function (content, contentType, name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };
            }]);


/* instEmpEduQuali-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('InstEmpEduQualiDeleteController',
        ['$scope', '$modalInstance', 'entity', 'InstEmpEduQuali',
            function ($scope, $modalInstance, entity, InstEmpEduQuali) {

                $scope.instEmpEduQuali = entity;
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    InstEmpEduQuali.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                    $rootScope.setErrorMessage('stepApp.instEmployee.deleted');
                };

            }]);
