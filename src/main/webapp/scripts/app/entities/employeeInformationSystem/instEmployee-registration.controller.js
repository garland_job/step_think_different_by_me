'use strict';

angular.module('stepApp').controller('InstEmployeeRegistrationController',
    ['$scope', '$rootScope', '$stateParams', '$state', '$q', 'DataUtils', 'entity', 'InstituteByLogin', 'InstEmployee', 'Institute', 'InstEmpAddress', 'InstEmpEduQuali', 'InstGenInfosByCurrentUser', 'InstLevel', 'IisTradesOfCurrentInst', 'CmsSubAssignByTrade', 'HrDesignationSetup', 'HrEmplTypeInfo', 'HrDesignationSetupByType', 'HrDesignationSetupByTypes', 'District', 'IisCourseInfosOfCurrentInst', 'IisCourseInfo', 'CmsCurriculumByCurrentInst', 'GetCmsSubjectByGeneralStatus', 'MpoCmsTradeByCurriculumId', 'AllCmsSubjectByTrade', 'DepartmentByInstituteAndType', 'CmsTradesByCurriculum', 'DeptHeadByDeptAndInstitute',
            'MiscTypeSetupByCategory','DateUtils',
        function ($scope, $rootScope, $stateParams, $state, $q, DataUtils, entity, InstituteByLogin, InstEmployee, Institute, InstEmpAddress, InstEmpEduQuali, InstGenInfosByCurrentUser, InstLevel, IisTradesOfCurrentInst, CmsSubAssignByTrade, HrDesignationSetup, HrEmplTypeInfo, HrDesignationSetupByType, HrDesignationSetupByTypes, District, IisCourseInfosOfCurrentInst, IisCourseInfo, CmsCurriculumByCurrentInst, GetCmsSubjectByGeneralStatus, MpoCmsTradeByCurriculumId, AllCmsSubjectByTrade, DepartmentByInstituteAndType, CmsTradesByCurriculum, DeptHeadByDeptAndInstitute,MiscTypeSetupByCategory,DateUtils) {

            $scope.cmsSubAssigns = [];
            $scope.instEmployee = {};
            $scope.agencyNameSelectionApplication = {};
            $scope.courseList = [];
            $scope.subjectActive = true;
            $scope.headMsgShow = false;
            $scope.districtsOwn = District.query({size: 200});
            $scope.cmscurriculums = CmsCurriculumByCurrentInst.query({page: 0, size: 2000});
            $scope.sec = false;
            $scope.dept = true;
            $scope.hrDepartmentList = [];
            $scope.encadrementList      = MiscTypeSetupByCategory.get({cat:'Encadrement',stat:'true'});
            $scope.dateError = false;

            $rootScope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $rootScope.calendar.opened[which] = true;
                }
            };

            InstituteByLogin.query({}, function (result) {
                $scope.logInInstitute = result;
                $scope.levelId = result.instLevel.id;
                $scope.levelName = '';
                $scope.instLevels = [];
                InstLevel.query({size: 200}, function (allInst) {
                    angular.forEach(allInst, function (value, key) {
                        if (parseInt($scope.levelId) == parseInt(value.id)) {
                            $scope.levelName = value.name;
                        }
                    });
                    var splitNames = $scope.levelName.split('&');
                    angular.forEach(allInst, function (value, key) {
                        angular.forEach(splitNames, function (spname, spkey) {
                            if (spname.trim() == value.name.trim()) {
                                $scope.instLevels.push(value);
                            }
                        });
                    });
                });
                if ($scope.logInInstitute.type == 'NonGovernment') {
                    HrDesignationSetupByType.query({type: 'Non_Govt_Employee', size: 200}, function (result) {
                        $scope.designationSetups = result;
                    });
                } else if ($scope.logInInstitute.type == 'Government') {
                    DepartmentByInstituteAndType.query({setupType: 'Department'}, function (result) {
                        $scope.hrDepartmentList = result;
                    });
                    $rootScope.setDeptSuccessMessage = function (message) {
                        $rootScope.closeAlert();
                        $('.deptmessage.deptsuccess').show();
                        localStorage.setItem('globalDeptSuccessMessage', message);
                        $rootScope.globalDeptSuccessMessage = localStorage.getItem('globalDeptSuccessMessage');
                        setTimeout(function () {
                            $(".deptmessage.deptsuccess").hide();
                        }, 3000);
                    }

                    $scope.callTest = function (depthead, instEmp) {
                        DeptHeadByDeptAndInstitute.query({deptId: depthead.id}, function (result) {
                            $scope.isdeptHaed = result;
                            $scope.headMsgShow = true;
                            if (instEmp == true) {
                                $rootScope.setDeptSuccessMessage('There is already a dept head of this dept');
                            }
                        }, function (response) {
                            if (response.status === 500) {
                                $scope.headMsgShow = false;
                            }
                        });
                    }
                    HrDesignationSetupByTypes.query({size: 200}, function (result) {
                        $scope.designationSetupsss = result;
                    });
                }

                HrEmplTypeInfo.query({}, function (result) {
                    $scope.employeeTypes = result;
                });
            });
            $scope.pgsType = function (stype) {
                $scope.getDeptList(stype);
                if (stype == 'Department') {
                    $scope.dept = true;
                    $scope.sec = false;
                } else if (stype == 'Section') {
                    $scope.sec = true;
                    $scope.dept = false;
                } else {
                    $scope.sec = false;
                    $scope.dept = false;
                }
            };
            $scope.getDeptList = function (setupType) {
                DepartmentByInstituteAndType.query({setupType: setupType}, function (result) {
                    $scope.hrDepartmentList = result;
                });
            }

            $scope.toDay = new Date();

            $scope.isRequired = false;

            $scope.updateFormFields = function () {
                if ($scope.instEmployee.category == 'Teacher') {
                    $scope.isVacancy = true;
                    $scope.staffVacancy = false;

                    $scope.isRequired = true;
                    if ($('#label-level').html().indexOf('*') == -1)
                        $('#label-level').append('<strong style="color:red"> * </strong>');
                    //if ($('#label-courseTech').html().indexOf('*') == -1)
                    //    $('#label-courseTech').append('<strong style="color:red"> * </strong>');
                    if ($('#label-subject').html().indexOf('*') == -1)
                        $('#label-subject').append('<strong style="color:red"> * </strong>');
                    if ($('#label-email').html().indexOf('*') == -1)
                        $('#label-email').append('<strong style="color:red"> * </strong>');
                    if ($('#label-contactNo').html().indexOf('*') == -1)
                        $('#label-contactNo').append('<strong style="color:red"> * </strong>');
                }
                else {
                    $scope.staffVacancy = true;
                    $scope.isVacancy = false;
                    $scope.isRequired = false;
                    $('#label-level').html($('#label-level').html().replace("*", ""));
                    //$('#label-courseTech').html($('#label-courseTech').html().replace("*", ""));
                    $('#label-subject').html($('#label-subject').html().replace("*", ""));
                    $('#label-email').html($('#label-email').html().replace("*", ""));
                    $('#label-contactNo').html($('#label-contactNo').html().replace("*", ""));
                }
            };
            IisCourseInfosOfCurrentInst.query({}, function (result) {
                $scope.courseList = result;
            });


            $scope.tradeChange = function (curriculumId) {
                CmsTradesByCurriculum.query({id: curriculumId}, function (result) {
                    $scope.cmsTrades = result;
                });
            };
            //$scope.courseSubsGlobal = CourseSub.query();
            //$scope.coursetechs = CourseTech.query();
            //$scope.coursesubs = [];
            $scope.selectedTech = "Select a Technology";
            $scope.SelectedSubs = "Select a Subject";

            //$scope.CourseTechChange = function (data) {
            //    $scope.coursesubs = [];
            //    angular.forEach($scope.courseSubsGlobal, function (subjectData) {
            //        if (data.id == subjectData.courseTech.id) {
            //            $scope.coursesubs.push(subjectData);
            //        }
            //    });
            //};
            $scope.selectedCourse = {};
            $scope.CourseTradeChange = function (data) {
                angular.forEach($scope.courseList, function (courseInfo) {
                    if (courseInfo.cmsTrade.id != null) {
                        if (courseInfo.cmsTrade.id == data) {
                            $scope.selectedCourse = courseInfo;
                            $scope.instEmployee.iisCourseInfo = $scope.selectedCourse;
                            if ($scope.selectedCourse != null) {
                                if ($scope.instEmployee.category == 'Teacher' && $scope.selectedCourse.noOfPostT != '0') {
                                    $scope.isVacancy = false;
                                }
                                else if ($scope.instEmployee.category == 'Staff' && $scope.selectedCourse.noOfPostStaff != '0') {
                                    $scope.staffVacancy = false;
                                }
                            }
                        }
                    }
                });


                CmsSubAssignByTrade.query({id: data}, function (result) {
                    $scope.subjectActive = false;
                    $scope.cmsSubAssigns = result;
                });
            };


            var onSaveSuccess = function (result) {
                $scope.instEmployee.applyDate = new Date();
                $scope.$emit('stepApp:instEmployeeUpdate', result);
                $scope.isSaving = false;
                if (result.institute.type == 'Government') {
                    IisCourseInfo.get({id: result.iisCourseInfo.id}, function (result3) {
                        $scope.iisCourseInfo = result3;
                        if (result.category == 'Teacher' && $scope.iisCourseInfo.noOfPostT != null) {
                            $scope.iisCourseInfo.noOfPostT = parseInt($scope.iisCourseInfo.noOfPostT) - 1;
                        }
                        else if (result.category == 'Staff' && $scope.iisCourseInfo.noOfPostStaff != null) {
                            $scope.iisCourseInfo.noOfPostStaff = parseInt($scope.iisCourseInfo.noOfPostStaff) - 1;
                        }
                        IisCourseInfo.update($scope.iisCourseInfo);
                    });
                }
                if (result.category === 'Staff') {
                    $state.go('instituteInfo.staffList', {}, {reload: true});
                    $rootScope.setSuccessMessage('Admin has got your request. Please wait for confirmation mail .');
                } else {
                    $state.go('instituteInfo.employeeList', {}, {reload: true});
                    $rootScope.setSuccessMessage('Admin has got your request. Please wait for confirmation mail .');
                }
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;
                var byteCharacters = atob(b64Data);
                var byteArrays = [];
                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);
                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    byteArrays.push(byteArray);
                }
                var blob = new Blob(byteArrays, {type: contentType});
                return blob;
            }

            $scope.abbreviate = DataUtils.abbreviate;
            $scope.byteSize = DataUtils.byteSize;
            $scope.setQuotaCert = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.quotaCert = base64Data;
                            instEmployee.quotaCertContentType = $file.type;
                            instEmployee.quotaCertName = $file.name;
                            var blob = b64toBlob(instEmployee.quotaCert, instEmployee.quotaCertContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                            $scope.picNotAdd = false;
                        });
                    };
                }
            };

            $scope.setImage = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.image = base64Data;
                            instEmployee.imageContentType = $file.type;
                            instEmployee.imageName = $file.name;
                            var blob = b64toBlob(instEmployee.image, instEmployee.imageContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };

            $scope.setImage = function ($file, instEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            instEmployee.image = base64Data;
                            instEmployee.imageContentType = $file.type;
                            instEmployee.imageName = $file.name;
                            var blob = b64toBlob(instEmployee.image, instEmployee.imageContentType);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };


            $scope.save = function () {
                $scope.isSaving = true;
                $scope.instEmployee.level = $scope.instEmployee.instLevel.name;
                if (typeof $scope.instEmployee.isJPAdmin == "undefined" || $scope.instEmployee.isJPAdmin == null) {
                    $scope.instEmployee.isJPAdmin = false;
                }
                if ($scope.instEmployee.isHeadOfDept == false || $scope.instEmployee.isHeadOfDept == null) {
                    $scope.instEmployee.isHeadOfDept = 'NO';
                    $scope.instEmployee.rollNo = 'NO';
                } else if ($scope.instEmployee.isHeadOfDept == true) {
                    $scope.instEmployee.isHeadOfDept = 'YES';
                    $scope.instEmployee.rollNo = 'YES';
                }
                $scope.instEmployee.institute = $scope.logInInstitute;
                InstEmployee.save($scope.instEmployee, onSaveSuccess, onSaveError);
                $rootScope.setSuccessMessage('stepApp.instEmployee.created');
            };

            $scope.clear = function () {
                $scope.instEmployee = null;
                $state.go('instituteInfo.addEmployee', {}, {reload: true});
            };

            $scope.testVal = function (test) {
                $scope.instEmployee.isHeadOfDept = test;
            }

            $scope.setTrades = function (curriculum) {
                IisTradesOfCurrentInst.query({curId: curriculum.id}, function (result) {
                    $scope.cmsTrades = result;
                });
                GetCmsSubjectByGeneralStatus.query({curriculumnId: curriculum.id, status: true}, function (result) {
                    $scope.cmsSubjects = result;
                });

            };

            $scope.setCmsSubject = function (tradeId) {
                $scope.CourseTradeChange(tradeId);
                AllCmsSubjectByTrade.get({tradeId: tradeId}, function (result) {
                    $scope.cmsSubjects = result;
                });
            };

            $scope.updatePrlDate = function()
            {
                if($scope.instEmployee.dob && $scope.instEmployee.jobQuota)
                {
                    var prlYear = 59;
                    if ($scope.instEmployee.jobQuota != 'General'){
                        prlYear = 61;
                    }
                    //if($scope.instEmployee.jobQuota=='Others' || $scope.hrEmployeeInfo.jobQuota == 'FreedomFighter' || $scope.hrEmployeeInfo.jobQuota == 'others' || $scope.hrEmployeeInfo.jobQuota == 'other' || $scope.hrEmployeeInfo.jobQuota == 'OTHERS')
                    //{
                    //    prlYear = 59;
                    //}
                    var brDt = new Date($scope.instEmployee.dob);
                    brDt.setDate(brDt.getDate() - 1);
                    var prlDt = new Date(brDt.getFullYear() + prlYear, brDt.getMonth(), brDt.getDate());
                    var retirementDt = new Date(brDt.getFullYear() + prlYear + 1, brDt.getMonth(), brDt.getDate());
                    $scope.instEmployee.prlDate = prlDt;
                    $scope.instEmployee.retirementDate = retirementDt;
                    //$scope.instEmployee.lastWorkingDay = retirementDt;
                }else {console.log("BirthDate and Quota is needed for PRL Calculation");}
            };
            $scope.dateValidation = function () {
                if($scope.instEmployee.dob != null &&  $scope.instEmployee.joiningDate !=null){
                    var d1 = Date.parse($scope.instEmployee.dob);
                    var d2 = Date.parse($scope.instEmployee.joiningDate);
                    if (d1 >= d2) {
                        $scope.dateError = true;
                    } else {
                        $scope.dateError = false;
                    }
                }else if($scope.instEmployee.dob != null &&  $scope.instEmployee.joiningDate ==null){
                    $scope.dateError = false;
                }else{
                    $scope.dateError = true;
                }
            };


            $scope.preCondition = function(){
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.name', $scope.instEmployee.name, 'text', 100, 3, 'atozAndAtoZ.-', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.dob', $scope.instEmployee.dob, 'date', 10, 4, 'date', true, '','' ));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.email', $scope.instEmployee.email, 'email', 10, 4, 'instEmployee.email', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.contactNo', $scope.instEmployee.contactNo, 'number', 11, 11, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.joiningDate',DateUtils.convertLocaleDateToDMY( $scope.instEmployee.joiningDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.prlDate', DateUtils.convertLocaleDateToDMY($scope.instEmployee.prlDate), 'date', 5, 60, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('instEmployee.retirementDate',DateUtils.convertLocaleDateToDMY($scope.instEmployee.retirementDate), 'date', 5, 60, 'date', true, '',''));

                if($scope.preConditionStatus.indexOf(false) !== -1){
                    $scope.isPreCondition = false;
                }else {
                    $scope.isPreCondition = true;
                }
                return $scope.isPreCondition;
            }


            //$scope.updatePrlDate = function(){
            //    if($scope.instEmployee.dob && $scope.instEmployee.jobQuota){
            //        var prlYear = 59;
            //        if($scope.instEmployee.jobQuota=='FreedomFighter' || $scope.instEmployee.jobQuota == 'freedomfighter')
            //        {
            //            prlYear = 61;
            //        }
            //        var brDt = new Date($scope.instEmployee.dob);
            //        brDt.setDate(brDt.getDate() - 1);
            //        var prlDt = new Date(brDt.getFullYear() + prlYear, brDt.getMonth(), brDt.getDate());
            //        var retirementDt = new Date(brDt.getFullYear() + prlYear + 1, brDt.getMonth(), brDt.getDate());
            //        $scope.instEmployee.prlDate = prlDt;
            //        $scope.instEmployee.retirementDate = retirementDt;
            //    }else {
            //        console.log("BirthDate and Quota is needed for PRL Calculation");
            //    }
            //};

        }]);
