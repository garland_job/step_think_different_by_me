'use strict';

angular.module('stepApp')
    .controller('EduLevelController',
    ['$scope', '$state', '$modal', 'EduLevel', 'EduLevelSearch', 'ParseLinks',
     function ($scope, $state, $modal, EduLevel, EduLevelSearch, ParseLinks) {

        $scope.eduLevels = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EduLevel.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.eduLevels = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            EduLevelSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.eduLevels = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.eduLevel = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);


/*  eduLevel-dialog.controller.js*/

angular.module('stepApp').controller('EduLevelDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'EduLevel',
        function($scope, $rootScope, $stateParams, $state, entity, EduLevel) {

            $scope.eduLevel = entity;
            $scope.load = function(id) {
                EduLevel.get({id : id}, function(result) {
                    $scope.eduLevel = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:eduLevelUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('eduLevel', null, { reload: true });
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.eduLevel.id != null) {
                    EduLevel.update($scope.eduLevel, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.eduLevel.updated');
                } else {
                    /* if($scope.eduLevel.status == null){
                     $scope.eduLevel.status = true;
                     }*/
                    EduLevel.save($scope.eduLevel, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.eduLevel.created');
                }
            };

            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
            };
        }]);

/* eduLevel-detail.controller.js */

angular.module('stepApp')
    .controller('EduLevelDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'EduLevel',
            function ($scope, $rootScope, $stateParams, entity, EduLevel) {
                $scope.eduLevel = entity;
                $scope.load = function (id) {
                    EduLevel.get({id: id}, function(result) {
                        $scope.eduLevel = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:eduLevelUpdate', function(event, result) {
                    $scope.eduLevel = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/* eduLevel-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('EduLevelDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'EduLevel',
            function($scope, $rootScope, $modalInstance, entity, EduLevel) {

                $scope.eduLevel = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    EduLevel.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.eduLevel.deleted');
                        });
                };

            }]);
