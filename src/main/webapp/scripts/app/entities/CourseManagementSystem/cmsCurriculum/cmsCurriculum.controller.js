'use strict';

angular.module('stepApp').controller('CmsCurriculumController',
    ['$scope', '$state', '$modal', 'CmsCurriculumQuery', 'CmsCurriculum', 'CmsCurriculumSearch', 'ParseLinks',
        function ($scope, $state, $modal, CmsCurriculumQuery,CmsCurriculum, CmsCurriculumSearch, ParseLinks) {

        $scope.cmsCurriculums = [];
        $scope.tableParams = '';
        $scope.page = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.loadAll = function() {
        CmsCurriculumQuery.query({page: $scope.page, size: 2000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cmsCurriculums = result;
                //$scope.tableParams = new NgTableParams({}, { dataset: $scope.cmsCurriculums});
            });
        };


        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            CmsCurriculumSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.cmsCurriculums = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cmsCurriculum = {
                code: null,
                name: null,
                duration: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);

/*cmsCurriculum-dialog.controller.js*/

angular.module('stepApp').controller('CmsCurriculumDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'CmsCurriculum',
        function($scope, $stateParams, $state, entity, CmsCurriculum) {

            $scope.message = '';
            $scope.cmsCurriculum = entity;
            $scope.cmsCurriculum.duration_type= "Semester";
            $scope.cmsCurriculum.status=true;

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:cmsCurriculumUpdate', result);
                /*$modalInstance.close(result);*/
                $scope.isSaving = false;
                $state.go('courseInfo.curriculum',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };



            $scope.save = function () {
                console.log('comes to save ');
                $scope.isSaving = true;
                if ($scope.cmsCurriculum.id != null) {
                    CmsCurriculum.update($scope.cmsCurriculum, onSaveSuccess, onSaveError);
                } else {
                    CmsCurriculum.save($scope.cmsCurriculum, onSaveSuccess, onSaveError);
                }
            };


            $scope.uniqueError = false;

            /*$scope.checkCurriculumUniqueness = function () {
             console.log($scope.cmsCurriculum.code);
             console.log($scope.cmsCurriculum.name);
             if ($scope.cmsCurriculum.code != null && $scope.cmsCurriculum.name != null) {
             CmsCurriculumByCodeAndName.get({code: $scope.cmsCurriculum.code, name: $scope.cmsCurriculum.name}, function (result) {
             console.log('exist');
             $scope.uniqueError = true;
             },
             function (response) {
             if (response.status === 404) {
             console.log('not exist');
             $scope.uniqueError = false;
             }
             }
             );
             }
             };*/

            $scope.clear = function() {
                $scope.cmsCurriculum = null;
            };
        }]);

/*cmsCurriculum-detail.controller.js*/

angular.module('stepApp')
    .controller('CmsCurriculumDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'CmsCurriculum',
            function ($scope, $rootScope, $stateParams, entity, CmsCurriculum) {
                $scope.cmsCurriculum = entity;

                console.log($scope.cmsCurriculum);
                $scope.load = function (id) {
                    CmsCurriculum.get({id: id}, function(result) {
                        $scope.cmsCurriculum = result;
                    });
                };

                var unsubscribe = $rootScope.$on('stepApp:cmsCurriculumUpdate', function(event, result) {
                    $scope.cmsCurriculum = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);


/*cmsCurriculum-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('CmsCurriculumDeleteController',
        ['$scope', '$modalInstance', 'entity', 'CmsCurriculum',
            function($scope, $modalInstance, entity, CmsCurriculum) {

                $scope.cmsCurriculum = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    CmsCurriculum.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);


/*reports.controller.js*/
angular.module('stepApp')
    .controller('reportsController',
        ['$scope', '$sce', '$state', 'DataUtils', 'ParseLinks', 'JasperReport', 'GetJasperParamByJasperReport', 'GetAllJasperReportByModule',
            function ($scope, $sce, $state, DataUtils, ParseLinks, JasperReport, GetJasperParamByJasperReport, GetAllJasperReportByModule) {

                $scope.jasperReports = [];

                var loadModule = function () {
                    var moduleName = "cms";
                    GetAllJasperReportByModule.query({module: 'sms'}, function (result) {
                            console.log(result);
                            $scope.jasperReports = result;
                        },
                        function (response) {
                            console.log(response);
                        }
                    );
                }
                loadModule();
                //$scope.jasperReports = JasperReport.query({page: $scope.page, size: 20}, function (result, headers) {
                //    $scope.jasperReports = result;
                //
                //});

                $scope.calendar = {
                    opened: {},
                    dateFormat: 'yyyy-MM-dd',
                    dateOptions: {},
                    open: function ($event, which) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.calendar.opened[which] = true;
                    }
                };

                $scope.jsRport;
                $scope.rptParamDiv;

                $scope.rptBaseurl = "";
                $scope.url = "";


                $scope.reportChange = function () {

                    $scope.obj = $scope.jasperReport.id;
                    $scope.jasperReportParameters = [];
                    GetJasperParamByJasperReport.query({id: $scope.obj.id}, function (result) {
                        $scope.jasperReportParameters = result;
                    });
                }

                $scope.reportPreview = function () {

                    var parmavar = "";
                    var urlString = "";


                    $scope.obj = $scope.jasperReport.id;


                    console.log("object = " + $scope.obj);
                    console.log("object path =" + $scope.obj.path);


                    urlString = "http://202.4.121.77:9090/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports&reportUnit=%2Freports%2F";
                    urlString = urlString + $scope.obj.path + "&standAlone=true&j_username=user&j_password=jasperuser&decorate=no";

                    var parmavar = "";
                    angular.forEach($scope.jasperReportParameters, function (data) {

                        parmavar = parmavar + "&" + data.name + "=";

                        if (data.actiontype) {
                            if (data.type == 'combo') {
                                var array = $.map(data.actiontype, function (value, index) {
                                    return [value];
                                });
                                if (array.length > 0) {
                                    parmavar = parmavar + array[1];
                                }
                                parmavar = parmavar + "%25";
                            }

                            else if (data.type == 'text') {

                                if (data.actiontype.toString().trim() != "")
                                    parmavar = parmavar + data.actiontype;

                                parmavar = parmavar + "%25";
                            }

                            else if (data.type == 'month' || data.type == 'year') {

                                if (data.actiontype.toString().trim() != "")
                                    parmavar = parmavar + data.actiontype;
                            }
                        }
                        else {
                            parmavar = parmavar + "%25";
                        }
                    })

                    $scope.rptBaseurl = urlString + parmavar;
                    console.log($scope.rptBaseurl);
                    $scope.url = $sce.trustAsResourceUrl($scope.rptBaseurl);
                };

            }]);



/*cmsSemester-dialog.controller.js*/
angular.module('stepApp').controller('CmsSemesterDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'CmsSemester', 'CmsCurriculum', 'CmsSemesterByCodeAndName','FindActivecmsCurriculums',
        function ($scope, $stateParams, $state, entity, CmsSemester, CmsCurriculum, CmsSemesterByCodeAndName,FindActivecmsCurriculums) {

            $scope.message = '';
            $scope.cmsSemester = entity;
            $scope.cmsSemester.status=true;

            $scope.cmscurriculums = FindActivecmsCurriculums.query({size: 500});
            $scope.load = function (id) {
                CmsSemester.get({id: id}, function (result) {
                    $scope.cmsSemester = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:cmsSemesterUpdate', result);
                $scope.isSaving = false;
                $state.go('courseInfo.cmsSemester', {}, {reload: true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
                $state.go('courseInfo.cmsSemester', {}, {reload: true});
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.cmsSemester.id != null) {
                    CmsSemester.update($scope.cmsSemester, onSaveSuccess, onSaveError);
                } else {
                    CmsSemester.save($scope.cmsSemester, onSaveSuccess, onSaveError);
                }
            };

            $scope.lions = false;
            $scope.cranes = false;
            $scope.dSemester = true;
            $scope.uniqueError = false;

            $scope.checkSemesterUniqueness = function () {
                console.log($scope.cmsSemester.code);
                console.log($scope.cmsSemester.name);
                if ($scope.cmsSemester.code != null && $scope.cmsSemester.name != null) {
                    CmsSemesterByCodeAndName.get({code: $scope.cmsSemester.code, name: $scope.cmsSemester.name}, function (result) {
                            console.log('exist');
                            $scope.uniqueError = true;
                        },
                        function (response) {
                            if (response.status === 404) {
                                console.log('not exist');
                                $scope.uniqueError = false;
                            }
                        }
                    );
                }
            };


            $scope.enableCodeAndName = function () {
                $scope.dSemester = true;
                if ($scope.cmsSemester.cmsCurriculum.duration_type == 'Semester')
                    $scope.dSemester = false;
                else
                    $scope.dSemester = true;
            };

            $scope.clear = function () {
                $scope.cmsSemester = null;
            };
        }]);



/*cmsSemester-detail.controller.js*/
angular.module('stepApp')
    .controller('CmsSemesterDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'CmsSemester', 'CmsCurriculum',
            function ($scope, $rootScope, $stateParams, entity, CmsSemester, CmsCurriculum) {
                $scope.cmsSemester = entity;
                /*$scope.load = function (id) {
                 CmsSemester.get({id: id}, function(result) {
                 $scope.cmsSemester = result;
                 });
                 };*/
                var unsubscribe = $rootScope.$on('stepApp:cmsSemesterUpdate', function(event, result) {
                    $scope.cmsSemester = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*cmsSemester-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('CmsSemesterDeleteController',
        ['$scope', '$modalInstance', 'entity', 'CmsSemester',
            function($scope, $modalInstance, entity, CmsSemester) {

                $scope.cmsSemester = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    CmsSemester.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*cmsSemester.controller.js*/
angular.module('stepApp')
    .controller('CmsSemesterController',
        ['$scope', '$state', '$modal', 'CmsSemester', 'CmsSemesterSearch', 'ParseLinks',
            function ($scope, $state, $modal, CmsSemester, CmsSemesterSearch, ParseLinks) {

                $scope.cmsSemesters = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    CmsSemester.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.cmsSemesters = result;
                    });
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    CmsSemesterSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.cmsSemesters = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.cmsSemester = {
                        code: null,
                        name: null,
                        year: null,
                        duration: null,
                        description: null,
                        status: null,
                        id: null
                    };
                };
            }]);



/*cmsSubAssign-dialog.controller.js*/
angular.module('stepApp').controller('CmsSubAssignDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'CmsSubAssign', 'CmsCurriculum', 'CmsTrade', 'CmsSemester', 'CmsSyllabus', 'CmsSubject','FindActivecmsCurriculums','FindActivecmstrades','CmsTradesByCurriculum','GetCmsSubjectByGeneralStatus',
        function($scope, $stateParams, $state, entity, CmsSubAssign, CmsCurriculum, CmsTrade, CmsSemester,CmsSyllabus,CmsSubject,FindActivecmsCurriculums,FindActivecmstrades,CmsTradesByCurriculum,GetCmsSubjectByGeneralStatus) {

            $scope.cmsSubAssign = entity;
            $scope.cmsSubAssign.status=true;
            $scope.cmscurriculums = FindActivecmsCurriculums.query({size:2000});

            $scope.cmstrades = FindActivecmstrades.query({size:2000});
            $scope.cmssemesters = CmsSemester.query({size:2000});
            $scope.cmssyllabuss = CmsSyllabus.query({size:2000});
            $scope.cmssubjects = CmsSubject.query({size:2000});

            $scope.load = function(id) {
                CmsSubAssign.get({id : id}, function(result) {
                    $scope.cmsSubAssign = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:cmsSubAssignUpdate', result);
                /*$modalInstance.close(result);*/
                $scope.isSaving = false;
                $state.go('courseInfo.subAssign',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.filterActiveSyllabus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.cmsSubAssign.id != null) {
                    CmsSubAssign.update($scope.cmsSubAssign, onSaveSuccess, onSaveError);
                } else {
                    CmsSubAssign.save($scope.cmsSubAssign, onSaveSuccess, onSaveError);
                }
            };

            $scope.lions = false;
            $scope.cranes = false;
            $scope.dSemester = true;

            $scope.enableCodeAndName = function (curriculum) {
                // $scope.dSemester = true;

                GetCmsSubjectByGeneralStatus.query({curriculumnId:curriculum.id,status:true},function(result){
                    $scope.generalSubjects = result;
                });

                CmsTradesByCurriculum.query({id:curriculum.id}, function(result) {
                    console.log("curriculum id");
                    console.log(curriculum.id);
                    $scope.cmsTradesActive = result;
                });
                if ($scope.cmsSubAssign.cmsCurriculum.duration_type == 'Semester')
                    $scope.dSemester = false;
                else
                    $scope.dSemester = true;


            };




            $scope.clear = function() {
                $scope.cmsSubAssign=null;
            };



        }]);

angular.module('stepApp')
    .controller('CmsSubAssignDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'CmsSubAssign', 'CmsCurriculum', 'CmsTrade', 'CmsSemester', 'CmsSyllabus',
            function ($scope, $rootScope, $stateParams, entity, CmsSubAssign, CmsCurriculum, CmsTrade, CmsSemester, CmsSyllabus) {
                $scope.cmsSubAssign = entity;
                $scope.load = function (id) {
                    CmsSubAssign.get({id: id}, function(result) {
                        $scope.cmsSubAssign = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:cmsSubAssignUpdate', function(event, result) {
                    $scope.cmsSubAssign = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*cmsSubAssign-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('CmsSubAssignDeleteController',
        ['$scope', '$modalInstance', 'entity', 'CmsSubAssign',
            function($scope, $modalInstance, entity, CmsSubAssign) {

                $scope.cmsSubAssign = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    CmsSubAssign.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*cmsSubAssign.controller.js*/

angular.module('stepApp')
    .controller('CmsSubAssignController',
        ['$scope', '$state', '$modal', 'CmsSubAssign', 'CmsSubAssignSearch', 'ParseLinks',
            function ($scope, $state, $modal, CmsSubAssign, CmsSubAssignSearch, ParseLinks) {

                $scope.cmsSubAssigns = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    CmsSubAssign.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.cmsSubAssigns = result;
                    });
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    CmsSubAssignSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.cmsSubAssigns = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.cmsSubAssign = {
                        subject: null,
                        description: null,
                        examFee: null,
                        status: null,
                        id: null
                    };
                };
            }]);



/*cmsSubject-dialog.controller.js*/

angular.module('stepApp').controller('CmsSubjectDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'CmsSubject', 'CmsCurriculum', 'CmsSyllabus', 'CmsSubByNameAndSyllabusAndCode','CmsSyllabusByCurriculum',
        function($scope, $stateParams,$state, entity, CmsSubject, CmsCurriculum, CmsSyllabus, CmsSubByNameAndSyllabusAndCode,CmsSyllabusByCurriculum) {

            $scope.message = '';
            $scope.cmsSubject = entity;
            $scope.cmsSubject.status=true;
            $scope.cmsSubject.isGeneralSubject= true;
            $scope.cmsSyllabuss = [];

            $scope.cmscurriculums = CmsCurriculum.query({size:2000});
            //$scope.cmssyllabuss = CmsSyllabus.query({size:100});
            $scope.load = function(id) {
                CmsSubject.get({id : id}, function(result) {
                    $scope.cmsSubject = result;
                });
            };

            $scope.loadSyllabusByCurriculum = function(curriculum){
                console.log('&&&&&&&&&&&&&&&&&&&&&&&&');
                CmsSyllabusByCurriculum.query({id:curriculum.id}, function(result) {
                    console.log("curriculum id");
                    console.log(curriculum.id);
                    $scope.cmsSyllabuss = result;
                    console.log(result);
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:cmsSubjectUpdate', result);
                $scope.isSaving = false;
                $state.go('courseInfo.cmsSubject',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.filterActiveSyllabus = function () {
                return function (item) {
                    if (item.status == true)
                    {
                        return true;
                    }
                    return false;
                };
            };


            $scope.save = function () {
                $scope.isSaving = true;

                if($scope.cmsSubject.isGeneralSubject == true){
                    $scope.cmsSubject.subjectStatus = 'YES';
                }else{
                    $scope.cmsSubject.subjectStatus = 'NO';
                }

                if ($scope.cmsSubject.id != null) {
                    CmsSubject.update($scope.cmsSubject, onSaveSuccess, onSaveError);
                } else {
                    CmsSubByNameAndSyllabusAndCode.get({cmsSyllabusId: $scope.cmsSubject.cmsSyllabus.id,code: $scope.cmsSubject.code, name: $scope.cmsSubject.name}, function (cmsSubject) {
                            console.log('exist');
                            $scope.message = "The Syllabus, Subject Code and Subject name combination should be unique.";
                            console.log( $scope.message);
                        },
                        function (response) {
                            if (response.status === 404) {
                                console.log('not exist');
                                console.log($scope.cmsSubject);
                                CmsSubject.save($scope.cmsSubject, onSaveSuccess, onSaveError);
                                console.log($scope.cmsSubject);
                            }
                        }
                    );

                }
            };

            $scope.uniqueError = false;
            $scope.checkCompositeUniqueness = function()
            {
                if ($scope.cmsSubject.cmsSyllabus.id != null && $scope.cmsSubject.code != null && $scope.cmsSubject.name != null) {
                    CmsSubByNameAndSyllabusAndCode.get({
                            cmsSyllabusId: $scope.cmsSubject.cmsSyllabus.id,
                            code: $scope.cmsSubject.code,
                            name: $scope.cmsSubject.name
                        }, function (cmsSubject) {
                            console.log('exist');
                            $scope.uniqueError = true;
                        },
                        function (response) {
                            if (response.status === 404) {
                                console.log('not exist');
                                $scope.uniqueError = false;
                                //CmsSubject.save($scope.cmsSubject, onSaveSuccess, onSaveError);
                                //console.log($scope.cmsSubject);
                            }
                        }
                    );
                }
            };

            $scope.clear = function() {
                $scope.cmsSubject=null;
            };
        }]);

/*cmsSubject-detail.controller.js*/
angular.module('stepApp')
    .controller('CmsSubjectDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'CmsSubject', 'CmsCurriculum', 'CmsSyllabus',
            function ($scope, $rootScope, $stateParams, entity, CmsSubject, CmsCurriculum, CmsSyllabus) {
                $scope.cmsSubject = entity;
                $scope.load = function (id) {
                    CmsSubject.get({id: id}, function(result) {
                        $scope.cmsSubject = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:cmsSubjectUpdate', function(event, result) {
                    $scope.cmsSubject = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*cmsSubject-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('CmsSubjectDeleteController',
        ['$scope', '$modalInstance', 'entity', 'CmsSubject',
            function($scope, $modalInstance, entity, CmsSubject) {

                $scope.cmsSubject = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    CmsSubject.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*cmsSubject.controller.js*/
angular.module('stepApp')
    .controller('CmsSubjectController',
        ['$scope', '$state', '$modal', 'CmsSubject', 'CmsSubjectSearch', 'ParseLinks',
            function ($scope, $state, $modal, CmsSubject, CmsSubjectSearch, ParseLinks) {

                $scope.cmsSubjects = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    CmsSubject.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.cmsSubjects = result;
                    });
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    CmsSubjectSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.cmsSubjects = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.cmsSubject = {
                        code: null,
                        name: null,
                        theoryCredHr: null,
                        pracCredHr: null,
                        totalCredHr: null,
                        theoryCon: null,
                        theoryFinal: null,
                        pracCon: null,
                        pracFinal: null,
                        totalMarks: null,
                        description: null,
                        status: null,
                        id: null
                    };
                };
            }]);

/*cmsSyllabus-dialog.controller.js*/

angular.module('stepApp').controller('CmsSyllabusDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'CmsSyllabus', 'CmsCurriculum', 'CmsSyllabusByNameAndVersion',
        function($scope, $rootScope, $stateParams, $state, entity, CmsSyllabus, CmsCurriculum, CmsSyllabusByNameAndVersion) {

            $scope.message = '';
            $scope.content = '';

            $scope.cmsSyllabus = {};
            $scope.cmsSyllabus.status=true;
            $scope.preview = false;

            $scope.cmscurriculums = CmsCurriculum.query({size:100});
            $scope.load = function() {
                CmsSyllabus.get({id : $stateParams.id}, function(result) {
                    $scope.cmsSyllabus = result;
                    $scope.showAlert = false;
                    if ($scope.cmsSyllabus.syllabus) {
                        console.log($scope.cmsSyllabus);
                        var aType = $scope.cmsSyllabus.syllabusContentType;
                        $scope.preview = false;
                        if(aType.indexOf("image") >= 0 || aType.indexOf("pdf") >= 0) {
                            $scope.preview = true;
                        }
                        console.log($scope.preview);
                        var blob = $rootScope.b64toBlob($scope.cmsSyllabus.syllabus, 'image/*,application/pdf');
                        $scope.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    }
                });
            };

            $scope.load();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:cmsSyllabusUpdate', result);
                /*$modalInstance.close(result);*/
                $scope.isSaving = false;

                $state.go('courseInfo.cmsSyllabus',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };



            $scope.setSyllabus = function ($file, cmsSyllabus) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            cmsSyllabus.syllabus = base64Data;
                            cmsSyllabus.syllabusContentType = $file.type;
                            $scope.fileAdded = "File added";
                            $scope.fileName = $file.name;
                            console.log(cmsSyllabus.syllabusContentType);

                            var aType = cmsSyllabus.syllabusContentType;

                            $scope.preview = false;
                            if(aType.indexOf("image") >= 0 || aType.indexOf("pdf") >= 0) {
                                $scope.preview = true;
                            }

                            var blob = $rootScope.b64toBlob(cmsSyllabus.syllabus , cmsSyllabus.syllabusContentType);
                            $scope.content = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };


            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.cmsSyllabus.id != null) {
                    CmsSyllabus.update($scope.cmsSyllabus, onSaveSuccess, onSaveError);
                }
                else {
                    //CmsSyllabus.save($scope.cmsSyllabus, onSaveSuccess, onSaveError);
                    CmsSyllabusByNameAndVersion.get({cmsCurriculumId: $scope.cmsSyllabus.cmsCurriculum.id, name: $scope.cmsSyllabus.name, version: $scope.cmsSyllabus.version}, function (cmsSyllabus) {
                            console.log('exist');
                            $scope.message = "The Curriculum, Name and Version combination should be unique.";
                            console.log( $scope.message);
                        },
                        function (response) {
                            if (response.status === 404) {
                                console.log('not exist');
                                CmsSyllabus.save($scope.cmsSyllabus, onSaveSuccess, onSaveError);
                                console.log($scope.cmsSyllabus);
                            }
                        }
                    );
                }
            };





            $scope.uniqueError = false;
            $scope.checkSyllabusUniqueness = function()
            {
                if ($scope.cmsSyllabus.cmsCurriculum.id != null && $scope.cmsSyllabus.name != null && $scope.cmsSyllabus.version != null) {
                    CmsSyllabusByNameAndVersion.get({
                            cmsCurriculumId: $scope.cmsSyllabus.cmsCurriculum.id,
                            name: $scope.cmsSyllabus.name,
                            version: $scope.cmsSyllabus.version
                        }, function (cmsSyllabus) {
                            console.log('exist');
                            $scope.uniqueError = true;
                        },
                        function (response) {
                            if (response.status === 404) {
                                console.log('not exist');
                                $scope.uniqueError = false;
                                //CmsSubject.save($scope.cmsSubject, onSaveSuccess, onSaveError);
                                //console.log($scope.cmsSubject);
                            }
                        }
                    );
                }
            };


            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };


            $scope.clear = function() {
                $scope.cmsSyllabus=null;
            };
        }]);



/*cmsSyllabus-detail.controller.js*/
angular.module('stepApp')
    .controller('CmsSyllabusDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'CmsSyllabus', 'CmsCurriculum',
            function ($scope, $rootScope, $stateParams, entity, CmsSyllabus, CmsCurriculum) {

                $scope.cmsSyllabus = {};
                $scope.content = '';

                $scope.load = function () {
                    CmsSyllabus.get({id: $stateParams.id}, function(result) {
                        $scope.cmsSyllabus = result;
                        if ($scope.cmsSyllabus.syllabus) {
                            var blob = $rootScope.b64toBlob($scope.cmsSyllabus.syllabus, 'application/pdf');
                            $scope.content = (window.URL || window.webkitURL).createObjectURL(blob);
                        }
                    });
                };

                $scope.load();

            }]);


/*cmsSyllabus-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('CmsSyllabusDeleteController',
        ['$scope', '$modalInstance', 'entity', 'CmsSyllabus',
            function($scope, $modalInstance, entity, CmsSyllabus) {

                $scope.cmsSyllabus = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };


                $scope.confirmDelete = function (id) {
                    CmsSyllabus.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*cmsSyllabus.controller.js*/
angular.module('stepApp')
    .controller('CmsSyllabusController',
        ['$scope', '$state', '$modal', 'CmsSyllabus', 'syllabusList', 'CmsSyllabusSearch',
            function ($scope, $state, $modal, CmsSyllabus,syllabusList,CmsSyllabusSearch) {

                $scope.cmsSyllabuss = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    syllabusList.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.cmsSyllabuss = result;
                    });
                };

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.viewQrCode = function (id) {

                    CmsSyllabus.get({id: id}, function(result) {
                        $scope.cmsSyllabuss = result;
                        //console.log(JSON.stringify(result));
                        console.log($scope.cmsSyllabuss.syllabusContent);
                        //console.log($scope.cmsSyllabuss.cmsS);
                        $('#viewQRCode').modal('show');
                    });
                    //$scope.qrCodeGateway.qrCodeImageLink = "qr_code_files/QR#1452932869160.png";
                    //$('#viewQRCode').modal('show');

                };

                $scope.search = function () {
                    CmsSyllabusSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.cmsSyllabuss = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.cmsSyllabus = {
                        version: null,
                        name: null,
                        description: null,
                        status: null,
                        id: null,
                        syllabusContentType: null,
                        syllabusContent: null
                    };
                };
            }]);

/*cmsTrade-dialog.controller.js*/
angular.module('stepApp').controller('CmsTradeDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'CmsTrade', 'CmsCurriculum', 'CmsTradeByCode','FindActivecmsCurriculums',
        function($scope, $stateParams, $state, entity, CmsTrade, CmsCurriculum, CmsTradeByCode,FindActivecmsCurriculums) {

            $scope.message = '';
            $scope.cmsTrade = entity;
            console.log(entity);
            $scope.cmsTrade.status=true;

            $scope.cmscurriculums = FindActivecmsCurriculums.query({size:100});
            $scope.load = function(id) {
                CmsTrade.get({id : id}, function(result) {
                    $scope.cmsTrade = result;
                });
            };
            console.log($scope.cmsTrade);
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:cmsTradeUpdate', result);
                /* $modalInstance.close(result);*/
                $scope.isSaving = false;
                $state.go('courseInfo.cmsTrade',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.cmsTrade.id != null) {
                    CmsTrade.update($scope.cmsTrade, onSaveSuccess, onSaveError);
                } else {

                    CmsTradeByCode.get({code: $scope.cmsTrade.code}, function (cmsTrade) {

                            $scope.message = "This Code is already existed.";
                        },
                        function (response) {
                            if (response.status === 404) {
                                console.log('not exist');
                                CmsTrade.save($scope.cmsTrade, onSaveSuccess, onSaveError);
                                console.log($scope.cmsTrade);
                            }
                        }
                    );
                }
            };

            $scope.clear = function() {
                $scope.cmsTrade=null;
            };

            $scope.lions = false;
            $scope.cranes = false;
            $scope.dSemester = true;

            $scope.enableSemester = function () {
                /*
                 $scope.dSemester = true;
                 */
                if ($scope.cmsTrade.cmsCurriculum.duration_type == 'Year')
                    $scope.dSemester = false;
                else if ($scope.cmsTrade.cmsCurriculum.duration_type == 'Semester')
                    $scope.dSemester = false;
                else if ($scope.cmsTrade.cmsCurriculum.duration_type == 'Month')
                    $scope.dSemester = false;


            };


            /* $scope.enableSemester = function () {
             $scope.dSemester = true;
             if ($scope.cmsSubAssign.cmsCurriculum.duration_type == 'Semester')
             $scope.dSemester = false;
             else
             $scope.dSemester = true;




             };*/

        }]);


/*cmsTrade-detail.controller.js*/
angular.module('stepApp')
    .controller('CmsTradeDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'CmsTrade', 'CmsCurriculum',
            function ($scope, $rootScope, $stateParams, entity, CmsTrade, CmsCurriculum) {
                $scope.cmsTrade = entity;
                $scope.load = function (id) {
                    CmsTrade.get({id: id}, function(result) {
                        $scope.cmsTrade = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:cmsTradeUpdate', function(event, result) {
                    $scope.cmsTrade = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/*cmsTrade-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('CmsTradeDeleteController',
        ['$scope', '$modalInstance', 'entity', 'CmsTrade',
            function($scope, $modalInstance, entity, CmsTrade) {

                $scope.cmsTrade = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    CmsTrade.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);


/*cmsTrade.controller.js*/
angular.module('stepApp')
    .controller('CmsTradeController',
        ['$scope', '$state', '$modal', 'CmsTrade', 'CmsTradeSearch', 'ParseLinks',
            function ($scope, $state, $modal, CmsTrade, CmsTradeSearch, ParseLinks) {

                $scope.cmsTrades = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    CmsTrade.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.cmsTrades = result;
                    });
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    CmsTradeSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.cmsTrades = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.cmsTrade = {
                        code: null,
                        name: null,
                        description: null,
                        status: null,
                        id: null
                    };
                };
            }]);


/*course-info-dashboard.controller.js*/

angular.module('stepApp')
    .controller('CourseDashboardController',
        ['$scope', '$state', 'DataUtils',
            function ($scope, $state, DataUtils) {

                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;
            }]);



/*course-info.controller.js*/
angular.module('stepApp')
    .controller('CourseInfoController',
        ['$scope', '$state', '$modal', 'DataUtils',
            function ($scope, $state, $modal, DataUtils) {

                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;

                $scope.lineObject = {
                    "type": "ComboChart",
                    "title": "Line Chart",
                    "displayed": false,
                    "data": {
                        "cols": [
                            {
                                "id": "durationType",
                                "label": "Duration Type",
                                "type": "string",
                                "p": {}
                            },
                            {
                                "id": "semester-id",
                                "label": "Semester",
                                "type": "number",
                                "p": {}
                            },
                            {
                                "id": "month-id",
                                "label": "Month",
                                "type": "number",
                                "p": {}
                            },
                            {
                                "id": "year-id",
                                "label": "Year",
                                "type": "number",
                                "p": {}
                            }
                        ],
                        "rows": [
                            {
                                "c": [
                                    {
                                        "v": "Semester"
                                    },
                                    {
                                        "v": 19,
                                        "f": "42 items"
                                    },
                                    {
                                        "v": 12,
                                        "f": "Ony 12 items"
                                    },
                                    {
                                        "v": 7,
                                        "f": "7 servers"
                                    }
                                ]
                            },
                            {
                                "c": [
                                    {
                                        "v": "Month"
                                    },
                                    {
                                        "v": 13
                                    },
                                    {
                                        "v": 1,
                                        "f": "1 unit (Out of stock this month)"
                                    },
                                    {
                                        "v": 12
                                    }
                                ]
                            },
                            {
                                "c": [
                                    {
                                        "v": "Year"
                                    },
                                    {
                                        "v": 24
                                    },
                                    {
                                        "v": 5
                                    },
                                    {
                                        "v": 11
                                    }
                                ]
                            }
                        ]
                    },
                    "options": {
                        "title": "Curriculum Summery",
                        "isStacked": "true",
                        "fill": 20,
                        "displayExactValues": true,
                        "vAxis": {
                            "title": "Sales unit",
                            "gridlines": {
                                "count": 10
                            }
                        },
                        "hAxis": {
                            "title": "Date"
                        }
                    },
                    "formatters": {}
                }

                $scope.pieObject = {
                    "type": "PieChart",
                    "title": "Pie Chart",
                    "displayed": false,
                    "data": {
                        "cols": [
                            {
                                "id": "month",
                                "label": "Month",
                                "type": "string",
                                "p": {}
                            },
                            {
                                "id": "laptop-id",
                                "label": "Laptop",
                                "type": "number",
                                "p": {}
                            },
                            {
                                "id": "desktop-id",
                                "label": "Desktop",
                                "type": "number",
                                "p": {}
                            },
                            {
                                "id": "server-id",
                                "label": "Server",
                                "type": "number",
                                "p": {}
                            },
                            {
                                "id": "cost-id",
                                "label": "Shipping",
                                "type": "number"
                            }
                        ],
                        "rows": [
                            {
                                "c": [
                                    {
                                        "v": "Semester-type"
                                    },
                                    {
                                        "v": 19,
                                        "f": "42 items"
                                    },
                                    {
                                        "v": 12,
                                        "f": "Ony 12 items"
                                    },
                                    {
                                        "v": 7,
                                        "f": "7 servers"
                                    },
                                    {
                                        "v": 4
                                    }
                                ]
                            },
                            {
                                "c": [
                                    {
                                        "v": "Yearly"
                                    },
                                    {
                                        "v": 13
                                    },
                                    {
                                        "v": 1,
                                        "f": "1 unit (Out of stock this month)"
                                    },
                                    {
                                        "v": 12
                                    },
                                    {
                                        "v": 2
                                    }
                                ]
                            },
                            {
                                "c": [
                                    {
                                        "v": "Monthly"
                                    },
                                    {
                                        "v": 24
                                    },
                                    {
                                        "v": 5
                                    },
                                    {
                                        "v": 11
                                    },
                                    {
                                        "v": 6
                                    }
                                ]
                            }
                        ]
                    },
                    "options": {
                        "title": "Curriculum Summery",
                        "isStacked": "true",
                        "fill": 20,
                        "displayExactValues": true,
                        "vAxis": {
                            "title": "Curriculum unit",
                            "gridlines": {
                                "count": 10
                            }
                        },
                        "hAxis": {
                            "title": "Date"
                        }
                    },
                    "formatters": {}
                }
            }]);















