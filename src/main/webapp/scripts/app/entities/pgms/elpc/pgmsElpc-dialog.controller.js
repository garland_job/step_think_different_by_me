/*
'use strict';

angular.module('stepApp').controller('PgmsElpcDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'GetAllHrEmployeeInfo', 'PgmsElpcAllowanceInfo','PrlSalaryStructureInfoByEmployeeId','GetPfmsEmpMembershipFormByEmployee','PgmsElpc',
    function($scope, $stateParams, $state, entity, GetAllHrEmployeeInfo, PgmsElpcAllowanceInfo,PrlSalaryStructureInfoByEmployeeId,GetPfmsEmpMembershipFormByEmployee,PgmsElpc) {

        $scope.pgmsElpc = entity;

        GetAllHrEmployeeInfo.query(function(result){
            $scope.hremployeeInfos = result;
        });

        $scope.allowanceInfoList = [];
        $scope.approveStatus = 0;
        var number = 0;
        var demoDate = "2016-04-20";
        var demoString = "Demo Data";

        $scope.approveStatus = function(status){
            console.log('*****************');
            $scope.pgmsElpc.approveStatus = status;
            elpcSave();
        }

        $scope.loadAll = function(id) {
            if($stateParams.id != null)
            {

                 PgmsElpcAllowanceInfo.get({empInfoId:$scope.pgmsElpc.hrEmployeeInfo.id}, function(result) {
                     console.log("Edit Structure Id:"+JSON.stringify(result));
                     $scope.allowanceInfoList = result;
                 });

            }
        };
        $scope.loadAll();

        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        $scope.employeeInfo = function(empId){
            // PayScale information
            PrlSalaryStructureInfoByEmployeeId.get({employeeId:empId.id},function(result){
                $scope.pgmsElpc.lastRcvPayscale = result.payscaleBasicInfo.basicAmount;
            });

           $scope.pgmsElpc.empName = empId.fullName;
           $scope.pgmsElpc.empCode = empId.id;
           $scope.pgmsElpc.instCode = empId.departmentInfo.departmentInfo.departmentCode;
           $scope.pgmsElpc.instName = empId.departmentInfo.departmentInfo.departmentName;
           $scope.pgmsElpc.desigId = empId.designationInfo.designationInfo.id;
           $scope.pgmsElpc.designation = empId.designationInfo.designationInfo.designationName;
           $scope.pgmsElpc.dateOfBirth = empId.birthDate;
           $scope.pgmsElpc.joinDate = empId.dateOfJoining;
           $scope.pgmsElpc.beginDateOfRetiremnt = demoDate;
           $scope.pgmsElpc.retirementDate = empId.retirementDate;
           //$scope.pgmsElpc.lastRcvPayscale = number;
           $scope.pgmsElpc.incrsDtOfYrlyPayment = number;
           $scope.pgmsElpc.gainingLeave = number;
           $scope.pgmsElpc.leaveType = demoString;
           $scope.pgmsElpc.leaveTotal = number;
           $scope.pgmsElpc.mainPayment = number;
           $scope.pgmsElpc.incrMonRateLeaving = number;
           $scope.pgmsElpc.specialPayment = number;
           $scope.pgmsElpc.specialAllowance = number;
           $scope.pgmsElpc.houserentAl = number;
           $scope.pgmsElpc.treatmentAl = number;
           $scope.pgmsElpc.dearnessAl = number;
           $scope.pgmsElpc.travellingAl = number;
           $scope.pgmsElpc.laundryAl = number;
           $scope.pgmsElpc.personalAl = number;
           $scope.pgmsElpc.technicalAl = number;
           $scope.pgmsElpc.hospitalityAl = number;
           $scope.pgmsElpc.tiffinAl = number;
           $scope.pgmsElpc.advOfMakingHouse = number;
           $scope.pgmsElpc.vechileStatus = number;
           $scope.pgmsElpc.advTravAl = number;
           $scope.pgmsElpc.advSalary = number;
           $scope.pgmsElpc.houseRent = number;
           $scope.pgmsElpc.carRent = number;
           $scope.pgmsElpc.gasBill = number;
           $scope.pgmsElpc.santryWaterTax = demoString;

           PgmsElpcAllowanceInfo.get({empInfoId:empId.id}, function(result) {
              $scope.allowanceInfoList = result;
               angular.forEach($scope.allowanceInfoList,function(allowanceInfo){
                   console.log('---------------------------');
                   console.log(allowanceInfo);
                   if(allowanceInfo.allowDeducInfo.name == 'Dearness Allowance'){
                       $scope.pgmsElpc.dearnessAl = allowanceInfo.allowDeducValue;
                   }
                   if(allowanceInfo.allowDeducInfo.name == 'Washing Allowance'){
                       $scope.pgmsElpc.laundryAl = allowanceInfo.allowDeducValue;
                   }
                   if(allowanceInfo.allowDeducInfo.name == 'House Rent'){
                       $scope.pgmsElpc.houserentAl = allowanceInfo.allowDeducValue;

                   }if(allowanceInfo.allowDeducInfo.name == 'Medical Allowance'){
                       $scope.pgmsElpc.treatmentAl = allowanceInfo.allowDeducValue;
                   }
                   if(allowanceInfo.allowDeducInfo.name == 'Travel Allowance'){
                       $scope.pgmsElpc.travellingAl = allowanceInfo.allowDeducValue;
                   }
                   if(allowanceInfo.allowDeducInfo.name == 'Personal Allowance'){
                       $scope.pgmsElpc.personalAl = allowanceInfo.allowDeducValue;
                   }
                   if(allowanceInfo.allowDeducInfo.name == 'Technical Allowance'){
                       $scope.pgmsElpc.technicalAl = allowanceInfo.allowDeducValue;
                   }
                   if(allowanceInfo.allowDeducInfo.name == 'Entertainment'){
                       $scope.pgmsElpc.hospitalityAl = allowanceInfo.allowDeducValue;
                   }

               });
           });

            GetPfmsEmpMembershipFormByEmployee.get({employeeInfoId:empId.id},function(data){
                console.log('&&&&&&&&&&&&&&&&&&&&&&&7');
                console.log(data);
                if(data==null){
                    $scope.pgmsElpc.bankAcc = demoString;
                }else {
                    $scope.pgmsElpc.bankAcc = data.accountNo;
                }
            });
           $scope.pgmsElpc.bankAcc = demoString;
           $scope.pgmsElpc.accBookNo = demoString;
           $scope.pgmsElpc.bookPageNo = number;
           $scope.pgmsElpc.bankInterest = number;
           $scope.pgmsElpc.monlyDepRateFrSalary = number;
           $scope.pgmsElpc.expectedDeposition = number;
           $scope.pgmsElpc.expectedDeposition = number;
           $scope.pgmsElpc.accDate = demoDate;
           $scope.pgmsElpc.appNo = demoString;

        }

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:pgmsElpcUpdate', result);
            $scope.isSaving = false;
            $state.go("pgmsElpc");
        };
        var onSaveError = function (result) {
             $scope.isSaving = false;
        };

        $scope.save = function () {
            console.log('##########');
            $scope.pgmsElpc.approveStatus = 1;
            console.log($scope.approveStatus);
            if ($scope.pgmsElpc.id != null) {
                PgmsElpc.update($scope.pgmsElpc, onSaveFinished,onSaveError);
            } else {
                PgmsElpc.save($scope.pgmsElpc, onSaveFinished,onSaveError);
            }
        };

        $scope.rejectSave = function () {
            console.log('##########');
            $scope.pgmsElpc.approveStatus = -1;
            console.log($scope.approveStatus);
            if ($scope.pgmsElpc.id != null) {
                PgmsElpc.update($scope.pgmsElpc, onSaveFinished,onSaveError);
            } else {
                PgmsElpc.save($scope.pgmsElpc, onSaveFinished,onSaveError);
            }
        };

        $scope.clear = function() {
            $state.dismiss('cancel');
        };
}]);
*/
