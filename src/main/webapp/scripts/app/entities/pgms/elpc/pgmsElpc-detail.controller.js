/*
'use strict';

angular.module('stepApp')
    .controller('PgmsElpcDetailController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsElpc', 'HrEmployeeInfo','PgmsElpcAllowanceInfo',
    function ($scope, $rootScope, $stateParams, entity, PgmsElpc, HrEmployeeInfo, PgmsElpcAllowanceInfo) {
        $scope.pgmsElpc = entity;
        $scope.allowanceInfoList = [];
        $scope.load = function (id) {
            PgmsElpc.get({id: $stateParams.id}, function(result) {
                $scope.pgmsElpc = result;
                PgmsElpcAllowanceInfo.get({empInfoId:$scope.pgmsElpc.hrEmployeeInfo.id}, function(result) {
                    $scope.allowanceInfoList = result;
                });
            });
        };

        $scope.load();

        var unsubscribe = $rootScope.$on('stepApp:pgmsElpcUpdate', function(event, result) {
            $scope.pgmsElpc = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
*/
