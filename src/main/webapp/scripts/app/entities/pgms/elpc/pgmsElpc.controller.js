'use strict';

angular.module('stepApp')
    .controller('PgmsElpcController',
    ['$scope', 'PgmsElpc', 'PgmsElpcSearch', 'ParseLinks',
    function ($scope, PgmsElpc, PgmsElpcSearch, ParseLinks) {
        $scope.pgmsElpcs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsElpc.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsElpcs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsElpc.get({id: id}, function(result) {
                $scope.pgmsElpc = result;
                $('#deletePgmsElpcConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsElpc.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsElpcConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsElpcSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsElpcs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsElpc = {
                empCode: null,
                instCode: null,
                empName: null,
                instName: null,
                desigId: null,
                designation: null,
                dateOfBirth: null,
                joinDate: null,
                beginDateOfRetiremnt: null,
                retirementDate: null,
                lastRcvPayscale: null,
                incrsDtOfYrlyPayment: null,
                gainingLeave: null,
                leaveType: null,
                leaveTotal: null,
                appRetirementDate: null,
                mainPayment: null,
                incrMonRateLeaving: null,
                specialPayment: null,
                specialAllowance: null,
                houserentAl: null,
                treatmentAl: null,
                dearnessAl: null,
                travellingAl: null,
                laundryAl: null,
                personalAl: null,
                technicalAl: null,
                hospitalityAl: null,
                tiffinAl: null,
                advOfMakingHouse: null,
                vechileStatus: null,
                advTravAl: null,
                advSalary: null,
                houseRent: null,
                carRent: null,
                gasBill: null,
                santryWaterTax: null,
                bankAcc: null,
                accBookNo: null,
                bookPageNo: null,
                bankInterest: null,
                monlyDepRateFrSalary: null,
                expectedDeposition: null,
                accDate: null,
                appNo: null,
                appDate: null,
                appType: null,
                appComments: null,
                aprvStatus: 'Pending',
                aprvDate: null,
                aprvComment: null,
                aprvBy: null,
                notificationStatus: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    }]);
/*pgmsElpc-dialog.controller.js*/

angular.module('stepApp').controller('PgmsElpcDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'GetAllHrEmployeeInfo', 'PgmsElpcAllowanceInfo','PrlSalaryStructureInfoByEmployeeId','GetPfmsEmpMembershipFormByEmployee','PgmsElpc', 'PgmsElpcYearlyPayment','PfmsRegisterListByEmployee','PfmsEmpMembershipListByEmployee',
        function($scope, $stateParams, $state, entity, GetAllHrEmployeeInfo, PgmsElpcAllowanceInfo,PrlSalaryStructureInfoByEmployeeId,GetPfmsEmpMembershipFormByEmployee,PgmsElpc,PgmsElpcYearlyPayment,PfmsRegisterListByEmployee,PfmsEmpMembershipListByEmployee) {

            $scope.pgmsElpc = entity;

            GetAllHrEmployeeInfo.query(function(result){
                $scope.hremployeeInfos = result;
            });

            $scope.allowanceInfoList = [];
            $scope.approveStatus = 0;
            var number = 0;
            var demoDate = "2016-04-20";
            var demoString = "Demo Data";

            $scope.approveStatus = function(status){
                console.log('*****************');
                $scope.pgmsElpc.approveStatus = status;
                elpcSave();
            }

            $scope.loadAll = function(id) {
                if($stateParams.id != null)
                {

                    PgmsElpcAllowanceInfo.get({empInfoId:$scope.pgmsElpc.hrEmployeeInfo.id}, function(result) {
                        console.log("Edit Structure Id:"+JSON.stringify(result));
                        $scope.allowanceInfoList = result;
                    });

                }
            };
            $scope.loadAll();

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.employeeInfo = function(empId){
                // PayScale information
                PrlSalaryStructureInfoByEmployeeId.get({employeeId:empId.id},function(result){
                    $scope.pgmsElpc.lastRcvPayscale = result.payscaleBasicInfo.basicAmount;
                });

                PgmsElpcYearlyPayment.get({employeeId:empId.id},function(result){
                    $scope.pgmsElpc.incrsDtOfYrlyPayment=result.hrEmpIncrementInfo.incrementDate;
                    console.log('--=====Yearly Increment payable date ======--');
                    console.log($scope.pgmsElpc.incrsDtOfYrlyPayment);
                });

                $scope.pgmsElpc.empName = empId.fullName;
                $scope.pgmsElpc.empCode = empId.id;
                $scope.pgmsElpc.instCode = empId.departmentInfo.departmentInfo.departmentCode;
                $scope.pgmsElpc.instName = empId.departmentInfo.departmentInfo.departmentName;
                $scope.pgmsElpc.desigId = empId.designationInfo.designationInfo.id;
                $scope.pgmsElpc.designation = empId.designationInfo.designationInfo.designationName;
                $scope.pgmsElpc.dateOfBirth = empId.birthDate;
                $scope.pgmsElpc.joinDate = empId.dateOfJoining;
                $scope.pgmsElpc.beginDateOfRetiremnt = empId.retirementDate;
                $scope.pgmsElpc.retirementDate = empId.retirementDate;
               // $scope.pgmsElpc.lastRcvPayscale = number;
               // $scope.pgmsElpc.incrsDtOfYrlyPayment = empId.incrementAmount;
                $scope.pgmsElpc.gainingLeave = number;
                $scope.pgmsElpc.leaveType = demoString;
                $scope.pgmsElpc.leaveTotal = number;
                $scope.pgmsElpc.mainPayment = number;
                $scope.pgmsElpc.incrMonRateLeaving = number;
                $scope.pgmsElpc.specialPayment = number;
                $scope.pgmsElpc.specialAllowance = number;
               // $scope.pgmsElpc.houserentAl = number;
               // $scope.pgmsElpc.treatmentAl = number;
               // $scope.pgmsElpc.dearnessAl = number;
               // $scope.pgmsElpc.travellingAl = number;
               // $scope.pgmsElpc.laundryAl = number;
               // $scope.pgmsElpc.personalAl = number;
               // $scope.pgmsElpc.technicalAl = number;
               // $scope.pgmsElpc.hospitalityAl = number;
                $scope.pgmsElpc.tiffinAl = number;
                $scope.pgmsElpc.advOfMakingHouse = number;
                $scope.pgmsElpc.vechileStatus = number;
                $scope.pgmsElpc.advTravAl = number;
                $scope.pgmsElpc.advSalary = number;
                $scope.pgmsElpc.houseRent = number;
                $scope.pgmsElpc.carRent = number;
                $scope.pgmsElpc.gasBill = number;
                $scope.pgmsElpc.santryWaterTax = demoString;

                PgmsElpcAllowanceInfo.get({empInfoId:empId.id}, function(result) {
                    $scope.allowanceInfoList = result;
                    angular.forEach($scope.allowanceInfoList,function(allowanceInfo){
                        console.log('---------------------------');
                        console.log(allowanceInfo);
                        if(allowanceInfo.allowDeducInfo.name == 'Dearness Allowance'){
                            $scope.pgmsElpc.dearnessAl = allowanceInfo.allowDeducValue;
                        }
                        if(allowanceInfo.allowDeducInfo.name == 'Washing Allowance'){
                            $scope.pgmsElpc.laundryAl = allowanceInfo.allowDeducValue;
                        }
                        if(allowanceInfo.allowDeducInfo.name == 'House Rent'){
                            $scope.pgmsElpc.houserentAl = allowanceInfo.allowDeducValue;

                        }if(allowanceInfo.allowDeducInfo.name == 'Medical Allowance'){
                            $scope.pgmsElpc.treatmentAl = allowanceInfo.allowDeducValue;
                        }
                        if(allowanceInfo.allowDeducInfo.name == 'Travel Allowance'){
                            $scope.pgmsElpc.travellingAl = allowanceInfo.allowDeducValue;
                        }
                        if(allowanceInfo.allowDeducInfo.name == 'Personal Allowance'){
                            $scope.pgmsElpc.personalAl = allowanceInfo.allowDeducValue;
                        }
                        if(allowanceInfo.allowDeducInfo.name == 'Technical Allowance'){
                            $scope.pgmsElpc.technicalAl = allowanceInfo.allowDeducValue;
                        }
                        if(allowanceInfo.allowDeducInfo.name == 'Entertainment'){
                            $scope.pgmsElpc.hospitalityAl = allowanceInfo.allowDeducValue;
                        }

                    });
                });

                GetPfmsEmpMembershipFormByEmployee.get({employeeInfoId:empId.id},function(data){
                    console.log('&&&&&&&&&&&&&&&&&&&&&&&7');
                    console.log(data);
                    if(data==null){
                        $scope.pgmsElpc.bankAcc = demoString;
                    }else {
                        $scope.pgmsElpc.bankAcc = data.accountNo;
                    }
                });

                PfmsRegisterListByEmployee.get({employeeInfoId:empId.id},function(result){
                    console.log('=====Provident Fund Info=======');
                    console.log(result);
                    $scope.pgmsElpc.accBookNo = result.checkNo;
                    $scope.pgmsElpc.bookPageNo = result.checkTokenNo;
                });

                PfmsEmpMembershipListByEmployee.get({employeeInfoId:empId.id},function(data){
                    console.log('=====MemberShip List Fund Info=======');
                    console.log(data);
                    $scope.pgmsElpc.bankInterest = data.curOwnContributeInt;
                });

               // $scope.pgmsElpc.bankAcc = demoString;
               // $scope.pgmsElpc.accBookNo = demoString;
               // $scope.pgmsElpc.bookPageNo = number;
                //$scope.pgmsElpc.bankInterest = number;
                $scope.pgmsElpc.monlyDepRateFrSalary = number;
                $scope.pgmsElpc.expectedDeposition = number;
                $scope.pgmsElpc.expectedDeposition = number;
                $scope.pgmsElpc.accDate = demoDate;
                $scope.pgmsElpc.appNo = demoString;

            }

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsElpcUpdate', result);
                $scope.isSaving = false;
                $state.go("pgmsElpc");
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                console.log('##########');
                $scope.pgmsElpc.approveStatus = 1;
                console.log($scope.approveStatus);
                if ($scope.pgmsElpc.id != null) {
                    PgmsElpc.update($scope.pgmsElpc, onSaveFinished,onSaveError);
                } else {
                    PgmsElpc.save($scope.pgmsElpc, onSaveFinished,onSaveError);
                }
            };

            $scope.rejectSave = function () {
                console.log('##########');
                $scope.pgmsElpc.approveStatus = -1;
                console.log($scope.approveStatus);
                if ($scope.pgmsElpc.id != null) {
                    PgmsElpc.update($scope.pgmsElpc, onSaveFinished,onSaveError);
                } else {
                    PgmsElpc.save($scope.pgmsElpc, onSaveFinished,onSaveError);
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };
        }]);
/*pgmsElpc-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsElpcDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsElpc', 'HrEmployeeInfo','PgmsElpcAllowanceInfo',
            function ($scope, $rootScope, $stateParams, entity, PgmsElpc, HrEmployeeInfo, PgmsElpcAllowanceInfo) {
                $scope.pgmsElpc = entity;
                $scope.allowanceInfoList = [];
                $scope.load = function (id) {
                    PgmsElpc.get({id: $stateParams.id}, function(result) {
                        $scope.pgmsElpc = result;
                        PgmsElpcAllowanceInfo.get({empInfoId:$scope.pgmsElpc.hrEmployeeInfo.id}, function(result) {
                            $scope.allowanceInfoList = result;
                        });
                    });
                };

                $scope.load();

                var unsubscribe = $rootScope.$on('stepApp:pgmsElpcUpdate', function(event, result) {
                    $scope.pgmsElpc = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
