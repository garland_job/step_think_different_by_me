'use strict';

angular.module('stepApp')
    .controller('PgmsAppFamilyPensionController',
    ['$scope', 'PgmsAppFamilyPension', 'PgmsAppFamilyPensionSearch', 'ParseLinks',
    function ($scope, PgmsAppFamilyPension, PgmsAppFamilyPensionSearch, ParseLinks) {
        $scope.pgmsAppFamilyPensions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsAppFamilyPension.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsAppFamilyPensions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsAppFamilyPension.get({id: id}, function(result) {
                $scope.pgmsAppFamilyPension = result;
                $('#deletePgmsAppFamilyPensionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsAppFamilyPension.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsAppFamilyPensionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsAppFamilyPensionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsAppFamilyPensions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsAppFamilyPension = {
                empName: null,
                empDepartment: null,
                empDesignation: null,
                empNid: null,
                nomineeStatus: null,
                nomineName: null,
                nomineDob: null,
                nomineGender: null,
                nomineRelation: null,
                nomineOccupation: null,
                nomineDesignation: null,
                nominePreAddress: null,
                nomineParAddress: null,
                nomineNid: null,
                nomineContNo: null,
                nomineBankName: null,
                nomineBranchName: null,
                nomineAccNo: null,
                applyDate: null,
                aprvStatus: 'Pending',
                aprvDate: null,
                aprvComment: null,
                aprvBy: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    }]);
/*pgmsAppFamilyPension-dialog.controller.js*/

angular.module('stepApp').controller('PgmsAppFamilyPensionDialogController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', 'entity', 'PgmsAppFamilyPension', 'PgmsAppFamilyAttach', 'HrEmployeeInfo', 'PgmsFmailyPenEmpInfo', 'PgmsRetirmntAttachInfo', 'PgmsAppFamilyAttachByTypeAndPen',
        'User', 'Principal', 'DateUtils', 'DataUtils','HrNomineeInfoByNomineeNidAndEmpId','HrEmployeeInfoByNationalId','HrEmployeeInfoByEmployeeId','GetAllHrEmployeeInfo',
        function($rootScope,$sce,$scope, $stateParams, $state, entity, PgmsAppFamilyPension,PgmsAppFamilyAttach,HrEmployeeInfo,PgmsFmailyPenEmpInfo, PgmsRetirmntAttachInfo, PgmsAppFamilyAttachByTypeAndPen,
                 User, Principal, DateUtils, DataUtils,HrNomineeInfoByNomineeNidAndEmpId,HrEmployeeInfoByNationalId,HrEmployeeInfoByEmployeeId,GetAllHrEmployeeInfo) {

            $scope.pgmsAppFamilyPension = entity;
            //$scope.hremployeeinfos = HrEmployeeInfo.query();

            GetAllHrEmployeeInfo.query(function(result){
                $scope.hremployeeInfos = result;
            });
            $scope.pgmsretirmntattachinfos = PgmsRetirmntAttachInfo.query();
            $scope.pgmsAppFamilyAttachList = [];
            $scope.hrEmployeeInfoId = {};

            $scope.showNomineeNid = true;
            var appPenId;

            $scope.showNomineeNID = function(value){
                if(value =='true'){
                    $scope.pgmsAppFamilyPension.nomineNid = '';
                    $scope.showNomineeNid = true;
                }else{
                    $scope.showNomineeNid = false;
                    $scope.pgmsAppFamilyPension.nomineNid = '';
                    $scope.pgmsAppFamilyPension.nomineName = '';
                    $scope.pgmsAppFamilyPension.nomineGender = '';
                    $scope.pgmsAppFamilyPension.nomineDob = '';
                    $scope.pgmsAppFamilyPension.nomineRelation = '';
                    $scope.pgmsAppFamilyPension.nomineParAddress = '';
                    $scope.pgmsAppFamilyPension.nominePreAddress = '';
                    $scope.pgmsAppFamilyPension.nomineContNo = '';
                    $scope.pgmsAppFamilyPension.nomineDesignation = '';
                    $scope.pgmsAppFamilyPension.nomineOccupation = '';
                }

            }

            if($stateParams.id == null){
                appPenId = 0;
            }
            else {
                appPenId = $stateParams.id;
            }
            $scope.viewFamilyAppForm = true;
            $scope.viewAttachForm = false;
            $scope.users = User.query({filter: 'pgmsAppFamilyPension-is-null'});

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.loadAll = function()
            {
                PgmsAppFamilyAttachByTypeAndPen.get({attacheType:'family',familyPensionId:appPenId},
                    function(result) {
                        $scope.pgmsAppFamilyAttachList = result;
                        //console.log("Len: "+$scope.pgmsAppFamilyAttachList.length);
                        //console.log(JSON.stringify("Pgms App Family Attach List:"+$scope.pgmsAppFamilyAttachList));
                    });
            };
            $scope.loadAll();

            $scope.load = function(id) {
                PgmsAppFamilyPension.get({id : id}, function(result) {
                    $scope.pgmsAppFamilyPension = result;
                });
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.employeeInfo = function(empId){
                //if(empNid == null){
                //     $scope.pgmsAppFamilyPension.empNid = empId.nationalId;
                //     $scope.pgmsAppFamilyPension.empName = empId.fullName;
                //     $scope.pgmsAppFamilyPension.empDepartment = empId.departmentInfo.departmentInfo.departmentName;
                //     $scope.pgmsAppFamilyPension.empDesignation = empId.designationInfo.designationInfo.designationName;
                //     var dataJsn = {employeeId:empId};
                //     //console.log("dataJsn:"+JSON.stringify(dataJsn));
                //}
                //else
                //{
                //if(!empId){
                //   var dataJsn = {employeeId:null, nid:$scope.pgmsAppFamilyPension.empNid};
                //}
                //else{
                //   var dataJsn = {employeeId:empId.employeeId, nid:$scope.pgmsAppFamilyPension.empNid};
                //}
                //PgmsFmailyPenEmpInfo.get(dataJsn, function(result) {
                //   if(!empId){
                //       $scope.pgmsAppFamilyPension.appEmpId = result.employeeId;
                //       $scope.pgmsAppFamilyPension.empName = result.fullName;
                //       $scope.pgmsAppFamilyPension.empDepartment = result.departmentInfo.departmentName;
                //       $scope.pgmsAppFamilyPension.empDesignation = result.designationInfo.designationName;
                //  } else{
                //       $scope.pgmsAppFamilyPension.empNid = result.nationalId;
                //       $scope.pgmsAppFamilyPension.empName = result.fullName;
                //       $scope.pgmsAppFamilyPension.empDepartment = result.departmentInfo.departmentName;
                //       $scope.pgmsAppFamilyPension.empDesignation = result.designationInfo.designationName;
                //  }
                //
                //});

                //}
                HrEmployeeInfoByEmployeeId.get({id:empId.employeeId},function(result){
                    console.log(result);
                    $scope.pgmsAppFamilyPension.empNid = result.nationalId;
                    $scope.pgmsAppFamilyPension.empName = result.fullName;
                    $scope.pgmsAppFamilyPension.empDepartment = result.departmentInfo.departmentInfo.departmentName;
                    $scope.pgmsAppFamilyPension.empDesignation = result.designationInfo.designationInfo.designationName;
                    $scope.hrEmployeeInfoId = result.id;
                });

            }

            $scope.getHrDataByNid= function(nid){
                HrEmployeeInfoByNationalId.get({nid:nid},function(hrData){
                    $scope.pgmsAppFamilyPension.appEmpId = hrData.employeeId;
                    $scope.pgmsAppFamilyPension.empName = hrData.fullName;
                    $scope.pgmsAppFamilyPension.empDepartment = hrData.departmentInfo.departmentInfo.departmentName;
                    $scope.pgmsAppFamilyPension.empDesignation = hrData.designationInfo.designationInfo.designationName;
                    $scope.hrEmployeeInfoId = hrData.id;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsAppFamilyPensionUpdate', result);
                $scope.isSaving = false;
                console.log("Attach Info Details: "+JSON.stringify($scope.pgmsAppFamilyAttachList));
                angular.forEach($scope.pgmsAppFamilyAttachList,function(application)
                {
                    application.appFamilyPenId = result.id;
                    if (application.id != null) {
                        PgmsAppFamilyAttach.update(application);
                    } else {

                        PgmsAppFamilyAttach.save(application);
                    }
                });
                //$state.go("pgmsAppFamilyPension");
                $state.go('pgmsAppFamilyPension');
            };

            /*var onSaveSuccess = function (result) {
             $scope.$emit('stepApp:pgmsAppFamilyAttachUpdate', result);
             $scope.isSaving = false;
             $scope.pgmsAppFamilyAttachList[$scope.selectedIndex].id=result.id;
             };

             $scope.saveAttachment = function (modelInfo, index)
             {
             $scope.selectedIndex = index;
             modelInfo.appFamilyPenId = $scope.appFamilyPenId;
             if (modelInfo.id != null)
             {
             PgmsAppFamilyAttach.update(modelInfo, onSaveSuccess, onSaveError);
             }
             else
             {
             PgmsAppFamilyAttach.save(modelInfo, onSaveSuccess, onSaveError);
             }
             };*/

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.pgmsAppFamilyPension.updateBy = $scope.loggedInUser.id;
                $scope.pgmsAppFamilyPension.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pgmsAppFamilyPension.id != null) {
                    PgmsAppFamilyPension.update($scope.pgmsAppFamilyPension, onSaveFinished, onSaveError);
                    $rootScope.setWarningMessage('stepApp.pgmsAppFamilyPension.updated');
                } else {
                    $scope.pgmsAppFamilyPension.createBy = $scope.loggedInUser.id;
                    $scope.pgmsAppFamilyPension.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PgmsAppFamilyPension.save($scope.pgmsAppFamilyPension, onSaveFinished, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.pgmsAppFamilyPension.created');
                }

            };

            $scope.showHideForm = function(viewAppStat, viewAttachSat)
            {
                $scope.viewFamilyAppForm = viewAppStat;
                $scope.viewAttachForm = viewAttachSat;
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setAttachment = function ($file, pgmsAppFamilyAttach) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppFamilyAttach.attachment = base64Data;
                            pgmsAppFamilyAttach.attachmentContentType = $file.type;
                            pgmsAppFamilyAttach.attachDocName = $file.name;
                        });
                    };
                }
            };


            $scope.previewDoc = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.delete = function (modelInfo) {
                $scope.pgmsAppFamilyAttach=modelInfo;
                $('#deletePgmsAppFamilyAttachConfirmation').modal('show');
            };

            $scope.confirmDelete = function (modelInfo)
            {
                PgmsAppFamilyAttach.delete({id: modelInfo.id},
                    function () {
                        $('#deletePgmsAppFamilyAttachConfirmation').modal('hide');
                        $scope.clear(modelInfo);
                    });

            };

            $scope.clear = function (modelInfo) {
                modelInfo.attachment= null;
                modelInfo.attachmentContentType= null;
                modelInfo.attachDocName= null;
                modelInfo.id= null;
            };

            $scope.getNomineeInfoByNID = function(nomineeNID){

                console.log('********************************');
                //console.log(empId);
                //if(applicationType == 'Death of Pensioner before approval'){

                HrNomineeInfoByNomineeNidAndEmpId.get({nid:nomineeNID,empId:$scope.hrEmployeeInfoId},function(nomineeData){
                    console.log('^^^^^^^^^^^^^^^^^^^^');
                    console.log(nomineeData);
                    $scope.pgmsAppFamilyPension.nomineName = nomineeData.nomineeName;
                    $scope.pgmsAppFamilyPension.nomineGender = nomineeData.gender;
                    $scope.pgmsAppFamilyPension.nomineDob = nomineeData.birthDate;
                    $scope.pgmsAppFamilyPension.nomineRelation = nomineeData.nomineeRelationship.typeName;
                    $scope.pgmsAppFamilyPension.nomineParAddress = nomineeData.address;
                    $scope.pgmsAppFamilyPension.nominePreAddress = nomineeData.address;
                    $scope.pgmsAppFamilyPension.nomineContNo = nomineeData.mobileNumber;
                    $scope.pgmsAppFamilyPension.nomineDesignation = nomineeData.designation;
                    $scope.pgmsAppFamilyPension.nomineOccupation = nomineeData.occupation;


                });
                //}
                //else if (applicationType == 'Death of pensioner while pension consumption'){
                //
                //
                //}else{
                //
                //}


            };
        }]);
/*pgmsAppFamilyPension-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsAppFamilyPensionDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'entity', 'PgmsAppFamilyPension', 'HrEmployeeInfo', 'PgmsAppFamilyAttachByPenId','PgmsAppFamilyAttach', 'PgmsRetirmntAttachInfo','DataUtils',
            function ($scope, $rootScope, $sce , $stateParams, entity, PgmsAppFamilyPension, HrEmployeeInfo, PgmsAppFamilyAttachByPenId, PgmsAppFamilyAttach, PgmsRetirmntAttachInfo, DataUtils) {
                $scope.pgmsAppFamilyPension = entity;
                $scope.pgmsAppFamilyAttachList = [];
                var appPenId = $stateParams.id;

                $scope.loadAll = function()
                {
                    PgmsAppFamilyAttachByPenId.get({familyPensionId:appPenId}, function(result) {
                        $scope.pgmsAppFamilyAttachList = result;
                        console.log("Len: "+$scope.pgmsAppFamilyAttachList.length);
                    });
                };
                $scope.loadAll();
                $scope.load = function (id) {
                    PgmsAppFamilyPension.get({id: id}, function(result) {
                        $scope.pgmsAppFamilyPension = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsAppFamilyPensionUpdate', function(event, result) {
                    $scope.pgmsAppFamilyPension = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.previewDoc = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                    $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                    $rootScope.showPreviewModal();
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;

            }]);
/*pgmsApplicationFamilyRequest.controller.js*/

angular.module('stepApp')
    .controller('PgmsApplicationFamilyRequestController',
        ['$scope', '$state','$sce','$rootScope', '$timeout', 'DataUtils','PgmsAppFamilyPension','PgmsAppFamilyPending','PgmsAppFamilyAttachByTypeAndPen','Principal', 'DateUtils',
            function($scope, $state, $sce, $rootScope, $timeout, DataUtils, PgmsAppFamilyPension,  PgmsAppFamilyPending, PgmsAppFamilyAttachByTypeAndPen,Principal, DateUtils)
            {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.pgmsAppFamilyAttachList = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;

                $scope.loggedInUser =   {};
                $scope.getLoggedInUser = function ()
                {
                    Principal.identity().then(function (account)
                    {
                        User.get({login: account.login}, function (result)
                        {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function()
                {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];
                    PgmsAppFamilyPending.get({statusType:'Pending'},function(result) {
                        $scope.newRequestList = result;
                        //console.log("App Family Pending List:"+JSON.stringify($scope.newRequestList));
                    });

                    $scope.newRequestList.sort($scope.sortById);
                    $scope.loadApprovedRejectList();
                };

                $scope.sortById = function(a,b){
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function ()
                {
                    if($scope.approvalObj.isApproved)
                        $scope.approvalObj.aprvStatus='Approved';
                    else $scope.approvalObj.aprvStatus='Reject';
                    $scope.approvalObj.aprvDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.approvalObj.aprvBy = $scope.loggedInUser.id;
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function ()
                {
                    if($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.aprvStatus = 'Approved';
                    }
                    else {
                        $scope.approvalObj.aprvStatus='Reject';
                    }
                    $scope.approvalObj.aprvDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.approvalObj.aprvBy = $scope.loggedInUser.id;

                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj){
                    PgmsAppFamilyPension.update(requestObj, function(result)
                    {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        $scope.loadAll();
                    });
                };

                $scope.loadApprovedRejectList = function ()
                {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                    PgmsAppFamilyPending.get({statusType:'Approved'},function(result) {
                            $scope.approvedList= result;
                        },function(response)
                        {
                            console.log("data from view load failed");
                        }
                    );
                    PgmsAppFamilyPending.get({statusType:'Reject'},function(result) {
                            $scope.rejectedList= result;
                        },function(response)
                        {
                            console.log("data from view load failed");
                        }
                    );
                };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source)
                {
                    if(source=='request')
                    {
                        $timeout(function(){
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };

                $scope.approvalViewDetail = function (dataObj)
                {
                    $scope.approvalObj = dataObj;
                    PgmsAppFamilyAttachByTypeAndPen.get({attacheType:'family',familyPensionId:dataObj.id},
                        function(result) {
                            $scope.pgmsAppFamilyAttachList = result;
                            // console.log("Len: "+$scope.pgmsAppFamilyAttachList.length);
                        });
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType){
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        entityName:null,
                        requestFrom:null,
                        requestSummary: null,
                        requestDate:null,
                        approveState: null,
                        aprvStatus:null,
                        logComments:null,
                        actionType:'',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function(event, data)
                {
                    $scope.loadAll();
                });


                $scope.loadEmployee = function () {
                    HrEmployeeInfo.get({id: 'my'}, function (result) {
                        $scope.hrEmployeeInfo = result;

                    }, function (response) {
                        $scope.hasProfile = false;
                        $scope.noEmployeeFound = true;
                        $scope.isSaving = false;
                    })
                };

                $scope.sort = function(keyname, source){
                    if(source=='request')
                    {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if(source=='approved')
                    {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if(source=='rejected')
                    {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };
                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;
                $scope.previewDoc = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                    $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                    $rootScope.showPreviewModal();
                };

                $scope.loadAll();

            }])
;

