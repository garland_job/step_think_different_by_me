'use strict';

angular.module('stepApp')
    .controller('PgmsAppRetirmntAttachController',
    ['$scope', 'PgmsAppRetirmntAttach', 'PgmsAppRetirmntAttachSearch', 'ParseLinks',
    function ($scope, PgmsAppRetirmntAttach, PgmsAppRetirmntAttachSearch, ParseLinks) {
        $scope.pgmsAppRetirmntAttachs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsAppRetirmntAttach.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsAppRetirmntAttachs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsAppRetirmntAttach.get({id: id}, function(result) {
                $scope.pgmsAppRetirmntAttach = result;
                $('#deletePgmsAppRetirmntAttachConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsAppRetirmntAttach.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsAppRetirmntAttachConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsAppRetirmntAttachSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsAppRetirmntAttachs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsAppRetirmntAttach = {
                appRetirmntPenId: null,
                attachment: null,
                attachmentContentType: null,
                attachDocName: null,
                attachDocType: null,
                id: null
            };
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    }]);
/*pgmsAppRetirmntAttach-dialog.controller.js*/

angular.module('stepApp').controller('PgmsAppRetirmntAttachDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsAppRetirmntAttach', 'PgmsRetirmntAttachInfo',
        function($scope, $stateParams, $state, entity, PgmsAppRetirmntAttach, PgmsRetirmntAttachInfo) {

            $scope.pgmsAppRetirmntAttach = entity;
            $scope.pgmsretirmntattachinfos = PgmsRetirmntAttachInfo.query();
            $scope.load = function(id) {
                PgmsAppRetirmntAttach.get({id : id}, function(result) {
                    $scope.pgmsAppRetirmntAttach = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsAppRetirmntAttachUpdate', result);
                $scope.isSaving = false;
                $state.go("pgmsAppRetirmntAttach");
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                if ($scope.pgmsAppRetirmntAttach.id != null) {
                    PgmsAppRetirmntAttach.update($scope.pgmsAppRetirmntAttach, onSaveFinished, onSaveError);
                } else {
                    PgmsAppRetirmntAttach.save($scope.pgmsAppRetirmntAttach, onSaveFinished, onSaveError);
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }
                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }
                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }
                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setAttachment = function ($file, pgmsAppRetirmntAttach) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppRetirmntAttach.attachment = base64Data;
                            pgmsAppRetirmntAttach.attachmentContentType = $file.type;
                        });
                    };
                }
            };
        }]);
/*pgmsAppRetirmntAttach-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsAppRetirmntAttachDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsAppRetirmntAttach', 'PgmsRetirmntAttachInfo',
            function ($scope, $rootScope, $stateParams, entity, PgmsAppRetirmntAttach, PgmsRetirmntAttachInfo) {
                $scope.pgmsAppRetirmntAttach = entity;
                $scope.load = function (id) {
                    PgmsAppRetirmntAttach.get({id: id}, function(result) {
                        $scope.pgmsAppRetirmntAttach = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsAppRetirmntAttachUpdate', function(event, result) {
                    $scope.pgmsAppRetirmntAttach = result;
                });
                $scope.$on('$destroy', unsubscribe);


                $scope.byteSize = function (base64String) {
                    if (!angular.isString(base64String)) {
                        return '';
                    }
                    function endsWith(suffix, str) {
                        return str.indexOf(suffix, str.length - suffix.length) !== -1;
                    }
                    function paddingSize(base64String) {
                        if (endsWith('==', base64String)) {
                            return 2;
                        }
                        if (endsWith('=', base64String)) {
                            return 1;
                        }
                        return 0;
                    }
                    function size(base64String) {
                        return base64String.length / 4 * 3 - paddingSize(base64String);
                    }
                    function formatAsBytes(size) {
                        return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                    }

                    return formatAsBytes(size(base64String));
                };
            }]);
