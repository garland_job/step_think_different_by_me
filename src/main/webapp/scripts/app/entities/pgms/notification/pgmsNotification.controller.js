'use strict';

angular.module('stepApp')
    .controller('PgmsNotificationController',
        function ($scope, $state,$stateParams, $filter, PgmsNotification, PgmsNotificationSearch, ParseLinks, NotificationHrEmpInfo) {
        $scope.pgmsNotifications = [];
        $scope.page = 0;
        $scope.notificationView = false;
        var firstDate;
        var secondDate;
        //$scope.pageSize = 30;
        $scope.loadAll = function() {
           //var thisyear = new Date().getFullYear();
           //var nextYear = thisyear+1;
           ////*nextYear = nextYear.toString().substr(2,2);
           //var fromDate = nextYear+"-01-01";
           //fromDate = $filter('date')(fromDate,'yyyy-MM-dd');
           //var toDate = nextYear+"-12-31";
           //toDate = $filter('date')(toDate,'yyyy-MM-dd');

           $scope.selectedNotificationList = [];
           var i=1;
           NotificationHrEmpInfo.query({financialYear:$stateParams.financialYear},function(result){
             angular.forEach(result,function(empInfo){
             //   //console.log("Retirement Date: "+empInfo.retirementDate+" FirstCondition: "+fromDate+" SecondCondition: "+toDate);
             //
             //   if((empInfo.retirementDate >= fromDate) && (empInfo.retirementDate<=toDate))
             //   {
             //        var durationCalculation = $scope.workDurationCalculation(empInfo.dateOfJoining,empInfo.retirementDate);
                     var retDate;
                     firstDate = empInfo.dateOfJoining;
                     secondDate = empInfo.retirementDate;
                     /*------*/

                 var date = $filter('date')(new Date(firstDate), 'yyyy/MM/dd');
                 if(secondDate==null){
                     var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                     var year = new Date().getFullYear();
                     var month = new Date().getMonth() + 1;
                     var day = new Date().getDate();
                     var time=new Date().getTime();
                 }else{
                     today =$filter('date')(new Date(secondDate), 'yyyy/MM/dd');
                     var year = new Date(secondDate).getFullYear();
                     var month = new Date(secondDate).getMonth() + 1;
                     var day = new Date(secondDate).getDate();
                     var time=  new Date(secondDate).getTime()
                 }

                 date = date.split('/');
                 today=today.split('/');
                 var yy = parseInt(date[0]);
                 var mm = parseInt(date[1]);
                 var dd = parseInt(date[2]);
                 var years, months, days;
                 // months
                 months = month - mm;
                 if (day < dd) {
                     months = months - 1;
                 }
                 // years
                 years = year - yy;
                 if (month * 100 + day < mm * 100 + dd) {
                     years = years - 1;
                     months = months + 12;
                 }
                 // days
                 $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
                 $scope.dura=years+" Years, "+months+" Months, "+$scope.days+" Days"

                 console.log('=====----Work Duration-----=======');
                 console.log($scope.dura);

                     /*-------*/

                     $scope.pgmsNotifications.push(
                         {

                             empId: empInfo.employeeId,
                             empName: empInfo.fullName,
                             empDesignation: empInfo.designationInfo.designationInfo.designationName,
                             empDepartment: empInfo.departmentInfo.departmentInfo.departmentName,
                             dateOfBirth: empInfo.birthDate,
                             joiningDate: empInfo.dateOfJoining,
                             retiremnntDate: empInfo.retirementDate,
                             prlDate:empInfo.retirementDate,
                             workDuration: $scope.dura,
                             contactNumber: empInfo.mobileNumber,
                             //message: null,
                             //notificationStatus: null,
                             //activeStatus: true,
                             //createDate: null,
                             //createBy: null,
                             //updateDate: null,
                             //updateBy: null,
                             notiCheck:false,
                             id: i
                         }
                     );
                     console.log('=======');
                     console.log(empInfo.retirementDate);
                 retDate=empInfo.retirementDate;
                 console.log('---== ---Retirement--- ===---');
                 var str=retDate.getYear()-1;
                 console.log(str);
                 //prlDate=retDate;
             //   }
                 i++;
             //
             });
             //$scope.pgmsNotifications = result;
             //console.log("Hr Employee Information :"+JSON.stringify($scope.pgmsNotifications));

           });

        };

        //$scope.loggedInUser =   {};
        //$scope.getLoggedInUser = function ()
        //{
        //    Principal.identity().then(function (account)
        //    {
        //        User.get({login: account.login}, function (result)
        //        {
        //            $scope.loggedInUser = result;
        //        });
        //    });
        //};
        //$scope.getLoggedInUser();

        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

       /* $scope.formatString = function(format) {
            var day   = parseInt(format.substring(0,2));
            var month  = parseInt(format.substring(3,5));
            var year   = parseInt(format.substring(6,10));
            var date = new Date(year, month-1, day);
            return date;
        }*/

        /*$scope.workDurationCalculation = function (firstDate, secondDate)
        {
            var firstDate = $filter('date')(firstDate,'dd/MM/yyyy');
            var secondDate = $filter('date')(secondDate,'dd/MM/yyyy');

            var date2 = new Date($scope.formatString(secondDate));
            var date1 = new Date($scope.formatString(firstDate));
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var diffYears = Math.round(diffDays/365);
            //console.log("Joining Date 2: "+firstDate+" Retirement Date 2: "+secondDate);
            return diffYears;
        };*/

           // $scope.duration=function (firstDate,secondDate) {


            //}

        $scope.checkAll = function ()
        {
            $scope.selectedNotificationList = [];
            if ($scope.selectedAll) {
                $scope.selectedAll = true;
                 angular.forEach($scope.pgmsNotifications, function (all) {
                      $scope.selectedNotificationList.push(all);

                 });
                 console.log("Select All Notification List: "+JSON.stringify($scope.selectedNotificationList));
                 $scope.notificationView = true;

            } else {
                $scope.selectedAll = false;
                $scope.notificationView = false;
            }
            angular.forEach($scope.pgmsNotifications, function (pgmsNotification) {
                pgmsNotification.notiCheck = $scope.selectedAll;
            });
        };

        /*$scope.delete = function (modelInfo) {
                $scope.pgmsNotification = modelInfo;
                $('#deletePgmsNotificationConfirmation').modal('show');
        };*/

        $scope.addRemoveRequestList = function(singleObj)
        {
           if(singleObj.notiCheck)
           {
                $scope.selectedNotificationList.push(singleObj);
                $scope.notificationView = true;
           }
           else
           {
             var index = $scope.selectedNotificationList.indexOf(singleObj);
             $scope.selectedNotificationList.splice(index, 1);
           }

           if(!$scope.selectedNotificationList.length){
             $scope.notificationView = false;
           }
          // console.log("Single Notification List: "+JSON.stringify($scope.selectedNotificationList));
        };

         /*$scope.initNotificationList = function(comments, notificationList)
         {
            return {
                comments: comments,
                notificationList:notificationList

            };
         };*/

        $scope.confirmSend = function () {

                //var updateBy = $scope.loggedInUser.id;
                //var updateDate = DateUtils.convertLocaleDateToServer(new Date());
                //var createBy = $scope.loggedInUser.id;
                //var createDate = DateUtils.convertLocaleDateToServer(new Date());
                var msg = $scope.pgmsNotification.message;
               // console.log("Message Details: "+msg);
                //PgmsNotification.save($scope.pgmsNotification);

               // var notificationActionObj =  $scope.initNotificationList ($scope.pgmsNotification.message, $scope.selectedNotificationList);
               console.log("selected Notification List: "+JSON.stringify($scope.selectedNotificationList));

                angular.forEach($scope.selectedNotificationList, function(selectedInfo){

                  selectedInfo.id = null;
                  selectedInfo.message = msg;
                  //selectedInfo.updateBy = updateBy;
                  //selectedInfo.updateDate = updateDate;
                  //selectedInfo.createBy = createBy;
                  //selectedInfo.createDate = createDate;
                  selectedInfo.notificationStatus = true;
                  PgmsNotification.save(selectedInfo);
                  $scope.removeDataFromList(selectedInfo);
                });

                $('#sendpgmsNotification').modal('hide');
                $scope.clear();

                //$state.go("pgmsNotification");
        };

        $scope.removeDataFromList = function(requestObj)
        {
            var indx2 = $scope.pgmsNotifications.indexOf(requestObj);
            $scope.pgmsNotifications.splice(indx2, 1);

        };

        $scope.search = function () {
            PgmsNotificationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsNotifications = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.notification = function (modelInfo) {
            $scope.pgmsNotification = modelInfo;
            $('#sendpgmsNotification').modal('show');
        };


        $scope.clear = function () {
         $scope.selectedNotificationList = [];
         $scope.pgmsNotification.message = null;
         $scope.notificationView = false;
        };

        $scope.cancel = function () {
         $scope.pgmsNotification.message = null;
        };
    });
    //
    //$scope.getPensionData = function(financialYear){
    //
    //    console.log('%%%%%%%%%%%%%%%%%%%');
    //}
/*pgmsNotification-dialog.controller.js*/

angular.module('stepApp').controller('PgmsNotificationDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsNotification', 'User', 'Principal', 'DateUtils',
        function($scope, $stateParams, $state, entity, PgmsNotification, User, Principal, DateUtils) {

            $scope.pgmsNotification = entity;
            $scope.users = User.query({filter: 'pgmsGrObtainSpecEmp-is-null'});
            $scope.loggedInUser =   {};

            $scope.load = function(id) {
                PgmsNotification.get({id : id}, function(result) {
                    $scope.pgmsNotification = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsNotificationUpdate', result);
                $scope.isSaving = false;
                $state.go("pgmsNotification");
            };

            $scope.save = function () {
                //$scope.pgmsNotification.updateBy = $scope.loggedInUser.id;
                //$scope.pgmsNotification.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pgmsNotification.id != null) {
                    PgmsNotification.update($scope.pgmsNotification, onSaveFinished);
                } else {
                    //$scope.pgmsNotification.createBy = $scope.loggedInUser.id;
                    //$scope.pgmsNotification.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PgmsNotification.save($scope.pgmsNotification, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
        }]);
/*pgmsNotification-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsNotificationDetailController', function ($scope, $rootScope, $stateParams, entity, PgmsNotification) {
        $scope.pgmsNotification = entity;
        $scope.load = function (id) {
            PgmsNotification.get({id: id}, function(result) {
                $scope.pgmsNotification = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pgmsNotificationUpdate', function(event, result) {
            $scope.pgmsNotification = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*pgmsNotificationSentListRequest.controller.js*/

angular.module('stepApp')
    .controller('PgmsNotificationSentListRequestController',
        ['$scope', 'PgmsNotification', 'PgmsNotificationSearch', 'ParseLinks',
            function ($scope, PgmsNotification, PgmsNotificationSearch, ParseLinks) {

                $scope.pgmsNotifications = [];
                $scope.page = 0;

                $scope.loadAll = function() {
                    PgmsNotification.query({page: $scope.page, size: 20}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.pgmsNotifications = result;
                        console.log(" Notification Send List: "+JSON.stringify($scope.pgmsNotifications));
                    });
                };

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();

                $scope.search = function () {
                    PgmsNotificationSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.pgmsNotifications = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };



                $scope.approvalViewDetail = function (dataObj)
                {
                    $scope.approvalObj = dataObj;
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.clear = function () {
                    $scope.PgmsNotification = {
                        empId: null,
                        empName: null,
                        empDesignation: null,
                        empDepartment: null,
                        dateOfBirth: null,
                        joiningDate: null,
                        retiremnntDate: null,
                        workDuration: null,
                        contactNumber: null,
                        message: null,
                        notificationStatus: null,
                        activeStatus: null,
                        createDate: null,
                        createBy: null,
                        updateDate: null,
                        updateBy: null,
                        id: null
                    };
                };

            }]);
