/*
'use strict';

angular.module('stepApp').controller('PgmsNotificationDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsNotification', 'User', 'Principal', 'DateUtils',
        function($scope, $stateParams, $state, entity, PgmsNotification, User, Principal, DateUtils) {

        $scope.pgmsNotification = entity;
        $scope.users = User.query({filter: 'pgmsGrObtainSpecEmp-is-null'});
        $scope.loggedInUser =   {};

        $scope.load = function(id) {
            PgmsNotification.get({id : id}, function(result) {
                $scope.pgmsNotification = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:pgmsNotificationUpdate', result);
            $scope.isSaving = false;
            $state.go("pgmsNotification");
        };

        $scope.save = function () {
            //$scope.pgmsNotification.updateBy = $scope.loggedInUser.id;
            //$scope.pgmsNotification.updateDate = DateUtils.convertLocaleDateToServer(new Date());
            if ($scope.pgmsNotification.id != null) {
                PgmsNotification.update($scope.pgmsNotification, onSaveFinished);
            } else {
                //$scope.pgmsNotification.createBy = $scope.loggedInUser.id;
                //$scope.pgmsNotification.createDate = DateUtils.convertLocaleDateToServer(new Date());
                PgmsNotification.save($scope.pgmsNotification, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $state.dismiss('cancel');
        };

        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };
}]);
*/
