'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('pensionGratuityRateSetup', {
                parent: 'pgms',
                url: '/pgmsPenGrSetups',
                data: {
                    authorities: ['ROLE_ADMIN','ROLE_HRM_USER'],
                    pageTitle: 'stepApp.pgmsPenGrSetup.home.title'
                },
                views: {
                    'pgmsHomeView@pgms': {
                        templateUrl: 'scripts/app/entities/pgms/pensionGratuityRateSetup/pensionGratuityRateSetups.html',
                        controller: 'PgmsPenGrSetupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pensionGratuityRateSetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('pensionGratuityRateSetup.detail', {
                parent: 'pgms',
                url: '/pgmsPenGrSetup/{version}',
                data: {
                    authorities: ['ROLE_ADMIN','ROLE_HRM_USER'],
                    pageTitle: 'stepApp.pgmsPenGrSetup.detail.title'
                },
                views: {
                    'pgmsHomeView@pgms': {
                        templateUrl: 'scripts/app/entities/pgms/pensionGratuityRateSetup/pensionGratuityRateSetup-detail.html',
                        controller: 'PgmsPenGrSetupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pensionGratuityRateSetup');
                        //$translatePartialLoader.addPart('pgmsPenGrRate');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PensionGratuityRateSetup', function($stateParams, PensionGratuityRateSetup) {
                        //return PgmsPenGrSetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('pensionGratuityRateSetup.new', {
                parent: 'pensionGratuityRateSetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER','ROLE_HRM_USER'],
                },
                views: {
                     'pgmsHomeView@pgms': {
                      templateUrl: 'scripts/app/entities/pgms/pensionGratuityRateSetup/pensionGratuityRateSetup-dialog.html',
                      controller: 'PgmsPenGrSetupDialogController'
                     }
                },
                resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pensionGratuityRateSetup');
                        //$translatePartialLoader.addPart('pgmsPenGrRate');
                        return $translate.refresh();
                      }],
                      entity: function () {
                              return {
                                    setupType: null,
                                    effectiveDate: null,
                                    setupVersion: null,
                                    activeStatus: null,
                                    createDate: null,
                                    createBy: null,
                                    updateDate: null,
                                    updateBy: null,
                                    id: null
                              }
                       }
                }
            })
            .state('pensionGratuityRateSetup.edit', {
                parent: 'pensionGratuityRateSetup',
                url: '/{version}/edit',
                data: {
                    authorities: ['ROLE_ADMIN','ROLE_HRM_USER'],
                },
                views: {
                     'pgmsHomeView@pgms': {
                          templateUrl: 'scripts/app/entities/pgms/pensionGratuityRateSetup/pensionGratuityRateSetup-dialog.html',
                          controller: 'PgmsPenGrSetupDialogController'
                     }
                },
                resolve: {
                     translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pensionGratuityRateSetup');
                        return $translate.refresh();
                     }],
                     entity: ['$stateParams','PensionGratuityRateSetup', function($stateParams,PensionGratuityRateSetup) {
                               //return PgmsPenGrSetup.get({id : $stateParams.id});
                     }]
                }
            });
    });
