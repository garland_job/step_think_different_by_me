'use strict';

angular.module('stepApp')
    .controller('PgmsPenGrSetupController',
    ['$scope', 'PensionGratuityRateSetup', 'PensionGratuityRateSetupSearch', 'ParseLinks','GetAllVersionInfo',
    function ($scope,PensionGratuityRateSetup,PgmsPenGrSetupSearch, ParseLinks,GetAllVersionInfo) {

        $scope.pgmsPenGrSetups = [];
        $scope.pgmsPenGrRateListDel = [];
        $scope.removeItems = [];
        $scope.pensionGratuityRateSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            GetAllVersionInfo.query({},function(result){
                console.log('-------------------------------');
                angular.forEach(result,function(value){
                    $scope.pensionGratuityRateSetups.push(value);
                    console.log(value);
                });
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        //$scope.delete = function (id) {
        //    $scope.pgmsPenGrRateListDel = PgmsPenGrRateList.get({penGrSetId : id});
        //    //console.log("POST "+JSON.stringify($scope.iisApplicantInfoListDel));
        //    angular.forEach($scope.pgmsPenGrRateListDel,function(ratDel)
        //    {
        //        PgmsPenGrRate.get({id: appDel.id});
        //
        //    });
        //    PgmsPenGrSetup.get({id: id}, function(result) {
        //        $scope.pgmsPenGrSetup = result;
        //        $('#deletePgmsPenGrSetupConfirmation').modal('show');
        //    });
        //};

        //$scope.confirmDelete = function (id) {
        //   angular.forEach($scope.pgmsPenGrRateListDel,function(appDelId)
        //   {
        //       PgmsPenGrRate.delete({id: appDelId.id});
        //   });
        //    PgmsPenGrSetup.delete({id: id},
        //        function () {
        //            $scope.loadAll();
        //            $('#deletePgmsPenGrSetupConfirmation').modal('hide');
        //            $scope.clear();
        //        });
        //};

        //$scope.search = function () {
        //    PgmsPenGrSetupSearch.query({query: $scope.searchQuery}, function(result) {
        //        $scope.pgmsPenGrSetups = result;
        //    }, function(response) {
        //        if(response.status === 404) {
        //            $scope.loadAll();
        //        }
        //    });
        //};



        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsPenGrSetup = {
                setupType: null,
                effectiveDate: null,
                setupVersion: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*pensionGratuityRateSetup-dialog.controller.js*/

'use strict';

angular.module('stepApp').controller('PgmsPenGrSetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PensionGratuityRateSetup','User', 'Principal', 'DateUtils','MaxVersionBySetupType','GetAllByVersionAndSetupType',
        function($scope, $stateParams, $state, entity, PensionGratuityRateSetup, User, Principal, DateUtils,MaxVersionBySetupType,GetAllByVersionAndSetupType) {

            $scope.pensionGratuityRateSetup = entity;
            $scope.pgmsPenGrRatelist = [];
            $scope.removeItems = [];
            $scope.setupVersion = 0;
            $scope.workingYearError = false;

            $scope.load = function(id) {
            };

            $scope.loadAllDataList = function (){

                if($stateParams.id == null)
                {
                    $scope.btnShow = true;
                    $scope.pgmsPenGrSetup = entity;
                    $scope.pgmsPenGrRatelist.push($scope.initiateModel());
                }
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                if($scope.activeStatus==null){
                    $scope.activeStatus = true;
                }

                angular.forEach($scope.pgmsPenGrRatelist,function(obj){
                    console.log(obj)
                    obj.effectiveDate = $scope.pensionGratuityRateSetup.effectiveDate;
                    obj.setupType = $scope.setupType;
                    if(obj.id !=null){
                        obj.setupVersion = $stateParams.version;
                        obj.activeStatus = $scope.activeStatus;
                        PensionGratuityRateSetup.update(obj);
                    }else{
                        if($scope.setupType == null){
                            $scope.setupType = "Pension";
                        }
                        if($stateParams.version!=null){
                            obj.setupVersion = $stateParams.version;
                        }else{
                            obj.setupVersion = $scope.setupVersion;
                        }
                        obj.activeStatus = $scope.activeStatus;
                        PensionGratuityRateSetup.save(obj);
                    }
                });
                if($scope.removeItems.length > 0){
                    angular.forEach($scope.removeItems,function(deleteObj){
                        PensionGratuityRateSetup.delete(deleteObj);
                    });
                }
                $state.go('pensionGratuityRateSetup',{},{reload: true});
            };

            $scope.pgsType = function(stype)
            {
                if(stype == 1){
                    $scope.setupType = "Pension";
                    MaxVersionBySetupType.get({pSetupType:$scope.setupType },function(data){
                        $scope.setupVersion = data.version + 1;
                    });
                }else{
                    $scope.setupType = "Gratuity";
                    MaxVersionBySetupType.get({pSetupType:$scope.setupType },function(data){
                        $scope.setupVersion = data.version + 1;
                    });
                }

                if($stateParams.version != null && $scope.setupType !=null)
                {
                    GetAllByVersionAndSetupType.query({page: $scope.page, size: 500,version:$stateParams.version,setupType:$scope.setupType},function(result){
                        $scope.pgmsPenGrRatelist = result;
                        $scope.activeStatus = result[0].activeStatus;
                        $scope.pensionGratuityRateSetup.effectiveDate = result[0].effectiveDate;
                    });
                }
            };

            if($stateParams.id != null){
                console.log($stateParams.id);
            }else{
                $scope.pgmsPenGrRatelist.push(
                    {
                        //penGrSetId: null,
                        setupVersion:null,
                        workingYear: null,
                        rateOfPenGr: null,
                        activeStatus: null,
                        id: null
                    }
                );
            }

            $scope.addMore = function()
            {
                $scope.pgmsPenGrRatelist.push(
                    {
                        //penGrSetId: null,
                        workingYear: '',
                        rateOfPenGr: '',
                        activeStatus: null,
                        id: null
                    }
                );
            };

            $scope.remove = function (result) {
                var index =  $scope.pgmsPenGrRatelist.indexOf(result);
                $scope.pgmsPenGrRatelist.splice(index,1);
                $scope.removeItems.push(result);
            }

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

            $scope.checkWorkingYearExist = function(workingYear,index){
                $scope.workingYearError = false;
                if(workingYear!=null){
                    angular.forEach($scope.pgmsPenGrRatelist,function(obj,key){
                        if(key!=index){
                            if(obj.workingYear == workingYear){
                                $scope.workingYearError = true;
                            }
                        }
                    });
                }

            }
        }]);
/*pensionGratuityRateSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsPenGrSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PensionGratuityRateSetup', 'PgmsPenGrRateList','GetAllByVersion',
            function ($scope, $rootScope, $stateParams, entity, PensionGratuityRateSetup,PgmsPenGrRateList,GetAllByVersion) {

                $scope.pgmsPenGrSetups = entity;
                $scope.pgmsPenGrRateListView = [];


                GetAllByVersion.query({page: $scope.page, size: 500 ,version:$stateParams.version},function(result){
                    $scope.pgmsPenGrSetups = result;

                });
                //$scope.loadAllDataView = function ()
                //{
                //    PgmsPenGrRateList.get({penGrSetId : $stateParams.id}, function(result) {
                //        $scope.pgmsPenGrRateListView = result;
                //       // console.log("Rate info:"+JSON.stringify($scope.pgmsPenGrRatelist));
                //    });
                //
                //};
                //$scope.loadAllDataView();

                //$scope.load();
                $scope.load = function () {


                    //PgmsPenGrSetup.get({id: id}, function(result) {
                    //    $scope.pgmsPenGrSetup = result;
                    //});
                };
                //var unsubscribe = $rootScope.$on('stepApp:pgmsPenGrSetupUpdate', function(event, result) {
                //    $scope.pgmsPenGrSetup = result;
                //});
                //$scope.$on('$destroy', unsubscribe);

            }]);
