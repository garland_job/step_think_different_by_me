/*
'use strict';

angular.module('stepApp').controller('PgmsPenGrSetupDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PensionGratuityRateSetup','User', 'Principal', 'DateUtils','MaxVersionBySetupType','GetAllByVersionAndSetupType',
        function($scope, $stateParams, $state, entity, PensionGratuityRateSetup, User, Principal, DateUtils,MaxVersionBySetupType,GetAllByVersionAndSetupType) {

        $scope.pensionGratuityRateSetup = entity;
        $scope.pgmsPenGrRatelist = [];
        $scope.removeItems = [];
        $scope.setupVersion = 0;

        $scope.load = function(id) {
            //PgmsPenGrSetup.get({id : id}, function(result) {
            //    $scope.pgmsPenGrSetup = result;
            //});
        };

        $scope.loadAllDataList = function (){

           if($stateParams.id == null)
           {
                $scope.btnShow = true;
                $scope.pgmsPenGrSetup = entity;
                $scope.pgmsPenGrRatelist.push($scope.initiateModel());
           }
        };

        var onSaveError = function (result) {
             $scope.isSaving = false;
        };

        $scope.save = function () {
            if($scope.activeStatus==null){
                $scope.activeStatus = true;
            }

            angular.forEach($scope.pgmsPenGrRatelist,function(obj){
                console.log(obj)
                obj.effectiveDate = $scope.pensionGratuityRateSetup.effectiveDate;
                obj.setupType = $scope.setupType;
                if(obj.id !=null){
                    console.log('--------------------');
                    obj.setupVersion = $scope.setupVersion;
                    obj.activeStatus = $scope.activeStatus;
                    PensionGratuityRateSetup.update(obj);
                }else{
                    console.log('*********');
                    if($scope.setupType == null){
                        $scope.setupType = "Pension";
                    }
                    obj.setupVersion = $scope.setupVersion;
                    obj.activeStatus = $scope.activeStatus;
                    PensionGratuityRateSetup.save(obj);
                }
            });
            if($scope.removeItems.length > 0){
                angular.forEach($scope.removeItems,function(deleteObj){
                    console.log('000000000000000');
                    console.log(deleteObj);
                    PensionGratuityRateSetup.delete(deleteObj);
                });
            }
            $state.go('pensionGratuityRateSetup',{},{reload: true});
        };

        $scope.pgsType = function(stype)
        {
            if(stype == 1){
              $scope.setupType = "Pension";
                MaxVersionBySetupType.get({pSetupType:$scope.setupType },function(data){
                    $scope.setupVersion = data.version + 1;
                    console.log('^^^^^^^^^^^^^^^^^^^');
                    console.log( $scope.setupVersion);
                });
            }else{
              $scope.setupType = "Gratuity";
                MaxVersionBySetupType.get({pSetupType:$scope.setupType },function(data){
                    $scope.setupVersion = data.version + 1;
                });
            }

            if($stateParams.version != null && $scope.setupType !=null)
            {
                console.log('(((((((((((((((((((((((((((((((((((((((((((((9');
                GetAllByVersionAndSetupType.query({version:$stateParams.version,setupType:$scope.setupType},function(result){
                    $scope.pgmsPenGrRatelist = result;
                    $scope.pensionGratuityRateSetup.effectiveDate = result[0].effectiveDate;

                });
            }
        };

        if($stateParams.id != null){
            console.log($stateParams.id);
        }else{
            $scope.pgmsPenGrRatelist.push(
                {
                    //penGrSetId: null,
                    workingYear: '',
                    rateOfPenGr: '',
                    activeStatus: null,
                    id: null
                }
            );
        }

        $scope.addMore = function()
        {
            $scope.pgmsPenGrRatelist.push(
                {
                  //penGrSetId: null,
                  workingYear: '',
                  rateOfPenGr: '',
                  activeStatus: null,
                  id: null
                }
            );
        };

        $scope.remove = function (result) {
            var index =  $scope.pgmsPenGrRatelist.indexOf(result);
            $scope.pgmsPenGrRatelist.splice(index,1);
            $scope.removeItems.push(result);
        }

        $scope.clear = function() {
            $state.dismiss('cancel');
        };
}]);
*/
