'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('retirementAppStatusLog', {
                parent: 'entity',
                url: '/retirementAppStatusLogs',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.retirementAppStatusLog.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/retirementAppStatusLog/retirementAppStatusLogs.html',
                        controller: 'RetirementAppStatusLogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('retirementAppStatusLog');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('retirementAppStatusLog.detail', {
                parent: 'entity',
                url: '/retirementAppStatusLog/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.retirementAppStatusLog.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/retirementAppStatusLog/retirementAppStatusLog-detail.html',
                        controller: 'RetirementAppStatusLogDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('retirementAppStatusLog');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'RetirementAppStatusLog', function($stateParams, RetirementAppStatusLog) {
                        return RetirementAppStatusLog.get({id : $stateParams.id});
                    }]
                }
            })
            .state('retirementAppStatusLog.new', {
                parent: 'retirementAppStatusLog',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/retirementAppStatusLog/retirementAppStatusLog-dialog.html',
                        controller: 'RetirementAppStatusLogDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    status: null,
                                    remarks: null,
                                    approvedDate: null,
                                    cause: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('retirementAppStatusLog', null, { reload: true });
                    }, function() {
                        $state.go('retirementAppStatusLog');
                    })
                }]
            })
            .state('retirementAppStatusLog.edit', {
                parent: 'retirementAppStatusLog',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/retirementAppStatusLog/retirementAppStatusLog-dialog.html',
                        controller: 'RetirementAppStatusLogDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['RetirementAppStatusLog', function(RetirementAppStatusLog) {
                                return RetirementAppStatusLog.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('retirementAppStatusLog', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
