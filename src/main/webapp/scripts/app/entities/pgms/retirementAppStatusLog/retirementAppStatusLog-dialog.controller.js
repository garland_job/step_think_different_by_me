/*
'use strict';

angular.module('stepApp').controller('RetirementAppStatusLogDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'RetirementAppStatusLog', 'PgmsAppRetirmntPen',
        function($scope, $stateParams, $modalInstance, entity, RetirementAppStatusLog, PgmsAppRetirmntPen) {

        $scope.retirementAppStatusLog = entity;
        $scope.pgmsappretirmntpens = PgmsAppRetirmntPen.query();
        $scope.load = function(id) {
            RetirementAppStatusLog.get({id : id}, function(result) {
                $scope.retirementAppStatusLog = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:retirementAppStatusLogUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.retirementAppStatusLog.id != null) {
                RetirementAppStatusLog.update($scope.retirementAppStatusLog, onSaveFinished);
            } else {
                RetirementAppStatusLog.save($scope.retirementAppStatusLog, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
*/
