'use strict';

angular.module('stepApp')
    .controller('RetirementAppStatusLogController', function ($scope, RetirementAppStatusLog, RetirementAppStatusLogSearch, ParseLinks) {
        $scope.retirementAppStatusLogs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            RetirementAppStatusLog.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.retirementAppStatusLogs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            RetirementAppStatusLog.get({id: id}, function(result) {
                $scope.retirementAppStatusLog = result;
                $('#deleteRetirementAppStatusLogConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            RetirementAppStatusLog.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteRetirementAppStatusLogConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            RetirementAppStatusLogSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.retirementAppStatusLogs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.retirementAppStatusLog = {
                status: null,
                remarks: null,
                approvedDate: null,
                cause: null,
                id: null
            };
        };
    });
/*retirementAppStatusLog-dialog.controller.js*/

angular.module('stepApp').controller('RetirementAppStatusLogDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'RetirementAppStatusLog', 'PgmsAppRetirmntPen',
        function($scope, $stateParams, $modalInstance, entity, RetirementAppStatusLog, PgmsAppRetirmntPen) {

            $scope.retirementAppStatusLog = entity;
            $scope.pgmsappretirmntpens = PgmsAppRetirmntPen.query();
            $scope.load = function(id) {
                RetirementAppStatusLog.get({id : id}, function(result) {
                    $scope.retirementAppStatusLog = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:retirementAppStatusLogUpdate', result);
                $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.retirementAppStatusLog.id != null) {
                    RetirementAppStatusLog.update($scope.retirementAppStatusLog, onSaveFinished);
                } else {
                    RetirementAppStatusLog.save($scope.retirementAppStatusLog, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*retirementAppStatusLog-detail.controller.js*/

angular.module('stepApp')
    .controller('RetirementAppStatusLogDetailController', function ($scope, $rootScope, $stateParams, entity, RetirementAppStatusLog, PgmsAppRetirmntPen) {
        $scope.retirementAppStatusLog = entity;
        $scope.load = function (id) {
            RetirementAppStatusLog.get({id: id}, function(result) {
                $scope.retirementAppStatusLog = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:retirementAppStatusLogUpdate', function(event, result) {
            $scope.retirementAppStatusLog = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
