'use strict';

angular.module('stepApp')
    .controller('PgmsAppRetirmntNmineController',
    ['$scope', 'PgmsAppRetirmntNmine', 'PgmsAppRetirmntNmineSearch', 'ParseLinks',
    function ($scope, PgmsAppRetirmntNmine, PgmsAppRetirmntNmineSearch, ParseLinks) {
        $scope.pgmsAppRetirmntNmines = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsAppRetirmntNmine.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsAppRetirmntNmines = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsAppRetirmntNmine.get({id: id}, function(result) {
                $scope.pgmsAppRetirmntNmine = result;
                $('#deletePgmsAppRetirmntNmineConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsAppRetirmntNmine.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsAppRetirmntNmineConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsAppRetirmntNmineSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsAppRetirmntNmines = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsAppRetirmntNmine = {
                appRetirmntPenId: null,
                nomineeStatus: null,
                nomineeName: null,
                gender: null,
                relation: null,
                dateOfBirth: null,
                presentAddress: null,
                nid: null,
                occupation: null,
                designation: null,
                maritalStatus: null,
                mobileNo: null,
                getPension: null,
                hrNomineeInfo: null,
                id: null
            };
        };
    }]);
/*pgmsAppRetirmntNmine-dialog.controller.js*/

angular.module('stepApp').controller('PgmsAppRetirmntNmineDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsAppRetirmntNmine','HrNomineeInforByEmpId',
        function($scope, $stateParams, $state, entity, PgmsAppRetirmntNmine,HrNomineeInforByEmpId) {
            //$scope.pgmsAppRetirmntNmine = entity;
            //$scope.pgmsAppRetirmntNmine = [];
            $scope.pgmsHrNomineeList = [];
            $scope.totalList = null;
            var hrEmpId = 5864;

            $scope.load = function(id) {
                PgmsAppRetirmntNmine.get({id : id}, function(result) {
                    $scope.pgmsAppRetirmntNmine = result;
                });
            };
            $scope.loadAll = function()
            {
                //$scope.pgmsHrNomineeList = entity;
                HrNomineeInforByEmpId.get({empId:hrEmpId},
                    function(result) {
                        angular.forEach(result,function(dtoInfo){
                            $scope.pgmsHrNomineeList.push(
                                {
                                    appRetirmntPenId:null,
                                    nomineeStatus:null,
                                    nomineeName:dtoInfo.nomineeName,
                                    gender:dtoInfo.gender,
                                    relation:dtoInfo.nomineeRelationship.typeName,
                                    dateOfBirth:dtoInfo.birthDate,
                                    presentAddress:dtoInfo.address,
                                    nid:dtoInfo.nationalId,
                                    occupation:dtoInfo.occupation,
                                    designation:dtoInfo.designation,
                                    maritalStatus:'Marrid',
                                    mobileNo:dtoInfo.mobileNumber,
                                    getPension:null,
                                    hrNomineeInfo:null,
                                    id:null
                                }
                            );

                        });
                        $scope.totalList = $scope.pgmsHrNomineeList.length;
                        console.log("Nominee Length :"+$scope.totalList);
                        console.log("Nominee List :"+JSON.stringify($scope.pgmsHrNomineeList));
                    });

            };
            $scope.loadAll();

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsAppRetirmntNmineUpdate', result);
                $scope.isSaving = false;
                $state.go("pgmsAppRetirmntNmine");
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {

                angular.forEach($scope.pgmsHrNomineeList,function(application)
                {
                    if(application.appRetirmntPenId== null){ application.appRetirmntPenId = 1;}
                    if (application.id != null) {
                        PgmsAppRetirmntNmine.update(application);
                    } else {

                        PgmsAppRetirmntNmine.save(application);
                    }
                });
                $scope.isSaving = false;
                $state.go("pgmsAppRetirmntNmine");

                /*if ($scope.pgmsAppRetirmntNmine.id != null) {
                 $scope.pgmsAppRetirmntNmine.appRetirmntPenId = 1;
                 PgmsAppRetirmntNmine.update($scope.pgmsAppRetirmntNmine, onSaveFinished, onSaveError);
                 } else {
                 PgmsAppRetirmntNmine.save($scope.pgmsAppRetirmntNmine, onSaveFinished, onSaveError);
                 }*/
            };

            $scope.addMore = function()
            {
                $scope.pgmsHrNomineeList.push(
                    {
                        appRetirmntPenId: null,
                        nomineeStatus: true,
                        nomineeName: null,
                        gender: null,
                        relation: null,
                        dateOfBirth: null,
                        presentAddress: null,
                        nid: null,
                        occupation: null,
                        designation: null,
                        maritalStatus: null,
                        mobileNo: null,
                        getPension: null,
                        hrNomineeInfo: null,
                        id: null
                    }
                );
                $scope.addmorelist = 'addView';
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };
        }]);
/*pgmsAppRetirmntNmine-detail.controller.js*/
angular.module('stepApp')
    .controller('PgmsAppRetirmntNmineDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsAppRetirmntNmine',
            function ($scope, $rootScope, $stateParams, entity, PgmsAppRetirmntNmine) {
                $scope.pgmsAppRetirmntNmine = entity;
                $scope.load = function (id) {
                    PgmsAppRetirmntNmine.get({id: id}, function(result) {
                        $scope.pgmsAppRetirmntNmine = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsAppRetirmntNmineUpdate', function(event, result) {
                    $scope.pgmsAppRetirmntNmine = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
