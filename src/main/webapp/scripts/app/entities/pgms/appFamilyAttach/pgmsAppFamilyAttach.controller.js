'use strict';

angular.module('stepApp')
    .controller('PgmsAppFamilyAttachController',
    ['$scope', 'PgmsAppFamilyAttach', 'PgmsAppFamilyAttachSearch', 'ParseLinks',
    function ($scope, PgmsAppFamilyAttach, PgmsAppFamilyAttachSearch, ParseLinks) {
        $scope.pgmsAppFamilyAttachs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsAppFamilyAttach.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsAppFamilyAttachs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsAppFamilyAttach.get({id: id}, function(result) {
                $scope.pgmsAppFamilyAttach = result;
                $('#deletePgmsAppFamilyAttachConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsAppFamilyAttach.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsAppFamilyAttachConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsAppFamilyAttachSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsAppFamilyAttachs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsAppFamilyAttach = {
                appFamilyPenId: null,
                attachment: null,
                attachmentContentType: null,
                attachDocName: null,
                attachDocType: null,
                id: null
            };
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    }]);
/*pgmsAppFamilyAttach-dialog.controller.js*/

angular.module('stepApp').controller('PgmsAppFamilyAttachDialogController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', 'entity', 'PgmsAppFamilyAttach', 'PgmsRetirmntAttachInfo','PgmsAppFamilyAttachByTypeAndPension','DataUtils',
        function($rootScope,$sce, $scope, $stateParams, $modalInstance, entity, PgmsAppFamilyAttach, PgmsRetirmntAttachInfo, PgmsAppFamilyAttachByTypeAndPension,DataUtils) {

            $scope.pgmsAppFamilyAttach = {};
            $scope.pgmsretirmntattachinfos = PgmsRetirmntAttachInfo.query();
            $scope.appFamilyPenId = 1;

            $scope.pgmsAppFamilyAttachList = [];
            $scope.load = function(id) {
                PgmsAppFamilyAttach.get({id : id}, function(result) {
                    $scope.pgmsAppFamilyAttach = result;
                });
            };
            $scope.loadAll = function()
            {
                PgmsAppFamilyAttachByTypeAndPension.get({attacheType:'family',familyPensionId:$scope.appFamilyPenId},
                    function(result) {
                        $scope.pgmsAppFamilyAttachList = result;
                        console.log("Len: "+$scope.pgmsAppFamilyAttachList.length);
                        console.log(JSON.stringify($scope.pgmsAppFamilyAttachList));
                    });

            };
            $scope.loadAll();

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:pgmsAppFamilyAttachUpdate', result);
                $scope.isSaving = false;
                $scope.pgmsAppFamilyAttachList[$scope.selectedIndex].id=result.id;
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.saveAttachment = function (modelInfo, index)
            {
                $scope.selectedIndex = index;
                modelInfo.appFamilyPenId = $scope.appFamilyPenId;
                if (modelInfo.id != null)
                {
                    PgmsAppFamilyAttach.update(modelInfo, onSaveSuccess, onSaveError);
                }
                else
                {
                    PgmsAppFamilyAttach.save(modelInfo, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

            $scope.abbreviate = DataUtils.abbreviate;

            $scope.byteSize = DataUtils.byteSize;

            $scope.setAttachment = function ($file, pgmsAppFamilyAttach) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppFamilyAttach.attachment = base64Data;
                            pgmsAppFamilyAttach.attachmentContentType = $file.type;
                            pgmsAppFamilyAttach.attachDocName = $file.name;
                        });
                    };
                }
            };


            $scope.previewDoc = function (modelInfo)
            {
                var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.delete = function (modelInfo) {
                $scope.pgmsAppFamilyAttach=modelInfo;
                $('#deletePgmsAppFamilyAttachConfirmation').modal('show');
            };

            $scope.confirmDelete = function (modelInfo)
            {
                PgmsAppFamilyAttach.delete({id: modelInfo.id},
                    function () {
                        $('#deletePgmsAppFamilyAttachConfirmation').modal('hide');
                        $scope.clear(modelInfo);
                    });

            };

            $scope.clear = function (modelInfo) {
                modelInfo.attachment= null;
                modelInfo.attachmentContentType= null;
                modelInfo.attachDocName= null;
                modelInfo.id= null;
            };
        }]);
/*pgmsAppFamilyAttach-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsAppFamilyAttachDetailController',
        ['$scope', '$rootScope', '$sce', '$stateParams', 'entity', 'PgmsAppFamilyAttach', 'PgmsRetirmntAttachInfo', 'DataUtils',
            function ($scope, $rootScope, $sce, $stateParams, entity, PgmsAppFamilyAttach, PgmsRetirmntAttachInfo, DataUtils) {
                $scope.pgmsAppFamilyAttach = entity;
                $scope.load = function (id) {
                    PgmsAppFamilyAttach.get({id: id}, function(result) {
                        $scope.pgmsAppFamilyAttach = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsAppFamilyAttachUpdate', function(event, result) {
                    $scope.pgmsAppFamilyAttach = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.previewDoc = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                    $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                    $rootScope.showPreviewModal();
                };

                $scope.abbreviate = DataUtils.abbreviate;

                $scope.byteSize = DataUtils.byteSize;

            }]);
