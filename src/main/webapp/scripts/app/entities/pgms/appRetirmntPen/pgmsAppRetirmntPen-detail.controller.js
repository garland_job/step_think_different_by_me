/*
'use strict';

angular.module('stepApp')
    .controller('PgmsAppRetirmntPenDetailController',
    function ($scope, $rootScope, $sce, $stateParams, entity, PgmsAppRetirmntPen, HrEmployeeInfo,PgmsAppRetirmntNmine,
    RetirementNomineeInfosByPenId, PgmsAppRetirementAttachByPenId, CalculationInfosByPenId, DataUtils,PgmsAppRetirementAttachByTypeAndPen) {
        $scope.pgmsAppRetirmntPen = entity;
        $scope.pgmsHrNomineeList = [];
        $scope.pgmsAppRetirementAttachList = [];
        $scope.pgmsAppRetirmntCalculation = {};

        $scope.loadAllDataView = function ()
        {
            PgmsAppRetirmntPen.get({id: $stateParams.id}, function(basicData) {
                $scope.pgmsAppRetirmntPen = basicData;
            });
           // RetirementNomineeInfosByPenId.get({penId:$stateParams.id},function(result) {
              RetirementNomineeInfosByPenId.get({penId:$stateParams.id},function(nomineeData){
                  $scope.pgmsHrNomineeList= nomineeData;
              });

           CalculationInfosByPenId.get({pensionId:$stateParams.id},function(result) {
                 $scope.pgmsAppRetirmntCalculation = result;
           });

           PgmsAppRetirementAttachByTypeAndPen.query({attacheType:'retirement',retirementPenId:$stateParams.id},function(result) {
              $scope.pgmsAppRetirementAttachList = result;
           });
        };
        $scope.loadAllDataView();

        $scope.load = function (id) {
            PgmsAppRetirmntPen.get({id: id}, function(result) {
                $scope.pgmsAppRetirmntPen = result;
            });
        };

        var unsubscribe = $rootScope.$on('stepApp:pgmsAppRetirmntPenUpdate', function(event, result) {
            $scope.pgmsAppRetirmntPen = result;
        });
        $scope.$on('$destroy', unsubscribe);

        $scope.nomineeDoc = function (certificateInfo)
        {
            var blob = $rootScope.b64toBlob(certificateInfo.birthCertificate, certificateInfo.certificateContentType);
            $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
            $rootScope.viewerObject.contentType = certificateInfo.certificateContentType;
            $rootScope.viewerObject.pageTitle = "Preview of Birth Certificate Order Document";
            $rootScope.showPreviewModal();
        };

        $scope.previewDoc = function (modelInfo)
        {
            console.log("Attachment: "+modelInfo.attachmentContentType);

            var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
            $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
            $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
            console.log("url: "+$rootScope.viewerObject.contentUrl);
            $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
            $rootScope.showPreviewModal();
        };

        $scope.abbreviate = DataUtils.abbreviate;
        $scope.byteSize = DataUtils.byteSize;


    });
*/
