/*
'use strict';

angular.module('stepApp').controller('PgmsAppRetirmntPenDialogController',
    ['$rootScope','$sce','$scope', '$stateParams', '$state', 'entity', 'PgmsAppRetirmntPen','PgmsAppRetirmntAttach',
     'HrEmployeeInfo', 'PgmsAppRetirmntNmine','RetirementNomineeInfosByPenId','HrNomineeInfosByEmpId','BankInfosByEmpId',
     'PgmsAppRetirementAttachByTypeAndPen','PgmsAppRetirmntCalculation', 'CalculationInfosByPenId', 'CalculationLastPayCodeInfoByEmpId',
     'User','DataUtils','PrlSalaryStructureInfoByEmployeeId','PensionAndGratuityRateByWorkingYear',
        function($rootScope, $sce, $scope, $stateParams, $state, entity, PgmsAppRetirmntPen,PgmsAppRetirmntAttach,
        HrEmployeeInfo, PgmsAppRetirmntNmine,RetirementNomineeInfosByPenId, HrNomineeInfosByEmpId,BankInfosByEmpId,
        PgmsAppRetirementAttachByTypeAndPen, PgmsAppRetirmntCalculation, CalculationInfosByPenId, CalculationLastPayCodeInfoByEmpId,
        User, DataUtils,PrlSalaryStructureInfoByEmployeeId,PensionAndGratuityRateByWorkingYear) {

        $scope.pgmsAppRetirmntPen = entity;
        $scope.hremployeeinfos = HrEmployeeInfo.query();
        $scope.retirmntCalculationList = [];
        $scope.workingYears = 0;
        $scope.pgmsAppRetirmntCalculation = {};
        $scope.surrenderType100 = false;
        $scope.pensionPercent = false;
        $scope.viewPensionAppForm = true;
        $scope.viewPensionNomineeForm = false;
        $scope.viewPensionAttachForm = false;
        $scope.viewPensionCalculationForm = false;
        $scope.view50lebel = true;
        $scope.view100lebel = false;
        $scope.pgmsHrNomineeList = [];
        $scope.pgmsHrEmpBankInfos = [];
        $scope.hrEmployeeInfo = [];
        $scope.lastPayCodeInfos = [];
        $scope.totalList = null;
        var loggedEmpId = null;
        var loggedUserEmpId = null;

        /!* For Attachment Information *!/
        $scope.pgmsAppRetirementAttachList = [];
        var appPenId;
        if($stateParams.id == null){
          appPenId = 0;
        }
        else {
          appPenId = $stateParams.id;
        }

        $scope.users = User.query({filter: 'pgmsAppRetirmntPen-is-null'});

        $scope.loadAll = function()
        {
           HrEmployeeInfo.get({id: 'my'}, function (result) {
               $scope.hrEmployeeInfo = result;
               $scope.workingYears = ((new Date(result.lastWorkingDay).getFullYear()) - (new Date(result.dateOfJoining).getFullYear()));

               if($stateParams.id == null)
               {
                    HrNomineeInfosByEmpId.get({empId:$scope.hrEmployeeInfo.id},
                       function(result) {
                             angular.forEach(result,function(dtoInfo){
                                 $scope.pgmsHrNomineeList.push(
                                     {
                                         appRetirmntPenId:null,
                                         nomineeStatus:null,
                                         nomineeName:dtoInfo.nomineeName,
                                         gender:dtoInfo.gender,
                                         relation:dtoInfo.nomineeRelationship.typeName,
                                         dateOfBirth:dtoInfo.birthDate,
                                         presentAddress:dtoInfo.address,
                                         nid:dtoInfo.nationalId,
                                         occupation:dtoInfo.occupation,
                                         designation:dtoInfo.designation,
                                         maritalStatus:'Marrid',
                                         mobileNo:dtoInfo.mobileNumber,
                                         getPension:null,
                                         hrNomineeInfo:null,
                                         birthCertificate: null,
                                         certificateContentType: null,
                                         certificateDocName: null,
                                         id:null
                                     }
                                 );
                             });
                             $scope.totalList = $scope.pgmsHrNomineeList.length;
                       });
                       $scope.calculationView('50');
                       $scope.generateRetirmntAppNo();
               }else{
                   RetirementNomineeInfosByPenId.get({penId:$stateParams.id},function(result) {
                     $scope.pgmsHrNomineeList = result;
                    });
               }

               BankInfosByEmpId.query({empId:$scope.hrEmployeeInfo.id},function(result) {
                   $scope.pgmsHrEmpBankInfos = result;
               });

               PrlSalaryStructureInfoByEmployeeId.get({employeeId:$scope.hrEmployeeInfo.id},function(result){
                   console.log('Basic '+result.payscaleBasicInfo.basicAmount);
                   $scope.pgmsAppRetirmntCalculation.lastPayCodeElpcInfo = result.payscaleBasicInfo.basicAmount;

                   PensionAndGratuityRateByWorkingYear.query({workYear:$scope.workingYears},function(data){
                       angular.forEach(data,function(eachData){
                           console.log('-----------------');
                           console.log(eachData);
                           if(eachData.setupType == 'Pension'){
                               console.log('((((((((((((');
                               console.log(eachData.rateOfPenGr);
                               $scope.pgmsAppRetirmntCalculation.rateOfPension = eachData.rateOfPenGr;
                               $scope.pgmsAppRetirmntCalculation.totalPenOrGr =  result.payscaleBasicInfo.basicAmount * (eachData.rateOfPenGr/100);
                               $scope.pgmsAppRetirmntCalculation.firstHalfOfPen =  $scope.pgmsAppRetirmntCalculation.totalPenOrGr/2;
                               $scope.pgmsAppRetirmntCalculation.secondHalfOfPen =  $scope.pgmsAppRetirmntCalculation.totalPenOrGr/2;
                           }else if(eachData.setupType == 'Gratuity'){
                               console.log('***********');
                               console.log(eachData.rateOfPenGr);
                               $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl = eachData.rateOfPenGr;
                           }
                       });
                       $scope.pgmsAppRetirmntCalculation.deservedRetirmntAl =  $scope.pgmsAppRetirmntCalculation.firstHalfOfPen * $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl;
                       $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = $scope.pgmsAppRetirmntCalculation.secondHalfOfPen + " + Medical Allowance +" + " Festival Allowance ";
                       console.log('&&&&&&&&&&');
                       console.log($scope.pgmsAppRetirmntCalculation.surrenderType);
                   });
               });
           });

           PgmsAppRetirementAttachByTypeAndPen.query({attacheType:'retirement',retirementPenId:appPenId},
               function(result) {
                   $scope.pgmsAppRetirementAttachList = result;
           });

        };
        $scope.loadAll();

        $scope.load = function(id) {
            PgmsAppRetirmntPen.get({id : id}, function(result) {
                $scope.pgmsAppRetirmntPen = result;
            });
        };

        $scope.generateRetirmntAppNo = function ()
        {
            var curDt = new Date();
            //console.log(curDt);
            var uniqCode = ""+curDt.getFullYear().toString()+""+(curDt.getMonth()+1).toString()+""+curDt.getDate().toString()+""+curDt.getHours().toString()+""+curDt.getMinutes()+""+curDt.getSeconds();
            //console.log(uniqCode);
            $scope.pgmsAppRetirmntPen.appNo = "RAPN"+uniqCode;
        };

        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:pgmsAppRetirmntPenUpdate', result);
             $scope.isSaving = false;

             angular.forEach($scope.pgmsHrNomineeList,function(application)
             {
                application.appRetirmntPenId = result;

                if (application.id != null) {
                       PgmsAppRetirmntNmine.update(application);
                } else {
                    if(application.nomineeStatus == true ){
                        console.log('Save Nominee');
                        PgmsAppRetirmntNmine.save(application);
                    }
                }
             });

             angular.forEach($scope.pgmsAppRetirementAttachList,function(appattach){
                appattach.appRetirmntPenInfo = result;
                if (appattach.id != null) {
                       PgmsAppRetirmntAttach.update(appattach);
                } else {
                     PgmsAppRetirmntAttach.save(appattach);
                }
             });
               $scope.pgmsAppRetirmntCalculation.appRetirmntPenInfo = result;

             if ($scope.pgmsAppRetirmntCalculation.id != null) {
                    $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = 0;
                    PgmsAppRetirmntCalculation.update($scope.pgmsAppRetirmntCalculation);
             }
             else {
                 $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = 0;
                 PgmsAppRetirmntCalculation.save($scope.pgmsAppRetirmntCalculation);
             }
             $state.go('pgmsAppRetirmntPen');
        };

        var onSaveError = function (result) {
             $scope.isSaving = false;
        };

        $scope.save = function () {
            if ($scope.pgmsAppRetirmntPen.id != null) {
                PgmsAppRetirmntPen.update($scope.pgmsAppRetirmntPen, onSaveFinished, onSaveError);
                $rootScope.setWarningMessage('stepApp.pgmsAppRetirmntPen.updated');
            } else {
                console.log('%%%%%%%%%%%%%%%%%%%%');
                $scope.pgmsAppRetirmntPen.hrEmployeeInfo = $scope.hrEmployeeInfo;
                PgmsAppRetirmntPen.save($scope.pgmsAppRetirmntPen, onSaveFinished, onSaveError);
                $rootScope.setSuccessMessage('stepApp.pgmsAppRetirmntPen.created');
            }
        };
        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        $scope.calculationInitiateModel = function()
        {
            return {
               surrenderType: "50",
               rateOfPension: null,
               totalPenOrGr: null,
               firstHalfOfPen: null,
               exchangeRetirmntAl: null,
               deservedRetirmntAl: null,
               monthlyRetirmntAl: null,
               dueAmountInfo: null,
               remainingTotal: null,
               id: null
            };
        };

        $scope.addMore = function()
        {
            $scope.pgmsHrNomineeList.push(
                {
                   appRetirmntPenId: null,
                   nomineeStatus: true,
                   nomineeName: null,
                   gender: null,
                   relation: null,
                   dateOfBirth: null,
                   presentAddress: null,
                   nid: null,
                   occupation: null,
                   designation: null,
                   maritalStatus: null,
                   mobileNo: null,
                   getPension: null,
                   hrNomineeInfo: null,
                   birthCertificate: null,
                   certificateContentType: null,
                   certificateDocName: null,
                   id: null
                }
            );
            $scope.addmorelist = 'addView';
        };
        $scope.showHideForm = function(viewPenApp, viewNomineeInfo,viewCalculationInfo,viewAttachInfo)
        {
            console.log('****************88');
            console.log(viewNomineeInfo);
            $scope.viewPensionAppForm = viewPenApp;
            $scope.viewPensionNomineeForm = viewNomineeInfo;
            $scope.viewPensionCalculationForm = viewCalculationInfo;
            $scope.viewPensionAttachForm = viewAttachInfo;
        };

        $scope.clear = function() {
            $state.dismiss('cancel');
        };

        $scope.abbreviate = DataUtils.abbreviate;
        $scope.byteSize = DataUtils.byteSize;

        $scope.setBirthCertificate = function ($file, pgmsAppRetirmntNmine) {
            if ($file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($file);
                fileReader.onload = function (e) {
                    var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                    $scope.$apply(function() {
                        pgmsAppRetirmntNmine.birthCertificate = base64Data;
                        pgmsAppRetirmntNmine.certificateContentType = $file.type;
                        pgmsAppRetirmntNmine.certificateDocName = $file.name;
                    });
                };
            }
        };

        $scope.setAttachment = function ($file, pgmsAppRetirmntAttach) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppRetirmntAttach.attachment = base64Data;
                            pgmsAppRetirmntAttach.attachmentContentType = $file.type;
                            pgmsAppRetirmntAttach.attachDocName = $file.name;
                        });
                    };
                }
        };

         $scope.previewDoc = function (modelInfo)
            {
                console.log("Update Attach Infos : "+JSON.stringify(modelInfo));
                var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                $rootScope.showPreviewModal();
            };

        $scope.nomineeDoc = function (certificateInfo)
        {
            var blob = $rootScope.b64toBlob(certificateInfo.birthCertificate, certificateInfo.certificateContentType);
            $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
            $rootScope.viewerObject.contentType = certificateInfo.certificateContentType;
            $rootScope.viewerObject.pageTitle = "Preview of Birth Certificate Order Document";
            $rootScope.showPreviewModal();
        };



        $scope.delete = function (modelInfo) {
            $scope.pgmsAppRetirmntAttach = modelInfo;
            $('#deletePgmsAppRetirmntAttachConfirmation').modal('show');
        };

        $scope.confirmDelete = function (modelInfo)
        {
            PgmsAppRetirmntAttach.delete({id: modelInfo.id},
                function () {
                    $('#deletePgmsAppRetirmntAttachConfirmation').modal('hide');
                    $scope.clear(modelInfo);
                });

        };

        $scope.bankInfoView = function(status,hrEmpBankInfo)
        {
            if(status == 'true')
            {
              console.log('---------------99999999999');
              $scope.pgmsAppRetirmntPen.bankName = hrEmpBankInfo.bankName.typeName;
              $scope.pgmsAppRetirmntPen.bankAcc = hrEmpBankInfo.accountNumber;
              $scope.pgmsAppRetirmntPen.bankBranch = hrEmpBankInfo.branchName;
              $scope.pgmsAppRetirmntPen.bankAccStatus = true;
            }
        };

        $scope.workDurationView = function(workInfo)
        {
            if(workInfo == 'willful')
            {
              $scope.pgmsAppRetirmntPen.workDuration = null;
            }
        };

        $scope.calculationView = function(surrenderType)
        {
            //var lastPayCode = 9900;
            //var rate = 90;
            //var tprice =  lastPayCode*(90/100);
            //var fhalf = tprice/2;
            //var erallowance = 230;
            //var dseramout = fhalf*erallowance;
            //var festibalAlloance = 1000;
            //var medicalAllowance = 1000;

            if(surrenderType == '50')
            {
                $scope.pgmsAppRetirmntCalculation.surrenderType = '50';
                $scope.surrenderType100 = false;
                  //$scope.view50lebel = true;
                  //$scope.view100lebel = false;
                  //var mamount = fhalf;
                  //$scope.pgmsAppRetirmntCalculation.lastPayCodeElpcInfo= 9900;
                  //$scope.pgmsAppRetirmntCalculation.rateOfPension= 90;
                  //$scope.pgmsAppRetirmntCalculation.totalPenOrGr= tprice;
                  //$scope.pgmsAppRetirmntCalculation.firstHalfOfPen= fhalf;
                  //$scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl= erallowance;
                  //$scope.pgmsAppRetirmntCalculation.deservedRetirmntAl= dseramout;
                  //$scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl= mamount;
                  //$scope.pgmsAppRetirmntCalculation.dueAmountInfo= medicalAllowance;
                  //$scope.pgmsAppRetirmntCalculation.remainingTotal= festibalAlloance;
            }
            else
            {
                $scope.surrenderType100 = true;
                $scope.pgmsAppRetirmntCalculation.surrenderType == '100';
                if($scope.pgmsAppRetirmntCalculation.surrenderType == '100'){
                    $scope.pgmsAppRetirmntCalculation.rateOfExForSecHalf = $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl/2;
                    $scope.pgmsAppRetirmntCalculation.deservedRetirementAllFor100 = $scope.pgmsAppRetirmntCalculation.secondHalfOfPen * $scope.pgmsAppRetirmntCalculation.rateOfExForSecHalf;
                    $scope.pgmsAppRetirmntCalculation.totalGratuity =  $scope.pgmsAppRetirmntCalculation.deservedRetirmntAl + $scope.pgmsAppRetirmntCalculation.deservedRetirementAllFor100;
                }
                //$scope.view50lebel = false;
                //$scope.view100lebel = true;

                //var mamount = (fhalf*115);
                //$scope.pgmsAppRetirmntCalculation.lastPayCodeElpcInfo= 9900;
                //$scope.pgmsAppRetirmntCalculation.rateOfPension= 90;
                //$scope.pgmsAppRetirmntCalculation.totalPenOrGr= tprice;
                //$scope.pgmsAppRetirmntCalculation.firstHalfOfPen= fhalf;
                //$scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl= erallowance;
                //$scope.pgmsAppRetirmntCalculation.deservedRetirmntAl= dseramout;
                //$scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl= mamount;
                //$scope.pgmsAppRetirmntCalculation.dueAmountInfo= medicalAllowance;
                //$scope.pgmsAppRetirmntCalculation.remainingTotal= festibalAlloance;
            }
        };
        /!*$scope.deductionCalculation = function(dueAmount)
        {
            $scope.pgmsAppRetirmntCalculation.remainingTotal= $scope.pgmsAppRetirmntCalculation.totalPenOrGr-dueAmount;
        };*!/

        $scope.clear = function (modelInfo) {
            modelInfo.attachment= null;
            modelInfo.attachmentContentType= null;
            modelInfo.attachDocName= null;
            modelInfo.id= null;
        };

        $scope.hidePensionPercent = function(index){
            $scope.pensionPercent = true;

        }


}]);
*/
