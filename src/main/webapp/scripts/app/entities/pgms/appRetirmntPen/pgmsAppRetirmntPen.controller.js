'use strict';

angular.module('stepApp')
    .controller('PgmsAppRetirmntPenController',
        ['$scope', 'PgmsAppRetirmntPen','HrEmployeeInfo','PgmsAppRetirmntPenSearch', 'EmpRetirmentAppByEmpId','PgmsElpcIsApproved',
         'DeleteRetirementNomineeInfosByPenId', 'DeleteRetirementCaculationInfosByPenId', 'DeleteRetirementAttachInfosByPenId','ParseLinks',
        function ($scope, PgmsAppRetirmntPen, HrEmployeeInfo, PgmsAppRetirmntPenSearch, EmpRetirmentAppByEmpId,PgmsElpcIsApproved,
        DeleteRetirementNomineeInfosByPenId, DeleteRetirementCaculationInfosByPenId, DeleteRetirementAttachInfosByPenId, ParseLinks) {

        $scope.pgmsAppRetirmntPens = [];
        $scope.hrEmployeeInfo = [];
        $scope.page = 0;
        $scope.applyEnable = false;

        $scope.loadAll = function() {
            HrEmployeeInfo.get({id: 'my'}, function (result) {
                $scope.hrEmployeeInfo = result;
                EmpRetirmentAppByEmpId.get({empId:$scope.hrEmployeeInfo.id},function(result) {
                  $scope.pgmsAppRetirmntPens = result;

                    PgmsElpcIsApproved.get({empId:$scope.hrEmployeeInfo.id},function(elpcCheck){
                        if(elpcCheck.status=='True'){
                            $scope.applyEnable = true;
                            angular.forEach(result,function(pensionApp){
                                if(pensionApp.approveStatus == 6){
                                    console.log('^^^^^^^^^^^');
                                    $scope.applyEnable = false;
                                }
                            });
                        }else{
                            $scope.applyEnable = false;
                        }
                    });

                });
            });
        };

        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsAppRetirmntPen.get({id: id}, function(result) {
                $scope.pgmsAppRetirmntPen = result;
                $('#deletePgmsAppRetirmntPenConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            DeleteRetirementNomineeInfosByPenId.get({penId:id});
            DeleteRetirementCaculationInfosByPenId.get({penId:id});
            DeleteRetirementAttachInfosByPenId.get({penId:id});
            PgmsAppRetirmntPen.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsAppRetirmntPenConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsAppRetirmntPenSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsAppRetirmntPens = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsAppRetirmntPen = {
                withdrawnType: null,
                applicationType: null,
                rcvGrStatus: false,
                workDuration: null,
                emergencyContact: null,
                bankAccStatus: false,
                bankName: null,
                bankAcc: null,
                bankBranch: null,
                appDate: null,
                appNo: null,
                aprvStatus: 'Pending',
                aprvDate: null,
                aprvComment: null,
                aprvBy: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    }]);
/*pgmsAppRetirmntPen-dialog.controller.js*/

angular.module('stepApp').controller('PgmsAppRetirmntPenDialogController',
    ['$rootScope', '$filter', '$sce','$scope', '$stateParams', '$state', 'entity', 'PgmsAppRetirmntPen','PgmsAppRetirmntAttach',
        'HrEmployeeInfo', 'PgmsAppRetirmntNmine','RetirementNomineeInfosByPenId','HrNomineeInfosByEmpId','BankInfosByEmpId',
        'PgmsAppRetirementAttachByTypeAndPen','PgmsAppRetirmntCalculation', 'CalculationInfosByPenId', 'CalculationLastPayCodeInfoByEmpId',
        'User','DataUtils','PrlSalaryStructureInfoByEmployeeId','PensionAndGratuityRateByWorkingYear', 'MiscTypeSetupByCategory',
        function($rootScope, $filter, $sce, $scope, $stateParams, $state, entity, PgmsAppRetirmntPen,PgmsAppRetirmntAttach,
                 HrEmployeeInfo, PgmsAppRetirmntNmine,RetirementNomineeInfosByPenId, HrNomineeInfosByEmpId,BankInfosByEmpId,
                 PgmsAppRetirementAttachByTypeAndPen, PgmsAppRetirmntCalculation, CalculationInfosByPenId, CalculationLastPayCodeInfoByEmpId,
                 User, DataUtils,PrlSalaryStructureInfoByEmployeeId,PensionAndGratuityRateByWorkingYear,MiscTypeSetupByCategory) {

            $scope.pgmsAppRetirmntPen = entity;
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.retirmntCalculationList = [];
            $scope.workingYears = 0;
            $scope.pgmsAppRetirmntCalculation = {};
            $scope.surrenderType100 = false;
            $scope.pensionPercent = false;
            $scope.viewPensionAppForm = true;
            $scope.viewPensionNomineeForm = false;
            $scope.viewPensionAttachForm = false;
            $scope.viewPensionCalculationForm = false;
            $scope.view50lebel = true;
            $scope.view100lebel = false;
            $scope.pgmsHrNomineeList = [];
            $scope.pgmsHrEmpBankInfos = [];
            $scope.hrEmployeeInfo = [];
            $scope.lastPayCodeInfos = [];
            $scope.totalList = null;

            $scope.misctypesetups = MiscTypeSetupByCategory.get({cat: 'BankName', stat: 'true'});

            $scope.misctypesetupss = MiscTypeSetupByCategory.get({cat: 'NomineeRelationship', stat: 'true'});


            var loggedEmpId = null;
            var loggedUserEmpId = null;


            $scope.addBankInfo=false;
            $scope.pensionPercent = 0;
            $scope.pensionPercentExcced = false;
            $scope.removeItems = [];

            /* For Attachment Information */
            $scope.pgmsAppRetirementAttachList = [];
            var appPenId;
            if($stateParams.id == null){
                appPenId = 0;
            }
            else {
                appPenId = $stateParams.id;
            }

            $scope.users = User.query({filter: 'pgmsAppRetirmntPen-is-null'});

            $scope.loadAll = function()
            {
                HrEmployeeInfo.get({id: 'my'}, function (result) {
                    $scope.hrEmployeeInfo = result;
                    $scope.workingYears = ((new Date(result.lastWorkingDay).getFullYear()) - (new Date(result.dateOfJoining).getFullYear()));
                    var joiningDate = result.dateOfJoining;
                    console.log('-----Joining Date-------');
                    console.log(joiningDate);
                    if($stateParams.id == null)
                    {
                        HrNomineeInfosByEmpId.get({empId:$scope.hrEmployeeInfo.id},
                            function(result) {
                                    var date = $filter('date')(new Date(joiningDate), 'yyyy/MM/dd');
                                    var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                                    var year = new Date().getFullYear();
                                    var month = new Date().getMonth() + 1;
                                    var day = new Date().getDate();
                                    var time=new Date().getTime();
                                    date = date.split('/');
                                    today=today.split('/');
                                    var yy = parseInt(date[0]);
                                    var mm = parseInt(date[1]);
                                    var dd = parseInt(date[2]);
                                    var years, months, days;
                                    // months
                                    months = month - mm;
                                    if (day < dd) {
                                        months = months - 1;
                                    }
                                    // years
                                    years = year - yy;
                                    if (month * 100 + day < mm * 100 + dd) {
                                        years = years - 1;
                                        months = months + 12;
                                    }
                                    // days
                                    $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
                                    $scope.dura=years+" Years, "+months+" Months, "+$scope.days+" Days"
                                    console.log('-----Work Duration -------');
                                    console.log($scope.dura);

                                $scope.pgmsAppRetirmntPen.workDuration = $scope.dura;


                                angular.forEach(result,function(dtoInfo){
                                    $scope.pgmsHrNomineeList.push(
                                        {
                                            appRetirmntPenId:null,
                                            nomineeStatus:null,
                                            nomineeName:dtoInfo.nomineeName,
                                            gender:dtoInfo.gender,
                                            relation:dtoInfo.nomineeRelationship.typeName,
                                            dateOfBirth:dtoInfo.birthDate,
                                            presentAddress:dtoInfo.address,
                                            nid:dtoInfo.nationalId,
                                            occupation:dtoInfo.occupation,
                                            designation:dtoInfo.designation,
                                            maritalStatus:'Marrid',
                                            mobileNo:dtoInfo.mobileNumber,
                                            getPension:null,
                                            hrNomineeInfo:null,
                                            birthCertificate: null,
                                            certificateContentType: null,
                                            certificateDocName: null,
                                            id:null
                                        }
                                    );
                                });
                                $scope.totalList = $scope.pgmsHrNomineeList.length;
                            });
                        $scope.calculationView('50');
                        $scope.generateRetirmntAppNo();
                    }else{
                        RetirementNomineeInfosByPenId.get({penId:$stateParams.id},function(result) {
                            $scope.pgmsHrNomineeList = result;
                        });
                    }

                    BankInfosByEmpId.query({empId:$scope.hrEmployeeInfo.id},function(result) {
                        $scope.pgmsHrEmpBankInfos = result;
                        if(result[0] == undefined){
                            $scope.addBankInfo=true;
                        }
                    });

                    PrlSalaryStructureInfoByEmployeeId.get({employeeId:$scope.hrEmployeeInfo.id},function(result){
                        console.log('Basic '+result.payscaleBasicInfo.basicAmount);
                        $scope.pgmsAppRetirmntCalculation.lastPayCodeElpcInfo = result.payscaleBasicInfo.basicAmount;

                        PensionAndGratuityRateByWorkingYear.query({workYear:$scope.workingYears},function(data){
                            angular.forEach(data,function(eachData){
                                if(eachData.setupType == 'Pension'){
                                    $scope.pgmsAppRetirmntCalculation.rateOfPension = eachData.rateOfPenGr;
                                    $scope.pgmsAppRetirmntCalculation.totalPenOrGr =  result.payscaleBasicInfo.basicAmount * (eachData.rateOfPenGr/100);
                                    $scope.pgmsAppRetirmntCalculation.firstHalfOfPen =  $scope.pgmsAppRetirmntCalculation.totalPenOrGr/2;
                                    $scope.pgmsAppRetirmntCalculation.secondHalfOfPen =  $scope.pgmsAppRetirmntCalculation.totalPenOrGr/2;
                                }else if(eachData.setupType == 'Gratuity'){
                                    $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl = eachData.rateOfPenGr;
                                }
                            });
                            $scope.pgmsAppRetirmntCalculation.deservedRetirmntAl =  $scope.pgmsAppRetirmntCalculation.firstHalfOfPen * $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl;
                            $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = $scope.pgmsAppRetirmntCalculation.secondHalfOfPen + " + Medical Allowance +" + " Festival Allowance ";
                            });
                    });
                });

                PgmsAppRetirementAttachByTypeAndPen.query({attacheType:'retirement',retirementPenId:appPenId},
                    function(result) {
                        $scope.pgmsAppRetirementAttachList = result;
                    });

            };
            $scope.loadAll();

            $scope.load = function(id) {
                PgmsAppRetirmntPen.get({id : id}, function(result) {
                    $scope.pgmsAppRetirmntPen = result;
                });
            };

            $scope.generateRetirmntAppNo = function ()
            {
                var curDt = new Date();
                var uniqCode = ""+curDt.getFullYear().toString()+""+(curDt.getMonth()+1).toString()+""+curDt.getDate().toString()+""+curDt.getHours().toString()+""+curDt.getMinutes()+""+curDt.getSeconds();
                $scope.pgmsAppRetirmntPen.appNo = "RAPN"+uniqCode;
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsAppRetirmntPenUpdate', result);
                $scope.isSaving = false;

                angular.forEach($scope.pgmsHrNomineeList,function(application)
                {
                    application.appRetirmntPenId = result;

                    if (application.id != null) {
                        PgmsAppRetirmntNmine.update(application);
                    } else {
                        if(application.nomineeStatus == true ){
                            console.log('Save Nominee');
                            PgmsAppRetirmntNmine.save(application);
                        }
                    }
                });

                angular.forEach($scope.pgmsAppRetirementAttachList,function(appattach){
                    appattach.appRetirmntPenInfo = result;
                    if (appattach.id != null) {
                        PgmsAppRetirmntAttach.update(appattach);
                    } else {
                        PgmsAppRetirmntAttach.save(appattach);
                    }
                });
                $scope.pgmsAppRetirmntCalculation.appRetirmntPenInfo = result;

                if ($scope.pgmsAppRetirmntCalculation.id != null) {
                    $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = 0;
                    PgmsAppRetirmntCalculation.update($scope.pgmsAppRetirmntCalculation);
                }
                else {
                    $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = 0;
                    PgmsAppRetirmntCalculation.save($scope.pgmsAppRetirmntCalculation);
                }
                $state.go('pgmsAppRetirmntPen');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                if ($scope.pgmsAppRetirmntPen.id != null) {
                    PgmsAppRetirmntPen.update($scope.pgmsAppRetirmntPen, onSaveFinished, onSaveError);
                    $rootScope.setWarningMessage('stepApp.pgmsAppRetirmntPen.updated');
                } else {
                    $scope.pgmsAppRetirmntPen.hrEmployeeInfo = $scope.hrEmployeeInfo;
                    PgmsAppRetirmntPen.save($scope.pgmsAppRetirmntPen, onSaveFinished, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.pgmsAppRetirmntPen.created');
                }
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.calculationInitiateModel = function()
            {
                return {
                    surrenderType: "50",
                    rateOfPension: null,
                    totalPenOrGr: null,
                    firstHalfOfPen: null,
                    exchangeRetirmntAl: null,
                    deservedRetirmntAl: null,
                    monthlyRetirmntAl: null,
                    dueAmountInfo: null,
                    remainingTotal: null,
                    id: null
                };
            };

            $scope.addMore = function()
            {
                $scope.pgmsHrNomineeList.push(
                    {
                        appRetirmntPenId: null,
                        nomineeStatus: true,
                        nomineeName: null,
                        gender: null,
                        relation: null,
                        dateOfBirth: null,
                        presentAddress: null,
                        nid: null,
                        occupation: null,
                        designation: null,
                        maritalStatus: null,
                        mobileNo: null,
                        getPension: null,
                        hrNomineeInfo: null,
                        birthCertificate: null,
                        certificateContentType: null,
                        certificateDocName: null,
                        id: null
                    }
                );
                $scope.addmorelist = 'addView';
            };
            $scope.showHideForm = function(viewPenApp, viewNomineeInfo,viewCalculationInfo,viewAttachInfo)
            {
                $scope.viewPensionAppForm = viewPenApp;
                $scope.viewPensionNomineeForm = viewNomineeInfo;
                $scope.viewPensionCalculationForm = viewCalculationInfo;
                $scope.viewPensionAttachForm = viewAttachInfo;
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

            $scope.abbreviate = DataUtils.abbreviate;
            $scope.byteSize = DataUtils.byteSize;

            $scope.setBirthCertificate = function ($file, pgmsAppRetirmntNmine) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppRetirmntNmine.birthCertificate = base64Data;
                            pgmsAppRetirmntNmine.certificateContentType = $file.type;
                            pgmsAppRetirmntNmine.certificateDocName = $file.name;
                        });
                    };
                }
            };

            $scope.setNomineeUpload = function ($file, pgmsAppRetirmntPhoto) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppRetirmntPhoto.nomineeUpload = base64Data;
                            pgmsAppRetirmntPhoto.nomineeContentType = $file.type;
                            pgmsAppRetirmntPhoto.nomineeDocName = $file.name;
                        });
                    };
                }
            };

            $scope.setAttachment = function ($file, pgmsAppRetirmntAttach) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            pgmsAppRetirmntAttach.attachment = base64Data;
                            pgmsAppRetirmntAttach.attachmentContentType = $file.type;
                            pgmsAppRetirmntAttach.attachDocName = $file.name;
                        });
                    };
                }
            };

            $scope.previewDoc = function (modelInfo)
            {
                console.log("Update Attach Infos : "+JSON.stringify(modelInfo));
                var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.nomineeDoc = function (certificateInfo)
            {
                var blob = $rootScope.b64toBlob(certificateInfo.birthCertificate, certificateInfo.certificateContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = certificateInfo.certificateContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Birth Certificate Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.nomineePhoto = function (photoInfo)
            {
                var blob = $rootScope.b64toBlob(photoInfo.nomineeUpload, photoInfo.nomineeContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = photoInfo.nomineeContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Nominee photo Info";
                $rootScope.showPreviewModal();
            };



            $scope.delete = function (modelInfo) {
                $scope.pgmsAppRetirmntAttach = modelInfo;
                $('#deletePgmsAppRetirmntAttachConfirmation').modal('show');
            };

            $scope.confirmDelete = function (modelInfo)
            {
                PgmsAppRetirmntAttach.delete({id: modelInfo.id},
                    function () {
                        $('#deletePgmsAppRetirmntAttachConfirmation').modal('hide');
                        $scope.clear(modelInfo);
                    });
            };

            $scope.bankInfoView = function(status,hrEmpBankInfo)
            {
                if(status == 'true')
                {
                    console.log('---------------99999999999');
                    $scope.pgmsAppRetirmntPen.bankName = hrEmpBankInfo.bankName.typeName;
                    $scope.pgmsAppRetirmntPen.bankAcc = hrEmpBankInfo.accountNumber;
                    $scope.pgmsAppRetirmntPen.bankBranch = hrEmpBankInfo.branchName;
                    $scope.pgmsAppRetirmntPen.bankAccStatus = true;
                    $scope.addBankInfo=false;
                }else if(status == 'false'){
                    $scope.pgmsAppRetirmntPen.bankName = '';
                    $scope.pgmsAppRetirmntPen.bankAcc = '';
                    $scope.pgmsAppRetirmntPen.bankBranch = '';
                    $scope.pgmsAppRetirmntPen.bankAccStatus = true;
                    $scope.addBankInfo=true;
                }
            };

           /* $scope.workDurationView = function(workInfo)
            {
                if(workInfo == 'willful')
                {
                    $scope.pgmsAppRetirmntPen.workDuration = null;
                }
            };*/

            $scope.calculationView = function(surrenderType)
            {
                //var lastPayCode = 9900;
                //var rate = 90;
                //var tprice =  lastPayCode*(90/100);
                //var fhalf = tprice/2;
                //var erallowance = 230;
                //var dseramout = fhalf*erallowance;
                //var festibalAlloance = 1000;
                //var medicalAllowance = 1000;

                if(surrenderType == '50')
                {
                    $scope.pgmsAppRetirmntCalculation.surrenderType = '50';
                    $scope.surrenderType100 = false;
                    //$scope.view50lebel = true;
                    //$scope.view100lebel = false;
                    //var mamount = fhalf;
                    //$scope.pgmsAppRetirmntCalculation.lastPayCodeElpcInfo= 9900;
                    //$scope.pgmsAppRetirmntCalculation.rateOfPension= 90;
                    //$scope.pgmsAppRetirmntCalculation.totalPenOrGr= tprice;
                    //$scope.pgmsAppRetirmntCalculation.firstHalfOfPen= fhalf;
                    //$scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl= erallowance;
                    //$scope.pgmsAppRetirmntCalculation.deservedRetirmntAl= dseramout;
                    //$scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl= mamount;
                    //$scope.pgmsAppRetirmntCalculation.dueAmountInfo= medicalAllowance;
                    //$scope.pgmsAppRetirmntCalculation.remainingTotal= festibalAlloance;
                }
                else
                {
                    $scope.surrenderType100 = true;
                    $scope.pgmsAppRetirmntCalculation.surrenderType == '100';
                    if($scope.pgmsAppRetirmntCalculation.surrenderType == '100'){
                        $scope.pgmsAppRetirmntCalculation.rateOfExForSecHalf = $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl/2;
                        $scope.pgmsAppRetirmntCalculation.deservedRetirementAllFor100 = $scope.pgmsAppRetirmntCalculation.secondHalfOfPen * $scope.pgmsAppRetirmntCalculation.rateOfExForSecHalf;
                        $scope.pgmsAppRetirmntCalculation.totalGratuity =  $scope.pgmsAppRetirmntCalculation.deservedRetirmntAl + $scope.pgmsAppRetirmntCalculation.deservedRetirementAllFor100;
                    }
                }
            };

            $scope.clear = function (modelInfo) {
                modelInfo.attachment= null;
                modelInfo.attachmentContentType= null;
                modelInfo.attachDocName= null;
                modelInfo.id= null;
            };

            $scope.hidePensionPercent = function(index){
                $scope.pensionPercent = true;
                $scope.retirmntNmine.getPension=false;

            }
            $scope.addMoreEmpBankInfo = function(){
                $scope.pgmsHrEmpBankInfos.push(
                    {
                        bankName: null,
                        accountNumber: null,
                        branchName:null,
                        id: null
                    }
                );
            };

            $scope.checkPensionPercent=function(){
                $scope.pensionPercent = 0;
                angular.forEach($scope.pgmsHrNomineeList,function(application){
                    if(application.nomineeStatus == true ){
                        $scope.pensionPercent = $scope.pensionPercent + application.getPension;
                    }
                });

                if($scope.pensionPercent > 100){
                    $scope.pensionPercentExcced = true;
                    $rootScope.setErrorMessage('Pension percent exceed the limit');
                    $scope.showHideForm(false,true,false,false);
                }else{
                    $scope.pensionPercentExcced = false;
                }
            }

            $scope.remove = function (result) {
                var index =  $scope.pgmsHrNomineeList.indexOf(result);
                $scope.pgmsHrNomineeList.splice(index,1);
                $scope.removeItems.push(result);
            }


        }]);
/*pgmsAppRetirmntPen-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsAppRetirmntPenDetailController',
        function ($scope, $rootScope, $sce, $stateParams, entity, PgmsAppRetirmntPen, HrEmployeeInfo,PgmsAppRetirmntNmine,
                  RetirementNomineeInfosByPenId, PgmsAppRetirementAttachByPenId, CalculationInfosByPenId, DataUtils,PgmsAppRetirementAttachByTypeAndPen,GetMedicalAllowanceByCurrentEmployee) {
            $scope.pgmsAppRetirmntPen = entity;
            $scope.pgmsHrNomineeList = [];
            $scope.pgmsAppRetirementAttachList = [];
            $scope.pgmsAppRetirmntCalculation = {};

            $scope.loadAllDataView = function ()
            {
                PgmsAppRetirmntPen.get({id: $stateParams.id}, function(basicData) {
                    $scope.pgmsAppRetirmntPen = basicData;
                });
                // RetirementNomineeInfosByPenId.get({penId:$stateParams.id},function(result) {
                RetirementNomineeInfosByPenId.get({penId:$stateParams.id},function(nomineeData){
                    $scope.pgmsHrNomineeList= nomineeData;
                });

                CalculationInfosByPenId.get({pensionId:$stateParams.id},function(result) {
                    $scope.pgmsAppRetirmntCalculation = result;
                    $scope.pgmsAppRetirmntCalculation.secondHalfOfPen = result.firstHalfOfPen;
                    $scope.pgmsAppRetirmntCalculation.deservedRetirmntAl = result.firstHalfOfPen * result.exchangeRetirmntAl;
                    $scope.pgmsAppRetirmntCalculation.monthlyRetirmntAl = $scope.pgmsAppRetirmntCalculation.secondHalfOfPen + " + Medical Allowance +" + " Festival Allowance ";
                    $scope.pgmsAppRetirmntCalculation.rateOfExForSecHalf = $scope.pgmsAppRetirmntCalculation.exchangeRetirmntAl/2;
                    $scope.pgmsAppRetirmntCalculation.deservedRetirementAllFor100 = $scope.pgmsAppRetirmntCalculation.secondHalfOfPen * $scope.pgmsAppRetirmntCalculation.rateOfExForSecHalf;
                    $scope.pgmsAppRetirmntCalculation.totalGratuity = $scope.pgmsAppRetirmntCalculation.deservedRetirmntAl + $scope.pgmsAppRetirmntCalculation.deservedRetirementAllFor100;
                    GetMedicalAllowanceByCurrentEmployee.get(function(result){
                        $scope.pgmsAppRetirmntCalculation.monthlyRetiAlSecHalf = result.basicMaximum;
                    });
                });

                PgmsAppRetirementAttachByTypeAndPen.query({attacheType:'retirement',retirementPenId:$stateParams.id},function(result) {
                    $scope.pgmsAppRetirementAttachList = result;
                });
            };
            $scope.loadAllDataView();

            $scope.load = function (id) {
                PgmsAppRetirmntPen.get({id: id}, function(result) {
                    $scope.pgmsAppRetirmntPen = result;
                });
            };

            var unsubscribe = $rootScope.$on('stepApp:pgmsAppRetirmntPenUpdate', function(event, result) {
                $scope.pgmsAppRetirmntPen = result;
            });
            $scope.$on('$destroy', unsubscribe);

            $scope.nomineeDoc = function (certificateInfo)
            {
                var blob = $rootScope.b64toBlob(certificateInfo.birthCertificate, certificateInfo.certificateContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = certificateInfo.certificateContentType;
                $rootScope.viewerObject.pageTitle = "Preview of Birth Certificate Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.previewDoc = function (modelInfo)
            {
                console.log("Attachment: "+modelInfo.attachmentContentType);

                var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                console.log("url: "+$rootScope.viewerObject.contentUrl);
                $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                $rootScope.showPreviewModal();
            };

            $scope.abbreviate = DataUtils.abbreviate;
            $scope.byteSize = DataUtils.byteSize;


        });
/*pgmsAppRetirmntPensionRequest.controller.js*/

angular.module('stepApp')
    .controller('pgmsAppRetirmntPensionRequestController',
        ['$scope', '$state','$sce','$rootScope', '$timeout', 'DataUtils','PgmsAppRetirmntPen','PgmsAppRetirementPending','RetirementNomineeInfosByPenId','PgmsAppRetirementAttachByPenId','CalculationInfosByPenId',
            'Principal', 'DateUtils','PgmsRetPensionAppByStatus','PensionAppApprovedListByRole',
            function($scope, $state, $sce, $rootScope, $timeout, DataUtils, PgmsAppRetirmntPen,  PgmsAppRetirementPending, RetirementNomineeInfosByPenId, PgmsAppRetirementAttachByPenId, CalculationInfosByPenId,
                     Principal,  DateUtils,PgmsRetPensionAppByStatus,PensionAppApprovedListByRole)
            {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.pgmsAppRetirementNomineeList = [];
                $scope.pgmsAppRetirementAttachList = [];
                $scope.pgmsAppRetirmntCalculation = {};
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;

                $scope.loggedInUser =   {};
                $scope.getLoggedInUser = function ()
                {
                    Principal.identity().then(function (account)
                    {
                        User.get({login: account.login}, function (result)
                        {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function()
                {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];
                    PgmsAppRetirementPending.query({statusType:2},function(result) {
                        $scope.newRequestList = result;
                    });

                    $scope.newRequestList.sort($scope.sortById);
                    $scope.loadApprovedRejectList();
                };

                $scope.sortById = function(a,b){
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function ()
                {
                    if($scope.approvalObj.isApproved)
                        $scope.approvalObj.aprvStatus='Approved';
                    else $scope.approvalObj.aprvStatus='Reject';
                    $scope.approvalObj.aprvDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.approvalObj.aprvBy = $scope.loggedInUser.id;

                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function ()
                {
                    if($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.aprvStatus = 'Approved';
                    }
                    else {
                        $scope.approvalObj.aprvStatus='Reject';
                    }
                    $scope.approvalObj.aprvDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.approvalObj.aprvBy = $scope.loggedInUser.id;

                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj){
                    PgmsAppRetirmntPen.update(requestObj, function(result)
                    {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        $scope.loadAll();
                    });
                };

                $scope.loadApprovedRejectList = function ()
                {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                    //PgmsRetPensionAppByStatus.query({statusType:6},function(result) {
                    PensionAppApprovedListByRole.query({},function(result){
                            $scope.approvedList= result;
                        },function(response)
                        {
                            console.log("data from view load failed");
                        }
                    );
                    PgmsRetPensionAppByStatus.query({statusType:0},function(result) {
                            $scope.rejectedList= result;
                        },function(response)
                        {
                            console.log("data from view load failed");
                        }
                    );
                };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source)
                {
                    if(source=='request')
                    {
                        $timeout(function(){
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };

                $scope.approvalViewDetail = function (dataObj)
                {
                    $scope.approvalObj = dataObj;
                    RetirementNomineeInfosByPenId.get({penId:dataObj.id},
                        function(result) {
                            $scope.pgmsAppRetirementNomineeList = result;
                        });

                    PgmsAppRetirementAttachByPenId.get({retirementPenId:dataObj.id},function(result) {
                        $scope.pgmsAppRetirementAttachList = result;
                    });

                    CalculationInfosByPenId.get({pensionId:dataObj.id},function(result) {
                        $scope.pgmsAppRetirmntCalculation = result;
                    });

                    /*PgmsAppRetirementAttachByTypeAndPen.get({attacheType:'retirement',retirementPenId:dataObj.id},
                     function(result) {
                     $scope.pgmsAppRetirementAttachList = result;
                     });*/

                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType){
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        entityName:null,
                        requestFrom:null,
                        requestSummary: null,
                        requestDate:null,
                        approveState: null,
                        aprvStatus:null,
                        logComments:null,
                        actionType:'',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function(event, data)
                {
                    $scope.loadAll();
                });

                $scope.sort = function(keyname, source){
                    if(source=='request')
                    {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if(source=='approved')
                    {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if(source=='rejected')
                    {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };

                $scope.abbreviate = DataUtils.abbreviate;
                $scope.byteSize = DataUtils.byteSize;

                $scope.nomineeDoc = function (certificateInfo)
                {
                    var blob = $rootScope.b64toBlob(certificateInfo.birthCertificate, certificateInfo.certificateContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = certificateInfo.certificateContentType;
                    $rootScope.viewerObject.pageTitle = "Preview of Birth Certificate Order Document";
                    $rootScope.showPreviewModal();
                };

                $scope.previewDoc = function (modelInfo)
                {
                    var blob = $rootScope.b64toBlob(modelInfo.attachment, modelInfo.attachmentContentType);
                    $rootScope.viewerObject.contentUrl = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                    $rootScope.viewerObject.contentType = modelInfo.attachmentContentType;
                    $rootScope.viewerObject.pageTitle = "Preview of Attachment Order Document";
                    $rootScope.showPreviewModal();
                };

                $scope.loadAll();

            }])
;

/*retirementApp-approve.controller.js*/
angular.module('stepApp')
    .controller('RetirementApplicationApproveController',
        ['$scope','$stateParams','$rootScope','entity','$state','AllForwaringLists','PgmsAppRetirmntPen','ForwardRetirePensionApplication',
            function ($scope,$stateParams,$rootScope,entity, $state,AllForwaringLists,PgmsAppRetirmntPen,ForwardRetirePensionApplication) {

                $scope.pgmsAppRetirmntPen = {};
                $scope.approveVal = false;
                $scope.forward = {};
                $scope.forwardShow = true;

                PgmsAppRetirmntPen.get({id : $stateParams.id},function(pensionApplication){
                    $scope.pgmsAppRetirmntPen = pensionApplication;
                    $scope.forwardShow = true;
                    console.log(')))))))))))))');
                    console.log(pensionApplication.approveComment);
                    if(pensionApplication.approveComment !=null && pensionApplication.approveComment == 'Waiting For Final Approve'){
                        $scope.forwardShow = false;
                    }
                });

                AllForwaringLists.query({'type':'General'},function(result){
                    console.log('&&&&&&&&&&&&&&&&&');
                    console.log(result);
                    $scope.forwardingList= result;
                });


                $scope.confirmApprove = function(forward){

                    if ($stateParams.id != null) {

                        if($scope.pgmsAppRetirmntPen.approveComment == 'Waiting For Final Approve'){
                            $scope.pgmsAppRetirmntPen.approveStatus = 6;
                            PgmsAppRetirmntPen.update($scope.pgmsAppRetirmntPen, onSaveSuccess, onSaveError);
                        }else{
                            ForwardRetirePensionApplication.forward({forwardTo:$scope.forward.forwardTo.code,cause: $scope.remark},$scope.pgmsAppRetirmntPen, onSaveSuccess, onSaveError);
                        }
                    }



                };
                //$scope.testfor = function(data){
                //    $scope.forwardTo = data;
                //    console.log(data);
                //};
                var onSaveSuccess = function(result){
                    //$modalInstance.close();
                    $rootScope.setSuccessMessage('Application Forwarded Successfully.');
                    $state.go('pgmsRetiremnetPensionAppPending', null, { reload: true });
                };
                var onSaveSuccess2 = function(result){
                    //$modalInstance.close();
                    $rootScope.setSuccessMessage('Application Approved Successfully.');
                    $state.go('mpo.details', null, { reload: true });
                };
                var onSaveError = function(result){
                    console.log(result);
                };
                $scope.clear = function(){
                    //$modalInstance.close();
                    window.history.back();
                    //$state.go('mpo.details');
                }

            }]);
