/*
'use strict';

angular.module('stepApp').controller('PgmsAppRetirmntCalculationDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsAppRetirmntCalculation', 'PgmsElpc', 'PgmsAppRetirmntPen',
        function($scope, $stateParams, $state, entity, PgmsAppRetirmntCalculation, PgmsElpc, PgmsAppRetirmntPen) {

        $scope.pgmsAppRetirmntCalculation = entity;
        $scope.pgmselpcs = PgmsElpc.query();
        $scope.pgmsappretirmntpens = PgmsAppRetirmntPen.query();
        $scope.load = function(id) {
            PgmsAppRetirmntCalculation.get({id : id}, function(result) {
                $scope.pgmsAppRetirmntCalculation = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:pgmsAppRetirmntCalculationUpdate', result);
            $scope.isSaving = false;
            $state.go("pgmsAppRetirmntCalculation");
        };

        $scope.save = function () {
            if ($scope.pgmsAppRetirmntCalculation.id != null) {
                PgmsAppRetirmntCalculation.update($scope.pgmsAppRetirmntCalculation, onSaveFinished);
            } else {
                PgmsAppRetirmntCalculation.save($scope.pgmsAppRetirmntCalculation, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $state.dismiss('cancel');
        };
}]);
*/
