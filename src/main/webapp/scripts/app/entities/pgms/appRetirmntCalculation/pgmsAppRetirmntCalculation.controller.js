'use strict';

angular.module('stepApp')
    .controller('PgmsAppRetirmntCalculationController', function ($scope, PgmsAppRetirmntCalculation, PgmsAppRetirmntCalculationSearch, ParseLinks) {
        $scope.pgmsAppRetirmntCalculations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsAppRetirmntCalculation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsAppRetirmntCalculations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsAppRetirmntCalculation.get({id: id}, function(result) {
                $scope.pgmsAppRetirmntCalculation = result;
                $('#deletePgmsAppRetirmntCalculationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsAppRetirmntCalculation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsAppRetirmntCalculationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsAppRetirmntCalculationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsAppRetirmntCalculations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsAppRetirmntCalculation = {
                surrenderType: null,
                rateOfPension: null,
                totalPenOrGr: null,
                firstHalfOfPen: null,
                exchangeRetirmntAl: null,
                deservedRetirmntAl: null,
                monthlyRetirmntAl: null,
                dueAmountInfo: null,
                remainingTotal: null,
                id: null
            };
        };
    });
/*pgmsAppRetirmntCalculation-dialog.controller.js*/

angular.module('stepApp').controller('PgmsAppRetirmntCalculationDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsAppRetirmntCalculation', 'PgmsElpc', 'PgmsAppRetirmntPen',
        function($scope, $stateParams, $state, entity, PgmsAppRetirmntCalculation, PgmsElpc, PgmsAppRetirmntPen) {

            $scope.pgmsAppRetirmntCalculation = entity;
            $scope.pgmselpcs = PgmsElpc.query();
            $scope.pgmsappretirmntpens = PgmsAppRetirmntPen.query();
            $scope.load = function(id) {
                PgmsAppRetirmntCalculation.get({id : id}, function(result) {
                    $scope.pgmsAppRetirmntCalculation = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsAppRetirmntCalculationUpdate', result);
                $scope.isSaving = false;
                $state.go("pgmsAppRetirmntCalculation");
            };

            $scope.save = function () {
                if ($scope.pgmsAppRetirmntCalculation.id != null) {
                    PgmsAppRetirmntCalculation.update($scope.pgmsAppRetirmntCalculation, onSaveFinished);
                } else {
                    PgmsAppRetirmntCalculation.save($scope.pgmsAppRetirmntCalculation, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };
        }]);
/*pgmsAppRetirmntCalculation-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsAppRetirmntCalculationDetailController', function ($scope, $rootScope, $stateParams, entity, PgmsAppRetirmntCalculation, PgmsElpc, PgmsAppRetirmntPen) {
        $scope.pgmsAppRetirmntCalculation = entity;
        $scope.load = function (id) {
            PgmsAppRetirmntCalculation.get({id: id}, function(result) {
                $scope.pgmsAppRetirmntCalculation = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pgmsAppRetirmntCalculationUpdate', function(event, result) {
            $scope.pgmsAppRetirmntCalculation = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
