'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('pgmsAppRetirmntCalculation', {
                parent: 'pgms',
                url: '/pgmsAppRetirmntCalculations',
                data: {
                    authorities: ['ROLE_HRM_USER'],
                    pageTitle: 'stepApp.pgmsAppRetirmntCalculation.home.title'
                },
                views: {
                    'pgmsHomeView@pgms': {
                        templateUrl: 'scripts/app/entities/pgms/appRetirmntCalculation/pgmsAppRetirmntCalculations.html',
                        controller: 'PgmsAppRetirmntCalculationController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pgmsAppRetirmntCalculation');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('pgmsAppRetirmntCalculation.detail', {
                parent: 'pgms',
                url: '/pgmsAppRetirmntCalculation/{id}',
                data: {
                    authorities: ['ROLE_HRM_USER'],
                    pageTitle: 'stepApp.pgmsAppRetirmntCalculation.detail.title'
                },
                views: {
                    'pgmsHomeView@pgms': {
                        templateUrl: 'scripts/app/entities/pgms/appRetirmntCalculation/pgmsAppRetirmntCalculation-detail.html',
                        controller: 'PgmsAppRetirmntCalculationDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pgmsAppRetirmntCalculation');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PgmsAppRetirmntCalculation', function($stateParams, PgmsAppRetirmntCalculation) {
                        return PgmsAppRetirmntCalculation.get({id : $stateParams.id});
                    }]
                }
            })
            .state('pgmsAppRetirmntCalculation.new', {
                parent: 'pgmsAppRetirmntCalculation',
                url: '/new',
                data: {
                    authorities: ['ROLE_HRM_USER'],
                },
                views: {
                     'pgmsHomeView@pgms': {
                      templateUrl: 'scripts/app/entities/pgms/appRetirmntCalculation/pgmsAppRetirmntCalculation-dialog.html',
                      controller: 'PgmsAppRetirmntCalculationDialogController'
                     }
                },
                resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pgmsAppRetirmntCalculation');
                        return $translate.refresh();
                      }],
                      entity: function () {
                              return {
                                    surrenderType: null,
                                    rateOfPension: null,
                                    totalPenOrGr: null,
                                    firstHalfOfPen: null,
                                    exchangeRetirmntAl: null,
                                    deservedRetirmntAl: null,
                                    monthlyRetirmntAl: null,
                                    dueAmountInfo: null,
                                    remainingTotal: null,
                                    id: null
                              }
                       }
                }
             })
            .state('pgmsAppRetirmntCalculation.edit', {
                parent: 'pgmsAppRetirmntCalculation',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_HRM_USER'],
                },
                views: {
                     'pgmsHomeView@pgms': {
                          templateUrl: 'scripts/app/entities/pgms/appRetirmntCalculation/pgmsAppRetirmntCalculation-dialog.html',
                          controller: 'PgmsAppRetirmntCalculationDialogController'
                     }
                },
                resolve: {
                     translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pgmsAppRetirmntCalculation');
                       return $translate.refresh();
                     }],
                     entity: ['$stateParams','PgmsAppRetirmntCalculation', function($stateParams, PgmsAppRetirmntCalculation) {
                               return PgmsAppRetirmntCalculation.get({id : $stateParams.id});
                     }]
                }
            });

    });
