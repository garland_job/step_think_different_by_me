'use strict';

angular.module('stepApp')
    .controller('PgmsRetirmntAttachInfoController',
    ['$scope','$rootScope', 'PgmsRetirmntAttachInfo', 'PgmsRetirmntAttachInfoSearch', 'ParseLinks',
    function ($scope, $rootScope, PgmsRetirmntAttachInfo, PgmsRetirmntAttachInfoSearch, ParseLinks) {
        $scope.pgmsRetirmntAttachInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsRetirmntAttachInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsRetirmntAttachInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsRetirmntAttachInfo.get({id: id}, function(result) {
                $scope.pgmsRetirmntAttachInfo = result;
                $('#deletePgmsRetirmntAttachInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsRetirmntAttachInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsRetirmntAttachInfoConfirmation').modal('hide');
                    $rootScope.setErrorMessage('stepApp.pgmsRetirmntAttachInfo.deleted');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsRetirmntAttachInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsRetirmntAttachInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsRetirmntAttachInfo = {
                attachName: null,
                priority: null,
                attachType: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    }]);
/*pgmsRetirmntAttachInfo-dialog.controller.js*/

angular.module('stepApp').controller('PgmsRetirmntAttachInfoDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'PgmsRetirmntAttachInfo', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, PgmsRetirmntAttachInfo, User, Principal, DateUtils) {

            $scope.pgmsRetirmntAttachInfo = entity;
            $scope.users = User.query({filter: 'pgmsPenGrSetup-is-null'});

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.load = function(id) {
                PgmsRetirmntAttachInfo.get({id : id}, function(result) {
                    $scope.pgmsRetirmntAttachInfo = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pgmsRetirmntAttachInfoUpdate', result);
                $scope.isSaving = false;
                $state.go("pgmsRetirmntAttachInfo");
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.pgmsRetirmntAttachInfo.updateBy = $scope.loggedInUser.id;
                $scope.pgmsRetirmntAttachInfo.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pgmsRetirmntAttachInfo.id != null) {
                    PgmsRetirmntAttachInfo.update($scope.pgmsRetirmntAttachInfo, onSaveFinished, onSaveError);
                    $rootScope.setWarningMessage('stepApp.pgmsRetirmntAttachInfo.updated');
                } else {
                    $scope.pgmsRetirmntAttachInfo.createBy = $scope.loggedInUser.id;
                    $scope.pgmsRetirmntAttachInfo.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PgmsRetirmntAttachInfo.save($scope.pgmsRetirmntAttachInfo, onSaveFinished, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.pgmsRetirmntAttachInfo.created');
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };
        }]);
/*pgmsRetirmntAttachInfo-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsRetirmntAttachInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsRetirmntAttachInfo',
            function ($scope, $rootScope, $stateParams, entity, PgmsRetirmntAttachInfo) {
                $scope.pgmsRetirmntAttachInfo = entity;
                $scope.load = function (id) {
                    PgmsRetirmntAttachInfo.get({id: id}, function(result) {
                        $scope.pgmsRetirmntAttachInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsRetirmntAttachInfoUpdate', function(event, result) {
                    $scope.pgmsRetirmntAttachInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
