/*
'use strict';

angular.module('stepApp').controller('PgmsPenRulesDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PgmsPenRules', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, PgmsPenRules, User, Principal, DateUtils) {

        $scope.pgmsPenRules = entity;
        $scope.users = User.query({filter: 'pgmsPenRules-is-null'});
        $scope.quotaTypeShow = true;
        $scope.maxAgeLimitError = false;
        $scope.maxAgeLimitErrorMessage = "";

        if($stateParams.id != null){
            PgmsPenRules.get({id : $stateParams.id }, function(result) {
                console.log('---');
                console.log(result.quotaStatus);
                $scope.pgmsPenRules = result;
                //if(result.quotaStatus == false){
                //    console.log('&&&&&&&&&&&&&&&&');
                //    $scope.pgmsPenRules.quotaStatus = false;
                //    $scope.quotaTypeShow = false;
                //}
            });
        }

        $scope.loggedInUser =   {};
        $scope.getLoggedInUser = function ()
        {
            Principal.identity().then(function (account)
            {
                User.get({login: account.login}, function (result)
                {
                    $scope.loggedInUser = result;
                });
            });
        };
        $scope.getLoggedInUser();

        $scope.load = function(id) {
            PgmsPenRules.get({id : id}, function(result) {
                $scope.pgmsPenRules = result;
            });
        };



        var onSaveFinished = function (result) {
            $rootScope.setSuccessMessage('stepApp.pgmsPenRules.created');
            //$scope.$emit('stepApp:pgmsPenRulesUpdate', result);
            $scope.isSaving = false;
            $state.go("pgmsPenRules");
        };
        var onSaveError = function (result) {
            $rootScope.setErrorMessage('stepApp.pgmsPenRules.SaveFailed');
             $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.pgmsPenRules.updateBy = $scope.loggedInUser.id;
            $scope.pgmsPenRules.updateDate = DateUtils.convertLocaleDateToServer(new Date());
            if ($scope.pgmsPenRules.id != null) {
                PgmsPenRules.update($scope.pgmsPenRules, onSaveFinished,onSaveError);
                $rootScope.setWarningMessage('stepApp.pgmsPenRules.updated');

            } else {
                if($scope.pgmsPenRules.quotaStatus == null){
                    $scope.pgmsPenRules.quotaStatus = true;
                }
                PgmsPenRules.save($scope.pgmsPenRules, onSaveFinished,onSaveError);
            }
        };

        $scope.clear = function() {
            $state.dismiss('cancel');
        };

        $scope.quotaStatusChange = function(status){
            if(status === 'false'){
            console.log('&&&&&&&&&&&&&&&&');
            $scope.quotaTypeShow = false;
            $scope.pgmsPenRules.quotaStatus = false;

            }else {
                $scope.quotaTypeShow = true;
                $scope.pgmsPenRules.quotaStatus = true;
            }
        }

         $scope.checkMaxGreaterThanMin = function(max,min){
             console.log("--------------------");
             $scope.maxAgeLimitError = false;

             console.log(max+","+min);

             if(max != null && min != null) {
                 console.log("++++++++++++++==");
                 if (max <= min) {
                     console.log("00000000000000000000");
                     $scope.maxAgeLimitError = true;
                     $scope.maxAgeLimitErrorMessage = "Min age is greater than max age";
                 }
             }
             //}else {
             //    $scope.maxAgeLimitError = true;
             //    $scope.maxAgeLimitErrorMessage = "Max Age or Min Age is Null";
             //}

         }
}]);
*/
