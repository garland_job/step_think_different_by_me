'use strict';

angular.module('stepApp')
    .controller('PgmsPenRulesController',

    ['$scope', '$rootScope', 'PgmsPenRules', 'PgmsPenRulesSearch', 'ParseLinks',
    function ($scope, $rootScope, PgmsPenRules, PgmsPenRulesSearch, ParseLinks) {
        $scope.pgmsPenRuless = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsPenRules.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsPenRuless = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsPenRules.get({id: id}, function(result) {
                $scope.pgmsPenRules = result;
                $('#deletePgmsPenRulesConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsPenRules.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsPenRulesConfirmation').modal('hide');
                    $rootScope.setErrorMessage('stepApp.pgmsPenRules.deleted');
                    $scope.clear();

                });

        };

        $scope.search = function () {
            PgmsPenRulesSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsPenRuless = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsPenRules = {
                quotaType: null,
                minAgeLimit: null,
                maxAgeLimit: null,
                minWorkDuration: null,
                disable: null,
                senile: null,
                activeStatus: true,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*pgmsPenRules-dialog.controller.js*/

angular.module('stepApp').controller('PgmsPenRulesDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PgmsPenRules', 'User', 'Principal', 'DateUtils',
        function($scope, $rootScope, $stateParams, $state, entity, PgmsPenRules, User, Principal, DateUtils) {

            $scope.pgmsPenRules = entity;
            $scope.users = User.query({filter: 'pgmsPenRules-is-null'});
            $scope.maxAgeLimitError = false;
            $scope.maxAgeLimitErrorMessage = "";

            if($stateParams.id != null){
                PgmsPenRules.get({id : $stateParams.id }, function(result) {
                    console.log('---');
                    console.log(result.quotaStatus);
                    $scope.pgmsPenRules = result;
                    console.log($scope.pgmsPenRules);
                    console.log($scope.pgmsPenRules.quotaStatus);
                    $scope.quotaStatusChange(result.quotaStatus);
                });
            }

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.load = function(id) {
                PgmsPenRules.get({id : id}, function(result) {
                    $scope.pgmsPenRules = result;
                });
            };

            var onSaveFinished = function (result) {
                $rootScope.setSuccessMessage('stepApp.pgmsPenRules.created');
                $scope.isSaving = false;
                $state.go("pgmsPenRules");
            };
            var onSaveError = function (result) {
                $rootScope.setErrorMessage('stepApp.pgmsPenRules.SaveFailed');
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.pgmsPenRules.updateBy = $scope.loggedInUser.id;
                $scope.pgmsPenRules.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pgmsPenRules.id != null) {
                    PgmsPenRules.update($scope.pgmsPenRules, onSaveFinished,onSaveError);
                    $rootScope.setWarningMessage('stepApp.pgmsPenRules.updated');

                } else {
                    if($scope.pgmsPenRules.quotaStatus == null){
                        $scope.pgmsPenRules.quotaStatus = true;
                    }
                    PgmsPenRules.save($scope.pgmsPenRules, onSaveFinished,onSaveError);
                }
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };

        }]);
/*pgmsPenRules-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsPenRulesDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsPenRules',
            function ($scope, $rootScope, $stateParams, entity, PgmsPenRules) {
                $scope.pgmsPenRules = entity;
                $scope.load = function (id) {
                    PgmsPenRules.get({id: id}, function(result) {
                        $scope.pgmsPenRules = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsPenRulesUpdate', function(event, result) {
                    $scope.pgmsPenRules = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
