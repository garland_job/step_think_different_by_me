'use strict';

angular.module('stepApp')
    .controller('PgmsPenGrCalculationController',
    ['$scope', 'PgmsPenGrCalculation', 'PgmsPenGrCalculationSearch', 'ParseLinks',
    function ($scope, PgmsPenGrCalculation, PgmsPenGrCalculationSearch, ParseLinks) {
        $scope.pgmsPenGrCalculations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PgmsPenGrCalculation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pgmsPenGrCalculations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PgmsPenGrCalculation.get({id: id}, function(result) {
                $scope.pgmsPenGrCalculation = result;
                $('#deletePgmsPenGrCalculationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PgmsPenGrCalculation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePgmsPenGrCalculationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PgmsPenGrCalculationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pgmsPenGrCalculations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pgmsPenGrCalculation = {
                withdrawnType: null,
                categoryType: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    }]);
/*pgmsPenGrCalculation-dialog.controller.js*/

angular.module('stepApp').controller('PgmsPenGrCalculationDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'PgmsPenGrCalculation', 'HrGradeSetupByStatus', 'PensionAndGratuityRateByWorkingYear','PayScaleBasicAmountByGrade', 'User', 'Principal',
        function($scope, $stateParams, $state, entity, PgmsPenGrCalculation, HrGradeSetupByStatus, PensionAndGratuityRateByWorkingYear,PayScaleBasicAmountByGrade, User, Principal) {

            $scope.pgmsPenGrCalculation = entity;
            $scope.pensionPercentage = 0;
            $scope.gratuityRate = 0;
            $scope.pensionTotal = 0;
            $scope.monthlyDrawn = 0;
            $scope.payScaleBasicAmounts = [];

            $scope.workingYears = [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
            HrGradeSetupByStatus.get({stat:true},function(gradeSetupData){
                $scope.hrgradesetups = gradeSetupData;
            });
            $scope.pensionParcentageList = {};
            $scope.gratuityRateList = {};

            $scope.users = User.query({filter: 'pgmsPenGrCalculation-is-null'});
            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.selCategory = function(selectType)
            {
                if(selectType == 'Contemporary')
                {
                    $scope.pgmsPenGrCalculation.categoryType = "gratuity";
                }
                else {
                    $scope.pgmsPenGrCalculation.categoryType = "pension";
                }

            }

            $scope.getTotalPension = function(withdrawnType,salaryScale,workingYear){


                PensionAndGratuityRateByWorkingYear.query({workYear:workingYear},function(result){

                    if(result[0].setupType == 'Pension'){
                        $scope.pensionPercentage = result[0].rateOfPenGr;
                        $scope.gratuityRate = result[1].rateOfPenGr;

                    }else if(result[1].setupType == 'Pension'){
                        $scope.pensionPercentage = result[1].rateOfPenGr;
                        $scope.gratuityRate = result[0].rateOfPenGr;
                    }

                    if($scope.pensionPercentage > 0 && $scope.gratuityRate > 0){
                        var pension = $scope.pensionPercentage/100;
                        $scope.pensionTotal = 0;
                        $scope.monthlyDrawn = 0;

                        if(withdrawnType == 'firstPart'){
                            $scope.pensionTotal =  ((salaryScale * pension)/2) * $scope.gratuityRate;
                            $scope.monthlyDrawn =  ((salaryScale * pension)/2);

                        }else if(withdrawnType == 'secondPart'){
                            $scope.pensionTotal =  ((((salaryScale * pension)/2) * $scope.gratuityRate) + (((salaryScale * pension)/2) * ($scope.gratuityRate/2)));
                            $scope.monthlyDrawn = 0;
                        }else{
                            $scope.pensionTotal =  0;
                            $scope.monthlyDrawn = 0;
                        }
                    }
                });
            };


            $scope.salaryInfo = function(gradeCode)
            {
                PayScaleBasicAmountByGrade.query({gradeId:gradeCode.id},function(result) {
                    $scope.payScaleBasicAmounts = result;

                });
            };

            $scope.clear = function() {
                $state.dismiss('cancel');
            };
        }]);
/*pgmsPenGrCalculation-detail.controller.js*/

angular.module('stepApp')
    .controller('PgmsPenGrCalculationDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'PgmsPenGrCalculation', 'HrGradeSetup', 'HrPayScaleSetup', 'PgmsPenGrRate',
            function ($scope, $rootScope, $stateParams, entity, PgmsPenGrCalculation, HrGradeSetup, HrPayScaleSetup, PgmsPenGrRate) {
                $scope.pgmsPenGrCalculation = entity;
                $scope.load = function (id) {
                    PgmsPenGrCalculation.get({id: id}, function(result) {
                        $scope.pgmsPenGrCalculation = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:pgmsPenGrCalculationUpdate', function(event, result) {
                    $scope.pgmsPenGrCalculation = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
