/*
'use strict';

angular.module('stepApp').controller('ErpAuthorityFlowDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ErpAuthorityFlow',
        function($scope, $stateParams, $modalInstance, entity, ErpAuthorityFlow) {

        $scope.erpAuthorityFlow = entity;
        $scope.load = function(id) {
            ErpAuthorityFlow.get({id : id}, function(result) {
                $scope.erpAuthorityFlow = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:erpAuthorityFlowUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.erpAuthorityFlow.id != null) {
                ErpAuthorityFlow.update($scope.erpAuthorityFlow, onSaveFinished);
            } else {
                ErpAuthorityFlow.save($scope.erpAuthorityFlow, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
*/
