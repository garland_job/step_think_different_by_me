'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('erpAuthorityFlow', {
                parent: 'entity',
                url: '/erpAuthorityFlows',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.erpAuthorityFlow.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/erpAuthorityFlow/erpAuthorityFlows.html',
                        controller: 'ErpAuthorityFlowController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('erpAuthorityFlow');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('erpAuthorityFlow.detail', {
                parent: 'entity',
                url: '/erpAuthorityFlow/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.erpAuthorityFlow.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/erpAuthorityFlow/erpAuthorityFlow-detail.html',
                        controller: 'ErpAuthorityFlowDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('erpAuthorityFlow');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ErpAuthorityFlow', function($stateParams, ErpAuthorityFlow) {
                        return ErpAuthorityFlow.get({id : $stateParams.id});
                    }]
                }
            })
            .state('erpAuthorityFlow.new', {
                parent: 'erpAuthorityFlow',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/erpAuthorityFlow/erpAuthorityFlow-dialog.html',
                        controller: 'ErpAuthorityFlowDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    authorityCategory: null,
                                    authorityType: null,
                                    actorName: null,
                                    activeStatus: null,
                                    createDate: null,
                                    createBy: null,
                                    updateDate: null,
                                    updateBy: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('erpAuthorityFlow', null, { reload: true });
                    }, function() {
                        $state.go('erpAuthorityFlow');
                    })
                }]
            })
            .state('erpAuthorityFlow.edit', {
                parent: 'erpAuthorityFlow',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/erpAuthorityFlow/erpAuthorityFlow-dialog.html',
                        controller: 'ErpAuthorityFlowDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ErpAuthorityFlow', function(ErpAuthorityFlow) {
                                return ErpAuthorityFlow.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('erpAuthorityFlow', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
