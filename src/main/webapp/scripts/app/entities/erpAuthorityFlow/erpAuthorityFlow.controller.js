'use strict';

angular.module('stepApp')
    .controller('ErpAuthorityFlowController', function ($scope, ErpAuthorityFlow, ErpAuthorityFlowSearch, ParseLinks) {
        $scope.erpAuthorityFlows = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            ErpAuthorityFlow.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.erpAuthorityFlows = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ErpAuthorityFlow.get({id: id}, function(result) {
                $scope.erpAuthorityFlow = result;
                $('#deleteErpAuthorityFlowConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ErpAuthorityFlow.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteErpAuthorityFlowConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ErpAuthorityFlowSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.erpAuthorityFlows = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.erpAuthorityFlow = {
                authorityCategory: null,
                authorityType: null,
                actorName: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*erpAuthorityFlow-dialog.controller.js*/

angular.module('stepApp').controller('ErpAuthorityFlowDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ErpAuthorityFlow',
        function($scope, $stateParams, $modalInstance, entity, ErpAuthorityFlow) {

            $scope.erpAuthorityFlow = entity;
            $scope.load = function(id) {
                ErpAuthorityFlow.get({id : id}, function(result) {
                    $scope.erpAuthorityFlow = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:erpAuthorityFlowUpdate', result);
                $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.erpAuthorityFlow.id != null) {
                    ErpAuthorityFlow.update($scope.erpAuthorityFlow, onSaveFinished);
                } else {
                    ErpAuthorityFlow.save($scope.erpAuthorityFlow, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*erpAuthorityFlow-detail.controller.js*/

angular.module('stepApp')
    .controller('ErpAuthorityFlowDetailController', function ($scope, $rootScope, $stateParams, entity, ErpAuthorityFlow) {
        $scope.erpAuthorityFlow = entity;
        $scope.load = function (id) {
            ErpAuthorityFlow.get({id: id}, function(result) {
                $scope.erpAuthorityFlow = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:erpAuthorityFlowUpdate', function(event, result) {
            $scope.erpAuthorityFlow = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
