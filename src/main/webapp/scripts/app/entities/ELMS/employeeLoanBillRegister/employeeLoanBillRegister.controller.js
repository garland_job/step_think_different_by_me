'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanBillRegisterController', function ($scope, EmployeeLoanBillRegister, EmployeeLoanBillRegisterSearch, ParseLinks) {
        $scope.employeeLoanBillRegisters = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EmployeeLoanBillRegister.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employeeLoanBillRegisters = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            EmployeeLoanBillRegister.get({id: id}, function(result) {
                $scope.employeeLoanBillRegister = result;
                $('#deleteEmployeeLoanBillRegisterConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            EmployeeLoanBillRegister.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmployeeLoanBillRegisterConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmployeeLoanBillRegisterSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.employeeLoanBillRegisters = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employeeLoanBillRegister = {
                applicationType: null,
                billNo: null,
                issueDate: null,
                receiverName: null,
                place: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*employeeLoanBillRegister-dialog.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanBillRegisterDialogController',
        ['$scope','$state', '$rootScope', '$stateParams' ,'$filter','entity', 'EmployeeLoanBillRegister', 'InstEmployee', 'EmployeeLoanRequisitionForm','CheckEmployeeEligibleForBillRegister',
            'findLoanRequisitionDataByHrEmpID','HrEmployeeInfoByEmployeeId','EmployeeLoanCheckRegister','FindBillRegisterByLoanRequiID','GetAllHrEmployeeInfo',
            function($scope,$state, $rootScope, $stateParams,$filter ,entity, EmployeeLoanBillRegister, InstEmployee, EmployeeLoanRequisitionForm,CheckEmployeeEligibleForBillRegister,
                     findLoanRequisitionDataByHrEmpID,HrEmployeeInfoByEmployeeId,EmployeeLoanCheckRegister,FindBillRegisterByLoanRequiID,GetAllHrEmployeeInfo) {

                $scope.employeeLoanBillRegister = entity;
                $scope.empInfoDiv = true;
                $scope.billRegisterShow = false;
                $scope.checkRegisterShow = false;
                $scope.hrEmployeeInfo = [];
                $scope.employeeLoanRequisitionForm = [];
                $scope.employeeLoanCheckRegister = entity;
                $scope.loanRequisitionCode = '';
                $scope.employeeName = '';
                $scope.employeeDesignation = '';
                $scope.employeeDept = '';
                $scope.formDiv = true;
                $scope.checkFormDiv = true;
                $scope.issueDateError = false;
                $scope.issueDateInvalid = true;

                $scope.load = function(id) {
                    EmployeeLoanBillRegister.get({id : id}, function(result) {
                        $scope.employeeLoanBillRegister = result;
                    });
                };
                $scope.registerShow = function(value){
                    if(value == 'billRegister'){
                        $scope.checkFormDiv = true;
                        $scope.formDiv = true;
                        $scope.empInfoDiv = true;
                        $scope.billRegisterShow = true;
                        $scope.checkRegisterShow = false;

                    }if(value == 'checkRegister'){
                        $scope.checkFormDiv = true;
                        $scope.formDiv = true;
                        $scope.empInfoDiv = true;
                        $scope.billRegisterShow = false;
                        $scope.checkRegisterShow = true;
                    }
                }
                GetAllHrEmployeeInfo.query({},function(result){
                    $scope.hremployeeinfos = result;
                });

                $scope.issueDateCheck = function(){
                    if($filter('date')(new Date($scope.employeeLoanCheckRegister.issueDate), 'yyyy-MM-dd') <  $scope.employeeLoanBillRegister.issueDate){
                        $scope.issueDateError = true;
                        $scope.issueDateInvalid = true;
                    }else{
                        $scope.issueDateError = false;
                        $scope.issueDateInvalid = false;
                    }
                }

                var onSaveFinished = function (result) {
                    $state.go('employeeLoanInfo.employeeLoanBillRegister',{},{reload:true});
                };

                $scope.save = function () {
                    if ($scope.employeeLoanBillRegister.id != null) {
                        EmployeeLoanBillRegister.update($scope.employeeLoanBillRegister, onSaveFinished);
                        $rootScope.setWarningMessage('stepApp.employeeLoanBillRegister.updated');
                    } else {
                        $scope.employeeLoanBillRegister.applicationType = $scope.employeeLoanRegister.applicationType
                        $scope.employeeLoanBillRegister.employeeInfo = $scope.hrEmployeeInfo;
                        $scope.employeeLoanBillRegister.loanRequisitionForm = $scope.employeeLoanRequisitionForm;
                        EmployeeLoanBillRegister.save($scope.employeeLoanBillRegister, onSaveFinished);
                        $rootScope.setSuccessMessage('stepApp.employeeLoanBillRegister.created');
                    }
                };

                // This Code to save Check Register
                $scope.saveEmployeeCheckRegister = function(){
                    if ($scope.employeeLoanCheckRegister.id != null) {
                        EmployeeLoanCheckRegister.update($scope.employeeLoanCheckRegister, onSaveFinished);
                    } else {
                        $scope.employeeLoanCheckRegister.applicationType = $scope.employeeLoanRegister.applicationType;
                        $scope.employeeLoanCheckRegister.loanBillRegister =  $scope.employeeLoanBillRegister;
                        $scope.employeeLoanCheckRegister.employeeInfo = $scope.hrEmployeeInfo;
                        $scope.employeeLoanCheckRegister.loanRequisitionForm = $scope.employeeLoanRequisitionForm;
                        EmployeeLoanCheckRegister.save($scope.employeeLoanCheckRegister, onSaveFinished);
                    }
                };

                $scope.clear = function() {

                };

                $scope.searchEmployee = function(employeeInfo){
                    $scope.formDiv = true;
                    $scope.checkFormDiv = true;
                    $scope.empInfoDiv = true;

                    //HrEmployeeInfoByEmployeeId.get({id: employeeId},function(hrEmployeeData){
                    //    console.log($scope.employeeLoanRegister.employeeId);
                    $scope.hrEmployeeInfo = employeeInfo;
                    $scope.employeeName = employeeInfo.fullName;
                    $scope.employeeDept = employeeInfo.departmentInfo.departmentInfo.departmentName;
                    $scope.employeeDesignation = employeeInfo.designationInfo.designationInfo.designationName;
                    CheckEmployeeEligibleForBillRegister.get({employeeInfoId: employeeInfo.id},function(result){
                        $scope.employeeLoanRequisitionForm = result;
                        $scope.loanRequisitionCode = result.loanRequisitionCode;
                        $scope.applyDate = result.createDate;
                        if($scope.loanRequisitionCode.length > 0){
                            if($scope.checkRegisterShow){
                                FindBillRegisterByLoanRequiID.get({loanRequisitionID:$scope.employeeLoanRequisitionForm.id},function(data){
                                    $scope.employeeLoanBillRegister = data;
                                    if(data.billNo){
                                        $scope.checkFormDiv = false;
                                        $scope.formDiv = true;
                                        $scope.empInfoDiv = false;
                                    }
                                });
                            }else{
                                $scope.formDiv = false;
                                $scope.empInfoDiv = false;
                                $scope.checkFormDiv = true;
                            }
                        }
                    });
                    //});
                }
            }]);
/*employeeLoanBillRegister-detail.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanBillRegisterDetailController', function ($scope, $rootScope, $stateParams, entity, EmployeeLoanBillRegister, InstEmployee, EmployeeLoanRequisitionForm) {
        $scope.employeeLoanBillRegister = entity;
        $scope.load = function (id) {
            EmployeeLoanBillRegister.get({id: id}, function(result) {
                $scope.employeeLoanBillRegister = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:employeeLoanBillRegisterUpdate', function(event, result) {
            $scope.employeeLoanBillRegister = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
