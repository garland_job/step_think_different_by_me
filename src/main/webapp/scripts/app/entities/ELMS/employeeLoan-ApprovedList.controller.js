'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanApprovedListController',
        ['$scope','$stateParams','Principal','FindLoanApprovedListByApplyType',
        function ($scope,$stateParams,Principal,FindLoanApprovedListByApplyType) {

        $scope.employeeLoanRequisitionForms = [];
        $scope.page = 0;
        $scope.employee = [];
        $scope.editLoanForm = true;

        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };

        if(Principal.hasAnyAuthority(['ROLE_ADMIN']) || Principal.hasAnyAuthority(['ROLE_DG'])){
            console.log('------------');
            FindLoanApprovedListByApplyType.query({applyType:$stateParams.applyType},function(result){
                   //  console.log('------------');
                $scope.employeeLoanRequisitionForms = result;
            });
        }

    }]);
