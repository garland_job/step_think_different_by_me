'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanAttachmentController', function ($scope, EmployeeLoanAttachment, EmployeeLoanAttachmentSearch, ParseLinks) {
        $scope.employeeLoanAttachments = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EmployeeLoanAttachment.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employeeLoanAttachments = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            EmployeeLoanAttachment.get({id: id}, function(result) {
                $scope.employeeLoanAttachment = result;
                $('#deleteEmployeeLoanAttachmentConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            EmployeeLoanAttachment.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmployeeLoanAttachmentConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmployeeLoanAttachmentSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.employeeLoanAttachments = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employeeLoanAttachment = {
                name: null,
                fileName: null,
                file: null,
                fileContentName: null,
                fileContentType: null,
                remarks: null,
                status: null,
                createBy: null,
                createDate: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    });
/*employeeLoanAttachment-dialog.controller.js*/

angular.module('stepApp').controller('EmployeeLoanAttachmentDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'EmployeeLoanAttachment', 'HrEmployeeInfo', 'AttachmentCategory',
        function($scope, $stateParams, $modalInstance, entity, EmployeeLoanAttachment, HrEmployeeInfo, AttachmentCategory) {

            $scope.employeeLoanAttachment = entity;
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.attachmentcategorys = AttachmentCategory.query();
            $scope.load = function(id) {
                EmployeeLoanAttachment.get({id : id}, function(result) {
                    $scope.employeeLoanAttachment = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:employeeLoanAttachmentUpdate', result);
                $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.employeeLoanAttachment.id != null) {
                    EmployeeLoanAttachment.update($scope.employeeLoanAttachment, onSaveFinished);
                } else {
                    EmployeeLoanAttachment.save($scope.employeeLoanAttachment, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }
                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }
                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }
                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setFile = function ($file, employeeLoanAttachment) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            employeeLoanAttachment.file = base64Data;
                            employeeLoanAttachment.fileContentType = $file.type;
                        });
                    };
                }
            };
        }]);
/*employeeLoanAttachment-detail.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanAttachmentDetailController', function ($scope, $rootScope, $stateParams, entity, EmployeeLoanAttachment, HrEmployeeInfo, AttachmentCategory) {
        $scope.employeeLoanAttachment = entity;
        $scope.load = function (id) {
            EmployeeLoanAttachment.get({id: id}, function(result) {
                $scope.employeeLoanAttachment = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:employeeLoanAttachmentUpdate', function(event, result) {
            $scope.employeeLoanAttachment = result;
        });
        $scope.$on('$destroy', unsubscribe);


        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    });
