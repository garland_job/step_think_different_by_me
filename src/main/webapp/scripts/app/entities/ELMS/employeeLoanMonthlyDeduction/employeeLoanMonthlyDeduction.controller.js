'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanMonthlyDeductionController', function ($scope, EmployeeLoanMonthlyDeduction, EmployeeLoanMonthlyDeductionSearch, ParseLinks) {
        $scope.employeeLoanMonthlyDeductions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EmployeeLoanMonthlyDeduction.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employeeLoanMonthlyDeductions = result;
                console.log($scope.employeeLoanMonthlyDeductions);
                console.log("deductions");
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            EmployeeLoanMonthlyDeduction.get({id: id}, function(result) {
                $scope.employeeLoanMonthlyDeduction = result;
                $('#deleteEmployeeLoanMonthlyDeductionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            EmployeeLoanMonthlyDeduction.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmployeeLoanMonthlyDeductionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmployeeLoanMonthlyDeductionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.employeeLoanMonthlyDeductions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employeeLoanMonthlyDeduction = {
                month: null,
                year: null,
                deductedAmount: null,
                reason: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*employeeLoanMonthlyDeduction-dialog.controller.js*/

angular.module('stepApp').controller('EmployeeLoanMonthlyDeductionDialogController',
    ['$scope', '$stateParams','$state' ,'$rootScope', '$q', 'entity', 'EmployeeLoanMonthlyDeduction', 'InstEmployee', 'EmployeeLoanRequisitionForm','findLoanRequisitionDataByHrEmpID','GetAllHrEmployeeInfo',
        'HrEmployeeInfoByEmployeeId','FindBillRegisterByBillNo','FindCheckRegisterByCheckNumber','GetApproveAmountByReqCode','CheckEmployeeEligibleForBillRegister',
        function($scope, $stateParams,$state ,$rootScope, $q, entity, EmployeeLoanMonthlyDeduction, InstEmployee, EmployeeLoanRequisitionForm,findLoanRequisitionDataByHrEmpID,GetAllHrEmployeeInfo,
                 HrEmployeeInfoByEmployeeId,FindBillRegisterByBillNo,FindCheckRegisterByCheckNumber,GetApproveAmountByReqCode,CheckEmployeeEligibleForBillRegister) {

            $scope.employeeLoanMonthlyDeduction = entity;
            $scope.employeeLoanRequisitionForm = [];
            $scope.hrEmployeeInfo = [];
            $scope.empInfoDiv = true;
            $scope.employeeloanrequisitionforms = EmployeeLoanRequisitionForm.query({filter: 'employeeloanmonthlydeduction-is-null'});
            $scope.loanRequisitionCode = '';
            $scope.employeeName = '';
            $scope.employeeDesignation = '';
            $scope.employeeDept = '';
            $scope.errorForBillRegister = false;
            $scope.errorForCheckRegister = false;
            $scope.errorForLoanAmount = false;

            $scope.load = function(id) {
                EmployeeLoanMonthlyDeduction.get({id : id}, function(result) {
                    $scope.employeeLoanMonthlyDeduction = result;
                });
            };
            GetAllHrEmployeeInfo.query({},function(result){
                $scope.hremployeeinfos = result;
            });

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:employeeLoanMonthlyDeductionUpdate', result);
                $state.go('employeeLoanInfo.employeeLoanMonthlyDeduction',{},{reload:true});
                //  $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.employeeLoanMonthlyDeduction.id != null) {
                    EmployeeLoanMonthlyDeduction.update($scope.employeeLoanMonthlyDeduction, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.employeeLoanMonthlyDeduction.updated');
                } else {
                    $scope.employeeLoanMonthlyDeduction.employeeInfo = $scope.hrEmployeeInfo;
                    $scope.employeeLoanMonthlyDeduction.loanRequisitionForm = $scope.employeeLoanRequisitionForm;
                    EmployeeLoanMonthlyDeduction.save($scope.employeeLoanMonthlyDeduction, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.employeeLoanMonthlyDeduction.created');
                }
            };

            $scope.clear = function() {
                //  $modalInstance.dismiss('cancel');
            };

            $scope.searchEmployee = function(employeeInfo){
                //HrEmployeeInfoByEmployeeId.get({id: $scope.employeeLoanMonthlyDeduction.employeeId},function(result){
                $scope.hrEmployeeInfo = employeeInfo;
                $scope.employeeName = employeeInfo.fullName;
                $scope.employeeDept = employeeInfo.departmentInfo.departmentInfo.departmentName;
                $scope.employeeDesignation = employeeInfo.designationInfo.designationInfo.designationName;

                CheckEmployeeEligibleForBillRegister.get({employeeInfoId: employeeInfo.id},function(data){
                    $scope.employeeLoanRequisitionForm = data;
                    $scope.loanRequisitionCode = data.loanRequisitionCode;
                    $scope.applyDate = data.createDate;
                    $scope.empInfoDiv = false;
                });
                //});
            };

            $scope.findBillRegister = function(){
                FindBillRegisterByBillNo.get({billNo:$scope.employeeLoanMonthlyDeduction.billNo},function(result){

                    if(result.employeeInfo.employeeId == $scope.employeeLoanMonthlyDeduction.employeeId){
                        console.log(result);
                        $scope.employeeLoanMonthlyDeduction.loanBillRegister =  result;
                    }else{
                        $scope.errorForBillRegister = true;
                    }
                });
            };
            $scope.findCheckRegister = function(){
                FindCheckRegisterByCheckNumber.get({checkNumber:$scope.employeeLoanMonthlyDeduction.checkNumber},function(result){
                    if(result.employeeInfo.employeeId == $scope.employeeLoanMonthlyDeduction.employeeId ){
                        console.log(result);
                        $scope.employeeLoanMonthlyDeduction.loanCheckRegister =  result;
                    }else {
                        $scope.errorForCheckRegister = true;
                    }
                });
            };

            // This function will check total Loan amount is equal to Loan Amount approved By DG

            $scope.ValidateTotalLoanAmount = function(){

                GetApproveAmountByReqCode.get({loanReqCode:$scope.employeeLoanRequisitionForm},function(result){
                    if(result.approvedLoanAmount == $scope.employeeLoanMonthlyDeduction.totalLoanAmountApproved){
                        $scope.errorForLoanAmount = false;
                    }else{
                        $scope.errorForLoanAmount = true;
                    }
                });

            }

        }]).directive('monthOptions', function() {
    return {
        restrict: 'A',
        template:
        '<option value="1">January</option>' +
        '<option value="2">February</option>' +
        '<option value="3">March</option>' +
        '<option value="4">April</option>' +
        '<option value="5">May</option>' +
        '<option value="6">June</option>' +
        '<option value="7">July</option>' +
        '<option value="8">August</option>' +
        '<option value="9">September</option>' +
        '<option value="10">October</option>' +
        '<option value="11">November</option>' +
        '<option value="12">December</option>'
    }
});
/*employeeLoanMonthlyDeduction-detail.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanMonthlyDeductionDetailController', function ($scope, $rootScope, $stateParams, entity, EmployeeLoanMonthlyDeduction, InstEmployee, EmployeeLoanRequisitionForm) {
        $scope.employeeLoanMonthlyDeduction = entity;
        $scope.load = function (id) {
            EmployeeLoanMonthlyDeduction.get({id: id}, function(result) {
                $scope.employeeLoanMonthlyDeduction = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:employeeLoanMonthlyDeductionUpdate', function(event, result) {
            $scope.employeeLoanMonthlyDeduction = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
