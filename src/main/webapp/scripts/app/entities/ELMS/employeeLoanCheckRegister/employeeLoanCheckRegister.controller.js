'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanCheckRegisterController', function ($scope, EmployeeLoanCheckRegister, EmployeeLoanCheckRegisterSearch, ParseLinks) {
        $scope.employeeLoanCheckRegisters = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EmployeeLoanCheckRegister.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employeeLoanCheckRegisters = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            EmployeeLoanCheckRegister.get({id: id}, function(result) {
                $scope.employeeLoanCheckRegister = result;
                $('#deleteEmployeeLoanCheckRegisterConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            EmployeeLoanCheckRegister.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmployeeLoanCheckRegisterConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmployeeLoanCheckRegisterSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.employeeLoanCheckRegisters = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employeeLoanCheckRegister = {
                applicationType: null,
                numberOfWithdrawal: null,
                checkNumber: null,
                tokenNumber: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*employeeLoanCheckRegister-dialog.controller.js*/

angular.module('stepApp').controller('EmployeeLoanCheckRegisterDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'EmployeeLoanCheckRegister', 'InstEmployee', 'EmployeeLoanRequisitionForm', 'EmployeeLoanBillRegister',
        function($scope, $stateParams, $modalInstance, $q, entity, EmployeeLoanCheckRegister, InstEmployee, EmployeeLoanRequisitionForm, EmployeeLoanBillRegister) {

            $scope.employeeLoanCheckRegister = entity;
            $scope.instemployees = InstEmployee.query();
            $scope.employeeloanrequisitionforms = EmployeeLoanRequisitionForm.query();
            $scope.employeeloanbillregisters = EmployeeLoanBillRegister.query({filter: 'employeeloancheckregister-is-null'});
            $q.all([$scope.employeeLoanCheckRegister.$promise, $scope.employeeloanbillregisters.$promise]).then(function() {
                if (!$scope.employeeLoanCheckRegister.employeeLoanBillRegister.id) {
                    return $q.reject();
                }
                return EmployeeLoanBillRegister.get({id : $scope.employeeLoanCheckRegister.employeeLoanBillRegister.id}).$promise;
            }).then(function(employeeLoanBillRegister) {
                $scope.employeeloanbillregisters.push(employeeLoanBillRegister);
            });
            $scope.load = function(id) {
                EmployeeLoanCheckRegister.get({id : id}, function(result) {
                    $scope.employeeLoanCheckRegister = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:employeeLoanCheckRegisterUpdate', result);
                $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.employeeLoanCheckRegister.id != null) {
                    EmployeeLoanCheckRegister.update($scope.employeeLoanCheckRegister, onSaveFinished);
                } else {
                    EmployeeLoanCheckRegister.save($scope.employeeLoanCheckRegister, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*employeeLoanCheckRegister-detail.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanCheckRegisterDetailController', function ($scope, $rootScope, $stateParams, entity, EmployeeLoanCheckRegister, InstEmployee, EmployeeLoanRequisitionForm, EmployeeLoanBillRegister) {
        $scope.employeeLoanCheckRegister = entity;
        $scope.load = function (id) {
            EmployeeLoanCheckRegister.get({id: id}, function(result) {
                $scope.employeeLoanCheckRegister = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:employeeLoanCheckRegisterUpdate', function(event, result) {
            $scope.employeeLoanCheckRegister = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
