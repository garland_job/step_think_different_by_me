'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanTypeSetupController', function ($scope, EmployeeLoanTypeSetup, EmployeeLoanTypeSetupSearch, ParseLinks) {
        $scope.employeeLoanTypeSetups = [];
        $scope.page = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.loadAll = function() {
            EmployeeLoanTypeSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employeeLoanTypeSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employeeLoanTypeSetup = {
                loanTypeCode: null,
                loanTypeName: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*employeeLoanTypeSetup-dialog.controller.js*/

angular.module('stepApp').controller('EmployeeLoanTypeSetupDialogController',
    ['$scope', '$state', '$rootScope', '$stateParams','entity', 'EmployeeLoanTypeSetup',
        function($scope,$state, $rootScope, $stateParams, entity, EmployeeLoanTypeSetup) {

            $scope.employeeLoanTypeSetup = entity;
            $scope.employeeLoanTypeSetup.status = true;
            $scope.load = function(id) {
                EmployeeLoanTypeSetup.get({id : id}, function(result) {
                    $scope.employeeLoanTypeSetup = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:employeeLoanTypeSetupUpdate', result);
                $scope.isSaving = false;
                $state.go('employeeLoanInfo.employeeLoanTypeSetup',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                if ($scope.employeeLoanTypeSetup.id != null) {
                    EmployeeLoanTypeSetup.update($scope.employeeLoanTypeSetup,  onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.employeeLoanTypeSetup.updated');
                } else {
                    EmployeeLoanTypeSetup.save($scope.employeeLoanTypeSetup,  onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.employeeLoanTypeSetup.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*employeeLoanTypeSetup-detail.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanTypeSetupDetailController', function ($scope, $rootScope, $stateParams, entity, EmployeeLoanTypeSetup) {
        $scope.employeeLoanTypeSetup = entity;
        $scope.load = function (id) {
            EmployeeLoanTypeSetup.get({id: id}, function(result) {
                $scope.employeeLoanTypeSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:employeeLoanTypeSetupUpdate', function(event, result) {
            $scope.employeeLoanTypeSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*employeeLoanTypeSetup-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanTypeSetupDeleteController',
        ['$scope','$state', '$rootScope', 'entity', 'EmployeeLoanTypeSetup',
            function($scope,$state, $rootScope, entity, EmployeeLoanTypeSetup) {

                $scope.employeeLoanTypeSetup = entity;
                $scope.clear = function() {
                    // $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    $state.go('employeeLoanInfo.employeeLoanTypeSetup',{},{reload:true});
                    EmployeeLoanTypeSetup.delete({id: id},
                        function () {
                            //  $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.employeeLoanTypeSetup.deleted');

                        });


                };

            }]);
