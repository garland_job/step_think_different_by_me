'use strict';

angular.module('stepApp')
    .controller('EmployeeLoanApproveAndForwardController', function ($scope, EmployeeLoanApproveAndForward, EmployeeLoanApproveAndForwardSearch, ParseLinks) {
        $scope.employeeLoanApproveAndForwards = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            EmployeeLoanApproveAndForward.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.employeeLoanApproveAndForwards = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            EmployeeLoanApproveAndForward.get({id: id}, function(result) {
                $scope.employeeLoanApproveAndForward = result;
                $('#deleteEmployeeLoanApproveAndForwardConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            EmployeeLoanApproveAndForward.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmployeeLoanApproveAndForwardConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmployeeLoanApproveAndForwardSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.employeeLoanApproveAndForwards = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.employeeLoanApproveAndForward = {
                comments: null,
                approveStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*employeeLoanApproveAndForward-dialog.controller.js*/

angular.module('stepApp').controller('EmployeeLoanApproveAndForwardDialogController',
    ['$scope', '$stateParams','$modalInstance','$state' ,'entity', 'EmployeeLoanApproveAndForward', 'EmployeeLoanRequisitionForm','Authority','Principal','GetForwardMessageByReqIdAndApproveStatus',
        'GetEmployeeLoanVocApprovalRole','GetEmployeeLoanOthersApprovalRole','GetEmployeeLoanDteApprovalRole',
        function($scope, $stateParams,$modalInstance,$state ,entity, EmployeeLoanApproveAndForward, EmployeeLoanRequisitionForm,Authority,Principal,GetForwardMessageByReqIdAndApproveStatus,
                 GetEmployeeLoanVocApprovalRole,GetEmployeeLoanOthersApprovalRole,GetEmployeeLoanDteApprovalRole) {

            $scope.employeeLoanApproveAndForward = {};
            $scope.authorityHide = false;
            $scope.approveLoanAmountDiv = false;
            $scope.approveLoanInstallmentDiv = false;
            $scope.authoritys = [];
            //$scope.employeeloanrequisitionforms = EmployeeLoanRequisitionForm.query();

            //$scope.authoritys = [{name:'ROLE_INSTITUTE'},{name:'ROLE_AD'},{name:'ROLE_DIRECTOR'},{name:'ROLE_DG'}];

            EmployeeLoanRequisitionForm.get({id:$stateParams.loanReqId},function(result){

                //GetForwardMessageByReqIdAndApproveStatus.get({approveStatus : result.approveStatus},function(result){
                //    $scope.employeeLoanApproveAndForward.approveFrom = result.prev ;
                //    $scope.employeeLoanApproveAndForward.forwardToAuthority = result.next;
                //});
                $scope.employeeLoanApproveAndForward.loanRequisitionForm = result;
                console.log('--------'+$scope.employeeLoanApproveAndForward.loanRequisitionForm.applyType);
                if($scope.employeeLoanApproveAndForward.loanRequisitionForm.applyType == "VOC"){
                    GetEmployeeLoanVocApprovalRole.query(function(result){
                        $scope.authoritys = result;
                        console.log($scope.authoritys);
                    });
                }else if($scope.employeeLoanApproveAndForward.loanRequisitionForm.applyType == "Others"){
                    GetEmployeeLoanOthersApprovalRole.query(function(result){
                        $scope.authoritys = result;
                        console.log($scope.authoritys);
                    });
                }else if($scope.employeeLoanApproveAndForward.loanRequisitionForm.applyType == "DTE"){
                    GetEmployeeLoanDteApprovalRole.query(function(result){
                        $scope.authoritys = result;
                    });
                }else {
                    $scope.authoritys = null;
                }
            });

            //if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
            //    $scope.authoritys.splice(0,1);
            //}else if(Principal.hasAnyAuthority(['ROLE_AD'])){
            //    $scope.authoritys.splice(1,1);
            //}else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
            //    $scope.authoritys.splice(2,1);
            //}else if(Principal.hasAnyAuthority(['ROLE_DG'])){
            //    $scope.authoritys.push([{name:null}]);
            //    $scope.authorityHide = true;
            //    $scope.approveLoanAmountDiv = true;
            //    $scope.approveLoanInstallmentDiv = true;
            //    $scope.authoritys.push([{name:'ROLE_USER'}]);
            //}

            if(Principal.hasAnyAuthority(['ROLE_DG'])){
                $scope.authorityHide = true;
            }

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:employeeLoanApproveAndForwardUpdate', result);
                $modalInstance.close();
                $state.go('employeeLoanInfo.dashboard',{},{reload:true});
            };

            $scope.save = function () {
                EmployeeLoanRequisitionForm.get({id:$stateParams.loanReqId},function(result){
                    $scope.employeeLoanApproveAndForward.loanRequisitionForm = result;
                    if(Principal.hasAnyAuthority(['ROLE_DG'])){
                        EmployeeLoanApproveAndForward.save($scope.employeeLoanApproveAndForward, onSaveFinished);
                    }else{
                        console.log('+++++++++++++++++');
                        console.log($scope.employeeLoanApproveAndForward.forwardToAuthority.role);
                        $scope.employeeLoanApproveAndForward.forwardToAuthority = $scope.employeeLoanApproveAndForward.forwardToAuthority.role;
                        EmployeeLoanApproveAndForward.save($scope.employeeLoanApproveAndForward, onSaveFinished);
                    }


                });
            };

            $scope.clear = function() {
                $modalInstance.close();
                $state.go('employeeLoanInfo.employeeLoanReqPending',{},{reload:true});
            };

        }])
    .controller('EmployeeLoanDeclineController',
        ['$scope','$stateParams' ,'$modalInstance', 'EmployeeLoanApproveAndForward', '$state', 'entity', 'EmployeeLoanRequisitionForm','EmployeeLoanDecline',
            function($scope,$stateParams ,$modalInstance, EmployeeLoanApproveAndForward, $state, entity, EmployeeLoanRequisitionForm,EmployeeLoanDecline) {

                $scope.decline = function(){
                    EmployeeLoanRequisitionForm.get({id:$stateParams.loanReqId},function(result){
                        $scope.employeeLoanApproveAndForward.loanRequisitionForm = result;
                        $scope.employeeLoanApproveAndForward.comments = "Loan Application Decline";
                        EmployeeLoanDecline.save($scope.employeeLoanApproveAndForward,onSaveFinished);
                    });
                };

                var onSaveFinished = function (result) {
                    // $scope.$emit('stepApp:employeeLoanApproveAndForwardUpdate', result);
                    $modalInstance.close();
                    $state.go('employeeLoanInfo.employeeLoanReqPending',{},{reload:true});
                };

                $scope.clear = function() {
                    $modalInstance.close();
                };
            }]);
/*employeeLoanApproveAndForward-detail.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanApproveAndForwardDetailController', function ($scope, $rootScope, $stateParams, entity, EmployeeLoanApproveAndForward, EmployeeLoanRequisitionForm, Authority) {
        $scope.employeeLoanApproveAndForward = entity;
        $scope.load = function (id) {
            EmployeeLoanApproveAndForward.get({id: id}, function(result) {
                $scope.employeeLoanApproveAndForward = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:employeeLoanApproveAndForwardUpdate', function(event, result) {
            $scope.employeeLoanApproveAndForward = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*employeeLoanReject-dialog.controller.js*/

angular.module('stepApp')
    .controller('EmployeeLoanRejectDialogController',
        ['$scope', '$modalInstance', 'EmployeeLoanApproveAndForward', '$state', 'entity', 'EmployeeLoanReject','$stateParams','EmployeeLoanRequisitionForm',
            function($scope, $modalInstance, EmployeeLoanApproveAndForward, $state, entity, EmployeeLoanReject,$stateParams,EmployeeLoanRequisitionForm) {
                $scope.employeeLoanApproveAndForward = entity;


                var onSaveSuccess = function (result) {
                    //$scope.$emit('stepApp:instituteUpdate', result);
                    $modalInstance.close(result);
                    $scope.isSaving = false;
                    $state.go('employeeLoanInfo.employeeLoanReqPending',{},{reload:true});
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.clear = function() {
                    $modalInstance.close();
                };
                $scope.confirmReject = function () {
                    $scope.isSaving = true;
                    console.log('Reject-- '+$stateParams.id);
                    EmployeeLoanRequisitionForm.get({id:$stateParams.id},function(result){
                        $scope.employeeLoanApproveAndForward.loanRequisitionForm = result;
                        EmployeeLoanReject.save($scope.employeeLoanApproveAndForward, onSaveSuccess, onSaveError);
                    });

                };



            }]);
