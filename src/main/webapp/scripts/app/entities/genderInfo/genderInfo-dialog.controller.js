'use strict';

angular.module('stepApp').controller('GenderInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'GenderInfo',
        function($scope, $stateParams, $modalInstance, entity, GenderInfo) {

        $scope.genderInfo = entity;
        $scope.load = function(id) {
            GenderInfo.get({id : id}, function(result) {
                $scope.genderInfo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:genderInfoUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.genderInfo.id != null) {
                GenderInfo.update($scope.genderInfo, onSaveSuccess, onSaveError);
            } else {
                GenderInfo.save($scope.genderInfo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
