'use strict';

angular.module('stepApp')
    .controller('GenderInfoController', function ($scope, $state, $modal, GenderInfo, GenderInfoSearch, ParseLinks) {
      
        $scope.genderInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            GenderInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.genderInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            GenderInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.genderInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.genderInfo = {
                name: null,
                status: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    });
