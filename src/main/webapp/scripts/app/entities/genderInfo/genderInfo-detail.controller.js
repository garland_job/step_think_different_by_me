'use strict';

angular.module('stepApp')
    .controller('GenderInfoDetailController', function ($scope, $rootScope, $stateParams, entity, GenderInfo) {
        $scope.genderInfo = entity;
        $scope.load = function (id) {
            GenderInfo.get({id: id}, function(result) {
                $scope.genderInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:genderInfoUpdate', function(event, result) {
            $scope.genderInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
