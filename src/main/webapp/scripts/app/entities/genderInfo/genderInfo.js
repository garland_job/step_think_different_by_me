'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('genderInfo', {
                parent: 'entity',
                url: '/genderInfos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.genderInfo.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/genderInfo/genderInfos.html',
                        controller: 'GenderInfoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('genderInfo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('genderInfo.detail', {
                parent: 'entity',
                url: '/genderInfo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.genderInfo.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/genderInfo/genderInfo-detail.html',
                        controller: 'GenderInfoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('genderInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'GenderInfo', function($stateParams, GenderInfo) {
                        return GenderInfo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('genderInfo.new', {
                parent: 'genderInfo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/genderInfo/genderInfo-dialog.html',
                        controller: 'GenderInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    status: null,
                                    createDate: null,
                                    createBy: null,
                                    updateBy: null,
                                    updateDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('genderInfo', null, { reload: true });
                    }, function() {
                        $state.go('genderInfo');
                    })
                }]
            })
            .state('genderInfo.edit', {
                parent: 'genderInfo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/genderInfo/genderInfo-dialog.html',
                        controller: 'GenderInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['GenderInfo', function(GenderInfo) {
                                return GenderInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('genderInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('genderInfo.delete', {
                parent: 'genderInfo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/genderInfo/genderInfo-delete-dialog.html',
                        controller: 'GenderInfoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['GenderInfo', function(GenderInfo) {
                                return GenderInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('genderInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
