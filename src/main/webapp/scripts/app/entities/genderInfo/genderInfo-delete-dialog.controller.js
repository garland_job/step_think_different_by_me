'use strict';

angular.module('stepApp')
	.controller('GenderInfoDeleteController', function($scope, $modalInstance, entity, GenderInfo) {

        $scope.genderInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            GenderInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });