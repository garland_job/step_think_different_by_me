'use strict';

angular.module('stepApp')
    .controller('RisNewVacancyController',
    ['$scope', 'RisNewVacancy', 'RisNewVacancySearch', 'ParseLinks',
    function ($scope, RisNewVacancy, RisNewVacancySearch, ParseLinks) {
        $scope.risNewVacancys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            RisNewVacancy.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.risNewVacancys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            RisNewVacancy.get({id: id}, function(result) {
                $scope.risNewVacancy = result;
                $('#deleteRisNewVacancyConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            RisNewVacancy.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteRisNewVacancyConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            RisNewVacancySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.risNewVacancys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.risNewVacancy = {
                vacancyNo: null,
                educationalQualification: null,
                otherQualification: null,
                remarks: null,
                publishDate: null,
                applicationDate: null,
                attachment: null,
                attachmentContentType: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    }]);
/*risNewVacancy-dialog.controller.js*/

angular.module('stepApp').controller('RisNewVacancyDialogController',
    ['$scope','$state', '$stateParams', 'entity', 'RisNewVacancy', 'HrDepartmentSetup',
        function($scope, $state, $stateParams, entity, RisNewVacancy, HrDepartmentSetup) {

            $scope.risNewVacancy = entity;
            $scope.hrdepartmentsetups = HrDepartmentSetup.query();
            $scope.load = function(id) {
                RisNewVacancy.get({id : id}, function(result) {
                    $scope.risNewVacancy = result;
                });
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:risNewVacancyUpdate', result);
                $state.go('risNewVacancy', null, { reload: true });

            };

            $scope.save = function () {
                if ($scope.risNewVacancy.id != null) {
                    RisNewVacancy.update($scope.risNewVacancy, onSaveFinished);
                } else {
                    RisNewVacancy.save($scope.risNewVacancy, onSaveFinished);
                }
            };

            $scope.clear = function() {

            };

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }
                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }
                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }
                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setAttachment = function ($file, risNewVacancy) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            risNewVacancy.attachment = base64Data;
                            risNewVacancy.attachmentImgName = $file.name;
                            risNewVacancy.attachmentContentType = $file.type;
                        });
                    };
                }
            };
        }]);
/*risNewVacancy-detail.controller.js*/

angular.module('stepApp')
    .controller('RisNewVacancyDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'RisNewVacancy', 'HrDepartmentSetup',
            function ($scope, $rootScope, $stateParams, entity, RisNewVacancy, HrDepartmentSetup) {
                $scope.risNewVacancy = entity;
                $scope.load = function (id) {
                    RisNewVacancy.get({id: id}, function(result) {
                        $scope.risNewVacancy = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:risNewVacancyUpdate', function(event, result) {
                    $scope.risNewVacancy = result;
                });
                $scope.$on('$destroy', unsubscribe);


                $scope.byteSize = function (base64String) {
                    if (!angular.isString(base64String)) {
                        return '';
                    }
                    function endsWith(suffix, str) {
                        return str.indexOf(suffix, str.length - suffix.length) !== -1;
                    }
                    function paddingSize(base64String) {
                        if (endsWith('==', base64String)) {
                            return 2;
                        }
                        if (endsWith('=', base64String)) {
                            return 1;
                        }
                        return 0;
                    }
                    function size(base64String) {
                        return base64String.length / 4 * 3 - paddingSize(base64String);
                    }
                    function formatAsBytes(size) {
                        return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                    }

                    return formatAsBytes(size(base64String));
                };
            }]);
