'use strict';

angular.module('stepApp')
    .controller('RisNewAppFormTwoController',
    ['$scope', 'RisNewAppFormTwo', 'RisNewAppFormTwoSearch', 'ParseLinks',
    function ($scope, RisNewAppFormTwo, RisNewAppFormTwoSearch, ParseLinks) {
        $scope.risNewAppFormTwos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            RisNewAppFormTwo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.risNewAppFormTwos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            RisNewAppFormTwo.get({id: id}, function(result) {
                $scope.risNewAppFormTwo = result;
                $('#deleteRisNewAppFormTwoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            RisNewAppFormTwo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteRisNewAppFormTwoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            RisNewAppFormTwoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.risNewAppFormTwos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.risNewAppFormTwo = {
                examName: null,
                subject: null,
                educationalInstitute: null,
                passingYear: null,
                boardUniversity: null,
                additionalInformation: null,
                experience: null,
                qouta: null,
                bankDraftNo: null,
                dateFinDocument: null,
                bankName: null,
                branchName: null,
                departmentalCandidate: null,
                bankInvoice: null,
                bankInvoiceContentType: null,
                signature: null,
                signatureContentType: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    }]);
/*risNewAppFormTwo-dialog.controller.js*/

angular.module('stepApp').controller('RisNewAppFormTwoDialogController',
    ['$scope','$state', '$stateParams', 'entity', 'RisNewAppFormTwo',
        function($scope,$state, $stateParams, entity, RisNewAppFormTwo) {

            $scope.risNewAppFormTwo = entity;
            $scope.load = function(id) {
                RisNewAppFormTwo.get({id : id}, function(result) {
                    $scope.risNewAppFormTwo = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:risNewAppFormTwoUpdate', result);
                $state.go('risNew.risNewAppFormTwo', null, { reload: true });
            };

            $scope.save = function () {
                if ($scope.risNewAppFormTwo.id != null) {
                    RisNewAppFormTwo.update($scope.risNewAppFormTwo, onSaveFinished);
                } else {
                    RisNewAppFormTwo.save($scope.risNewAppFormTwo, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $state.go('risNew.risNewAppFormTwo', null, { reload: true });
            };

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }
                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }
                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }
                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setBankInvoice = function ($file, risNewAppFormTwo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            risNewAppFormTwo.bankInvoice = base64Data;
                            risNewAppFormTwo.bankInvoiceContentType = $file.type;
                        });
                    };
                }
            };

            $scope.setSignature = function ($file, risNewAppFormTwo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            risNewAppFormTwo.signature = base64Data;
                            risNewAppFormTwo.signatureContentType = $file.type;
                        });
                    };
                }
            };
        }]);
/*risNewAppFormTwo-detail.controller.js*/

angular.module('stepApp')
    .controller('RisNewAppFormTwoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'RisNewAppFormTwo',
            function ($scope, $rootScope, $stateParams, entity, RisNewAppFormTwo) {
                $scope.risNewAppFormTwo = entity;
                $scope.load = function (id) {
                    RisNewAppFormTwo.get({id: id}, function(result) {
                        $scope.risNewAppFormTwo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:risNewAppFormTwoUpdate', function(event, result) {
                    $scope.risNewAppFormTwo = result;
                });
                $scope.$on('$destroy', unsubscribe);


                $scope.byteSize = function (base64String) {
                    if (!angular.isString(base64String)) {
                        return '';
                    }
                    function endsWith(suffix, str) {
                        return str.indexOf(suffix, str.length - suffix.length) !== -1;
                    }
                    function paddingSize(base64String) {
                        if (endsWith('==', base64String)) {
                            return 2;
                        }
                        if (endsWith('=', base64String)) {
                            return 1;
                        }
                        return 0;
                    }
                    function size(base64String) {
                        return base64String.length / 4 * 3 - paddingSize(base64String);
                    }
                    function formatAsBytes(size) {
                        return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                    }

                    return formatAsBytes(size(base64String));
                };
            }]);
