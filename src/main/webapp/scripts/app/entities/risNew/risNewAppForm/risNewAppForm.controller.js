'use strict';

angular.module('stepApp')
    .controller('RisNewAppFormController',
    ['$scope', '$state', '$rootScope', 'RisNewAppForm', 'RisNewAppFormSearch', 'ParseLinks', 'getEmployed', 'userregistration', 'jobPosting', 'TestHello',
        'getApplicantsByDesignation', 'HrDesignationSetup', 'sendingEmail', 'findById', 'smsWritten', 'seatwithcircular', 'gettingwithcircular', 'sendingEmailAppointmentLetter', 'getOneApplicantByStatus',
        'Selection', 'getApplicantsByStatus', 'Auth', 'User', 'getjobRequest', 'jobRequestUpdate', 'jobRequest', 'getCircularNumber', 'jobRequestUpdateByCircularNo', 'getJobCircular', 'getJobByCircular',
        'getJobByCircularStatus', 'getApplicantByRegNo', 'getapplicantbycircularandstatus', 'uniqueCircular', '$timeout', 'getalljobPosting', 'getjobPosting', 'risNewAppFormsbyuser', 'Principal', 'emailSend',
        function ($scope, $state, $rootScope, RisNewAppForm, RisNewAppFormSearch, ParseLinks, getEmployed, userregistration, jobPosting, TestHello,
                  getApplicantsByDesignation, HrDesignationSetup, sendingEmail, findById, smsWritten, seatwithcircular, gettingwithcircular, sendingEmailAppointmentLetter, getOneApplicantByStatus,
                  Selection, getApplicantsByStatus, Auth, User, getjobRequest, jobRequestUpdate, jobRequest, getCircularNumber, jobRequestUpdateByCircularNo, getJobCircular, getJobByCircular
            , getJobByCircularStatus, getApplicantByRegNo, getapplicantbycircularandstatus, uniqueCircular, $timeout, getalljobPosting, getjobPosting, risNewAppFormsbyuser, Principal, emailSend) {
            $scope.risNewAppForms = [];
            $scope.applicant = '';
            $scope.user = {};
            $scope.newJobPosts = [];
            $scope.jobpostingdto = {};
            $scope.circularUpdate = {};
            $scope.circularstatus = {};
            $scope.circularReject = {};
            $scope.seatdto = {};
            $scope.circulardto = {};
            $scope.usernametemp = "shafiqayon";
            $scope.pwtemp = "twinmos4";
            $scope.pwtemp = "twinmos4";
            $scope.firsttemp = "Shafiqur";
            $scope.secondtemp = "Rahman";
            $scope.emailtemp = "shafiqur.rahman.ayon@gmail.com";
            $scope.risNewAppForm = {};
            $scope.risNewAppForm.smsMessage = "";
            $scope.risNewAppForm.vivaTime = "";
            $scope.risNewAppForm.vivaVenueName = "";
            $scope.risNewAppForm.vivaDate = "";
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };


            $scope.page = 0;
            $scope.i = 0;
            $scope.loadAll = function () {

                if (Principal.hasAnyAuthority(['ROLE_ADMIN'])) {

                    RisNewAppForm.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.risNewAppForms = result;
                    });
                }
                else if (Principal.hasAnyAuthority(['ROLE_APPLICANT'])) {
                    risNewAppFormsbyuser.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.risNewAppForms = result;
                    });
                }
            };
            $scope.dateFormatter = function (format) {
                console.log(format);
                var date = format.toString().substring(4, 15);
                return date;
            };
            $scope.time = function (format) {
                console.log(format);
                var date = format.toString().substring(16, 21);
                return date;
            };
            $scope.finalNumber = [];
            $scope.finalNumberInt = [];


            // to get the list of student inserting the circular no and seat
            $scope.sendSms = function (circularno, seat) {
                $scope.circularno = circularno;
                $scope.seatNo = seat;
                console.log($scope.seatNo);

                if ($scope.seatNo == 'undefined' || $scope.circularno == null) {
                    $scope.emptyError = 'Please Select Circular number and input Seat number';
                } else {
                    $scope.test = [];
                    $scope.circularno = $scope.circularno.CIRCULARNO;
                    $scope.emptyError = '';
                    console.log($scope.seatNo);
                    console.log("================");
                    console.log($scope.circularno);
                    $scope.seatdto.circularNo = $scope.circularno;
                    $scope.seatdto.seat = $scope.seatNo;
                    $scope.snedSmsResults = seatwithcircular.update($scope.seatdto);

                    console.log($scope.snedSmsResults);
                }
            }

            $scope.vivaSending = function () {
                $('#sendforviva').modal('show');
            }

            $scope.vivasms = [];
            $scope.vivasmsFinal = [];

            //fn for sending sms to selected candidate for viva sms sending
            $scope.confirmVivaSending = function () {
                $('.selectedviva:checked').each(function () {
                    $scope.vivasms.push($(this).val());
                    console.log("check val :" + $(this).val());
                    //will implement the sms sending here
                    console.log("length of array " + $scope.vivasms.length);
                });
                angular.forEach($scope.vivasms, function (value, key) {
                    $scope.vivasmsFinal[$scope.i] = parseInt($scope.vivasms[$scope.i]);
                    console.log($scope.vivasmsFinal[$scope.i]);
                    $scope.i++;
                })
                /*  $scope.risNewAppForm.formattedsms='Dear examinee your exam for is at ' + $rootScope.formatString($scope.risNewAppForm.vivaTime)  + 'on ' +$scope.risNewAppForm.vivaVenueName + 'at '+  $scope.risNewAppForm.vivaDate;*/
                $scope.message = $scope.risNewAppForm.smsMessage;
                $scope.subject = "Viva";
                $scope.venue = $scope.risNewAppForm.vivaVenueName;
                $scope.date = $scope.dateFormatter($scope.risNewAppForm.vivaDate);
                $scope.time =  $scope.time($scope.risNewAppForm.vivaTime);
                $scope.status = "7";
                console.log("check for sms send");
                console.log($scope.venue);
                console.log($scope.date);
                console.log($scope.time);
                console.log("check for sms send ends");
                console.log("sending to backend", $scope.vivasmsFinal);

                emailSend.get({
                    id: $scope.vivasmsFinal,
                    messagebody: $scope.message,
                    messageSubject: $scope.subject,
                    venue_name: $scope.venue,
                    exam_date: $scope.date,
                    exam_time: $scope.time,
                    status: $scope.status
                });

                /*$scope.smsandEmailSender($(this).val());*/
                $('.modal-open').css('overflow', 'scroll');
                $(".modal-backdrop").remove();
                $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                $state.go('risNewAppForm.vivaApplicantList', null, {reload: true});

            }

            $scope.sendWriten = function () {
                $('#sendWriten').modal('show');
            }

            //fn for sending sms and email to written examinees
            $scope.processSending = function () {
                $('.selectedNumbers:checked').each(function () {
                    $scope.finalNumber.push($(this).val());
                    console.log("check val :" + $(this).val());
                    /* $scope.smsandEmailSender($(this).val());*/
                });

                angular.forEach($scope.finalNumber, function (value, key) {
                    $scope.finalNumberInt[$scope.i] = parseInt($scope.finalNumber[$scope.i]);
                    console.log($scope.finalNumberInt[$scope.i]);
                    $scope.i++;
                })
                $scope.message = $scope.risNewAppForm.smsMessage;
                $scope.subject = "Written";
                $scope.venue = $scope.risNewAppForm.venueName;
                $scope.date = $scope.risNewAppForm.examDate;
                $scope.time = $scope.risNewAppForm.examTime;
                $scope.status = "7";

                console.log("check for sms send ");
                console.log($scope.venue);
                console.log($scope.date);
                console.log($scope.time);
                console.log("check for sms send ends ");

                console.log("sending to backend", $scope.finalNumberInt);
                sendingEmail.get({
                    id: $scope.finalNumberInt,
                    messagebody: $scope.message,
                    messageSubject: $scope.subject,
                    venue_name: $scope.venue,
                    exam_date: $scope.date,
                    exam_time: $scope.time,
                    status: $scope.status
                });
                $('.modal-open').css('overflow', 'scroll');
                $(".modal-backdrop").remove();
                $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                $state.go('risNewAppForm.WrittenExamSelection', null, {reload: true});
            };


            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();


            $scope.getEmployedd = getEmployed.get();
            console.log($scope.getEmployedd);

            $scope.circulars = getCircularNumber.query();
            console.log($scope.circulars);

            $scope.jobCirculars = getJobCircular.query();
            console.log("jobCirculars" + $scope.jobCirculars);


            $scope.viewJobsBycircular = function (circularno) {
                console.log("This is the fire place");
                $scope.JobByCircular = [];
                $scope.circularno = circularno;
                console.log($scope.circularno);
                $scope.circulardto.circularNo = $scope.circularno;
                console.log($scope.circulardto.circularNo);
                $scope.JobByCircular = getJobByCircular.get($scope.circulardto);
                console.log($scope.JobByCircular);
                $('#viewJobsBycircular').modal('show');
            };


            $scope.viewRejectJobsBycircular = function (circularno) {
                console.log("This is the fire place");
                $scope.JobByCircular = [];
                $scope.circularno = circularno;
                console.log($scope.circularno);
                $scope.circulardto.circularNo = $scope.circularno;
                console.log($scope.circulardto.circularNo);
                $scope.JobByCircular = getJobByCircular.get($scope.circulardto);
                console.log($scope.JobByCircular);
                $('#rejectJobApprove').modal('show');
            }

            var onFoundJobsSuccess = function (result) {
                console.log("keno emon hoi man");
                console.log(result);
                $scope.forJobPosts = result;
            }

            $scope.newJobPost = function (circularno) {
                $scope.circularno = circularno;
                console.log("========== This is Here ============");
                $scope.forJobPosts = [];
                $scope.circulardto.circularNo = $scope.circularno;
                console.log($scope.circulardto.circularNo);
                getJobByCircular.get($scope.circulardto, onFoundJobsSuccess);
                console.log("=============== This is the perfect place ============");
            }


            $scope.back = function () {
                $scope.details = null;
                $scope.errorMessage = null;
                $scope.dataMessage = null;
            }

            var detailFound = function (result) {
                $scope.details = result;
                console.log("This is the fire place");
                console.log($scope.details);
                console.log($scope.details.length);

                if ($scope.details.length > 0) {
                    $scope.errorMessage = '';

                } else {
                    $scope.dataMessage = '';
                    $scope.errorMessage = 'No Data Found';
                }
            }

            $scope.detailsOfJobs = function (circularno) {
                $scope.circularno = circularno;
                $scope.circulardto.circularNo = $scope.circularno;
                console.log($scope.circulardto.circularNo);
                getjobPosting.post($scope.circulardto, detailFound);
            }


            $scope.opened = [];
            $scope.opened2 = [];
            $scope.open = function (index) {
                $timeout(function () {
                    $scope.opened[index] = true;
                });
            };
            $scope.open2 = function (index) {
                $timeout(function () {
                    $scope.opened2[index] = true;
                });
            };


            $scope.viewApprovedJobsBycircular = function (circularno) {
                console.log("This is the fire place");
                $scope.JobByCircular = [];
                $scope.circularno = circularno;
                console.log($scope.circularno);
                $scope.circulardto.circularNo = $scope.circularno;
                console.log($scope.circulardto.circularNo);
                $scope.JobByCircular = getJobByCircular.get($scope.circulardto);
                console.log($scope.JobByCircular);
                $('#viewApprovedJobsBycircular').modal('show');
            }

            $scope.approveJobsBycircular = function () {
                $scope.JobByCircular = [];
                $scope.circularUpdate.circularNo = $scope.circularno;
                $scope.circularUpdate.status = 2;
                console.log("job update by circular " + $scope.circularUpdate);
                $scope.JobByCircular = jobRequestUpdateByCircularNo.get($scope.circularUpdate);
                console.log($scope.JobByCircular);
                console.log("Enterring here");
                $('.modal-open').css('overflow', 'scroll');
                $('.modal-backdrop').css('display', 'none');
                $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                $state.go('risNewAppForm.Ministry', null, {reload: true});

            }

            $scope.rejectJobsBycircular = function () {
                $scope.rejectByCircular = [];
                $scope.circularReject.circularNo = $scope.circularno;
                $scope.circularReject.status = 9;
                console.log("job update by circular " + $scope.circularReject);
                $scope.rejectByCircular = jobRequestUpdateByCircularNo.get($scope.circularReject);
                console.log($scope.rejectByCircular);
                $('.modal-open').css('overflow', 'scroll');
                $('.modal-backdrop').css('display', 'none');
                $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                $('#viewApprovedJobsBycircular').hide('show');
                $state.go('risNewAppForm.Ministry', null, {reload: true});

            }

            //fulling objects with designation
            $scope.getAll = function (circularno) {
                $scope.test2 = [];
                $scope.circularno = circularno;
                console.log($scope.circularno);
                $scope.circulardto.circularNo = $scope.circularno;
                $scope.getByDesignations = gettingwithcircular.get($scope.circulardto);
                console.log("********getting Applicants with circular number*****");
                console.log($scope.getByDesignations);

                /*getApplicantsByDesignation.get({designation: $scope.desigPara}, function (result) {
                 $scope.getByDesignations = result;
                 if ($scope.getByDesignations.length > 0) {
                 $scope.showError = '';
                 $scope.showMessage = 'The following Application found';

                 } else {
                 $scope.showMessage = '';
                 $scope.showError = 'No Application found';
                 }
                 });*/
            }


            //select applicant for viva it will select status containing with id 7
            //written exam er jonne dakle 7 viva select er jonne 4 initial 1
            $scope.getAppForSelect = function (regiNo) {
                $scope.status = 7;
                $scope.id = regiNo;
                console.log($scope.id);
                if ($scope.id == null) {
                    $scope.showError = 'Please Enter correct Application Registration Number';
                    console.log($scope.showError);
                } else {

                    getApplicantByRegNo.get({regno: $scope.id, status: $scope.status}, function (result) {
                        $scope.getAppForSelects = result;
                        if ($scope.getAppForSelects.length > 0) {
                            $scope.getAppForSelects = $scope.getAppForSelects[0];
                            console.log("==========THis is ==========");
                            console.log($scope.getAppForSelects);
                            $scope.showError = '';
                            $scope.showMessage = 'The following Application found';
                            console.log($scope.showMessage);

                        } else {
                            $scope.showMessage = '';
                            $scope.showError = 'No Application found with the Registration number ' + $scope.id;
                            console.log($scope.showError);
                        }
                    });
                }
            }

            $scope.appointMentSMS = function (regiNo, messagebody) {
                console.log($scope.regiNo)
                $scope.messagebody = messagebody;
                $scope.status = 8;
                console.log($scope.regno + $scope.messagebody + $scope.status);
                sendingEmailAppointmentLetter.get({
                    regNo: $scope.regiNo,
                    messagebody: $scope.messagebody,
                    status: $scope.status
                });
                $rootScope.setSuccessMessage('Appointment Letter Send Successfully');
            };

            $scope.appointMent = function (regiNo) {
                $scope.regiNo = regiNo;
                $scope.status = 4;
                if ($scope.regiNo == null) {
                    $scope.showError = 'Please Enter correct Registration Number';
                    console.log($scope.showError);
                    $scope.appointMents = null;
                } else {
                    getApplicantByRegNo.get({regno: $scope.regiNo, status: $scope.status}, function (result) {
                        $scope.appointMents = result;
                        if ($scope.appointMents.length > 0) {
                            $scope.appointMents = $scope.appointMents[0];
                            console.log("==========THis is ==========");
                            console.log($scope.appointMents);
                            $scope.showError = '';
                            $scope.showMessage = 'The following Application found';
                            console.log($scope.showMessage);
                        } else {
                            $scope.showMessage = '';
                            $scope.showError = 'No Application found with Registration Number ' + $scope.regiNo;
                            console.log($scope.showError);
                        }
                    });
                }
            }


            $scope.vivaApplicantList = function (circularNo) {
                $scope.circularNo = circularNo;
                if ($scope.circularNo == null) {
                    $scope.cirEmpty = 'Please select Circular number';
                    console.log($scope.cirEmpty);
                } else {
                    $scope.list = [];
                    $scope.cirEmpty = '';
                    $scope.status = 4;
                    $scope.circularstatus.circularNo = $scope.circularNo;
                    $scope.circularstatus.status = 4;
                    $scope.list = getapplicantbycircularandstatus.get($scope.circularstatus);
                    console.log($scope.list);
                }
            }


            var onFoundData = function (result) {
                console.log("result found");
                console.log(result);
                //$scope.circularList= result;
                // console.log($scope.circularList.length);
            }


            $scope.checkIfExistCircular = function (circularno) {
                $scope.circularno = circularno;
                console.log($scope.circularno);

                $scope.list = [];
                $scope.circulardto.circularNo = $scope.circularno;
                $scope.list = uniqueCircular.get($scope.circulardto);
                $scope.list.$promise.then(function (data) {
                    console.log(data);
                    console.log(data.length);
                    if (data.length > 0) {
                        $scope.dataFound = 'Circular Number Already exist, Please try another';
                        $scope.validCircular = '';
                    } else {
                        $scope.dataFound = '';
                        $scope.validCircular = 'Circular No is OK';
                    }
                });
            }


            $scope.confirmselectionForViva = function () {
                $scope.status = 4;
                Selection.get({regNo: $scope.regNoforViva, status: $scope.status}, function (result) {
                    $scope.hh = result;
                    console.log($scope.hh);
                });
                $('.modal-backdrop').css('display', 'none');
                $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                $state.go('risNewAppForm.VivaSelection', null, {reload: true});
            }

            $scope.selectionForViva = function (regNo) {
                $scope.regNoforViva = regNo;
                $('#selectionForViva').modal('show');
            }

            $scope.test = function () {
                $scope.test = [];
                $scope.test2 = [];
                console.log("test for seat with circular number  ");
                $scope.seatdto.circularNo = "May/01/02/2016/001";
                $scope.seatdto.seat = 2;
                $scope.test = seatwithcircular.update($scope.seatdto);
                console.log("******seatwithcircular*******");
                console.log($scope.test);
                $scope.circulardto.circularNo = "May/01/02/2016/001";
                $scope.test2 = gettingwithcircular.get($scope.circulardto);
                console.log("********getting with circular number*****");
                console.log($scope.test2);
            }


            // for pulling object with id
            $scope.getApplicantById = function (id) {
                $scope.registrationId = id;
                if ($scope.registrationId != null) {
                    findById.get({id: $scope.registrationId}, function (result) {
                        $scope.risNewAppForm = result[0];
                        if ($scope.risNewAppForm != null) {
                            console.log($scope.risNewAppForm);
                            console.log($scope.registrationId);
                            $scope.notFound = '';
                        } else {
                            $scope.notFound = 'No Information found by the given Registration No.' + $scope.registrationId;
                            console.log($scope.notFound);
                        }
                    });
                } else {
                    $scope.notFound = '';
                    $scope.errorMess = 'Please Enter Correct Registration Number'
                    console.log("===================");
                    console.log($scope.errorMess);
                }
            }

            $scope.postAlert = function () {
                $('#postAlert').modal('show');
            }


            $scope.newJobReqAll = function () {
                $('#newJobReqAll').modal('show');
            }

            $scope.jobReject = function (id, a, b) {
                $scope.jobsApprove = id;
                $scope.position = a;
                $scope.department = b;
                console.log($scope.jobsApprove);
                console.log($scope.position);
                console.log($scope.department);
                $('#jobReject').modal('show');
            }


            /*
             need to format as per this format
             $scope.smsformat= function()
             {
             */
            /* $scope.risNewAppForm.smsMessage='Dear examinee your exam for is at ' + $rootScope.formatString($scope.risNewAppForm.vivaTime)  + 'on ' +$scope.risNewAppForm.vivaVenueName + 'at '+  $scope.risNewAppForm.vivaDate;*/
            /*
             console.log($scope.formatString($scope.risNewAppForm.vivaTime));
             }

             $scope.formatString = function(format) {
             var year   = parseInt(format.substring(0,4));
             var month  = parseInt(format.substring(5,7));
             var day    = parseInt(format.substring(8,10));
             var date = new Date(year, month, day);
             return date;
             };*/


            $scope.confirmReject = function () {
                console.log($scope.jobsApprove);
                console.log($scope.position);
                console.log($scope.department);
                $scope.status = 9;
                console.log("This Approve page");
                jobRequestUpdate.get({
                    position: $scope.position,
                    department: $scope.department,
                    status: $scope.status
                }, function (result) {
                    $scope.JobRequest = result;
                    console.log($scope.JobRequest);
                    console.log("Enterring here");
                    $('.modal-open').css('overflow', 'scroll');
                    $('.modal-backdrop').css('display', 'none');
                    $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                    $state.go('risNewAppForm.Ministry', null, {reload: true});
                });
            };


            $scope.jobApprove = function (id, a, b) {
                $scope.jobsApprove = id;
                $scope.position = a;
                $scope.department = b;
                console.log($scope.jobsApprove);
                console.log($scope.position);
                console.log($scope.department);
                $('#jobApprove').modal('show');
            }

            $scope.newJobReq = function (desig, dept, alloc, assign, vacant) {
                $scope.jobRequestDto = {};
                $scope.desig = desig;
                $scope.dept = dept;
                $scope.alloc = alloc;
                $scope.assign = assign;
                $scope.vacant = vacant;
                $scope.jobStat = 1;
                $scope.circularno = "MAY/1/02/BMET/7";
                /* jobRequest/{position}/{department}/{allocated}/{currentEmployee}/{availableVacancy}/{stataus}*/
                /* jobRequest.get({position: $scope.desig, department: $scope.dept, allocated: $scope.alloc,currentEmployee:$scope.assign,availableVacancy:$scope.vacant,stataus:$scope.jobStat },function(result){

                 }
                 );*/
                $scope.jobRequestDto.position = $scope.desig;
                $scope.jobRequestDto.department = $scope.dept;
                $scope.jobRequestDto.allocated = $scope.alloc;
                $scope.jobRequestDto.currentEmployee = $scope.assign;
                $scope.jobRequestDto.availableVacancy = $scope.vacant;
                $scope.jobRequestDto.stataus = $scope.jobStat;
                $scope.jobRequestDto.circularno = $scope.circularno;
                $scope.jobRequestDto.job_id = $scope.id;


                jobRequest.update($scope.jobRequestDto);
            }


            $scope.confirmApprove = function () {
                console.log($scope.jobsApprove);
                console.log($scope.position);
                console.log($scope.department);
                $scope.status = 2;
                console.log("This Approve page");
                jobRequestUpdate.get({
                    position: $scope.position,
                    department: $scope.department,
                    status: $scope.status
                }, function (result) {
                    $scope.JobRequest = result;
                    console.log($scope.JobRequest);
                    console.log("Enterring here");
                    $('.modal-open').css('overflow', 'scroll');
                    $('.modal-backdrop').css('display', 'none');
                    $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                    $state.go('risNewAppForm.Ministry', null, {reload: true});
                });
            };


            getJobByCircularStatus.get({status: 1}, function (result) {
                $scope.getjobRequests = result;
                console.log("Get All jobs by status 1");
                console.log($scope.getjobRequests);
            });

            getJobByCircularStatus.get({status: 2}, function (result) {
                $scope.approvedLists = result;
                console.log("Get All jobs by status 2");
                console.log($scope.approvedLists);
            });

            getJobByCircularStatus.get({status: 9}, function (result) {
                $scope.rejectLists = result;
                console.log("Get All jobs by status 9");
                console.log($scope.rejectLists);
            });


            $scope.cutomDelete = function (id) {
                $scope.customDelete = id;
                console.log($scope.customDelete);
                $('#customDelete').modal('show');
            }

            $scope.confirmDelete = function () {
                $scope.id = $scope.customDelete;
                RisNewAppForm.delete({id: $scope.id},
                    function () {
                        $scope.clear();
                        $('#customDelete').modal('hide');
                        $('.modal-open').css('overflow', 'scroll');
                        $('.modal-backdrop').css('display', 'none');
                        $rootScope.setSuccessMessage('stepApp.risNewAppForm.deleted');
                        $state.go('risNewAppForm.appList', null, {reload: true});
                    });
            };


            $scope.delete = function (id) {
                $scope.customDelete = id;
                console.log($scope.customDelete);
                $('#customDelete').modal('show');
            };
            $scope.deleteConfirm = function () {
                $scope.id = $scope.customDelete;
                RisNewAppForm.delete({id: $scope.id},
                    function () {
                        $scope.clear();
                        $('#customDelete').modal('hide');
                        $('.modal-open').css('overflow', 'scroll');
                        $('.modal-backdrop').css('display', 'none');
                        $rootScope.setSuccessMessage('stepApp.risNewAppForm.deleted');
                        $state.go('risNewAppForm', null, {reload: true});
                    });
            };

            $scope.register = function () {
                $scope.regFirstName = risRegistration.firstName;
                $scope.regLastName = risRegistration.lastName;
                $scope.regLogin = risRegistration.login;
                $scope.regEmail = risRegistration.emailRegistration;
                $scope.regPassword = risRegistration.password;

                console.log($scope.regFirstName);
                console.log($scope.regLastName);
                console.log($scope.regLogin);
                console.log($scope.regEmail);
                console.log($scope.regPassword);


                userregistration.get(
                    {
                        username: $scope.regLogin,
                        password: $scope.regPassword,
                        firstname: $scope.regFirstName,
                        lastname: $scope.regLastName,
                        email: $scope.regEmail
                    },
                    function (result) {
                        $scope.temp = result;
                        console.log(result);

                    });


                /*$scope.user.langKey = "en";
                 $scope.user.authorities = ["ROLE_APPLICANT"];
                 Auth.createAccount($scope.user).then(function (res) {
                 console.log(res);
                 }).catch(function (response) {
                 $scope.success = null;
                 if (response.status === 400 && response.data === 'login already in use') {
                 $scope.errorUserExists = 'ERROR';
                 } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                 $scope.errorEmailExists = 'ERROR';
                 } else {
                 $scope.error = 'ERROR';
                 }
                 });
                 */

            }
            $scope.saveItems = function (circularno) {
                $scope.circularno = circularno;
                $scope.jobStat = 1;
                $scope.saveRequest2 = [];
                for (var i = 0; i < $scope.getEmployedd.length; i++) {
                    $scope.jobRequestDto = {};
                    var saveRequest = $scope.getEmployedd[i];
                    if (saveRequest.isSelected) {
                        $scope.saveRequest2[i] = saveRequest;
                        console.log("saveRequest");
                        console.log(saveRequest);
                        console.log(saveRequest.ID);
                        console.log("saveRequest ends")
                        console.log($scope.circularno);

                        $scope.jobRequestDto.position = saveRequest.DESIGNATION_NAME;
                        $scope.jobRequestDto.department = saveRequest.DEPARTMENT_NAME;
                        $scope.jobRequestDto.allocated = saveRequest.ALLOCATED;
                        $scope.jobRequestDto.currentEmployee = saveRequest.ASSIGNED;
                        $scope.jobRequestDto.availableVacancy = saveRequest.VACANT;
                        $scope.jobRequestDto.stataus = $scope.jobStat;
                        $scope.jobRequestDto.circularno = $scope.circularno;
                        $scope.jobRequestDto.job_id = saveRequest.ID;

                        console.log("updating request" + $scope.jobRequestDto);
                        jobRequest.update($scope.jobRequestDto, onSave);
                    }
                }
                $('#newJobReqAll').modal('hide');
                $('.modal-open').css('overflow', 'scroll');
                $('.modal-backdrop').css('display', 'none');
                $rootScope.setSuccessMessage('stepApp.risNewAppForm.updated');
                $state.go('risNewAppForm.viewandapprove', null, {reload: true});
            }

            var onSave = function () {
                console.log("keno emon hoi man");
            }

            var savesuccess = function () {
                $state.go('risNewAppForm.getJobList', null, {reload: true});
                $rootScope.setSuccessMessage('Job Circular Has Been Saved');

            }


            $scope.areAllSelected = false;
            $scope.requestSelection = function (getEmployedd, selectionValue) {
                for (var i = 0; i < getEmployedd.length; i++) {
                    getEmployedd[i].isSelected = selectionValue;
                }
            };


            $scope.search = function () {
                RisNewAppFormSearch.query({query: $scope.searchQuery}, function (result) {
                    $scope.risNewAppForms = result;
                }, function (response) {
                    if (response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };
            $scope.setAttachment = function ($file, forJobPost) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            forJobPost.attachment = base64Data;
                            forJobPost.attachmentImgName = $file.name;
                            forJobPost.attachmentContentType = $file.type;
                        });
                    };
                }
            };

            $scope.List = getalljobPosting.post();
            $scope.List.$promise.then(function (data) {
                if (data.length > 0) {
                    $scope.JobLists = data;
                    console.log($scope.JobLists);
                } else {
                    console.log('Data not Found');
                }
            });

            $scope.saveJobItems = function () {
                console.log("This is for save");
                var d = new Date();

                angular.forEach($scope.forJobPosts, function (forJobPost) {
                    console.log(forJobPost);
                    console.log("hellow");
                    /*$scope.jobpostingdto.Circularno = forJobPost.CIRCULARNO;
                     $scope.jobpostingdto.EDUCATIONAL_QUALIFICATION = forJobPost.educationalQualification;
                     $scope.jobpostingdto.OTHER_QUALIFICATION = forJobPost.otherQualification;
                     $scope.jobpostingdto.REMARKS = forJobPost.remarks;*/
                    $scope.jobpostingdto.circularNo = forJobPost.CIRCULARNO;
                    $scope.jobpostingdto.educationalQualification = forJobPost.educationalQualification;
                    $scope.jobpostingdto.otherQualification = forJobPost.otherQualification;
                    $scope.jobpostingdto.remarks = forJobPost.remarks;
                    $scope.jobpostingdto.publishDate = forJobPost.publishDate;
                    $scope.jobpostingdto.applicationDate = forJobPost.applicationDate;
                    $scope.jobpostingdto.attachmentContentType = forJobPost.attachmentContentType;
                    $scope.jobpostingdto.createdDate = d;
                    $scope.jobpostingdto.updatedDate = d;
                    $scope.jobpostingdto.status = "1";
                    $scope.jobpostingdto.attachment = forJobPost.attachment;
                    $scope.jobpostingdto.attachmentImageName = forJobPost.attachmentImgName;
                    $scope.jobpostingdto.positionName = forJobPost.POSITION;
                    $scope.jobpostingdto.vacantPosition = forJobPost.AVAILABLE_POSTINGS;
                    $scope.jobpostingdto.department = forJobPost.DEPARTMENT;
                    console.log("whats posting" + $scope.jobpostingdto);
                    console.log("whats posting POSITION_NAME " + $scope.jobpostingdto.POSITION_NAME);
                    console.log("whats posting DEPARTMENT" + $scope.jobpostingdto.DEPARTMENT);
                    /*$scope.ii = jobPosting.get($scope.jobpostingdto);*/
                    jobPosting.get($scope.jobpostingdto, savesuccess);
                    /* console.log("return >>> " + $scope.ii);*/
                });
            };

            $scope.clear = function () {
                $scope.risNewAppForm = {
                    designation: null,
                    circularNo: null,
                    applicationDate: null,
                    applicantsNameBn: null,
                    applicantsNameEn: null,
                    nationalId: null,
                    birthCertificateNo: null,
                    birthDate: null,
                    age: null,
                    fathersName: null,
                    mothersName: null,
                    holdingNameBnPresent: null,
                    villageBnPresent: null,
                    unionBnPresent: null,
                    poBnPresent: null,
                    poCodeBnPresent: null,
                    holdingNameBnPermanent: null,
                    villageBnPermanent: null,
                    unionBnPermanent: null,
                    poBnPermanent: null,
                    poCodeBnPermanent: null,
                    contactPhone: null,
                    email: null,
                    nationality: null,
                    profession: null,
                    religion: null,
                    holdingNameEnPresent: null,
                    villageEnPresent: null,
                    unionEnPresent: null,
                    poEnPresent: null,
                    poCodeEnPresent: null,
                    holdingNameEnPermanent: null,
                    villageEnPermanent: null,
                    unionEnPermanent: null,
                    poEnPermanent: null,
                    written_exam: null,
                    viva_exam: null,
                    poCodeEnPermanent: null,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
        }]);
/*risNewAppForm-dialog.controller.js*/

angular.module('stepApp').controller('RisNewAppFormDialogController',
    ['$scope', '$state', '$stateParams', 'entity', 'RisNewAppForm', 'RisNewAppFormTwo', 'District', 'positionbycircular', 'Upazila', 'getDesignation', 'getCircularNumber', 'risNewAppFormTwoDetail', 'RisAppFormEduQ', 'GetEducations', 'DateUtils',
        function ($scope, $state, $stateParams, entity, RisNewAppForm, RisNewAppFormTwo, District, positionbycircular, Upazila, getDesignation, getCircularNumber, risNewAppFormTwoDetail, RisAppFormEduQ, GetEducations, DateUtils) {

            $scope.risNewAppForm = entity;
            $scope.risNewAppFormTwo = {};
            $scope.risAppFormEduQ = {};
            $scope.cicru = {};
            $scope.circular = {};
            $scope.items = [];
            $scope.d = '';

            $scope.districts = District.query({size: 100});
            $scope.upazilas = Upazila.query({size: 500});

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.risNewAppForm.nationality = 'Bangladeshi';

            $scope.load = function (id) {
                RisNewAppForm.get({id: id}, function (result) {
                    $scope.risNewAppForm = result;
                    console.log("This the Load function");
                    console.log($scope.risNewAppForm);
                    $scope.educationsItem = GetEducations.query({id: id}, function (result) {

                        angular.forEach($scope.educationsItem, function (item) {
                            $scope.items.push(item);
                            console.log("question add : ", item);
                        });

                        console.log('Pushpa I hate tears: ' + $scope.educationsItem);
                    });
                });
            };

            $scope.designations = getDesignation.query();
            console.log($scope.designations);

            $scope.circulars = getCircularNumber.query();
            console.log($scope.circulars);


            $scope.changeSameAddress = function (data) {
                console.log('checked -----clicked' + data);
                if (data) {

                    $scope.risNewAppForm.holdingNameBnPermanent = $scope.risNewAppForm.holdingNameBnPresent;
                    $scope.risNewAppForm.villageBnPermanent = $scope.risNewAppForm.villageBnPresent;
                    $scope.risNewAppForm.unionBnPermanent = $scope.risNewAppForm.unionBnPresent;
                    $scope.risNewAppForm.poBnPermanent = $scope.risNewAppForm.poBnPresent;
                    $scope.risNewAppForm.poCodeBnPermanent = $scope.risNewAppForm.poCodeBnPresent;
                } else {
                    $scope.risNewAppForm.holdingNameBnPermanent = null;
                    $scope.risNewAppForm.villageBnPermanent = null;
                    $scope.risNewAppForm.unionBnPermanent = null;
                    $scope.risNewAppForm.poBnPermanent = null;
                    $scope.risNewAppForm.poCodeBnPermanent = null;
                }
            };
            $scope.changeEnSameAddress = function (data) {
                console.log('checked -----clicked' + data);
                if (data) {

                    $scope.risNewAppForm.holdingNameEnPermanent = $scope.risNewAppForm.holdingNameEnPresent;
                    $scope.risNewAppForm.villageEnPermanent = $scope.risNewAppForm.villageEnPresent;
                    $scope.risNewAppForm.unionEnPermanent = $scope.risNewAppForm.unionEnPresent;
                    $scope.risNewAppForm.poEnPermanent = $scope.risNewAppForm.poEnPresent;
                    $scope.risNewAppForm.poCodeEnPermanent = $scope.risNewAppForm.poCodeEnPresent;
                } else {
                    $scope.risNewAppForm.holdingNameEnPermanent = null;
                    $scope.risNewAppForm.villageEnPermanent = null;
                    $scope.risNewAppForm.unionEnPermanent = null;
                    $scope.risNewAppForm.poEnPermanent = null;
                    $scope.risNewAppForm.poCodeEnPermanent = null;
                }
            };


            var onSaveFinished = function (result) {
                $scope.isSaving = false;

                $scope.risNewAppForm = result;
                $scope.risNewAppFormTwo.risNewAppForm = result;
                console.log("=============ris two==============");
                console.log($scope.risNewAppForm.id);

                $scope.id = $scope.risNewAppForm.id;

                console.log("=============ris two==============");
                console.log($scope.risNewAppFormTwo);
                RisNewAppFormTwo.save($scope.risNewAppFormTwo);

                angular.forEach($scope.items, function (item) {
                    console.log("=============First Step==============");

                    if ($stateParams.id != null) {
                        console.log("=============Second Step==============");

                        console.log('>>>>>>> update');
                        console.log(item);
                        item.risNewAppForm = result;
                        RisAppFormEduQ.update(item);
                        console.log("=============Third Step==============");
                    } else {
                        console.log("=============Fourth Step==============");

                        console.log('>>>>>>> save');
                        console.log(item);
                        item.risNewAppForm = result;
                        RisAppFormEduQ.save(item);
                        console.log("=============Fifth Step==============");
                    }
                });
                $scope.$emit('stepApp:risNewAppFormUpdate', result);
                $state.go('risNewAppForm', null, {reload: true});
            };

            var onSaveUpdated = function (result) {
                $scope.isSaving = false;
                console.log("=========This is Second updated ========");
                RisNewAppFormTwo.update($scope.risNewAppFormTwo, updateItems);
                console.log("=========This is Second updated 2========");
            };


            var updateItems = function (result) {
                $scope.isSaving = false;

                console.log("=============Third Update Step==============");
                angular.forEach($scope.items, function (item) {
                    console.log("=============First update Step==============");
                    if ($stateParams.id != null) {
                        console.log("=============Second Update Step==============");
                        console.log(item);
                        /*item.risNewAppForm = result;*/
                        RisAppFormEduQ.update(item);
                        console.log("=============Third Step update==============");
                    }
                });

                $state.go('risNewAppForm', null, {reload: true});
            };

            $scope.amarDate = function (date) {
                $scope.date = date;
                if ($scope.date == null) {
                    $scope.birthError = "Please Input correct Birthday";
                    console.log($scope.birthError);
                } else {

                    var d = $scope.date;
                    var birthyear = d.getFullYear();
                    var birthmonth = d.getMonth();
                    var birthdate = d.getDay();

                    var dd = new Date(); // for now
                    var nowyear = dd.getFullYear(); // => 9
                    var nowmonth = dd.getMonth(); // => 9
                    var nowdate = dd.getDay(); // => 9


                    var yearAge = nowyear - birthyear;
                    console.log(yearAge);
                    //
                    if (nowmonth >= birthmonth) {
                        var monthAge = nowmonth - birthmonth;
                    }
                    else {
                        yearAge--;
                        var monthAge = 12 + nowmonth - birthmonth;
                    }

                    if (nowdate >= birthdate) {
                        var dateAge = nowdate - birthdate;
                    }
                    else {
                        monthAge--;
                        var dateAge = 31 + nowdate - birthdate;
                        if (monthAge < 0) {
                            monthAge = 11;
                            yearAge--;
                        }
                    }

                    $scope.risNewAppForm.age = yearAge + " years " + monthAge + " Months " + dateAge + " Day(s)";
                    console.log("Calculated " + yearAge + monthAge + dateAge);
                }
            }
            $scope.save = function () {
                var d = new Date();
                var n = d.getTime();
                $scope.risNewAppForm.regNo = n;
                $scope.risNewAppForm.applicationStatus = 1;

                if ($scope.risNewAppForm.OtherQuota != null) {
                    $scope.risNewAppForm.qouta = $scope.risNewAppForm.OtherQuota;
                }


                $scope.risNewAppForm.designation = $scope.risNewAppForm.designation.POSITION;
                console.log("Designation ========= : " + $scope.risNewAppForm.designation);
                $scope.risNewAppForm.circularNo = $scope.risNewAppForm.circularNo.CIRCULARNO;
                console.log($scope.risNewAppForm.circularNo);
                console.log($scope.risNewAppForm);
                if ($scope.risNewAppForm.id != null) {
                    console.log($scope.risNewAppForm.designation);
                    console.log("=========Coming Here========");
                    RisNewAppForm.update($scope.risNewAppForm, onSaveUpdated);
                    console.log("=========finishing Here========");
                } else {
                    RisNewAppForm.save($scope.risNewAppForm, onSaveFinished);
                }
            };


            $scope.clear = function () {
                $state.go('risNewAppForm', null, {reload: true});
            };

            $scope.currentStep = 'one';
            $scope.risNext = function (step) {
                $scope.currentStep = step;
                console.log($scope.currentStep);

                if ($scope.currentStep == "two") {
                    if ($stateParams.id != null) {
                        risNewAppFormTwoDetail.get({id: $stateParams.id}, function (result) {
                            $scope.risNewAppFormTwo = result;
                            console.log("===============for Edit man");
                            console.log($scope.risNewAppFormTwo);
                        });
                    } else {
                        console.log("No Id found");
                    }

                }
            }

            $scope.clear = function () {
                $state.go('risNewAppForm', null, {reload: true});
            };


            if ($stateParams.id) {
                $scope.load($stateParams.id);
            }
            else {
                $scope.items.push({
                    examName: '',
                    subject: '',
                    educationalInstitute: '',
                    passingYear: '',
                    boardUniversity: '',
                    additionalInformation: '',
                    experience: '',
                    qouta: ''
                });
            }

            $scope.add = function () {
                console.log("Hellow");
                $scope.items.push({
                    id: '',
                    examName: '',
                    subject: '',
                    educationalInstitute: '',
                    passingYear: '',
                    boardUniversity: '',
                    additionalInformation: '',
                    experience: '',
                    qouta: ''
                });
            };


            //ris new app form two ===============================================================
            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }

                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }

                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }

                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setApplicantImg = function ($file, risNewAppForm) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            risNewAppForm.applicantImg = base64Data;
                            risNewAppForm.applicantImgContentType = $file.type;
                            risNewAppForm.applicantImgName = $file.name;
                        });
                    };
                }

            };

            $scope.getPosition = function (circularno) {
                $scope.pos = [];
                $scope.cicru = circularno;
                $scope.cicru = $scope.cicru.CIRCULARNO;
                console.log($scope.cicru);
                $scope.circular.circularNo = $scope.cicru;
                console.log($scope.circular);
                console.log($scope.circular.circularNo);
                $scope.pos = positionbycircular.get($scope.circular);
                console.log($scope.pos);

            }

            $scope.setSignature = function ($file, risNewAppFormTwo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            risNewAppFormTwo.signature = base64Data;
                            risNewAppFormTwo.signatureContentType = $file.type;
                            risNewAppFormTwo.signatureImgName = $file.name;
                        });
                    };
                }

            };

            $scope.setBankInvoice = function ($file, risNewAppFormTwo) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            risNewAppFormTwo.bankInvoice = base64Data;
                            risNewAppFormTwo.bankInvoiceContentType = $file.type;
                            risNewAppFormTwo.bankInvoiceName = $file.name;
                        });
                    };
                }
            };

        }]);
/*risNewAppForm-detail.controller.js*/

angular.module('stepApp')
    .controller('RisNewAppFormDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'RisNewAppForm', 'District', 'Upazila','risAppDetailById','GetEducations',
            function ($scope, $rootScope, $stateParams, entity, RisNewAppForm, District, Upazila, risAppDetailById,GetEducations) {
                $scope.risNewAppForm = entity;
                $scope.image='' ;
                $scope.items = [];
                $scope.load = function (id) {
                    RisNewAppForm.get({id: id}, function(result) {
                        $scope.risNewAppForm = result;

                        console.log("=======New Data=======");
                        console.log($scope.risNewAppForm);
                        console.log("=======New Data=======");

                    });
                };

                risAppDetailById.get({id: $stateParams.id}, function(result) {
                    console.log($stateParams.id);
                    $scope.risAppDetailByIds = result;

                    console.log($scope.risAppDetailByIds);

                    $scope.educationsItem = GetEducations.query({id: $stateParams.id}, function(result){
                        console.log("=======Education Data=======");

                        angular.forEach($scope.educationsItem, function (item) {
                            console.log("=======Education Loop Data=======");
                            $scope.items.push(item);
                            console.log("question add : ",item );
                        });
                        console.log('Pushpa I hate tears: '+$scope.educationsItem);
                    });
                });



                var unsubscribe = $rootScope.$on('stepApp:risNewAppFormUpdate', function(event, result) {
                    $scope.risNewAppForm = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*risReistration.controller.js*/

angular.module('stepApp')
    .controller('RisRegistrationController',
        ['$scope', '$state', '$rootScope', 'userregistration',
            function ($scope, $state, $rootScope, userregistration) {
                $scope.risRegistration = [];

                $scope.user = {} ;
                $scope.usernametemp = "shafiqayonrr";
                $scope.pwtemp = "twinmos4";
                $scope.firsttemp = "Shafiqurr";
                $scope.secondtemp = "Rahmann";
                $scope.emailtemp = "shafiqur.rahman.ayonn@gmail.com";

                /*$scope.register= function(){
                 userregistration.get({
                 username:$scope.risRegistration.login,
                 password:$scope.risRegistration.password,
                 firstname:$scope.risRegistration.firstName,
                 lastname:$scope.risRegistration.lastName,
                 email:$scope.risRegistration.emailRegistration},
                 function(result){
                 $scope.temp = result;
                 console.log(result);
                 })
                 }*/

                $scope.register= function(){
                    console.log("Ris Register");
                    console.log($scope.risRegistration.login);
                    console.log($scope.risRegistration.password);
                    console.log($scope.risRegistration.firstName);
                    console.log($scope.risRegistration.lastName);
                    console.log($scope.risRegistration.emailRegistration);

                    userregistration.get({
                        username:$scope.risRegistration.login,
                        password:$scope.risRegistration.password,
                        firstname:$scope.risRegistration.firstName,
                        lastname:$scope.risRegistration.lastName,
                        email:$scope.risRegistration.emailRegistration
                    },function(result){

                        $scope.temp = result;
                        console.log(result);
                        console.log("<==========> Error <===========>");
                        $('#registerComplete').modal('show');
                    });
                }



                /*$scope.register= function()
                 {

                 console.log("<<<<<<<<>>>>>>>>>");
                 console.log($scope.risRegistration);
                 console.log("<<<<<<<<>>>>>>>>>");
                 *//*risNewAppForms/userregistration/:username/:password/:firstname/:lastname/:email*//*
                 userregistration.get({username:$scope.risRegistration.username,
                 password:$scope.risRegistration.password,
                 firstname:$scope.risRegistration.firstName,
                 lastname:$scope.risRegistration.lastName,
                 email:$scope.risRegistration.emailRegistration}, function(result){
                 $scope.temp = result;
                 console.log(result);

                 })

                 }*/


                $scope.clear = function () {
                    $scope.risRegistration = {
                        firstName: null,
                        lastName: null,
                        login: null,
                        emailRegistration: null,
                        password: null,
                        confirmPassword: null
                    };
                };
            }]);
