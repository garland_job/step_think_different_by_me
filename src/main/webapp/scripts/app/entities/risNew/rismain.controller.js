'use strict';

angular.module('stepApp')
    .controller('risMgtHomeController',
    ['$scope', '$state', 'DataUtils', 'ParseLinks',
        function ($scope, $state, DataUtils, ParseLinks) {
            $scope.myChartObject1 = {};
            $scope.myChartObject1.type = "ColumnChart";
            $scope.onions = [
                {v: "Onions"},
                {v: 3},
            ];
            $scope.myChartObject1.data = {
                "cols": [
                    {id: "t", label: "", type: "string"},
                    {id: "s", label: "", type: "number"},
                    {id: "p", label: "", type: "number"}

                ],
                "rows":
                    [{c: [{v: "Approved Vacant Position"},{v: 15}]},
                        {c: [{v: "Vacant Position List"},{v: 112}]},
                        {c: [{v: "Posted Circular List"},{v: 8}]}
                    ]};

            $scope.myChartObject1.options = {
                legend: 'none',
                'title': 'Recruitment Information System',
                '': [{color:'#0D9947'},{color:'#F42A41'}]
            };
            /*==== 2 ===*/
                $scope.myChartObject2 = {};
            $scope.myChartObject2.type = "BarChart";
            $scope.onions = [
                {v: "Onions"},
                {v: 3},
            ];
            $scope.myChartObject2.data = {
                "cols": [
                    {id: "t", label: "Topping", type: "string"},
                    {id: "s", label: "Slices", type: "number"}

                ],
                "rows":
                    [{c: [{v: "Approve"},{v: 3}]},
                        {c: [{v: "Pending"},{v: $scope.pendingMPOCount}]}]};

            $scope.myChartObject2.options = {
                legend: 'none',
                'title': 'MPO Applications',
                'slices': [{color:'#0D9947'},{color:'#F42A41'}]
            };

        }]);
//original file was


/*
angular.module('stepApp')
    .controller('risMgtHomeController',
    ['$scope', '$state', 'DataUtils', 'SmsServiceComplaintsByEmployee', 'ParseLinks', 'SmsServiceDepartment', 'SmsServiceComplaint',
        function ($scope, $state, DataUtils, SmsServiceComplaintsByEmployee, ParseLinks, SmsServiceDepartment, SmsServiceComplaint) {


        }]);*/
