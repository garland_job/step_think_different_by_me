'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('sisStudentInfo', {
                parent: 'sis',
                url: '/sisStudentInfos',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisStudentInfo.home.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentInfo/sisStudentInfos.html',
                        controller: 'SisStudentInfoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentInfo');
                        return $translate.refresh();
                    }]
                }
            })
            .state('sisStudentInfo.detail', {
                parent: 'sis',
                url: '/sisStudentInfo/{id}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisStudentInfo.detail.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentInfo/sisStudentInfo-detail.html',
                        controller: 'SisStudentInfoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisStudentInfo', function($stateParams, SisStudentInfo) {
                        return SisStudentInfo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('sisStudentInfo.new2', {
                parent: 'sisStudentInfo',
                url: '/new',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentInfo/sisStudentInfo-dialog.html',
                        controller: 'SisStudentInfoDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentInfo');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            name: null,
                            stuPicture: null,
                            stuPictureContentType: null,
                            instituteName: null,
                            TradeTechnology: null,
                            studentName: null,
                            fatherName: null,
                            motherName: null,
                            dateOfBirth: null,
                            presentAddress: null,
                            permanentAddress: null,
                            nationality: 'Bangladeshi',
                            nationalIdNo: null,
                            birthCertificateNo: null,
                            mobileNo: null,
                            contactNoHome: null,
                            emailAddress: null,
                            gender: null,
                            maritalStatus: null,
                            bloodGroup: null,
                            religion: null,
                            activeStatus: true,
                            createDate: null,
                            createBy: null,
                            updateDate: null,
                            updateBy: null,
                            curriculum: null,
                            applicationId: null,
                            id: null
                        };
                    }
                }
            })
            .state('sisStudentInfo.edit', {
                parent: 'sisStudentInfo',
                url: '/{id}/edit',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentInfo/sisStudentInfo-dialog.html',
                        controller: 'SisStudentInfoDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisStudentInfo', function($stateParams, SisStudentInfo) {
                        return SisStudentInfo.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*--------------------sisStudentReg--------------------------------------*/

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('sisStudentReg', {
                parent: 'sis',
                url: '/sisStudentRegs',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisStudentReg.home.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentReg/sisStudentRegs.html',
                        controller: 'SisStudentRegController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentReg');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('sisStudentReg.detail', {
                parent: 'sis',
                url: '/sisStudentReg/{id}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisStudentReg.detail.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentReg/sisStudentReg-detail.html',
                        controller: 'SisStudentRegDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentReg');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisStudentReg', function($stateParams, SisStudentReg) {
                        return SisStudentReg.get({id : $stateParams.id});
                    }]
                }
            })
            .state('sisStudentReg.new', {
                parent: 'sisStudentReg',
                url: '/new',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentReg/sisStudentReg-dialog.html',
                        controller: 'SisStudentRegDialogController'
                    }
                },

                resolve: {
                    entity: function () {
                        return {
                            applicationId: null,
                            instCategory: null,
                            instituteName: null,
                            curriculum: null,
                            TradeTechnology: null,
                            subject1: null,
                            subject2: null,
                            subject3: null,
                            subject4: null,
                            subject5: null,
                            optional: null,
                            shift: null,
                            semester: null,
                            studentName: null,
                            fatherName: null,
                            motherName: null,
                            dateOfBirth: null,
                            gender: null,
                            religion: null,
                            bloodGroup: null,
                            quota: null,
                            nationality: 'Bangladeshi',
                            mobileNo: null,
                            contactNoHome: null,
                            emailAddress: null,
                            presentAddress: null,
                            permanentAddress: null,
                            activeStatus: null,
                            createDate: null,
                            createBy: null,
                            updateDate: null,
                            updateBy: null,
                            maritalStatus: null,
                            id: null
                        };
                    }
                }

            })
            .state('sisStudentReg.edit', {
                parent: 'sisStudentReg',
                url: '/{id}/edit',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisStudentReg/sisStudentReg-dialog.html',
                        controller: 'SisStudentRegDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisStudentReg');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisStudentReg', function($stateParams, SisStudentReg) {
                       // return SisStudentReg.get({id : $stateParams.id});
                    }]
                }

            });
    });

/*-------------------------------END-----------------------------*/

/*-------------------sisQuota-----------*/
angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('sisQuota', {
                parent: 'sis',
                url: '/sisQuotas',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisQuota.home.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisQuota/sisQuotas.html',
                        controller: 'SisQuotaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisQuota');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('sisQuota.detail', {
                parent: 'sis',
                url: '/sisQuota/{id}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisQuota.detail.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisQuota/sisQuota-detail.html',
                        controller: 'SisQuotaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisQuota');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisQuota', function($stateParams, SisQuota) {
                        return SisQuota.get({id : $stateParams.id});
                    }]
                }
            })

            .state('sisQuota.new', {
                parent: 'sisQuota',
                url: '/new',
                data: {
                    authorities: []
                },

                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisQuota/sisQuota-dialog.html',
                        controller: 'SisQuotaDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisQuota');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            name: null,
                            description: null,
                            isFor: true,
                            activeStatus: true
                        };
                    }
                }
            })
            .state('sisQuota.edit', {
                parent: 'sisQuota',
                url: '/{id}/edit',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisQuota/sisQuota-dialog.html',
                        controller: 'SisQuotaDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisQuota');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisQuota', function($stateParams, SisQuota) {
                        return SisQuota.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*---------------------------------------END---------------------*/

/*----------------sisEducationHistory----------------------*/
angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('sisEducationHistory', {
                parent: 'sis',
                url: '/sisEducationHistorys',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisEducationHistory.home.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisEducationHistory/sisEducationHistorys.html',
                        controller: 'SisEducationHistoryController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisEducationHistory');
                        $translatePartialLoader.addPart('achievedCertificate');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('sisEducationHistory.detail', {
                parent: 'sisEducationHistory',
                url: '/sisEducationHistory/{id}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.sisEducationHistory.detail.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisEducationHistory/sisEducationHistory-detail.html',
                        controller: 'SisEducationHistoryDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisEducationHistory');
                        //$translatePartialLoader.addPart('achievedCertificate');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisEducationHistory', function($stateParams, SisEducationHistory) {
                        return SisEducationHistory.get({id : $stateParams.id});
                    }]
                }
            })
            .state('sisEducationHistory.new', {
                parent: 'sisEducationHistory',
                url: '/new',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisEducationHistory/sisEducationHistory-dialog.html',
                        controller: 'SisEducationHistoryDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisEducationHistory');
                        //$translatePartialLoader.addPart('achievedCertificate');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return {
                            yearOrSemester: null,
                            rollNo: null,
                            majorOrDept: null,
                            divisionOrGpa: null,
                            passingYear: null,
                            achievedCertificate: null,
                            activeStatus: true
                        };
                    }
                }
            })
            .state('sisEducationHistory.edit', {
                parent: 'sisEducationHistory',
                url: '/{id}/edit',
                data: {
                    authorities: []
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/sisEducationHistory/sisEducationHistory-dialog.html',
                        controller: 'SisEducationHistoryDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('sisEducationHistory');
                        //$translatePartialLoader.addPart('achievedCertificate');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SisEducationHistory', function($stateParams, SisEducationHistory) {
                        return SisEducationHistory.get({id : $stateParams.id});
                    }]
                }
            });
    });

/*------------------------------END-----------------------------------*/

/*----------------Start ScholarShipSetup--------------*/


angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('scholarShipSetUp', {
                parent: 'sis',
                url: '/scholarShipSetUps',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.scholarShipSetUp.home.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/scholarShipSetUp/scholarShipSetUps.html',
                        controller: 'ScholarShipSetUpController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('scholarShipSetUp');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('scholarShipSetUp.detail', {
                parent: 'sis',
                url: '/scholarShipSetUp/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.scholarShipSetUp.detail.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/scholarShipSetUp/scholarShipSetUp-detail.html',
                        controller: 'ScholarShipSetUpDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('scholarShipSetUp');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ScholarShipSetUp', function($stateParams, ScholarShipSetUp) {
                        return ScholarShipSetUp.get({id : $stateParams.id});
                    }]
                }
            })
            .state('scholarShipSetUp.new', {
                parent: 'scholarShipSetUp',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/scholarShipSetUp/scholarShipSetUp-dialog.html',
                        controller: 'ScholarShipSetUpDialogController'
                    }
                },

                resolve: {
                    entity: function () {
                        return {
                            scholarId: null,
                            scholarName: null,
                            stiphen: null,
                            given_by: null,
                            duration: null,
                            run_institute: null,
                            remarks: null,
                            createDate: null,
                            createBy: null,
                            updateBy: null,
                            updateDate: null,
                            id: null
                        };
                    }
                }

            })
            .state('scholarShipSetUp.edit', {
                parent: 'scholarShipSetUp',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/scholarShipSetUp/scholarShipSetUp-dialog.html',
                        controller: 'ScholarShipSetUpDialogController'
                    }
                },

                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('scholarShipSetUp');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ScholarShipSetUp', function($stateParams, ScholarShipSetUp) {
                        return ScholarShipSetUp.get({id : $stateParams.id});
                    }]
                }

            });
    });



/*-------------------END-------------------------------*/

/*---------------trainingSetup------------------------*/


angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('trainingSetup', {
                parent: 'sis',
                url: '/trainingSetups',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.trainingSetup.home.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/trainingSetup/trainingSetups.html',
                        controller: 'TrainingSetupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('trainingSetup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('trainingSetup.detail', {
                parent: 'sis',
                url: '/trainingSetup/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.trainingSetup.detail.title'
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/trainingSetup/trainingSetup-detail.html',
                        controller: 'TrainingSetupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('trainingSetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'TrainingSetup', function($stateParams, TrainingSetup) {
                        return TrainingSetup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('trainingSetup.new', {
                parent: 'trainingSetup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/trainingSetup/trainingSetup-dialog.html',
                        controller: 'TrainingSetupDialogController'
                    }
                },

                resolve: {
                    entity: function () {
                        return {
                            trainId: null,
                            tainType: null,
                            trainName: null,
                            instituteName: null,
                            fromDate: null,
                            toDate: null,
                            remarks: null,
                            createDate: null,
                            createBy: null,
                            updateBy: null,
                            updateDate: null,
                            id: null
                        };
                    }
                }

            })
            .state('trainingSetup.edit', {
                parent: 'trainingSetup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                views: {
                    'sisView@sis': {
                        templateUrl: 'scripts/app/entities/sis/trainingSetup/trainingSetup-dialog.html',
                        controller: 'TrainingSetupDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('trainingSetup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'TrainingSetup', function($stateParams, TrainingSetup) {
                        return TrainingSetup.get({id : $stateParams.id});
                    }]
                }
            });
    });
/*-----------------------------------*/
