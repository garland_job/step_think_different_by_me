'use strict';

angular.module('stepApp').controller('SisStudentInfoDialogController',
    ['$scope', '$stateParams', 'entity', 'Religion', 'SisStudentInfo', 'Division', 'District', 'UpazilasByDistrict', 'CountrysByName', 'SisQuota', '$state', 'Institute', 'SisStudentReg', 'ParseLinks', 'SisStudentRegByAppId','DistrictsByDivision','CmsCurriculum','InstituteByLogin', 'BloodGroupInfo', 'GenderInfo', 'MaritalInfo','MiscTypeSetupByCategory','TrainingSetup','ScholarShipSetUp',
        function ($scope, $stateParams, entity, Religion, SisStudentInfo, Division, District, UpazilasByDistrict, CountrysByName, SisQuota, $state, Institute, SisStudentReg, ParseLinks, SisStudentRegByAppId,DistrictsByDivision,CmsCurriculum, InstituteByLogin, BloodGroupInfo, GenderInfo, MaritalInfo,MiscTypeSetupByCategory,TrainingSetup,ScholarShipSetUp) {

            $scope.sisStudentInfo = entity;
            $scope.divisions = Division.query({size:20000});
            $scope.religions = Religion.query({size:200});
            $scope.bloodGroupInfos = BloodGroupInfo.query({size:200});
            $scope.genderInfos = GenderInfo.query({size:200});
            $scope.maritalInfos = MaritalInfo.query({size:200});
            $scope.quotas = SisQuota.query({size:2000});

            $scope.miscTypeSetupByCategorys = MiscTypeSetupByCategory.query({cat:'Shift',stat:true});
            $scope.trainingSetups = TrainingSetup.query({size:2000});
            $scope.scholarShipSetUps=ScholarShipSetUp.query({size:2000});

            $scope.cmsCurriculums = CmsCurriculum.query({size:9000});
            $scope.countrys = CountrysByName.query({size:9000});

            $scope.sisInstitute = [];
            $scope.sisInstitutes = [];
            $scope.sisApplication = [];
            $scope.sisApplications = [];
            $scope.sisApplicationPreview = false;
            $scope.sisApplicationPreviewOff = true;
            $scope.selectedInstitute = 'Admin';

            $scope.options1 = [];
            $scope.selectedOptions1 = [];
            $scope.options2 = [];
            $scope.selectedOptions2 = [];


            /*-------------------*/

            if (true) {
                $scope.selectedOptions1 = [];
            }

            if (true) {
                $scope.selectedOptions2 = [];
            }

            $scope.TrainInformation = function () {
                $scope.options1 = [];
                angular.forEach($scope.trainingSetups, function (value) {
                    $scope.options1.push(JSON.parse('{"Value":' + value.id + ',' + '"Text":"' + value.trainName + '"}'));
                });
            };

            $scope.ScholarInformation = function () {
                $scope.options2 = [];
                angular.forEach($scope.scholarShipSetUps, function (value) {
                    $scope.options2.push(JSON.parse('{"Value":' + value.id + ',' + '"Text":"' + value.scholarName + '"}'));
                });
            };

            $scope.multiTraining = function () {
                var values = "";
                angular.forEach($scope.selectedOptions1, function (value, key) {
                    values = values + value.Text + ',';
                });
                values = values.substring(0, values.length - 1);
                $scope.sisStudentInfo.trainName = values;
                console.log('-----Training Info Name -------');
                console.log(values);
            };

            $scope.multiScholar = function () {
                var values = "";
                angular.forEach($scope.selectedOptions2, function (value, key) {
                    values = values + value.Text + ',';
                });
                values = values.substring(0, values.length - 1);
                $scope.sisStudentInfo.scholarName = values;
                console.log('-----Scholar Info Name -------');
                console.log(values);
            };

            $scope.loadTrainingEdit = function () {
                $scope.options1 = [];
                var array = $scope.selectTraining.split(',');
                var exist = false;
                var selectedExist = true;
                angular.forEach($scope.trainingSetups, function (value) {
                    exist = false;
                    selectedExist = true;
                    angular.forEach(array, function (arrValue, keys) {
                        exist = true;
                        if (selectedExist) {
                            if (arrValue == value.trainName) {
                                $scope.selectedOptions1.push(JSON.parse('{"Value":' + value.id + ',' + '"Text":"' + value.trainName + '"}'));
                            }
                        }
                    });
                    $scope.options1.push(JSON.parse('{"Value":' + value.id + ',' + '"Text":"' + value.trainName + '"}'));
                });
            };

            $scope.loadScholarEdit = function () {
                $scope.options2 = [];
                var array = $scope.selectScholar.split(',');
                var exist = false;
                var selectedExist = true;
                angular.forEach($scope.scholarShipSetUps, function (value) {
                    exist = false;
                    selectedExist = true;
                    angular.forEach(array, function (arrValue, keys) {
                        exist = true;
                        if (selectedExist) {
                            if (arrValue == value.scholarName) {
                                $scope.selectedOptions2.push(JSON.parse('{"Value":' + value.id + ',' + '"Text":"' + value.scholarName + '"}'));
                            }
                        }
                    });
                    $scope.options2.push(JSON.parse('{"Value":' + value.id + ',' + '"Text":"' + value.scholarName + '"}'));
                });
            };

            /*-------------------*/

            // Here to attached the related document.
            $scope.previewImage = function (content, contentType, name) {
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                $rootScope.showFilePreviewModal();
            };

            if($stateParams.id!=null){
                SisStudentInfo.get({id: $stateParams.id}, function (result) {
                    $scope.sisStudentInfo = result;
                    $scope.selectTraining=result.trainName;
                    $scope.selectScholar=result.scholarName;
                    $scope.loadTrainingEdit();
                    $scope.loadScholarEdit();
                    DistrictsByDivision.query({id:$scope.sisStudentInfo.divisionPresent.id},function(result){
                        $scope.districts=result;
                    });

                    DistrictsByDivision.query({id:$scope.sisStudentInfo.divisionPermanent.id},function(result){
                        $scope.districtions=result;
                    });

                    UpazilasByDistrict.query({id:$scope.sisStudentInfo.districtPresent.id},function(result){
                        $scope.Upazilas=result;
                    });

                    UpazilasByDistrict.query({id:$scope.sisStudentInfo.districtPermanent.id},function(result){
                        $scope.UpazilaPer=result;
                    });
                });
            }

            $scope.previewAction = function () {
                if($scope.sisApplicationPreview == false && $scope.sisApplicationPreviewOff == true){
                    $scope.sisApplicationPreview = true;
                    $scope.sisApplicationPreviewOff = false;
                }else if($scope.sisApplicationPreview == true && $scope.sisApplicationPreviewOff == false){
                    $scope.sisApplicationPreview = false;
                    $scope.sisApplicationPreviewOff = true;
                }
            };

            console.log(JSON.stringify(entity));


            Institute.query({page: $scope.page, size: 99999}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.sisInstitute = result;

                 $scope.sisInstitute.forEach(function(data)
                 {
                 $scope.sisInstitutes.push(data.code);

                 });
            });

            $scope.DistrictQuery = function(division){
                DistrictsByDivision.query({id:division.id},function(result){
                    $scope.districts=result;
                  });
            };

            $scope.DistrictPermanent = function(divisionId){
                DistrictsByDivision.query({id:divisionId.id},function(result){
                    $scope.districtions=result;
                });
            };

            $scope.LoadUpazila = function(district){
                UpazilasByDistrict.query({id:district.id},function(result){
                    $scope.Upazilas=result;
                });
            };

            $scope.LoadPerUpazila=function(dist){
                UpazilasByDistrict.query({id:dist.id},function(result){
                    $scope.UpazilaPer=result;
                });
            };

            SisStudentReg.query({page: $scope.page, size: 99999}, function(result, headers){
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.sisApplication = result;
                $scope.sisApplication.forEach(function(data){

                    $scope.sisApplications.push(data.applicationId);
                });

            });

            $scope.searchApplicationId = function (searchdata) {
                if(searchdata != null){

                    $scope.sisStudentReg = [];
                    SisStudentRegByAppId.get({applicationId : searchdata}, function(result) {
                        $scope.sisStudentReg = result;
                        $scope.sisStudentInfo.name = $scope.sisStudentReg.studentName;
                        $scope.sisStudentInfo.instituteName = $scope.sisStudentReg.instituteName;
                        $scope.sisStudentInfo.institute = $scope.sisStudentReg.instituteId;

                        $scope.sisStudentInfo.fatherName = $scope.sisStudentReg.fatherName;
                        $scope.sisStudentInfo.motherName = $scope.sisStudentReg.motherName;
                        $scope.sisStudentInfo.dateOfBirth = $scope.sisStudentReg.dateOfBirth;
                        $scope.sisStudentInfo.gender = $scope.sisStudentReg.genderInfo;
                        $scope.sisStudentInfo.religion = $scope.sisStudentReg.religionId;
                        $scope.sisStudentInfo.bloodGroup = $scope.sisStudentReg.bloodGroups;

                        $scope.sisStudentInfo.presentAddress = $scope.sisStudentReg.presentAddress;
                        $scope.sisStudentInfo.permanentAddress = $scope.sisStudentReg.permanentAddress;
                        $scope.sisStudentInfo.mobileNo = $scope.sisStudentReg.mobileNo;
                        $scope.sisStudentInfo.curriculum = $scope.sisStudentReg.curriculumId;

                        $scope.sisStudentInfo.tradeTechnology = $scope.sisStudentReg.tradeId.name;
                        $scope.sisStudentInfo.shift = $scope.sisStudentReg.shiftId;
                        $scope.sisStudentInfo.emailAddress = $scope.sisStudentReg.emailAddress;
                        $scope.sisStudentInfo.maritalStatus = $scope.sisStudentReg.maritalStatus;
                        console.log($scope.sisStudentInfo.TradeTechnology);
                    });
                }else{
                    alert("Please Enter Application ID");
                }

            };

            $scope.filterByStatus = function () {
                return function (item) {
                    if (item.activeStatus == true)
                    {
                        return true;
                    }
                    return false;
                };
            };

            $scope.filterShiftByStatus = function () {
                return function (item) {
                    if (item.activeStatus == true)
                    {
                        return true;
                    }
                    return false;
                };
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:sisStudentInfoUpdate', result);
                $state.go('sisStudentInfo',{},{reload:true});
            };

            $scope.save = function () {
                if ($scope.sisStudentInfo.id != null) {
                    SisStudentInfo.update($scope.sisStudentInfo, onSaveFinished);
                } else {
                    SisStudentInfo.save($scope.sisStudentInfo, onSaveFinished);
                }
                //$state.go('sisStudentInfo');
            };

            $scope.clear = function () {
                $state.go('sisStudentInfo');
            };

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }

                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }

                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }

                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setStuPicture = function ($file, sisStudentInfo) {
                if (!$file) {
                }
                if ($file.length / 1024 > 200) {

                    sisStudentInfo.stuPictureContentType = null;
                    sisStudentInfo.stupicName = null;
                }
                else {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            sisStudentInfo.stuPicture = base64Data;
                            sisStudentInfo.stuPictureContentType = $file.type;
                            sisStudentInfo.stupicName = $file.name;
                        });
                    };

                }
            };

        }]).directive('aadropdownmultiselect1', function () { //here initialized the new directory for populate the data into Dropdown List box(Option in angularJS).
    return {
        restrict: 'E',
        scope: {
            model: '=',
            multiselectoptions: '=',
            maxlenghttoshowselectedvalues: '=',
            onchangeeventofcheckbox: '&',

        },
        template: '<div class="btn-group" ng-class={open:open}> \
                <button type="button" class="multiselect dropdown-toggle btn btn-width" title="None selected" ng-click="toggledropdown()"> \
                    <span class="multiselect-selected-text"></span> \
                    <b class="caret"></b> \
                </button> \
                <ul class="multiselect-container dropdown-menu"> \
                    <li class="multiselect-item filter" value="0"> \
                        <div class="input-group"> \
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span> \
                            <input class="form-control multiselect-search" type="text" placeholder="Search" ng-model="model.query"><span class="input-group-btn"> \
                                <button class="btn btn-default multiselect-clear-filter" ng-click="clearsearch()" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button> \
                            </span> \
                        </div> \
                    </li> \
                    <li class="multiselect-item multiselect-all"><label style="padding: 3px 20px 3px 40px;margin-top:0px;margin-bottom:0px" class="checkbox"><input type="checkbox" ng-model="model.selectall" ng-change="toggleselectall()">Select all</label></li> \
                    <li ng-repeat="option in (filteredOptions = (multiselectoptions| filter:model.query)) track by $index"><label style="padding: 3px 20px 3px 40px;margin-top:0px;margin-bottom:0px" class="checkbox"><input type="checkbox" ng-checked="isselected(option)" ng-model="option.Selected" ng-change="toggleselecteditem(option);doOnChangeOfCheckBox()" >{{option.Text}}</label></li> \
                </ul> \
            </div>',
        controller: function ($scope) {
            debugger;
            $scope.toggledropdown = function () {
                $scope.open = !$scope.open;
            };

            $scope.toggleselectall = function ($event) {
                var selectallclicked = true;
                if ($scope.model.selectall == false) {
                    selectallclicked = false;
                }
                $scope.doonselectallclick(selectallclicked, $scope.filteredOptions);
            };

            $scope.doonselectallclick = function (selectallclicked, optionArrayList) {
                $scope.model = [];
                if (selectallclicked) {
                    angular.forEach(optionArrayList, function (item, index) {
                        item["Selected"] = true;
                        $scope.model.push(item);
                    });

                    if (optionArrayList.length == $scope.multiselectoptions.length) {
                        $scope.model.selectall = true;
                    }
                }
                else {
                    angular.forEach(optionArrayList, function (item, index) {
                        item["Selected"] = false;
                    });
                    $scope.model.selectall = false;
                }
                $scope.settoggletext();
            }

            $scope.toggleselecteditem = function (option) {
                var intIndex = -1;
                angular.forEach($scope.model, function (item, index) {
                    if (item.Value == option.Value) {
                        intIndex = index;
                    }
                });

                if (intIndex >= 0) {
                    $scope.model.splice(intIndex, 1);
                }
                else {
                    $scope.model.push(option);
                }

                if ($scope.model.length == $scope.multiselectoptions.length) {
                    $scope.model.selectall = true;
                }
                else {
                    $scope.model.selectall = false;
                }
                $scope.settoggletext();
            };

            $scope.clearsearch = function () {
                $scope.model.query = "";
            }

            $scope.settoggletext = function () {
                if ($scope.model.length > $scope.maxlenghttoshowselectedvalues) {
                    $scope.model.toggletext = $scope.model.length + " Selected";
                }
                else {
                    $scope.model.toggletext = "";
                    angular.forEach($scope.model, function (item, index) {
                        if (index == 0) {
                            $scope.model.toggletext = item.Text;
                        }
                        else {
                            $scope.model.toggletext += ", " + item.Text;
                        }
                    });

                    if (!($scope.model.toggletext.length > 0)) {
                        $scope.model.toggletext = "None Selected"
                    }
                }
            }

            $scope.isselected = function (option) {
                var selected = false;
                angular.forEach($scope.model, function (item, index) {
                    if (item.Value == option.Value) {
                        selected = true;
                    }
                });
                option.Selected = selected;
                return selected;
            }

            $scope.doOnChangeOfCheckBox = function () {
                $scope.onchangeeventofcheckbox();
            }

        }
    }
}).directive('aadropdownmultiselect2', function () { //here initialized the new directory for populate the data into Dropdown List box(Option in angularJS).
    return {
        restrict: 'E',
        scope: {
            model: '=',
            multiselectoptions: '=',
            maxlenghttoshowselectedvalues: '=',
            onchangeeventofcheckbox: '&',

        },
        template: '<div class="btn-group" ng-class={open:open}> \
                <button type="button" class="multiselect dropdown-toggle btn btn-width" title="None selected" ng-click="toggledropdown()"> \
                    <span class="multiselect-selected-text"></span> \
                    <b class="caret"></b> \
                </button> \
                <ul class="multiselect-container dropdown-menu"> \
                    <li class="multiselect-item filter" value="0"> \
                        <div class="input-group"> \
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span> \
                            <input class="form-control multiselect-search" type="text" placeholder="Search" ng-model="model.query"><span class="input-group-btn"> \
                                <button class="btn btn-default multiselect-clear-filter" ng-click="clearsearch()" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button> \
                            </span> \
                        </div> \
                    </li> \
                    <li class="multiselect-item multiselect-all"><label style="padding: 3px 20px 3px 40px;margin-top:0px;margin-bottom:0px" class="checkbox"><input type="checkbox" ng-model="model.selectall" ng-change="toggleselectall()">Select all</label></li> \
                    <li ng-repeat="option in (filteredOptions = (multiselectoptions| filter:model.query)) track by $index"><label style="padding: 3px 20px 3px 40px;margin-top:0px;margin-bottom:0px" class="checkbox"><input type="checkbox" ng-checked="isselected(option)" ng-model="option.Selected" ng-change="toggleselecteditem(option);doOnChangeOfCheckBox()" >{{option.Text}}</label></li> \
                </ul> \
            </div>',
        controller: function ($scope) {
            debugger;
            $scope.toggledropdown = function () {
                $scope.open = !$scope.open;
            };

            $scope.toggleselectall = function ($event) {
                var selectallclicked = true;
                if ($scope.model.selectall == false) {
                    selectallclicked = false;
                }
                $scope.doonselectallclick(selectallclicked, $scope.filteredOptions);
            };

            $scope.doonselectallclick = function (selectallclicked, optionArrayList) {
                $scope.model = [];
                if (selectallclicked) {
                    angular.forEach(optionArrayList, function (item, index) {
                        item["Selected"] = true;
                        $scope.model.push(item);
                    });

                    if (optionArrayList.length == $scope.multiselectoptions.length) {
                        $scope.model.selectall = true;
                    }
                }
                else {
                    angular.forEach(optionArrayList, function (item, index) {
                        item["Selected"] = false;
                    });
                    $scope.model.selectall = false;
                }
                $scope.settoggletext();
            }

            $scope.toggleselecteditem = function (option) {
                var intIndex = -1;
                angular.forEach($scope.model, function (item, index) {
                    if (item.Value == option.Value) {
                        intIndex = index;
                    }
                });

                if (intIndex >= 0) {
                    $scope.model.splice(intIndex, 1);
                }
                else {
                    $scope.model.push(option);
                }

                if ($scope.model.length == $scope.multiselectoptions.length) {
                    $scope.model.selectall = true;
                }
                else {
                    $scope.model.selectall = false;
                }
                $scope.settoggletext();
            };

            $scope.clearsearch = function () {
                $scope.model.query = "";
            }

            $scope.settoggletext = function () {
                if ($scope.model.length > $scope.maxlenghttoshowselectedvalues) {
                    $scope.model.toggletext = $scope.model.length + " Selected";
                }
                else {
                    $scope.model.toggletext = "";
                    angular.forEach($scope.model, function (item, index) {
                        if (index == 0) {
                            $scope.model.toggletext = item.Text;
                        }
                        else {
                            $scope.model.toggletext += ", " + item.Text;
                        }
                    });

                    if (!($scope.model.toggletext.length > 0)) {
                        $scope.model.toggletext = "None Selected"
                    }
                }
            }

            $scope.isselected = function (option) {
                var selected = false;
                angular.forEach($scope.model, function (item, index) {
                    if (item.Value == option.Value) {
                        selected = true;
                    }
                });
                option.Selected = selected;
                return selected;
            }

            $scope.doOnChangeOfCheckBox = function () {
                $scope.onchangeeventofcheckbox();
            }

        }
    }
});

/*--------------SisStudentInfoController--------*/
angular.module('stepApp')
    .controller('SisStudentInfoController',
        ['$scope', 'SisStudentInfo', 'SisStudentInfoSearch', 'ParseLinks', 'SisStudentInfoGovt','Principal',
        function ($scope, SisStudentInfo, SisStudentInfoSearch, ParseLinks, SisStudentInfoGovt,Principal) {
        $scope.sisStudentInfos = [];
        $scope.SisStudentInfoGovt = [];
        $scope.page = 0;
        $scope.pageSize = 10;
        $scope.loadAll = function() {
            if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                SisStudentInfo.query({page: $scope.page, size: 2000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.sisStudentInfos = result;
                });
            }else
            if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_GOVT_STUDENT']) || Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                SisStudentInfoGovt.query({page: $scope.page, size: 2000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.sisStudentInfos = result;
                });
            }
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            SisStudentInfo.get({id: id}, function(result) {
                $scope.sisStudentInfo = result;
                $('#deleteSisStudentInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            SisStudentInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSisStudentInfoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SisStudentInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.sisStudentInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.sisStudentInfo = {
                name: null,
                stuPicture: null,
                stuPictureContentType: null,
                instituteName: null,
                TradeTechnology: null,
                studentName: null,
                fatherName: null,
                motherName: null,
                dateOfBirth: null,
                presentAddress: null,
                permanentAddress: null,
                nationality: null,
                nationalIdNo: null,
                birthCertificateNo: null,
                mobileNo: null,
                contactNoHome: null,
                emailAddress: null,
                gender: null,
                maritalStatus: null,
                bloodGroup: null,
                religion: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                curriculum: null,
                applicationId: null,
                id: null
            };
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    }]);


/*---------------END---------------------------*/


/*----------------------------SisStudentInfoDetailController----------------------*/

angular.module('stepApp')
    .controller('SisStudentInfoDetailController',
        ['$scope', '$sce', '$rootScope', '$stateParams', 'entity', 'SisStudentInfo', 'Division','District', 'Country', 'SisQuota',
        function ($scope, $sce, $rootScope, $stateParams, entity, SisStudentInfo, Division, District, Country, SisQuota) {
        $scope.sisStudentInfo = entity;
        $scope.load = function (id) {
            SisStudentInfo.get({id: id}, function(result) {
                $scope.sisStudentInfo = result;
                if ($scope.sisStudentInfo.stupicName != null) {
                    var blob = $rootScope.b64toBlob($scope.sisStudentInfo.stuPicture, $scope.sisStudentInfo.stuPictureContentType);
                    $scope.urlStupicName = (window.URL || window.webkitURL).createObjectURL(blob);
                }
            });
        };

        var unsubscribe = $rootScope.$on('stepApp:sisStudentInfoUpdate', function(event, result) {
            $scope.sisStudentInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

        // Here to attached the related document.
        $scope.previewImage = function (content, contentType, name) {
            var blob = $rootScope.b64toBlob(content, contentType);
            $rootScope.viewerObject.content = $sce.trustAsResourceUrl((window.URL || window.webkitURL).createObjectURL(blob));
            $rootScope.viewerObject.contentType = contentType;
            $rootScope.viewerObject.pageTitle = name;
            $rootScope.showFilePreviewModal();
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    }]);

/*  ------------------ END ----------------- */

/*---------------SisStudentRegController--------------------------*/

angular.module('stepApp')
    .controller('SisStudentRegController', function ($scope, SisStudentReg, SisStudentRegSearch, ParseLinks,Principal) {
        $scope.sisStudentRegs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                SisStudentReg.query({page: $scope.page, size: 2000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.sisStudentRegs = result;
                });
            }else
            if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_INSTITUTE'])) {
                SisStudentReg.query({page: $scope.page, size: 2000}, function (result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.sisStudentRegs = result;

                });
            }
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            SisStudentReg.get({id: id}, function(result) {
                $scope.sisStudentReg = result;
                $('#deleteSisStudentRegConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            SisStudentReg.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSisStudentRegConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SisStudentRegSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.sisStudentRegs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.sisStudentReg = {
                applicationId: null,
                instCategory: null,
                instituteName: null,
                curriculum: null,
                TradeTechnology: null,
                subject1: null,
                subject2: null,
                subject3: null,
                subject4: null,
                subject5: null,
                optional: null,
                shift: null,
                semester: null,
                studentName: null,
                fatherName: null,
                motherName: null,
                dateOfBirth: null,
                gender: null,
                religion: null,
                bloodGroup: null,
                quota: null,
                nationality: null,
                mobileNo: null,
                contactNoHome: null,
                emailAddress: null,
                presentAddress: null,
                permanentAddress: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                maritalStatus: null,
                id: null
            };
        };
    });

/*-------------------END---------------------*/
/*------------------SisStudentRegDetailController------------------------------*/
angular.module('stepApp')
    .controller('SisStudentRegDetailController', function ($scope, $rootScope, $stateParams, entity, SisStudentReg) {
        $scope.sisStudentReg = entity;
        $scope.load = function (id) {
            SisStudentReg.get({id:id}, function(result) {
                $scope.sisStudentReg = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:sisStudentRegUpdate', function(event, result) {
            $scope.sisStudentReg = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });

/*------------------------END------------------------------*/

/*-------------------SisStudentRegDialogController--------------------*/

angular.module('stepApp').controller('SisStudentRegDialogController',
    ['$scope', '$state', '$stateParams', 'entity', 'SisStudentReg','InstituteInformation','BloodGroupInfo','GenderInfo','MaritalInfo','SisQuota','CmsCurriculum','Division','CmsSemester','TradeInformation','DistrictsByDivision','UpazilasByDistrict','Religion','AllSubjectInfo','AllCurriculumInfo','MiscTypeSetupByCategory',
        function($scope, $state, $stateParams, entity, SisStudentReg,InstituteInformation,BloodGroupInfo,GenderInfo,MaritalInfo,SisQuota,CmsCurriculum,Division,CmsSemester,TradeInformation,DistrictsByDivision,UpazilasByDistrict,Religion,AllSubjectInfo,AllCurriculumInfo,MiscTypeSetupByCategory) {

            $scope.sisStudentReg = entity;
            $scope.InstituteInformation=[];
            $scope.TradeInformation=[];
            $scope.iisCurriculums =[];

            $scope.AllSubjectInfo=[];
            $scope.AllCurriculumInfo=[];
            $scope.divisions = Division.query({size:200});
            $scope.religions = Religion.query({size:10});
            $scope.cmsSemesters = CmsSemester.query({size:2000});
            $scope.bloodGroupInfos = BloodGroupInfo.query({size:15});
            $scope.genderInfos = GenderInfo.query({size:10});
            $scope.maritalInfos = MaritalInfo.query({size:10});
            $scope.quotas = SisQuota.query({size:2000});
            //$scope.almshiftsetups = AlmShiftSetup.query({size:2000});
            $scope.miscTypeSetupByCategorys = MiscTypeSetupByCategory.query({cat:'Shift',stat:true});
            $scope.miscTypeSetupByCategoryss = MiscTypeSetupByCategory.query({cat:'Section',stat:true});

            if($stateParams.id!=null){
                SisStudentReg.get({id : $stateParams.id}, function(result) {
                    $scope.sisStudentReg = result;
                    DistrictsByDivision.query({id:result.divisionPresent.id},function(data){
                        $scope.districts=data;
                    });
                    DistrictsByDivision.query({id:result.divisionPermanent.id},function(distPer){
                        $scope.districtions=distPer;
                    });
                    UpazilasByDistrict.query({id:result.districtPresent.id},function(thanaUpa){
                        $scope.Upazilas=thanaUpa;
                    });
                    UpazilasByDistrict.query({id:result.districtPermanent.id},function(UpaThana){
                        $scope.UpazilaPer=UpaThana;
                    });
                    AllCurriculumInfo.get({instituteId:result.instituteId.id},function(data){
                        $scope.iisCurriculums=data;
                    });
                    TradeInformation.get({cmsCurriculumId:result.curriculumId.cmsCurriculum.id,size:2000000},function(tradeData){
                        $scope.trades=tradeData;
                    });
                    AllSubjectInfo.get({tradeId:result.tradeId.id},function(data){
                        $scope.allSubjects= data;
                    });

                });

            };

             $scope.DistrictQuery = function(division){
                 DistrictsByDivision.query({id:division.id},function(result){
                     $scope.districts=result;

                 });
             };
            $scope.DistrictPermanent = function(divisionId){
                DistrictsByDivision.query({id:divisionId.id},function(result){
                    $scope.districtions=result;
                });
            };

            $scope.LoadUpazila = function(district){
                UpazilasByDistrict.query({id:district.id},function(result){
                    $scope.Upazilas=result;
                });
            };

            $scope.LoadPerUpazila=function(dist){
                UpazilasByDistrict.query({id:dist.id},function(result){
                    $scope.UpazilaPer=result;
                });
            };
            $scope.instituteSearch = function(code){
                InstituteInformation.get({code:code},function(result){

                    $scope.sisStudentReg.instituteName = result.name;
                    console.log($scope.sisStudentReg.instituteName);
                    $scope.sisStudentReg.instCategory = result.instCategory.name;
                    $scope.sisStudentReg.instituteId=result.institute.id;
                    $scope.instId = result.institute.id;
                    $scope.InstType=result.type;

                        AllCurriculumInfo.get({instituteId:$scope.instId},function(result){
                            $scope.iisCurriculums=result;
                        });
                });
            };
            $scope.AllowedStudent = function(){
                if($scope.InstType == 'NonGovernment'){
                    alert('Only Government Students are allowed to registration');
                    $scope.sisStudentReg.instituteName = '';
                    $scope.sisStudentReg.instCategory = '';
                    $scope.sisStudentReg.studentName = '';
                }
            };
            $scope.subjectInfo = function(subject){
                AllSubjectInfo.get({tradeId:subject.id},function(result){
                    $scope.allSubjects= result;
                });
            };
            $scope.curriculumTrade = function(curriculumId){
                TradeInformation.get({cmsCurriculumId:curriculumId.cmsCurriculum.id,size:2000000},function(result){
                    $scope.trades=result;

                });
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            var onSaveUpdateFinished = function (result) {
                $scope.$emit('stepApp:sisStudentRegUpdate', result);
                $scope.isSaving = false;
                $state.go('sisStudentReg');
            };

            $scope.save = function () {
                if ($scope.sisStudentReg.id != null) {
                    SisStudentReg.update($scope.sisStudentReg, onSaveUpdateFinished);
                } else {
                    SisStudentReg.save($scope.sisStudentReg, onSaveUpdateFinished);
                }
            };

            $scope.clear = function() {
                $state.go('sisStudentReg');
            };
        }]);

/*------------------------END-----------------------------------------*/

/*------------------SisQuotaController-----------------------------*/
angular.module('stepApp')
    .controller('SisQuotaController',
    ['$scope', 'SisQuota', 'SisQuotaSearch', 'ParseLinks',
        function ($scope, SisQuota, SisQuotaSearch, ParseLinks) {
            $scope.sisQuotas = [];
            $scope.page = 0;
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.loadAll = function() {
                SisQuota.query({page: $scope.page, size: 1000000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.sisQuotas = result;
                });
            };
            $scope.loadPage = function(page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            $scope.delete = function (id) {
                SisQuota.get({id: id}, function(result) {
                    $scope.sisQuota = result;
                    $('#deleteSisQuotaConfirmation').modal('show');
                });
            };

            $scope.confirmDelete = function (id) {
                SisQuota.delete({id: id},
                    function () {
                        $scope.loadAll();
                        $('#deleteSisQuotaConfirmation').modal('hide');
                        $scope.clear();
                    });
            };

            $scope.search = function () {
                SisQuotaSearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.sisQuotas = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.sisQuota = {
                    name: null,
                    description: null,
                    isFor: null,
                    activeStatus: null,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
        }]);

/*--------------------------END---------------------------------*/

/*-----------------------SisQuotaDetailController---------------*/
angular.module('stepApp')
    .controller('SisQuotaDetailController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'SisQuota',

        function ($scope, $rootScope, $stateParams, entity, SisQuota) {
            $scope.sisQuota = entity;
            $scope.load = function (id) {
                SisQuota.get({id: id}, function(result) {
                    $scope.sisQuota = result;
                });
            };
            var unsubscribe = $rootScope.$on('stepApp:sisQuotaUpdate', function(event, result) {
                $scope.sisQuota = result;
            });
            $scope.$on('$destroy', unsubscribe);

        }]);

/*------------------------------END---------------------*/

/*--------------------SisQuotaDialogController----------------------------------*/

angular.module('stepApp').controller('SisQuotaDialogController',
    ['$scope', '$rootScope','$stateParams', 'entity', 'Principal','SisQuota', '$state', 'User','DateUtils',
        function($scope, $rootScope, $stateParams,  entity, Principal, SisQuota, $state, User, DateUtils) {

            $scope.sisQuota = entity;
            $scope.load = function(id) {
                SisQuota.get({id : id}, function(result) {
                    $scope.sisQuota = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:sisQuotaUpdate', result);
                $scope.isSaving = false;
                $state.go('sisQuota');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.isSaving = true;
                        $scope.sisQuota.updateBy = result.id;
                        $scope.sisQuota.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                        if ($scope.sisQuota.id != null)
                        {
                            SisQuota.update($scope.sisQuota, onSaveSuccess, onSaveError);
                            $rootScope.setWarningMessage('stepApp.sisQuota.updated');
                        }
                        else
                        {
                            $scope.sisQuota.createBy = result.id;
                            $scope.sisQuota.createDate = DateUtils.convertLocaleDateToServer(new Date());
                            SisQuota.save($scope.sisQuota, onSaveSuccess, onSaveError);
                            $rootScope.setSuccessMessage('stepApp.sisQuota.created');
                        }
                    });
                });
            };


            $scope.clear = function() {
                $state.go('sisQuota');
            };

        }]);

/*-----------------------------END-----------------------*/

/*-----------------SisEducationHistoryController------------------------*/

angular.module('stepApp')
    .controller('SisEducationHistoryController',
    ['$scope', 'Principal', 'SisEducationHistory', 'SisEducationHistorySearch', 'ParseLinks', 'SisEducationHistoryInfo',
        function ($scope, Principal, SisEducationHistory, SisEducationHistorySearch, ParseLinks,SisEducationHistoryInfo) {
            $scope.sisEducationHistorys = [];
            $scope.SisEducationHistoryInfo = [];
            $scope.page = 0;
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.loadAll = function() {

                if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_ADMIN'])) {
                    SisEducationHistory.query({page: $scope.page, size: 999999999999999999999999}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.sisEducationHistorys = result;
                    });
                }else
                if (Principal.isAuthenticated() && Principal.hasAnyAuthority(['ROLE_GOVT_STUDENT'])) {
                    SisEducationHistoryInfo.query({page: $scope.page, size: 2000}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.sisEducationHistorys = result;
                    });
                }

            };
            $scope.loadPage = function(page) {
                $scope.page = page;
                $scope.loadAll();
            };
            $scope.loadAll();

            $scope.delete = function (id) {
                SisEducationHistory.get({id: id}, function(result) {
                    $scope.sisEducationHistory = result;
                    $('#deleteSisEducationHistoryConfirmation').modal('show');
                });
            };

            $scope.confirmDelete = function (id) {
                SisEducationHistory.delete({id: id},
                    function () {
                        $scope.loadAll();
                        $('#deleteSisEducationHistoryConfirmation').modal('hide');
                        $scope.clear();
                    });
            };

            $scope.search = function () {
                SisEducationHistorySearch.query({query: $scope.searchQuery}, function(result) {
                    $scope.sisEducationHistorys = result;
                }, function(response) {
                    if(response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };

            $scope.clear = function () {
                $scope.sisEducationHistory = {
                    yearOrSemester: null,
                    rollNo: null,
                    majorOrDept: null,
                    divisionOrGpa: null,
                    passingYear: null,
                    achievedCertificate: null,
                    activeStatus: null,
                    createDate: null,
                    createBy: null,
                    updateDate: null,
                    updateBy: null,
                    id: null
                };
            };
        }]);

/*-----------------------------END-------------------------*/
/*-----------------------SisEducationHistoryDetailController-----------------*/
angular.module('stepApp')
    .controller('SisEducationHistoryDetailController',
    ['$scope', '$rootScope', '$stateParams', 'entity', 'SisEducationHistory', 'EduLevel', 'EduBoard',
        function ($scope, $rootScope, $stateParams, entity, SisEducationHistory, EduLevel, EduBoard) {
            $scope.sisEducationHistory = entity;
            $scope.load = function (id) {
                SisEducationHistory.get({id: id}, function(result) {
                    $scope.sisEducationHistory = result;
                });
            };
            var unsubscribe = $rootScope.$on('stepApp:sisEducationHistoryUpdate', function(event, result) {
                $scope.sisEducationHistory = result;
            });
            $scope.$on('$destroy', unsubscribe);

        }]);

/*-----------------------------------END-------------------------*/

/*-------------------SisEducationHistoryDialogController-------------------*/

angular.module('stepApp').controller('SisEducationHistoryDialogController',
    ['$scope', '$rootScope','$stateParams', 'entity', 'Principal','SisEducationHistory', '$state', 'User','DateUtils','EduLevel', 'EduBoard', 'JobPlacementStudent',
        function($scope, $rootScope, $stateParams,  entity, Principal, SisEducationHistory, $state, User, DateUtils, EduLevel, EduBoard, JobPlacementStudent) {

            $scope.sisEducationHistory = entity;
            $scope.edulevels = EduLevel.query({size:999999});
            $scope.eduboards = EduBoard.query({size:999999});
            $scope.JobPlacementStudent =[];
            $scope.sisEducationHistory.boardUni=false;
            $scope.load = function(id) {
                SisEducationHistory.get({id : id}, function(result) {
                    $scope.sisEducationHistory = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:sisEducationHistoryUpdate', result);
                $state.go('sisEducationHistory');
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:sisEducationHistoryUpdate', result);
                $scope.isSaving = false;
                $state.go('sisEducationHistory');
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            $scope.userId =$scope.accountName;
            $scope.JobPlacement = function(){
                JobPlacementStudent.get({userId:$scope.userId},function(result){
                    $scope.sisEducationHistory.orgName=result.orgName;
                });
            };

            $scope.NotAssign = function(orgName){
                if(orgName == null){
                    alert('Please Fill up the job placement Information')
                }
            };

            $scope.save = function ()
            {
                if ($scope.sisEducationHistory.id != null) {
                    SisEducationHistory.update($scope.sisEducationHistory, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.sisEducationHistory.updated');
                } else {
                    SisEducationHistory.save($scope.sisEducationHistory, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.sisEducationHistory.created');
                }

            };


            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
        }]);


/*--------------------------------------END---------------------------*/

/*-----------ScholarShipSetUpController----------*/
angular.module('stepApp')
    .controller('ScholarShipSetUpController',
        ['$scope', 'ScholarShipSetUp', 'ScholarShipSetUpSearch', 'ParseLinks',
        function ($scope, ScholarShipSetUp, ScholarShipSetUpSearch, ParseLinks) {
        $scope.scholarShipSetUps = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            ScholarShipSetUp.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.scholarShipSetUps = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ScholarShipSetUp.get({id: id}, function(result) {
                $scope.scholarShipSetUp = result;
                $('#deleteScholarShipSetUpConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ScholarShipSetUp.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteScholarShipSetUpConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ScholarShipSetUpSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.scholarShipSetUps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.scholarShipSetUp = {
                scholarId: null,
                scholarName: null,
                stiphen: null,
                given_by: null,
                duration: null,
                run_institute: null,
                remarks: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    }]);

/*--------END-------------*/

/*-------------------------ScholarShipSetUpDetailController---------*/

angular.module('stepApp')
    .controller('ScholarShipSetUpDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'ScholarShipSetUp',
        function ($scope, $rootScope, $stateParams, entity, ScholarShipSetUp) {
        $scope.scholarShipSetUp = entity;
        $scope.load = function (id) {
            ScholarShipSetUp.get({id: id}, function(result) {
                $scope.scholarShipSetUp = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:scholarShipSetUpUpdate', function(event, result) {
            $scope.scholarShipSetUp = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);
/*------------------------------END------------------------------------*/

/*------------------ScholarShipSetUpDialogController--------------------*/

angular.module('stepApp').controller('ScholarShipSetUpDialogController',
    ['$state','$scope', '$stateParams', 'entity', 'ScholarShipSetUp',
        function($state,$scope, $stateParams, entity, ScholarShipSetUp) {

            $scope.scholarShipSetUp = entity;
            $scope.load = function(id) {
                ScholarShipSetUp.get({id : id}, function(result) {
                    $scope.scholarShipSetUp = result;
                });
            };


            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:scholarShipSetUpUpdate', result);
                //$modalInstance.close(result);
                $state.go('scholarShipSetUp');
            };

            $scope.save = function () {
                if ($scope.scholarShipSetUp.id != null) {
                    ScholarShipSetUp.update($scope.scholarShipSetUp, onSaveFinished);
                } else {
                    ScholarShipSetUp.save($scope.scholarShipSetUp, onSaveFinished);
                }

            };

            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
                $state.go('scholarShipSetUp');
            };
        }]);

/*-------------------END------------------------*/

/*-------------------TrainingSetupController----------------------*/

angular.module('stepApp')
    .controller('TrainingSetupController',
        ['$scope', 'TrainingSetup', 'TrainingSetupSearch', 'ParseLinks',
        function ($scope, TrainingSetup, TrainingSetupSearch, ParseLinks) {
        $scope.trainingSetups = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TrainingSetup.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.trainingSetups = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            TrainingSetup.get({id: id}, function(result) {
                $scope.trainingSetup = result;
                $('#deleteTrainingSetupConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            TrainingSetup.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTrainingSetupConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TrainingSetupSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.trainingSetups = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.trainingSetup = {
                trainId: null,
                tainType: null,
                trainName: null,
                instituteName: null,
                fromDate: null,
                toDate: null,
                remarks: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    }]);


/*------------------END---------------------*/

/*-------------------TrainingSetupDetailController---------------*/

angular.module('stepApp')
    .controller('TrainingSetupDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'TrainingSetup', 'Country',
        function ($scope, $rootScope, $stateParams, entity, TrainingSetup, Country) {
        $scope.trainingSetup = entity;
        $scope.load = function (id) {
            TrainingSetup.get({id: id}, function(result) {
                $scope.trainingSetup = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:trainingSetupUpdate', function(event, result) {
            $scope.trainingSetup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }]);

/*----------------END-----------------------*/

/*------------------------TrainingSetupDialogController-------------------*/

angular.module('stepApp').controller('TrainingSetupDialogController',
    ['$state', '$filter', '$scope', '$stateParams', 'entity', 'TrainingSetup', 'Country',
        function($state, $filter, $scope, $stateParams, entity, TrainingSetup, Country) {

            $scope.trainingSetup = entity;
            $scope.countrys = Country.query();
            $scope.load = function(id) {
                TrainingSetup.get({id : id}, function(result) {
                    $scope.trainingSetup = result;
                });
            };

            $scope.dateCompare = function (date1, date2) {
                if (moment(date1).isAfter(date2)) {
                    alert('To Date will be greater than From date!')
                    $scope.trainingSetup.toDate = null;
                }

            };


            /*----------------*/

            $scope.duration=function (firstDate,secondDate) {
                console.log('-----Duration----------');
                var date = $filter('date')(new Date(firstDate), 'yyyy/MM/dd');
                if(secondDate==null){
                    //$scope.trainingSetup.toDate=null;
                    var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                    var year = new Date().getFullYear();
                    var month = new Date().getMonth() + 1;
                    var day = new Date().getDate();
                    var time=new Date().getTime();
                }else{
                    today =$filter('date')(new Date(secondDate), 'yyyy/MM/dd');
                    var year = new Date(secondDate).getFullYear();
                    var month = new Date(secondDate).getMonth() + 1;
                    var day = new Date(secondDate).getDate();
                    var time=  new Date(secondDate).getTime()
                }

                date = date.split('/');
                today=today.split('/');
                var yy = parseInt(date[0]);
                var mm = parseInt(date[1]);
                var dd = parseInt(date[2]);
                var years, months, days;
                // months
                months = month - mm;
                if (day < dd) {
                    months = months - 1;
                }
                // years
                years = year - yy;
                if (month * 100 + day < mm * 100 + dd) {
                    years = years - 1;
                    months = months + 12;
                }
                // days
                $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
                $scope.dura=years+" Years, "+months+" Months, "+$scope.days+" Days"
                $scope.trainingSetup.duration=$scope.dura;

            }

            /*----------------*/

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:trainingSetupUpdate', result);
               // $modalInstance.close(result);
                $state.go('trainingSetup');
            };

            $scope.save = function () {
                if ($scope.trainingSetup.id != null) {
                    TrainingSetup.update($scope.trainingSetup, onSaveFinished);
                } else {
                    TrainingSetup.save($scope.trainingSetup, onSaveFinished);
                }
            };

            $scope.clear = function() {
               // $modalInstance.dismiss('cancel');
                $state.go('trainingSetup');
            };
        }]);
/*----------------------END------------------------*/


