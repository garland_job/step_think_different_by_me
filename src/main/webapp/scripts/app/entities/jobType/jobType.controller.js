'use strict';

angular.module('stepApp')
    .controller('JobTypeController',
     ['$scope', '$state', '$modal', 'JobType', 'JobTypeSearch', 'ParseLinks',
     function ($scope, $state, $modal, JobType, JobTypeSearch, ParseLinks) {

        $scope.jobTypes = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            JobType.query({page: $scope.page, size: 5000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.jobTypes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            JobTypeSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.jobTypes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.jobType = {
                name: null,
                description: null,
                status: null,
                id: null
            };
        };
    }]);
/*jobType-dialog.controller.js*/

angular.module('stepApp').controller('JobTypeDialogController',
    ['$scope','$rootScope', '$stateParams', '$state', 'entity', 'JobType',
        function($scope, $rootScope, $stateParams, $state, entity, JobType) {

            $scope.jobType = entity;
            $scope.load = function(id) {
                JobType.get({id : id}, function(result) {
                    $scope.jobType = result;
                });
            };

            var onSaveSuccess = function (result) {
                // $scope.$emit('stepApp:jobTypeUpdate', result);
                // $modalInstance.close(result);
                $scope.isSaving = false;
                $state.go('jobType', null, { reload: true });
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.jobType.id != null) {
                    JobType.update($scope.jobType, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.jobType.updated');
                } else {
                    JobType.save($scope.jobType, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.jobType.created');
                }
            };

            $scope.clear = function() {
                //$modalInstance.dismiss('cancel');
            };
        }]);
/*jobType-detail.controller.js*/

angular.module('stepApp')
    .controller('JobTypeDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'JobType',
            function ($scope, $rootScope, $stateParams, entity, JobType) {
                $scope.jobType = entity;
                $scope.load = function (id) {
                    JobType.get({id: id}, function(result) {
                        $scope.jobType = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:jobTypeUpdate', function(event, result) {
                    $scope.jobType = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*jobType-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('JobTypeDeleteController',
        ['$scope', '$rootScope', '$modalInstance', 'entity', 'JobType',
            function($scope, $rootScope, $modalInstance, entity, JobType) {

                $scope.jobType = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    JobType.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.jobType.deleted');
                        });
                };

            }]);
