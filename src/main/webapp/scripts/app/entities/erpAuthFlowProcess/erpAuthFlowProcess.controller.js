'use strict';

angular.module('stepApp')
    .controller('ErpAuthFlowProcessController', function ($scope, ErpAuthFlowProcess, ErpAuthFlowProcessSearch, ParseLinks) {
        $scope.erpAuthFlowProcesss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            ErpAuthFlowProcess.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.erpAuthFlowProcesss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ErpAuthFlowProcess.get({id: id}, function(result) {
                $scope.erpAuthFlowProcess = result;
                $('#deleteErpAuthFlowProcessConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ErpAuthFlowProcess.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteErpAuthFlowProcessConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ErpAuthFlowProcessSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.erpAuthFlowProcesss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.erpAuthFlowProcess = {
                refDomainId: null,
                refDomainName: null,
                authorityStatus: null,
                erpAuthorityAction: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*erpAuthFlowProcess-dialog.controller.js*/

angular.module('stepApp').controller('ErpAuthFlowProcessDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ErpAuthFlowProcess', 'ErpAuthorityFlow', 'HrEmployeeInfo',
        function($scope, $stateParams, $modalInstance, entity, ErpAuthFlowProcess, ErpAuthorityFlow, HrEmployeeInfo) {

            $scope.erpAuthFlowProcess = entity;
            $scope.erpauthorityflows = ErpAuthorityFlow.query();
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function(id) {
                ErpAuthFlowProcess.get({id : id}, function(result) {
                    $scope.erpAuthFlowProcess = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:erpAuthFlowProcessUpdate', result);
                $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.erpAuthFlowProcess.id != null) {
                    ErpAuthFlowProcess.update($scope.erpAuthFlowProcess, onSaveFinished);
                } else {
                    ErpAuthFlowProcess.save($scope.erpAuthFlowProcess, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*erpAuthFlowProcess-detail.controller.js*/

angular.module('stepApp')
    .controller('ErpAuthFlowProcessDetailController', function ($scope, $rootScope, $stateParams, entity, ErpAuthFlowProcess, ErpAuthorityFlow, HrEmployeeInfo) {
        $scope.erpAuthFlowProcess = entity;
        $scope.load = function (id) {
            ErpAuthFlowProcess.get({id: id}, function(result) {
                $scope.erpAuthFlowProcess = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:erpAuthFlowProcessUpdate', function(event, result) {
            $scope.erpAuthFlowProcess = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
