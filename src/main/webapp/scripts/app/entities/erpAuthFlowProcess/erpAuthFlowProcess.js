'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('erpAuthFlowProcess', {
                parent: 'entity',
                url: '/erpAuthFlowProcesss',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.erpAuthFlowProcess.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/erpAuthFlowProcess/erpAuthFlowProcesss.html',
                        controller: 'ErpAuthFlowProcessController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('erpAuthFlowProcess');
                        $translatePartialLoader.addPart('authorityStatus');
                        $translatePartialLoader.addPart('erpAuthorityAction');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('erpAuthFlowProcess.detail', {
                parent: 'entity',
                url: '/erpAuthFlowProcess/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.erpAuthFlowProcess.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/erpAuthFlowProcess/erpAuthFlowProcess-detail.html',
                        controller: 'ErpAuthFlowProcessDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('erpAuthFlowProcess');
                        $translatePartialLoader.addPart('authorityStatus');
                        $translatePartialLoader.addPart('erpAuthorityAction');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ErpAuthFlowProcess', function($stateParams, ErpAuthFlowProcess) {
                        return ErpAuthFlowProcess.get({id : $stateParams.id});
                    }]
                }
            })
            .state('erpAuthFlowProcess.new', {
                parent: 'erpAuthFlowProcess',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/erpAuthFlowProcess/erpAuthFlowProcess-dialog.html',
                        controller: 'ErpAuthFlowProcessDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    refDomainId: null,
                                    refDomainName: null,
                                    authorityStatus: null,
                                    erpAuthorityAction: null,
                                    activeStatus: null,
                                    createDate: null,
                                    createBy: null,
                                    updateDate: null,
                                    updateBy: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('erpAuthFlowProcess', null, { reload: true });
                    }, function() {
                        $state.go('erpAuthFlowProcess');
                    })
                }]
            })
            .state('erpAuthFlowProcess.edit', {
                parent: 'erpAuthFlowProcess',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/erpAuthFlowProcess/erpAuthFlowProcess-dialog.html',
                        controller: 'ErpAuthFlowProcessDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ErpAuthFlowProcess', function(ErpAuthFlowProcess) {
                                return ErpAuthFlowProcess.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('erpAuthFlowProcess', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
