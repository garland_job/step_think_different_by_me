/*
'use strict';

angular.module('stepApp').controller('ErpAuthFlowProcessDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ErpAuthFlowProcess', 'ErpAuthorityFlow', 'HrEmployeeInfo',
        function($scope, $stateParams, $modalInstance, entity, ErpAuthFlowProcess, ErpAuthorityFlow, HrEmployeeInfo) {

        $scope.erpAuthFlowProcess = entity;
        $scope.erpauthorityflows = ErpAuthorityFlow.query();
        $scope.hremployeeinfos = HrEmployeeInfo.query();
        $scope.load = function(id) {
            ErpAuthFlowProcess.get({id : id}, function(result) {
                $scope.erpAuthFlowProcess = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:erpAuthFlowProcessUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.erpAuthFlowProcess.id != null) {
                ErpAuthFlowProcess.update($scope.erpAuthFlowProcess, onSaveFinished);
            } else {
                ErpAuthFlowProcess.save($scope.erpAuthFlowProcess, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
*/
