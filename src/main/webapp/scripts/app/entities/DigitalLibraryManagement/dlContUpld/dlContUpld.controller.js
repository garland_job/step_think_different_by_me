'use strict';

angular.module('stepApp')
    .controller('DlContUpldController',
    ['$scope', '$state', '$modal', 'DlContUpld', 'FindAllByAdminUpload', 'FindAllByUserUpload', 'DlContCatSet', 'DlContSCatSet', 'DlContTypeSet', 'DlContUpldSearch', 'ParseLinks', 'getallbyid', 'getallbysid', 'getallbytype', 'FindActiveTypes', 'FindActivcategory', 'FindActiveSubcategory','FindAllByUserUploadByCurrentInstitiute','FindStudentsDlContUpldsByTeacherId',
        function ($scope, $state, $modal, DlContUpld, FindAllByAdminUpload, FindAllByUserUpload, DlContCatSet, DlContSCatSet, DlContTypeSet, DlContUpldSearch, ParseLinks, getallbyid, getallbysid, getallbytype, FindActiveTypes, FindActivcategory, FindActiveSubcategory,FindAllByUserUploadByCurrentInstitiute,FindStudentsDlContUpldsByTeacherId) {


            //$scope.contUplds = DlContUpld.query();

            $scope.contUpldsBYadmin = FindAllByAdminUpload.query();
            $scope.contUpldsBYuser = FindAllByUserUpload.query();
            $scope.dlcontcatsets = DlContCatSet.query();
            $scope.dlcontscatsets = DlContSCatSet.query();
            $scope.dlconttypesets = DlContTypeSet.query();
            $scope.dlContTypeSets = FindActiveTypes.query();
            $scope.dlContCatSets = FindActivcategory.query();
            $scope.dlContCatSetsFilter = $scope.dlContCatSets;

            $scope.dlContSubCatSets = FindActiveSubcategory.query();
            $scope.dlContSubCatSetsFilter = $scope.dlContSubCatSets;


            $scope.dlContUplds = [];
            $scope.page = 0;

            $scope.currentPage = 1;
            $scope.pageSize = 10;

            /*  $scope.loadAll = function() {
             DlContUpld.query({page: $scope.page, size: 20}, function(result, headers) {
             $scope.links = ParseLinks.parse(headers('link'));
             $scope.dlContUplds = result;
             console.log("===============More Jah ================");
             console.log($scope.dlContUplds);
             });
             }; */
            //$scope.loadAll = function () {
            FindAllByAdminUpload.query({page: $scope.page, size: 20000}, function (result) {
                $scope.dlContUplds = result;
                console.log("===============Total Contents by admin ================");
                console.log($scope.dlContUplds);
            });
            //};

           FindAllByUserUpload.query({page: $scope.page, size: 2000}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlContUpldsbyUser = result;
                console.log("uploaded contents by user");
                console.log($scope.dlContUpldsbyUser);


            });

            FindAllByUserUploadByCurrentInstitiute.query({page: $scope.page, size: 200}, function (result) {
                $scope.dlContUpldsbyCurrentInstitiute = result;
                console.log("uploaded contents by CurrentInstitiute");
                console.log($scope.dlContUpldsbyCurrentInstitiute);


            });
            FindStudentsDlContUpldsByTeacherId.query({page: $scope.page, size: 200}, function (result) {
                $scope.dlContUpldsByCurrentTeacher = result;
                console.log("uploaded contents by dlContUpldsByCurrentTeacher");
                console.log($scope.dlContUpldsByCurrentTeacher);


            });


            //$scope.loadPage = function (page) {
            //    $scope.page = page;
            //    $scope.loadAll();
            //};
            ////$scope.loadAll();

            $scope.search = function () {
                DlContUpldSearch.query({query: $scope.searchQuery}, function (result) {
                    $scope.dlContUplds = result;
                }, function (response) {
                    if (response.status === 404) {
                        $scope.loadAll();
                    }
                });
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };


            $scope.clear = function () {
                $scope.dlContUpld = {
                    code: null,
                    authName: null,
                    edition: null,
                    isbnNo: null,
                    copyright: null,
                    publisher: null,
                    createdDate: null,
                    updatedDate: null,
                    createdBy: null,
                    updatedBy: null,
                    status: null,
                    id: null
                };
            };



            //this code is for type-category wise book show
            $scope.loadCategoryByType = function (typeObj) {
                console.log(typeObj);
                $scope.typeId = typeObj.id;

                //$scope.getBookByType = function () {

                $scope.getallbytype = getallbytype.get({id: $scope.typeId}, function (result) {
                        $scope.dlContUplds = result;
                        console.log($scope.dlContUplds);
                    }
                );
                //};
                $scope.dlContCatSetsFilter = [];
                angular.forEach($scope.dlContCatSets, function (categoryObj) {
                    //console.log(categoryObj.dlContTypeSet.id);
                    if (typeObj.id == categoryObj.dlContTypeSet.id) {
                        $scope.dlContCatSetsFilter.push(categoryObj);
                        //$scope.getBookByType();
                    }
                });
            };
            $scope.loadSubCatByCategory = function (categoryObj) {
                $scope.categoryID = categoryObj.id;
                console.log($scope.categoryID);

                //this code is for category wise book show
                    $scope.getallbyid = getallbyid.get({id: $scope.categoryID}, function (result) {
                            $scope.dlContUplds = result;
                        }
                    );
                ////Cascading part
                $scope.dlContSubCatSetsFilter = [];
                angular.forEach($scope.dlContSubCatSets, function (subCategoryObj) {
                    if (categoryObj.id == subCategoryObj.dlContCatSet.id) {
                        $scope.dlContSubCatSetsFilter.push(subCategoryObj);
                    }
                });
            };


            //this code is for sub-category wise book show
            $scope.getBookBySCategory = function (getSubCatObj) {
                console.log(getSubCatObj);
                $scope.getSubCatObj=getSubCatObj.id;
                $scope.getallbysid = getallbysid.get({id: $scope.getSubCatObj}, function (result) {
                        $scope.dlContUplds = result;
                        console.log($scope.dlContUplds);
                    }
                );
            };



        }]);
/*dlContUpld-dialog.controller.js*/

angular.module('stepApp').controller('DlContUpldDialogController',

    ['$scope', '$rootScope', '$state', '$q', '$stateParams', 'entity', 'ParseLinks', 'DlContUpld', 'DlContTypeSet', 'DlContCatSet', 'DlContSCatSet', 'InstEmployee', 'DlFileType', 'ContUpldByCode', 'Principal', 'findOneByIsbn', 'FindActiveTypes', 'FindActivcategory', 'FindActiveSubcategory', 'DlSourceSetUp','FindActiveFileType','FindActiveSourceSetup',
        function ($scope, $rootScope, $state, $q, $stateParams, entity, ParseLinks, DlContUpld, DlContTypeSet, DlContCatSet, DlContSCatSet, InstEmployee, DlFileType, ContUpldByCode, Principal, findOneByIsbn, FindActiveTypes, FindActivcategory, FindActiveSubcategory, DlSourceSetUp,FindActiveFileType,FindActiveSourceSetup) {

            $scope.dlContUpld = entity;

            /*
             $scope.dlContTypeSets = DlContTypeSet.query();
             */
            $scope.dlContTypeSets = FindActiveTypes.query();

            $scope.dlfiletypes = FindActiveFileType.query();
            $scope.dlSourceSetUps = FindActiveSourceSetup.query();


            ContUpldByCode.get({code: $scope.dlContUpld.code}, function (dlContUpld) {
                $scope.message = "The Code is already existed.";
            });
            var roles = $rootScope.userRoles;
            /* var a = roles.indexOf("ROLE_INSTEMP");
             //var b = roles.search("ROLE_ADMIN");
             console.log(a);
             //console.log(b);*/
            /*if(roles.indexOf("ROLE_INSTEMP") != -1){
             console.log("Employee Role");
             }
             if(roles.indexOf("ROLE_ADMIN") != -1){
             console.log("Admin Role");
             }*/
            findOneByIsbn.get({name: $scope.dlContUpld.isbnNo}, function (dlContUpld) {

                $scope.message = "The  File Type is already exist.";

            });


            $scope.load = function (id) {
                DlContUpld.get({id: id}, function (result) {
                    $scope.dlContUpld = result;
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlContUpldUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlContUpld', {}, {reload: true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };


            $q.all([entity.$promise]).then(function () {
                if (entity.dlContSCatSet) {
                    $scope.dlContSCatSet = entity.dlContSCatSet.name;
                    if (entity.dlContSCatSet.dlContCatSet) {
                        $scope.dlContCatSet = entity.dlContSCatSet.dlContCatSet.name;
                        if (entity.dlContSCatSet.dlContCatSet.dlContTypeSet) {
                            $scope.dlContTypeSet = entity.dlContSCatSet.dlContCatSet.dlContTypeSet.name;
                        }
                        else {
                            $scope.dlContTypeSet = "Select Content Type"
                        }
                    }
                    else {
                        $scope.dlContCatSet = "Select Category";
                        $scope.dlContTypeSet = "Select Content Type"
                    }
                }
                else {
                    $scope.dlContTypeSet = "Select Content Type"
                    $scope.dlContCatSet = "Select Category";
                    $scope.dlContSCatSet = "Select Sub Category";
                }
            });

            $scope.dlContTypeSets = FindActiveTypes.query();
            var allDlContCatSet = FindActivcategory.query({page: $scope.page, size: 65}, function (result, headers) {
                return result;
            });
            var allDlContSCatSet = FindActiveSubcategory.query({
                page: $scope.page,
                size: 500
            }, function (result, headers) {
                return result;
            });

            $scope.updatedDlContSCatSet = function (select) {
                /* console.log("selected district .............");
                 console.log(select);*/
                $scope.dlContSCatSets = [];
                angular.forEach(allDlContSCatSet, function (dlContSCatSet) {
                    if (select.id == dlContSCatSet.dlContCatSet.id) {
                        $scope.dlContSCatSets.push(dlContSCatSet);
                    }
                });

            };

            $scope.dlContCatSets = FindActivcategory.query();
            $scope.dlContSCatSets = FindActiveSubcategory.query();

            $scope.updatedDlContCatSet = function (select) {
                $scope.dlContCatSets = [];
                angular.forEach(allDlContCatSet, function (dlContCatSet) {

                    if ((dlContCatSet.dlContTypeSet && select) && (select.id != dlContCatSet.dlContTypeSet.id)) {
                        console.log("There is error");
                    } else {
                        console.log("There is the fire place");
                        $scope.dlContCatSets.push(dlContCatSet);
                    }
                });
            };


            $scope.save = function () {

                if (roles.indexOf("ROLE_ADMIN") != -1) {
                    console.log("==============is authenticated============");
                    console.log(Principal.isAuthenticated());
                    console.log(Principal.isAuthenticated());
                    console.log("==============is Role============");
                    console.log(Principal.hasAuthority());

                    $scope.isSaving = true;
                    if ($scope.dlContUpld.id != null) {
                        DlContUpld.update($scope.dlContUpld, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.dlContUpld.updated');
                    } else {
                        $scope.dlContUpld.status = 4;
                        DlContUpld.save($scope.dlContUpld, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.dlContUpld.created');
                    }
                }
                else if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    console.log("==============Else is authenticated============");
                    $scope.isSaving = true;
                    if ($scope.dlContUpld.id != null) {
                        DlContUpld.update($scope.dlContUpld, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.dlContUpld.updated');
                    }
                    else {
                        $scope.dlContUpld.status = 2;
                        DlContUpld.save($scope.dlContUpld, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.dlContUpld.created');
                    }
                }
                else if(Principal.hasAnyAuthority(['ROLE_GOVT_STUDENT'])){
                    console.log("==============Else is authenticated============");
                    $scope.isSaving = true;
                    if ($scope.dlContUpld.id != null) {
                        DlContUpld.update($scope.dlContUpld, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.dlContUpld.updated');
                    }
                    else {
                        $scope.dlContUpld.status = 1;
                        console.log("student entry");
                        console.log($scope.dlContUpld.status);
                        DlContUpld.save($scope.dlContUpld, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.dlContUpld.created');
                    }
                }


            };
            $scope.allowedTypes = '';
            $scope.filetype = function (dlFileType) {

                $scope.appId = dlFileType;
                console.log($scope.appId);

                if (dlFileType.fileType == '.jpg') {
                    $scope.allowedTypes = '.jpg'
                }
                else if (dlFileType.fileType == '.pdf') {
                    $scope.allowedTypes = '.pdf'
                }
                else if (dlFileType.fileType == '.doc') {
                    $scope.allowedTypes = '.doc'
                }
                else if (dlFileType.fileType == '.mp4') {
                    $scope.allowedTypes = '.mp4'
                }
                else if (dlFileType.fileType == '.mp3') {
                    $scope.allowedTypes = '.mp3'
                }
                else if (dlFileType.fileType == '.docx') {
                    $scope.allowedTypes = '.docx'
                }
                else if (dlFileType.fileType == '.png') {
                    $scope.allowedTypes = '.png'
                }
                else if (dlFileType.fileType == '.gif') {
                    $scope.allowedTypes = '.gif'
                }
                else if (dlFileType.fileType == '.csv') {
                    $scope.allowedTypes = '.csv'
                }
                else if (dlFileType.fileType == '.ppt') {
                    $scope.allowedTypes = '.ppt'
                }


            }

            $scope.setContent = function ($file, dlContUpld) {
                console.log($scope.appId);
                if (!$file) {
                }
                if (($file.size / 1024) / 1024 > $scope.appId.limitMb) {
                    alert(
                        'Your file size is greater than' + $scope.appId.limitMb + "mb Please, select image less than" + $scope.appId.limitMb + " mb.\n\n"
                        + '▬▬▬▬▬▬▬▬▬ஜ۩۞۩ஜ▬▬▬▬▬▬▬▬▬\n\n'
                    );
                    //$state.go('libraryInfo.dlContUpld.new.alert',{},{reload:false});
                    dlContUpld.contentContentType = null;
                    dlContUpld.contentName = null;
                }
                else {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            dlContUpld.content = base64Data;
                            dlContUpld.contentContentType = $file.type;
                            dlContUpld.contentName = $file.name;
                        });
                    };
                }
            };

            $scope.setContentImage = function ($file, dlContUpld) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            dlContUpld.contentImage = base64Data;
                            dlContUpld.contentImageContentType = $file.type;
                            dlContUpld.contentImageName = $file.name;

                        });
                    };
                }

            };
        }]);
/*dlContUpld-detail.controller.js*/

angular.module('stepApp')
    .controller('DlContUpldDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'DlContUpld', 'DlContTypeSet', 'DlContCatSet', 'InstEmployee', 'DlFileType','findAllByAllType','getallbyid',
            function ($scope, $rootScope, $stateParams, entity, DlContUpld, DlContTypeSet, DlContCatSet, InstEmployee, DlFileType,findAllByAllType,getallbyid) {
                //$scope.dlContUpld = entity;

                console.log($stateParams.id);



                console.log($scope.dlContCatSet)

                DlContUpld.get({id: $stateParams.id}, function(result) {
                    $scope.dlContUpld = result;
                    $scope.catSetId = $scope.dlContUpld.dlContCatSet;
                    $scope.ScatSetId = $scope.dlContUpld.dlContSCatSet;
                    $scope.cTypeId = $scope.dlContUpld.dlContTypeSet;

                    ////This function is for Download counting
                    console.log($scope.dlContUpld.counter);
                    $scope.dlContUpld.counter=  $scope.dlContUpld.counter+0;
                    $scope.increment = function() {
                        $scope.dlContUpld.counter+=1;
                        DlContUpld.update($scope.dlContUpld);
                    };
                    /////End of download counting

                    if($scope.catSetId != null && $scope.ScatSetId != null && $scope.cTypeId != null){
                        $scope.id1 = $scope.catSetId.id;
                        $scope.id2 = $scope.ScatSetId.id;
                        $scope.id3 = $scope.cTypeId.id;
                        findAllByAllType.query({dlContCatSet: $scope.id1, dlContSCatSet: $scope.id2 , dlContTypeSet: $scope.id3}, function(result) {
                            $scope.dlAllByAllTypes = result;
                            console.log("===== **** Data  * Found **** ======");
                            console.log($scope.dlAllByAllTypes);
                        });
                    }else{
                        $scope.dlAllByAllTypes = null;
                        $scope.noContent = 'No Similar Content Found';

                    }
                });

                var unsubscribe = $rootScope.$on('stepApp:dlContUpldUpdate', function(event, result) {
                    $scope.dlContUpld = result;
                });
                $scope.$on('$destroy', unsubscribe);

                $scope.previewImage = function (content, contentContentType,contentName)
                {
                    var blob = $rootScope.b64toBlob(content,contentContentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = $scope.dlContUpld.contentContentType;
                    $rootScope.viewerObject.pageTitle = $scope.dlContUpld.contentName;

                    console.log($rootScope.viewerObject.content);
                    console.log($rootScope.viewerObject.contentType);
                    console.log($rootScope.viewerObject.pageTitle);

                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

                /////counter



            }]);
/*dlContUpld-delete-dialog.controller.js*/


angular.module('stepApp')
    .controller('DlContUpldDeleteController',
        ['$scope','$state','$stateParams', '$rootScope', '$modalInstance', 'entity', 'DlContUpld',
            function($scope,$state,$stateParams, $rootScope, $modalInstance, entity, DlContUpld) {

                $scope.dlContUpld = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    $state.go('libraryInfo.dlContUpld', null, { reload: true });
                    DlContUpld.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.dlContUpld.deleted');

                        });

                };


            }]);
/*dlContUpld-approve-dialog.controller.js*/

angular.module('stepApp')
    .controller('DlContUpldApproveController',
        ['$scope','$state', '$rootScope', '$modalInstance', 'entity', 'DlContUpld',
            function($scope,$state, $rootScope, $modalInstance, entity, DlContUpld) {

                $scope.dlContUpld = entity;
                /*
                 DlContUpld.get({id: $stateParams.id}, function (result) {
                 */

                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmApprove = function () {
                    $scope.dlContUpld.status = 4;
                    DlContUpld.update($scope.dlContUpld, onSaveSuccess);

                };
                $scope.confirmApproveByEmployee = function () {
                    $scope.dlContUpld.status = 2;
                    DlContUpld.update($scope.dlContUpld, onSaveSuccess);

                };
                $scope.confirmApproveByAdmin = function () {
                    $scope.dlContUpld.status = 4;
                    DlContUpld.update($scope.dlContUpld, onSaveSuccess);

                };


                var onSaveSuccess = function (result) {
                    $modalInstance.close(true);
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $state.go('libraryInfo.dlContUpldByUser', null, { reload: true });

                };

            }]);
