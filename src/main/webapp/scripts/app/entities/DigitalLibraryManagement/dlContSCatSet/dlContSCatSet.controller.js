'use strict';

angular.module('stepApp')
    .controller('DlContSCatSetController',
    ['$scope', '$state', '$modal', 'DlContSCatSet', 'DlContSCatSetSearch', 'ParseLinks',
     function ($scope, $state, $modal, DlContSCatSet, DlContSCatSetSearch, ParseLinks) {

        $scope.dlContSCatSets = [];
        $scope.page = 0;
        $scope.currentPage = 1;
                 $scope.pageSize = 10;
        $scope.loadAll = function() {
            DlContSCatSet.query({page: $scope.page, size: 1000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlContSCatSets = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DlContSCatSetSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlContSCatSets = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlContSCatSet = {
                code: null,
                name: null,
                description: null,
                pStatus: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };
    }]);
/*dlContSCatSet-dialog.controller.js*/

angular.module('stepApp').controller('DlContSCatSetDialogController',
    ['$scope', '$rootScope','$state', '$stateParams', 'entity', 'DlContSCatSet', 'DlContTypeSet', 'DlContCatSet', 'DlContSCatSetByName', 'DlContSCatSetByCode','FindActivcategory','FindActiveTypes',
        function($scope, $rootScope, $state, $stateParams, entity, DlContSCatSet, DlContTypeSet, DlContCatSet,DlContSCatSetByName,DlContSCatSetByCode,FindActivcategory,FindActiveTypes) {

            //$scope.dlContSCatSet = entity;
            //$scope.dlContSCatSet.pStatus = true;
            $scope.dlconttypesets = FindActiveTypes.query({size:20000});
            $scope.dlcontcatsets = FindActivcategory.query({size:20000});

            // $scope.load = function(id) {
                DlContSCatSet.get({id : $stateParams.id}, function(result) {
                    $scope.dlContSCatSet = result;
                });
            // };

            if($stateParams.id != null){
                DlContSCatSet.get({id : $stateParams.id}, function(result) {
                    $scope.dlContSCatSet = result;
                    /*
                     $scope.dlContSCatSet.dlContCatSet=result.dlContCatSet;
                     */

                });
            }
            /* DlContSCatSetByName.get({name: $scope.dlContSCatSet.name}, function (dlContSCatSetBy) {

             $scope.message = "The  File Type is already exist.";

             });
             DlContSCatSetByCode.get({code: $scope.dlContSCatSet.code}, function (dlContSCatSetBy) {

             $scope.message = "The  File Type is already exist.";

             });*/


            var allDlContCatSet= FindActivcategory.query({page: $scope.page, size: 500}, function(result, headers) { return result;});


            $scope.updatedDlContCatSet=function(select){
                console.log(select.id);
                console.log("come to s cat set");

                $scope.dlcontcatsets=[];
                angular.forEach(allDlContCatSet, function(dlContCatSet) {
                    console.log(dlContCatSet.dlContTypeSet);
                    if(select.id==dlContCatSet.dlContTypeSet.id){
                        $scope.dlcontcatsets.push(dlContCatSet);
                    }
                });
            };










            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlContSCatSetUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlContSCatSet',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.dlContSCatSet.id != null) {
                    if($scope.dlContSCatSet.pStatus ==null){

                        $scope.dlContSCatSet.pStatus = true;
                    }
                    DlContSCatSet.update($scope.dlContSCatSet, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.dlContSCatSet.updated');

                } else {
                    if($scope.dlContSCatSet.pStatus ==null){

                        $scope.dlContSCatSet.pStatus = true;
                    }
                    DlContSCatSet.save($scope.dlContSCatSet, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlContSCatSet.created');
                }
            };
        }]);
/*dlContSCatSet-detail.controller.js*/

angular.module('stepApp')
    .controller('DlContSCatSetDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'DlContSCatSet', 'DlContTypeSet', 'DlContCatSet',
            function ($scope, $rootScope, $stateParams, entity, DlContSCatSet, DlContTypeSet, DlContCatSet) {
                $scope.dlContSCatSet = entity;
                $scope.load = function (id) {
                    DlContSCatSet.get({id: id}, function(result) {
                        $scope.dlContSCatSet = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:dlContSCatSetUpdate', function(event, result) {
                    $scope.dlContSCatSet = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*dlContSCatSet-delete-dialog.controller.js*/


angular.module('stepApp')
    .controller('DlContSCatSetDeleteController',
        ['$scope','$state', '$rootScope','$modalInstance', 'entity', 'DlContSCatSet',
            function($scope,$state, $rootScope, $modalInstance, entity, DlContSCatSet) {

                $scope.dlContSCatSet = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    $state.go('libraryInfo.dlContSCatSet', null, { reload: true });
                    DlContSCatSet.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.dlContSCatSet.deleted');

                        });

                };


            }]);
