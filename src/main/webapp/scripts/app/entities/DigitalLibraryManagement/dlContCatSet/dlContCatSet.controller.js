'use strict';

angular.module('stepApp')
    .controller('DlContCatSetController',
    ['$scope', '$state', '$modal', 'DlContCatSet', 'DlContCatSetSearch', 'ParseLinks',
    function ($scope, $state, $modal, DlContCatSet, DlContCatSetSearch, ParseLinks) {

        $scope.dlContCatSets = [];
        $scope.page = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.loadAll = function() {
            DlContCatSet.query({page: $scope.page, size: 1000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlContCatSets = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DlContCatSetSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlContCatSets = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlContCatSet = {
                code: null,
                name: null,
                description: null,
                pStatus: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };
    }]);
/*dlContCatSet-dialog.controller.js*/

angular.module('stepApp').controller('DlContCatSetDialogController',
    ['$scope', '$rootScope','$state', '$stateParams', 'entity', 'DlContCatSet', 'DlContTypeSet','DlContCatSetByName', 'DlContCatSetByCode','FindActiveTypes',
        function($scope, $rootScope, $state, $stateParams, entity, DlContCatSet, DlContTypeSet,DlContCatSetByName,DlContCatSetByCode,FindActiveTypes) {

            //$scope.dlContCatSet = entity;
            //$scope.dlContCatSet.pStatus = true;

            //DlContCatSet.get({id : $stateParams.id});
            // $scope.load = function(id) {
                DlContCatSet.get({id : $stateParams.id}, function(result) {
                    $scope.dlContCatSet = result;
                });
            $scope.dlconttypesets = FindActiveTypes.query({size:1000});

            // };
            if($stateParams.id != null){
                DlContCatSet.get({id : $stateParams.id}, function(result) {
                    $scope.dlContCatSet = result;
                });
            }

            //DlContCatSetByName.get({name: $scope.dlContCatSet.name}, function (dlContCatSet) {
            //
            //                  $scope.message = "The  File Type is already exist.";
            //
            //              });
            //DlContCatSetByCode.get({code: $scope.dlContCatSet.code}, function (dlContCatSet) {
            //
            //    $scope.message = "The  File Type is already exist.";
            //
            //});
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlContCatSetUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlContCatSet',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.dlContCatSet.id != null) {
                    if($scope.dlContCatSet.pStatus ==null){

                        $scope.dlContCatSet.pStatus = true;
                    }
                    DlContCatSet.update($scope.dlContCatSet, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.dlContCatSet.updated');
                } else {
                    if($scope.dlContCatSet.pStatus ==null){
                        $scope.dlContCatSet.pStatus = true;
                    }
                    DlContCatSet.save($scope.dlContCatSet, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlContCatSet.created');

                }
            };
        }]);
/*dlContCatSet-detail.controller.js*/

angular.module('stepApp')
    .controller('DlContCatSetDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'DlContCatSet', 'DlContTypeSet',
            function ($scope, $rootScope, $stateParams, entity, DlContCatSet, DlContTypeSet) {
                $scope.dlContCatSet = entity;
                $scope.load = function (id) {
                    DlContCatSet.get({id: id}, function(result) {
                        $scope.dlContCatSet = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:dlContCatSetUpdate', function(event, result) {
                    $scope.dlContCatSet = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*dlContCatSet-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('DlContCatSetDeleteController',
        ['$scope','$state', '$rootScope','$modalInstance', 'entity', 'DlContCatSet',
            function($scope,$state, $rootScope, $modalInstance, entity, DlContCatSet) {

                $scope.dlContCatSet = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    $state.go('libraryInfo.dlContCatSet',{},{reload:true});
                    DlContCatSet.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.dlContCatSet.deleted');

                        });


                };

            }]);
