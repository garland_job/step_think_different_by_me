'use strict';

angular.module('stepApp')
    .controller('DlFileTypeController',
    ['$scope', '$state', '$modal', 'DlFileType', 'DlFileTypeSearch', 'ParseLinks',
    function ($scope, $state, $modal, DlFileType, DlFileTypeSearch, ParseLinks) {

        $scope.dlFileTypes = [];
        $scope.page = 0;
        $scope.currentPage = 1;
         $scope.pageSize = 10;
        $scope.loadAll = function() {
            DlFileType.query({page: $scope.page, size: 1000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlFileTypes = result;
                console.log($scope.dlFileTypes);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DlFileTypeSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlFileTypes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlFileType = {
                fileType: null,
                limitMb: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };
    }]);
/*dlFileType-dialog.controller.js*/

angular.module('stepApp').controller('DlFileTypeDialogController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'DlFileType','DlFileTypeByfileType',
        function($scope, $rootScope, $state, $stateParams, entity, DlFileType,DlFileTypeByfileType) {

            $scope.dlFileType = entity;
            $scope.dlFileType.pStatus = true;
            $scope.load = function(id) {
                DlFileType.get({id : id}, function(result) {
                    $scope.dlFileType = result;
                });
            };
            DlFileTypeByfileType.get({fileType: $scope.dlFileType.fileType}, function (dlFileType) {

                $scope.message = "The  File Type is already exist.";

            });


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlFileTypeUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlFileType',{},{reload:true});

            };


            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.setFileImg = function ($file, dlFileType) {
                if ($file && $file.$error == 'pattern') {
                    return;
                }
                if ($file) {
                    if($file.size/1024 > 200){
                        alert("File size should be maximum 200KB!");
                    }else{
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function() {
                                dlFileType.fileImg = base64Data;
                                dlFileType.fileImgContentType = $file.type;
                                dlFileType.fileImgName = $file.name;
                            });
                        };
                    }
                }
            };


            /*$scope.setFileImg = function ($file, dlFileType) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            dlFileType.fileImg = base64Data;
                            dlFileType.fileImgContentType = $file.type;
                            dlFileType.fileImgName = $file.name;

                        });
                    };
                }

            };*/
            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.dlFileType.id != null) {
                    DlFileType.update($scope.dlFileType, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.dlFileType.updated');
                } else {
                    DlFileType.save($scope.dlFileType, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlFileType.created');
                }
            };
        }]);
/*dlFileType-detail.controller.js*/

angular.module('stepApp')
    .controller('DlFileTypeDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'DlFileType',
            function ($scope, $rootScope, $stateParams, entity, DlFileType) {
                $scope.dlFileType = entity;
                $scope.load = function (id) {
                    DlFileType.get({id: id}, function(result) {
                        $scope.dlFileType = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:dlFileTypeUpdate', function(event, result) {
                    $scope.dlFileType = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*dlFileType-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('DlFileTypeDeleteController',
        ['$scope','$state','$rootScope', '$modalInstance', 'entity', 'DlFileType',
            function($scope,$state,$rootScope, $modalInstance, entity, DlFileType) {

                $scope.dlFileType = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    $state.go('libraryInfo.dlFileType', null, { reload: true });
                    DlFileType.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.dlFileType.deleted');


                        });


                };

            }]);
