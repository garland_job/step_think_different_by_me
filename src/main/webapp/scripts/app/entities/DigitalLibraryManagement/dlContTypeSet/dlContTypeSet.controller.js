'use strict';

angular.module('stepApp')
    .controller('DlContTypeSetController',
    ['$scope', '$state', '$modal', 'DlContTypeSet', 'DlContTypeSetSearch', 'ParseLinks',
    function ($scope, $state, $modal, DlContTypeSet, DlContTypeSetSearch, ParseLinks) {

        $scope.dlContTypeSets = [];
        $scope.page = 0;
        $scope.currentPage = 1;
                 $scope.pageSize = 10;
        $scope.loadAll = function() {
            DlContTypeSet.query({page: $scope.page, size: 1000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlContTypeSets = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DlContTypeSetSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlContTypeSets = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlContTypeSet = {
                code: null,
                name: null,
                description: null,
                pStatus: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };
    }]);
/*dlContTypeSet-dialog.controller.js*/

angular.module('stepApp').controller('DlContTypeSetDialogController',
    ['$scope','$rootScope','$state', '$stateParams', 'entity', 'DlContTypeSet','DlContTypeSetByName','DlContTypeSetByCode',
        function($scope, $rootScope, $state, $stateParams, entity, DlContTypeSet, DlContTypeSetByName, DlContTypeSetByCode) {

            $scope.dlContTypeSet = entity;
            $scope.dlContTypeSet.pStatus = true;
            $scope.load = function(id) {
                DlContTypeSet.get({id : id}, function(result) {
                    $scope.dlContTypeSet = result;
                });
            };

            DlContTypeSetByName.get({name: $scope.dlContTypeSet.name}, function (dlContTypeSet) {

                $scope.message = "The  File Type is already exist.";

            });
            DlContTypeSetByCode.get({code: $scope.dlContTypeSet.code}, function (dlContTypeSet) {

                $scope.message = "The  File Type is already exist.";

            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlContTypeSetUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlContTypeSet',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.dlContTypeSet.id != null) {
                    DlContTypeSet.update($scope.dlContTypeSet, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.dlContTypeSet.updated');
                } else {
                    DlContTypeSet.save($scope.dlContTypeSet, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlContTypeSet.created');
                }
            };
        }]);
/*dlContTypeSet-detail.controller.js*/

angular.module('stepApp')
    .controller('DlContTypeSetDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'DlContTypeSet',
            function ($scope, $rootScope, $stateParams, entity, DlContTypeSet) {
                $scope.dlContTypeSet = entity;
                $scope.load = function (id) {
                    DlContTypeSet.get({id: id}, function(result) {
                        $scope.dlContTypeSet = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:dlContTypeSetUpdate', function(event, result) {
                    $scope.dlContTypeSet = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
/*dlContTypeSet-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('DlContTypeSetDeleteController',
        ['$scope','$state', '$rootScope','$modalInstance', 'entity', 'DlContTypeSet',
            function($scope,$state, $rootScope, $modalInstance, entity, DlContTypeSet) {

                $scope.dlContTypeSet = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (id) {
                    $state.go('libraryInfo.dlContTypeSet', null, { reload: true });
                    DlContTypeSet.delete({id: id},
                        function () {
                            $modalInstance.close(true);
                            $rootScope.setErrorMessage('stepApp.dlContTypeSet.deleted');

                        });

                };

            }]);
