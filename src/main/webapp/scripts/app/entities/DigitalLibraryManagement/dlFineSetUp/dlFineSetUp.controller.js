'use strict';

angular.module('stepApp')
    .controller('DlFineSetUpController', function ($scope, $state, $modal, DlFineSetUp, DlFineSetUpSearch, ParseLinks) {

        $scope.dlFineSetUps = [];
        $scope.page = 0;
        $scope.currentPage = 1;
         $scope.pageSize = 10;
        $scope.loadAll = function() {
            DlFineSetUp.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlFineSetUps = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DlFineSetUpSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlFineSetUps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlFineSetUp = {
                timeLimit: null,
                fine: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*dlFineSetUp-dialog.controller.js*/

angular.module('stepApp').controller('DlFineSetUpDialogController',
    ['$scope','$state', '$rootScope', '$stateParams', 'entity', 'DlFineSetUp', 'DlContSCatSet','DlContTypeSet','DlContCatSet','FindActivcategory','FindActiveSubcategory','DlFineInfoBySCatId','FindActiveTypes',
        function($scope,$state, $rootScope, $stateParams, entity, DlFineSetUp, DlContSCatSet,DlContTypeSet,DlContCatSet,FindActivcategory,FindActiveSubcategory,DlFineInfoBySCatId,FindActiveTypes) {

            //$scope.dlFineSetUp = entity;
            $scope.dlFineSetUp = {};
            $scope.dlcontscatsets = DlContSCatSet.query({page: $scope.page, size: 20000});
            $scope.dlFineSetUp.status=true;
            $scope.buttondisabled=false;


            if($stateParams.id !=null){
                DlFineSetUp.get({id : $stateParams.id,size: 2000}, function(result) {
                    $scope.dlFineSetUp = result;
                    /*   CmsTradesByCurriculum.query({id:result.cmsTrade.cmsCurriculum.id}, function(data){
                     $scope.cmstrades = data;

                     });*/
                });
            }


            /*   $scope.load = function(id) {
             DlFineSetUp.get({id : id}, function(result) {
             $scope.dlFineSetUp = result;
             });
             };*/
            $scope.subcatValidationbyScatId =function(getSCatId) {
                $scope.subcategoryId=getSCatId.id;
                console.log($scope.subcategoryId);

                DlFineInfoBySCatId.get({id:  $scope.subcategoryId}, function (result) {
                    $scope.fineResult=result;
                    console.log($scope.fineResult);
                    if($scope.fineResult){
                        $scope.errormsg="This Sub-category is Already exist";
                        $scope.buttondisabled=true;
                    }
                }, function (respons) {

                    if (respons.status === 404) {
                        console.log("This Sub-category is ok")

                        $scope.buttondisabled=false;
                        $scope.errormsg = 'Accepted';


                    }
                });
            };
            $scope.dlContTypeSets = FindActiveTypes.query({page: $scope.page, size: 200});
            var allDlContCatSet = FindActivcategory.query({page: $scope.page, size: 65}, function (result, headers) {
                return result;
            });
            var allDlContSCatSet = FindActiveSubcategory.query({page: $scope.page, size: 500}, function (result, headers) {
                return result;
            });

            $scope.updatedDlContSCatSet = function (select) {
                /* console.log("selected district .............");
                 console.log(select);*/
                $scope.dlContSCatSets = [];
                angular.forEach(allDlContSCatSet, function (dlContSCatSet) {
                    if (select.id == dlContSCatSet.dlContCatSet.id) {
                        $scope.dlContSCatSets.push(dlContSCatSet);
                    }
                });

            };

            $scope.dlContCatSets = DlContCatSet.query({page: $scope.page, size: 200});
            $scope.dlContSCatSets = DlContSCatSet.query({page: $scope.page, size: 200});

            $scope.updatedDlContCatSet = function (select) {
                $scope.dlContCatSets = [];
                angular.forEach(allDlContCatSet, function (dlContCatSet) {

                    if ((dlContCatSet.dlContTypeSet && select) && (select.id != dlContCatSet.dlContTypeSet.id)) {
                        console.log("There is error");
                    } else {
                        console.log("There is the fire place");
                        $scope.dlContCatSets.push(dlContCatSet);
                    }
                });
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlFineSetUpUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlFineSetUp',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.dlFineSetUp.id != null) {
                    DlFineSetUp.update($scope.dlFineSetUp, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlFineSetUp.updated');

                } else {
                    DlFineSetUp.save($scope.dlFineSetUp, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlFineSetUp.created');

                }
            };

            $scope.clear = function() {
            };
        }]);
/*dlFineSetUp-detail.controller.js*/

angular.module('stepApp')
    .controller('DlFineSetUpDetailController', function ($scope, $rootScope, $stateParams, entity, DlFineSetUp, DlContCatSet) {
        $scope.dlFineSetUp = entity;
        $scope.load = function (id) {
            DlFineSetUp.get({id: id}, function(result) {
                $scope.dlFineSetUp = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:dlFineSetUpUpdate', function(event, result) {
            $scope.dlFineSetUp = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*dlFineSetUp-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('DlFineSetUpDeleteController', function($scope, $rootScope, $modalInstance, entity, DlFineSetUp) {

        $scope.dlFineSetUp = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            DlFineSetUp.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.DlFineSetUp.deleted');
                });
        };

    });
