'use strict';

angular.module('stepApp')
    .controller('DlEmpRegController',
    ['$scope', 'DlEmpReg', 'DlEmpRegSearch', 'ParseLinks',
    function ($scope, DlEmpReg, DlEmpRegSearch, ParseLinks) {
        $scope.dlEmpRegs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            DlEmpReg.query({page: $scope.page, size: 1000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlEmpRegs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            DlEmpReg.get({id: id}, function(result) {
                $scope.dlEmpReg = result;
                $('#deleteDlEmpRegConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            DlEmpReg.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDlEmpRegConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            DlEmpRegSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlEmpRegs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlEmpReg = {
                userName: null,
                userPw: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                status: null,
                id: null
            };
        };
    }]);
/*dlEmpReg-dialog.controller.js*/

angular.module('stepApp').controller('DlEmpRegDialogController',
    ['$scope','$state', '$stateParams',  'entity', 'DlEmpReg', 'InstEmployee',
        function($scope,$state, $stateParams, entity, DlEmpReg, InstEmployee) {

            $scope.dlEmpReg = entity;
            $scope.instemployees = InstEmployee.query();
            $scope.load = function(id) {
                DlEmpReg.get({id : id}, function(result) {
                    $scope.dlEmpReg = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:dlEmpRegUpdate', result);
                $scope.isSaving = false;
                $state.go('libraryInfo.dlEmpReg',{},{reload:true});

            };

            $scope.save = function () {
                $scope.isSaving = true;

                if ($scope.dlEmpReg.id != null) {
                    DlEmpReg.update($scope.dlEmpReg, onSaveFinished);
                } else {
                    DlEmpReg.save($scope.dlEmpReg, onSaveFinished);
                    //$state.go('dlEmpReg');

                }
            };

        }]);
/*dlEmpReg-detail.controller.js*/

angular.module('stepApp')
    .controller('DlEmpRegDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'DlEmpReg', 'InstEmployee',
            function ($scope, $rootScope, $stateParams, entity, DlEmpReg, InstEmployee) {
                $scope.dlEmpReg = entity;
                $scope.load = function (id) {
                    DlEmpReg.get({id: id}, function(result) {
                        $scope.dlEmpReg = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:dlEmpRegUpdate', function(event, result) {
                    $scope.dlEmpReg = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);
