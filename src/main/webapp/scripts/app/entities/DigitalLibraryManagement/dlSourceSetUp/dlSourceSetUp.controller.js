'use strict';

angular.module('stepApp')
    .controller('DlSourceSetUpController', function ($scope, $state, $modal, DlSourceSetUp, DlSourceSetUpSearch, ParseLinks) {

        $scope.dlSourceSetUps = [];
        $scope.page = 0;
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.loadAll = function() {
            DlSourceSetUp.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dlSourceSetUps = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            DlSourceSetUpSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dlSourceSetUps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dlSourceSetUp = {
                name: null,
                fine: null,
                status: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*dlSourceSetUp-dialog.controller.js*/

angular.module('stepApp').controller('DlSourceSetUpDialogController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'entity', 'DlSourceSetUp',
        function($scope, $rootScope, $state, $stateParams, entity, DlSourceSetUp) {

            $scope.dlSourceSetUp = entity;
            $scope.load = function(id) {
                DlSourceSetUp.get({id : id}, function(result) {
                    $scope.dlSourceSetUp = result;
                });
            };
            $scope.dlSourceSetUp.status = true;

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:dlSourceSetUpUpdate', result);

                $scope.isSaving = false;
                $state.go('libraryInfo.dlSourceSetUp',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.dlSourceSetUp.id != null) {
                    DlSourceSetUp.update($scope.dlSourceSetUp, onSaveSuccess, onSaveError);
                    $rootScope.setWarningMessage('stepApp.dlSourceSetUp.updated');
                } else {
                    DlSourceSetUp.save($scope.dlSourceSetUp, onSaveSuccess, onSaveError);
                    $rootScope.setSuccessMessage('stepApp.dlSourceSetUp.created');
                }
            };

            $scope.clear = function() {

            };
        }]);
/*dlSourceSetUp-detail.controller.js*/

angular.module('stepApp')
    .controller('DlSourceSetUpDetailController', function ($scope, $rootScope, $stateParams, entity, DlSourceSetUp) {
        $scope.dlSourceSetUp = entity;
        $scope.load = function (id) {
            DlSourceSetUp.get({id: id}, function(result) {
                $scope.dlSourceSetUp = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:dlSourceSetUpUpdate', function(event, result) {
            $scope.dlSourceSetUp = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*dlSourceSetUp-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('DlSourceSetUpDeleteController', function($scope, $rootScope, $modalInstance, entity, DlSourceSetUp) {

        $scope.dlSourceSetUp = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            DlSourceSetUp.delete({id: id},
                function () {
                    $modalInstance.close(true);
                    $rootScope.setErrorMessage('stepApp.dlSourceSetUp.deleted');
                });
        };

    });
