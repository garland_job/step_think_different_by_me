'use strict';

angular.module('stepApp')
	.controller('FeePaymentTypeSetupDeleteController', function($scope, $modalInstance, entity, FeePaymentTypeSetup) {

        $scope.feePaymentTypeSetup = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            FeePaymentTypeSetup.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });
