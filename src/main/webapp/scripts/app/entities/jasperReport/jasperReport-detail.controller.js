'use strict';

angular.module('stepApp')
    .controller('JasperReportDetailController', function ($scope, $rootScope, $stateParams, entity, JasperReport) {
        $scope.jasperReport = entity;
        $scope.load = function (id) {
            JasperReport.get({id: id}, function(result) {
                $scope.jasperReport = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:jasperReportUpdate', function(event, result) {
            $scope.jasperReport = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
