'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('jasperReport', {
                parent: 'entity',
                url: '/jasperReports',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.jasperReport.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasperReport/jasperReports.html',
                        controller: 'JasperReportController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReport');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('jasperReport.detail', {
                parent: 'entity',
                url: '/jasperReport/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.jasperReport.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasperReport/jasperReport-detail.html',
                        controller: 'JasperReportDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReport');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JasperReport', function($stateParams, JasperReport) {
                        return JasperReport.get({id : $stateParams.id});
                    }]
                }
            })
            .state('jasperReport.new', {
                parent: 'jasperReport',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReport/jasperReport-dialog.html',
                        controller: 'JasperReportDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    path: null,
                                    module: null,
                                    role: null,
                                    status: null,
                                    createdDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReport', null, { reload: true });
                    }, function() {
                        $state.go('jasperReport');
                    })
                }]
            })
            .state('jasperReport.edit', {
                parent: 'jasperReport',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReport/jasperReport-dialog.html',
                        controller: 'JasperReportDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['JasperReport', function(JasperReport) {
                                return JasperReport.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReport', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
