'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('ReportEngine', {
                parent: 'entity',
                url: '/Report-Engine/{module}',
                data: {
                    authorities: [],
                    pageTitle: 'stepApp.jasperReport.detail.containerTitle'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/ReportModule/report-viewer.html',
                        controller: 'ReportContainer'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReport');
                        $translatePartialLoader.addPart('jasperReportParameter');
                        // $translatePartialLoader.addPart('jasperReportSecurity');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            });
            /*.state('ReportEngine.addReport', {
                parent: 'ReportEngine',
                url: '/add-Report',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                onEnter: ['$stateParams', '$state', '$modal', function ($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasper/jasperReport/jasperReport-dialog.html',
                        controller: 'JasperReportDialogController',
                        size: 'md',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    path: null,
                                    module: null,
                                    role: null,
                                    status: null,
                                    createdate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function (result) {
                        $state.go('ReportEngine', null, {reload: true});
                    }, function () {
                        $state.go('ReportEngine');
                    })
                }]
            })
            .state('ReportEngine.editReport', {
                parent: 'ReportEngine',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$modal', function ($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasper/jasperReport/jasperReport-dialog.html',
                        controller: 'JasperReportDialogController',
                        size: 'md',
                        resolve: {
                            entity: ['JasperReport', function (JasperReport) {
                                return JasperReport.get({id: $stateParams.id});
                            }]
                        }
                    }).result.then(function (result) {
                        $state.go('jasperReport', null, {reload: true});
                    }, function () {
                        $state.go('^');
                    })
                }]
            })
            .state('ReportEngine.addParam', {
                parent: 'ReportEngine',
                url: '/add-param',
                data: {
                    authorities: ['ROLE_ADMIN']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasper/jasperReportParameter/jasperReportParameter-form.html',
                        controller: 'JasperReportParameterFormController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportParameter');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', function ($stateParams) {

                    }]
                }
            })
            .state('ReportEngine.editParam', {
                parent: 'ReportEngine',
                url: '/param-edit/{id}',

                data: {
                    authorities: ['ROLE_ADMIN']
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasper/jasperReportParameter/jasperReportParameter-form.html',
                        controller: 'JasperReportParameterFormController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportParameter');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JasperReportParameter', function ($stateParams, JasperReportParameter) {
                        return JasperReportParameter.get({id: $stateParams.id});
                    }]
                }
            })
            .state('ReportEngine.param-detail', {
                parent: 'ReportEngine',
                url: '/param-detail/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'stepApp.jasperReportParameter.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasper/jasperReportParameter/jasperReportParameter-detail.html',
                        controller: 'JasperReportParameterDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportParameter');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JasperReportParameter', function ($stateParams, JasperReportParameter) {
                        return JasperReportParameter.get({id: $stateParams.id});
                    }]
                }
            })
            .state('ReportEngine.report-detail', {
                parent: 'ReportEngine',
                url: '/report-detail/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'stepApp.jasperReport.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasper/jasperReport/jasperReport-detail.html',
                        controller: 'JasperReportDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReport');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JasperReport', function ($stateParams, JasperReport) {
                        return JasperReport.get({id: $stateParams.id});
                    }]
                }
            })
            .state('ReportEngine.module', {
                parent: 'ReportEngine',
                url: '/Module/{module}',
                data: {
                    authorities: ['ROLE_INSTITUTE','ROLE_ADMIN'],
                    pageTitle: 'stepApp.cmsCurriculum.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/ReportModule/display_report.html',
                        controller: 'ReportModuleController'
                    }
                }
            });*/
    });
