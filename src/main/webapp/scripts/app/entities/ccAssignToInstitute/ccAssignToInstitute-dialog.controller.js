'use strict';

angular.module('stepApp').controller('CcAssignToInstituteDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'CcAssignToInstitute', 'InstGenInfo',
        function($scope, $stateParams, $modalInstance, $q, entity, CcAssignToInstitute, InstGenInfo) {

        $scope.ccAssignToInstitute = entity;
        $scope.instGenInfo = {};
        $scope.instgeninfos = InstGenInfo.query({filter: 'ccassigntoinstitute-is-null'});
        /*$q.all([$scope.ccAssignToInstitute.$promise, $scope.instgeninfos.$promise]).then(function() {
            if (!$scope.ccAssignToInstitute.instGenInfo.id) {
                return $q.reject();
            }
            return InstGenInfo.get({id : $scope.ccAssignToInstitute.instGenInfo.id}).$promise;
        }).then(function(instGenInfo) {
            $scope.instgeninfos.push(instGenInfo);
        });*/
        $scope.load = function(id) {
            CcAssignToInstitute.get({id : id}, function(result) {
                $scope.ccAssignToInstitute = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.instGenInfo = result.instGenInfo;
            $scope.instGenInfo.centerCodeId = result.id;
            console.log("ttttttttt"+$scope.instGenInfo.centerCodeId);
            InstGenInfo.update($scope.instGenInfo);

            $scope.$emit('stepApp:ccAssignToInstituteUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.ccAssignToInstitute.id != null) {
                CcAssignToInstitute.update($scope.ccAssignToInstitute, onSaveFinished);
            } else {

                /*$scope.instGenInfo = $scope.ccAssignToInstitute.instGenInfo;
                $scope.instGenInfo.ccAssignToInstitute = $scope.ccAssignToInstitute;
*/
                /*console.log($scope.instGenInfo);
                console.log($scope.ccAssignToInstitute);*/
                CcAssignToInstitute.save($scope.ccAssignToInstitute, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
