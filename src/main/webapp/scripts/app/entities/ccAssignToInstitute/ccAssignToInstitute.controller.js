'use strict';

angular.module('stepApp')
    .controller('CcAssignToInstituteController', function ($scope, CcAssignToInstitute, CcAssignToInstituteSearch, ParseLinks) {
        $scope.ccAssignToInstitutes = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            CcAssignToInstitute.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.ccAssignToInstitutes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            CcAssignToInstitute.get({id: id}, function(result) {
                $scope.ccAssignToInstitute = result;
                $('#deleteCcAssignToInstituteConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            CcAssignToInstitute.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCcAssignToInstituteConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CcAssignToInstituteSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.ccAssignToInstitutes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.ccAssignToInstitute = {
                centerCode: null,
                id: null
            };
        };
    });
