'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('ccAssignToInstitute', {
                parent: 'entity',
                url: '/ccAssignToInstitutes',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.ccAssignToInstitute.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/ccAssignToInstitute/ccAssignToInstitutes.html',
                        controller: 'CcAssignToInstituteController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('ccAssignToInstitute');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('ccAssignToInstitute.detail', {
                parent: 'entity',
                url: '/ccAssignToInstitute/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.ccAssignToInstitute.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/ccAssignToInstitute/ccAssignToInstitute-detail.html',
                        controller: 'CcAssignToInstituteDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('ccAssignToInstitute');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'CcAssignToInstitute', function($stateParams, CcAssignToInstitute) {
                        return CcAssignToInstitute.get({id : $stateParams.id});
                    }]
                }
            })
            .state('ccAssignToInstitute.new', {
                parent: 'ccAssignToInstitute',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/ccAssignToInstitute/ccAssignToInstitute-dialog.html',
                        controller: 'CcAssignToInstituteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    centerCode: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('ccAssignToInstitute', null, { reload: true });
                    }, function() {
                        $state.go('ccAssignToInstitute');
                    })
                }]
            })
            .state('ccAssignToInstitute.edit', {
                parent: 'ccAssignToInstitute',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/ccAssignToInstitute/ccAssignToInstitute-dialog.html',
                        controller: 'CcAssignToInstituteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['CcAssignToInstitute', function(CcAssignToInstitute) {
                                return CcAssignToInstitute.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('ccAssignToInstitute', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
