'use strict';

angular.module('stepApp')
    .controller('CcAssignToInstituteDetailController', function ($scope, $rootScope, $stateParams, entity, CcAssignToInstitute, InstGenInfo) {
        $scope.ccAssignToInstitute = entity;
        $scope.load = function (id) {
            CcAssignToInstitute.get({id: id}, function(result) {
                $scope.ccAssignToInstitute = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:ccAssignToInstituteUpdate', function(event, result) {
            $scope.ccAssignToInstitute = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
