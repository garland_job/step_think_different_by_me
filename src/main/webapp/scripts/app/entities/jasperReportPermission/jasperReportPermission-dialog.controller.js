'use strict';

angular.module('stepApp').controller('JasperReportPermissionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'JasperReportPermission', 'JasperReport',
        function($scope, $stateParams, $modalInstance, entity, JasperReportPermission, JasperReport) {

        $scope.jasperReportPermission = entity;
        $scope.jasperreports = JasperReport.query();
        $scope.load = function(id) {
            JasperReportPermission.get({id : id}, function(result) {
                $scope.jasperReportPermission = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:jasperReportPermissionUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.jasperReportPermission.id != null) {
                JasperReportPermission.update($scope.jasperReportPermission, onSaveSuccess, onSaveError);
            } else {
                JasperReportPermission.save($scope.jasperReportPermission, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
