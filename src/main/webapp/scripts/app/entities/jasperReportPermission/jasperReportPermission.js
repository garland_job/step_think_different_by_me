'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('jasperReportPermission', {
                parent: 'entity',
                url: '/jasperReportPermissions',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.jasperReportPermission.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasperReportPermission/jasperReportPermissions.html',
                        controller: 'JasperReportPermissionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportPermission');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('jasperReportPermission.detail', {
                parent: 'entity',
                url: '/jasperReportPermission/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.jasperReportPermission.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasperReportPermission/jasperReportPermission-detail.html',
                        controller: 'JasperReportPermissionDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportPermission');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JasperReportPermission', function($stateParams, JasperReportPermission) {
                        return JasperReportPermission.get({id : $stateParams.id});
                    }]
                }
            })
            .state('jasperReportPermission.new', {
                parent: 'jasperReportPermission',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReportPermission/jasperReportPermission-dialog.html',
                        controller: 'JasperReportPermissionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    authorityName: null,
                                    activeStatus: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReportPermission', null, { reload: true });
                    }, function() {
                        $state.go('jasperReportPermission');
                    })
                }]
            })
            .state('jasperReportPermission.edit', {
                parent: 'jasperReportPermission',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReportPermission/jasperReportPermission-dialog.html',
                        controller: 'JasperReportPermissionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['JasperReportPermission', function(JasperReportPermission) {
                                return JasperReportPermission.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReportPermission', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('jasperReportPermission.delete', {
                parent: 'jasperReportPermission',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReportPermission/jasperReportPermission-delete-dialog.html',
                        controller: 'JasperReportPermissionDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['JasperReportPermission', function(JasperReportPermission) {
                                return JasperReportPermission.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReportPermission', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
