'use strict';

angular.module('stepApp')
	.controller('JasperReportPermissionDeleteController', function($scope, $modalInstance, entity, JasperReportPermission) {

        $scope.jasperReportPermission = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            JasperReportPermission.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });