'use strict';

angular.module('stepApp')
    .controller('JasperReportPermissionDetailController', function ($scope, $rootScope, $stateParams, entity, JasperReportPermission, JasperReport) {
        $scope.jasperReportPermission = entity;
        $scope.load = function (id) {
            JasperReportPermission.get({id: id}, function(result) {
                $scope.jasperReportPermission = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:jasperReportPermissionUpdate', function(event, result) {
            $scope.jasperReportPermission = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
