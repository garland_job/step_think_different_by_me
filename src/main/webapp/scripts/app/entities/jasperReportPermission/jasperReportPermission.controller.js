'use strict';

angular.module('stepApp')
    .controller('JasperReportPermissionController', function ($scope, $state, $modal, JasperReportPermission, JasperReportPermissionSearch, ParseLinks) {
      
        $scope.jasperReportPermissions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            JasperReportPermission.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.jasperReportPermissions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            JasperReportPermissionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.jasperReportPermissions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.jasperReportPermission = {
                authorityName: null,
                activeStatus: null,
                id: null
            };
        };
    });
