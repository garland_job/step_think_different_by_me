'use strict';

angular.module('stepApp').controller('MaritalInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'MaritalInfo',
        function($scope, $stateParams, $modalInstance, entity, MaritalInfo) {

        $scope.maritalInfo = entity;
        $scope.load = function(id) {
            MaritalInfo.get({id : id}, function(result) {
                $scope.maritalInfo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:maritalInfoUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.maritalInfo.id != null) {
                MaritalInfo.update($scope.maritalInfo, onSaveSuccess, onSaveError);
            } else {
                MaritalInfo.save($scope.maritalInfo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
