'use strict';

angular.module('stepApp')
    .controller('MaritalInfoDetailController', function ($scope, $rootScope, $stateParams, entity, MaritalInfo) {
        $scope.maritalInfo = entity;
        $scope.load = function (id) {
            MaritalInfo.get({id: id}, function(result) {
                $scope.maritalInfo = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:maritalInfoUpdate', function(event, result) {
            $scope.maritalInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
