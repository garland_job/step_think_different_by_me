'use strict';

angular.module('stepApp')
    .controller('MaritalInfoController', function ($scope, $state, $modal, MaritalInfo, MaritalInfoSearch, ParseLinks) {
      
        $scope.maritalInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MaritalInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.maritalInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            MaritalInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.maritalInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.maritalInfo = {
                name: null,
                status: null,
                createDate: null,
                createBy: null,
                updateBy: null,
                updateDate: null,
                id: null
            };
        };
    });
