'use strict';

angular.module('stepApp')
	.controller('MaritalInfoDeleteController', function($scope, $modalInstance, entity, MaritalInfo) {

        $scope.maritalInfo = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            MaritalInfo.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });