'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('maritalInfo', {
                parent: 'entity',
                url: '/maritalInfos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.maritalInfo.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/maritalInfo/maritalInfos.html',
                        controller: 'MaritalInfoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('maritalInfo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('maritalInfo.detail', {
                parent: 'entity',
                url: '/maritalInfo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.maritalInfo.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/maritalInfo/maritalInfo-detail.html',
                        controller: 'MaritalInfoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('maritalInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MaritalInfo', function($stateParams, MaritalInfo) {
                        return MaritalInfo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('maritalInfo.new', {
                parent: 'maritalInfo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/maritalInfo/maritalInfo-dialog.html',
                        controller: 'MaritalInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    status: null,
                                    createDate: null,
                                    createBy: null,
                                    updateBy: null,
                                    updateDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('maritalInfo', null, { reload: true });
                    }, function() {
                        $state.go('maritalInfo');
                    })
                }]
            })
            .state('maritalInfo.edit', {
                parent: 'maritalInfo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/maritalInfo/maritalInfo-dialog.html',
                        controller: 'MaritalInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MaritalInfo', function(MaritalInfo) {
                                return MaritalInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('maritalInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('maritalInfo.delete', {
                parent: 'maritalInfo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/maritalInfo/maritalInfo-delete-dialog.html',
                        controller: 'MaritalInfoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['MaritalInfo', function(MaritalInfo) {
                                return MaritalInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('maritalInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
