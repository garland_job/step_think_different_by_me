'use strict';

angular.module('stepApp')
    .controller('InstEmpSalaryAdjDetailController', function ($scope, $rootScope, $stateParams, entity, InstEmpSalaryAdj, InstEmployee, PayScale, GazetteSetup) {
        $scope.instEmpSalaryAdj = entity;
        $scope.load = function (id) {
            InstEmpSalaryAdj.get({id: id}, function(result) {
                $scope.instEmpSalaryAdj = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:instEmpSalaryAdjUpdate', function(event, result) {
            $scope.instEmpSalaryAdj = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
