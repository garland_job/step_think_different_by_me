'use strict';

angular.module('stepApp').controller('InstEmpSalaryAdjDialogController',
    ['$scope', '$stateParams', 'entity', 'InstEmpSalaryAdj', 'InstEmployee', 'PayScalesOfActiveGazette', 'GazetteSetup',
        function ($scope, $stateParams, entity, InstEmpSalaryAdj, InstEmployee, PayScalesOfActiveGazette, GazetteSetup) {

            $scope.instEmpSalaryAdj = entity;
            $scope.instemployees = InstEmployee.query({size: 2000});
            $scope.payscales = PayScalesOfActiveGazette.query();
            $scope.gazettesetups = GazetteSetup.query();

            if ($stateParams.id != null) {
                InstEmpSalaryAdj.get({id: $stateParams.id}, function (result) {
                    console.log('====================== XXXXXX =================');
                    $scope.instEmpSalaryAdj = result;
                    $scope.instEmpSalaryAdj.payScale = result.payScale;
                    $scope.instEmpSalaryAdj.gazetteSetup = result.gazetteSetup;
                    $scope.instEmployee = result.instEmployee;
                    $scope.instEmpSalaryAdj.instEmployee = result.instEmployee;
                    console.log('==============');
                    console.log(result);
                    console.log('=============');
                });
            }
            $scope.load = function (id) {
                InstEmpSalaryAdj.get({id: id}, function (result) {
                    $scope.instEmpSalaryAdj = result;
                });
            };

            <!-- Added By Bappi Mazumder -->
            $scope.instEmployee = function (value, servicio) {
                $scope.instEmpSalaryAdj.instEmployee = value.originalObject;
            }

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:instEmpSalaryAdjUpdate', result);
                //$modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.instEmpSalaryAdj.id != null) {
                    InstEmpSalaryAdj.update($scope.instEmpSalaryAdj, onSaveFinished);
                } else {
                    InstEmpSalaryAdj.save($scope.instEmpSalaryAdj, onSaveFinished);
                }
            };

            $scope.clear = function () {
                //$modalInstance.dismiss('cancel');
            };
        }]);
