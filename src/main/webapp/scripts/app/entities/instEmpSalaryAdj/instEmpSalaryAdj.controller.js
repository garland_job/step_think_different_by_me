'use strict';

angular.module('stepApp')
    .controller('InstEmpSalaryAdjController', function ($scope, InstEmpSalaryAdj, InstEmpSalaryAdjSearch, ParseLinks) {
        $scope.instEmpSalaryAdjs = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            InstEmpSalaryAdj.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.instEmpSalaryAdjs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            InstEmpSalaryAdj.get({id: id}, function(result) {
                $scope.instEmpSalaryAdj = result;
                $('#deleteInstEmpSalaryAdjConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            InstEmpSalaryAdj.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteInstEmpSalaryAdjConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            InstEmpSalaryAdjSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.instEmpSalaryAdjs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.instEmpSalaryAdj = {
                typeAdjustment: null,
                amount: null,
                startDate: null,
                endDate: null,
                activeStatus: null,
                salaryAdjusted: null,
                createDate: null,
                updateDate: null,
                createBy: null,
                updateBy: null,
                type: null,
                remarks: null,
                salary_adjusted_date: null,
                id: null
            };
        };
    });
