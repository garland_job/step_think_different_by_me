'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('instEmpSalaryAdj', {
                parent: 'entity',
                url: '/instEmpSalaryAdjs',
                data: {
                    authorities: ['ROLE_MPOADMIN'],
                    pageTitle: 'stepApp.instEmpSalaryAdj.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/instEmpSalaryAdj/instEmpSalaryAdjs.html',
                        controller: 'InstEmpSalaryAdjController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('instEmpSalaryAdj');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('instEmpSalaryAdj.detail', {
                parent: 'entity',
                url: '/instEmpSalaryAdj/{id}',
                data: {
                    authorities: ['ROLE_MPOADMIN'],
                    pageTitle: 'stepApp.instEmpSalaryAdj.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/instEmpSalaryAdj/instEmpSalaryAdj-detail.html',
                        controller: 'InstEmpSalaryAdjDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('instEmpSalaryAdj');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'InstEmpSalaryAdj', function ($stateParams, InstEmpSalaryAdj) {
                        return InstEmpSalaryAdj.get({id: $stateParams.id});
                    }]
                }
            })
            .state('instEmpSalaryAdj.new', {
                parent: 'instEmpSalaryAdj',
                url: '/new',
                data: {
                    authorities: ['ROLE_MPOADMIN'],
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/instEmpSalaryAdj/instEmpSalaryAdj-dialog.html',
                        controller: 'InstEmpSalaryAdjDialogController',
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            typeAdjustment: null,
                            amount: null,
                            startDate: null,
                            endDate: null,
                            activeStatus: null,
                            salaryAdjusted: null,
                            createDate: null,
                            updateDate: null,
                            createBy: null,
                            updateBy: null,
                            type: null,
                            remarks: null,
                            salary_adjusted_date: null,
                            id: null
                        };
                    }
                }
            })
            .state('instEmpSalaryAdj.edit', {
                parent: 'instEmpSalaryAdj',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_MPOADMIN'],
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/instEmpSalaryAdj/instEmpSalaryAdj-dialog.html',
                        controller: 'InstEmpSalaryAdjDialogController',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('instEmpSalaryAdj');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'InstEmpSalaryAdj', function ($stateParams, InstEmpSalaryAdj) {
                       // return InstEmpSalaryAdj.get({id: $stateParams.id});
                    }]
                }
            });
    });
