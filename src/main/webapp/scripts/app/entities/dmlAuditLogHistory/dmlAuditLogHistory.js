'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('dmlAuditLogHistory', {
                parent: 'entity',
                url: '/dmlAuditLogHistorys',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.dmlAuditLogHistory.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/dmlAuditLogHistory/dmlAuditLogHistorys.html',
                        controller: 'DmlAuditLogHistoryController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dmlAuditLogHistory');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('dmlAuditLogHistory.detail', {
                parent: 'entity',
                url: '/dmlAuditLogHistory/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.dmlAuditLogHistory.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/dmlAuditLogHistory/dmlAuditLogHistory-detail.html',
                        controller: 'DmlAuditLogHistoryDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dmlAuditLogHistory');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'DmlAuditLogHistory', function($stateParams, DmlAuditLogHistory) {
                        return DmlAuditLogHistory.get({id : $stateParams.id});
                    }]
                }
            })
            .state('dmlAuditLogHistory.new', {
                parent: 'dmlAuditLogHistory',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/dmlAuditLogHistory/dmlAuditLogHistory-dialog.html',
                        controller: 'DmlAuditLogHistoryDialogController',
                    }
                },
                resolve: {
                    entity: function () {
                        return {
                            tableName: null,
                            colName: null,
                            valueBefore: null,
                            valueAfter: null,
                            appUserId: null,
                            userIpAddress: null,
                            userHostname: null,
                            osUser: null,
                            sessionId: null,
                            eventDttm: null,
                            eventType: null,
                            eventName: null,
                            pkColValue: null,
                            dmlSequence: null,
                            id: null
                        };
                    }
                }
            })
            .state('dmlAuditLogHistory.edit', {
                parent: 'dmlAuditLogHistory',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.dmlAuditLogHistory.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/dmlAuditLogHistory/dmlAuditLogHistory-dialog.html',
                        controller: 'DmlAuditLogHistoryDialogController',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dmlAuditLogHistory');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'DmlAuditLogHistory', function($stateParams, DmlAuditLogHistory) {
                        return DmlAuditLogHistory.get({id : $stateParams.id});
                    }]
                }
            });
    });
