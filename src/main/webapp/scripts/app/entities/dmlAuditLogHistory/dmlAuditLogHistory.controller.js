'use strict';

angular.module('stepApp')
    .controller('DmlAuditLogHistoryController', function ($scope, $state, DmlAuditLogHistory, DmlAuditLogHistorySearch, ParseLinks, UsersByIds) {
        $scope.dmlAuditLogHistorys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            //if($scope.dmlAuditLogHistorys.size < 0){
                DmlAuditLogHistory.query({page: $scope.page, size: 200000}, function(result, headers) {
                    $scope.links = ParseLinks.parse(headers('link'));
                    $scope.dmlAuditLogHistorys = result;
                    console.log('Hello');
                    console.log($scope.dmlAuditLogHistorys);
                });
            //}
        };
        $scope.refresh = function() {
            DmlAuditLogHistory.query({page: $scope.page, size: 200000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dmlAuditLogHistorys = result;
                console.log('Hello');
                console.log($scope.dmlAuditLogHistorys);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        //$state.go('user-management-detail', {login:user.login});

        $scope.loadUser = function (id) {
            UsersByIds.get({id: id}, function (result, headers) {
                $state.go('user-management-detail',{login:result.login});
            });
        }

        $scope.loadUserNameList = [];
        $scope.loadUserName = function (id, index) {
            UsersByIds.get({id: id}, function (result, headers) {
                $scope.loadUserNameList[index] = result.login;
            });
        }

        $scope.delete = function (id) {
            DmlAuditLogHistory.get({id: id}, function(result) {
                $scope.dmlAuditLogHistory = result;
                $('#deleteDmlAuditLogHistoryConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            DmlAuditLogHistory.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDmlAuditLogHistoryConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            DmlAuditLogHistorySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dmlAuditLogHistorys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.dmlAuditLogHistory = {
                tableName: null,
                colName: null,
                valueBefore: null,
                valueAfter: null,
                appUserId: null,
                userIpAddress: null,
                userHostname: null,
                osUser: null,
                sessionId: null,
                eventDttm: null,
                eventType: null,
                eventName: null,
                pkColValue: null,
                dmlSequence: null,
                id: null
            };
        };
    });
