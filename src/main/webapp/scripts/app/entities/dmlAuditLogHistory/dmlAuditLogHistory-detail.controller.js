'use strict';

angular.module('stepApp')
    .controller('DmlAuditLogHistoryDetailController', function ($scope, $rootScope, $stateParams, entity, DmlAuditLogHistory) {
        $scope.dmlAuditLogHistory = entity;
        $scope.load = function (id) {
            DmlAuditLogHistory.get({id: id}, function(result) {
                $scope.dmlAuditLogHistory = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:dmlAuditLogHistoryUpdate', function(event, result) {
            $scope.dmlAuditLogHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
