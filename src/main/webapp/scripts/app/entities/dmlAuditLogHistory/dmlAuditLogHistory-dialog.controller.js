'use strict';

angular.module('stepApp').controller('DmlAuditLogHistoryDialogController',
    ['$scope', '$stateParams', '$state', 'entity', 'DmlAuditLogHistory',
        function($scope, $stateParams, $state, entity, DmlAuditLogHistory) {

        $scope.dmlAuditLogHistory = entity;
        $scope.load = function(id) {
            DmlAuditLogHistory.get({id : id}, function(result) {
                $scope.dmlAuditLogHistory = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:dmlAuditLogHistoryUpdate', result);
            $state.go('dmlAuditLogHistory', null, { reload: true });
        };

        $scope.save = function () {
            if ($scope.dmlAuditLogHistory.id != null) {
                DmlAuditLogHistory.update($scope.dmlAuditLogHistory, onSaveFinished);
            } else {
                DmlAuditLogHistory.save($scope.dmlAuditLogHistory, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $state.go('dmlAuditLogHistory', null, { reload: true });
        };
}]);
