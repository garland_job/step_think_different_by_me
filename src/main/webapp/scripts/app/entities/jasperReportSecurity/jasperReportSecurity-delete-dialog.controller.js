'use strict';

angular.module('stepApp')
	.controller('JasperReportSecurityDeleteController', function($scope, $modalInstance, entity, JasperReportSecurity) {

        $scope.jasperReportSecurity = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            JasperReportSecurity.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });