'use strict';

angular.module('stepApp')
    .controller('JasperReportSecurityDetailController', function ($scope, $rootScope, $stateParams, entity, JasperReportSecurity) {
        $scope.jasperReportSecurity = entity;
        $scope.load = function (id) {
            JasperReportSecurity.get({id: id}, function(result) {
                $scope.jasperReportSecurity = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:jasperReportSecurityUpdate', function(event, result) {
            $scope.jasperReportSecurity = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
