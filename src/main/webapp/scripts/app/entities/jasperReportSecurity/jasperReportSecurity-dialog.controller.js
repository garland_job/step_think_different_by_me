'use strict';

angular.module('stepApp').controller('JasperReportSecurityDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'JasperReportSecurity',
        function($scope, $stateParams, $modalInstance, entity, JasperReportSecurity) {

        $scope.jasperReportSecurity = entity;
        $scope.load = function(id) {
            JasperReportSecurity.get({id : id}, function(result) {
                $scope.jasperReportSecurity = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:jasperReportSecurityUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.jasperReportSecurity.id != null) {
                JasperReportSecurity.update($scope.jasperReportSecurity, onSaveSuccess, onSaveError);
            } else {
                JasperReportSecurity.save($scope.jasperReportSecurity, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
