'use strict';

angular.module('stepApp')
    .controller('JasperReportSecurityController', function ($scope, $state, $modal, JasperReportSecurity, JasperReportSecuritySearch, ParseLinks) {
      
        $scope.jasperReportSecuritys = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            JasperReportSecurity.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.jasperReportSecuritys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            JasperReportSecuritySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.jasperReportSecuritys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.jasperReportSecurity = {
                domain: null,
                port: null,
                username: null,
                password: null,
                ActiveStatus: null,
                createdDate: null,
                updatedDate: null,
                createdBy: null,
                updatedBy: null,
                id: null
            };
        };
    });
