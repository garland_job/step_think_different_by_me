'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('jasperReportSecurity', {
                parent: 'entity',
                url: '/jasperReportSecuritys',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.jasperReportSecurity.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasperReportSecurity/jasperReportSecuritys.html',
                        controller: 'JasperReportSecurityController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportSecurity');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('jasperReportSecurity.detail', {
                parent: 'entity',
                url: '/jasperReportSecurity/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.jasperReportSecurity.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/jasperReportSecurity/jasperReportSecurity-detail.html',
                        controller: 'JasperReportSecurityDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('jasperReportSecurity');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'JasperReportSecurity', function($stateParams, JasperReportSecurity) {
                        return JasperReportSecurity.get({id : $stateParams.id});
                    }]
                }
            })
            .state('jasperReportSecurity.new', {
                parent: 'jasperReportSecurity',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReportSecurity/jasperReportSecurity-dialog.html',
                        controller: 'JasperReportSecurityDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    domain: null,
                                    port: null,
                                    username: null,
                                    password: null,
                                    ActiveStatus: null,
                                    createdDate: null,
                                    updatedDate: null,
                                    createdBy: null,
                                    updatedBy: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReportSecurity', null, { reload: true });
                    }, function() {
                        $state.go('jasperReportSecurity');
                    })
                }]
            })
            .state('jasperReportSecurity.edit', {
                parent: 'jasperReportSecurity',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReportSecurity/jasperReportSecurity-dialog.html',
                        controller: 'JasperReportSecurityDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['JasperReportSecurity', function(JasperReportSecurity) {
                                return JasperReportSecurity.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReportSecurity', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('jasperReportSecurity.delete', {
                parent: 'jasperReportSecurity',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/jasperReportSecurity/jasperReportSecurity-delete-dialog.html',
                        controller: 'JasperReportSecurityDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['JasperReportSecurity', function(JasperReportSecurity) {
                                return JasperReportSecurity.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('jasperReportSecurity', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
