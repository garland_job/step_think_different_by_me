'use strict';

angular.module('stepApp')
    .controller('PfmsEmpMembershipFormController', function ($scope, PfmsEmpMembershipForm, PfmsEmpMembershipFormSearch, ParseLinks) {
        $scope.pfmsEmpMembershipForms = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PfmsEmpMembershipForm.query({page: $scope.page, size: 10000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pfmsEmpMembershipForms = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PfmsEmpMembershipForm.get({id: id}, function(result) {
                $scope.pfmsEmpMembershipForm = result;
                $('#deletePfmsEmpMembershipFormConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsEmpMembershipForm.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsEmpMembershipFormConfirmation').modal('hide');
                    $scope.clear();
                });
        };


        $scope.search = function () {
            PfmsEmpMembershipFormSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsEmpMembershipForms = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsEmpMembershipForm = {
                isMinimumThreeYrs: false,
                isMandatoryContribute: false,
                isAnotherContFund: null,
                fundName: null,
                isEmpFamily: false,
                percentOfDeduct: null,
                isMoneySection: false,
                nomineeName: null,
                ageOfNominee: null,
                nomineeAddress: null,
                witnessNameOne: null,
                witnessMobileNoOne: null,
                witnessAddressOne: null,
                witnessNameTwo: null,
                witnessMobileNoTwo: null,
                witnessAddressTwo: null,
                stationName: null,
                applicationDate: null,
                remarks: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsEmpMembershipForm-dialog.controller.js*/

angular.module('stepApp').controller('PfmsEmpMembershipFormDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'PfmsEmpMembershipForm','Relationship', 'User', 'Principal', 'DateUtils','HrEmployeeInfo','HrEmployeeByUserTyping','GetAllHrEmployeeInfo',
        function($scope, $rootScope, $stateParams, $state, entity, PfmsEmpMembershipForm,Relationship, User, Principal, DateUtils, HrEmployeeInfo,HrEmployeeByUserTyping,GetAllHrEmployeeInfo) {

            $scope.pfmsEmpMembershipForm = entity;
            $scope.hremployeeinfos = [];
            $scope.relationships = Relationship.query();
            $scope.pfmsEmpMembershipFormOne = entity;
            $scope.predicate = 'id';
            $scope.reverse = true;
            $scope.page = 0;

            //HrEmployeeInfo.query({page: $scope.page, size: 5000, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
            //    // $scope.links = ParseLinks.parse(headers('link'));
            //    // $scope.totalItems = headers('X-Total-Count');
            //    $scope.hremployeeinfos = result;
            //});
            GetAllHrEmployeeInfo.query({},function(result){
                // $scope.links = ParseLinks.parse(headers('link'));
                // $scope.totalItems = headers('X-Total-Count');
                $scope.hremployeeinfos = result;
            });

            //$scope.getHrEmployeeByTyping = function (fullName) {
            //    console.log(fullName);
            //    HrEmployeeByUserTyping.query({firstName: fullName}, function (result) {
            //        $scope.HrEmployeeByUserTypings = result;
            //        console.log(result);
            //    });
            //};


            $scope.load = function(id) {
                PfmsEmpMembershipForm.get({id : id}, function(result) {
                    $scope.pfmsEmpMembershipFormOne = result;
                });
            };

            $scope.isExitsData = true;
            $scope.duplicateCheckMember = function(){
                $scope.isExitsData = true;
                if($stateParams.id){
                    $scope.load($stateParams.id);
                    if($scope.pfmsEmpMembershipForm.employeeInfo.id != $scope.pfmsEmpMembershipFormOne.employeeInfo.id){
                        PfmsEmpMembershipForm.query(function(result){
                            angular.forEach(result,function(dtoInfo){
                                if(dtoInfo.employeeInfo.id == $scope.pfmsEmpMembershipForm.employeeInfo.id){
                                    $scope.isExitsData = false;
                                }
                            });
                        });
                    }

                }else{
                    PfmsEmpMembershipForm.query(function(result){
                        angular.forEach(result,function(dtoInfo){
                            if(dtoInfo.employeeInfo.id == $scope.pfmsEmpMembershipForm.employeeInfo.id){
                                $scope.isExitsData = false;
                            }
                        });
                    });
                }

            };

            var today=new Date();
            $scope.newDate = today;

            if($stateParams.id){
                PfmsEmpMembershipForm.get({id : $stateParams.id}, function(result) {
                    $scope.pfmsEmpMembershipForm= result;
                    $scope.loadEmployeeInfo();
                });

            }
            $scope.loadEmployeeInfo = function(){
                $scope.empInfo = $scope.pfmsEmpMembershipForm.employeeInfo;
                $scope.designationName          = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName           = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide                 = $scope.empInfo.workArea.typeName;
                $scope.nationality              = $scope.empInfo.nationality;
                $scope.fatherName               = $scope.empInfo.fatherName;
                $scope.duplicateCheckMember();
            };

            $scope.isInRange = true;
            $scope.onChangePercentOfDeduct = function(){
                if($scope.pfmsEmpMembershipForm.percentOfDeduct < 5 || $scope.pfmsEmpMembershipForm.percentOfDeduct > 25){
                    $scope.isInRange = false;
                }else{
                    $scope.isInRange = true;
                }
            };

            $scope.isMinimumThreeYrsShow = false;
            $scope.onChangeEmployeeStatus = function(){
                if($scope.pfmsEmpMembershipForm.employeeStatusPfms == 'Temporary'){
                    $scope.isMinimumThreeYrsShow = true;
                }else{
                    $scope.isMinimumThreeYrsShow = false;
                }
            };

            $scope.curContributeChange = function(){
                $scope.pfmsEmpMembershipForm.curOwnContributeTot = $scope.pfmsEmpMembershipForm.curOwnContribute + $scope.pfmsEmpMembershipForm.curOwnContributeInt;
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsEmpMembershipFormUpdate', result);
                $scope.isSaving = false;
                $state.go("pfmsEmpMembershipForm");
            };

            $scope.save = function () {
                $scope.pfmsEmpMembershipForm.updateBy = $scope.loggedInUser.id;
                $scope.pfmsEmpMembershipForm.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pfmsEmpMembershipForm.id != null) {
                    PfmsEmpMembershipForm.update($scope.pfmsEmpMembershipForm, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.pfmsEmpMembershipForm.updated');
                } else {
                    $scope.pfmsEmpMembershipForm.createBy = $scope.loggedInUser.id;
                    $scope.pfmsEmpMembershipForm.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PfmsEmpMembershipForm.save($scope.pfmsEmpMembershipForm, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsEmpMembershipForm.created');

                }
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);



/*pfmsEmpMembershipForm-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsEmpMembershipFormDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsEmpMembershipForm, HrEmployeeInfo, Relationship) {
        $scope.pfmsEmpMembershipForm = entity;
        $scope.load = function (id) {
            PfmsEmpMembershipForm.get({id: id}, function(result) {
                $scope.pfmsEmpMembershipForm = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsEmpMembershipFormUpdate', function(event, result) {
            $scope.pfmsEmpMembershipForm = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
