'use strict';

angular.module('stepApp')
    .controller('PfmsLoanScheduleController', function ($scope, $rootScope, PfmsLoanSchedule, PfmsLoanScheduleSearch, ParseLinks) {
        $scope.pfmsLoanSchedules = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PfmsLoanSchedule.query({page: $scope.page, size: 10000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pfmsLoanSchedules = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PfmsLoanSchedule.get({id: id}, function(result) {
                $scope.pfmsLoanSchedule = result;
                $('#deletePfmsLoanScheduleConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsLoanSchedule.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsLoanScheduleConfirmation').modal('hide');
                    $scope.clear();
                    $rootScope.setErrorMessage('stepApp.pfmsLoanSchedule.deleted');
                });
        };

        $scope.search = function () {
            PfmsLoanScheduleSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsLoanSchedules = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsLoanSchedule = {
                loanYear: null,
                loanMonth: null,
                deductedAmount: null,
                installmentNo: null,
                totInstallNo: null,
                totLoanAmount: null,
                isRepay: false,
                reason: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsLoanSchedule-dialog.controller.js*/

angular.module('stepApp').controller('PfmsLoanScheduleDialogController',
    ['$scope', '$filter', '$rootScope','$stateParams', '$state', 'entity', 'PfmsLoanApplicationByApprove', 'PfmsLoanSchedule','PfmsLoanApplication','PfmsDeductionListByEmployee','PfmsLoanScheduleListByEmployee','PfmsEmpMembershipForm', 'User', 'Principal', 'DateUtils','HrEmployeeInfo',
        function($scope,$filter,$rootScope, $stateParams, $state, entity, PfmsLoanApplicationByApprove, PfmsLoanSchedule,PfmsLoanApplication, PfmsDeductionListByEmployee, PfmsLoanScheduleListByEmployee, PfmsEmpMembershipForm,  User, Principal, DateUtils, HrEmployeeInfo) {

            $scope.pfmsLoanSchedule = entity;
            //$scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.hremployeeinfos = [];
            $scope.pfmsEmpMembershipForms = [];
            $scope.pfmsloanapplications = [];

            PfmsEmpMembershipForm.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.pfmsEmpMembershipForms.push(dtoInfo);
                        //$scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            PfmsLoanApplication.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.approvalStatus == 'Approved' && dtoInfo.settlementStatus != 'Y'){
                        $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            // PfmsLoanApplication.query(function (result) {
            //    // console.log(JSON.stringify(result));
            //     angular.forEach(result, function (dtoInfo) {
            //        // $scope.pfmsloanapplications.push(dtoInfo);
            //         $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
            //     });
            // });

            $scope.isExitsData = true;
            $scope.duplicateDataMessage = '';
            $scope.duplicateScheduleCheckByYearAndMonth = function(){
                $scope.isExitsData = true;
                $scope.duplicateDataMessage = '';
                PfmsLoanScheduleListByEmployee.query({employeeInfoId: $scope.pfmsLoanSchedule.employeeInfo.id},function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.adjYear == $scope.pfmsLoanSchedule.adjYear
                            && dtoInfo.adjMonth == $scope.pfmsLoanSchedule.adjMonth){
                            $scope.isExitsData = false;
                            $scope.duplicateDataMessage = 'PF Loan is already schedule for this month.';
                        }
                    });
                });
            };

            $scope.loadEmployeeInfo = function(){
                $scope.empInfo = $scope.pfmsLoanSchedule.employeeInfo;
                $scope.designationName          = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName           = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide                 = $scope.empInfo.workArea.typeName;
                $scope.nationality              = $scope.empInfo.nationality;
                $scope.fatherName               = $scope.empInfo.fatherName;
                $scope.birthDate                = $scope.empInfo.birthDate;
                console.log('Employee Date of Birth is :');
                console.log($scope.birthDate);
                $scope.dateOfBirth = $filter('date')(new Date($scope.birthDate), 'dd/MM/yyyy');
                $scope.onChangeEmplyeeInfo();
            };

            /*------------Employee Age calculation--------------------*/

            $scope.employeeAgeNow=function () {
                var date = $filter('date')(new Date($scope.birthDate), 'yyyy/MM/dd');
                var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                var year = new Date().getFullYear();
                var month = new Date().getMonth() + 1;
                var day = new Date().getDate();
                var time=new Date().getTime();
                date = date.split('/');
                today=today.split('/');
                var yy = parseInt(date[0]);
                var mm = parseInt(date[1]);
                var dd = parseInt(date[2]);
                var years, months, days;
                // months
                months = month - mm;
                if (day < dd) {
                    months = months - 1;
                }
                // years
                years = year - yy;
                if (month * 100 + day < mm * 100 + dd) {
                    years = years - 1;
                    months = months + 12;
                }
                // days
                $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
                $scope.ageIs=years+" Years, "+months+" Months, "+$scope.days+" Days"
                // age is:
                console.log('-----Age is Now-------');
                console.log($scope.ageIs);
                var empAge=parseInt(years);
                if(empAge>52){
                    $('#reScheduleDeduction').modal('show');
                }else {
                    console.log('Hello');
                }

            }

            /*---------------------------------------------------------*/

            $scope.clear = function(){
                $scope.pfmsLoanSchedule.employeeInfo=null ;
            }

            $scope.deductionContinue =function(){

                $('#reScheduleDeduction').modal('hide');
            }

            $scope.onChangeEmplyeeInfo = function(){
                PfmsDeductionListByEmployee.query({employeeInfoId: $scope.pfmsLoanSchedule.employeeInfo.id},function(result){
                    angular.forEach(result,function(dtoInfo){
                        $scope.pfmsloanapplications.push(dtoInfo.pfmsLoanApp);
                    });
                });
            };

            $scope.load = function(id) {
                PfmsLoanSchedule.get({id : id}, function(result) {
                    $scope.pfmsLoanSchedule = result;
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsLoanScheduleUpdate', result);
                $scope.isSaving = false;
                $state.go("pfmsLoanSchedule");
            };

            $scope.monthList = {
                monthOptions: ['January','February','March','April', 'May','June','July','August','September','October','November','December']
            };

            $scope.save = function () {
                $scope.pfmsLoanSchedule.updateBy = $scope.loggedInUser.id;
                $scope.pfmsLoanSchedule.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pfmsLoanSchedule.id != null) {
                    PfmsLoanSchedule.update($scope.pfmsLoanSchedule, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsLoanSchedule.updated');
                } else {
                    $scope.pfmsLoanSchedule.isRepay = false;
                    $scope.pfmsLoanSchedule.createBy = $scope.loggedInUser.id;
                    $scope.pfmsLoanSchedule.totLoanAmount = $scope.pfmsLoanSchedule.pfmsLoanApp.loanAmount;
                    $scope.pfmsLoanSchedule.totInstallNo = $scope.pfmsLoanSchedule.pfmsLoanApp.noOfInstallment;
                    $scope.pfmsLoanSchedule.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.pfmsLoanSchedule.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PfmsLoanSchedule.save($scope.pfmsLoanSchedule, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsLoanSchedule.created');
                }
            };

        }]);

/*pfmsLoanSchedule-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsLoanScheduleDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsLoanSchedule, HrEmployeeInfo, PfmsLoanApplication) {
        $scope.pfmsLoanSchedule = entity;
        $scope.load = function (id) {
            PfmsLoanSchedule.get({id: id}, function(result) {
                $scope.pfmsLoanSchedule = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsLoanScheduleUpdate', function(event, result) {
            $scope.pfmsLoanSchedule = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
