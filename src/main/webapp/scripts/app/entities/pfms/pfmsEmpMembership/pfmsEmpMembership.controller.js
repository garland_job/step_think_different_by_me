'use strict';

angular.module('stepApp')
    .controller('PfmsEmpMembershipController', function ($scope, PfmsEmpMembership, PfmsEmpMembershipSearch, ParseLinks) {
        $scope.pfmsEmpMemberships = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PfmsEmpMembership.query({page: $scope.page, size: 10000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pfmsEmpMemberships = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PfmsEmpMembership.get({id: id}, function(result) {
                $scope.pfmsEmpMembership = result;
                $('#deletePfmsEmpMembershipConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsEmpMembership.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsEmpMembershipConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PfmsEmpMembershipSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsEmpMemberships = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsEmpMembership = {
                initOwnContribute: null,
                initOwnContributeInt: null,
                curOwnContribute: null,
                curOwnContributeInt: null,
                curOwnContributeTot: null,
                percentOfDeduct: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsEmpMembership-dialog.controller.js*/

angular.module('stepApp').controller('PfmsEmpMembershipDialogController',
    ['$scope', '$rootScope','$stateParams', '$state', 'entity', 'PfmsEmpMembership','PfmsEmpMembershipForm', 'User', 'Principal', 'DateUtils','HrEmployeeInfo',
        function($scope, $rootScope, $stateParams, $state, entity, PfmsEmpMembership, PfmsEmpMembershipForm, User, Principal, DateUtils, HrEmployeeInfo) {

            $scope.pfmsEmpMembership = entity;
            $scope.hremployeeinfos = [];
            $scope.pfmsEmpMembershipForms = [];
            $scope.pfmsEmpMembershipOne = entity;


            PfmsEmpMembershipForm.query({page: $scope.page, size: 2000},function(result){
                console.log('*************');
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        console.log('*************');
                        console.log(dtoInfo);
                        $scope.pfmsEmpMembershipForms.push(dtoInfo);
                        $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            if($stateParams.id){
                PfmsEmpMembership.get({id : $stateParams.id}, function(result) {
                    $scope.pfmsEmpMembership = result;
                    $scope.loadEmployeeInfo();
                });

            }

            $scope.loadPfmsEmpMembership = function(){
                PfmsEmpMembership.get({id : $stateParams.id}, function(result) {
                    $scope.pfmsEmpMembershipOne = result;
                });
            };

            $scope.isExitsData = true;
            $scope.duplicateCheckMember = function(){
                $scope.isExitsData = true;
                if($stateParams.id){
                    $scope.loadPfmsEmpMembership();
                    if($scope.pfmsEmpMembership.employeeInfo.id != $scope.pfmsEmpMembershipOne.employeeInfo.id){
                        PfmsEmpMembership.query(function(result){
                            angular.forEach(result,function(dtoInfo){
                                if(dtoInfo.employeeInfo.id == $scope.pfmsEmpMembership.employeeInfo.id){
                                    $scope.isExitsData = false;
                                }
                            });
                        });
                    }

                }else{
                    PfmsEmpMembership.query(function(result){
                        angular.forEach(result,function(dtoInfo){
                            if(dtoInfo.employeeInfo.id == $scope.pfmsEmpMembership.employeeInfo.id){
                                $scope.isExitsData = false;
                            }
                        });
                    });
                }

            };

            $scope.curContributeChange = function(){
                $scope.pfmsEmpMembership.curOwnContributeTot = $scope.pfmsEmpMembership.curOwnContribute
                    + $scope.pfmsEmpMembership.curOwnContributeInt
                    + $scope.pfmsEmpMembership.initOwnContribute
                    + $scope.pfmsEmpMembership.initOwnContributeInt;

            };

            $scope.loadEmployeeInfo = function(){
                $scope.empInfo = $scope.pfmsEmpMembership.employeeInfo;
                $scope.designationName          = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName           = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide                 = $scope.empInfo.workArea.typeName;
                $scope.nationality              = $scope.empInfo.nationality;
                $scope.fatherName               = $scope.empInfo.fatherName;
                $scope.duplicateCheckMember();
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsEmpMembershipUpdate', result);
                $scope.isSaving = false;
                $state.go("pfmsEmpMembership");
            };

            $scope.save = function () {
                $scope.pfmsEmpMembership.updateBy = $scope.loggedInUser.id;
                $scope.pfmsEmpMembership.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pfmsEmpMembership.id != null) {
                    PfmsEmpMembership.update($scope.pfmsEmpMembership, onSaveFinished);
                    $rootScope.setWarningMessage('stepApp.pfmsEmpMembership.updated');
                } else {
                    $scope.pfmsEmpMembership.createBy = $scope.loggedInUser.id;
                    $scope.pfmsEmpMembership.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PfmsEmpMembership.save($scope.pfmsEmpMembership, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsEmpMembership.created');

                }
            };

        }]);

/*pfmsEmpMembership-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsEmpMembershipDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsEmpMembership, HrEmployeeInfo) {
        $scope.pfmsEmpMembership = entity;
        $scope.load = function (id) {
            PfmsEmpMembership.get({id: id}, function(result) {
                $scope.pfmsEmpMembership = result;
            });
        };
        console.log($scope.pfmsEmpMembership);
        var unsubscribe = $rootScope.$on('stepApp:pfmsEmpMembershipUpdate', function(event, result) {
            $scope.pfmsEmpMembership = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*pfmsEmpMemDialog.controller.js*/

angular.module('stepApp').controller('PfmsEmpMemDialogController',
    ['$scope', 'PfmsEmpMembership','PfmsEmpMembershipForm','PfmsEmpMembershipListByEmployee','PfmsLoanApplicationByEmployee',
        function($scope, PfmsEmpMembership, PfmsEmpMembershipForm, PfmsEmpMembershipListByEmployee, PfmsLoanApplicationByEmployee) {

            $scope.hremployeeinfos = [];

            PfmsEmpMembershipForm.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            $scope.pfmsLoanNo = function(){
                $scope.totalLoanNoTaken = 0;
                $scope.reimberesedLonNo = 0;
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.pfmsEmpMembership.employeeInfo.id}, function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.approvalStatus != 'Reject'){
                            if(dtoInfo.settlementStatus == 'Y'){
                                $scope.reimberesedLonNo += 1 ;
                            }
                            $scope.totalLoanNoTaken += 1;
                        }

                    });
                });


            };

            $scope.loadEmployeePfInfo = function() {
                PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.pfmsEmpMembership.employeeInfo.id}, function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.activeStatus) {
                            $scope.curOwnContributeTot = dtoInfo.curOwnContributeTot;
                            $scope.withdrawAmount = (dtoInfo.curOwnContributeTot * 75) / 100;
                        }
                    });
                });
            };



            $scope.loadEmployeeInfo = function () {
                $scope.empInfo = $scope.pfmsEmpMembership.employeeInfo;
                $scope.designationName = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide = $scope.empInfo.workArea.typeName;
                $scope.nationality = $scope.empInfo.nationality;
                $scope.fatherName = $scope.empInfo.fatherName;
                $scope.loadEmployeePfInfo();
                $scope.pfmsLoanNo();
            };



        }]);


/*pfmsEmpPfAmount.controller.js*/

angular.module('stepApp').controller('PfmsEmpAmountController',
    ['$scope', 'PfmsEmpMembership','HrEmployeeInfo','PfmsEmpMembershipListByEmployee','PfmsLoanApplicationByEmployee',
        function($scope, PfmsEmpMembership, HrEmployeeInfo, PfmsEmpMembershipListByEmployee, PfmsLoanApplicationByEmployee) {

            $scope.hremployeeinfos = [];

            HrEmployeeInfo.get({id: 'my'}, function (result) {

                $scope.empInfo = result;
                $scope.designationName = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide = $scope.empInfo.workArea.typeName;
                $scope.nationality = $scope.empInfo.nationality;
                $scope.fatherName = $scope.empInfo.fatherName;
                $scope.loadEmployeePfInfo();
                $scope.pfmsLoanNo();
            });


            $scope.pfmsLoanNo = function(){
                $scope.totalLoanNoTaken = 0;
                $scope.reimberesedLonNo = 0;
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.empInfo.id}, function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.approvalStatus != 'Reject'){
                            if(dtoInfo.settlementStatus == 'Y'){
                                $scope.reimberesedLonNo += 1 ;
                            }
                            $scope.totalLoanNoTaken += 1;
                        }

                    });
                });


            };

            $scope.loadEmployeePfInfo = function() {
                PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.empInfo.id}, function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.activeStatus) {
                            $scope.curOwnContributeTot = dtoInfo.curOwnContributeTot;
                            $scope.withdrawAmount = (dtoInfo.curOwnContributeTot * 75) / 100;
                        }
                    });
                });
            };



        }]);

