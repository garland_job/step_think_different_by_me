/*
'use strict';

angular.module('stepApp').controller('PfmsEmpAmountController',
    ['$scope', 'PfmsEmpMembership','HrEmployeeInfo','PfmsEmpMembershipListByEmployee','PfmsLoanApplicationByEmployee',
        function($scope, PfmsEmpMembership, HrEmployeeInfo, PfmsEmpMembershipListByEmployee, PfmsLoanApplicationByEmployee) {

            $scope.hremployeeinfos = [];

            HrEmployeeInfo.get({id: 'my'}, function (result) {

                $scope.empInfo = result;
                $scope.designationName = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide = $scope.empInfo.workArea.typeName;
                $scope.nationality = $scope.empInfo.nationality;
                $scope.fatherName = $scope.empInfo.fatherName;
                $scope.loadEmployeePfInfo();
                $scope.pfmsLoanNo();
            });


            $scope.pfmsLoanNo = function(){
                $scope.totalLoanNoTaken = 0;
                $scope.reimberesedLonNo = 0;
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.empInfo.id}, function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.approvalStatus != 'Reject'){
                            if(dtoInfo.settlementStatus == 'Y'){
                                $scope.reimberesedLonNo += 1 ;
                            }
                            $scope.totalLoanNoTaken += 1;
                        }

                    });
                });


            };

            $scope.loadEmployeePfInfo = function() {
                PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.empInfo.id}, function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.activeStatus) {
                            $scope.curOwnContributeTot = dtoInfo.curOwnContributeTot;
                            $scope.withdrawAmount = (dtoInfo.curOwnContributeTot * 75) / 100;
                        }
                    });
                });
            };



        }]);



*/
