/*
'use strict';

angular.module('stepApp').controller('PfmsEmpMemDialogController',
    ['$scope', 'PfmsEmpMembership','PfmsEmpMembershipForm','PfmsEmpMembershipListByEmployee','PfmsLoanApplicationByEmployee',
        function($scope, PfmsEmpMembership, PfmsEmpMembershipForm, PfmsEmpMembershipListByEmployee, PfmsLoanApplicationByEmployee) {

            $scope.hremployeeinfos = [];

            PfmsEmpMembershipForm.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            $scope.pfmsLoanNo = function(){
                $scope.totalLoanNoTaken = 0;
                $scope.reimberesedLonNo = 0;
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.pfmsEmpMembership.employeeInfo.id}, function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.approvalStatus != 'Reject'){
                            if(dtoInfo.settlementStatus == 'Y'){
                                $scope.reimberesedLonNo += 1 ;
                            }
                            $scope.totalLoanNoTaken += 1;
                        }

                    });
                });


            };

            $scope.loadEmployeePfInfo = function() {
                PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.pfmsEmpMembership.employeeInfo.id}, function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.activeStatus) {
                            $scope.curOwnContributeTot = dtoInfo.curOwnContributeTot;
                            $scope.withdrawAmount = (dtoInfo.curOwnContributeTot * 75) / 100;
                        }
                    });
                });
            };



            $scope.loadEmployeeInfo = function () {
                $scope.empInfo = $scope.pfmsEmpMembership.employeeInfo;
                $scope.designationName = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide = $scope.empInfo.workArea.typeName;
                $scope.nationality = $scope.empInfo.nationality;
                $scope.fatherName = $scope.empInfo.fatherName;
                $scope.loadEmployeePfInfo();
                $scope.pfmsLoanNo();
            };



        }]);



*/
