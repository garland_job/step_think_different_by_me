'use strict';

angular.module('stepApp')
    .controller('PfmsLoanApplicationController', function ($scope, $rootScope, PfmsLoanApplication, HrEmployeeInfo, PfmsLoanApplicationByEmployee, PfmsLoanApplicationSearch, ParseLinks) {
        $scope.pfmsLoanApplications = [];
        /* $scope.page = 0;
         $scope.loadAll = function() {
         PfmsLoanApplication.query({page: $scope.page, size: 20}, function(result, headers) {
         $scope.links = ParseLinks.parse(headers('link'));
         $scope.pfmsLoanApplications = result;
         });
         };
         $scope.loadPage = function(page) {
         $scope.page = page;
         $scope.loadAll();
         };
         $scope.loadAll();*/


        $scope.newRequestList = [];
        $scope.loadingInProgress = true;
        $scope.requestEntityCounter = 0;
        var hrEmployeeInfoId = 0;

        HrEmployeeInfo.get({id: 'my'}, function (result) {
            hrEmployeeInfoId = result.id;
            $scope.loadAll();
        });


        $scope.loadAll = function () {
            $scope.requestEntityCounter = 1;
            $scope.newRequestList = [];
            PfmsLoanApplicationByEmployee.query({employeeId: hrEmployeeInfoId}, function (result) {
                $scope.requestEntityCounter++;
                angular.forEach(result, function (dtoInfo) {
                    $scope.newRequestList.push(dtoInfo);
                });
            });
            $scope.newRequestList.sort($scope.sortById);
        };


        $scope.sortById = function (a, b) {
            return b.id - a.id;
        };

        $scope.searchText = "";
        $scope.updateSearchText = "";

        $scope.clearSearchText = function (source) {
            if (source == 'request') {
                $timeout(function () {
                    $('#searchText').val('');
                    angular.element('#searchText').triggerHandler('change');
                });
            }
        };


        $scope.delete = function (id) {
            PfmsLoanApplication.get({id: id}, function (result) {
                $scope.pfmsLoanApplication = result;
                $('#deletePfmsLoanApplicationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsLoanApplication.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsLoanApplicationConfirmation').modal('hide');
                    $scope.clear();
                    $rootScope.setErrorMessage('stepApp.pfmsLoanApplication.deleted');
                });
        };

        $scope.search = function () {
            PfmsLoanApplicationSearch.query({query: $scope.searchQuery}, function (result) {
                $scope.pfmsLoanApplications = result;
            }, function (response) {
                if (response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsLoanApplication = {
                timesOfWithdraw: null,
                loanAmount: null,
                noOfInstallment: null,
                reasonOfWithdrawal: null,
                isRepayFirstWithdraw: null,
                noOfInstallmentLeft: null,
                lastInstallmentReDate: null,
                applicationDate: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsLoanApplication-dialog.controller.js*/

angular.module('stepApp').controller('PfmsLoanApplicationDialogController',
    ['$scope', '$filter', '$rootScope', '$stateParams', '$state', 'entity', 'PfmsLoanApplication', 'PfmsLoanApplicationByEmployee', 'PfmsEmpMembershipListByEmployee', 'PfmsEmpMonthlyAdj', 'User', 'Principal', 'DateUtils', 'HrEmployeeInfo',
        function ($scope, $filter, $rootScope, $stateParams, $state, entity, PfmsLoanApplication, PfmsLoanApplicationByEmployee, PfmsEmpMembershipListByEmployee, PfmsEmpMonthlyAdj, User, Principal, DateUtils, HrEmployeeInfo) {

            $scope.pfmsLoanApplication = entity;
            //$scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.load = function (id) {
                PfmsLoanApplication.get({id: id}, function (result) {
                    $scope.pfmsLoanApplication = result;
                });
            };

            if ($stateParams.id) {
                PfmsLoanApplication.get({id: $stateParams.id}, function (result) {
                    $scope.pfmsLoanApplication = result;
                });

            }

            HrEmployeeInfo.get({id: 'my'}, function (result) {
                console.log('===Employee Information====');
                console.log(result);
                $scope.hrEmployeeInfo = result;
                $scope.designationName = $scope.hrEmployeeInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName = $scope.hrEmployeeInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide = $scope.hrEmployeeInfo.workArea.typeName;
                $scope.nationality = $scope.hrEmployeeInfo.nationality;
                $scope.fatherName = $scope.hrEmployeeInfo.fatherName;
                $scope.birthDate  = $scope.hrEmployeeInfo.birthDate;
                console.log('Employee Date of Birth is :');
                $scope.dateOfBirth = $filter('date')(new Date($scope.birthDate), 'dd/MM/yyyy');
                console.log($scope.dateOfBirth);
                $scope.employeeAgeNow();
                //$scope.loadGroupByEmployee();
            });

            /*------------Employee Age calculation--------------------*/

            $scope.employeeAgeNow=function () {
                var date = $filter('date')(new Date($scope.birthDate), 'yyyy/MM/dd');
                var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                var year = new Date().getFullYear();
                var month = new Date().getMonth() + 1;
                var day = new Date().getDate();
                var time=new Date().getTime();
                date = date.split('/');
                today=today.split('/');
                var yy = parseInt(date[0]);
                var mm = parseInt(date[1]);
                var dd = parseInt(date[2]);
                var years, months, days;
                // months
                months = month - mm;
                if (day < dd) {
                    months = months - 1;
                }
                // years
                years = year - yy;
                if (month * 100 + day < mm * 100 + dd) {
                    years = years - 1;
                    months = months + 12;
                }
                // days
                $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
                $scope.ageIs=years+" Years, "+months+" Months, "+$scope.days+" Days"
                // age is:
                console.log('-----Age is Now-------');
                console.log($scope.ageIs);
                var empAge=parseInt(years);
                if(empAge>52){
                    $('#reScheduleDeduction').modal('show');
                }else {
                    console.log('Hello');
                }

            };

            $scope.clear = function(){
                $state.go('pfmsLoanApplication');
            };

            /*$scope.deductionContinue =function(){

                $('#reScheduleDeduction').modal('hide');
            };*/

            /*---------------------------------------------------------*/

            $scope.loggedInUser = {};
            $scope.getLoggedInUser = function () {
                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            $scope.isLoanParcentCheck = true;
            $scope.isLoanParcentCheckEmpty = true;
            $scope.loanPercentCheckMessage = '';
            $scope.loanPercentCheckMessageEmpty = '';

            $scope.loanTypeInfo = function(lType){
                 if(lType == 'HouseBuildingLoan'){
                     console.log('I am in House Building Loan amount!!!!');
                     $scope.pfmsLoanApplication.loanAmount = '';
                     $scope.loanPercentCheck = function () {
                         $scope.isLoanParcentCheck = true;
                         $scope.pendingLoanCheck();
                         $scope.timesOfWithdrawalChck();
                         $scope.timesOfWithdrawalTwoChck();
                         $scope.loanPercentCheckMessage = '';
                         PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.hrEmployeeInfo.id}, function (result) {
                             if (result.length) {
                                 angular.forEach(result, function (dtoInfo) {
                                     $scope.curTotEightyParcent = (dtoInfo.curOwnContributeTot * 80) / 100;
                                     if ($scope.pfmsLoanApplication.loanAmount > $scope.curTotEightyParcent) {
                                         $scope.isLoanParcentCheck = false;
                                         $scope.loanPercentCheckMessage = 'You can not take more than 80 Percent of your current PF amount.';
                                     }
                                 });
                             } else {
                                 $scope.isLoanParcentCheckEmpty = false;
                                 $scope.loanPercentCheckMessageEmpty = 'You do not have any PF amount.';
                             }

                         });
                     };

                 }
                 if(lType == 'HouseRepairingLoan'){
                     console.log('I am in House Repairing Loan amount!!!!');
                     $scope.pfmsLoanApplication.loanAmount = '';
                     $scope.loanPercentCheck = function () {
                         $scope.isLoanParcentCheck = true;
                         $scope.pendingLoanCheck();
                         $scope.timesOfWithdrawalChck();
                         $scope.timesOfWithdrawalTwoChck();
                         $scope.loanPercentCheckMessage = '';
                         PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.hrEmployeeInfo.id}, function (result) {
                             if (result.length) {
                                 angular.forEach(result, function (dtoInfo) {
                                     $scope.curTotSeventyFiveParcent = (dtoInfo.curOwnContributeTot * 75) / 100;
                                     if ($scope.pfmsLoanApplication.loanAmount > $scope.curTotSeventyFiveParcent) {
                                         $scope.isLoanParcentCheck = false;
                                         $scope.loanPercentCheckMessage = 'You can not take more than 75 Percent of your current PF amount.';
                                     }
                                 });
                             } else {
                                 $scope.isLoanParcentCheckEmpty = false;
                                 $scope.loanPercentCheckMessageEmpty = 'You do not have any PF amount.';
                             }

                         });
                     };
                 }
                 if(lType == 'OthersLoan'){
                     console.log('I am in Others Loan amount!!!!');
                     $scope.pfmsLoanApplication.loanAmount = '';
                     $scope.loanPercentCheck = function () {
                         $scope.isLoanParcentCheck = true;
                         $scope.pendingLoanCheck();
                         $scope.timesOfWithdrawalChck();
                         $scope.timesOfWithdrawalTwoChck();
                         $scope.loanPercentCheckMessage = '';
                         PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.hrEmployeeInfo.id}, function (result) {
                             if (result.length) {
                                 angular.forEach(result, function (dtoInfo) {
                                     $scope.curTotSeventyFiveParcent = (dtoInfo.curOwnContributeTot * 75) / 100;
                                     if ($scope.pfmsLoanApplication.loanAmount > $scope.curTotSeventyFiveParcent) {
                                         $scope.isLoanParcentCheck = false;
                                         $scope.loanPercentCheckMessage = 'You can not take more than 75 Percent of your current PF amount.';
                                     }
                                 });
                             } else {
                                 $scope.isLoanParcentCheckEmpty = false;
                                 $scope.loanPercentCheckMessageEmpty = 'You do not have any PF amount.';
                             }

                         });
                     };
                 }
            };

            $scope.isMaxInstall = true;
            $scope.maxInstallMsg = '';
            $scope.maxNoOfInstallment = function () {
                $scope.isMaxInstall = true;
                $scope.maxInstallMsg = '';
                if ($scope.pfmsLoanApplication.noOfInstallment < 12) {
                    $scope.isMaxInstall = false;
                    $scope.maxInstallMsg = 'You have to get minimum 12 installment.';

                }else if($scope.pfmsLoanApplication.noOfInstallment > 50){
                    $scope.isMaxInstall = false;
                    $scope.maxInstallMsg = 'You have to get maximum 50 installment.';
                }
            };


            $scope.isTimeOfWithDrawCheck = true;
            $scope.timesOfWithdrawalCheckMessage = '';
            $scope.timesOfWithdrawalChck = function () {
                $scope.isTimeOfWithDrawCheck = true;
                $scope.timesOfWithdrawalCheckMessage = '';
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.hrEmployeeInfo.id}, function (result) {
                    var timesOfWithdraw = 0;
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.isRepayFirstWithdraw) {
                            timesOfWithdraw += 1;
                            if (timesOfWithdraw > 3) {
                                $scope.isTimeOfWithDrawCheck = false;
                                $scope.timesOfWithdrawalCheckMessage = 'You can not take more than three Loan at a time.';
                            }
                        }
                    });
                });
            };

            $scope.isTimeOfWithDrawTwoCheck = true;
            $scope.timesOfWithdrawalTwoCheckMessage = '';
            $scope.timesOfWithdrawalTwoChck = function () {
                $scope.isTimeOfWithDrawTwoCheck = true;
                $scope.timesOfWithdrawalTwoCheckMessage = '';
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.hrEmployeeInfo.id}, function (result) {
                    var timesOfWithdraw = 0;
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.isRepayFirstWithdraw) {
                            timesOfWithdraw += 1;
                            if (timesOfWithdraw == 2) {
                                $scope.isTimeOfWithDrawTwoCheck = false;
                                $scope.timesOfWithdrawalTwoCheckMessage = 'You can take loan one more time.';
                            }
                        }
                    });
                });
            };

            $scope.isPendingLoan = true;
            $scope.pendingLoanCheckMessage = '';
            $scope.pendingLoanCheck = function () {
                $scope.isPendingLoan = true;
                $scope.pendingLoanCheckMessage = '';
                PfmsLoanApplicationByEmployee.query({employeeId: $scope.hrEmployeeInfo.id}, function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        if (dtoInfo.isDisburseLoan == false && dtoInfo.approvalStatus == 'Approved') {
                            $scope.isPendingLoan = false;
                            $scope.pendingLoanCheckMessage = 'You already have a pending Loan waiting for disburse.';
                        }
                    });
                });
            };

            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsLoanApplicationUpdate', result);
                $scope.isSaving = false;
                $state.go("pfmsLoanApplication");
            };

            $scope.save = function () {
                $scope.pfmsLoanApplication.updateBy = $scope.loggedInUser.id;
                $scope.pfmsLoanApplication.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.preCondition()) {
                    if ($scope.pfmsLoanApplication.id != null) {
                        PfmsLoanApplication.update($scope.pfmsLoanApplication, onSaveFinished);
                        $rootScope.setSuccessMessage('stepApp.pfmsLoanApplication.updated');
                    } else {
                        $scope.pfmsLoanApplication.employeeInfo = $scope.hrEmployeeInfo;
                        $scope.pfmsLoanApplication.isRepayFirstWithdraw = false;
                        $scope.pfmsLoanApplication.isDisburseLoan = false;
                        $scope.pfmsLoanApplication.settlementStatus = 'N';
                        $scope.pfmsLoanApplication.createBy = $scope.loggedInUser.id;
                        $scope.pfmsLoanApplication.createDate = DateUtils.convertLocaleDateToServer(new Date());
                        $scope.pfmsLoanApplication.applicationDate = DateUtils.convertLocaleDateToServer(new Date());
                        $scope.pfmsLoanApplication.approvalStatus = 'Pending';
                        $scope.pfmsLoanApplication.activeStatus = true;
                        PfmsLoanApplication.save($scope.pfmsLoanApplication, onSaveFinished);
                        $rootScope.setSuccessMessage('stepApp.pfmsLoanApplication.created');
                    }
                } else {
                    $rootScope.setErrorMessage("Please input valid data");
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsLoanApplication.loanAmount', $scope.pfmsLoanApplication.loanAmount, 'number', 10, 1, '0to9.', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsLoanApplication.noOfInstallment', $scope.pfmsLoanApplication.noOfInstallment, 'number', 2, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsLoanApplication.reasonOfWithdrawal', $scope.pfmsLoanApplication.reasonOfWithdrawal, 'text', 250, 10, 'atozAndAtoZ0-9.-', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);
/*pfmsLoanApplication-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsLoanApplicationDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsLoanApplication, HrEmployeeInfo) {
        $scope.pfmsLoanApplication = entity;
        $scope.load = function (id) {
            PfmsLoanApplication.get({id: id}, function (result) {
                $scope.pfmsLoanApplication = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsLoanApplicationUpdate', function (event, result) {
            $scope.pfmsLoanApplication = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*pfmsLoanAppRequest.controller.js*/

angular.module('stepApp')
    .controller('PfmsLoanAppRequestLController',
        ['$scope', '$state', '$rootScope', '$timeout', 'DataUtils', 'PfmsLoanAppFlow', 'ErpAuthorityFlowByCategory', 'PfmsLoanApplication', 'PfmsLoanAppFlowByActorName', 'PfmsLoanApplicationByEmployee', 'HrEmployeeInfo', 'Principal', 'DateUtils',

            function ($scope, $state, $rootScope, $timeout, DataUtils, PfmsLoanAppFlow, ErpAuthorityFlowByCategory, PfmsLoanApplication, PfmsLoanAppFlowByActorName, PfmsLoanApplicationByEmployee,
                      HrEmployeeInfo, Principal, DateUtils) {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.erpauthorityflows = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;

                ErpAuthorityFlowByCategory.query({authorityCategory: "PfmsLoanApplication"}, function (result) {
                    angular.forEach(result, function (dtoInfo) {
                        $scope.erpauthorityflows.push(dtoInfo);
                    });
                });

                $scope.loggedInUser = {};
                $scope.getLoggedInUser = function () {
                    console.log(JSON.stringify(Principal));
                    Principal.identity().then(function (account) {
                        User.get({login: account.login}, function (result) {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function () {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];

                    PfmsLoanAppFlowByActorName.query({}, function (result) {
                        $scope.requestEntityCounter++;
                        angular.forEach(result, function (dtoInfo) {
                            if (dtoInfo.erpAuthorityAction == "OnAuthorization") {
                                $scope.newRequestList.push(dtoInfo);
                            }
                        });
                    });
                    $scope.newRequestList.sort($scope.sortById);
                    $scope.loadApprovedRejectList();

                };

                $scope.sortById = function (a, b) {
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function () {

                    if ($scope.approvalObj.isApproved)
                        $scope.approvalObj.erpAuthorityAction = 'Forwarded';
                    else $scope.approvalObj.erpAuthorityAction = 'Reject';
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function () {

                    if ($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.erpAuthorityAction = 'Forwarded';
                    }
                    else {
                        $scope.approvalObj.erpAuthorityAction = 'Reject';
                        $scope.approvalObj.pfmsLoanApp.comments = $scope.approvalObj.comments;
                    }
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj) {
                    console.log(JSON.stringify(requestObj));

                    PfmsLoanAppFlow.update(requestObj, function (result) {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        //$scope.updateLeaveBalance(requestObj);
                        $scope.loadAll();
                    });
                };

                $scope.loadApprovedRejectList = function () {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                    PfmsLoanAppFlowByActorName.query({}, function (result) {
                        angular.forEach(result, function (requestObj) {
                            // if(requestObj.pfmsLoanApp.id != requestObj.pfmsLoanApp.id){
                            if (requestObj.erpAuthorityAction == "Forwarded" || requestObj.erpAuthorityAction == "Approved") {
                                $scope.approvedList.push(requestObj);
                            }
                            if (requestObj.erpAuthorityAction == "Reject") {
                                $scope.rejectedList.push(requestObj);
                            }
                            // }
                        });
                    }, function (response) {
                        console.log("data from view load failed");
                    });
                };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source) {
                    if (source == 'request') {
                        $timeout(function () {
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source) {
                    if (source == 'approved') {
                        $timeout(function () {
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source) {
                    if (source == 'approved') {
                        $timeout(function () {
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };


                $scope.approvalViewDetail = function (dataObj) {
                    $scope.approvalObj = dataObj;
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType) {
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        employeeId: null,
                        entityName: null,
                        requestFrom: null,
                        requestSummary: null,
                        requestDate: null,
                        approveState: null,
                        logStatus: null,
                        logComments: null,
                        actionType: '',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function (event, data) {
                    $scope.loadAll();
                });


                $scope.loadEmployee = function () {
                    HrEmployeeInfo.get({id: 'my'}, function (result) {
                        $scope.hrEmployeeInfo = result;

                    }, function (response) {
                        $scope.hasProfile = false;
                        $scope.noEmployeeFound = true;
                        $scope.isSaving = false;
                    })
                };

                $scope.sort = function (keyname, source) {
                    if (source == 'request') {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if (source == 'approved') {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if (source == 'rejected') {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };

                $scope.loadAll();

            }])
;

