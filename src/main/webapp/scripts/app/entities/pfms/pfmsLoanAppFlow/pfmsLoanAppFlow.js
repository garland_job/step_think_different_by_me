'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('pfmsLoanAppFlow', {
                parent: 'entity',
                url: '/pfmsLoanAppFlows',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.pfmsLoanAppFlow.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/pfms/pfmsLoanAppFlow/pfmsLoanAppFlows.html',
                        controller: 'PfmsLoanAppFlowController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pfmsLoanAppFlow');
                        $translatePartialLoader.addPart('authorityStatus');
                        $translatePartialLoader.addPart('erpAuthorityAction');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('pfmsLoanAppFlow.detail', {
                parent: 'entity',
                url: '/pfmsLoanAppFlow/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'stepApp.pfmsLoanAppFlow.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/pfms/pfmsLoanAppFlow/pfmsLoanAppFlow-detail.html',
                        controller: 'PfmsLoanAppFlowDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pfmsLoanAppFlow');
                        $translatePartialLoader.addPart('authorityStatus');
                        $translatePartialLoader.addPart('erpAuthorityAction');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PfmsLoanAppFlow', function($stateParams, PfmsLoanAppFlow) {
                        return PfmsLoanAppFlow.get({id : $stateParams.id});
                    }]
                }
            })
            .state('pfmsLoanAppFlow.new', {
                parent: 'pfmsLoanAppFlow',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pfms/pfmsLoanAppFlow/pfmsLoanAppFlow-dialog.html',
                        controller: 'PfmsLoanAppFlowDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    authorityStatus: null,
                                    erpAuthorityAction: null,
                                    activeStatus: null,
                                    createDate: null,
                                    createBy: null,
                                    updateDate: null,
                                    updateBy: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('pfmsLoanAppFlow', null, { reload: true });
                    }, function() {
                        $state.go('pfmsLoanAppFlow');
                    })
                }]
            })
            .state('pfmsLoanAppFlow.edit', {
                parent: 'pfmsLoanAppFlow',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pfms/pfmsLoanAppFlow/pfmsLoanAppFlow-dialog.html',
                        controller: 'PfmsLoanAppFlowDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['PfmsLoanAppFlow', function(PfmsLoanAppFlow) {
                                return PfmsLoanAppFlow.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('pfmsLoanAppFlow', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
