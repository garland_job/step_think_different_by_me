/*
'use strict';

angular.module('stepApp').controller('PfmsLoanAppFlowDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'PfmsLoanAppFlow', 'ErpAuthorityFlow', 'HrEmployeeInfo', 'PfmsLoanApplication',
        function($scope, $stateParams, $modalInstance, entity, PfmsLoanAppFlow, ErpAuthorityFlow, HrEmployeeInfo, PfmsLoanApplication) {

        $scope.pfmsLoanAppFlow = entity;
        $scope.erpauthorityflows = ErpAuthorityFlow.query();
        $scope.hremployeeinfos = HrEmployeeInfo.query();
        $scope.pfmsloanapplications = PfmsLoanApplication.query();
        $scope.load = function(id) {
            PfmsLoanAppFlow.get({id : id}, function(result) {
                $scope.pfmsLoanAppFlow = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:pfmsLoanAppFlowUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.pfmsLoanAppFlow.id != null) {
                PfmsLoanAppFlow.update($scope.pfmsLoanAppFlow, onSaveFinished);
            } else {
                PfmsLoanAppFlow.save($scope.pfmsLoanAppFlow, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
*/
