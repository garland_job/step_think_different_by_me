'use strict';

angular.module('stepApp')
    .controller('PfmsLoanAppFlowController', function ($scope, PfmsLoanAppFlow, PfmsLoanAppFlowSearch, ParseLinks) {
        $scope.pfmsLoanAppFlows = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PfmsLoanAppFlow.query({page: $scope.page, size: 10000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pfmsLoanAppFlows = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PfmsLoanAppFlow.get({id: id}, function(result) {
                $scope.pfmsLoanAppFlow = result;
                $('#deletePfmsLoanAppFlowConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsLoanAppFlow.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsLoanAppFlowConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PfmsLoanAppFlowSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsLoanAppFlows = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsLoanAppFlow = {
                authorityStatus: null,
                erpAuthorityAction: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsLoanAppFlow-dialog.controller.js*/

angular.module('stepApp').controller('PfmsLoanAppFlowDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'PfmsLoanAppFlow', 'ErpAuthorityFlow', 'HrEmployeeInfo', 'PfmsLoanApplication',
        function($scope, $stateParams, $modalInstance, entity, PfmsLoanAppFlow, ErpAuthorityFlow, HrEmployeeInfo, PfmsLoanApplication) {

            $scope.pfmsLoanAppFlow = entity;
            $scope.erpauthorityflows = ErpAuthorityFlow.query();
            $scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.pfmsloanapplications = PfmsLoanApplication.query();
            $scope.load = function(id) {
                PfmsLoanAppFlow.get({id : id}, function(result) {
                    $scope.pfmsLoanAppFlow = result;
                });
            };

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsLoanAppFlowUpdate', result);
                $modalInstance.close(result);
            };

            $scope.save = function () {
                if ($scope.pfmsLoanAppFlow.id != null) {
                    PfmsLoanAppFlow.update($scope.pfmsLoanAppFlow, onSaveFinished);
                } else {
                    PfmsLoanAppFlow.save($scope.pfmsLoanAppFlow, onSaveFinished);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
/*pfmsLoanAppFlow-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsLoanAppFlowDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsLoanAppFlow, ErpAuthorityFlow, HrEmployeeInfo, PfmsLoanApplication) {
        $scope.pfmsLoanAppFlow = entity;
        $scope.load = function (id) {
            PfmsLoanAppFlow.get({id: id}, function(result) {
                $scope.pfmsLoanAppFlow = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsLoanAppFlowUpdate', function(event, result) {
            $scope.pfmsLoanAppFlow = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
