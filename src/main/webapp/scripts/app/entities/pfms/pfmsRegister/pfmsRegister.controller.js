'use strict';

angular.module('stepApp')
    .controller('PfmsRegisterController', function ($scope, $rootScope, PfmsRegister, PfmsRegisterSearch, ParseLinks) {
        $scope.pfmsRegisters = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PfmsRegister.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pfmsRegisters = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PfmsRegister.get({id: id}, function(result) {
                $scope.pfmsRegister = result;
                $('#deletePfmsRegisterConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsRegister.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsRegisterConfirmation').modal('hide');
                    $scope.clear();
                    $rootScope.setErrorMessage('stepApp.pfmsRegister.deleted');
                });
        };

        $scope.search = function () {
            PfmsRegisterSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsRegisters = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsRegister = {
                applicationType: null,
                isBillRegister: false,
                billNo: null,
                billIssueDate: null,
                billReceiverName: null,
                billPlace: null,
                billDate: null,
                noOfWithdrawal: null,
                checkNo: null,
                checkDate: null,
                checkTokenNo: null,
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsRegister-dialog.controller.js*/

angular.module('stepApp').controller('PfmsRegisterDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PfmsRegister','PfmsEmpMembershipForm', 'User', 'Principal', 'DateUtils','HrEmployeeInfo',
        function($scope, $rootScope, $stateParams, $state, entity, PfmsRegister, PfmsEmpMembershipForm,  User, Principal, DateUtils, HrEmployeeInfo) {

            $scope.pfmsRegister = entity;
            //$scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.hremployeeinfos = [];
            $scope.pfmsEmpMembershipForms = [];

            PfmsEmpMembershipForm.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.activeStatus){
                        $scope.pfmsEmpMembershipForms.push(dtoInfo);
                        $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            $scope.loadEmployeeInfo = function(){
                $scope.empInfo = $scope.pfmsRegister.employeeInfo;
                $scope.designationName          = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName           = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide                 = $scope.empInfo.workArea.typeName;
                $scope.nationality              = $scope.empInfo.nationality;
                $scope.fatherName               = $scope.empInfo.fatherName;
            };

            $scope.load = function(id) {
                PfmsRegister.get({id : id}, function(result) {
                    $scope.pfmsRegister = result;
                });
            };

            if($stateParams.id){
                PfmsRegister.get({id : $stateParams.id}, function(result) {
                    $scope.pfmsRegister= result;
                    $scope.loadEmployeeInfo();
                });

            }

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsRegisterUpdate', result);
                $scope.isSaving = false;
                $state.go("pfmsRegister");
            };

            $scope.save = function () {
                $scope.pfmsRegister.updateBy = $scope.loggedInUser.id;
                $scope.pfmsRegister.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pfmsRegister.id != null) {
                    PfmsRegister.update($scope.pfmsRegister, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsRegister.updated');
                    console.log("Coming here");

                } else {
                    $scope.pfmsRegister.createBy = $scope.loggedInUser.id;
                    $scope.pfmsRegister.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PfmsRegister.save($scope.pfmsRegister, onSaveFinished);
                    console.log("Coming here");
                    $rootScope.setSuccessMessage('stepApp.pfmsRegister.created');
                }
            };

        }]);
/*pfmsRegister-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsRegisterDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsRegister, HrEmployeeInfo) {
        $scope.pfmsRegister = entity;
        $scope.load = function (id) {
            PfmsRegister.get({id: id}, function(result) {
                $scope.pfmsRegister = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsRegisterUpdate', function(event, result) {
            $scope.pfmsRegister = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
