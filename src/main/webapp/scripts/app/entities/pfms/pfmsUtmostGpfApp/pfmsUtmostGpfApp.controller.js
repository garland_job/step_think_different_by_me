'use strict';

angular.module('stepApp')
    .controller('PfmsUtmostGpfAppController', function ($scope, $rootScope, PfmsUtmostGpfApp,HrEmployeeInfo, PfmsUtmostGpfAppListByEmployee, PfmsUtmostGpfAppSearch, ParseLinks) {
        $scope.pfmsUtmostGpfApps = [];
        //$scope.page = 0;
        //$scope.loadAll = function() {
        //    PfmsUtmostGpfApp.query({page: $scope.page, size: 20}, function(result, headers) {
        //        $scope.links = ParseLinks.parse(headers('link'));
        //        $scope.pfmsUtmostGpfApps = result;
        //    });
        //};
        //$scope.loadPage = function(page) {
        //    $scope.page = page;
        //    $scope.loadAll();
        //};
        //$scope.loadAll();

        $scope.newRequestList = [];
        $scope.loadingInProgress = true;
        $scope.requestEntityCounter = 0;
        var hrEmployeeInfoId = 0;

        HrEmployeeInfo.get({id: 'my'}, function (result) {
            hrEmployeeInfoId = result.id;
            $scope.loadAll();
        });


        $scope.loadAll = function()
        {
            $scope.requestEntityCounter = 1;
            $scope.newRequestList = [];
            PfmsUtmostGpfAppListByEmployee.query({employeeInfoId: hrEmployeeInfoId},function(result)
            {
                $scope.requestEntityCounter++;
                angular.forEach(result,function(dtoInfo)
                {
                    $scope.newRequestList.push(dtoInfo);
                });
            });
            $scope.newRequestList.sort($scope.sortById);
        };



        $scope.sortById = function(a,b){
            return b.id - a.id;
        };

        $scope.searchText = "";
        $scope.updateSearchText = "";

        $scope.clearSearchText = function (source)
        {
            if(source=='request')
            {
                $timeout(function(){
                    $('#searchText').val('');
                    angular.element('#searchText').triggerHandler('change');
                });
            }
        };

        $scope.delete = function (id) {
            PfmsUtmostGpfApp.get({id: id}, function(result) {
                $scope.pfmsUtmostGpfApp = result;
                $('#deletePfmsUtmostGpfAppConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsUtmostGpfApp.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsUtmostGpfAppConfirmation').modal('hide');
                    $scope.clear();
                    $rootScope.setErrorMessage('stepApp.pfmsUtmostGpfApp.deleted');
                });
        };

        $scope.search = function () {
            PfmsUtmostGpfAppSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsUtmostGpfApps = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsUtmostGpfApp = {
                prlDate: null,
                gpfNo: null,
                lastDeduction: null,
                deductionDate: null,
                billNo: null,
                billDate: null,
                tokenNo: null,
                tokenDate: null,
                withdrawFrom: null,
                applyDate: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*pfmsUtmostGpfApp-dialog.controller.js*/

angular.module('stepApp').controller('PfmsUtmostGpfAppDialogController',
    ['$scope', '$rootScope', '$stateParams', '$state', 'entity', 'PfmsUtmostGpfApp','PfmsEmpMembershipFormListByEmployee','PfmsEmpMembershipForm','PfmsRegister', 'User', 'Principal', 'DateUtils','HrEmployeeInfo',
        function($scope, $rootScope, $stateParams, $state, entity, PfmsUtmostGpfApp,PfmsEmpMembershipFormListByEmployee, PfmsEmpMembershipForm, PfmsRegister, User, Principal, DateUtils, HrEmployeeInfo) {

            $scope.pfmsUtmostGpfApp = entity;
            //$scope.hremployeeinfos = HrEmployeeInfo.query();
            $scope.hrEmployeeInfo = null;

            HrEmployeeInfo.get({id: 'my'}, function (result) {

                $scope.hrEmployeeInfo = result;
                $scope.designationName          = $scope.hrEmployeeInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName           = $scope.hrEmployeeInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide                 = $scope.hrEmployeeInfo.workArea.typeName;
                $scope.nationality              = $scope.hrEmployeeInfo.nationality;
                $scope.fatherName               = $scope.hrEmployeeInfo.fatherName;
                $scope.pfmsUtmostGpfApp.prlDate               = $scope.hrEmployeeInfo.prlDate;
                $scope.loadPfAccountNo(result.id);
                $scope.loadPfmsRegister(result.id);
            });

            $scope.loadPfAccountNo= function(empId)
            {
                PfmsEmpMembershipFormListByEmployee.query({employeeInfoId:  empId},function(result){
                    angular.forEach(result,function(dtoInfo){
                        $scope.pfmsUtmostGpfApp.gpfNo = dtoInfo.accountNo;
                    });
                });

            };

            $scope.load = function(id) {
                PfmsUtmostGpfApp.get({id : id}, function(result) {
                    $scope.pfmsUtmostGpfApp = result;
                });
            };
            $scope.loadPfmsRegister = function(employeeId){
                PfmsRegister.query(function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.employeeInfo.id == employeeId
                            && dtoInfo.applicationType == 'Provident Fund') {
                            if (dtoInfo.isBillRegister) {
                                $scope.pfmsUtmostGpfApp.billNo = dtoInfo.billNo;
                                $scope.pfmsUtmostGpfApp.billDate = dtoInfo.billDate;
                            }else{
                                $scope.pfmsUtmostGpfApp.tokenNo = dtoInfo.checkTokenNo;
                                $scope.pfmsUtmostGpfApp.tokenDate = dtoInfo.checkDate;
                            }
                        }
                    });
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsUtmostGpfAppUpdate', result);
                $scope.isSaving = false;
                $state.go("pfmsUtmostGpfApp");
            };

            $scope.save = function () {
                $scope.pfmsUtmostGpfApp.updateBy = $scope.loggedInUser.id;
                $scope.pfmsUtmostGpfApp.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if($scope.preCondition()){
                if ($scope.pfmsUtmostGpfApp.id != null) {
                    PfmsUtmostGpfApp.update($scope.pfmsUtmostGpfApp, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsUtmostGpfApp.updated');
                } else {
                    $scope.pfmsUtmostGpfApp.employeeInfo = $scope.hrEmployeeInfo;
                    $scope.pfmsUtmostGpfApp.createBy = $scope.loggedInUser.id;
                    $scope.pfmsUtmostGpfApp.isDeductFinalize = false;
                    $scope.pfmsUtmostGpfApp.approvalStatus = 'Pending';
                    $scope.pfmsUtmostGpfApp.applyDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.pfmsUtmostGpfApp.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PfmsUtmostGpfApp.save($scope.pfmsUtmostGpfApp, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsUtmostGpfApp.created');
                }
                }else{
                    $rootScope.setErrorMessage("Please input valid data");
                }
            };

            $scope.preCondition = function () {
                $scope.isPreCondition = true;
                $scope.preConditionStatus = [];
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.prlDate',DateUtils.convertLocaleDateToDMY($scope.pfmsUtmostGpfApp.prlDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.gpfNo', $scope.pfmsUtmostGpfApp.gpfNo, 'number', 12, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.lastDeduction', $scope.pfmsUtmostGpfApp.lastDeduction, 'number', 12, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.deductionDate',DateUtils.convertLocaleDateToDMY($scope.pfmsUtmostGpfApp.deductionDate), 'date', 60, 5, 'date', true, '',''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.billNo', $scope.pfmsUtmostGpfApp.billNo, 'number', 12, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.tokenNo', $scope.pfmsUtmostGpfApp.tokenNo, 'number', 12, 1, '0to9', true, '', ''));
                $scope.preConditionStatus.push($rootScope.layerDataCheck('pfmsUtmostGpfApp.withdrawFrom', $scope.pfmsUtmostGpfApp.withdrawFrom, 'text', 100, 4, 'atozAndAtoZ().-', true, '', ''));

                if ($scope.preConditionStatus.indexOf(false) !== -1) {
                    $scope.isPreCondition = false;
                } else {
                    $scope.isPreCondition = true;
                }
                console.log("&&&&&&&&&&&&&" + $scope.isPreCondition);
                console.log($scope.preConditionStatus);
                return $scope.isPreCondition;
            };

            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

        }]);


/*pfmsUtmostGpfApp-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsUtmostGpfAppDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsUtmostGpfApp, HrEmployeeInfo) {
        $scope.pfmsUtmostGpfApp = entity;
        $scope.load = function (id) {
            PfmsUtmostGpfApp.get({id: id}, function(result) {
                $scope.pfmsUtmostGpfApp = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsUtmostGpfAppUpdate', function(event, result) {
            $scope.pfmsUtmostGpfApp = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
/*pfmsUtmostGpfAppRequest.controller.js*/

angular.module('stepApp')
    .controller('PfmsUtmostGpfAppLController',
        ['$scope', '$state', '$rootScope', '$timeout', 'DataUtils', 'PfmsUtmostGpfApp', 'HrEmployeeInfo', 'Principal',  'DateUtils',

            function($scope, $state, $rootScope, $timeout, DataUtils, PfmsUtmostGpfApp,
                     HrEmployeeInfo, Principal, DateUtils)
            {

                $scope.newRequestList = [];
                $scope.approvedList = [];
                $scope.rejectedList = [];
                $scope.loadingInProgress = true;
                $scope.requestEntityCounter = 0;

                $scope.loggedInUser =   {};
                $scope.getLoggedInUser = function ()
                {
                    Principal.identity().then(function (account)
                    {
                        User.get({login: account.login}, function (result)
                        {
                            $scope.loggedInUser = result;
                        });
                    });
                };

                $scope.loadAll = function()
                {
                    $scope.requestEntityCounter = 1;
                    $scope.newRequestList = [];
                    PfmsUtmostGpfApp.query(function(result)
                    {
                        $scope.requestEntityCounter++;
                        angular.forEach(result,function(dtoInfo)
                        {
                            if(dtoInfo.approvalStatus == "Pending"){
                                $scope.newRequestList.push(dtoInfo);
                            }
                        });
                    });
                    $scope.newRequestList.sort($scope.sortById);
                    $scope.loadApprovedRejectList();

                };

                $scope.sortById = function(a,b){
                    return b.id - a.id;
                };

                $scope.approvalViewAction = function ()
                {
                    if($scope.approvalObj.isApproved)
                        $scope.approvalObj.approvalStatus='Approved';
                    else $scope.approvalObj.approvalStatus='Reject';
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalActionDirect = function ()
                {

                    if($scope.approvalObj.actionType == 'accept') {
                        $scope.approvalObj.approvalStatus = 'Approved';
                    }
                    else {
                        $scope.approvalObj.approvalStatus='Reject';
                    }
                    $scope.approvalAction($scope.approvalObj);
                };

                $scope.approvalAction = function (requestObj){

                    PfmsUtmostGpfApp.update(requestObj, function(result)
                    {
                        $('#approveRejectConfirmation').modal('hide');
                        $('#approveViewDetailForm').modal('hide');
                        $scope.loadAll();
                    });
                };


                $scope.loadApprovedRejectList = function ()
                {
                    $scope.approvedList = [];
                    $scope.rejectedList = [];
                    PfmsUtmostGpfApp.query({}, function(result)
                    {
                        angular.forEach(result,function(requestObj)
                        {
                            if(requestObj.approvalStatus=="Approved")
                            {
                                $scope.approvedList.push(requestObj);
                            }
                            if(requestObj.approvalStatus=="Reject")
                            {
                                $scope.rejectedList.push(requestObj);
                            }
                        });
                    },function(response)
                    {
                        console.log("data from view load failed");
                    });
                };

                $scope.searchText = "";
                $scope.updateSearchText = "";

                $scope.clearSearchText = function (source)
                {
                    if(source=='request')
                    {
                        $timeout(function(){
                            $('#searchText').val('');
                            angular.element('#searchText').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextApp = "";

                $scope.clearSearchTextApp = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextApp').val('');
                            angular.element('#searchTextApp').triggerHandler('change');
                        });
                    }
                };

                $scope.searchTextRej = "";

                $scope.clearSearchTextRej = function (source)
                {
                    if(source=='approved')
                    {
                        $timeout(function(){
                            $('#searchTextRej').val('');
                            angular.element('#searchTextRej').triggerHandler('change');
                        });
                    }
                };


                $scope.approvalViewDetail = function (dataObj)
                {
                    $scope.approvalObj = dataObj;
                    $('#approveViewDetailForm').modal('show');
                };

                $scope.approvalConfirmation = function (dataObj, actionType){
                    $scope.approvalObj = dataObj;
                    $scope.approvalObj.actionType = actionType;
                    $('#approveRejectConfirmation').modal('show');
                };

                $scope.clear = function () {
                    $scope.approvalObj = {
                        entityId: null,
                        employeeId:null,
                        entityName:null,
                        requestFrom:null,
                        requestSummary: null,
                        requestDate:null,
                        approveState: null,
                        logStatus:null,
                        logComments:null,
                        actionType:'',
                        entityObject: null
                    };
                };

                $rootScope.$on('onEntityApprovalProcessCompleted', function(event, data)
                {
                    $scope.loadAll();
                });


                $scope.loadEmployee = function () {
                    HrEmployeeInfo.get({id: 'my'}, function (result) {
                        $scope.hrEmployeeInfo = result;

                    }, function (response) {
                        $scope.hasProfile = false;
                        $scope.noEmployeeFound = true;
                        $scope.isSaving = false;
                    })
                };

                $scope.sort = function(keyname, source){
                    if(source=='request')
                    {
                        $scope.sortKey = keyname;   //set the sortKey to the param passed
                        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
                    }

                    else if(source=='approved')
                    {
                        $scope.sortKey3 = keyname;
                        $scope.reverse3 = !$scope.reverse3;
                    }
                    else if(source=='rejected')
                    {
                        $scope.sortKey4 = keyname;
                        $scope.reverse4 = !$scope.reverse4;
                    }

                };

                $scope.loadAll();

            }])
;

