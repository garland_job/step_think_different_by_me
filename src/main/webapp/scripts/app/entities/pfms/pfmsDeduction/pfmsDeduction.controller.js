'use strict';

angular.module('stepApp')
    .controller('PfmsDeductionController', function ($scope, $rootScope, PfmsDeduction, PfmsDeductionSearch, ParseLinks) {
        $scope.pfmsDeductions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            PfmsDeduction.query({page: $scope.page, size: 10000}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pfmsDeductions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PfmsDeduction.get({id: id}, function(result) {
                $scope.pfmsDeduction = result;
                $('#deletePfmsDeductionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PfmsDeduction.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePfmsDeductionConfirmation').modal('hide');
                    $scope.clear();
                    $rootScope.setErrorMessage('stepApp.pfmsDeduction.deleted');
                });
        };

        $scope.search = function () {
            PfmsDeductionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pfmsDeductions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pfmsDeduction = {
                accountNo: null,
                deductionAmount: null,
                deductionDate: null,
                activeStatus: false,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
/*------=====------pfmsDeduction-dialog.controller.js--------=======------*/

angular.module('stepApp').controller('PfmsDeductionDialogController',
    ['$scope', '$filter', '$rootScope', '$stateParams', '$state', 'entity', 'PfmsDeduction','PfmsLoanApplicationByEmployee', 'PfmsEmpMembership','PfmsEmpMembershipFormListByEmployee','PfmsEmpMembershipListByEmployee','PfmsLoanSchedule','PfmsLoanApplication', 'User', 'Principal', 'DateUtils',
        function($scope, $filter, $rootScope, $stateParams, $state, entity, PfmsDeduction, PfmsLoanApplicationByEmployee, PfmsEmpMembership, PfmsEmpMembershipFormListByEmployee, PfmsEmpMembershipListByEmployee,PfmsLoanSchedule, PfmsLoanApplication, User, Principal, DateUtils) {

            $scope.pfmsDeduction = entity;
            $scope.hremployeeinfos = [];
            $scope.pfmsEmpMembership = null;
            $scope.pfmsloanapplications = [];
            $scope.pfmsLoanScheduleList = [];

            PfmsLoanApplication.query(function(result){
                angular.forEach(result,function(dtoInfo){
                    if(dtoInfo.approvalStatus == 'Approved' && dtoInfo.settlementStatus == 'N' && dtoInfo.isDisburseLoan == false){
                        $scope.hremployeeinfos.push(dtoInfo.employeeInfo);
                    }
                });
            });

            $scope.loadPfmsLoanApp = function(empInfo){
                $scope.pfmsloanapplications = [];
                PfmsLoanApplicationByEmployee.query({employeeId: empInfo.id},function(result){
                    angular.forEach(result,function(dtoInfo){
                        if(dtoInfo.approvalStatus == 'Approved' && dtoInfo.settlementStatus == 'N' && dtoInfo.isDisburseLoan == false){
                            $scope.pfmsloanapplications.push(dtoInfo);
                        }
                    });
                });

            };

            $scope.loadEmployeeInfo = function(){
                $scope.empInfo = $scope.pfmsDeduction.employeeInfo;
                console.log($scope.empInfo);
                $scope.designationName          = $scope.empInfo.designationInfo.designationInfo.designationName;
                $scope.departmentName           = $scope.empInfo.departmentInfo.departmentInfo.departmentName;
                $scope.dutySide                 = $scope.empInfo.workArea.typeName;
                $scope.nationality              = $scope.empInfo.nationality;
                $scope.fatherName               = $scope.empInfo.fatherName;
                $scope.birthDate                = $scope.empInfo.birthDate;
                console.log('Employee Date of Birth is :');
                console.log($scope.birthDate);
                $scope.dateOfBirth = $filter('date')(new Date($scope.birthDate), 'dd/MM/yyyy');
                $scope.loadPfAccountNo();
                $scope.loadPfmsLoanApp($scope.empInfo);
            };

            /*------------Employee Age calculation--------------------*/

            $scope.employeeAgeNow=function () {
                var date = $filter('date')(new Date($scope.birthDate), 'yyyy/MM/dd');
                var today =$filter('date')(new Date(), 'yyyy/MM/dd');
                var year = new Date().getFullYear();
                var month = new Date().getMonth() + 1;
                var day = new Date().getDate();
                var time=new Date().getTime();
                date = date.split('/');
                today=today.split('/');
                var yy = parseInt(date[0]);
                var mm = parseInt(date[1]);
                var dd = parseInt(date[2]);
                var years, months, days;
                // months
                months = month - mm;
                if (day < dd) {
                    months = months - 1;
                }
                // years
                years = year - yy;
                if (month * 100 + day < mm * 100 + dd) {
                    years = years - 1;
                    months = months + 12;
                }
                // days
                $scope.days = Math.floor((time - (new Date(yy + years, mm + months - 1, dd)).getTime()) / (24 * 60 * 60 * 1000));
                $scope.ageIs=years+" Years, "+months+" Months, "+$scope.days+" Days"
                // age is:
                console.log('-----Age is Now-------');
                console.log($scope.ageIs);
                var empAge=parseInt(years);
                if(empAge>52){
                    $('#reScheduleDeduction').modal('show');
                }else {
                    console.log('Hello');
                }

            };

            $scope.clear = function(){
                $scope.pfmsDeduction.employeeInfo=null ;
            };

            $scope.deductionContinue =function(){

                $('#reScheduleDeduction').modal('hide');
            };

            /*---------------------------------------------------------*/

            $scope.onChangeLoanApplication = function(){
                $scope.pfmsDeduction.installmentNo = $scope.pfmsDeduction.pfmsLoanApp.noOfInstallment;
                $scope.pfmsDeduction.deductionAmount = $scope.pfmsDeduction.pfmsLoanApp.loanAmount;
                $scope.loadPfmsEmpMembership();
            };

            $scope.monthList = {
                monthOptions: ['January','February','March','April', 'May','June','July','August','September','October','November','December']
            };

            $scope.isExitsData = true;
            //$scope.duplicateCheckByYearAndMonth = function(){
            //    $scope.isExitsData = true;
            //    PfmsDeduction.query(function(result){
            //        angular.forEach(result,function(dtoInfo){
            //            if(dtoInfo.employeeInfo.id == $scope.pfmsDeduction.employeeInfo.id
            //                && dtoInfo.deductionYear == $scope.pfmsDeduction.deductionYear
            //                && dtoInfo.deductionMonth == $scope.pfmsDeduction.deductionMonth){
            //                $scope.isExitsData = false;
            //            }
            //        });
            //    });
            //};

            if($stateParams.id){
                PfmsDeduction.get({id : $stateParams.id}, function(result) {
                    $scope.pfmsDeduction = result;
                    $scope.pfmsDeduction.deductionAmountPrev = $scope.pfmsDeduction.deductionAmount;
                    if(result.employeeInfo.id){
                        $scope.loadEmployeeInfo(result.employeeInfo);
                        $scope.loadPfmsEmpMembership();
                    }
                });
            }


            /*$scope.load = function(id) {
             PfmsDeduction.get({id : id}, function(result) {
             $scope.pfmsDeduction = result;
             console.log(JSON.stringify(result));
             if(result.employeeInfo.id){
             $scope.loadEmployeeInfo(result.employeeInfo);
             $scope.loadPfmsEmpMembership();
             }
             });
             };*/

            $scope.loadPfAccountNo= function()
            {
                PfmsEmpMembershipFormListByEmployee.query({employeeInfoId:  $scope.empInfo.id},function(result){
                    angular.forEach(result,function(dtoInfo){
                        $scope.pfmsDeduction.accountNo = dtoInfo.accountNo;
                    });
                });

            };

            $scope.loadPfmsEmpMembership= function(){

                PfmsEmpMembershipListByEmployee.query({employeeInfoId: $scope.empInfo.id},function(result){
                    angular.forEach(result,function(dtoInfo){
                        $scope.pfmsEmpMembership = dtoInfo;
                    });
                });
            };

            $scope.loggedInUser =   {};
            $scope.getLoggedInUser = function ()
            {
                Principal.identity().then(function (account)
                {
                    User.get({login: account.login}, function (result)
                    {
                        $scope.loggedInUser = result;
                    });
                });
            };
            $scope.getLoggedInUser();

            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";

            var onSaveFinished = function (result) {
                $scope.$emit('stepApp:pfmsDeductionUpdate', result);
                if ($scope.pfmsDeduction.id != null) {
                    console.log($scope.pfmsEmpMembership.curOwnContributeTot+ ' pre '+ $scope.pfmsDeduction.deductionAmountPrev + ' tot '+ ($scope.pfmsEmpMembership.curOwnContributeTot + $scope.pfmsDeduction.deductionAmountPrev) - $scope.pfmsDeduction.deductionAmount);
                    $scope.pfmsEmpMembership.curOwnContributeTot = ($scope.pfmsEmpMembership.curOwnContributeTot + $scope.pfmsDeduction.deductionAmountPrev) - $scope.pfmsDeduction.deductionAmount;
                }else {
                    $scope.pfmsEmpMembership.curOwnContributeTot = $scope.pfmsEmpMembership.curOwnContributeTot - $scope.pfmsDeduction.deductionAmount;
                }

                PfmsEmpMembership.update($scope.pfmsEmpMembership, onSaveFinisedMem);
            };

            var onSaveFinisedMem = function(){
                $scope.isSaving = false;
                $state.go("pfmsDeduction");
            };

            $scope.save = function () {
                $scope.pfmsDeduction.updateBy = $scope.loggedInUser.id;
                $scope.pfmsDeduction.updateDate = DateUtils.convertLocaleDateToServer(new Date());
                if ($scope.pfmsDeduction.id != null) {
                    PfmsDeduction.update($scope.pfmsDeduction, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsDeduction.updated');
                } else {
                    $scope.pfmsDeduction.createBy = $scope.loggedInUser.id;
                    $scope.pfmsDeduction.deductionDate = DateUtils.convertLocaleDateToServer(new Date());
                    $scope.pfmsDeduction.createDate = DateUtils.convertLocaleDateToServer(new Date());
                    PfmsDeduction.save($scope.pfmsDeduction, onSaveFinished);
                    $rootScope.setSuccessMessage('stepApp.pfmsDeduction.created');
                }
            };

            $scope.initiatePfmsLoanScheduleModel = function()
            {
                return {
                    loanYear: null,
                    loanMonth: null,
                    deductedAmount: null,
                    installmentNo: null,
                    totInstallNo: null,
                    totLoanAmount: null,
                    isRepay: false,
                    reason: null,
                    activeStatus: true
                };
            };

        }]);


/*pfmsDeduction-detail.controller.js*/

angular.module('stepApp')
    .controller('PfmsDeductionDetailController', function ($scope, $rootScope, $stateParams, entity, PfmsDeduction, HrEmployeeInfo) {
        $scope.pfmsDeduction = entity;
        $scope.load = function (id) {
            PfmsDeduction.get({id: id}, function(result) {
                $scope.pfmsDeduction = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:pfmsDeductionUpdate', function(event, result) {
            $scope.pfmsDeduction = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
