angular.module('stepApp').controller('ProfessorApplicationControllers',
    ['$scope', '$stateParams', '$state', 'ProfessorApplication', 'Principal', '$rootScope', 'CurrentInstEmployee', 'Attachment',  'InstEmployeeCode','AttachmentCategoryByApplicationName','PrincipleApplicationCheck',
    function ($scope, $stateParams, $state, ProfessorApplication, Principal, $rootScope, CurrentInstEmployee, Attachment,InstEmployeeCode,AttachmentCategoryByApplicationName,PrincipleApplicationCheck) {

        $scope.educationName = ['SSC/Dakhil', 'SSC (Vocational)', 'HSC/Alim', 'HSC (Vocational)', 'HSC (BM)', 'Honors/Degree', 'Masters'];
        $scope.currentSection = 'personal';
        $scope.applicantEducation = [];
        $scope.applicantTraining = [];
        $scope.applicantAttachment = [];
        $scope.applicantEducationCount = [0];
        $scope.applicantAttachmentCount = [0];
        $scope.applicantTrainingCount = [0];
        $scope.attachmentCategories = [];
        $scope.applicantTraining[0] = {};
        $scope.applicantTraining[0].gpa = '';
        $scope.applicantEducation[0] = {};
        $scope.applicantEducation[0].examYear = '';
        $scope.attachmentList = [];
        $scope.mpoAttachmentType = '';
        $scope.mpoApplication = {};
        $scope.search = {};
        $scope.mpoForm = false;
        $scope.showAddMoreButton = false;
        $scope.currentSelectItem = [];
        $scope.mpoFormHasError = true;
        $scope.duplicateError = false;
        $scope.timeScaleApplication = {};
        $scope.noDisciplinaryAction=false;
        $scope.noWorkingBreakClick=false;
        $scope.timeScaleApplication.disciplinaryAction=true;
        $scope.timeScaleApplication.workingBreak=true;
        //TODO: add all validation to back end

        //Conditions to apply for Assistant Professor
        /* 1. must be mpo enlisted
         2.work duration at least 12 years
         3.must have timescale */
        CurrentInstEmployee.get({}, function (res) {
            $scope.employee = res;
            //checking teacher is BM level
            if (res.instLevel.name === 'HSC (BM)' || res.instLevel.name === 'MADRASA (BM)' || res.instLevel.name === 'HSC_BM' || res.instLevel.name === 'MADRASA_BM' || res.instLevel.name === 'Madrasha (BM)' ) {
                //checking teacher is mpo enlisted
                if (res.mpoAppStatus >= 5 && res.mpoActiveDate != null) {
                    if(getYearDifferenceWithCurrentDate(res.mpoActiveDate) >= 12 ){
                    }else{
                        $state.go('employeeInfo.personalInfo');
                        $rootScope.setErrorMessage('You are not eligible to apply. You must have at least 12 years experience.');
                    }
                } else if (res.mpoAppStatus < 5) {
                    $state.go('employeeInfo.personalInfo');
                    $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                } else {
                    $state.go('employeeInfo.personalInfo');
                    $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                }
            }else{
                $state.go('employeeInfo.personalInfo');
                $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
            }
        });

        function getYearDifferenceWithCurrentDate(dateString) {
            var today = new Date();
            var otherDate = new Date(dateString);
            var year = today.getFullYear() - otherDate.getFullYear();
            var m = today.getMonth() - otherDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < otherDate.getDate())) {
                year--;
            }
            return year;
        }
        AttachmentCategoryByApplicationName.get({name: 'Principle-Scale'}, function (result) {
            $scope.attachmentCategoryList = result;
        });

        Principal.identity().then(function (account) {
            $scope.account = account;
            PrincipleApplicationCheck.get({'code': $scope.account.login}, function (res) {
                if(res.id == 0){
                    console.log("No result found");
                }else{
                    if(res.status > 1){
                        $scope.showApplicationButtion = true;
                        $state.go('PrincipalApplicationStatus');
                        $rootScope.setErrorMessage('You have applied already for this Application. Contact with your institute admin.');
                    }
                }
            });
            if ($scope.isInArray('ROLE_ADMIN', $scope.account.authorities)) {
                $scope.showTimeScaleForm();
            }
            else if ($scope.isInArray('ROLE_INSTITUTE', $scope.account.authorities)) {
                $scope.showTimeScaleForm();
            }
            else if ($scope.isInArray('ROLE_INSTEMP', $scope.account.authorities)) {
                $scope.search.code = $scope.account.login;
                $scope.showTimeScaleForm();
            }
        });

        $scope.isInArray = function isInArray(value, array) {
            return array.indexOf(value) > -1;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            $scope.timeScaleApplication.instEmployee = $scope.employee;
            ProfessorApplication.save($scope.timeScaleApplication);

            angular.forEach($scope.applicantAttachmentCount, function (value, key) {
                if ($scope.applicantAttachment[value].attachment != '') {
                    $scope.applicantAttachment[value].instEmployee = {};
                    $scope.applicantAttachment[value].name = $scope.applicantAttachment[value].attachmentCategory.attachmentName;
                    $scope.applicantAttachment[value].instEmployee.id = $scope.employee.id;
                   Attachment.save($scope.applicantAttachment[value]);
                }
            });
            $rootScope.setSuccessMessage('Application Submitted Successfully.');
            $scope.isSaving = false;
            $state.go('PrincipalApplicationStatus', {}, {reload: true});
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.showTimeScaleForm = function () {
            InstEmployeeCode.get({'code': $scope.search.code}, function (res) {
                $scope.employee = res.instEmployee;
                $scope.address = res.instEmpAddress;
                $scope.applicantEducation = res.instEmpEduQualis;
                $scope.applicantTraining = res.instEmplTrainings;
                $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                $scope.instEmplBankInfo = res.instEmplBankInfo;
                $scope.mpoForm = true;
            });
        };

        $scope.hideMpoForm = function () {
            $scope.mpoForm = false;
        };

        $scope.timescaleNext = function (current, next) {
            $scope.currentSection = next;
            $scope.prevSection = current;
        }
        $scope.timescalePrev = function (current, prev) {
            $scope.currentSection = prev;
            $scope.nextSection = current;
        }
        $scope.addMoreEducation = function () {
            $scope.applicantEducationCount.push($scope.applicantEducationCount.length);
            $scope.applicantEducation[$scope.applicantEducationCount.length].examYear = '';

        }
        $scope.addMoreAttachment = function () {
            //removeItem
            if (!inArray($scope.applicantAttachmentCount.length, $scope.applicantAttachmentCount)) {
                $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
            } else {
                $scope.applicantAttachmentCount.length++;
                $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
            }

            $scope.showAddMoreButton = false;
        }
        $scope.removeAttachment = function (attachment) {
            $scope.showAddMoreButton = true;
            $scope.mpoFormHasError = true;
            var index = $scope.applicantAttachmentCount.indexOf(attachment);
            $scope.applicantAttachmentCount.splice(index, 1);

            if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = false;
            }
        }
        $scope.addMoreTraining = function () {
            $scope.applicantTrainingCount.push($scope.applicantTrainingCount.length);
            $scope.applicantTraining[$scope.applicantTrainingCount.length].gpa = '';
        }

        $scope.setAttachment = function($index, attachment,noAttach) {

            if(noAttach ){
                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }else{
                    $scope.showAddMoreButton = true;
                }
            }
            try{
                if(attachment==""){
                    $scope.mpoFormHasError = true;
                }else{
                    if(!noAttach && attachment.file)
                        $scope.showAddMoreButton = true;
                    if(noAttach && (attachment.remarks == undefined || attachment.remarks==""))
                        $scope.showAddMoreButton = true;
                }
                attachment.attachmentCategory = angular.fromJson(attachment.attachment);

                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }

                if(attachment.attachmentCategory.id)
                {
                    $scope.currentSelectItem[$index] = attachment.attachmentCategory.id;
                }
            }catch(e) {
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = true;
                $scope.currentSelectItem.splice($index, 1);
            }

            if($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length){
                $scope.mpoFormHasError = false;
                angular.forEach($scope.applicantAttachment, function (value, key) {

                    if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                        $scope.mpoFormHasError = true;
                    }else {
                        if (value.fileName) {
                            $scope.mpoFormHasError = true;
                        }
                    }

                });
            }
            else{
                angular.forEach($scope.applicantAttachment, function (value, key) {

                    if (!value.noAttachment && (value.fileName)) {
                        $scope.mpoFormHasError = true;
                    }
                });
            }

            if(arrayUnique($scope.currentSelectItem).length != $scope.currentSelectItem.length)
                $scope.duplicateError = true;
            else
                $scope.duplicateError = false;

            if($scope.duplicateError){
                $scope.showAddMoreButton = false;
            }
        }
        $scope.remarksChange = function(noAttachment,remarks)
        {
            if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = false;
                angular.forEach($scope.applicantAttachment, function (value, key) {

                    if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                        $scope.mpoFormHasError = true;
                    }
                });
            }else{
                if(noAttachment && !(remarks == undefined || remarks=="")){
                    $scope.showAddMoreButton = true;
                }else{
                    $scope.showAddMoreButton = false;
                }
            }
        }
        $scope.selectNoAttachment = function(val,val2,file,remarks)
        {
            if(val && val2){
                if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                            $scope.mpoFormHasError = true;
                        }
                    });

                }else{
                    if (val && (remarks == undefined || remarks=="")) {
                        $scope.mpoFormHasError = true;
                        $scope.showAddMoreButton = false;
                    }else{
                        $scope.showAddMoreButton = true;
                    }

                }
            }else{
                if(file==undefined){
                    $scope.mpoFormHasError = true;
                    $scope.showAddMoreButton = false;
                }else{
                    $scope.showAddMoreButton = true;
                }

                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }
            }
            if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                $scope.showAddMoreButton = false;
            }
        }

        function inArray(needle, haystack) {
            var length = haystack.length;
            for (var i = 0; i < length; i++) {
                if (typeof haystack[i] == 'object') {
                    if (arrayCompare(haystack[i], needle)) return true;
                } else {
                    if (haystack[i] == needle) return true;
                }
            }
            return false;
        }

        function arrayUnique(a) {
            return a.reduce(function (p, c) {
                if (p.indexOf(c) < 0) p.push(c);
                return p;
            }, []);
        };

        function arrayCompare(a1, a2) {
            if (a1.length != a2.length) return false;
            var length = a2.length;
            for (var i = 0; i < length; i++) {
                if (a1[i] !== a2[i]) return false;
            }
            return true;
        }

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }
            return formatAsBytes(size(base64String));
        };

        $scope.setFile = function ($file, attachment) {
            $scope.showAddMoreButton = true;
            $scope.mpoFormHasError = true;
            try {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            try {
                                attachment.file = base64Data;
                                attachment.fileContentType = $file.type;
                                attachment.fileName = $file.name;
                            } catch (e) {
                                $scope.showAddMoreButton = false;
                            }
                        });
                    };

                    if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                        $scope.showAddMoreButton = false;
                    }

                    if ($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length)
                        $scope.mpoFormHasError = false;
                    else
                        $scope.mpoFormHasError = true;
                }
            } catch (e) {
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = true;
            }
        };

        $scope.changeActionFile = function ($file, timeScaleApplication) {
            if ($file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($file);
                fileReader.onload = function (e) {
                    var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                    $scope.$apply(function () {
                        $scope.timeScaleApplication.disActionFile = base64Data;
                        $scope.timeScaleApplication.disActionFileType = $file.type;
                        $scope.timeScaleApplication.disActionFileName = $file.name;
                    });
                };
            }
        };
        $scope.disciplinaryActionClick = function(data){
            if(!data){
                $scope.noDisciplinaryAction = true;
                $scope.timeScaleApplication.disActionCaseNo=null;
                $scope.timeScaleApplication.disActionFileName=null;
                $scope.timeScaleApplication.disActionFile=null;
                $scope.timeScaleApplication.resulationDate=null;
                $scope.timeScaleApplication.agendaNumber=null;
            }else{
                $scope.noDisciplinaryAction = false;
            }

        }
        $scope.workingBreakClick = function(data){
            if(!data){
                $scope.noWorkingBreakClick = true;
                $scope.timeScaleApplication.workingBreakStart=null;
                $scope.timeScaleApplication.workingBreakEnd=null;
            }else{
                $scope.noWorkingBreakClick = false;
            }
        }
        var today = new Date();
        $scope.toDay = today;

    }])
.controller('PrincipalApplicationStatusController',
    ['$scope', '$rootScope', '$state', '$stateParams', 'Principal','ProfessorApplicationLogEmployeeCode','PrincipleApplicationCheck',
        function ($scope, $rootScope, $state, $stateParams, Principal,ProfessorApplicationLogEmployeeCode,PrincipleApplicationCheck) {

            Principal.identity().then(function (account) {
                $scope.account = account;
                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    PrincipleApplicationCheck.get({'code': $scope.account.login}, function (res) {
                        if (res.id == 0) {
                            $rootScope.setErrorMessage('You Assistant Principal Application Yet Not Complete. Please Complete your Application Procedure');
                            $state.go('mpo.application');
                        }
                    });
                }
                ProfessorApplicationLogEmployeeCode.get({'code':$scope.account.login}, function(result) {
                    $scope.applicationlogs = result;
                });
            });

        }])
.controller('ProfessorListController',
    ['$scope', 'entity', '$state','Principal', 'InstLevelByName', 'PrincipleApplicationSummaryList', 'PrincipleApplicationsByLevel', 'ProfessorApplication',
        function ($scope, entity, $state, Principal, InstLevelByName, PrincipleApplicationSummaryList, PrincipleApplicationsByLevel, ProfessorApplication) {

            $scope.principleAppliedLists = entity;
            $scope.summaryList = [];
            $scope.employees = [];
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.page = 0;
            $scope.mpoApplicationForMPoMeeting = [];
            $scope.showSummary = false;

            if (entity.pending === "pending" && (Principal.hasAnyAuthority(['ROLE_DIRECTOR']) || Principal.hasAnyAuthority(['ROLE_DG']))) {
                PrincipleApplicationSummaryList.query({}, function (result) {
                    $scope.summaryList = result;
                });
            }

            $scope.createSummary = function () {
                PrincipleApplicationSummaryList.query({}, function (result) {
                    $scope.summaryList = result;
                    if(result.length == 0){
                        alert("No application found!");
                    }
                },function(response) {
                    if(response.status === 404) {
                        alert("No application found!");
                    }
                });
            };

            $scope.all = function () {
                $scope.showSummary = false;
                $scope.principleAppliedLists = entity;
            };

            //$scope.ssc = function () {
            //    $scope.principleAppliedLists = [];
            //    InstLevelByName.get({name: "SSC (VOC)"}, function (result) {
            //        PrincipleApplicationsByLevel.query({status: 0, levelId: result.id}, function (result2) {
            //            $scope.principleAppliedLists = result2;
            //        });
            //    });
            //};
            $scope.hsc = function () {
                $scope.showSummary = false;
                $scope.principleAppliedLists = [];
                InstLevelByName.get({name: "HSC (BM)"}, function (result) {
                    PrincipleApplicationsByLevel.query({status: 0, levelId: result.id}, function (result2) {
                        $scope.principleAppliedLists = result2;
                    });
                });
            };
            //$scope.mdvoc = function () {
            //    $scope.principleAppliedLists = [];
            //    InstLevelByName.get({name: "Madrasha (VOC)"}, function (result) {
            //        PrincipleApplicationsByLevel.query({status: 0, levelId: result.id}, function (result2) {
            //            $scope.principleAppliedLists = result2;
            //        });
            //    });
            //};
            $scope.mdbm = function () {
                $scope.showSummary = false;
                $scope.principleAppliedLists = [];
                InstLevelByName.get({name: "Madrasha (BM)"}, function (result) {
                    PrincipleApplicationsByLevel.query({status: 0, levelId: result.id}, function (result2) {
                        $scope.principleAppliedLists = result2;
                    });
                });
            };

            $scope.forwardSummaryList = function () {
                angular.forEach($scope.summaryList, function (data) {
                    ProfessorApplication.get({id: data.PROFESSOR_AP_ID}, function (result) {
                        result.adForwarded = true;
                        ProfessorApplication.update(result);
                    });
                });
                location.reload();
                $rootScope.setSuccessMessage("Application Forward Successfully");
            };


            $scope.loadPage = function (page) {
                $scope.page = page;
                $scope.loadAll();
            };

            $scope.refresh = function () {
                $scope.loadAll();
                $scope.clear();
            };
            $scope.showSummary1 = function () {
                $scope.showSummary = true;
            };
            $scope.isAllSelected = false;
            $scope.updateAllSelection = function (mpoApp, selectionValue) {
                $scope.mpoApplicationForMPoMeeting = [];
                for (var i = 0; i < mpoApp.length; i++) {
                    mpoApp[i].isSelected = selectionValue;
                    if(mpoApp[i].isSelected == true){
                        $scope.mpoApplicationForMPoMeeting.push(mpoApp[i]);
                    }else {
                        var index = $scope.mpoApplicationForMPoMeeting.indexOf(mpoApp[i]);
                        $scope.mpoApplicationForMPoMeeting.splice(index);
                    }

                }
            };

            $scope.updateSelection = function (mpoApp) {
                if(mpoApp.isSelected == true){
                    $scope.mpoApplicationForMPoMeeting.push(mpoApp);
                }else {
                    var index = $scope.mpoApplicationForMPoMeeting.indexOf(mpoApp);
                    $scope.mpoApplicationForMPoMeeting.splice(index);
                }
            };

            //$scope.clear = function () {
            //    $scope.employee = {
            //        name: null,
            //        nameEnglish: null,
            //        father: null,
            //        mother: null,
            //        presentAddress: null,
            //        permanentAddress: null,
            //        gender: null,
            //        dob: null,
            //        zipCode: null,
            //        registrationCertificateSubject: null,
            //        registrationExamYear: null,
            //        registrationCetificateNo: null,
            //        indexNo: null,
            //        bankName: null,
            //        bankBranch: null,
            //        bankAccountNo: null,
            //        designation: null,
            //        subject: null,
            //        payScale: null,
            //        payScaleCode: null,
            //        month: null,
            //        monthlySalaryGovtProvided: null,
            //        monthlySalaryInstituteProvided: null,
            //        gbResolutionReceiveDate: null,
            //        gbResolutionAgendaNo: null,
            //        circularPaperName: null,
            //        circularPublishedDate: null,
            //        recruitExamDate: null,
            //        recruitApproveGBResolutionDate: null,
            //        recruitPermitAgendaNo: null,
            //        recruitmentDate: null,
            //        presentInstituteJoinDate: null,
            //        presentPostJoinDate: null,
            //        dgRepresentativeType: null,
            //        dgRepresentativeName: null,
            //        dgRepresentativeDesignation: null,
            //        dgRepresentativeAddress: null,
            //        representativeType: null,
            //        boardRepresentativeName: null,
            //        boardRepresentativeDesignation: null,
            //        boardRepresentativeAddress: null,
            //        salaryCode: null,
            //        id: null
            //    };
            //};
            //$scope.areAllEmployeesSelected = false;
        }
    ])
.controller('ProfessorApproveDialogController',
    ['$scope','ProfessorApplication','$rootScope', '$stateParams', '$state', 'Principal','$modalInstance', 'AllForwaringList','ForwardPrincipleApplication',
        function ($scope,ProfessorApplication,$rootScope,$stateParams, $state, Principal, $modalInstance, AllForwaringList,ForwardPrincipleApplication) {

            //TODO: remove comments after testing
            /*  *//*$scope.timescaleApplication = entity;*//*
             console.log($stateParams.apAll);
             $scope.professorApplication = ProfessorApplication.get({id:$stateParams.id});
             $scope.confirmApprove = function(id){
             ProfessorApplication.update($scope.professorApplication, onSaveSuccess, onSaveError);
             }
             var onSaveSuccess = function(result){
             $modalInstance.close();
             $rootScope.setSuccessMessage('Application Approved Successfully.');
             $state.go('professorApplicationDetails',null,{reload: true});
             }
             var onSaveError = function(result){
             console.log(result);
             }
             $scope.clear = function(){
             $modalInstance.close();
             $state.go('professorApplicationDetails');
             }*/

            $scope.approveVal = false;
            $scope.forward = {};
            ProfessorApplication.get({id: $stateParams.id}, function(result){
                $scope.professorApplication = result;
                if(result.instEmployee.instLevel.name === 'MADRASA_BM' || result.instEmployee.instLevel.name === 'HSC_BM' || result.instEmployee.instLevel.name === 'Madrasha (BM)' || result.instEmployee.instLevel.name === 'HSC (BM)'){
                    AllForwaringList.get({type:'BM'}, function(result){
                        $scope.forwardingList=result;
                    });
                }else{
                    AllForwaringList.get({type:'VOC'}, function(result){
                        $scope.forwardingList=result;
                    });
                }

                if(result.status > 6){
                    $scope.approveVal = true;
                };

            });


            $scope.confirmApprove = function(forward){

                if($scope.professorApplication.status > 6){
                    if(Principal.hasAnyAuthority(['ROLE_DG'])){
                        $scope.professorApplication.dgFinalApproval=true;
                    }
                    if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                        $scope.professorApplication.directorComment=$scope.remark;
                    }
                    ProfessorApplication.update($scope.professorApplication, onSaveSuccess, onSaveError);
                }else{
                    ForwardPrincipleApplication.forward({forwardTo:$scope.forward.forwardTo.code,cause: $scope.remark,memoNo: $scope.professorApplication.memoNo},$scope.professorApplication, onSaveSuccess, onSaveError);

                }

            };

            var onSaveSuccess = function(result){
                $modalInstance.close();
                $rootScope.setSuccessMessage('Application Approved Successfully.');
                $state.go('professorApplicationDetails', null, { reload: true });
            }
            var onSaveError = function(result){
            }
            $scope.clear = function(){
                $modalInstance.close();
                $state.go('professorApplicationDetails');
            }

        }])
    .controller('ProfessorDetailsController',
    ['$scope', '$state', '$modal', 'entity','Principal','BEdApplicationCheck','InstEmployeeCode','$stateParams', 'MpoApplicationLogEmployeeCode','AttachmentByName','$rootScope','AttachmentByEmployeeAndName','ProfessorApplication','PrincipleApplicationCheck',
        function ($scope, $state, $modal,entity,Principal,BEdApplicationCheck,InstEmployeeCode,$stateParams, MpoApplicationLogEmployeeCode,AttachmentByName,$rootScope,AttachmentByEmployeeAndName,ProfessorApplication,PrincipleApplicationCheck) {
            $scope.url = null;
            $scope.employee = entity;
            $scope.isCollapsed = false;
            $scope.showApproveButton=false;
            $scope.showApplicationButtion=true;

            $scope.checkStatus = function(){
                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    return 1;
                }else if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                    return 2;
                }else if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                    return 3;
                }else if(Principal.hasAnyAuthority(['ROLE_DEO'])){
                    return 4;
                }else if(Principal.hasAnyAuthority(['ROLE_FRONTDESK'])){
                    return 5;
                }else if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])){
                    return 6;
                }else if(Principal.hasAnyAuthority(['ROLE_AD'])){
                    return 7;
                }else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                    return 9;
                }else if(Principal.hasAnyAuthority(['ROLE_DG'])){
                    return 10;
                }else if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                    return 11;
                }else{
                    return 0;
                }
            };
            if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                Principal.identity().then(function (account) {
                    $scope.account = account;
                    InstEmployeeCode.get({'code':$scope.account.login},function(res){
                        $scope.employee = res.instEmployee;
                        PrincipleApplicationCheck.get({'code':$scope.employee.code}, function(result) {
                            $scope.professorApplication = result;
                            if(result.id>0){
                                $scope.showApplicationButtion=false;
                            }else{
                                $rootScope.setErrorMessage("You Did not complete your Assistant Professor Application. Please Complete Your Application If You are Eligible.");
                                $state.go('mpo.application');
                            }
                            $scope.employee.id = res.instEmployee.id;
                            $scope.address = res.instEmpAddress;
                            $scope.applicantEducation = res.instEmpEduQualis;
                            $scope.applicantTraining = res.instEmplTrainings;
                            $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                            $scope.instEmplBankInfo = res.instEmplBankInfo;

                            AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"Principle-Scale"}, function(result){
                                $scope.attachments = result;
                            });
                        });
                    });
                });
            }else{
                ProfessorApplication.get({id:$stateParams.id}, function(result) {
                    console.log('%%%%%%%%%%%%%%%5555');
                    $scope.apAll=result;
                    $scope.professorApplication = result;
                    if($stateParams.id > 0){
                        $scope.showApplicationButtion=false;
                    }
                    if( $scope.checkStatus()!= 0 && $scope.checkStatus() == result.status){

                        $scope.showApproveButton=true;
                        $scope.mpoId = result.id;
                    }else{
                        $scope.showApproveButton=false;
                    }

                    InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                        $scope.employee = res.instEmployee;
                        $scope.employee.id = res.instEmployee.id;
                        $scope.address = res.instEmpAddress;
                        $scope.applicantEducation = res.instEmpEduQualis;
                        $scope.applicantTraining = res.instEmplTrainings;
                        $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                        $scope.instEmplBankInfo = res.instEmplBankInfo;
                        AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"Principle-Scale"}, function(result){
                            $scope.attachments = result;
                        });
                        MpoApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                            $scope.mpoApplicationlogs = result;
                        });
                    });

                });
            }

            $scope.educations = function(){
                $scope.applicantEducations = $scope.applicantEducation;
            };

            $scope.displayAttachment= function(applicaiton) {
                AttachmentByName.get({id:applicaiton.id},applicaiton,function(response){
                    var blob = b64toBlob(response.file, applicaiton.fileContentType);
                    $scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
                });
            }

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {type: contentType});
                return blob;
            }
            $scope.previewImage = function (content, contentType,name) {
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                // call the modal
                $rootScope.showFilePreviewModal();
            };

        }])
    .controller('ProfessorDenyDialogController',
        ['$scope', '$stateParams', 'DateUtils', '$state', '$modalInstance', 'PrincipleApplicationDecline',
            function ($scope,$stateParams, DateUtils, $state, $modalInstance,PrincipleApplicationDecline) {

                $scope.causeDeny = "";

                $scope.confirmDecline = function(){

                    PrincipleApplicationDecline.decline({id: $stateParams.id, cause: $scope.causeDeny},{}, onSaveSuccess, onSaveError);

                };
                var onSaveSuccess = function(result){
                    $modalInstance.close();
                    console.log('@@@@@@@@@ principalPendingList @@@@@@@@@@');
                    $state.go('principalPendingList',{reload:true});
                    console.log('############principalPendingList ############');
                };
                var onSaveError = function(result){
                    console.log(result);
                };
                $scope.clear = function(){
                    $modalInstance.close();
                    window.history.back();
                };

            }])
    .controller('ProfessorApplicationCheckListController',
        ['$scope', '$rootScope','ProfessorApplicationLogEmployeeCode', '$state', '$stateParams', 'InstEmployeeCode' ,
            function ($scope, $rootScope,ProfessorApplicationLogEmployeeCode, $state, $stateParams,InstEmployeeCode) {

                $scope.showSearchForm = true;
                $scope.principleApplicationlogs = [];
                ProfessorApplicationLogEmployeeCode.get({'code':$stateParams.code}, function(result) {
                    $scope.principleApplicationlogs = result;
                });
            }]);

