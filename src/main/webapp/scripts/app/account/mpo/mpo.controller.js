'use strict';

angular.module('stepApp')
    .controller('MpoController',
     ['$scope', '$state', 'PayScale', 'Principal', 'ParseLinks', 'Institute', 'User', 'Upazila', 'Course',  'DataUtils', 'StaffCount','InstituteByLogin','CurrentInstEmployee',
     function ($scope, $state, PayScale, Principal, ParseLinks, Institute, User, Upazila, Course, DataUtils, StaffCount,InstituteByLogin,CurrentInstEmployee) {

        $scope.calendar = {
            opened: {},
            dateFormat: 'yyyy-MM-dd',
            dateOptions: {},
            open: function($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };
         $scope.showBmMenu = true;

         if(Principal.hasAnyAuthority(['ROLE_INSTITUTE']) || Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
             InstituteByLogin.query({}, function(result){
                 $scope.logInInstitute = result;
                 if(result.instLevel.name === 'SSC (VOC)' || result.instLevel.name === 'Madrasha (VOC)'){
                     console.log("Institute is voc");
                     $scope.showBmMenu = false;
                 }

             });
         }

        CurrentInstEmployee.get({},function(result){
            $scope.CurrentInstEmployee = result;
        });

        Principal.identity().then(function (account) {
            $scope.settingsAccount = account;
        });

        $scope.page = 0;

        $scope.trainings = function () {
            //Training.query({page: $scope.page, size: 10}, function (result, headers) {
            //    $scope.links = ParseLinks.parse(headers('link'));
            //    $scope.trainings = result;
            //    $scope.total = headers('x-total-count');
            //});
        };

        $scope.educations = function () {
            //ApplicantEducation.query({page: $scope.page, size: 10}, function (result, headers) {
            //    $scope.links = ParseLinks.parse(headers('link'));
            //    $scope.applicantEducations = result;
            //    $scope.total = headers('x-total-count');
            //});
        };

        $scope.payScales = function () {
            PayScale.query({page: $scope.page, size: 10}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.payScales = result;
                $scope.total = headers('x-total-count');
            });
        };

        //$scope.employees = function () {
        //    Employee.query({page: $scope.page, size: 10}, function (result, headers) {
        //        $scope.links = ParseLinks.parse(headers('link'));
        //        $scope.employees = result;
        //        $scope.total = headers('x-total-count');
        //    });
        //};

        $scope.staffCount = function () {
            StaffCount.query({page: $scope.page, size: 10}, function (result, headers) {
                $scope.staffCounts = result;
            });
        };
        // institute operations
        $scope.institute = function () {
            Institute.get({id: 'my'}, function (result) {
                $scope.institute = result;
            });
        };

        $scope.users = User.query();
        $scope.upazilas = Upazila.query({size: 500});
        $scope.courses = Course.query();

        $scope.saveInstitute = function () {
            $scope.isSaving = true;
            if ($scope.institute.id != null) {
                Institute.update($scope.institute, onSaveSuccess, onSaveError);
            } else {
                Institute.save($scope.institute, onSaveSuccess, onSaveError);
            }
            $state.go('mpo', {reload: true});
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('stepApp:instituteUpdate', result);
            $scope.isSaving = false;
        };
        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;

        $scope.setLastCommitteeApprovedFile = function ($file, institute) {
            if ($file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($file);
                fileReader.onload = function (e) {
                    var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                    $scope.$apply(function () {
                        institute.lastCommitteeApprovedFile = base64Data;
                        institute.lastCommitteeApprovedFileContentType = $file.type;
                    });
                };
            }
        };

        $scope.setLastCommittee1stMeetingFile = function ($file, institute) {
            if ($file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($file);
                fileReader.onload = function (e) {
                    var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                    $scope.$apply(function () {
                        institute.lastCommittee1stMeetingFile = base64Data;
                        institute.lastCommittee1stMeetingFileContentType = $file.type;
                    });
                };
            }
        };

    }]);


/*mpo-salary-process.controller.js*/
angular.module('stepApp')
    .controller('MpoSalaryProcessController',
        ['$scope', '$state','MpoSalaryMonthList','MpoSalaryYearList','SalaryGeneratedForYearAndMonth','$rootScope',
            function ($scope, $state,MpoSalaryMonthList,MpoSalaryYearList,SalaryGeneratedForYearAndMonth,$rootScope) {

                $scope.yearList=MpoSalaryYearList.get({},function(result){
                });
                $scope.monthList=MpoSalaryMonthList.get({});

                $scope.checkMonthAndSalary = function(val,val2)
                {
                    SalaryGeneratedForYearAndMonth.get({year: val,'month': val2},function(result){
                        $rootScope.confirmationObject.pageTitle = 'Process Confirmation';
                        $rootScope.confirmationObject.pageTexts = 'Requested Salary Process Executed';
                        $rootScope.showConfirmation();
                    });
                }

            }]);


/*mpo-salary-disburse.controller.js */
angular.module('stepApp')
    .controller('MpoSalaryDisburseController',
        ['$scope', '$state','MpoSalaryMonthList','MpoSalaryYearList','SalaryDisburseForYearAndMonth','$rootScope',
            function ($scope, $state,MpoSalaryMonthList,MpoSalaryYearList,SalaryDisburseForYearAndMonth,$rootScope) {

                $scope.yearList=MpoSalaryYearList.get({},function(result){
                });
                $scope.monthList=MpoSalaryMonthList.get({});
                $scope.checkMonthAndSalary = function(val,val2)
                {
                    SalaryDisburseForYearAndMonth.get({year: val,'month': val2},function(result){

                        $rootScope.confirmationObject.pageTitle = 'Disburse Confirmation';
                        $rootScope.confirmationObject.pageTexts = 'Requested Salary Disburse Executed';
                        $rootScope.showConfirmation();
                    });
                }
            }]);

/* new-application.controller.js*/

angular.module('stepApp').controller('NewApplicationController',
    ['$scope', '$stateParams', '$state', 'entity','Principal','$rootScope','AttachmentByEmployeeAndName','CurrentInstEmployee', 'Attachment', 'MpoApplication', 'InstEmployeeCode','MpoApplicationCheck','AttachmentCatByAppNameAndDesignation',
        function ($scope, $stateParams, $state, entity, Principal,$rootScope,AttachmentByEmployeeAndName,CurrentInstEmployee, Attachment, MpoApplication, InstEmployeeCode,MpoApplicationCheck,AttachmentCatByAppNameAndDesignation) {

            $scope.educationName = ['SSC/Dakhil', 'SSC (Vocational)', 'HSC/Alim', 'HSC (Vocational)', 'HSC (BM)', 'Honors/Degree', 'Masters'];
            $scope.currentSection = 'personal';
            $scope.applicantEducation = [];
            $scope.applicantTraining = [];
            $scope.applicantAttachment = [];
            $scope.applicantEducationCount = [0];
            $scope.applicantAttachmentCount = [0];
            $scope.applicantTrainingCount = [0];
            $scope.attachmentCategories = [];
            $scope.applicantTraining[0]={};
            $scope.applicantTraining[0].gpa = '';
            $scope.applicantEducation[0] ={};
            $scope.applicantEducation[0].examYear = '';
            $scope.attachmentList = [];
            $scope.mpoAttachmentType ='';
            $scope.mpoApplication ={};
            $scope.search = {};
            $scope.mpoForm = false;
            $scope.showAddMoreButton = false;
            $scope.currentSelectItem = [];
            $scope.mpoFormHasError = true;
            $scope.duplicateError = false;
            $scope.mpoFormEditMode = false;

            CurrentInstEmployee.get({},function(res){
                AttachmentCatByAppNameAndDesignation.query({name:'MPO-Enlisting', designationId:res.designationSetup.id}, function (result){
                   $scope.attachmentCategoryList = result;
                });
                if(res.mpoAppStatus == 2){
                    console.log("Eligible to apply");
                }else if(res.mpoAppStatus == 1){
                    AttachmentByEmployeeAndName.query({id: res.id, applicationName:"MPO-Enlisting"}, function(result){
                        $scope.mpoFormHasError = false;
                        $scope.mpoFormEditMode = true;
                    });
                }else if(res.mpoAppStatus < 1){
                    $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                    $state.go('employeeInfo.personalInfo');
                }else{
                    $rootScope.setErrorMessage('You have applied already for this Application. Contact with your institute admin.');
                }
            });
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };
            $scope.employee = entity;

            Principal.identity().then(function (account) {
                $scope.account = account;
                if($scope.isInArray('ROLE_ADMIN', $scope.account.authorities))
                {
                    $scope.showTimeScaleForm();
                }
                else if($scope.isInArray('ROLE_INSTITUTE', $scope.account.authorities))
                {
                    $scope.showTimeScaleForm();
                }
                else if($scope.isInArray('ROLE_INSTEMP', $scope.account.authorities))
                {
                    $scope.search.code = $scope.account.login;
                    $scope.showTimeScaleForm();
                }

            });

            $scope.isInArray = function isInArray(value, array) {
                return array.indexOf(value) > -1;
            };
            //$scope.load = function (id) {
                //Employee.get({id: id}, function (result) {
                //    $scope.employee = result;
                //});
            //};
            $scope.save = function () {
                MpoApplicationCheck.get({'code':$scope.account.login},function(res){
                    if(res.status > 2){
                        $state.go('mpo.employeeTrack',{},{reload:true});
                    }
                });
                //mpoApplication entry
                $scope.isSaving = true;
                $scope.mpoApplication.name = 'MPO-Enlisting';
                $scope.mpoApplication.mpoApplicationDate = new Date();
                $scope.mpoApplication.status = 1;
                $scope.mpoApplication.instEmployee = $scope.employee;
                MpoApplication.save($scope.mpoApplication);

                //employee attachment
                angular.forEach($scope.applicantAttachmentCount, function (value, key) {
                    if ($scope.applicantAttachment[value].attachment != '') {
                        $scope.applicantAttachment[value].instEmployee = {};
                        $scope.applicantAttachment[value].name = $scope.applicantAttachment[value].attachmentCategory.attachmentName;
                        $scope.applicantAttachment[value].instEmployee.id = $scope.employee.id;
                        Attachment.save($scope.applicantAttachment[value]);
                    }
                });
                //$scope.$emit('stepApp:mpoApplicationUpdate', result);
                $rootScope.setSuccessMessage('Application Submitted Successfully.');
                $scope.isSaving = false;
                $state.go('mpo.employeeTrack',{},{reload:true});
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.showTimeScaleForm = function () {
                InstEmployeeCode.get({'code':$scope.search.code},function(res){
                    $scope.employee = res.instEmployee;
                    if((res.instEmployee.category === 'Teacher' && res.instEmpEduQualis.length < 2) || res.instEmpAddress == null || res.instEmplRecruitInfo == null || res.instEmplBankInfo == null){

                       //recruit information is mandatory for mpo application
                        if(res.instEmplRecruitInfo == null){
                            $rootScope.setErrorMessage('Enrollment Information not found! Please, Complete your Enrollment Information.');
                            $state.go('employeeInfo.recruitmentInfo');
                        }
                        //minimum 2 certificate exam is needed for teacher
                        if(res.instEmployee.category === 'Teacher' && res.instEmpEduQualis.length < 2){
                            $rootScope.setErrorMessage('Educational Qualifications not found! Please, Complete your educational qualifications.');
                            $state.go('employeeInfo.educationalHistory');
                        }
                        //personal information is mandatory for mpo application
                        if(res.instEmpAddress == null || res.instEmplBankInfo == null){
                            $rootScope.setErrorMessage('Personal Information not found! Please, Complete your Personal Information.');
                            $state.go('employeeInfo.personalInfo');
                        }
                    }

                    if(res.instEmployee.mpoAppStatus == 2){
                        console.log("Eligible to apply");
                    }else if(res.instEmployee.mpoAppStatus == 1){
                        console.log("apply after decline");
                    }else if(res.instEmployee.mpoAppStatus < 1){
                        $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                        $state.go('employeeInfo.personalInfo');
                    }else{
                        console.log("already applied!");
                        $state.go('mpo.employeeTrack');
                    }
                    $scope.address = res.instEmpAddress;
                    $scope.applicantEducation = res.instEmpEduQualis;
                    $scope.applicantTraining = res.instEmplTrainings;
                    $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                    $scope.instEmplBankInfo = res.instEmplBankInfo;
                    $scope.mpoForm = true;
                });
            };

            $scope.hideMpoForm = function () {
                $scope.mpoForm = false;
            };

            $scope.mpoNext = function (current, next) {
                $scope.currentSection = next;
                $scope.prevSection = current;
            }
            $scope.mpoPrev = function (current, prev) {
                $scope.currentSection = prev;
                $scope.nextSection = current;
            }
            $scope.addMoreEducation = function () {
                $scope.applicantEducationCount.push($scope.applicantEducationCount.length);
                $scope.applicantEducation[$scope.applicantEducationCount.length].examYear = '';
            }
            $scope.addMoreAttachment = function () {
                //removeItem
                if(!inArray($scope.applicantAttachmentCount.length, $scope.applicantAttachmentCount)) {
                    $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
                } else {
                    $scope.applicantAttachmentCount.length++;
                    $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
                }
                $scope.showAddMoreButton = false;
            }
            $scope.removeAttachment = function(attachment) {
                $scope.showAddMoreButton = true;
                $scope.mpoFormHasError = true;
                var index =  $scope.applicantAttachmentCount.indexOf(attachment);
                $scope.applicantAttachmentCount.splice(index,1);

                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length-1) {
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = false;
                }
            }
            $scope.addMoreTraining = function () {
                $scope.applicantTrainingCount.push($scope.applicantTrainingCount.length);
                $scope.applicantTraining[$scope.applicantTrainingCount.length].gpa = '';
            }

            $scope.educationHtml = function () {

            }

            $scope.setAttachment = function($index, attachment,noAttach) {
                console.log(attachment);
                if(noAttach ){
                    if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                        $scope.showAddMoreButton = false;
                    }else{
                        $scope.showAddMoreButton = true;
                    }

                    if($scope.mpoFormEditMode) $scope.mpoFormHasError = false;

                }
                try{
                    if(attachment==""){
                        $scope.mpoFormHasError = true;
                    }else{
                        if(!noAttach && attachment.file)
                            $scope.showAddMoreButton = true;
                        if(noAttach && (attachment.remarks == undefined || attachment.remarks==""))
                            $scope.showAddMoreButton = true;
                    }
                    attachment.attachmentCategory = angular.fromJson(attachment.attachment);

                    if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                        $scope.showAddMoreButton = false;
                    }

                    if(attachment.attachmentCategory.id)
                    {
                        $scope.currentSelectItem[$index] = attachment.attachmentCategory.id;
                    }
                    if($scope.mpoFormEditMode) $scope.mpoFormHasError = false;

                }catch(e) {
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = true;
                    $scope.currentSelectItem.splice($index, 1);
                }
                if($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length){
                    $scope.mpoFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        console.log(value);

                        if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                            $scope.mpoFormHasError = true;
                        }else {
                            if (value.fileName) {
                                $scope.mpoFormHasError = true;
                            }
                        }

                    });
                }
                else{
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        console.log(value);

                        if (!value.noAttachment && (value.fileName)) {
                            $scope.mpoFormHasError = true;
                        }
                    });
                }

                if($scope.mpoFormEditMode) $scope.mpoFormHasError = false;

                if(arrayUnique($scope.currentSelectItem).length != $scope.currentSelectItem.length)
                    $scope.duplicateError = true;
                else
                    $scope.duplicateError = false;

                if($scope.duplicateError){
                    $scope.showAddMoreButton = false;
                }
            }

            $scope.selectNoAttachment = function(val,val2,file)
            {

                if(val && val2){
                    if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){

                        $scope.showAddMoreButton = false;
                        $scope.mpoFormHasError = false;
                        angular.forEach($scope.applicantAttachment, function (value, key) {

                            if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                                $scope.mpoFormHasError = true;
                            }
                        });

                    }else{
                        $scope.showAddMoreButton = true;
                    }
                }else{
                    if(file==undefined){
                        $scope.mpoFormHasError = true;
                    }
                    if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                        $scope.showAddMoreButton = false;
                    }
                }

                if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }
            }
            $scope.remarksChange = function(val,val2)
            {
                if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {

                        if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                            $scope.mpoFormHasError = true;
                        }
                    });

                }

                if($scope.mpoFormEditMode) $scope.mpoFormHasError = false;
            }

            function inArray(needle, haystack) {
                var length = haystack.length;
                for(var i = 0; i < length; i++) {
                    if(typeof haystack[i] == 'object') {
                        if(arrayCompare(haystack[i], needle)) return true;
                    } else {
                        if(haystack[i] == needle) return true;
                    }
                }
                return false;
            }

            function arrayUnique(a) {
                return a.reduce(function(p, c) {
                    if (p.indexOf(c) < 0) p.push(c);
                    return p;
                }, []);
            };

            function arrayCompare(a1, a2) {
                if (a1.length != a2.length) return false;
                var length = a2.length;
                for (var i = 0; i < length; i++) {
                    if (a1[i] !== a2[i]) return false;
                }
                return true;
            }

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }
                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }
                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }
                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };

            $scope.setFile = function ($file, attachment) {
                $scope.showAddMoreButton = true;
                $scope.mpoFormHasError = true;
                try{
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function() {
                                try{
                                    attachment.file = base64Data;
                                    attachment.fileContentType = $file.type;
                                    attachment.fileName = $file.name;
                                }catch(e) {
                                    $scope.showAddMoreButton = false;
                                }
                            });
                        };
                        if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                            $scope.showAddMoreButton = false;
                        }
                        if($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length)
                            $scope.mpoFormHasError = false;
                        else
                            $scope.mpoFormHasError = true;

                        if($scope.mpoFormEditMode) $scope.mpoFormHasError = false;
                    }
                }catch(e) {
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = true;

                    if($scope.mpoFormEditMode) $scope.mpoFormHasError = false;
                }
            };
            $scope.previewImage = function (content, contentType,name)
            {
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                // call the modal
                $rootScope.showFilePreviewModal();
            };
            $scope.cancel = function () {
                $state.go('employeeInfo.dashboard');
            };
        }]);

/* mpo-list.controller.js*/
angular.module('stepApp')
    .controller('MPOListController',
        ['$scope', '$state', 'Principal', 'MpoApplication', 'ParseLinks',
            function ($scope, $state, Principal, MpoApplication , ParseLinks) {

                $scope.employees = [];
                $scope.page = 0;
                $scope.showApproveForward = true;

                if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                    $scope.showApproveForward = false;
                }

                // MpoApplication.query({page: $scope.page, size: 100}, function(result, headers) {
                //     $scope.mpoApplications = result;
                //
                // });

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                // $scope.search = function () {
                //     EmployeeSearch.query({query: $scope.searchQuery}, function(result) {
                //         $scope.employees = result;
                //     }, function(response) {
                //         if(response.status === 404) {
                //             $scope.loadAll();
                //         }
                //     });
                // };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.employee = {
                        name: null,
                        nameEnglish: null,
                        father: null,
                        mother: null,
                        presentAddress: null,
                        permanentAddress: null,
                        gender: null,
                        dob: null,
                        zipCode: null,
                        registrationCertificateSubject: null,
                        registrationExamYear: null,
                        registrationCetificateNo: null,
                        indexNo: null,
                        bankName: null,
                        bankBranch: null,
                        bankAccountNo: null,
                        designation: null,
                        subject: null,
                        payScale: null,
                        payScaleCode: null,
                        month: null,
                        monthlySalaryGovtProvided: null,
                        monthlySalaryInstituteProvided: null,
                        gbResolutionReceiveDate: null,
                        gbResolutionAgendaNo: null,
                        circularPaperName: null,
                        circularPublishedDate: null,
                        recruitExamDate: null,
                        recruitApproveGBResolutionDate: null,
                        recruitPermitAgendaNo: null,
                        recruitmentDate: null,
                        presentInstituteJoinDate: null,
                        presentPostJoinDate: null,
                        dgRepresentativeType: null,
                        dgRepresentativeName: null,
                        dgRepresentativeDesignation: null,
                        dgRepresentativeAddress: null,
                        representativeType: null,
                        boardRepresentativeName: null,
                        boardRepresentativeDesignation: null,
                        boardRepresentativeAddress: null,
                        salaryCode: null,
                        id: null
                    };
                };

                // bulk operations start
                $scope.areAllEmployeesSelected = false;

                // $scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                //     for (var i = 0; i < employeeArray.length; i++)
                //     {
                //         employeeArray[i].isSelected = selectionValue;
                //     }
                // };
                //
                // $scope.import = function (){
                //     for (var i = 0; i < $scope.employees.length; i++){
                //         var employee = $scope.employees[i];
                //         if(employee.isSelected){
                //             //Employee.update(employee);
                //             //TODO: handle bulk export
                //         }
                //     }
                // };
                //
                // $scope.export = function (){
                //     for (var i = 0; i < $scope.employees.length; i++){
                //         var employee = $scope.employees[i];
                //         if(employee.isSelected){
                //             //Employee.update(employee);
                //             //TODO: handle bulk export
                //         }
                //     }
                // };
                //
                // $scope.deleteSelected = function (){
                //     for (var i = 0; i < $scope.employees.length; i++){
                //         var employee = $scope.employees[i];
                //         if(employee.isSelected){
                //             Employee.delete(employee);
                //         }
                //     }
                // };
                //
                // $scope.sync = function (){
                //     for (var i = 0; i < $scope.employees.length; i++){
                //         var employee = $scope.employees[i];
                //         if(employee.isSelected){
                //             Employee.update(employee);
                //         }
                //     }
                // };
                //
                // $scope.order = function (predicate, reverse) {
                //     $scope.predicate = predicate;
                //     $scope.reverse = reverse;
                //     Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                //         $scope.links = ParseLinks.parse(headers('link'));
                //         $scope.employees = result;
                //         $scope.total = headers('x-total-count');
                //     });
                // };
                //
                // $scope.committeeAgree = function (id) {
                //     console.log('asdjlkfdaskljfdsakjl;fdsa comendene');
                // };
                // bulk operations end
            }]);
/* mpo-details.controller.js*/
angular.module('stepApp')
    .controller('MPODetailsController',
        ['$scope', '$state', 'entity','AttachmentByEmployeeAndName', 'InstEmployeeCode','MpoApplication','$stateParams','MpoApplicationCheck', 'MpoApplicationLogEmployeeCode','AttachmentByName','$rootScope','AllInstInfraInfo', 'InstituteAllInfo','InstInfraInfoTempAll','Principal','InstVacancyTemp','HrDesignationSetupByType','InstVacancyTempByInstitute','MpoAppSummaryListByInstituteAndMonth','MpoUpdateApplication','InstEmployeeByInstitute',
            function ($scope, $state, entity,AttachmentByEmployeeAndName,InstEmployeeCode,MpoApplication,$stateParams,MpoApplicationCheck, MpoApplicationLogEmployeeCode,AttachmentByName,$rootScope, AllInstInfraInfo, InstituteAllInfo,InstInfraInfoTempAll,Principal,InstVacancyTemp,HrDesignationSetupByType,InstVacancyTempByInstitute,MpoAppSummaryListByInstituteAndMonth,MpoUpdateApplication,InstEmployeeByInstitute) {
                // $scope.url = null;
                $scope.employee = entity;
                $scope.isCollapsed = false;
                $scope.showApproveButton=false;
                $scope.showApplicationButtion=true;
                $scope.showApprove=false;
                $scope.finalApproved=false;
                $scope.mpoApplicationlogs = {};
                $scope.mpoVacancyRoleDesgnations = [];
                $scope.hrDesignationSetups = [];
                $scope.OthersInfoEdit = false;
                $scope.institute = {};
                $scope.othersInfoVerify = {};
                $scope.othersInfoVerify.mcValidity = "";
                $scope.othersInfoVerify.newsAddDate = "";
                $scope.othersInfoVerify.recruitAppResol = "";
                $scope.othersInfoVerify.recruitExamTabSheet = "";
                $scope.othersInfoVerify.mpoSheet = "";
                $scope.othersInfoVerify.criminalCase = "";
                $scope.othersInfoVerify.femaleQuota = "";
                $scope.othersInfoVerify.vacancyAnalysis = "";
                $scope.othersInfoVerify.recommendationOfAO = "";
                $scope.othersInfoVerify.recommendationOfAD = "";
                $scope.mpoApplication = {};
                $scope.editButton = true;
                $scope.ntrcaAttachments = [];
                $scope.instEmpCount = {};
                $scope.instEmpCount.totalMaleTeacher = 0;
                $scope.instEmpCount.totalFemaleTeacher = 0;
                $scope.instEmpCount.totalMaleEmployee = 0;
                $scope.instEmpCount.totalFemaleEmployee = 0;
                //TODO:change return value on MpoApplicationStatusType.java
                $scope.checkStatus = function(){
                    if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                        return 1;
                    }else if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                        return 2;
                    }else if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                        return 3;
                    }else if(Principal.hasAnyAuthority(['ROLE_DEO'])){
                        return 4;
                    }else if(Principal.hasAnyAuthority(['ROLE_FRONTDESK'])){
                        return 5;
                    }else if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])){
                        return 6;
                    }else if(Principal.hasAnyAuthority(['ROLE_AD'])){
                        return 7;
                    }else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                        return 9;
                    }else if(Principal.hasAnyAuthority(['ROLE_DG'])){
                        return 10;
                    }else if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                        return 11;
                    }else{
                        return 0;
                    }
                };

                HrDesignationSetupByType.query({type: 'Non_Govt_Employee', size: 200}, function (result) {
                    $scope.hrDesignationSetups = result;
                });


                $scope.previewImage = function (content, contentType,name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    $rootScope.showFilePreviewModal();
                };
                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    Principal.identity().then(function (account) {
                        $scope.account = account;
                        InstEmployeeCode.get({'code':$scope.account.login},function(res){
                            $scope.employee = res.instEmployee;
                            MpoApplicationCheck.get({'code':$scope.employee.code}, function(result) {
                                if(result.id>0){
                                    $scope.showApplicationButtion=false;
                                }else{
                                    $rootScope.setErrorMessage('Your Mpo Application is not Complete yet.Please CompleteYour Application First.');
                                    $state.go('mpo.application',{},{reload:true});
                                }
                                $scope.employee = res.instEmployee;
                                $scope.employee.id = res.instEmployee.id;
                                $scope.address = res.instEmpAddress;
                                $scope.applicantEducation = res.instEmpEduQualis;
                                $scope.instEmplExperiences = res.instEmplExperiences;
                                $scope.applicantTraining = res.instEmplTrainings;
                                $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                                $scope.instEmplBankInfo = res.instEmplBankInfo;
                                AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"MPO-Enlisting"}, function(result){
                                    $scope.attachments = result;
                                });
                                AttachmentByEmployeeAndName.query({
                                    id: $scope.employee.id,
                                    applicationName: "Employee-Personal-Info"
                                }, function (result) {
                                    $scope.ntrcaAttachments = result;
                                    console.log("Attachments");
                                    console.log($scope.attachments);
                                });
                            });
                        });
                    });
                }else{
                    MpoApplication.get({id:$stateParams.id}, function(result) {
                        $scope.mpoApplication = result;
                        $scope.mpoId = result.id;
                        if(result.status > 6 && result.status < 10){
                            $scope.showApprove = true;
                        }
                        if(result.status > 10){
                            $scope.finalApproved = true;
                        }
                        if($stateParams.id>0){
                            $scope.showApplicationButtion=false;
                        }
                        if( $scope.checkStatus()!=0 & $scope.checkStatus() == result.status){
                            $scope.showApproveButton=true;
                            $scope.mpoId = result.id;
                        }else{
                            $scope.showApproveButton=false;
                        }
                        InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                            $scope.employee = res.instEmployee;
                            $scope.employee.id = res.instEmployee.id;
                            $scope.address = res.instEmpAddress;
                            $scope.instEmplExperiences = res.instEmplExperiences;
                            $scope.applicantEducation = res.instEmpEduQualis;
                            $scope.applicantTraining = res.instEmplTrainings;
                            $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                            $scope.instEmplBankInfo = res.instEmplBankInfo;

                            AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"MPO-Enlisting"}, function(result){
                                $scope.attachments = result;
                            });
                            MpoApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                                $scope.mpoApplicationlogs = result;
                            });
                            InstVacancyTempByInstitute.query({instituteId:$scope.employee.institute.id}, function(instVacantTemp){
                                $scope.mpoVacancyRoleDesgnations = instVacantTemp;
                            });
                            MpoAppSummaryListByInstituteAndMonth.query({mpoApplicationId:$stateParams.id},function(mpoSummaryForAInstitute){
                                $scope.summaryList = mpoSummaryForAInstitute;
                            });
                            if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE']) || Principal.hasAnyAuthority(['ROLE_AD'])){
                                if($scope.mpoApplication.verifyInfoByAO != null){
                                    var res = $scope.mpoApplication.verifyInfoByAO.split("|");
                                    $scope.othersInfoVerify.mcValidity = res[0];
                                    $scope.othersInfoVerify.newsAddDate = res[1];
                                    $scope.othersInfoVerify.recruitAppResol = res[2];
                                    $scope.othersInfoVerify.recruitExamTabSheet = res[3];
                                    $scope.othersInfoVerify.mpoSheet = res[4];
                                    $scope.othersInfoVerify.criminalCase = res[5];
                                    $scope.othersInfoVerify.femaleQuota = res[6];
                                    $scope.othersInfoVerify.vacancyAnalysis = res[7];
                                    $scope.othersInfoVerify.recommendationOfAO = res[8];
                                    $scope.othersInfoVerify.recommendationOfAD = res[9];
                                }
                            }
                        });
                    });
                }


                // $scope.profile = function(){
                //
                // };

                $scope.instituteInformation = function(){
                    AllInstInfraInfo.get({institueid: $scope.employee.institute.id},function(result){
                        $scope.instShopInfos = result.instShopInfoList;
                        $scope.instLabInfos = result.instLabInfoList;
                        $scope.instPlayGroundInfos = result.instPlayGroundInfoList;
                    });

                    InstInfraInfoTempAll.get({institueid: $scope.employee.institute.id},function(res){
                        $scope.instShopInfos = res.instShopInfoList;
                        $scope.instLabInfos = res.instLabInfoList;
                        $scope.instPlayGroundInfos = res.instPlayGroundInfoList;
                    });
                    InstituteAllInfo.get({id: $scope.employee.institute.id}, function (res) {
                        $scope.instGenInfo = res.instGenInfo;
                        $scope.instAdmInfo = res.instAdmInfo;
                        $scope.insAcademicInfo = res.insAcademicInfo;
                        //$scope.instEmpCount = res.instEmpCount;
                        $scope.instFinancialInfos = res.instFinancialInfo;
                        $scope.instInfraInfo = res.instInfraInfo;
                        $scope.instGovernBodys = res.instGovernBody;
                        $scope.instCurriculums = res.instCurriculums;
                        $scope.instCources = res.instCourses;
                        $scope.instituteAllInfo = res;
                    });

                    InstEmployeeByInstitute.query({instituteID:$scope.employee.institute.id},function(result){
                        if(result !=null){
                            $scope.instEmpCount.totalMaleTeacher = 0;
                            $scope.instEmpCount.totalFemaleTeacher = 0;
                            $scope.instEmpCount.totalMaleEmployee = 0;
                            $scope.instEmpCount.totalFemaleEmployee = 0;
                            $scope.instEmpCount.totalEngaged = 0;
                            $scope.instEmpCount.totalGranted = 0;
                            angular.forEach(result,function(value){
                                if(value.category == 'Teacher'){
                                    if(value.gender == 'Male'){
                                        $scope.instEmpCount.totalMaleTeacher = $scope.instEmpCount.totalMaleTeacher + 1;
                                        $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalEngaged + 1;
                                    }else if(value.gender == 'Female'){
                                        $scope.instEmpCount.totalFemaleTeacher = $scope.instEmpCount.totalFemaleTeacher + 1;
                                        $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalEngaged + 1;
                                    }
                                }else{
                                    if(value.gender == 'Male'){
                                        $scope.instEmpCount.totalMaleEmployee = $scope.instEmpCount.totalMaleEmployee + 1;
                                        $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalEngaged + 1;
                                    }else if(value.gender == 'Female'){
                                        $scope.instEmpCount.totalFemaleEmployee = $scope.instEmpCount.totalFemaleEmployee + 1;
                                        $scope.instEmpCount.totalEngaged = $scope.instEmpCount.totalEngaged + 1;
                                    }
                                }
                                if(value.mpoActive == true){
                                    $scope.instEmpCount.totalGranted = $scope.instEmpCount.totalGranted +1;
                                }
                            });

                        }

                    });
                    //InstVacancysByInstitute.query({instituteId: $scope.employee.institute.id},function(result){
                    //    $scope.instVacancys = result;
                    //});
                };


                $scope.educations = function(){
                    $scope.applicantEducations = $scope.applicantEducation;
                };
                // $scope.training = function(){
                // };
                // $scope.attachment = function(){
                // };

                // $scope.displayAttachment= function(applicaiton) {
                // }

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;
                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];
                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);
                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }
                        var byteArray = new Uint8Array(byteNumbers);
                        byteArrays.push(byteArray);
                    }
                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }

                $scope.mpoVacancyRoleDesgnations.push(
                    {
                        status: null,
                        totalPost: null,
                        id: null
                    }
                );

                $scope.AddMoreVacancyRoleDesgnations = function () {

                    $scope.mpoVacancyRoleDesgnations.push(
                        {
                            status: null,
                            totalPost: null,
                            id: null
                        }
                    );
                    // Start Add this code for showing required * in add more fields
                    //$timeout(function () {
                    //    $rootScope.refreshRequiredFields();
                    //}, 100);
                    // End Add this code for showing required * in add more fields

                };

                $scope.vacancyTempInput = function(){
                    angular.forEach($scope.mpoVacancyRoleDesgnations, function (data) {
                        data.institute = $scope.employee.institute;
                        if(data.id !=null){
                            InstVacancyTemp.update(data);
                        }else{
                            InstVacancyTemp.save(data);
                        }
                    });
                }

                $scope.editEnable = function(){

                    if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE']) && $scope.showApproveButton){
                        $scope.OthersInfoEdit = true;
                        $scope.editButton = false;
                    }else if(Principal.hasAnyAuthority(['ROLE_AD']) && $scope.showApproveButton){
                        $scope.OthersInfoEdit = false;
                        $scope.OthersInfoEditByAO = true;
                        $scope.editButton = false;
                    }
                }

                $scope.saveOthersVerifyInfo = function(){
                    $scope.OthersVerifyMPOInfo  =  $scope.othersInfoVerify.mcValidity+"|"+ $scope.othersInfoVerify.newsAddDate + "|"+ $scope.othersInfoVerify.recruitAppResol + "|"+$scope.othersInfoVerify.recruitExamTabSheet +
                                                    "|"+ $scope.othersInfoVerify.mpoSheet + "|" + $scope.othersInfoVerify.criminalCase + "|"+ $scope.othersInfoVerify.femaleQuota + "|"+$scope.othersInfoVerify.vacancyAnalysis +
                                                    "|"+$scope.othersInfoVerify.recommendationOfAO +"|"+$scope.othersInfoVerify.recommendationOfAD;
                    $scope.mpoApplication.verifyInfoByAO = $scope.OthersVerifyMPOInfo;
                    if($scope.OthersVerifyMPOInfo != null){
                        MpoUpdateApplication.update($scope.mpoApplication);
                        $scope.OthersInfoEdit = false;
                        $scope.OthersInfoEditByAO = false;
                        $scope.editButton = true;
                    }
                }

            }]);

/* mpo-employee-dialog.controller.js*/
angular.module('stepApp').controller('MPOEmployeeDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Principal', 'User', 'Institute',
        function ($scope, $stateParams, $modalInstance, entity, Principal, User, Institute) {

            $scope.employee = entity;
            $scope.users = User.query();
            $scope.institutes = Institute.query();

            Principal.identity().then(function (account) {
                $scope.account = account;
                $scope.employee.manager = User.get({login: $scope.account.login});
            });

            $scope.employee.institute = Institute.get({id: 'my'});


            $scope.load = function (id) {
                //Employee.get({id: id}, function (result) {
                //    $scope.employee = result;
                //});
            };

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:employeeUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                //if ($scope.employee.id != null) {
                //    Employee.update($scope.employee, onSaveSuccess, onSaveError);
                //} else {
                //    Employee.save($scope.employee, onSaveSuccess, onSaveError);
                //}
            };

            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);

/*mpo-education-dialog.js*/
angular.module('stepApp').controller('MPOEmployeeEducationDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ApplicantEducation', 'Institute',
        function ($scope, $stateParams, $modalInstance, entity, ApplicantEducation, Institute) {

            $scope.educationName = ['SSC/Dakhil', 'SSC (Vocational)', 'HSC/Alim', 'HSC (Vocational)', 'HSC (BM)', 'Honors/Degree', 'Masters']
            $scope.employee = entity;
            $scope.calendar = {
                opened: {},
                dateFormat: 'yyyy-MM-dd',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };


            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:MpoApplicantEducationEdit', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.applicantEducation.id != null) {
                    ApplicantEducation.update($scope.applicantEducation, onSaveSuccess, onSaveError);
                } else {
                    $scope.applicantEducation.employee = {};
                    $scope.applicantEducation.manager = $scope.employee.manager;
                    $scope.applicantEducation.institute = $scope.employee.institute;
                    $scope.applicantEducation.employee.id = $scope.employee.id;
                    ApplicantEducation.save($scope.applicantEducation, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);

/*mpo-education-delete-dialog.controller.js*/

angular.module('stepApp')
    .controller('MpoEducationDeleteController',
        ['$scope', '$modalInstance', 'entity', 'ApplicantEducation',
            function($scope, $modalInstance, entity, ApplicantEducation) {

                $scope.applicantEducation = entity;

                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };

                $scope.confirmDelete = function (educationId) {
                    ApplicantEducation.delete({id: educationId},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/* mpo-training-delete-dialog.controller.js */
angular.module('stepApp')
    .controller('MPOTrainingDeleteController',
        ['$scope', '$modalInstance', 'entity', 'Training',
            function($scope, $modalInstance, entity, Training) {

                $scope.training = entity;
                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDelete = function (trainingId) {
                    Training.delete({id: trainingId},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*mpo-attachment-delete-dialog.controller.js*/
angular.module('stepApp')
    .controller('MpoAttachmentDeleteController',
        ['$scope', '$modalInstance', 'entity', 'Attachment',
            function($scope, $modalInstance, entity, Attachment) {

                $scope.attachment = entity;

                $scope.clear = function() {
                    $modalInstance.dismiss('cancel');
                };

                $scope.confirmDelete = function (attachmentId) {
                    Attachment.delete({id: attachmentId},
                        function () {
                            $modalInstance.close(true);
                        });
                };

            }]);

/*mpo-training-dialog.controller.js*/
angular.module('stepApp').controller('MPOTrainingDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Training', 'District', 'User',
        function($scope, $stateParams, $modalInstance, entity, Training, District, User) {

            $scope.training = entity;

            $scope.employee = Employee.get({id : $stateParams.id});
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:trainingUpdate', result);
                $modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.training.id != null) {
                    Training.update($scope.training, onSaveSuccess, onSaveError);
                } else {
                    $scope.training.employee = $scope.employee;
                    $scope.training.manager = $scope.employee.manager;
                    $scope.training.location = "Dhaka";
                    Training.save($scope.training, onSaveSuccess, onSaveError);
                }
            };

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);



/*application.controller.js*/
angular.module('stepApp').controller('ApplicationController',
    //function ($scope, $state, Principal, Employee, Institute) {
    ['$scope', '$state', 'Principal', 'Institute','MpoApplicationCheck', 'MpoApplicationCount','CurrentInstEmployee','$rootScope','TimeScaleApplicationCount','BEdApplicationList','APcaleApplicationList','PrincipleApplicationList',
        function ($scope, $state, Principal, Institute,MpoApplicationCheck, MpoApplicationCount,CurrentInstEmployee,$rootScope,TimeScaleApplicationCount,BEdApplicationList,APcaleApplicationList,PrincipleApplicationList) {



            $scope.showApplicationButtion = false;
            $scope.isAdmin = false;
            Principal.identity().then(function (account) {
                CurrentInstEmployee.get({},function(res){
                    $scope.instEmployee = res;
                });
                $scope.account = account;
                $scope.pendingMPOCount = 0;
                $scope.pendingBedCount = 0;
                $scope.pendingTimeScaleCount = 0;
                $scope.pendingApCount = 0;
                $scope.pendingPrincipalCount = 0;

                if($scope.isInArray('ROLE_INSTEMP', $scope.account.authorities))
                {
                    $scope.showApplicationButtion = true;
                }
                if($scope.isInArray('ROLE_ADMIN', $scope.account.authorities))
                {
                    $scope.isAdmin = true;
                }
                else {
                    $scope.showApplicationButtion = false;
                    MpoApplicationCount.get({status : 0}, function(result){
                        $scope.pendingMPOCount = result.size;

// chart1 start
                        $scope.myChartObject1 = {};
                        $scope.myChartObject1.type = "PieChart";
                        $scope.onions = [
                            {v: "Onions"},
                            {v: 3},
                        ];
                        $scope.myChartObject1.data = {
                            "cols": [
                                {id: "t", label: "Topping", type: "string"},
                                {id: "s", label: "Slices", type: "number"}

                            ],
                            "rows":
                                [{c: [{v: "Approve"},{v: 3}]},
                                    {c: [{v: "Pending"},{v: $scope.pendingMPOCount}]}]};

                        $scope.myChartObject1.options = {
                            legend: 'none',
                            'title': 'MPO Applications',
                            'slices': [{color:'#0D9947'},{color:'#F42A41'}]
                        };

// chart1 end
                    });
                    BEdApplicationList.query({status : 0}, function(result){
                        $scope.pendingBedCount = result.length;
// chart2 start
                        $scope.myChartObject2 = {};
                        $scope.myChartObject2.type = "PieChart";
                        $scope.onions = [
                            {v: "Onions"},
                            {v: 3},
                        ];
                        $scope.myChartObject2.data = {
                            "cols": [
                                {id: "t", label: "Topping", type: "string"},
                                {id: "s", label: "Slices", type: "number"}

                            ],


                            "rows":
                                [{c: [{v: "Approve"},{v: 3}]},
                                    {c: [{v: "Pending"},{v: $scope.pendingBedCount}]}]};


                        $scope.myChartObject2.options = {
                            legend: 'none',
                            'title': 'B.ed Applications',
                            'slices': [{color:'#0D9947'},{color:'#F42A41'}]

                        };

// chart2 end

                    });
                    TimeScaleApplicationCount.get({status : 0}, function(result){ //
                        $scope.pendingTimeScaleCount = result.size;
// chart3 start
                        $scope.myChartObject3 = {};
                        $scope.myChartObject3.type = "PieChart";
                        $scope.myChartObject3.data = {
                            "cols": [
                                {id: "t", label: "Topping", type: "string"},
                                {id: "s", label: "Slices", type: "number"}
                            ],
                            "rows":
                                [{c: [{v: "Approve"},{v: 3},]},
                                    {c: [{v: "Pending"},{v: $scope.pendingTimeScaleCount},]}]};

                        $scope.myChartObject3.options = {
                            legend: 'none',
                            'title': 'Time Scale Applications',
                            'slices': [{color:'#0D9947'},{color:'#F42A41'}]
                        };
// chart3 end


                    });
                    APcaleApplicationList.query({status : 0}, function(result){ ///
                        $scope.pendingApCount = result.length;
                        // chart4 start
                        $scope.myChartObject4 = {};
                        $scope.myChartObject4.type = "PieChart";
                        $scope.onions = [
                            {v: "Onions"},
                            {v: 3}
                        ];
                        $scope.myChartObject4.data = {
                            "cols": [
                                {id: "t", label: "Topping", type: "string"},
                                {id: "s", label: "Slices", type: "number"}
                            ],
                            "rows":
                                [{c: [{v: "Approve"},{v: 3},]},
                                    {c: [{v: "Pending"},{v: $scope.pendingApCount},{v: "green"}]}]};

                        $scope.myChartObject4.options = {
                            legend: 'none',
                            'title': 'Assistant Professor Applications',
                            'slices': [{color:'#0D9947'},{color:'#F42A41'}]
                        };
// chart4 end

                    });



                    PrincipleApplicationList.query({status : 0}, function(result){ //
                        $scope.pendingPrincipalCount = result.length;
                        // chart5 start
                        $scope.myChartObject5 = {};
                        $scope.myChartObject5.type = "PieChart";
                        $scope.onions = [
                            {v: "Onions"},
                            {v: 3},
                        ];
                        $scope.myChartObject5.data = {
                            "cols": [
                                {id: "t", label: "Topping", type: "string"},
                                {id: "s", label: "Slices", type: "number"}
                            ],
                            "rows":
                                [{c: [{v: "Approve"},{v: 3},]},
                                    {c: [{v: "Pending"},{v: $scope.pendingPrincipalCount},{v: "green"}]}]};

                        $scope.myChartObject5.options = {
                            legend: 'none',
                            'title': 'Principal Applications',
                            'slices': [{color:'#0D9947'},{color:'#F42A41'}]
                        };
// chart5 end

                    });
                    PrincipleApplicationList.query({status : 0}, function(result){ //
                        $scope.pendingPrincipalCount = result.length;

                        // chart6 start
                        $scope.myChartObject6 = {};
                        $scope.myChartObject6.type = "PieChart";
                        $scope.onions = [
                            {v: "Zucchini"},
                            {v: 3},
                        ];
                        $scope.myChartObject6.data = {
                            "cols": [
                                {id: "t", label: "Topping", type: "string"},
                                {id: "s", label: "Slices", type: "number"}
                            ],
                            "rows":
                                [{c: [{v: "Approve"},{v: 3},]},
                                    {c: [{v: "Pending"},{v: $scope.pendingPrincipalCount},{v: "green"}]}]};

                        $scope.myChartObject6.options = {
                            legend: 'none',
                            'title': 'Name Cancellation Application',
                            'slices': [{color:'#0D9947'},{color:'#F42A41'}]
                        };
// chart6 end


                    });
                }

                MpoApplicationCheck.get({'code':$scope.account.login},function(res){

                    if(res.id == 0){
                        if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                            $scope.showApplicationButtion = true;
                        }
                        else{
                            $scope.showApplicationButtion = false;
                        }
                    }
                    /*else {
                     $state.go('mpo.employeeTrack');
                     }*/
                    /* $scope.employee = res.instEmployee;
                     $scope.address = res.instEmpAddress;
                     $scope.applicantEducation = res.instEmpEduQualis;
                     console.log($scope.applicantEducation);
                     $scope.applicantTraining = res.instEmplTrainings;
                     $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                     $scope.instEmplBankInfo = res.instEmplBankInfo;
                     console.log($scope.applicantTraining);
                     $scope.mpoForm = true;*/
                });

            });

            $scope.isInArray = function isInArray(value, array) {
                return array.indexOf(value) > -1;
            };

        }]);

/*mpo-payscale-application.controller.js*/
angular.module('stepApp').controller('MPOPayScaleApplicationController',
    ['$scope', 'Employee', 'Institute',
        function ($scope, Employee, Institute) {


        }]);

/*mpo-application-track.controller.js */
angular.module('stepApp')
    .controller('MpoApplicationTrack',
        ['$scope', '$rootScope', '$state', '$stateParams', 'Sessions', 'InstEmployeeCode','MpoApplicationLogEmployeeCode','MpoApplication', 'PayScale', 'Jobapplication', 'Principal', 'MpoApplicationCheck',
            function ($scope, $rootScope, $state, $stateParams, Sessions, InstEmployeeCode,MpoApplicationLogEmployeeCode,MpoApplication, PayScale, Jobapplication, Principal, MpoApplicationCheck) {
                //BedApplicationStatusController
                $scope.search = {};
                $rootScope.searchByTrackID = false;
                $scope.showSearchForm = true;
                $scope.showApplicationButtion=true;

                $scope.showMpoForm = function () {
                    InstEmployeeCode.get({'code':$scope.search.code},function(res){
                        $scope.employee = res.instEmployee;
                        $scope.employee.id = res.instEmployee.id;
                        $rootScope.searchByTrackID = true;
                        $state.go('mpo.details', {id: $scope.employee.id});
                    });
                    $scope.mpoNotFoundByCode = 'No Teacher found with this Tracking ID'
                };
                if(!Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    $scope.showApplicationButtion=false;
                }


                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    Principal.identity().then(function (account) {
                        $scope.account = account;
                        $scope.showSearchForm = false;
                        MpoApplicationCheck.get({'code':$scope.account.login},function(res){
                            if(!res.id){
                                $rootScope.setErrorMessage('Your Mpo Application is Declined.Please Complete Your Application First.');
                                //$state.go('mpo.application',{},{reload:true});
                            }
                            // else {
                            //     $rootScope.setSuccessMessage('Your MPO Application is Submitted Successfully.');
                            // }
                        });
                        InstEmployeeCode.get({'code':$scope.account.login},function(res){
                            $scope.employee = res.instEmployee;

                            MpoApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                                $scope.mpoApplicationlogs = result;
                                if($scope.mpoApplicationlogs.length>0){
                                    $scope.showApplicationButtion=false;
                                }
                            });
                        });
                    });
                }else{
                    MpoApplication.get({id:$stateParams.id}, function(result) {
                        InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                            $scope.employee = res.instEmployee;
                            $scope.employee.id = res.instEmployee.id;
                            $scope.address = res.instEmpAddress;
                            $scope.applicantEducation = res.instEmpEduQualis;
                            $scope.applicantTraining = res.instEmplTrainings;
                            $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                            $scope.instEmplBankInfo = res.instEmplBankInfo;
                            AttachmentEmployee.query({id: $scope.employee.id}, function(result){
                                $scope.attachments = result;
                            });
                        });
                        //$state.go('mpo.employeeTrack');

                    });
                }
            }]);

/* mpo-application-checklist.controller.js */
angular.module('stepApp')
    .controller('MpoApplicationCheckListController',
        ['$scope', '$rootScope', '$state','MpoCommitteeDescisionByMpo', '$stateParams', 'Sessions', 'InstEmployeeCode','MpoApplicationLogEmployeeCode','MpoApplication', 'PayScale', 'Jobapplication', 'Principal',
            function ($scope, $rootScope, $state,MpoCommitteeDescisionByMpo, $stateParams, Sessions, InstEmployeeCode,MpoApplicationLogEmployeeCode,MpoApplication, PayScale, Jobapplication, Principal) {

                $scope.showSearchForm = true;
                $scope.showApprove = false;
                $scope.showApproveDecline = false;
                $scope.mpoApplication = {};

                $scope.checkStatus = function(){
                    if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                        return 1;
                    }else if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                        return 2;
                    }else if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                        return 3;
                    }else if(Principal.hasAnyAuthority(['ROLE_DEO'])){
                        return 4;
                    }else if(Principal.hasAnyAuthority(['ROLE_FRONTDESK'])){
                        return 5;
                    }else if(Principal.hasAnyAuthority(['ROLE_AD'])){
                        return 6;
                    }else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                        return 7;
                    }else if(Principal.hasAnyAuthority(['ROLE_DG'])){
                        return 8;
                    }else if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                        return 9;
                    }else{
                        return 0;
                    }
                };
                MpoApplicationLogEmployeeCode.get({'code':$stateParams.code}, function(result) {
                    $scope.mpoApplicationlogs = result;
                    $scope.mpoApplication = result[0].mpoApplicationId;

                    if( ($scope.checkStatus()!=0 & $scope.checkStatus() == $scope.mpoApplication.status) || $scope.mpoApplication.status > 8){
                        if($scope.mpoApplication.status < 11){
                            $scope.showApproveDecline=true;
                        }
                        $scope.showApprove=true;
                        $scope.mpoId = result.id;
                    }else{
                        $scope.showApprove=false;
                    }
                    $scope.mpoCommitteeComments= MpoCommitteeDescisionByMpo.query({mpoApplicationid: result[0].mpoApplicationId.id});
                });

                InstEmployeeCode.get({'code':$stateParams.code},function(res){
                    $scope.address = res.instEmpAddress;
                    //$scope.applicantEducation = res.instEmpEduQualis;
                    $scope.instEmplExperiences = res.instEmplExperiences;
                    $scope.applicantEducation = res.instEmpEduQualis;
                    $scope.applicantTraining = res.instEmplTrainings;
                    $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                    $scope.instEmplBankInfo = res.instEmplBankInfo;
                });
            }]);


/*mpo-list-by-status.controller.js */
angular.module('stepApp')
    .controller('MpoListByStatus',
        ['$scope', 'entity', '$state','MpoApplication','ParseLinks','MpoApplicationSummaryList','Principal','MpoApplicationsByLevel','InstLevelByName', '$rootScope','ForwardMpoSummaryList',
            function ($scope, entity, $state,  MpoApplication, ParseLinks,MpoApplicationSummaryList,Principal,MpoApplicationsByLevel,InstLevelByName, $rootScope,ForwardMpoSummaryList) {

                $scope.mpoAppliedLists = entity;
                $scope.allApplications = entity;
                $scope.showSummary = false;
                $scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.employees = [];
                $scope.summaryList = [];
                $scope.page = 0;
                $scope.mpoForwardedIdList = [];
                $scope.mpoApplicationForMPoMeeting = [];

                if(entity.pending === "pending" && (Principal.hasAnyAuthority(['ROLE_DIRECTOR']) || Principal.hasAnyAuthority(['ROLE_DG']))){
                    MpoApplicationSummaryList.query({page: $scope.page, size: 2000}, function(result){
                        $scope.summaryList = result;
                    });
                }

                $scope.all = function () {
                    $scope.showSummary = false;
                    $scope.mpoAppliedLists = entity;
                };
                $scope.ssc = function () {
                    $scope.showSummary = false;
                    InstLevelByName.get({name: "SSC (VOC)"}, function(result){
                        MpoApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                            $scope.mpoAppliedLists = result2;
                        });
                    });
                };
                $scope.hsc = function () {
                    $scope.showSummary = false;
                    InstLevelByName.get({name: "HSC (BM)"}, function(result){
                        MpoApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                            $scope.mpoAppliedLists = result2;
                        });
                    });
                };
                $scope.mdvoc = function () {
                    $scope.showSummary = false;
                    InstLevelByName.get({name: "Madrasha (VOC)"}, function(result){
                        MpoApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                            $scope.mpoAppliedLists = result2;
                        });
                    });
                };
                $scope.mdbm = function () {
                    $scope.showSummary = false;
                    InstLevelByName.get({name: "Madrasha (BM)"}, function(result){
                        MpoApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                            $scope.mpoAppliedLists = result2;
                        });
                    });
                };

                var onForwardSuccess = function (index) {

                };

                $scope.showSummary1 = function () {
                    $scope.showSummary = true;
                };

                $scope.createSummary = function () {
                    MpoApplicationSummaryList.query({}, function(result){
                        $scope.summaryList = result;
                        if(result.length == 0){
                            alert("No application found!");
                        }
                    }, function(response) {
                        if(response.status === 404) {
                            alert("No application found!");
                        }
                    });
                };
                $scope.forwardSummaryList = function () {
                    angular.forEach($scope.summaryList, function (value,key) {
                        $scope.mpoForwardedIdList.push(value.MPO_ID);
                    });
                    ForwardMpoSummaryList.get({mpoForwardedIdList:$scope.mpoForwardedIdList},function(mpoData){
                        if(mpoData.saveValue == true){
                            $rootScope.setSuccessMessage('Applications Forwarded Successfully.');
                            $state.go('mpoApprovedList', null, { reload: true });
                        }
                    });
                };

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.areAllEmployeesSelected = false;
                $scope.isAllSelected = false;
                $scope.updateAllSelection = function (mpoApp, selectionValue) {
                    $scope.mpoApplicationForMPoMeeting = [];
                    for (var i = 0; i < mpoApp.length; i++) {
                        mpoApp[i].isSelected = selectionValue;
                        if(mpoApp[i].isSelected == true){
                            $scope.mpoApplicationForMPoMeeting.push(mpoApp[i]);
                        }else {
                            var index = $scope.mpoApplicationForMPoMeeting.indexOf(mpoApp[i]);
                            $scope.mpoApplicationForMPoMeeting.splice(index);
                        }
                    }
                };

                $scope.updateSelection = function (mpoApp) {
                    if(mpoApp.isSelected == true){
                        $scope.mpoApplicationForMPoMeeting.push(mpoApp);
                    }else {
                        var index = $scope.mpoApplicationForMPoMeeting.indexOf(mpoApp);
                        $scope.mpoApplicationForMPoMeeting.splice(index);
                    }
                };
            }]);

/* mpo-approve-dialog.controller.js */
angular.module('stepApp')
    .controller('MpoApproveDialogController',
        ['$scope','$stateParams','$rootScope','MpoApplication','MpoCommitteeDescisionCurWithMpo','ForwardMpoApplication','MpoCommitteeDescision', 'AllForwaringList','entity', '$state', 'Sessions', 'InstEmployeeCode','MpoApplicationLogEmployeeCode', 'PayScale', 'Jobapplication', 'Principal', 'ParseLinks', 'Institute', 'User', 'Upazila', 'Course',  'DataUtils', '$modalInstance', 'StaffCount',
            function ($scope,$stateParams,$rootScope,MpoApplication,MpoCommitteeDescisionCurWithMpo,ForwardMpoApplication,MpoCommitteeDescision, AllForwaringList,entity, $state, Sessions, InstEmployeeCode,MpoApplicationLogEmployeeCode, PayScale, Jobapplication, Principal, ParseLinks, Institute, User, Upazila, Course, DataUtils, $modalInstance, StaffCount) {
                $scope.mpoCommitteeDescision = {};
                MpoCommitteeDescisionCurWithMpo.get({mpoApplicationid: $stateParams.id}, function(result){
                    $scope.mpoCommitteeDescision = result;
                });
                $scope.approveVal = false;
                $scope.forward = {};
                MpoApplication.get({id: $stateParams.id}, function(result){
                    $scope.mpoApplication = result;

                    if(result.instEmployee.instLevel.name === 'MADRASA_BM' || result.instEmployee.instLevel.name === 'HSC_BM' || result.instEmployee.instLevel.name === 'Madrasha (BM)' || result.instEmployee.instLevel.name === 'HSC (BM)'){
                        AllForwaringList.get({type:'BM'}, function(result){
                            $scope.forwardingList=result;
                            if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                                $scope.forwardingList.splice(1,1);
                            }
                        });
                    }else{
                        AllForwaringList.get({type:'VOC'}, function(result){
                            $scope.forwardingList=result;
                            //if(Principal.hasAnyAuthority(['ROLE_INSTITUTE']) && $scope.mpoApplication.status == 2){
                            //    $scope.forwardingList.splice(1,1);
                            //}else
                            if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                                $scope.forwardingList.splice(1,1);
                            }
                        });
                    }
                    if(result.status > 6){
                        $scope.approveVal = true;
                    };
                });

                $scope.confirmApprove = function(forward){

                    if($scope.mpoApplication.status > 6){
                        if(Principal.hasAnyAuthority(['ROLE_DG'])){
                            $scope.mpoApplication.dgFinalApproval=true;
                            $scope.mpoApplication.dgComments = $scope.remark;
                        }
                        if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                            $scope.mpoApplication.directorComment=$scope.remark;
                        }if(Principal.hasAnyAuthority(['ROLE_AD'])){
                            $scope.mpoApplication.directorComment = $scope.remark;
                        }
                        MpoApplication.update($scope.mpoApplication, onSaveSuccess, onSaveError);
                    }else{
                        ForwardMpoApplication.forward({forwardTo:$scope.forward.forwardTo.code,cause: $scope.remark,memoNo: $scope.mpoApplication.memoNo},$scope.mpoApplication, onSaveSuccess, onSaveError);

                    }
                };
                //$scope.testfor = function(data){
                //    $scope.forwardTo = data;
                //};
                var onSaveSuccess = function(result){
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('Application Forwarded Successfully.');
                    $state.go('mpo.details', null, { reload: true });
                };
                var onSaveSuccess2 = function(result){
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('Application Approved Successfully.');
                    $state.go('mpo.details', null, { reload: true });
                };
                var onSaveError = function(result){

                };
                $scope.clear = function(){
                    $modalInstance.close();
                    window.history.back();
                    //$state.go('mpo.details');
                }

            }])
    /* mpo-approve-dialog.controller.js */
    .controller('MpoApproveAllDialogController',
    ['$scope','$stateParams','$rootScope','MpoApplication','MpoApplicationSummaryList', '$state', 'InstEmployeeCode','MpoApplicationLogEmployeeCode', 'PayScale', 'Principal', 'ParseLinks', 'Institute', 'User', 'Upazila','DataUtils', '$modalInstance',
        function ($scope,$stateParams,$rootScope,MpoApplication,MpoApplicationSummaryList, $state,InstEmployeeCode,MpoApplicationLogEmployeeCode, PayScale, Principal, ParseLinks, Institute, User, Upazila, DataUtils, $modalInstance) {

            $scope.approveVal = true;

            $scope.confirmApprove = function(){
                MpoApplicationSummaryList.query({}, function(result) {
                    //$scope.summaryList = result;
                    var listLength = 0;
                    if (Principal.hasAnyAuthority(['ROLE_DIRECTOR']) || Principal.hasAnyAuthority(['ROLE_DG'])) {
                        angular.forEach(result, function (data) {
                            listLength = listLength +1;
                            $scope.mpoApplication = {};
                            MpoApplication.get({id: data.MPO_ID}, function (result) {
                                $scope.mpoApplication = result;
                                if ($scope.mpoApplication.id != null) {
                                    if (Principal.hasAnyAuthority(['ROLE_DG'])) {
                                        $scope.mpoApplication.dgFinalApproval = true;
                                        MpoApplication.update($scope.mpoApplication);
                                        //data.directorComment = $scope.remarks;
                                    } else if (Principal.hasAnyAuthority(['ROLE_DIRECTOR'])) {
                                        $scope.mpoApplication.directorComment = $scope.remarks;
                                        MpoApplication.update($scope.mpoApplication);
                                    }
                                }
                            })
                        });
                    }
                    $modalInstance.close();
                    $state.go('mpoPendingList',{},{reload:true});
                    // window.history.back();
                });
            };

            var onSaveSuccess = function(result){
                $modalInstance.close();
                $rootScope.setSuccessMessage('Applications Approved Successfully.');
                window.history.back();
            };

            var onSaveError = function(result){

            };
            $scope.clear = function(){
                $modalInstance.close();
                window.history.back();
                //$state.go('mpo.details');
            }

        }]);


angular.module('stepApp')
    .controller('MpoDenyDialogController',
        ['$scope','$stateParams','Principal','MpoApplication', 'entity', 'DateUtils', '$state', '$modalInstance','MpoApplicationDecline','MpoCommitteeDescision', 'MpoApplicationStatusLog',
            function ($scope,$stateParams,Principal,MpoApplication, entity, DateUtils, $state, $modalInstance,MpoApplicationDecline,MpoCommitteeDescision, MpoApplicationStatusLog) {
                $scope.mpoApplication = entity;
                $scope.mpoApplicationStatusLog = {};
                $scope.mpoCommitteeDescision = {};
                $scope.causeDeny = "";

                if($stateParams.id !=null){
                    MpoApplication.get({id : $stateParams.id},function(mpoApplicationResult){
                        $scope.mpoApplication = mpoApplicationResult;
                    });
                }


                $scope.confirmDecline = function(){
                    if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE']) && !Principal.hasAnyAuthority(['ROLE_DG'])){
                        if($scope.mpoCommitteeDescision.id != null){
                            MpoCommitteeDescision.update($scope.mpoCommitteeDescision, onSaveSuccess, onSaveError);
                        }else{
                            $scope.mpoCommitteeDescision.mpoApplication = $scope.mpoApplication;
                            $scope.mpoCommitteeDescision.status = 0;
                            $scope.mpoCommitteeDescision.comments = $scope.causeDeny;
                            MpoCommitteeDescision.save($scope.mpoCommitteeDescision, onSaveSuccess, onSaveError);
                        }
                    }else{
                        MpoApplicationDecline.decline({id: $scope.mpoApplication.id, cause: $scope.causeDeny},{},onSaveSuccess, onSaveError);

                    }
                };
                var onSaveSuccess = function(result){
                    $modalInstance.close();
                };
                var onSaveError = function(result){

                };
                $scope.clear = function(){
                    $modalInstance.close();
                    window.history.back();
                };


            }]);

/* salary-generate.controller.js */
angular.module('stepApp')
    .controller('SalaryGenerateController',
        ['$scope', 'entity', '$state', 'InstEmployee', 'MpoApplication', 'PayScale',
            function ($scope, entity, $state, InstEmployee, MpoApplication, PayScale) {

                $scope.mpoApplication = entity;
                $scope.instEmployee = {};
                $scope.mpoApplication.instEmployee = {};

                $scope.years = [
                    {'key' : '2015', 'value' : '2015'},
                    {'key' : '2016', 'value' : '2016'},
                    {'key' : '2017', 'value' : '2017'},
                    {'key' : '2018', 'value' : '2018'}
                ];

                $scope.months = [
                    {'key' : '01', 'value' : 'January'},
                    {'key' : '02', 'value' : 'February'},
                    {'key' : '03', 'value' : 'March'},
                    {'key' : '04', 'value' : 'April'},
                    {'key' : '05', 'value' : 'May'},
                    {'key' : '06', 'value' : 'June'},
                    {'key' : '07', 'value' : 'July'},
                    {'key' : '08', 'value' : 'August'},
                    {'key' : '09', 'value' : 'September'},
                    {'key' : '10', 'value' : 'October'},
                    {'key' : '11', 'value' : 'Nobember'},
                    {'key' : '12', 'value' : 'December'}
                ];


                $scope.loadAll = function() {
                    PayScale.query({page: $scope.page, size: 2000}, function(result) {
                        $scope.payScales = result;
                    });
                };

                $scope.loadAll();

                $scope.generateSalary = function()
                {
                    $http({
                        url: 'your/webservice',
                        method: 'POST',
                        responseType: 'arraybuffer',
                        data: json, //this is your json data string
                        headers: {
                            'Content-type': 'application/json',
                            'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        }
                    }).success(function(data){
                        var blob = new Blob([data], {
                            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        });
                        saveAs(blob, 'File_Name_With_Some_Unique_Id_Time' + '.xlsx');
                    }).error(function(){
                        //Some error log
                    });
                };

                $scope.save = function () {

                    if($scope.payScaleID == '') {
                        return false;
                    }

                    $scope.isSaving = true;

                    $scope.instEmployee = $scope.mpoApplication.instEmployee;

                    PayScale.get({id: $scope.payScaleID}, function(result) {
                        $scope.instEmployee.payScale = result;
                        InstEmployee.update($scope.instEmployee);
                        $state.go('mpo.payScaleApprovedList',{},{reload:true});
                    });

                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

            }]);


/* salary-generate-by-institute.controller.js */
angular.module('stepApp')
    .controller('SalaryGenerateByInstituteController',
        ['$scope', 'entity', '$state', 'SalaryGenerateByInstitute', 'InstEmployee', 'MpoApplication', 'PayScale',
            function ($scope, entity, $state, SalaryGenerateByInstitute, InstEmployee, MpoApplication, PayScale) {

                //$scope.salaryReports = entity;

                $scope.salaryReports = SalaryGenerateByInstitute.query(function(response){
                });
                $scope.years = [
                    {'key' : '2015', 'value' : '2015'},
                    {'key' : '2016', 'value' : '2016'},
                    {'key' : '2017', 'value' : '2017'},
                    {'key' : '2018', 'value' : '2018'}
                ];

                $scope.months = [
                    {'key' : '01', 'value' : 'January'},
                    {'key' : '02', 'value' : 'February'},
                    {'key' : '03', 'value' : 'March'},
                    {'key' : '04', 'value' : 'April'},
                    {'key' : '05', 'value' : 'May'},
                    {'key' : '06', 'value' : 'June'},
                    {'key' : '07', 'value' : 'July'},
                    {'key' : '08', 'value' : 'August'},
                    {'key' : '09', 'value' : 'September'},
                    {'key' : '10', 'value' : 'October'},
                    {'key' : '11', 'value' : 'Nobember'},
                    {'key' : '12', 'value' : 'December'}
                ];

            }]);
