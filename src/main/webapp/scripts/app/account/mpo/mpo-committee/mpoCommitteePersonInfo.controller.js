'use strict';

angular.module('stepApp')
    .controller('MpoCommitteePersonInfoController',
     ['$scope', '$state', '$modal', 'MpoCommitteePersonInfo', 'MpoCommitteePersonInfoSearch', 'ParseLinks',
     function ($scope, $state, $modal, MpoCommitteePersonInfo, MpoCommitteePersonInfoSearch, ParseLinks) {

        $scope.mpoCommitteePersonInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MpoCommitteePersonInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.mpoCommitteePersonInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            MpoCommitteePersonInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.mpoCommitteePersonInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.mpoCommitteePersonInfo = {
                contactNo: null,
                address: null,
                designation: null,
                orgName: null,
                dateCrated: null,
                dateModified: null,
                status: null,
                activated: null,
                id: null
            };
        };
    }]);

/* mpoCommitteeFindMembers.controller.js*/
angular.module('stepApp')
    .controller('MpoCommitteeAllMembersController',
        ['$scope', '$state', '$modal', 'MpoCommitteePersonInfo', 'MpoCommitteePersonInfoSearch', 'ParseLinks',
            function ($scope, $state, $modal, MpoCommitteePersonInfo, MpoCommitteePersonInfoSearch, ParseLinks) {

                $scope.mpoCommitteePersonInfos = [];
                $scope.page = 0;
                $scope.loadAll = function() {
                    MpoCommitteePersonInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.mpoCommitteePersonInfos = result;
                    });
                };
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.loadAll();


                $scope.search = function () {
                    MpoCommitteePersonInfoSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.mpoCommitteePersonInfos = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.deactivateMember = function (data) {
                    console.log(data);
                    data.activated=false;
                    MpoCommitteePersonInfo.update(data);

                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.mpoCommitteePersonInfo = {
                        contactNo: null,
                        address: null,
                        designation: null,
                        orgName: null,
                        dateCrated: null,
                        dateModified: null,
                        status: null,
                        activated: null,
                        id: null
                    };
                };
            }]);

/* mpoCommitteePersonInfo-detail.controller.js */
angular.module('stepApp')
    .controller('MpoCommitteePersonInfoDetailController',
        ['$scope', '$rootScope', '$stateParams', 'entity', 'MpoCommitteePersonInfo', 'User',
            function ($scope, $rootScope, $stateParams, entity, MpoCommitteePersonInfo, User) {
                $scope.mpoCommitteePersonInfo = entity;
                $scope.load = function (id) {
                    MpoCommitteePersonInfo.get({id: id}, function(result) {
                        $scope.mpoCommitteePersonInfo = result;
                    });
                };
                var unsubscribe = $rootScope.$on('stepApp:mpoCommitteePersonInfoUpdate', function(event, result) {
                    $scope.mpoCommitteePersonInfo = result;
                });
                $scope.$on('$destroy', unsubscribe);

            }]);

/* mpoCommitteeFindMembers.controller.js */
angular.module('stepApp')
    .controller('MpoCommitteeMembersOfCommitteeController',
        ['$scope', '$state', '$modal', 'MpoCommitteePersonInfo', 'MpoCommitteePersonInfoSearch','User', 'ParseLinks','MpoCommitteeHistoryMonthYear', 'MpoCommitteeHistory','$rootScope',
            function ($scope, $state, $modal, MpoCommitteePersonInfo, MpoCommitteePersonInfoSearch,User, ParseLinks,MpoCommitteeHistoryMonthYear, MpoCommitteeHistory,$rootScope) {

                $scope.mpoCommitteePersonInfos = [];
                $scope.mpoCommitteeHistorys = [];
                $scope.page = 0;
                $scope.years = [];
                var currentYear = new Date().getFullYear();
                $scope.searchCommittee = function () {
                    console.log('month :'+$scope.mpoCommitteeHistory.month +' year :'+$scope.mpoCommitteeHistory.year);
                    MpoCommitteeHistoryMonthYear.query({month:$scope.mpoCommitteeHistory.month, year: $scope.mpoCommitteeHistory.year}, function(result){
                        $scope.mpoCommitteeHistorys = result;
                    });
                    /*MpoCommitteePersonInfoSearch.query({query: $scope.searchQuery}, function(result) {
                     $scope.mpoCommitteePersonInfos = result;
                     }, function(response) {
                     if(response.status === 404) {
                     $scope.loadAll();
                     }
                     });*/
                };

                $scope.initDates = function() {
                    var i;
                    for (i = currentYear+1 ;i >= currentYear-25; i--) {
                        $scope.years.push(i);
                        //console.log( $scope.years);
                    }
                };
                $scope.initDates();

                $scope.deactivateCommitteeMembers = function () {
                    if($scope.mpoCommitteeHistorys.length > 0 ){
                        angular.forEach($scope.mpoCommitteeHistorys, function(data){
                            if (data.id != null) {
                                data.mpoCommitteePersonInfo.user.activated = false;
                                User.update(data.mpoCommitteePersonInfo.user, function () {
                                    data.mpoCommitteePersonInfo.activated= false;
                                    MpoCommitteePersonInfo.update(data.mpoCommitteePersonInfo, function(result){
                                        data.activated= false;
                                        MpoCommitteeHistory.update(data);
                                        $rootScope.setSuccessMessage('stepApp.mpoCommitteeHistory.successDeactivation');
                                    });
                                });
                            }
                        });
                    }

                };

                $scope.activateCommitteeMembers = function () {
                    if($scope.mpoCommitteeHistorys.length > 0 ){
                        angular.forEach($scope.mpoCommitteeHistorys, function(data){
                            if (data.id != null) {
                                data.mpoCommitteePersonInfo.activated= true;
                                MpoCommitteePersonInfo.update(data.mpoCommitteePersonInfo, function(result){
                                    data.activated= true;
                                    MpoCommitteeHistory.update(data);
                                    $rootScope.setSuccessMessage('stepApp.mpoCommitteeHistory.successActivation');
                                });
                            }
                        });
                    }

                };



                $scope.deactivateMember = function (data) {

                    if (data.id != null) {
                        data.mpoCommitteePersonInfo.activated= false;
                        MpoCommitteePersonInfo.update(data.mpoCommitteePersonInfo, function(result){
                            data.activated= false;
                            MpoCommitteeHistory.update(data);
                            $rootScope.setSuccessMessage('stepApp.mpoCommitteeHistory.successDeactivation');
                        });
                    }

                };

                $scope.activateMember = function (data) {
                    if (data.id != null) {
                        data.mpoCommitteePersonInfo.activated= true;
                        MpoCommitteePersonInfo.update(data.mpoCommitteePersonInfo, function(result){
                            data.activated= true;
                            MpoCommitteeHistory.update(data);
                            $rootScope.setSuccessMessage('stepApp.mpoCommitteeHistory.successActivation');
                        });
                    }
                };

            }]);

/* mpoCommitteeFormation-dialog.controller.js */
angular.module('stepApp').controller('MpoCommitteeFormationDialogController',
    ['$scope', '$stateParams', '$translate', '$q', 'entity', 'MpoCommitteePersonInfo', 'Auth','User','MpoCommitteeHistory','ActiveMpoCommitteePersonInfo','MpoCommitteeOneByEmail','$rootScope','MpoCommitteeHistoryOneByEmailMonthYear',
        function($scope,$modalInstance, $translate, $q, entity, MpoCommitteePersonInfo,Auth, User, MpoCommitteeHistory,ActiveMpoCommitteePersonInfo,MpoCommitteeOneByEmail,$rootScope,MpoCommitteeHistoryOneByEmailMonthYear) {

            $scope.mpoCommitteePersonInfos = [];
            $scope.years = [];
            $scope.activeMembers = [];
            $scope.registerAccount = {};
            $scope.searchCommittee = {};
            $scope.mpoCommitteeHistory = {};
            $scope.persnonEmail=null;
            var currentYear = new Date().getFullYear();
            $scope.users = User.query();
            $scope.load = function(id) {

                MpoCommitteePersonInfo.get({id : id}, function(result) {
                    $scope.mpoCommitteePersonInfo = result;
                });
            };
            $scope.activeMembers = ActiveMpoCommitteePersonInfo.query();
            console.log($scope.activeMembers);
            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:mpoCommitteePersonInfoUpdate', result);
                //$modalInstance.close(result);
                $scope.isSaving = false;
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.searchCommittee2 = function searchCommittee2()
            {
                console.log('>>>>>>>>>>><<<<<<<<<<<<<<<<<<<');
                MpoCommitteeOneByEmail.get( {email: $scope.persnonEmail}, function(result)
                {
                    $scope.searchCommittee = result;
                    console.log('***********************<<<<<<<<<>>>>>>>>>>>');
                }, function(response) {
                    console.log("Error with status code", response.status);
                    if(response.status == 404){
                        $rootScope.setErrorMessage('Member Not found!');
                        console.log('Member Not found!');
                        $scope.searchCommittee = {};
                        /*$scope.mpoCommitteeHistory.mpoCommitteePersonInfo= data;
                         $scope.mpoCommitteeHistory.activated= 1;
                         MpoCommitteeHistory.save($scope.mpoCommitteeHistory, function(result){
                         $rootScope.setSuccessMessage('Added to committee successfully.');
                         $scope.searchCommittee = {};
                         $scope.mpoCommitteePersonInfo.email = null;
                         });*/
                    }
                });
            };
            $scope.addMember = function(data) {
                console.log(data);
                if($scope.mpoCommitteeHistory.month ==null ||  $scope.mpoCommitteeHistory.year == null){
                    console.log('Committee month or year not found!');
                    $rootScope.setErrorMessage('Committee month or year not found!');
                }else{
                    MpoCommitteeHistoryOneByEmailMonthYear.get({email:data.user.email, month:$scope.mpoCommitteeHistory.month, year: $scope.mpoCommitteeHistory.year}, function() {
                        console.log('success');
                    }, function(response) {
                        console.log("Error with status code", response.status);
                        if(response.status == 404){
                            $scope.mpoCommitteeHistory.mpoCommitteePersonInfo= data;
                            $scope.mpoCommitteeHistory.activated= 1;
                            MpoCommitteeHistory.save($scope.mpoCommitteeHistory, function(result){
                                $rootScope.setSuccessMessage('Added to committee successfully.');
                                $scope.searchCommittee = {};
                                $scope.persnonEmail = null;
                            });
                        }

                    }); /*, function(resp){
                     console.log(resp);
                     console.log('comes to block');

                     }).catch(function (response1) {

                     if (response1.status === 404) {
                     console.log('status 404');?"
                     /              });*/

                }

            };
            $scope.initDates = function() {
                var i;
                for (i = currentYear ;i <= currentYear+5; i++) {
                    $scope.years.push(i);
                    //console.log( $scope.years);
                }
            };
            $scope.initDates();
            $scope.save = function () {
                $scope.isSaving = true;
                /*angular.forEach($scope.mpoCommitteePersonInfos, function(data){
                 console.log($scope.mpoCommitteePersonInfos.length+" :length of array")
                 if (data.id != null) {
                 $scope.mpoCommitteeHistory.mpoCommitteePersonInfo= data;
                 $scope.mpoCommitteeHistory.activated= 1;
                 console.log("saving hist");
                 console.log($scope.mpoCommitteeHistory);
                 MpoCommitteeHistory.save($scope.mpoCommitteeHistory);
                 //MpoCommitteePersonInfo.update(data, onSaveSuccess, onSaveError);
                 } else {
                 data.user.langKey = $translate.use();
                 $scope.doNotMatch = null;
                 $scope.error = null;
                 $scope.errorUserExists = null;
                 $scope.errorEmailExists = null;
                 data.user.activated = true;
                 data.user.authorities = ["ROLE_MPOCOMMITTEE"];

                 Auth.createAccount(data.user).then(function (res) {
                 console.log('********************');
                 console.log(res);
                 data.user = res;
                 MpoCommitteePersonInfo.save(data, function (result){
                 console.log("&*&*&**&*&*& saving person info :");
                 $scope.mpoCommitteeHistory.mpoCommitteePersonInfo= result;
                 $scope.mpoCommitteeHistory.activated= 1;
                 console.log("saving hist");
                 console.log($scope.mpoCommitteeHistory);
                 MpoCommitteeHistory.save($scope.mpoCommitteeHistory);
                 });
                 $scope.success = 'OK';
                 }).catch(function (response) {
                 $scope.success = null;
                 if (response.status === 400 && response.data === 'login already in use') {
                 $scope.errorUserExists = 'ERROR';
                 } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                 $scope.errorEmailExists = 'ERROR';
                 } else {
                 $scope.error = 'ERROR';
                 }
                 });

                 //$scope.mpoCommitteePersonInfo.user = $scope.registerAccount;



                 }
                 });*/

            };

            $scope.clear = function() {
                // $modalInstance.dismiss('cancel');
            };


            /*$scope.matchPass=function(pass,conPass){

             if(pass != conPass){
             $scope.notMatched=true;
             }else{
             $scope.notMatched=false;
             }



             };*/

            /* $scope.AddMore = function(){
             $scope.mpoCommitteePersonInfos.push(
             {
             contactNo: null,
             address: null,
             designation: null,
             orgName: null,
             activated: false,
             user: {},
             confirmPassword: null
             }
             );
             };
             $scope.AddMore();*/

            /*$scope.register = function () {
             if ($scope.registerAccount.password !== $scope.confirmPassword) {
             $scope.doNotMatch = 'ERROR';
             } else {
             $scope.registerAccount.langKey = $translate.use();
             $scope.doNotMatch = null;
             $scope.error = null;
             $scope.errorUserExists = null;
             $scope.errorEmailExists = null;
             $scope.registerAccount.activated = false;
             $scope.registerAccount.authorities = ["ROLE_USER"];

             Auth.createAccount($scope.registerAccount).then(function () {
             $scope.success = 'OK';
             }).catch(function (response) {
             $scope.success = null;
             if (response.status === 400 && response.data === 'login already in use') {
             $scope.errorUserExists = 'ERROR';
             } else if (response.status === 400 && response.data === 'e-mail address already in use') {
             $scope.errorEmailExists = 'ERROR';
             } else {
             $scope.error = 'ERROR';
             }
             });
             }
             };*/
        }]);

/* mpo-list-for-committee.controller.js */
angular.module('stepApp')
    .controller('MpoListCommittee',
        ['$scope', 'entity', '$state', '$modal','MpoCommitteeDescisionByLogin','MpoApplicationPendingForCommittee',  'MpoApplication', 'Employee', 'Institute' ,'EmployeeSearch', 'EmployeeInstitute', 'ParseLinks', 'MpoApplicationLogEmployeeCode' , 'MpoApplicationInstitute','MpoApplicationList',
            function ($scope, entity, $state, $modal,MpoCommitteeDescisionByLogin,MpoApplicationPendingForCommittee,  MpoApplication, Employee, Institute ,EmployeeSearch, EmployeeInstitute, ParseLinks, MpoApplicationLogEmployeeCode , MpoApplicationInstitute,MpoApplicationList) {

                // $scope.mpoAppliedLists = entity;

                $scope.employees = [];
                $scope.page = 0;
                MpoApplicationPendingForCommittee.query({}, function(result){
                    console.log('committee result found');
                    console.log(result.length);
                    $scope.mpoAppliedLists = result;
                });
                MpoCommitteeDescisionByLogin.query({}, function(result){
                    console.log(result[0].mpoApplicationId);
                });
                /*MpoApplicationList.query({status:1,page: $scope.page, size: 100}, function(result, headers) {
                 // $scope.pendingApplications = result;
                 console.log(result);
                 });
                 MpoApplicationList.query({status:0,page: $scope.page, size: 100}, function(result, headers) {
                 // $scope.pendingApplications = result;
                 console.log(result);
                 });
                 MpoApplication.query({page: $scope.page, size: 100}, function(result, headers) {
                 $scope.mpoApplications = result;
                 //  console.log($scope.mpoApplications);
                 });

                 $scope.loadPage = function(page) {
                 $scope.page = page;
                 $scope.loadAll();
                 };

                 $scope.search = function () {
                 EmployeeSearch.query({query: $scope.searchQuery}, function(result) {
                 $scope.employees = result;
                 }, function(response) {
                 if(response.status === 404) {
                 $scope.loadAll();
                 }
                 });
                 };

                 $scope.refresh = function () {
                 $scope.loadAll();
                 $scope.clear();
                 };

                 $scope.clear = function () {
                 $scope.employee = {
                 name: null,
                 nameEnglish: null,
                 father: null,
                 mother: null,
                 presentAddress: null,
                 permanentAddress: null,
                 gender: null,
                 dob: null,
                 zipCode: null,
                 registrationCertificateSubject: null,
                 registrationExamYear: null,
                 registrationCetificateNo: null,
                 indexNo: null,
                 bankName: null,
                 bankBranch: null,
                 bankAccountNo: null,
                 designation: null,
                 subject: null,
                 payScale: null,
                 payScaleCode: null,
                 month: null,
                 monthlySalaryGovtProvided: null,
                 monthlySalaryInstituteProvided: null,
                 gbResolutionReceiveDate: null,
                 gbResolutionAgendaNo: null,
                 circularPaperName: null,
                 circularPublishedDate: null,
                 recruitExamDate: null,
                 recruitApproveGBResolutionDate: null,
                 recruitPermitAgendaNo: null,
                 recruitmentDate: null,
                 presentInstituteJoinDate: null,
                 presentPostJoinDate: null,
                 dgRepresentativeType: null,
                 dgRepresentativeName: null,
                 dgRepresentativeDesignation: null,
                 dgRepresentativeAddress: null,
                 representativeType: null,
                 boardRepresentativeName: null,
                 boardRepresentativeDesignation: null,
                 boardRepresentativeAddress: null,
                 salaryCode: null,
                 id: null
                 };
                 };

                 // bulk operations start
                 $scope.areAllEmployeesSelected = false;

                 $scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                 for (var i = 0; i < employeeArray.length; i++)
                 {
                 employeeArray[i].isSelected = selectionValue;
                 }
                 };


                 $scope.import = function (){
                 for (var i = 0; i < $scope.employees.length; i++){
                 var employee = $scope.employees[i];
                 if(employee.isSelected){
                 //Employee.update(employee);
                 //TODO: handle bulk export
                 }
                 }
                 };

                 $scope.export = function (){
                 for (var i = 0; i < $scope.employees.length; i++){
                 var employee = $scope.employees[i];
                 if(employee.isSelected){
                 //Employee.update(employee);
                 //TODO: handle bulk export
                 }
                 }
                 };

                 $scope.deleteSelected = function (){
                 for (var i = 0; i < $scope.employees.length; i++){
                 var employee = $scope.employees[i];
                 if(employee.isSelected){
                 Employee.delete(employee);
                 }
                 }
                 };

                 $scope.sync = function (){
                 for (var i = 0; i < $scope.employees.length; i++){
                 var employee = $scope.employees[i];
                 if(employee.isSelected){
                 Employee.update(employee);
                 }
                 }
                 };

                 $scope.order = function (predicate, reverse) {
                 $scope.predicate = predicate;
                 $scope.reverse = reverse;
                 Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                 $scope.links = ParseLinks.parse(headers('link'));
                 $scope.employees = result;
                 $scope.total = headers('x-total-count');
                 });
                 };
                 // bulk operations end*/

            }]);
