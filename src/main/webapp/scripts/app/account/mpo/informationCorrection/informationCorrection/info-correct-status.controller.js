'use strict';

angular.module('stepApp')
    .controller('InfoCorrectStatusController',
    ['$scope', '$rootScope', '$state','InfoCorrectLogsByEmpCode', '$stateParams', 'Sessions', 'InstEmployeeCode','MpoApplicationLogEmployeeCode','InformationCorrection', 'PayScale', 'Jobapplication', 'Principal',
    function ($scope, $rootScope, $state,InfoCorrectLogsByEmpCode, $stateParams, Sessions, InstEmployeeCode,MpoApplicationLogEmployeeCode,InformationCorrection, PayScale, Jobapplication, Principal) {

        $scope.search = {};
        $rootScope.searchByTrackID = false;
        $scope.showSearchForm = true;
        $scope.showApplicationButtion=true;

        $scope.showMpoForm = function () {
            console.log($scope.search.code);
            InstEmployeeCode.get({'code':$scope.search.code},function(res){
                $scope.employee = res.instEmployee;
                $scope.employee.id = res.instEmployee.id;
                $rootScope.searchByTrackID = true;
                $state.go('mpo.details', {id: $scope.employee.id});
            });
            $scope.mpoNotFoundByCode = 'No Teacher found with this Tracking ID'
        };
        // if(!Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
        //     $scope.showApplicationButtion=false;
        // }


        if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
            Principal.identity().then(function (account) {
                $scope.account = account;
                InstEmployeeCode.get({'code':$scope.account.login},function(res){
                    $scope.employee = res.instEmployee;
                    //console.log($scope.employee.code);
                    InfoCorrectLogsByEmpCode.query({'code':$scope.employee.code}, function(result) {
                        $scope.infoCorrectionLogs = result;
                    });
                });
            });
        }
        /*else{
            InformationCorrection.get({id:$stateParams.id}, function(result) {
                console.log(result);
                InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                    console.log(res);
                    $scope.employee = res.instEmployee;
                    $scope.employee.id = res.instEmployee.id;

                    $state.go('infoCorrectAppStatus');
                    $scope.address = res.instEmpAddress;
                    $scope.applicantEducation = res.instEmpEduQualis;
                    $scope.applicantTraining = res.instEmplTrainings;
                    $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                    $scope.instEmplBankInfo = res.instEmplBankInfo;
                    // console.log($scope.applicantTraining);
                    // AttachmentEmployee.query({id: $scope.employee.id}, function(result){
                    //     $scope.attachments = result;
                    //
                    // });
                });

            });
        }*/
    }]);
