'use strict';

angular.module('stepApp')
    .controller('InformationCorrectionDetailController',
        ['$scope', '$state', '$rootScope', '$modal', '$stateParams','entity' , 'InformationCorrection','InstEmplBankInfoByInstEmplId', 'Principal', 'InstEmployeeCode', 'AttachmentByEmployeeAndName', 'MpoApplicationLogEmployeeCode', 'AttachmentByName','InformationCorrectionByEmployeeCode',
        function ($scope, $state, $rootScope, $modal, $stateParams, entity, InformationCorrection,InstEmplBankInfoByInstEmplId, Principal, InstEmployeeCode, AttachmentByEmployeeAndName, MpoApplicationLogEmployeeCode, AttachmentByName,InformationCorrectionByEmployeeCode) {
            $scope.informationCorrection = entity;

            $scope.showApproveButton=false;
            $scope.showApplicationButtion=true;
            $scope.applicantTraining = [];
            $scope.attachments = [];

            $scope.checkStatus = function(){
                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    return 1;
                }else if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                    return 2;
                }else if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                    return 3;
                }else if(Principal.hasAnyAuthority(['ROLE_DEO'])){
                    return 4;
                }else if(Principal.hasAnyAuthority(['ROLE_FRONTDESK'])){
                    return 5;
                }else if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])){
                    return 6;
                }else if(Principal.hasAnyAuthority(['ROLE_AD'])){
                    return 7;
                }else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                    return 9;
                }else if(Principal.hasAnyAuthority(['ROLE_DG'])){
                    return 10;
                }else if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                    return 11;
                }else{
                    return 0;
                }
            };

            if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                Principal.identity().then(function (account) {
                    $scope.account = account;
                    console.log('______________________');

                    InstEmployeeCode.get({'code':$scope.account.login},function(res){
                        console.log('OOOOOOOOOOOOOOOO');

                        $scope.employee = res.instEmployee;
                        InformationCorrectionByEmployeeCode.query({code: $scope.account.login}, function(result) {
                            $scope.informationCorrection = result;
                            console.log('rrrrrrrrrrrrrrrrrrrr');
                            console.log(result);
                            if ($scope.informationCorrection.id ==null ) {
                                $rootScope.setErrorMessage('Your Information Correction Application Not Completed Yet. Please Complete your Application Procedure First');
                                $state.go('mpo.application');
                            }else{
                                console.log('eeeeeeeeeeeeeeeeeee');
                                if(result.bankAccountNo != null){
                                    console.log('_________**************_____________');
                                    InstEmplBankInfoByInstEmplId.get({instEmplId:result.instEmployee.id},function(bankInfo){
                                        $scope.previousBankAccNo = bankInfo.bankAccountNo;
                                    });
                                }
                            }
                        });
                            $scope.employee = res.instEmployee;
                            $scope.employee.id = res.instEmployee.id;
                            $scope.address = res.instEmpAddress;
                            $scope.applicantEducation = res.instEmpEduQualis;
                            // $scope.applicantTraining = res.instEmplTrainings;

                            // angular.forEach(res.instEmplTrainings,function(results){
                            //     if(results.name === 'B.Ed' && results.status === true){
                            //         $scope.applicantTraining.push(results);
                            //     }
                            // });
                            $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                            $scope.instEmplBankInfo = res.instEmplBankInfo;
                            AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"InfoCorrect"}, function(bedAttachment){
                                $scope.attachments = bedAttachment;
                            });
                        });
                    });
                // });
            }
            else{
                InformationCorrection.get({id: $stateParams.id}, function(result) {
                    $scope.informationCorrection = result;
                    if(result.bankAccountNo != null){
                        InstEmplBankInfoByInstEmplId.get({instEmplId:result.instEmployee.id},function(bankInfo){
                            $scope.previousBankAccNo = bankInfo.bankAccountNo;
                        });
                    }
                    $scope.bedInfo = result;
                    if($stateParams.id>0){
                        $scope.showApplicationButtion=false;
                    }
                    if( $scope.checkStatus()!=0 & $scope.checkStatus() ==result.status){

                        $scope.showApproveButton=true;
                        $scope.timescaleId = result.id;
                    }else{
                        $scope.showApproveButton=false;
                    }

                    InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                        $scope.employee = res.instEmployee;
                        $scope.employee.id = res.instEmployee.id;
                        $scope.address = res.instEmpAddress;
                        $scope.applicantEducation = res.instEmpEduQualis;
                        $scope.applicantTraining = res.instEmplTrainings;
                        // angular.forEach(res.instEmplTrainings,function(results){
                        //     if(results.name === 'B.Ed' && results.status ===true){
                        //         $scope.applicantTraining.push(results);
                        //     }
                        // });
                        $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                        $scope.instEmplBankInfo = res.instEmplBankInfo;
                        AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"InfoCorrect"}, function(bedAttachment){
                            $scope.attachments = bedAttachment;
                        });
                        // MpoApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                        //     $scope.mpoApplicationlogs = result;
                        // });
                    });


                });
            }
            $scope.displayAttachment= function(applicaiton) {
                AttachmentByName.get({id:applicaiton.id},applicaiton,function(response){
                    var blob = b64toBlob(response.file, applicaiton.fileContentType);
                    $scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
                });
            };
            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {type: contentType});
                return blob;
            }
            $scope.previewImage = function (content, contentType,name) {
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                // call the modal
                $rootScope.showFilePreviewModal();
            };


    }]);
