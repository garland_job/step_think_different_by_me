'use strict';

angular.module('stepApp')
    .controller('MpoAOMappingController', function ($scope, MpoAOMapping, MpoAOMappingSearch, ParseLinks) {
        $scope.mpoAOMappings = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            MpoAOMapping.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.mpoAOMappings = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            MpoAOMapping.get({id: id}, function(result) {
                $scope.mpoAOMapping = result;
                $('#deleteMpoAOMappingConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            MpoAOMapping.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteMpoAOMappingConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            MpoAOMappingSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.mpoAOMappings = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.mpoAOMapping = {
                activeStatus: null,
                createDate: null,
                createBy: null,
                updateDate: null,
                updateBy: null,
                id: null
            };
        };
    });
