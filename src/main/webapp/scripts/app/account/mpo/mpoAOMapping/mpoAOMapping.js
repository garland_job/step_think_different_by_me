'use strict';

angular.module('stepApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('mpoAOMapping', {
                parent: 'mpo',
                url: '/mpoAOMappings',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'stepApp.mpoAOMapping.home.title'
                },
                views: {
                    'mpoView@mpo': {
                        templateUrl: 'scripts/app/account/mpo/mpoAOMapping/mpoAOMappings.html',
                        controller: 'MpoAOMappingController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('mpoAOMapping');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('mpoAOMapping.detail', {
                parent: 'mpo',
                url: '/mpoAOMapping/{id}',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'stepApp.mpoAOMapping.detail.title'
                },
                views: {
                    'mpoView@mpo': {
                        templateUrl: 'scripts/app/account/mpo/mpoAOMapping/mpoAOMapping-detail.html',
                        controller: 'MpoAOMappingDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('mpoAOMapping');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MpoAOMapping', function($stateParams, MpoAOMapping) {
                        return MpoAOMapping.get({id : $stateParams.id});
                    }]
                }
            })
            .state('mpoAOMapping.new', {
                parent: 'mpo',
                url: '/new',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },

                views: {
                    'mpoView@mpo': {
                        templateUrl: 'scripts/app/account/mpo/mpoAOMapping/mpoAOMapping-dialog.html',
                        controller: 'MpoAOMappingDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('mpoAOMapping');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MpoAOMapping', function($stateParams, MpoAOMapping) {
                        //return MpoTrade.get({id : $stateParams.id});
                    }]
                }
                //onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                //    $modal.open({
                //        templateUrl: 'scripts/app/account/mpo/mpoAOMapping/mpoAOMapping-dialog.html',
                //        controller: 'MpoAOMappingDialogController',
                //        size: 'lg',
                //        resolve: {
                //            entity: function () {
                //                return {
                //                    activeStatus: null,
                //                    createDate: null,
                //                    createBy: null,
                //                    updateDate: null,
                //                    updateBy: null,
                //                    id: null
                //                };
                //            }
                //        }
                //    }).result.then(function(result) {
                //        $state.go('mpoAOMapping', null, { reload: true });
                //    }, function() {
                //        $state.go('mpoAOMapping');
                //    })
                //}]
            })
            .state('mpoAOMapping.edit', {
                parent: 'mpo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_ADMIN'],
                },
                views: {
                    'mpoView@mpo': {
                        templateUrl: 'scripts/app/account/mpo/mpoAOMapping/mpoAOMapping-dialog.html',
                        controller: 'MpoAOMappingDialogController',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('mpoTrade');
                        $translatePartialLoader.addPart('mpoAOMapping');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MpoAOMapping', function($stateParams, MpoAOMapping) {
                        return MpoAOMapping.get({id : $stateParams.id});
                    }]
                }
                /*onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/account/mpo/mpoAOMapping/mpoAOMapping-dialog.html',
                        controller: 'MpoAOMappingDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MpoAOMapping', function(MpoAOMapping) {
                                return MpoAOMapping.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('mpoAOMapping', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]*/
            });
    });
