'use strict';

angular.module('stepApp')
    .controller('MpoAOMappingDetailController', function ($scope, $rootScope, $stateParams, entity, MpoAOMapping, InstLevel, Division, HrEmployeeInfo) {
        $scope.mpoAOMapping = entity;
        $scope.load = function (id) {
            MpoAOMapping.get({id: id}, function(result) {
                $scope.mpoAOMapping = result;
            });
        };
        var unsubscribe = $rootScope.$on('stepApp:mpoAOMappingUpdate', function(event, result) {
            $scope.mpoAOMapping = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
