'use strict';

angular.module('stepApp').controller('MpoAOMappingDialogController',
    ['$scope', '$stateParams', 'entity', 'MpoAOMapping', 'FindAllLevelByNonGovtCat', 'Division', 'HrEmployeeInfo','HrEmployeeInfoByDesignationName','$state',
        function($scope, $stateParams, entity, MpoAOMapping, FindAllLevelByNonGovtCat, Division, HrEmployeeInfo,HrEmployeeInfoByDesignationName,$state) {

        $scope.mpoAOMapping = entity;
        $scope.instlevels = FindAllLevelByNonGovtCat.query();
        $scope.divisions = Division.query();

            HrEmployeeInfoByDesignationName.query({designationName:'Attached Officer',desigType:'HRM',status:true},function(result){
             $scope.hremployeeinfos = result;
         });
        $scope.load = function(id) {
            MpoAOMapping.get({id : id}, function(result) {
                $scope.mpoAOMapping = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('stepApp:mpoAOMappingUpdate', result);
            //$modalInstance.close(result);
            $state.go('mpoAOMapping');
        };

        $scope.save = function () {
            if ($scope.mpoAOMapping.id != null) {
                MpoAOMapping.update($scope.mpoAOMapping, onSaveFinished);
            } else {
                MpoAOMapping.save($scope.mpoAOMapping, onSaveFinished);
            }
        };

        $scope.clear = function() {
            console.log('=====================Click=====================');
            $state.go('mpoAOMapping');
            $scope.mpoAOMapping = null;

            //$modalInstance.dismiss('cancel');
        };
}]);
