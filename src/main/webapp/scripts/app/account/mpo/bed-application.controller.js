angular.module('stepApp').controller('BEdAppController',
    ['$scope', '$stateParams', '$parse', '$state', 'entity', 'Auth','InstEmplTraining','BEdApplication', 'BEdApplicationCheck', 'TimeScaleApplicationCheck', 'Principal','TimeScaleApplication', '$rootScope', 'CurrentInstEmployee', 'User', 'Institute',  'AttachmentCategoryModule', 'Attachment', 'MpoApplication', 'InstEmployeeCode', 'InstEmployeeAddressInstitute', 'AttachmentEmployee', 'MpoApplicationCheck','AttachmentCategoryByApplicationName',
        function ($scope, $stateParams, $parse, $state, entity, Auth,InstEmplTraining,BEdApplication, BEdApplicationCheck, TimeScaleApplicationCheck, Principal,TimeScaleApplication, $rootScope, CurrentInstEmployee, User, Institute, AttachmentCategoryModule, Attachment, MpoApplication, InstEmployeeCode, InstEmployeeAddressInstitute, AttachmentEmployee, MpoApplicationCheck,AttachmentCategoryByApplicationName) {

            $scope.educationName = ['SSC/Dakhil', 'SSC (Vocational)', 'HSC/Alim', 'HSC (Vocational)', 'HSC (BM)', 'Honors/Degree', 'Masters'];
            $scope.currentSection = 'personal';
            $scope.applicantEducation = [];
            $scope.applicantTraining = [];
            $scope.applicantAttachment = [];
            $scope.applicantEducationCount = [0];
            $scope.applicantAttachmentCount = [0];
            $scope.applicantTrainingCount = [0];
            $scope.attachmentCategories = [];
            $scope.applicantTraining[0] = {};
            $scope.applicantTraining[0].gpa = '';
            $scope.applicantEducation[0] = {};
            $scope.applicantEducation[0].examYear = '';
            $scope.attachmentList = [];
            $scope.mpoAttachmentType = '';
            $scope.mpoApplication = {};
            $scope.search = {};
            $scope.mpoForm = false;
            $scope.showAddMoreButton = false;
            $scope.currentSelectItem = [];
            $scope.mpoFormHasError = true;
            $scope.duplicateError = false;
            $scope.instEmplTraining = {};
            $scope.bEdApplication = {};
            $scope.instEmplTraining.name = "B.Ed";

            CurrentInstEmployee.get({}, function (res) {
                $scope.employee = res;
                if ((res.level === 'SSC_VOC' || res.level === 'MADRASA_VOC' || res.level === 'SSC (VOC)' || res.level === 'Madrasha (VOC)' || res.level === 'SSC (VOCATIONAL)' || res.level === 'MADRASHA (VOCATIONAL)')) {
                    if (res.mpoAppStatus >= 5 && res.mpoActive === true && res.mpoActiveDate != null) {
                        //if(!getYearDifferenceWithCurrentDate(res.mpoActiveDate) >= 8 ){
                        //    $state.go('employeeInfo.personalInfo');
                        //    $rootScope.setErrorMessage('You are not eligible to apply. You must have at least 8 years experience.');
                        //}
                    } else if (res.mpoAppStatus < 5) {
                        $state.go('employeeInfo.personalInfo');
                        $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                    } else {
                        $state.go('employeeInfo.personalInfo');
                        $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                    }
                } else {
                    $state.go('mpo.bedApplicationStatus');
                    $rootScope.setErrorMessage('You are not eligible to apply. Contact with your institute admin.');
                }
            });
            Principal.identity().then(function (account) {
                BEdApplicationCheck.get({'code':account.login},function(res){
                    if(res.id == 0){
                    }else if(res.id > 0 && res.status < 2){
                        $scope.bEdApplication = res;
                    } else {
                        $state.go('mpo.bedApplicationStatus');
                    }
                });
            });
            $scope.toDay = new Date();
            $scope.cancel = function () {
                $state.go('mpo.application', null, {reload: true});
            };
            $scope.calendar = {
                opened: {},
                dateFormat: 'dd-MM-yyyy',
                dateOptions: {},
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.calendar.opened[which] = true;
                }
            };

            $scope.previewImage = function (content, contentType,name)
            {
                var blob = $rootScope.b64toBlob(content, contentType);
                $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                $rootScope.viewerObject.contentType = contentType;
                $rootScope.viewerObject.pageTitle = name;
                $rootScope.showFilePreviewModal();
            };
            $scope.employee = entity;

            AttachmentCategoryByApplicationName.get({name: 'BED-Attachment'}, function (result) {
                $scope.attachmentCategoryList = result;
            });

            Principal.identity().then(function (account) {
                $scope.account = account;
                if ($scope.isInArray('ROLE_ADMIN', $scope.account.authorities)) {
                    $scope.showTimeScaleForm();
                }else if ($scope.isInArray('ROLE_INSTITUTE', $scope.account.authorities)) {
                    $scope.showTimeScaleForm();
                }else if ($scope.isInArray('ROLE_INSTEMP', $scope.account.authorities)) {
                    $scope.search.code = $scope.account.login;
                    $scope.showTimeScaleForm();
                }
            });

            $scope.isInArray = function isInArray(value, array) {
                return array.indexOf(value) > -1;
            };

            $scope.save = function () {
                BEdApplicationCheck.get({'code': $scope.account.login}, function (res) {
                    if (res.id > 0 && res.status > 1) {
                        $state.go('mpo.employeeTrack', {}, {reload: true});
                    }
                });
                $scope.isSaving = true;
                $scope.instEmplTraining.status = true;
                $scope.instEmplTraining.instEmployee = $scope.employee;
                if($scope.instEmplTraining.id != null){
                    InstEmplTraining.update($scope.instEmplTraining);
                }else{
                    InstEmplTraining.save($scope.instEmplTraining);
                }

                if($scope.bEdApplication.id > 0){
                    BEdApplication.update($scope.bEdApplication);
                }else{
                    $scope.bEdApplication.instEmployee = $scope.employee;
                    BEdApplication.save($scope.bEdApplication);
                }
                //employee attachment
                angular.forEach($scope.applicantAttachmentCount, function (value, key) {
                    if ($scope.applicantAttachment[value].attachment != '') {
                        $scope.applicantAttachment[value].instEmployee = {};
                        $scope.applicantAttachment[value].name = $scope.applicantAttachment[value].attachmentCategory.attachmentName;
                        $scope.applicantAttachment[value].instEmployee.id = $scope.employee.id;
                        $scope.applicantAttachment[value].status = true;
                        Attachment.save($scope.applicantAttachment[value]);
                    }
                });
                $rootScope.setSuccessMessage('Application Submitted Successfully.');
                $scope.isSaving = false;
                $state.go('mpo.bedApplicationStatus',{},{reload:true});

            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.showTimeScaleForm = function () {
                InstEmployeeCode.get({'code': $scope.search.code}, function (res) {
                    $scope.employee = res.instEmployee;
                    $scope.address = res.instEmpAddress;
                    /*$scope.applicantEducation = res.instEmpEduQualis;*/
                    $scope.applicantEducation = res.instEmpEduQualisList;
                    //$scope.applicantTraining = res.instEmplTrainings;
                    angular.forEach(res.instEmplTrainings,function(results){
                        if(results.name === 'B.Ed'){
                            $scope.instEmplTraining = results;
                        }
                    });
                    $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                    $scope.instEmplBankInfo = res.instEmplBankInfo;
                    $scope.mpoForm = true;
                });
            };

            $scope.hideMpoForm = function () {
                $scope.mpoForm = false;
            };

            $scope.timescaleNext = function (current, next) {
                $scope.currentSection = next;
                $scope.prevSection = current;
            }
            $scope.timescalePrev = function (current, prev) {
                $scope.currentSection = prev;
                $scope.nextSection = current;
            }
            $scope.addMoreEducation = function () {
                $scope.applicantEducationCount.push($scope.applicantEducationCount.length);
                $scope.applicantEducation[$scope.applicantEducationCount.length].examYear = '';

            }
            $scope.addMoreAttachment = function () {
                //removeItem
                if (!inArray($scope.applicantAttachmentCount.length, $scope.applicantAttachmentCount)) {
                    $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
                } else {
                    $scope.applicantAttachmentCount.length++;
                    $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
                }
                $scope.showAddMoreButton = false;
            }
            $scope.removeAttachment = function (attachment) {
                $scope.showAddMoreButton = true;
                $scope.mpoFormHasError = true;
                var index = $scope.applicantAttachmentCount.indexOf(attachment);
                $scope.applicantAttachmentCount.splice(index, 1);

                if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = false;
                }
            }
            $scope.addMoreTraining = function () {
                $scope.applicantTrainingCount.push($scope.applicantTrainingCount.length);
                $scope.applicantTraining[$scope.applicantTrainingCount.length].gpa = '';
            }

            //$scope.educationHtml = function () {
            //
            //}
            $scope.setAttachment = function($index, attachment,noAttach) {
                if(noAttach ){
                    if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                        $scope.showAddMoreButton = false;
                    }else{
                        $scope.showAddMoreButton = true;
                    }
                }
                try{
                    if(attachment==""){
                        $scope.mpoFormHasError = true;
                    }else{
                        if(!noAttach && attachment.file)
                            $scope.showAddMoreButton = true;
                        if(noAttach && (attachment.remarks == undefined || attachment.remarks==""))
                            $scope.showAddMoreButton = true;
                    }
                    attachment.attachmentCategory = angular.fromJson(attachment.attachment);
                    if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                        $scope.showAddMoreButton = false;
                    }

                    if(attachment.attachmentCategory.id)
                    {
                        $scope.currentSelectItem[$index] = attachment.attachmentCategory.id;
                    }
                }catch(e) {
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = true;
                    $scope.currentSelectItem.splice($index, 1);
                }

                //if($scope.attachmentCategoryList.length-1 == arrayUnique($scope.currentSelectItem).length){
                if($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length){
                    $scope.mpoFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                            $scope.mpoFormHasError = true;
                        }else {
                            if (value.fileName) {
                                $scope.mpoFormHasError = true;
                            }
                        }
                    });
                }
                else{
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        if (!value.noAttachment && (value.fileName)) {
                            $scope.mpoFormHasError = true;
                        }
                    });
                }

                if(arrayUnique($scope.currentSelectItem).length != $scope.currentSelectItem.length)
                    $scope.duplicateError = true;
                else
                    $scope.duplicateError = false;

                if($scope.duplicateError){
                    $scope.showAddMoreButton = false;
                }
            };

            function getYearDifferenceWithCurrentDate(dateString) {
                var today = new Date();
                var otherDate = new Date(dateString);
                var year = today.getFullYear() - otherDate.getFullYear();
                var m = today.getMonth() - otherDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < otherDate.getDate())) {
                    year--;
                }
                return year;
            };
            $scope.setBedAttachment = function ($file, instEmplExperience) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function() {
                            instEmplExperience.attachment = base64Data;
                            instEmplExperience.attachmentContentType = $file.type;
                            instEmplExperience.attachmentName = $file.name;
                        });
                    };
                }
            };

            $scope.selectNoAttachment = function(val,val2,file,remarks)
            {

                if(val && val2){
                    // if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length-1){
                    if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){

                        $scope.showAddMoreButton = false;
                        $scope.mpoFormHasError = false;
                        angular.forEach($scope.applicantAttachment, function (value, key) {

                            if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                                $scope.mpoFormHasError = true;
                            }
                        });

                    }else{
                        if (val && (remarks == undefined || remarks=="")) {
                            $scope.mpoFormHasError = true;
                            $scope.showAddMoreButton = false;
                        }else{
                            $scope.showAddMoreButton = true;
                        }
                    }
                }else{
                    if(file==undefined){
                        $scope.mpoFormHasError = true;
                        $scope.showAddMoreButton = false
                    }else{
                        $scope.showAddMoreButton = true;
                    }

                    if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                        $scope.showAddMoreButton = false;
                    }
                }
                if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }
            }

            $scope.remarksChange = function(noAttachment,remarks)
            {
                if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                            $scope.mpoFormHasError = true;
                        }
                    });
                }else{
                    if(noAttachment && !(remarks == undefined || remarks=="")){
                        $scope.showAddMoreButton = true;
                    }else{
                        $scope.showAddMoreButton = false;
                    }
                }
            }
            function inArray(needle, haystack) {
                var length = haystack.length;
                for (var i = 0; i < length; i++) {
                    if (typeof haystack[i] == 'object') {
                        if (arrayCompare(haystack[i], needle)) return true;
                    } else {
                        if (haystack[i] == needle) return true;
                    }
                }
                return false;
            }

            function arrayUnique(a) {
                return a.reduce(function (p, c) {
                    if (p.indexOf(c) < 0) p.push(c);
                    return p;
                }, []);
            };

            function arrayCompare(a1, a2) {
                if (a1.length != a2.length) return false;
                var length = a2.length;
                for (var i = 0; i < length; i++) {
                    if (a1[i] !== a2[i]) return false;
                }
                return true;
            }

            $scope.abbreviate = function (text) {
                if (!angular.isString(text)) {
                    return '';
                }
                if (text.length < 30) {
                    return text;
                }
                return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
            };

            $scope.byteSize = function (base64String) {
                if (!angular.isString(base64String)) {
                    return '';
                }
                function endsWith(suffix, str) {
                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }

                function paddingSize(base64String) {
                    if (endsWith('==', base64String)) {
                        return 2;
                    }
                    if (endsWith('=', base64String)) {
                        return 1;
                    }
                    return 0;
                }

                function size(base64String) {
                    return base64String.length / 4 * 3 - paddingSize(base64String);
                }

                function formatAsBytes(size) {
                    return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
                }

                return formatAsBytes(size(base64String));
            };
            $scope.setFile = function ($file, attachment) {
                $scope.showAddMoreButton = true;
                $scope.mpoFormHasError = true;
                try{
                    if ($file) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL($file);
                        fileReader.onload = function (e) {
                            console.log('--------------');
                            var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                            $scope.$apply(function() {
                                try{
                                    attachment.file = base64Data;
                                    attachment.fileContentType = $file.type;
                                    attachment.fileName = $file.name;
                                }catch(e) {
                                    $scope.showAddMoreButton = false;
                                }
                            });
                        };
                        if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                            $scope.showAddMoreButton = false;
                        }
                        if($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length)
                            $scope.mpoFormHasError = false;
                        else
                            $scope.mpoFormHasError = true;
                    }
                }catch(e) {
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = true;
                }
            };

            $scope.changeActionFile = function ($file) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            $scope.timeScaleApplication.disActionFile = base64Data;
                            $scope.timeScaleApplication.disActionFileName = $file.name;
                        });
                    };
                }
            };

        }]);

/* bed-application-checklist.controller.js */
angular.module('stepApp')
    .controller('BedApplicationStatusController',
        ['$scope', '$rootScope','BEdApplicationLogEmployeeCode', '$state', '$stateParams', 'Sessions',  'InstEmployeeCode','MpoApplicationLogEmployeeCode','MpoApplication', 'PayScale', 'Jobapplication', 'Principal',
            function ($scope, $rootScope,BEdApplicationLogEmployeeCode, $state, $stateParams, Sessions, InstEmployeeCode,MpoApplicationLogEmployeeCode,MpoApplication, PayScale, Jobapplication, Principal) {

                //$scope.showSearchForm = true;
                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    Principal.identity().then(function (account) {
                        $scope.account = account;
                        BEdApplicationLogEmployeeCode.get({'code':$scope.account.login}, function(result) {
                            $scope.applicationlogs = result;
                        });
                    });
                }

    }]);

/* bed-application-status.controller.js */

angular.module('stepApp')
    .controller('BedApplicationStatusControllerPre',
        ['$scope', '$rootScope', '$state','BEdApplicationLogEmployeeCode', '$stateParams', 'Sessions',  'InstEmployeeCode','MpoApplicationLogEmployeeCode','MpoApplication', 'PayScale', 'Jobapplication', 'Principal',
            function ($scope, $rootScope, $state,BEdApplicationLogEmployeeCode, $stateParams, Sessions, InstEmployeeCode,MpoApplicationLogEmployeeCode,MpoApplication, PayScale, Jobapplication, Principal) {

                $scope.search = {};
                $rootScope.searchByTrackID = false;
                $scope.showSearchForm = true;
                $scope.showApplicationButtion=true;

                $scope.showMpoForm = function () {
                    InstEmployeeCode.get({'code':$scope.search.code},function(res){
                        $scope.employee = res.instEmployee;
                        $scope.employee.id = res.instEmployee.id;
                        $rootScope.searchByTrackID = true;
                        $state.go('mpo.details', {id: $scope.employee.id});
                    });
                    $scope.mpoNotFoundByCode = 'No Teacher found with this Tracking ID'
                };
                if(!Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    $scope.showApplicationButtion=false;
                }


                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    Principal.identity().then(function (account) {
                        $scope.account = account;
                        $scope.showSearchForm = false;
                        InstEmployeeCode.get({'code':$scope.account.login},function(res){
                            $scope.employee = res.instEmployee;
                            BEdApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                                console.log('>>>>>>>>>>>>>>');
                                console.log(result);
                                $scope.mpoApplicationlogs = result;
                                if($scope.mpoApplicationlogs.length>0){
                                    $scope.showApplicationButtion=false;
                                }
                            });
                        });
                    });
                }else{
                    MpoApplication.get({id:$stateParams.id}, function(result) {
                        console.log(result);
                        InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                            $scope.employee = res.instEmployee;
                            $scope.employee.id = res.instEmployee.id;
                            $state.go('mpo.employeeTrack');
                            $scope.address = res.instEmpAddress;
                            $scope.applicantEducation = res.instEmpEduQualis;
                            $scope.applicantTraining = res.instEmplTrainings;
                            $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                            $scope.instEmplBankInfo = res.instEmplBankInfo;
                            console.log($scope.applicantTraining);
                            AttachmentEmployee.query({id: $scope.employee.id}, function(result){
                                $scope.attachments = result;

                            });
                        });

                    });
                }
            }]);

/* bed-approve-dialog.controller.js */

angular.module('stepApp')
    .controller('BEDApproveDialogController',
        ['$scope','$rootScope','$stateParams', '$state', 'Sessions', 'Principal', '$modalInstance','BEdApplication','AllForwaringList','ForwardBedApplication',
            function ($scope,$rootScope,$stateParams, $state, Sessions, Principal, $modalInstance,BEdApplication,AllForwaringList,ForwardBedApplication) {
                $scope.approveVal = false;
                $scope.forward = {};
                BEdApplication.get({id: $stateParams.id}, function(result){
                    $scope.bEdApplication = result;
                    if(result.instEmployee.instLevel.name === 'MADRASA_BM' || result.instEmployee.instLevel.name === 'HSC_BM' || result.instEmployee.instLevel.name === 'Madrasha (BM)' || result.instEmployee.instLevel.name === 'HSC (BM)'){
                        AllForwaringList.get({type:'BM'}, function(result){
                            $scope.forwardingList=result;
                            if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                                $scope.forwardingList.splice(1,1);
                            }
                        });
                    }else{
                        AllForwaringList.get({type:'VOC'}, function(result){
                            $scope.forwardingList=result;
                            if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                                $scope.forwardingList.splice(1,1);
                            }
                        });
                    }

                    if(result.status > 6){
                        $scope.approveVal = true;
                    };

                });

                $scope.confirmApprove = function(forward){
                    if($scope.bEdApplication.status > 6){
                        if(Principal.hasAnyAuthority(['ROLE_DG'])){
                            $scope.bEdApplication.dgFinalApproval=true;
                            $scope.bEdApplication.dgComments=$scope.remark;
                        }
                        if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                            $scope.bEdApplication.directorComment=$scope.remark;
                        }
                        if(Principal.hasAnyAuthority(['ROLE_AD'])){
                            $scope.bEdApplication.directorComment = $scope.remark;
                        }
                        BEdApplication.update($scope.bEdApplication, onSaveSuccess, onSaveError);
                    }else{
                        ForwardBedApplication.forward({forwardTo:$scope.forward.forwardTo.code,cause: $scope.remark,memoNo: $scope.bEdApplication.memoNo},$scope.bEdApplication, onSaveSuccess, onSaveError);
                    }

                };

                var onSaveSuccess = function(result){
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('Application Approved Successfully.');
                    $state.go('bedDetails', null, { reload: true });
                }
                var onSaveError = function(result){

                }
                $scope.clear = function(){
                    $modalInstance.close();
                    $state.go('bedDetails');
                }

            }]);

/*bed-deny-dialog.controller.js */
angular.module('stepApp')
    .controller('BEDDenyDialogController',
        ['$scope','MpoApplication','$stateParams', 'DateUtils', '$state', '$modalInstance', 'BEdApplicationDecline',
            function ($scope,MpoApplication, $stateParams, DateUtils, $state, $modalInstance,BEdApplicationDecline) {
                $scope.causeDeny = "";
                $scope.confirmDecline = function(){
                    BEdApplicationDecline.decline({id: $stateParams.id, cause: $scope.causeDeny},{}, onSaveSuccess, onSaveError);
                };
                var onSaveSuccess = function(result){
                    $modalInstance.close();
                };
                var onSaveError = function(result){
                    console.log(result);
                };
                $scope.clear = function(){
                    $modalInstance.close();
                    window.history.back();
                };
            }]);

/* bed-details.controller.js */

angular.module('stepApp')
    .controller('BEdDetailsController',
        ['$scope', '$state', '$modal', 'BEdApplication','entity','BEdApplicationCheck', 'ParseLinks','InstEmployeeCode','MpoApplication','$stateParams','AttachmentEmployee','Principal','MpoApplicationCheck', 'BEdApplicationLogEmployeeCode','AttachmentByName','$rootScope','AttachmentByEmployeeAndName',
            function ($scope, $state, $modal, BEdApplication,entity,BEdApplicationCheck, ParseLinks,InstEmployeeCode,MpoApplication,$stateParams,AttachmentEmployee,Principal,MpoApplicationCheck, BEdApplicationLogEmployeeCode,AttachmentByName,$rootScope,AttachmentByEmployeeAndName) {
                $scope.url = null;
                $scope.employee = entity;
                $scope.bedInfo = null;
                $scope.isCollapsed = false;
                $scope.showApproveButton=false;
                $scope.showApplicationButtion=true;
                $scope.applicantTraining = [];
                $scope.attachments = [];

                $scope.checkStatus = function(){
                    if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                        return 1;
                    }else if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                        return 2;
                    }else if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                        return 3;
                    }else if(Principal.hasAnyAuthority(['ROLE_DEO'])){
                        return 4;
                    }else if(Principal.hasAnyAuthority(['ROLE_FRONTDESK'])){
                        return 5;
                    }else if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])){
                        return 6;
                    }else if(Principal.hasAnyAuthority(['ROLE_AD'])){
                        return 7;
                    }else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                        return 9;
                    }else if(Principal.hasAnyAuthority(['ROLE_DG'])){
                        return 10;
                    }else if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                        return 11;
                    }else{
                        return 0;
                    }
                };
                if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                    Principal.identity().then(function (account) {
                        $scope.account = account;
                        InstEmployeeCode.get({'code':$scope.account.login},function(res){
                            $scope.employee = res.instEmployee;
                            BEdApplicationCheck.get({'code':$scope.employee.code}, function(result) {
                                if(result.id>0){
                                    $scope.showApplicationButtion=false;
                                }else{
                                    $rootScope.setErrorMessage("You Did not complete your BED Application. Please Complete Your Application If You are Eligible.");
                                    $state.go('mpo.application');
                                }
                                $scope.employee = res.instEmployee;
                                $scope.employee.id = res.instEmployee.id;
                                $scope.address = res.instEmpAddress;
                                $scope.applicantEducation = res.instEmpEduQualis;
                                //$scope.applicantTraining = res.instEmplTrainings;

                                angular.forEach(res.instEmplTrainings,function(results){
                                   if(results.name === 'B.Ed' && results.status === true){
                                       $scope.applicantTraining.push(results);
                                   }
                                });
                                $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                                $scope.instEmplBankInfo = res.instEmplBankInfo;
                                AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"BED-Attachment"}, function(bedAttachment){
                                     $scope.attachments = bedAttachment;
                                });
                            });
                        });
                    });
                }else{
                    BEdApplication.get({id:$stateParams.id}, function(result) {
                        $scope.bedInfo = result;
                        if($stateParams.id>0){
                            $scope.showApplicationButtion=false;
                        }
                        if( $scope.checkStatus()!=0 & $scope.checkStatus() ==result.status){
                            $scope.showApproveButton=true;
                            $scope.timescaleId = result.id;
                        }else{
                            $scope.showApproveButton=false;
                        }
                        InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                            $scope.employee = res.instEmployee;
                            $scope.employee.id = res.instEmployee.id;
                            $scope.address = res.instEmpAddress;
                            $scope.applicantEducation = res.instEmpEduQualis;
                            //$scope.applicantTraining = res.instEmplTrainings;
                            angular.forEach(res.instEmplTrainings,function(results){
                                if(results.name === 'B.Ed' && results.status ===true){
                                    $scope.applicantTraining.push(results);
                                }
                            });
                            $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                            $scope.instEmplBankInfo = res.instEmplBankInfo;
                            AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:$scope.employee.designationSetup.designationInfo.designationName}, function(result){
                                $scope.attachments = result;
                            });
                            BEdApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                                $scope.mpoApplicationlogs = result;
                            });
                        });

                    });
                }

                $scope.attachment = function(){
                    AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"BED-Attachment"}, function(bedAttachment){
                        $scope.attachments = bedAttachment;
                    });
                };

                $scope.displayAttachment= function(applicaiton) {
                    AttachmentByName.get({id:applicaiton.id},applicaiton,function(response){
                        var blob = b64toBlob(response.file, applicaiton.fileContentType);
                        $scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
                    });
                };

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }

                        var byteArray = new Uint8Array(byteNumbers);

                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }
                $scope.previewImage = function (content, contentType,name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

            }])


/* bed-list.controller.js */
angular.module('stepApp')
    .controller('BEdListController',
        ['$scope', 'entity', '$state','BEdApplication', '$modal',  'ParseLinks', 'BEdApplicationList','Principal','InstLevelByName','BEdSummaryList','BEdApplicationsByLevel',
            function ($scope, entity, $state,BEdApplication, $modal,  ParseLinks, BEdApplicationList,Principal,InstLevelByName,BEdSummaryList,BEdApplicationsByLevel) {

                $scope.bEdAppliedLists = entity;
                $scope.summaryList = [];
                $scope.employees = [];
                $scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.page = 0;
                $scope.mpoApplicationForMPoMeeting = [];
                $scope.showSummary = false;

                //BEdApplicationList.query({status:1,page: $scope.page, size: 100}, function(result, headers) {
                //    // $scope.pendingApplications = result;
                //    console.log(result);
                //});
                //BEdApplicationList.query({status:0,page: $scope.page, size: 100}, function(result, headers) {
                //    console.log(result);
                //});
                //BEdApplication.query({page: $scope.page, size: 100}, function(result, headers) {
                //    $scope.mpoApplications = result;
                //});

                if(entity.pending === "pending" && (Principal.hasAnyAuthority(['ROLE_DIRECTOR']) || Principal.hasAnyAuthority(['ROLE_DG']))){
                    $scope.bEdAppliedLists =[];
                    BEdSummaryList.query({}, function(result){
                        $scope.summaryList = result;
                    });
                }

                $scope.showSummary1 = function () {
                    $scope.showSummary = true;
                };
                $scope.createSummary = function () {
                    //$scope.bEdAppliedLists =[];
                    BEdSummaryList.query({}, function(result){
                        $scope.summaryList = result;
                        if(result.length == 0){
                            alert("No application found!");
                        }
                    },function(response) {
                        if(response.status === 404) {
                            alert("No application found!");
                        }
                    });
                };

                $scope.all = function () {
                    $scope.showSummary = false;
                    $scope.bEdAppliedLists = entity;
                };
                $scope.ssc = function () {
                    $scope.showSummary = false;
                    $scope.bEdAppliedLists =[];
                    InstLevelByName.get({name: "SSC (VOC)"}, function(result){
                        BEdApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                            $scope.bEdAppliedLists = result2;
                        });
                    });
                };
                // $scope.hsc = function () {
                //     InstLevelByName.get({name: "HSC (BM)"}, function(result){
                //         console.log(result);
                //         BEdApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                //             $scope.hscBmApplicationLists = result2;
                //         });
                //     });
                // };
                $scope.mdvoc = function () {
                    $scope.showSummary = false;
                    $scope.bEdAppliedLists =[];
                    InstLevelByName.get({name: "Madrasha (VOC)"}, function(result){
                        BEdApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                            $scope.bEdAppliedLists = result2;
                        });
                    });
                };
                // $scope.mdbm = function () {
                //     InstLevelByName.get({name: "Madrasha (BM)"}, function(result){
                //         console.log(result);
                //         BEdApplicationsByLevel.query({status: 0, levelId: result.id}, function(result2){
                //             $scope.madrasaBmApplicationLists = result2;
                //         });
                //     });
                // };
                // var onForwardSuccess = function (index) {
                //
                // };

                $scope.forwardSummaryList = function () {
                    angular.forEach($scope.summaryList, function (data) {
                        BEdApplication.get({id: data.MPO_ID}, function(result){
                            result.adForwarded = true;
                            BEdApplication.update(result);
                        });
                    });
                    location.reload();
                };

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.isAllSelected = false;
                $scope.updateAllSelection = function (mpoApp, selectionValue) {
                    $scope.mpoApplicationForMPoMeeting = [];
                    for (var i = 0; i < mpoApp.length; i++) {
                        mpoApp[i].isSelected = selectionValue;
                        if(mpoApp[i].isSelected == true){
                            $scope.mpoApplicationForMPoMeeting.push(mpoApp[i]);
                        }else {
                            var index = $scope.mpoApplicationForMPoMeeting.indexOf(mpoApp[i]);
                            $scope.mpoApplicationForMPoMeeting.splice(index);
                        }
                    }
                };

                $scope.updateSelection = function (mpoApp) {
                    if(mpoApp.isSelected == true){
                        $scope.mpoApplicationForMPoMeeting.push(mpoApp);
                    }else {
                        var index = $scope.mpoApplicationForMPoMeeting.indexOf(mpoApp);
                        $scope.mpoApplicationForMPoMeeting.splice(index);
                    }
                };

            }]);

/* mpo-bed-application.controller.js  */
// angular.module('stepApp').controller('MPOBedApplicationController',
//     ['$scope', 'Employee', 'Institute',
//         function ($scope, Employee, Institute) {
//
//
//         }]);
angular.module('stepApp')
    .controller('BEdApplicationCheckListController',
        ['$scope', '$rootScope', '$state', '$stateParams', 'BEdApplicationLogEmployeeCode',
            function ($scope, $rootScope, $state,$stateParams,  BEdApplicationLogEmployeeCode) {

                $scope.showSearchForm = true;
                $scope.showApprove = false;
                $scope.showApproveDecline = false;
                $scope.bEdApplication = {};

                BEdApplicationLogEmployeeCode.get({'code':$stateParams.code}, function(result) {
                    $scope.bEdApplicationlogs = result;
                    // $scope.bEdApplication = result[0].timeScaleApplicationId;

                });


            }]);
