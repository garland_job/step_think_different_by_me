//'use strict';
//
//angular.module('stepApp')
//    .controller('BedPayCodeAssignController',
//     ['$scope', 'entity', '$state','$stateParams', 'InstEmployee', 'MpoApplication','BEdApplication','InstEmplPayscaleHist','GetPayScaleByCode','InstEmplRecruitInfoByEmp','InstEmplRecruitInfo',
//     function ($scope, entity, $state,$stateParams, InstEmployee, MpoApplication,BEdApplication,InstEmplPayscaleHist,GetPayScaleByCode,InstEmplRecruitInfoByEmp,InstEmplRecruitInfo) {
//
//        $scope.mpoApplication = entity;
//        $scope.instEmployee = {};
//        $scope.payScaleDetails = '';
//        $scope.payScaleID = '';
//        $scope.mpoApplication.instEmployee = {};
//        $scope.instEmplPayscaleHist = {};
//
//        $scope.mpoApplication.instEmployee.payScale = {};
//
//        if ($stateParams.id !=null){
//            BEdApplication.get({id : $stateParams.id},function(result){
//                $scope.mpoApplication=result;
//                if($scope.mpoApplication.instEmployee.payScale !=null){
//                    $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
//                    $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
//                    console.log($scope.payScaleDetails);
//                }
//            });
//         }
//         $scope.showPayScaleDetails = function(){
//             console.log($scope.payScaleID);
//             GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
//                 $scope.payScaleDetails = result;
//                 console.log($scope.payScaleDetails);
//             });
//         };
//
//        $scope.save = function () {
//             console.log('&&&&&&&&&&&&7777');
//            if($scope.payScaleID == '') {
//                console.log('Please Select a PayScale');
//                return false;
//            }
//
//           $scope.isSaving = true;
//
//           $scope.instEmployee = $scope.mpoApplication.instEmployee;
//
//            GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
//                $scope.instEmployee.payScale = result;
//                console.log($scope.instEmployee);
//                $scope.instEmployee.mpoActive=1;
//                InstEmployee.update($scope.instEmployee);
//                $scope.instEmplPayscaleHist.activationDate = new Date();
//                $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
//                $scope.instEmplPayscaleHist.payScale = result;
//                InstEmplPayscaleHist.save($scope.instEmplPayscaleHist, onSaveSuccess);
//            });
//        };
//
//         var onSaveSuccess = function (result) {
//             InstEmplRecruitInfoByEmp.query({id:result.instEmployee.id}, function(recruitInfo){
//                 //console.log("previous pay scale:"+recruitInfo.payScale.id);
//                 recruitInfo.payScale = result.instEmployee.payScale;
//                 recruitInfo = InstEmplRecruitInfo.update(recruitInfo);
//                 //console.log("current pay scale:"+recruitInfo.payScale.id);
//                 $state.go('mpo.bedpayCodePendingList',{},{reload:true});
//             });
//             $scope.isSaving = false;
//         };
//
//         var onSaveError = function (result) {
//             $scope.isSaving = false;
//         };
//
//    }]);
