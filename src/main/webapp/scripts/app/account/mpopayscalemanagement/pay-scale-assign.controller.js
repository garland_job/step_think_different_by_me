'use strict';

angular.module('stepApp')
    .controller('PayScaleAssignController',
        ['$scope', 'entity', '$state','$rootScope','$stateParams', 'InstEmployee', 'MpoApplication','InstEmplPayscaleHist','InstEmplRecruitInfo','GetPayScaleByCode','FilledUpInstVacancy','UpdatePayScaleOfInstEmpRecruit','InstEmplRecruitInfoByEmp',
            function ($scope, entity, $state, $rootScope, $stateParams, InstEmployee, MpoApplication,InstEmplPayscaleHist, InstEmplRecruitInfo,GetPayScaleByCode,FilledUpInstVacancy,UpdatePayScaleOfInstEmpRecruit,InstEmplRecruitInfoByEmp) {

                $scope.mpoApplication = entity;
                $scope.instEmployee = {};
                $scope.payScaleDetails = '';
                $scope.payScaleID = '';
                //$scope.mpoApplication.instEmployee = {};
                $scope.instEmplPayscaleHist = {};
                //$scope.mpoApplication.instEmployee.payScale = {};

                MpoApplication.get({id : $stateParams.id},function(result){
                    $scope.mpoApplication=result;
                    if($scope.mpoApplication.instEmployee.payScale !=null){
                        $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
                        $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
                    }
                });

                $scope.showPayScaleDetails = function(){
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.payScaleDetails = result;
                    });
                };

                var onSaveSuccess = function (result) {
                    InstEmplRecruitInfoByEmp.query({id:result.instEmployee.id}, function(recruitInfo){
                        //console.log("previous pay scale:"+recruitInfo.payScale.id);
                        recruitInfo.payScale = result.instEmployee.payScale;
                        InstEmplRecruitInfo.update(recruitInfo);
                        //console.log("current pay scale:"+recruitInfo.payScale.id);
                        $rootScope.setSuccessMessage('Pay Scale Assign successfully');
                        $state.go('mpo.payScaleApprovedList',{},{reload:true});
                    });
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.save = function () {
                    if($scope.payScaleID == '') {
                        console.log('Please Select a PayScale');
                        return false;
                    }
                    $scope.isSaving = true;
                    $scope.instEmployee = $scope.mpoApplication.instEmployee;
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.instEmployee.payScale = result;
                        $scope.instEmployee.mpoActive=1;
                        InstEmployee.update($scope.instEmployee);
                        //InstEmplRecruitInfoByEmp.query({id:$scope.instEmployee.id},function(recruitmentInfo){
                        //    $scope.instEmplRecruitInfo = recruitmentInfo;
                        //    $scope.instEmplRecruitInfo.payScale = result;
                        //    InstEmplRecruitInfo.save($scope.instEmplRecruitInfo);
                        //});
                        UpdatePayScaleOfInstEmpRecruit.update({id:$scope.instEmployee.id},result);
                        //FilledUpInstVacancy.filled($scope.instEmployee);
                        $scope.instEmplPayscaleHist.activationDate = new Date();
                        $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
                        $scope.instEmplPayscaleHist.payScale = result;
                        InstEmplPayscaleHist.save($scope.instEmplPayscaleHist, onSaveSuccess,onSaveError);
                    });
                };



            }]);


/* payscale-list-by-status.controller.js*/
angular.module('stepApp')
    .controller('PayScaleListByStatusController',
        ['$scope', 'entity', '$state', '$modal',  'Principal','MpoApplication', 'MpoApplicationPayScale',
            function ($scope, entity, $state, $modal,  Principal,MpoApplication, MpoApplicationPayScale) {

                $scope.payScalePendingLists = entity;

                $scope.employees = [];
                $scope.page = 0;

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.employee = {
                        name: null,
                        nameEnglish: null,
                        father: null,
                        mother: null,
                        presentAddress: null,
                        permanentAddress: null,
                        gender: null,
                        dob: null,
                        zipCode: null,
                        registrationCertificateSubject: null,
                        registrationExamYear: null,
                        registrationCetificateNo: null,
                        indexNo: null,
                        bankName: null,
                        bankBranch: null,
                        bankAccountNo: null,
                        designation: null,
                        subject: null,
                        payScale: null,
                        payScaleCode: null,
                        month: null,
                        monthlySalaryGovtProvided: null,
                        monthlySalaryInstituteProvided: null,
                        gbResolutionReceiveDate: null,
                        gbResolutionAgendaNo: null,
                        circularPaperName: null,
                        circularPublishedDate: null,
                        recruitExamDate: null,
                        recruitApproveGBResolutionDate: null,
                        recruitPermitAgendaNo: null,
                        recruitmentDate: null,
                        presentInstituteJoinDate: null,
                        presentPostJoinDate: null,
                        dgRepresentativeType: null,
                        dgRepresentativeName: null,
                        dgRepresentativeDesignation: null,
                        dgRepresentativeAddress: null,
                        representativeType: null,
                        boardRepresentativeName: null,
                        boardRepresentativeDesignation: null,
                        boardRepresentativeAddress: null,
                        salaryCode: null,
                        id: null
                    };
                };

                // bulk operations start
                $scope.areAllEmployeesSelected = false;

                $scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                    for (var i = 0; i < employeeArray.length; i++)
                    {
                        employeeArray[i].isSelected = selectionValue;
                    }
                };


                //$scope.import = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.export = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.deleteSelected = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.delete(employee);
                //        }
                //    }
                //};
                //
                //$scope.sync = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.update(employee);
                //        }
                //    }
                //};
                //
                //$scope.order = function (predicate, reverse) {
                //    $scope.predicate = predicate;
                //    $scope.reverse = reverse;
                //    Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                //        $scope.links = ParseLinks.parse(headers('link'));
                //        $scope.employees = result;
                //        $scope.total = headers('x-total-count');
                //    });
                //};

            }]);

/* ap-paycode-assign.controller.js */
angular.module('stepApp')
    .controller('ApPayCodeAssignController',
        ['$scope', 'entity', '$state','$stateParams', 'InstEmployee', 'MpoApplication', 'PayScale','APScaleApplication','InstEmplPayscaleHist','GetPayScaleByCode','InstEmplRecruitInfoByEmp','InstEmplRecruitInfo',
            function ($scope, entity, $state,$stateParams, InstEmployee, MpoApplication, PayScale,APScaleApplication,InstEmplPayscaleHist,GetPayScaleByCode,InstEmplRecruitInfoByEmp,InstEmplRecruitInfo) {

                $scope.mpoApplication = entity;
                $scope.instEmployee = {};
                $scope.payScaleDetails = '';
                $scope.payScaleID = '';
                $scope.mpoApplication.instEmployee = {};
                $scope.instEmplPayscaleHist = {};
                $scope.mpoApplication.instEmployee.payScale = {};

                APScaleApplication.get({id : $stateParams.id},function(result){
                    $scope.mpoApplication=result;
                    if($scope.mpoApplication.instEmployee.payScale !=null){
                        $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
                        $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
                    }
                });

                $scope.payScales = [];

                $scope.showPayScaleDetails = function(){
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.payScaleDetails = result;
                    });
                };

                $scope.save = function () {
                    if($scope.payScaleID == '') {
                        console.log('Please Select a PayScale');
                        return false;
                    }
                    $scope.isSaving = true;
                    $scope.instEmployee = $scope.mpoApplication.instEmployee;
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.instEmployee.payScale = result;
                        $scope.instEmployee.aPAppStatus = 6;
                        InstEmployee.update($scope.instEmployee);
                        $scope.instEmplPayscaleHist.activationDate = new Date();
                        $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
                        $scope.instEmplPayscaleHist.payScale = result;
                        InstEmplPayscaleHist.save($scope.instEmplPayscaleHist,onSaveSuccess,onSaveError);
                    });
                };

                var onSaveSuccess = function (result) {
                    InstEmplRecruitInfoByEmp.query({id:result.instEmployee.id}, function(recruitInfo){
                     recruitInfo.payScale = result.instEmployee.payScale;
                     InstEmplRecruitInfo.update(recruitInfo);
                        //console.log("current pay scale:"+recruitInfo.payScale.id);
                        $state.go('mpo.appayCodePendingList',{},{reload:true});
                    });
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

            }]);

/* ap-paycode-list-by-status.controller.js*/
angular.module('stepApp')
    .controller('ApPaycodeListByStatusController',
        ['$scope', 'entity', '$state', '$modal',  'Principal','MpoApplication', 'ApApplicationPayScaleList',
            function ($scope, entity, $state, $modal,  Principal,MpoApplication, ApApplicationPayScaleList) {

                $scope.payScalePendingLists = entity;

                //console.log($scope.payScalePendingLists);

                $scope.employees = [];
                $scope.page = 0;
                if (Principal.hasAuthority("ROLE_MPOADMIN")) {
                    console.log("-----------------------" + true);
                } else {
                    console.log("-----------------------" + true);
                }

                $scope.loadPage = function (page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                //$scope.search = function () {
                //    EmployeeSearch.query({query: $scope.searchQuery}, function (result) {
                //        $scope.employees = result;
                //    }, function (response) {
                //        if (response.status === 404) {
                //            $scope.loadAll();
                //        }
                //    });
                //};

                ApApplicationPayScaleList.query({status : 0},function(results){
                    console.log('****************');
                    console.log(results);
                });


                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.employee = {
                        name: null,
                        nameEnglish: null,
                        father: null,
                        mother: null,
                        presentAddress: null,
                        permanentAddress: null,
                        gender: null,
                        dob: null,
                        zipCode: null,
                        registrationCertificateSubject: null,
                        registrationExamYear: null,
                        registrationCetificateNo: null,
                        indexNo: null,
                        bankName: null,
                        bankBranch: null,
                        bankAccountNo: null,
                        designation: null,
                        subject: null,
                        payScale: null,
                        payScaleCode: null,
                        month: null,
                        monthlySalaryGovtProvided: null,
                        monthlySalaryInstituteProvided: null,
                        gbResolutionReceiveDate: null,
                        gbResolutionAgendaNo: null,
                        circularPaperName: null,
                        circularPublishedDate: null,
                        recruitExamDate: null,
                        recruitApproveGBResolutionDate: null,
                        recruitPermitAgendaNo: null,
                        recruitmentDate: null,
                        presentInstituteJoinDate: null,
                        presentPostJoinDate: null,
                        dgRepresentativeType: null,
                        dgRepresentativeName: null,
                        dgRepresentativeDesignation: null,
                        dgRepresentativeAddress: null,
                        representativeType: null,
                        boardRepresentativeName: null,
                        boardRepresentativeDesignation: null,
                        boardRepresentativeAddress: null,
                        salaryCode: null,
                        id: null
                    };
                };
                $scope.areAllEmployeesSelected = false;

                $scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                    for (var i = 0; i < employeeArray.length; i++) {
                        employeeArray[i].isSelected = selectionValue;
                    }
                };

                //$scope.import = function () {
                //    for (var i = 0; i < $scope.employees.length; i++) {
                //        var employee = $scope.employees[i];
                //        if (employee.isSelected) {
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.export = function () {
                //    for (var i = 0; i < $scope.employees.length; i++) {
                //        var employee = $scope.employees[i];
                //        if (employee.isSelected) {
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.deleteSelected = function () {
                //    for (var i = 0; i < $scope.employees.length; i++) {
                //        var employee = $scope.employees[i];
                //        if (employee.isSelected) {
                //            Employee.delete(employee);
                //        }
                //    }
                //};
                //
                //$scope.sync = function () {
                //    for (var i = 0; i < $scope.employees.length; i++) {
                //        var employee = $scope.employees[i];
                //        if (employee.isSelected) {
                //            Employee.update(employee);
                //        }
                //    }
                //};
                //
                //$scope.order = function (predicate, reverse) {
                //    $scope.predicate = predicate;
                //    $scope.reverse = reverse;
                //    Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                //        $scope.links = ParseLinks.parse(headers('link'));
                //        $scope.employees = result;
                //        $scope.total = headers('x-total-count');
                //    });
                //};
                // bulk operations end

            }]);

/* bed-paycode-assign.controller.js */
angular.module('stepApp')
    .controller('BedPayCodeAssignController',
        ['$scope', 'entity', '$state','$stateParams', 'InstEmployee', 'MpoApplication','BEdApplication','InstEmplPayscaleHist','GetPayScaleByCode','InstEmplRecruitInfoByEmp','InstEmplRecruitInfo',
            function ($scope, entity, $state,$stateParams, InstEmployee, MpoApplication,BEdApplication,InstEmplPayscaleHist,GetPayScaleByCode,InstEmplRecruitInfoByEmp,InstEmplRecruitInfo) {

                $scope.mpoApplication = entity;
                $scope.instEmployee = {};
                $scope.payScaleDetails = '';
                $scope.payScaleID = '';
                $scope.mpoApplication.instEmployee = {};
                $scope.instEmplPayscaleHist = {};

                $scope.mpoApplication.instEmployee.payScale = {};

                if ($stateParams.id !=null){
                    BEdApplication.get({id : $stateParams.id},function(result){
                        $scope.mpoApplication=result;
                        if($scope.mpoApplication.instEmployee.payScale !=null){
                            $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
                            $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
                            console.log($scope.payScaleDetails);
                        }
                    });
                }
                $scope.showPayScaleDetails = function(){
                    console.log($scope.payScaleID);
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.payScaleDetails = result;
                        console.log($scope.payScaleDetails);
                    });
                };

                $scope.save = function () {
                    if($scope.payScaleID == '') {
                        return false;
                    }
                    $scope.isSaving = true;
                    $scope.instEmployee = $scope.mpoApplication.instEmployee;
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.instEmployee.payScale = result;
                        $scope.instEmployee.mpoActive=1;
                        $scope.instEmployee.bEDAppStatus=6;
                        InstEmployee.update($scope.instEmployee);
                        $scope.instEmplPayscaleHist.activationDate = new Date();
                        $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
                        $scope.instEmplPayscaleHist.payScale = result;
                        InstEmplPayscaleHist.save($scope.instEmplPayscaleHist, onSaveSuccess);
                    });
                };

                var onSaveSuccess = function (result) {
                    InstEmplRecruitInfoByEmp.query({id:result.instEmployee.id}, function(recruitInfo){
                        //console.log("previous pay scale:"+recruitInfo.payScale.id);
                        recruitInfo.payScale = result.instEmployee.payScale;
                        recruitInfo = InstEmplRecruitInfo.update(recruitInfo);
                        //console.log("current pay scale:"+recruitInfo.payScale.id);
                        $state.go('mpo.bedpayCodePendingList',{},{reload:true});
                    });
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

            }]);


/* bed-paycode-list-by-status.controller.js */

angular.module('stepApp')
    .controller('BedPaycodeListByStatusController',
        ['$scope', 'entity', '$state', '$modal',  'Principal','BEdApplicationPayScale',
            function ($scope, entity, $state, $modal,  Principal,BEdApplicationPayScale) {
                $scope.payScalePendingLists = entity;
                $scope.employees = [];
                $scope.page = 0;

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                $scope.search = function () {
                    EmployeeSearch.query({query: $scope.searchQuery}, function(result) {
                        $scope.employees = result;
                    }, function(response) {
                        if(response.status === 404) {
                            $scope.loadAll();
                        }
                    });
                };

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                $scope.clear = function () {
                    $scope.employee = {
                        name: null,
                        nameEnglish: null,
                        father: null,
                        mother: null,
                        presentAddress: null,
                        permanentAddress: null,
                        gender: null,
                        dob: null,
                        zipCode: null,
                        registrationCertificateSubject: null,
                        registrationExamYear: null,
                        registrationCetificateNo: null,
                        indexNo: null,
                        bankName: null,
                        bankBranch: null,
                        bankAccountNo: null,
                        designation: null,
                        subject: null,
                        payScale: null,
                        payScaleCode: null,
                        month: null,
                        monthlySalaryGovtProvided: null,
                        monthlySalaryInstituteProvided: null,
                        gbResolutionReceiveDate: null,
                        gbResolutionAgendaNo: null,
                        circularPaperName: null,
                        circularPublishedDate: null,
                        recruitExamDate: null,
                        recruitApproveGBResolutionDate: null,
                        recruitPermitAgendaNo: null,
                        recruitmentDate: null,
                        presentInstituteJoinDate: null,
                        presentPostJoinDate: null,
                        dgRepresentativeType: null,
                        dgRepresentativeName: null,
                        dgRepresentativeDesignation: null,
                        dgRepresentativeAddress: null,
                        representativeType: null,
                        boardRepresentativeName: null,
                        boardRepresentativeDesignation: null,
                        boardRepresentativeAddress: null,
                        salaryCode: null,
                        id: null
                    };
                };

                // bulk operations start
                $scope.areAllEmployeesSelected = false;

                $scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                    for (var i = 0; i < employeeArray.length; i++)
                    {
                        employeeArray[i].isSelected = selectionValue;
                    }
                };


                //$scope.import = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.export = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.deleteSelected = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.delete(employee);
                //        }
                //    }
                //};
                //
                //$scope.sync = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.update(employee);
                //        }
                //    }
                //};
                //
                //$scope.order = function (predicate, reverse) {
                //    $scope.predicate = predicate;
                //    $scope.reverse = reverse;
                //    Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                //        $scope.links = ParseLinks.parse(headers('link'));
                //        $scope.employees = result;
                //        $scope.total = headers('x-total-count');
                //    });
                //};
                // bulk operations end

            }]);


/* principal-paycode-assign.controller.js */
angular.module('stepApp')
    .controller('PrincipalPayCodeAssignController',
        ['$scope', 'entity', '$state','$stateParams', 'InstEmployee',  'InstEmplRecruitInfoByEmp','ProfessorApplication','InstEmplPayscaleHist','GetPayScaleByCode','InstEmplRecruitInfo',
            function ($scope, entity, $state,$stateParams, InstEmployee, InstEmplRecruitInfoByEmp,ProfessorApplication,InstEmplPayscaleHist,GetPayScaleByCode,InstEmplRecruitInfo) {

                $scope.mpoApplication = entity;
                $scope.payScaleDetails = '';
                $scope.payScaleID = '';
                $scope.mpoApplication.instEmployee = {};
                $scope.instEmplPayscaleHist = {};
                $scope.mpoApplication.instEmployee.payScale = {};

                ProfessorApplication.get({id : $stateParams.id},function(result){
                    $scope.mpoApplication=result;
                    if($scope.mpoApplication.instEmployee.payScale !=null){
                        $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
                        $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
                    }
                });

                $scope.payScales = [];

                $scope.showPayScaleDetails = function(){
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result){
                        $scope.payScaleDetails = result;
                    });
                };
                $scope.save = function () {
                    if($scope.payScaleID == '') {
                        return false;
                    }
                    $scope.isSaving = true;
                    $scope.instEmployee = $scope.mpoApplication.instEmployee;
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.instEmployee.payScale = result;
                        $scope.instEmployee.mpoActive=1;
                        $scope.instEmployee.principleAppStatus = 6;
                        InstEmployee.update($scope.instEmployee);
                        $scope.instEmplPayscaleHist.activationDate = new Date();
                        $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
                        $scope.instEmplPayscaleHist.payScale = result;
                        InstEmplPayscaleHist.save($scope.instEmplPayscaleHist, onSaveSuccess);
                    });
                };
                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                var onSaveSuccess = function (result) {
                    InstEmplRecruitInfoByEmp.query({id:result.instEmployee.id}, function(recruitInfo){
                        recruitInfo.payScale = result.instEmployee.payScale;
                        InstEmplRecruitInfo.update(recruitInfo);
                        $state.go('mpo.principalpayCodeApprovedList',{},{reload:true});
                    });
                    $scope.isSaving = false;
                };


            }]);

/* principal-paycode-list-by-status.controller.js*/

angular.module('stepApp')
    .controller('PrincipalPaycodeListByStatusController',
        ['$scope', 'entity', '$state', '$modal',  'Principal','MpoApplication', 'MpoApplicationPayScale',
            function ($scope, entity, $state, $modal,  Principal,MpoApplication, MpoApplicationPayScale) {
                $scope.payScalePendingLists = entity;
                console.log($scope.payScalePendingLists);
                $scope.employees = [];
                $scope.page = 0;
                //if(Principal.hasAuthority("ROLE_MPOADMIN")){
                //    console.log("-----------------------"+true);
                //}else{
                //    console.log("-----------------------"+true);
                //}
                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                //$scope.search = function () {
                //    EmployeeSearch.query({query: $scope.searchQuery}, function(result) {
                //        $scope.employees = result;
                //    }, function(response) {
                //        if(response.status === 404) {
                //            $scope.loadAll();
                //        }
                //    });
                //};

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };
                //$scope.clear = function () {
                //    $scope.employee = {
                //        name: null,
                //        nameEnglish: null,
                //        father: null,
                //        mother: null,
                //        presentAddress: null,
                //        permanentAddress: null,
                //        gender: null,
                //        dob: null,
                //        zipCode: null,
                //        registrationCertificateSubject: null,
                //        registrationExamYear: null,
                //        registrationCetificateNo: null,
                //        indexNo: null,
                //        bankName: null,
                //        bankBranch: null,
                //        bankAccountNo: null,
                //        designation: null,
                //        subject: null,
                //        payScale: null,
                //        payScaleCode: null,
                //        month: null,
                //        monthlySalaryGovtProvided: null,
                //        monthlySalaryInstituteProvided: null,
                //        gbResolutionReceiveDate: null,
                //        gbResolutionAgendaNo: null,
                //        circularPaperName: null,
                //        circularPublishedDate: null,
                //        recruitExamDate: null,
                //        recruitApproveGBResolutionDate: null,
                //        recruitPermitAgendaNo: null,
                //        recruitmentDate: null,
                //        presentInstituteJoinDate: null,
                //        presentPostJoinDate: null,
                //        dgRepresentativeType: null,
                //        dgRepresentativeName: null,
                //        dgRepresentativeDesignation: null,
                //        dgRepresentativeAddress: null,
                //        representativeType: null,
                //        boardRepresentativeName: null,
                //        boardRepresentativeDesignation: null,
                //        boardRepresentativeAddress: null,
                //        salaryCode: null,
                //        id: null
                //    };
                //};
                // bulk operations start
                //$scope.areAllEmployeesSelected = false;
                //$scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                //    for (var i = 0; i < employeeArray.length; i++)
                //    {
                //        employeeArray[i].isSelected = selectionValue;
                //    }
                //};

                //$scope.import = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.export = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.deleteSelected = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.delete(employee);
                //        }
                //    }
                //};
                //
                //$scope.sync = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.update(employee);
                //        }
                //    }
                //};
                //
                //$scope.order = function (predicate, reverse) {
                //    $scope.predicate = predicate;
                //    $scope.reverse = reverse;
                //    Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                //        $scope.links = ParseLinks.parse(headers('link'));
                //        $scope.employees = result;
                //        $scope.total = headers('x-total-count');
                //    });
                //};
                // bulk operations end
            }]);

/* timescale-paycode-assign.controller.js */
angular.module('stepApp')
    .controller('TimeScalePayCodeAssignController',
        ['$scope', 'entity', '$state','$stateParams', 'InstEmployee', 'MpoApplication', 'PayScale','TimeScaleApplication','InstEmplPayscaleHist','GetPayScaleByCode','InstEmplRecruitInfoByEmp','InstEmplRecruitInfo',
            function ($scope, entity, $state,$stateParams, InstEmployee, MpoApplication, PayScale,TimeScaleApplication,InstEmplPayscaleHist,GetPayScaleByCode,InstEmplRecruitInfoByEmp,InstEmplRecruitInfo) {

                $scope.mpoApplication = entity;
                $scope.instEmployee = {};
                $scope.payScaleDetails = '';
                $scope.payScaleID = '';
                $scope.mpoApplication.instEmployee = {};
                $scope.instEmplPayscaleHist = {};

                $scope.mpoApplication.instEmployee.payScale = {};
                //console.log($scope.mpoApplication);

                //TimeScaleApplication.get({id : $stateParams.id},function(result){
                //    $scope.mpoApplication=result;
                //    if($scope.mpoApplication.instEmployee.payScale !=null){
                //        $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
                //        $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
                //        console.log($scope.payScaleDetails);
                //    }
                //});
                //
                //$scope.payScales = [];
                //
                //$scope.loadAll = function() {
                //    PayScale.query({page: $scope.page, size: 500}, function(result) {
                //        $scope.payScales = result;
                //        console.log($scope.payScales);
                //    });
                //};
                //
                //$scope.loadAll();
                //
                //$scope.showPayScaleDetails = function(){
                //    console.log($scope.payScaleIDins);
                //    PayScale.get({id: $scope.payScaleID}, function(result) {
                //        $scope.payScaleDetails = result;
                //        console.log($scope.payScaleDetails);
                //    });
                //};
                //$scope.save = function () {
                //    if($scope.payScaleID == '') {
                //        console.log('Please Select a PayScale');
                //        return false;
                //    }
                //    $scope.isSaving = true;
                //    $scope.instEmployee = $scope.mpoApplication.instEmployee;
                //    PayScale.get({id: $scope.payScaleID}, function(result) {
                //        $scope.instEmployee.payScale = result;
                //        console.log($scope.instEmployee);
                //        $scope.instEmployee.timescaleAppStatus=6;
                //
                //        InstEmployee.update($scope.instEmployee);
                //
                //        $scope.instEmplPayscaleHist.activationDate = new Date();
                //        //$scope.instEmplPayscaleHist.endDate = {};
                //        $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
                //        $scope.instEmplPayscaleHist.payScale = result;
                //        InstEmplPayscaleHist.save($scope.instEmplPayscaleHist);
                //
                //        $state.go('mpo.timeScalepayCodePendingList',{},{reload:true});
                //    });
                //};

                if ($stateParams.id !=null){
                    TimeScaleApplication.get({id : $stateParams.id},function(result){
                        $scope.mpoApplication=result;
                        if($scope.mpoApplication.instEmployee.payScale !=null){
                            $scope.payScaleDetails = $scope.mpoApplication.instEmployee.payScale;
                            $scope.payScaleID = $scope.mpoApplication.instEmployee.payScale.id;
                            console.log($scope.payScaleDetails);
                        }
                    });
                }
                $scope.showPayScaleDetails = function(){
                    console.log($scope.payScaleID);
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.payScaleDetails = result;
                        console.log($scope.payScaleDetails);
                    });
                };

                $scope.save = function () {
                    if($scope.payScaleID == '') {
                        return false;
                    }
                    $scope.isSaving = true;
                    $scope.instEmployee = $scope.mpoApplication.instEmployee;
                    GetPayScaleByCode.get({code: $scope.payScaleID}, function(result) {
                        $scope.instEmployee.payScale = result;
                        console.log($scope.instEmployee);
                        $scope.instEmployee.mpoActive=1;
                        $scope.instEmployee.timescaleAppStatus=6;
                        InstEmployee.update($scope.instEmployee);
                        $scope.instEmplPayscaleHist.activationDate = new Date();
                        $scope.instEmplPayscaleHist.instEmployee = $scope.instEmployee;
                        $scope.instEmplPayscaleHist.payScale = result;
                        InstEmplPayscaleHist.save($scope.instEmplPayscaleHist, onSaveSuccess);
                    });
                };

                var onSaveSuccess = function (result) {
                    InstEmplRecruitInfoByEmp.query({id:result.instEmployee.id}, function(recruitInfo){
                        recruitInfo.payScale = result.instEmployee.payScale;
                        recruitInfo = InstEmplRecruitInfo.update(recruitInfo);
                        $state.go('mpo.timeScalepayCodeApprovedList',{},{reload:true});
                    });
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };
            }]);

/* timescale-paycode-list-by-status.controller.js */
angular.module('stepApp')
    .controller('TimeScalePaycodeListByStatusController',
        ['$scope', 'entity', '$state', '$modal',  'Principal', 'TimeScaleApplicationPayScales',
            function ($scope, entity, $state, $modal,  Principal,TimeScaleApplicationPayScales) {

                $scope.payScalePendingLists = entity;

                $scope.employees = [];
                $scope.page = 0;

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };

                //$scope.search = function () {
                //    EmployeeSearch.query({query: $scope.searchQuery}, function(result) {
                //        $scope.employees = result;
                //    }, function(response) {
                //        if(response.status === 404) {
                //            $scope.loadAll();
                //        }
                //    });
                //};

                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };

                //$scope.clear = function () {
                //    $scope.employee = {
                //        name: null,
                //        nameEnglish: null,
                //        father: null,
                //        mother: null,
                //        presentAddress: null,
                //        permanentAddress: null,
                //        gender: null,
                //        dob: null,
                //        zipCode: null,
                //        registrationCertificateSubject: null,
                //        registrationExamYear: null,
                //        registrationCetificateNo: null,
                //        indexNo: null,
                //        bankName: null,
                //        bankBranch: null,
                //        bankAccountNo: null,
                //        designation: null,
                //        subject: null,
                //        payScale: null,
                //        payScaleCode: null,
                //        month: null,
                //        monthlySalaryGovtProvided: null,
                //        monthlySalaryInstituteProvided: null,
                //        gbResolutionReceiveDate: null,
                //        gbResolutionAgendaNo: null,
                //        circularPaperName: null,
                //        circularPublishedDate: null,
                //        recruitExamDate: null,
                //        recruitApproveGBResolutionDate: null,
                //        recruitPermitAgendaNo: null,
                //        recruitmentDate: null,
                //        presentInstituteJoinDate: null,
                //        presentPostJoinDate: null,
                //        dgRepresentativeType: null,
                //        dgRepresentativeName: null,
                //        dgRepresentativeDesignation: null,
                //        dgRepresentativeAddress: null,
                //        representativeType: null,
                //        boardRepresentativeName: null,
                //        boardRepresentativeDesignation: null,
                //        boardRepresentativeAddress: null,
                //        salaryCode: null,
                //        id: null
                //    };
                //};

                // bulk operations start
                //$scope.areAllEmployeesSelected = false;

                //$scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                //    for (var i = 0; i < employeeArray.length; i++)
                //    {
                //        employeeArray[i].isSelected = selectionValue;
                //    }
                //};


                //$scope.import = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.export = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //
                //$scope.deleteSelected = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.delete(employee);
                //        }
                //    }
                //};
                //
                //$scope.sync = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.update(employee);
                //        }
                //    }
                //};
                //
                //$scope.order = function (predicate, reverse) {
                //    $scope.predicate = predicate;
                //    $scope.reverse = reverse;
                //    Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                //        $scope.links = ParseLinks.parse(headers('link'));
                //        $scope.employees = result;
                //        $scope.total = headers('x-total-count');
                //    });
                //};
                // bulk operations end

            }]);
