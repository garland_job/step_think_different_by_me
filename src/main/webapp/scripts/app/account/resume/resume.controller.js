'use strict';

angular.module('stepApp')
    .controller('ResumeController',
        ['$scope', '$rootScope', '$state', '$q', 'EduBoard', 'JpSkill', 'DistrictsByName', 'User', 'CountrysByName', 'JpEmployeeReference', 'JpEmployeeTraining', 'JpLanguageProficiency', 'JpLanguageProfeciencyJpEmployee', 'JpAcademicQualification', 'JpEmployeeTrainingJpEmployee', 'JpEmployeeExperienceJpEmployee', 'JpEmploymentHistoryJpEmployee',
            'JpEmployeeExperience', 'Principal', 'ParseLinks', 'JpEmployee', 'Religion', 'AcademicQualificationJpEmployee', 'JpEmploymentHistory', 'JpEmployeeReferenceJpEmployee', '$timeout', 'JpSkillByName', 'EduBoardByType', 'ActiveJpSkill', 'ActiveJpSkillLevel', 'ActiveEduLevels',
            function ($scope, $rootScope, $state, $q, EduBoard, JpSkill, DistrictsByName, User, CountrysByName, JpEmployeeReference, JpEmployeeTraining, JpLanguageProficiency, JpLanguageProfeciencyJpEmployee, JpAcademicQualification, JpEmployeeTrainingJpEmployee, JpEmployeeExperienceJpEmployee, JpEmploymentHistoryJpEmployee,
                      JpEmployeeExperience, Principal, ParseLinks, JpEmployee, Religion, AcademicQualificationJpEmployee, JpEmploymentHistory, JpEmployeeReferenceJpEmployee, $timeout, JpSkillByName, EduBoardByType, ActiveJpSkill, ActiveJpSkillLevel, ActiveEduLevels) {

                $scope.jpEmployee = {};
                $scope.jpAcademicQualification = {};
                $scope.jpEmploymentHistory = {};
                $scope.jpEmployeeExperience = {};
                $scope.jpEmployeeTraining = {};
                $scope.jpLanguageProficiency = {};
                $scope.jpEmployeeReference = {};
                $scope.jpSkill = {};
                $scope.districts = [];
                $scope.jpAcademicQualifications = [];
                $scope.jpLanguageProficiencys = [];
                $scope.jpSkills = [];
                $scope.addNewSkill = false;
                $scope.jpEmployee.nationality = "Bangladeshi";

                Principal.identity().then(function (account) {
                    User.get({login: account.login}, function (result) {
                        $scope.jpEmployee.user = result;
                        $scope.jpEmployee.email = result.email;

                    });
                });

                $scope.years = [];
                var currentYear = new Date().getFullYear();
                EduBoardByType.query({boardType: 'board'}, function (result) {
                    $scope.eduBoards = result;
                });

                EduBoardByType.query({boardType: 'university'}, function (result) {
                    $scope.eduUniversitys = result;
                });

                $scope.religions = Religion.query({size: 300});

                EduBoard.query({}, function (result, headers) {
                    $scope.eduBoards = result;
                });

                ActiveEduLevels.query({}, function (result, headers) {
                    $scope.eduLevels = result;
                });

                ActiveJpSkill.query({page: 0, size: 1000}, function (result, headers) {
                    angular.forEach(result, function (value, key) {
                        {
                            $scope.jpSkills.push({
                                description: value.description,
                                id: value.id,
                                name: value.name,
                                status: value.status
                            });
                        }
                        ;
                    });
                });

                ActiveJpSkillLevel.query({page: 0, size: 1000}, function (result, headers) {
                    $scope.jpSkillLevels = result;
                });


                $scope.initDates = function () {
                    var i;
                    for (i = currentYear; i >= currentYear - 50; i--) {
                        $scope.years.push(i);
                    }
                };
                $scope.initDates();
                $scope.countrys = CountrysByName.query({size: 300});
                DistrictsByName.query({}, function (result, headers) {
                    $scope.districts = result;
                });
                $scope.calendar = {
                    opened: {},
                    dateFormat: 'yyyy-MM-dd',
                    dateOptions: {},
                    open: function ($event, which) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.calendar.opened[which] = true;
                    }
                };
                $scope.hasProfile = true;
                $scope.viewProfileMode = true;
                $scope.viewEducationMode = true;
                $scope.viewEmploymentMode = true;
                $scope.viewSkillsMode = true;
                $scope.viewTrainingsMode = true;
                $scope.viewLanguageMode = true;
                $scope.viewReferenceMode = true;

                $scope.jpSkills.push(
                    {
                        description: 'Other',
                        id: -1,
                        name: 'Other',
                        status: true
                    }
                );
                $scope.changeJpSkill = function () {
                    console.log("skill :" + $scope.jpEmployeeExperience.jpSkill);
                    if ($scope.jpEmployeeExperience.jpSkill.id == -1) {
                        console.log("Other found");
                        $scope.addNewSkill = true;
                    } else {
                        $scope.addNewSkill = false;
                    }
                };
                $scope.changeProfileMode = function (value) {
                    $scope.viewProfileMode = value;
                }
                $scope.changeEducationMode = function (value) {
                    $timeout(function () {
                        $rootScope.refreshRequiredFields();
                    }, 100);
                    if ($scope.jpAcademicQualifications.length == 0) {
                        $scope.jpAcademicQualifications.push(
                            {
                                degreeTitle: null,
                                major: null,
                                institute: null,
                                isGpaResult: true,
                                resulttype: 'gpa',
                                result: null,
                                passingyear: null,
                                duration: null,
                                achivement: null,
                                cgpa: null,
                                certificateCopy: null,
                                certificateCopyContentType: null,
                                status: null,
                                id: null,
                                instEmployee: null
                            }
                        );
                        $timeout(function () {
                            $rootScope.refreshRequiredFields();
                        }, 100);
                    }
                    $scope.viewEducationMode = value;
                };

                $scope.AddMoreEducation = function () {
                    $scope.jpAcademicQualifications.push(
                        {
                            degreeTitle: null,
                            major: null,
                            institute: null,
                            isGpaResult: true,
                            resulttype: 'gpa',
                            result: null,
                            passingyear: null,
                            duration: null,
                            achivement: null,
                            cgpa: null,
                            certificateCopy: null,
                            certificateCopyContentType: null,
                            status: null,
                            id: null,
                            instEmployee: null
                        }
                    );
                    // Start Add this code for showing required * in add more fields
                    $timeout(function () {
                        $rootScope.refreshRequiredFields();
                    }, 100);
                    // End Add this code for showing required * in add more fields

                };

                $scope.AddMoreLanguage = function () {
                    $scope.jpLanguageProficiencys.push(
                        {
                            name: null,
                            reading: 'Average',
                            writing: 'Average',
                            speaking: 'Average',
                            listening: 'Average'
                        }
                    );
                    // Start Add this code for showing required * in add more fields
                    $timeout(function () {
                        $rootScope.refreshRequiredFields();
                    }, 100);
                    // End Add this code for showing required * in add more fields

                };

                $scope.changeEmploymentMode = function (value) {
                    $scope.clearForm();
                    if (value) {
                        $scope.jpEmploymentHistory = {};
                    }
                    $scope.viewEmploymentMode = value;
                }

                $scope.changeSkillsMode = function (value) {
                    $scope.clearForm();
                    if (value) {
                        $scope.jpEmployeeExperience = {};
                    }
                    $scope.viewSkillsMode = value;
                }

                $scope.changeTrainingsMode = function (value) {
                    $scope.clearForm();
                    if (value) {
                        $scope.jpEmployeeTraining = {};
                    }
                    $scope.viewTrainingsMode = value;
                }

                $scope.changeLanguageMode = function (value) {
                    if ($scope.jpLanguageProficiencys.length == 0) {
                        $scope.jpLanguageProficiencys.push(
                            {
                                name: null,
                                reading: 'Average',
                                writing: 'Average',
                                speaking: 'Average',
                                listening: 'Average'
                            }
                        );
                        // Start Add this code for showing required * in add more fields
                        $timeout(function () {
                            $rootScope.refreshRequiredFields();
                        }, 100);
                    }

                    $scope.clearForm();
                    if (value) {
                        $scope.jpLanguageProficiency = {};
                    }
                    $scope.viewLanguageMode = value;
                }

                $scope.changeReferenceMode = function (value) {
                    $scope.clearForm();
                    if (value) {
                        $scope.jpEmployeeReference = {};
                    }
                    $scope.viewReferenceMode = value;
                }

                Principal.identity().then(function (account) {
                    $scope.settingsAccount = account;
                    $scope.user = User.get({'login': $scope.settingsAccount.login});
                });

                $scope.profile = function () {
                    JpEmployee.get({id: 'my'}, function (result) {
                        $scope.jpEmployee = result;
                        if (result.picture) {
                            var blob = b64toBlob(result.picture);
                            $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
                            $scope.fileRequired = 'Got';
                        }


                    }, function (response) {
                        $scope.hasProfile = false;
                    })
                };

                var onSaveSuccess = function (result) {
                    $scope.$emit('stepApp:employeeUpdate', result);
                    $scope.isSaving = false;

                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                var onSaveProfileFinished = function (result) {
                    $scope.changeProfileMode(true);
                    $state.go('resume', {}, {reload: true});
                };

                $scope.saveProfile = function () {
                    if ($scope.jpEmployee.id != null) {
                        JpEmployee.update($scope.jpEmployee, onSaveProfileFinished);
                        $rootScope.setWarningMessage('stepApp.jpEmployee.updated');
                    } else {
                        Principal.identity().then(function (account) {
                            User.get({login: account.login}, function (result) {
                                $scope.jpEmployee.user = result;
                                JpEmployee.save($scope.jpEmployee, onSaveProfileFinished);
                                $rootScope.setSuccessMessage('stepApp.jpEmployee.created');
                            });
                        });
                    }
                };

                $scope.employeeUpdate = function () {
                    $scope.isSaving = true;
                    if ($scope.jpEmployee.id != null) {
                        JpEmployee.update($scope.jpEmployee, onSaveSuccess, onSaveError);
                        $rootScope.setWarningMessage('stepApp.jpEmployee.updated');
                    } else {
                        $scope.jpEmployee.user.id = $scope.user.id;
                        JpEmployee.save($scope.jpEmployee, onSaveSuccess, onSaveError);
                        $rootScope.setSuccessMessage('stepApp.jpEmployee.created');
                    }
                };

                $scope.fileRequired = null;
                console.log($scope.fileRequired);
                console.log("==============================");

                $scope.resizeMaxHeight = 300;
                $scope.resizeMaxWidth = 300;

                $scope.setPicture = function ($file, jpEmployee) {
                    if (!$file) {
                    } else {
                        if ($file.size / 1024 > 100) {
                            alert("Please, select image less than 100 kb.");
                        } else {
                            //Image resize init
                            $scope.getResizeArea = function () {
                                $scope.resizeAreaId = 'fileupload-resize-area';
                                $scope.resizeArea = document.getElementById($scope.resizeAreaId);

                                if (!$scope.resizeArea) {
                                    $scope.resizeArea = document.createElement('canvas');
                                    $scope.resizeArea.id = $scope.resizeAreaId;
                                    $scope.resizeArea.style.visibility = 'hidden';
                                    document.body.appendChild($scope.resizeArea);
                                }
                                return $scope.resizeArea;
                            }
                            $scope.createImage = function (url, callback) {
                                $scope.image = new Image();
                                $scope.image.onload = function () {
                                    callback($scope.image);
                                };
                                $scope.image.src = url;
                            };
                            $scope.fileToDataURL = function (file) {
                                $scope.deferred = $q.defer();
                                $scope.reader = new FileReader();
                                $scope.reader.onload = function (e) {
                                    $scope.deferred.resolve(e.target.result);
                                };
                                $scope.reader.readAsDataURL(file);
                                return $scope.deferred.promise;
                            };
                            $scope.resizeImage = function (origImage, options) {
                                $scope.maxHeight = options.resizeMaxHeight || 50;
                                $scope.maxWidth = options.resizeMaxWidth || 50;
                                $scope.quality = options.resizeQuality || 0.7;
                                $scope.type = options.resizeType || 'image/jpg';

                                $scope.canvas = $scope.getResizeArea();

                                $scope.height = origImage.height;
                                $scope.width = origImage.width;

                                // calculate the width and height, constraining the proportions
                                if ($scope.width > $scope.height) {
                                    if ($scope.width > $scope.maxWidth) {
                                        $scope.height = Math.round($scope.height *= $scope.maxWidth / $scope.width);
                                        $scope.width = $scope.maxWidth;
                                    }
                                } else {
                                    if ($scope.height > $scope.maxHeight) {
                                        $scope.width = Math.round($scope.width *= $scope.maxHeight / $scope.height);
                                        $scope.height = $scope.maxHeight;
                                    }
                                }

                                $scope.canvas.width = $scope.width;
                                $scope.canvas.height = $scope.height;
                                //draw image on canvas
                                $scope.ctx = $scope.canvas.getContext("2d");
                                $scope.ctx.drawImage(origImage, 0, 0, $scope.width, $scope.height);

                                // get the data from canvas as 70% jpg (or specified type).
                                return $scope.canvas.toDataURL($scope.type, $scope.quality);
                            };
                            $scope.doResizing = function (imageResult, callback) {
                                $scope.createImage(imageResult.url,
                                    function (image) {
                                        $scope.dataURL = $scope.resizeImage(image, $scope);
                                        $scope.imageResult.resized = {dataURL: $scope.dataURL, type: $scope.dataURL.match(/:(.+\/.+);/)[1],};
                                        callback($scope.imageResult);
                                    });
                            };
                            $scope.imageResult = { file: $file, url: URL.createObjectURL($file)};
                            $scope.fileToDataURL($file).then(function (dataURL) {
                                $scope.imageResult.dataURL = dataURL;
                            });
                            if ($scope.resizeMaxHeight ||
                                $scope.resizeMaxWidth) { //resize image
                                $scope.doResizing($scope.imageResult,
                                    function (imageResult) {
                                        console.log(imageResult);
                                        var base64Data = imageResult.resized.dataURL.substr(imageResult.resized.dataURL.indexOf('base64,') + 'base64,'.length);
                                        $scope.$apply(function () {
                                            $scope.jpEmployee.picture = base64Data;
                                            $scope.jpEmployee.pictureType = imageResult.resized.type;
                                            var blob = b64toBlob(jpEmployee.picture, jpEmployee.pictureType);
                                            $scope.url = (window.URL ||
                                            window.webkitURL).createObjectURL(blob);
                                            $scope.picNotAdd = false;
                                        });
                                    });
                            }
                        }
                    }
                };
                $scope.toDay = new Date();
                var onSaveEducationFinished = function (result) {
                    AcademicQualificationJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.jpAcademicQualifications = result;
                    });
                    $scope.changeEducationMode(true);
                };

                $scope.saveEducation = function () {
                    angular.forEach($scope.jpAcademicQualifications, function (data) {
                        if (data.id != null) {
                            JpAcademicQualification.update(data, onSaveEducationFinished);
                            $rootScope.setWarningMessage('stepApp.jpAcademicQualification.updated');
                        } else {
                            JpEmployee.get({id: 'my'}, function (result) {
                                data.jpEmployee = result;
                                JpAcademicQualification.save(data, onSaveEducationFinished);
                                $rootScope.setSuccessMessage('stepApp.jpAcademicQualification.created');
                            });
                        }
                    });
                };

                $scope.editEducation = function (id) {
                    JpAcademicQualification.get({id: id}, function (result) {
                        $scope.jpAcademicQualification = result;
                        $scope.viewEducationMode = false;
                    });
                };

                var onSaveEmploymentFinished = function (result) {
                    $scope.careerInfo();
                    $scope.changeEmploymentMode(true);
                };

                $scope.saveEmplymentHistory = function () {
                    if ($scope.jpEmploymentHistory.id != null) {
                        JpEmploymentHistory.update($scope.jpEmploymentHistory, onSaveEmploymentFinished);
                        $rootScope.setWarningMessage('stepApp.jpEmploymentHistory.updated');
                    } else {
                        JpEmployee.get({id: 'my'}, function (result) {
                            $scope.jpEmploymentHistory.jpEmployee = result;
                            JpEmploymentHistory.save($scope.jpEmploymentHistory, onSaveEmploymentFinished);
                            $rootScope.setSuccessMessage('stepApp.jpEmploymentHistory.created');
                        });
                    }
                };

                $scope.editEmploymentHistory = function (id) {
                    JpEmploymentHistory.get({id: id}, function (result) {
                        $scope.jpEmploymentHistory = result;
                        $scope.viewEmploymentMode = false;
                    });
                };

                var onSaveSkillFinished = function (result) {
                    $scope.experienceTab();
                    $scope.changeSkillsMode(true);
                };

                var onSaveNewSkill = function (result) {
                    $scope.jpEmployeeExperience.jpSkill = result;
                    $scope.isSaving = true;
                    if ($scope.jpEmployeeExperience.id != null) {
                        JpEmployeeExperience.update($scope.jpEmployeeExperience, onSaveSkillFinished);
                        $rootScope.setWarningMessage('stepApp.jpEmployeeExperience.updated');

                    } else {
                        JpEmployee.get({id: 'my'}, function (result) {
                            $scope.jpEmployeeExperience.jpEmployee = result;
                            JpEmployeeExperience.save($scope.jpEmployeeExperience, onSaveSkillFinished);
                            $rootScope.setSuccessMessage('stepApp.jpEmployeeExperience.created');
                        });
                    }
                }

                $scope.saveSkill = function () {
                    JpSkillByName.get({name: $scope.jpSkill.name}, function (result) {
                        alert('skill already exists');
                        location.reload(true);
                    }, function (response) {
                        if (response.status == 404 || response.status == 400) {
                            if ($scope.jpEmployeeExperience.jpSkill.id == -1) {
                                //console.log("Other found");
                                $scope.jpSkill.id = null;
                                $scope.jpSkill.status = true;
                                // $scope.jpSkill.name = $scope.organizationCategory;
                                JpSkill.save($scope.jpSkill, onSaveNewSkill);

                            } else {
                                if ($scope.jpEmployeeExperience.id != null) {
                                    JpEmployeeExperience.update($scope.jpEmployeeExperience, onSaveSkillFinished);
                                    $rootScope.setWarningMessage('stepApp.jpEmployeeExperience.updated');
                                } else {
                                    JpEmployee.get({id: 'my'}, function (result) {
                                        $scope.jpEmployeeExperience.jpEmployee = result;
                                        JpEmployeeExperience.save($scope.jpEmployeeExperience, onSaveSkillFinished);
                                        $rootScope.setSuccessMessage('stepApp.jpEmployeeExperience.created');
                                    });
                                }
                            }
                        }
                    });
                };

                $scope.editSkill = function (id) {
                    JpEmployeeExperience.get({id: id}, function (result) {
                        $scope.jpEmployeeExperience = result;
                        $scope.viewSkillsMode = false;
                    });
                };

                var onSaveTrainingFinished = function (result) {
                    $scope.trainingsTab();
                    $scope.changeTrainingsMode(true);
                };

                $scope.saveTraining = function () {
                    if ($scope.jpEmployeeTraining.id != null) {
                        JpEmployeeTraining.update($scope.jpEmployeeTraining, onSaveTrainingFinished);
                        $rootScope.setWarningMessage('stepApp.jpEmployeeTraining.updated');
                    } else {
                        JpEmployee.get({id: 'my'}, function (result) {
                            $scope.jpEmployeeTraining.jpEmployee = result;
                            JpEmployeeTraining.save($scope.jpEmployeeTraining, onSaveTrainingFinished);
                            $rootScope.setSuccessMessage('stepApp.jpEmployeeTraining.created');
                        });
                    }
                };

                $scope.editTraining = function (id) {
                    JpEmployeeTraining.get({id: id}, function (result) {
                        $scope.jpEmployeeTraining = result;
                        $scope.viewTrainingsMode = false;
                    });
                };

                var onSaveLanguageFinished = function (result) {
                    JpLanguageProfeciencyJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.jpLanguageProficiencys = result;
                    });
                    $scope.languageTab();
                    $scope.changeLanguageMode(true);
                };

                $scope.saveLanguage = function () {
                    console.log("comes to save language");
                    angular.forEach($scope.jpLanguageProficiencys, function (data) {
                        if (data.id != null) {
                            JpLanguageProficiency.update(data, onSaveLanguageFinished);
                            $rootScope.setWarningMessage('stepApp.jpLanguageProficiency.updated');
                        } else {
                            JpEmployee.get({id: 'my'}, function (result) {
                                data.jpEmployee = result;
                                JpLanguageProficiency.save(data, onSaveLanguageFinished);
                                $rootScope.setSuccessMessage('stepApp.jpLanguageProficiency.created');
                            });
                        }
                    });
                };

                $scope.editLanguage = function (id) {
                    JpLanguageProficiency.get({id: id}, function (result) {
                        $scope.jpLanguageProficiency = result;
                        $scope.viewLanguageMode = false;
                    });
                };
                $scope.cancel = function () {
                    $state.go('resume', {}, {reload: true});
                };

                var onSaveReferenceFinished = function (result) {
                    $scope.referenceTab();
                    $scope.changeReferenceMode(true);
                };

                $scope.saveReference = function () {
                    if ($scope.jpEmployeeReference.id != null) {
                        JpEmployeeReference.update($scope.jpEmployeeReference, onSaveReferenceFinished);
                        $rootScope.setWarningMessage('stepApp.jpEmployeeReference.updated');
                    } else {
                        JpEmployee.get({id: 'my'}, function (result) {
                            $scope.jpEmployeeReference.jpEmployee = result;
                            JpEmployeeReference.save($scope.jpEmployeeReference, onSaveReferenceFinished);
                            $rootScope.setSuccessMessage('stepApp.jpEmployeeReference.created');
                        });
                    }
                };

                $scope.editReference = function (id) {
                    JpEmployeeReference.get({id: id}, function (result) {
                        $scope.jpEmployeeReference = result;
                        $scope.viewReferenceMode = false;
                    });
                };

                /*$scope.qualifications = function () {
                 ProfessionalQualification.query({id: $scope.jpEmployee.id}, function (result, headers) {
                 $scope.professionalQualifications = result;
                 });
                 };*/

                $scope.educations = function () {
                    AcademicQualificationJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.jpAcademicQualifications = result;
                    });
                    $scope.jpAcademicQualifications.push(
                        {
                            degreeTitle: null,
                            major: null,
                            institute: null,
                            isGpaResult: true,
                            resulttype: 'gpa',
                            result: null,
                            passingyear: null,
                            duration: null,
                            achivement: null,
                            cgpa: null,
                            certificateCopy: null,
                            certificateCopyContentType: null,
                            status: null,
                            id: null,
                            instEmployee: null
                        }
                    );
                };

                $scope.careerInfo = function () {
                    JpEmploymentHistoryJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.employments = result;
                    });
                };

                $scope.experienceTab = function () {
                    JpEmployeeExperienceJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.skills = result;
                    });
                };

                $scope.trainingsTab = function () {
                    JpEmployeeTrainingJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.trainings = result;
                    });
                };

                $scope.languageTab = function () {
                    JpLanguageProfeciencyJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.jpLanguageProficiencys = result;
                    });
                };

                $scope.referenceTab = function () {
                    JpEmployeeReferenceJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                        $scope.references = result;
                    });
                };

                $scope.clearForm = function () {
                    $('.form-group').removeClass('has-error');
                }

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;
                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];
                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);
                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }
                        var byteArray = new Uint8Array(byteNumbers);
                        byteArrays.push(byteArray);
                    }
                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }
            }]);

/* profile.controller.js  */

angular.module('stepApp')
    .controller('ProfileController',
        ['$scope', 'Sessions', 'User', 'Employee,Reference', 'Principal', 'ParseLinks',
            function ($scope, Sessions, User, Employee, Reference, Principal, ParseLinks) {

                $scope.employee = {};
                $scope.employee.id = null;
                $scope.employee.user = {};

                Principal.identity().then(function (account) {
                    $scope.settingsAccount = account;
                    $scope.user = User.get({'login': $scope.settingsAccount.login});
                });


                Employee.get({id: 'my'}, function (result) {
                    $scope.employee = result;
                });


                var onSaveSuccess = function (result) {
                    $scope.$emit('stepApp:employeeUpdate', result);
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };

                $scope.employeeUpdate = function () {
                    $scope.isSaving = true;
                    if ($scope.employee.id != null) {
                        Employee.update($scope.employee, onSaveSuccess, onSaveError);
                    } else {
                        $scope.employee.user.id = $scope.user.id;
                        Employee.save($scope.employee, onSaveSuccess, onSaveError);
                    }
                };

                $scope.references = function () {
                    Reference.query({page: $scope.page, size: 20}, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers('link'));
                        $scope.references = result;
                        $scope.total = headers('x-total-count');
                    });
                }

            }]);

/* resume-attachment.controller.js */

angular.module('stepApp').controller('ResumeAttachmentController',
    ['$scope', '$rootScope', '$stateParams', 'Principal', '$state', 'JpEmployee',
        function ($scope, $rootScope, $stateParams, Principal, $state, JpEmployee) {

            $scope.content = '';
            $scope.preview = false;
            $scope.showCv = false;
            Principal.identity().then(function (account) {
                $scope.account = account;
            });

            JpEmployee.get({id: 'my'}, function (result) {
                $scope.jpEmployee = result;
                $scope.showAlert = false;
                if ($scope.jpEmployee.cv) {
                    var aType = $scope.jpEmployee.cvContentType;
                    $scope.preview = false;
                    if (aType.indexOf("image") >= 0 || aType.indexOf("pdf") >= 0) {
                        $scope.preview = true;
                    }
                    var blob = $rootScope.b64toBlob($scope.jpEmployee.cv, aType);
                    $scope.content = (window.URL || window.webkitURL).createObjectURL(blob);
                }
            }, function (res) {
                if (res.status == 404) {
                    $scope.showAlert = true;
                }
            });

            var onSaveSuccess = function (result) {
                $scope.$emit('stepApp:employee', result);
                $scope.isSaving = false;
                $state.go('resume');
            };
            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.jpEmployee.id != null) {
                    JpEmployee.update($scope.jpEmployee, onSaveSuccess, onSaveError);
                }
            };

            $scope.showMyCv = function () {
                $scope.showCv = !$scope.showCv;
            };

            $scope.setCv = function ($file, jpEmployee) {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            jpEmployee.cv = base64Data;
                            jpEmployee.cvContentType = $file.type;
                            jpEmployee.cvName = $file.name;
                            $scope.fileAdded = "File added";
                            var aType = jpEmployee.cvContentType;
                            $scope.preview = false;
                            if (aType.indexOf("image") >= 0 || aType.indexOf("pdf") >= 0) {
                                $scope.preview = true;
                            }
                            var blob = $rootScope.b64toBlob($scope.jpEmployee.cv, aType);
                            $scope.content = (window.URL || window.webkitURL).createObjectURL(blob);
                        });
                    };
                }
            };
        }]);

/* view-resume.controller.js */
angular.module('stepApp')
    .controller('ViewResumeController',
        ['$scope', 'JpEmployeeReferenceJpEmployee', 'JpLanguageProfeciencyJpEmployee', 'AcademicQualificationJpEmployee', 'JpEmployeeTrainingJpEmployee', 'JpEmployeeExperienceJpEmployee', 'JpEmploymentHistoryJpEmployee', 'Principal', 'JpEmployee',
            function ($scope, JpEmployeeReferenceJpEmployee, JpLanguageProfeciencyJpEmployee, AcademicQualificationJpEmployee, JpEmployeeTrainingJpEmployee, JpEmployeeExperienceJpEmployee, JpEmploymentHistoryJpEmployee, Principal, JpEmployee) {
                Principal.identity().then(function (account) {
                    $scope.account = account;
                });
                $scope.loadAll = function () {
                    JpEmployee.get({id: 'my'}, function (result) {
                        $scope.jpEmployee = result;
                        var blob = b64toBlob($scope.jpEmployee.picture);
                        $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);

                        AcademicQualificationJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                            $scope.academicQualifications = result;

                        });
                        JpEmploymentHistoryJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                            $scope.employments = result;
                        });
                        JpEmployeeExperienceJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                            $scope.skills = result;
                        });
                        JpEmployeeTrainingJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                            $scope.trainings = result;
                        });
                        JpLanguageProfeciencyJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                            $scope.langs = result;
                        });
                        JpEmployeeReferenceJpEmployee.query({id: $scope.jpEmployee.id}, function (result, headers) {
                            $scope.references = result;
                        });
                    })
                };

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }

                        var byteArray = new Uint8Array(byteNumbers);

                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }

                $scope.loadAll();

            }]);
