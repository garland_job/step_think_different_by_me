'use strict';
angular.module('stepApp').controller('NameCancelApplicationController',
    [ '$rootScope', '$scope', '$stateParams', '$parse', '$state', 'entity', 'NameCnclApplication','Attachment','AttachmentCategoryByApplicationName','InstEmployeeOfCurrentInstByIndex','NameCnclApplicationByEmpCode',
    function ($rootScope, $scope, $stateParams, $parse, $state, entity, NameCnclApplication, Attachment,AttachmentCategoryByApplicationName, InstEmployeeOfCurrentInstByIndex, NameCnclApplicationByEmpCode) {

        $scope.currentSection = 'personal';
        $scope.applicantAttachment = [];
        $scope.employee = {};
        $scope.applicantAttachmentCount = [0];
        $scope.attachmentCategories = [];
        $scope.attachmentList = [];
        $scope.mpoAttachmentType = '';
        $scope.mpoApplication = {};
        $scope.search = null;
        $scope.mpoForm = false;
        $scope.showAddMoreButton = false;
        $scope.currentSelectItem = [];
        $scope.mpoFormHasError = true;
        $scope.duplicateError = false;
        $scope.showMsg = false;
        $scope.nameCnclApplication = {};
        $scope.noEmpFound = false;
        $scope.teacherIndexNo = null;
        $scope.teacherFoundMsg = false;

        $rootScope.calendar = {
            opened: {},
            dateFormat: 'dd-MM-yyyy',
            dateOptions: {},
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $rootScope.calendar.opened[which] = true;
            }
        };
        $scope.searchTeacher = function (teacherIndexNo) {
            console.log(teacherIndexNo);
            InstEmployeeOfCurrentInstByIndex.get({index: teacherIndexNo}, function(result) {
                $scope.employee = result;
                NameCnclApplicationByEmpCode.get({code: result.code}, function(application){
                    if(application.status != 1){
                        $state.go('instituteNameCancelList', {}, {reload: true});
                        $rootScope.setSuccessMessage('Application Already Submitted! ');
                    }
                });
                $scope.teacherFoundMsg = false;
            }, function(response) {
                if(response.status === 404) {
                    $scope.teacherFoundMsg = true;
                    $scope.employee = {};
                }if(response.status === 500) {
                    $scope.teacherFoundMsg = true;
                    $scope.employee={};
                }
            });
        };

        AttachmentCategoryByApplicationName.get({name: 'NameCancle'}, function (result) {
            $scope.attachmentCategoryList = result;
        });
        $scope.toDay = new Date();
        $scope.isInArray = function isInArray(value, array) {
            return array.indexOf(value) > -1;
        };
        $scope.load = function (id) {
            //Employee.get({id: id}, function (result) {
            //    $scope.employee = result;
            //});
        };

        $scope.save = function () {

            if($scope.employee){
            }
            $scope.isSaving = true;

            $scope.nameCnclApplication.instEmployee = $scope.employee;

            console.log($scope.applicantAttachmentCount);
            //employee attachment
            angular.forEach($scope.applicantAttachmentCount, function (value, key) {
                if ($scope.applicantAttachment[value].attachment != '') {
                    $scope.applicantAttachment[value].instEmployee = {};
                    $scope.applicantAttachment[value].name = $scope.applicantAttachment[value].attachmentCategory.attachmentName;
                    $scope.applicantAttachment[value].instEmployee.id = $scope.employee.id;
                    Attachment.save($scope.applicantAttachment[value]);
                }
            });
            NameCnclApplication.save($scope.nameCnclApplication);
            $rootScope.setSuccessMessage('Application Submitted Successfully.');
            $scope.isSaving = false;
            $state.go('nameCncl-statusLog', {'code':$scope.employee.code}, {reload: true});
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };


        $scope.hideMpoForm = function () {
            $scope.mpoForm = false;
        };

        $scope.timescaleNext = function (current, next) {
            $scope.currentSection = next;
            $scope.prevSection = current;
        }
        $scope.timescalePrev = function (current, prev) {
            $scope.currentSection = prev;
            $scope.nextSection = current;
        }
        $scope.addMoreEducation = function () {
            $scope.applicantEducationCount.push($scope.applicantEducationCount.length);
            $scope.applicantEducation[$scope.applicantEducationCount.length].examYear = '';

        }
        $scope.addMoreAttachment = function () {
            //removeItem
            if (!inArray($scope.applicantAttachmentCount.length, $scope.applicantAttachmentCount)) {
                $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
            } else {
                $scope.applicantAttachmentCount.length++;
                $scope.applicantAttachmentCount.push($scope.applicantAttachmentCount.length);
            }

            $scope.showAddMoreButton = false;
        }
        $scope.removeAttachment = function (attachment) {
            $scope.showAddMoreButton = true;
            $scope.mpoFormHasError = true;
            var index = $scope.applicantAttachmentCount.indexOf(attachment);
            $scope.applicantAttachmentCount.splice(index, 1);

            if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = false;
            }
        }
        $scope.addMoreTraining = function () {
            $scope.applicantTrainingCount.push($scope.applicantTrainingCount.length);
            $scope.applicantTraining[$scope.applicantTrainingCount.length].gpa = '';
        }

        $scope.educationHtml = function () {

        };

        $scope.cancel = function () {
            $state.go('mpo.dashboard', null, {reload: true});
        };

        $scope.setAttachment = function($index, attachment,noAttach) {
            if(noAttach ){
                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }else{
                    $scope.showAddMoreButton = true;
                }
            }
            try{
                /*$scope.showAddMoreButton = true;*/
                if(attachment==""){
                    $scope.mpoFormHasError = true;
                }else{
                    if(!noAttach && attachment.file)
                        $scope.showAddMoreButton = true;
                    if(noAttach && (attachment.remarks == undefined || attachment.remarks==""))
                        $scope.showAddMoreButton = true;
                }
                attachment.attachmentCategory = angular.fromJson(attachment.attachment);

                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }

                if(attachment.attachmentCategory.id)
                {
                    $scope.currentSelectItem[$index] = attachment.attachmentCategory.id;
                }
            }catch(e) {
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = true;
                $scope.currentSelectItem.splice($index, 1);
            }

            //if($scope.attachmentCategoryList.length-1 == arrayUnique($scope.currentSelectItem).length){
            if($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length){
                $scope.mpoFormHasError = false;
                angular.forEach($scope.applicantAttachment, function (value, key) {

                    if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                        $scope.mpoFormHasError = true;
                    }else {
                        if (value.fileName) {
                            $scope.mpoFormHasError = true;
                        }
                    }

                });
            }
            else{
                angular.forEach($scope.applicantAttachment, function (value, key) {
                    console.log(value);

                    if (!value.noAttachment && (value.fileName)) {
                        $scope.mpoFormHasError = true;

                    }
                });
                /* $scope.mpoFormHasError = true;*/
            }

            if(arrayUnique($scope.currentSelectItem).length != $scope.currentSelectItem.length)
                $scope.duplicateError = true;
            else
                $scope.duplicateError = false;

            if($scope.duplicateError){
                $scope.showAddMoreButton = false;
            }
        }
        $scope.remarksChange = function(noAttachment,remarks)
        {
            console.log(noAttachment);
            console.log(remarks);
            //if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length-1){

            if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = false;
                angular.forEach($scope.applicantAttachment, function (value, key) {

                    if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                        $scope.mpoFormHasError = true;
                    }
                });

            }else{
                if(noAttachment && !(remarks == undefined || remarks=="")){
                    $scope.showAddMoreButton = true;
                }else{
                    $scope.showAddMoreButton = false;
                }
            }
        }
        $scope.selectNoAttachment = function(val,val2,file,remarks)
        {
            if(val && val2){
                // if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length-1){
                if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                    $scope.mpoFormHasError = false;
                    angular.forEach($scope.applicantAttachment, function (value, key) {
                        if (value.noAttachment && (value.remarks == undefined || value.remarks=="")) {
                            $scope.mpoFormHasError = true;
                        }
                    });

                }else{
                    if (val && (remarks == undefined || remarks=="")) {
                        $scope.mpoFormHasError = true;
                        $scope.showAddMoreButton = false;
                    }else{
                        $scope.showAddMoreButton = true;
                    }

                }
            }else{
                if(file==undefined){
                    $scope.mpoFormHasError = true;
                    $scope.showAddMoreButton = false;
                }else{
                    $scope.showAddMoreButton = true;
                }

                if($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length){
                    $scope.showAddMoreButton = false;
                }
                /*$scope.showAddMoreButton = false;*/
            }
            if($scope.applicantAttachmentCount.length === $scope.attachmentCategoryList.length){
                $scope.showAddMoreButton = false;
            }
        };
        function inArray(needle, haystack) {
            var length = haystack.length;
            for (var i = 0; i < length; i++) {
                if (typeof haystack[i] == 'object') {
                    if (arrayCompare(haystack[i], needle)) return true;
                } else {
                    if (haystack[i] == needle) return true;
                }
            }
            return false;
        }

        function arrayUnique(a) {
            return a.reduce(function (p, c) {
                if (p.indexOf(c) < 0) p.push(c);
                return p;
            }, []);
        };

        function arrayCompare(a1, a2) {
            if (a1.length != a2.length) return false;
            var length = a2.length;
            for (var i = 0; i < length; i++) {
                if (a1[i] !== a2[i]) return false;
            }
            return true;
        }

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }

            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }

            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }

            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };

        $scope.setFile = function ($file, attachment) {
            $scope.showAddMoreButton = true;
            $scope.mpoFormHasError = true;
            try {
                if ($file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($file);
                    fileReader.onload = function (e) {
                        var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                        $scope.$apply(function () {
                            try {
                                attachment.file = base64Data;
                                attachment.fileContentType = $file.type;
                                attachment.fileName = $file.name;
                            } catch (e) {
                                $scope.showAddMoreButton = false;
                            }
                        });
                    };

                    if ($scope.applicantAttachmentCount.length == $scope.attachmentCategoryList.length) {
                        $scope.showAddMoreButton = false;
                    }

                    if ($scope.attachmentCategoryList.length == arrayUnique($scope.currentSelectItem).length)
                        $scope.mpoFormHasError = false;
                    else
                        $scope.mpoFormHasError = true;
                }
            } catch (e) {
                $scope.showAddMoreButton = false;
                $scope.mpoFormHasError = true;
            }
        };
    }]);

/*name-cancel-status.controller.js*/

angular.module('stepApp')
    .controller('NameCancelStatusLogController',
        ['$scope', '$rootScope', '$state','NameCnclApplicationLogEmployeeCode', '$stateParams',
            function ($scope, $rootScope, $state,NameCnclApplicationLogEmployeeCode, $stateParams) {
                $scope.applicationlogs =[];
                $scope.showSearchForm = true;
                NameCnclApplicationLogEmployeeCode.get({'code':$stateParams.code}, function(result) {
                    console.log(result);
                    $scope.applicationlogs = result;
                });
    }]);

/* name-cancellation-approve-dialog.controller.js  */
angular.module('stepApp')
    .controller('NameCancellationApproveDialogController',
        ['$scope','$rootScope','NameCnclApplication', '$stateParams', '$state', '$modalInstance',
            function ($scope,$rootScope,NameCnclApplication, $stateParams, $state,$modalInstance) {

                $scope.confirmApprove = function(){
                    NameCnclApplication.get({id: $stateParams.id}, function(result){
                        NameCnclApplication.update(result, onSaveSuccess, onSaveError);
                    });
                }
                var onSaveSuccess = function(result){
                    $modalInstance.close();
                    $rootScope.setSuccessMessage('Application Approved Successfully.');
                    $state.go('nameCnclApplicationDetails',null,{reload: true});
                }
                var onSaveError = function(result){
                }
                $scope.clear = function(){
                    $modalInstance.close();
                    $state.go('nameCnclApplicationDetails');
                }

    }]);

/* name-cancellation-checklist.controller.js */
angular.module('stepApp')
    .controller('NameCnclApplicationCheckListController',
        ['$scope', '$rootScope','NameCnclApplicationLogEmployeeCode', '$stateParams',
            function ($scope, $rootScope,NameCnclApplicationLogEmployeeCode, $stateParams) {

                $scope.showSearchForm = true;
                NameCnclApplicationLogEmployeeCode.get({'code':$stateParams.code}, function(result) {
                    $scope.timescaleApplicationlogs = result;
                });
    }]);
/* name-cancellation-deny-dialog.controller.js */
/*angular.module('stepApp')
    .controller('ProfessorDenyDialogController',
        ['$scope','TimeScaleApplicationStatusLog', '$stateParams', 'DateUtils', '$state', '$modalInstance',
            function ($scope,TimeScaleApplicationStatusLog, $stateParams, DateUtils, $state, $modalInstance) {
                /!*$scope.timescaleApplication = entity;*!/
                $scope.timescaleApplication = $stateParams.apAll;
                $scope.timescaleApplicationStatusLog = {};

                $scope.confirmDecline = function(){
                    if($scope.timescaleApplication.id > 0 && $scope.causeDeny){
                        $scope.timescaleApplicationStatusLog.status = 1;
                        $scope.timescaleApplicationStatusLog.timeScaleApplicationId = $scope.timescaleApplication;
                        $scope.timescaleApplicationStatusLog.cause=$scope.causeDeny;
                        $scope.timescaleApplicationStatusLog.fromDate= DateUtils.convertLocaleDateToServer(new Date());
                        TimeScaleApplicationStatusLog.save($scope.timescaleApplicationStatusLog, onSaveSuccess, onSaveError);

                    }
                }
                var onSaveSuccess = function(result){
                    $modalInstance.close();
                }
                var onSaveError = function(result){

                }
                $scope.clear = function(){
                    $modalInstance.close();
                }
                $scope.submitForm = function(){
                    //$modalInstance.close();
                }
    }]);*/
/* name-cancellation-details.controller.js */
angular.module('stepApp')
    .controller('NameCnclDetailsController',
        ['$scope', '$state', '$modal','NameCnclApplicationCheck', 'NameCnclApplicationLogEmployeeCode','NameCnclApplication','entity','InstEmployeeCode','$stateParams','Principal','AttachmentByName','$rootScope','AttachmentByEmployeeAndName',
            function ($scope, $state, $modal,NameCnclApplicationCheck, NameCnclApplicationLogEmployeeCode,NameCnclApplication,entity,InstEmployeeCode,$stateParams,Principal,AttachmentByName,$rootScope,AttachmentByEmployeeAndName) {
                $scope.url = null;
                $scope.employee = entity;
                $scope.isCollapsed = false;
                $scope.showApproveButton=false;
                $scope.showApplicationButtion=true;

                $scope.checkStatus = function(){
                    if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                        return 1;
                    }else if(Principal.hasAnyAuthority(['ROLE_INSTITUTE'])){
                        return 2;
                    }else if(Principal.hasAnyAuthority(['ROLE_MANEGINGCOMMITTEE'])){
                        return 3;
                    }else if(Principal.hasAnyAuthority(['ROLE_DEO'])){
                        return 4;
                    }else if(Principal.hasAnyAuthority(['ROLE_FRONTDESK'])){
                        return 5;
                    }else if(Principal.hasAnyAuthority(['ROLE_DTE_EMPLOYEE'])){
                        return 6;
                    }else if(Principal.hasAnyAuthority(['ROLE_AD'])){
                        return 7;
                    }else if(Principal.hasAnyAuthority(['ROLE_DIRECTOR'])){
                        return 9;
                    }else if(Principal.hasAnyAuthority(['ROLE_DG'])){
                        return 10;
                    }else if(Principal.hasAnyAuthority(['ROLE_MPOCOMMITTEE'])){
                        return 11;
                    }else{
                        return 0;
                    }
                };
                $scope.profile = function(){

                    if(Principal.hasAnyAuthority(['ROLE_INSTEMP'])){
                        Principal.identity().then(function (account) {
                            $scope.account = account;
                            NameCnclApplicationCheck.get({'code': $scope.account.login}, function (res) {
                                console.log(res);
                                if (res.id == 0) {
                                    $rootScope.setErrorMessage('You Name Cancellation Application Yet Not Complete. Please Complete your Application Procedure');
                                    $state.go('mpo.application');
                                }
                            });
                            InstEmployeeCode.get({'code':$scope.account.login},function(res){
                                $scope.employee = res.instEmployee;

                                NameCnclApplicationCheck.get({'code':$scope.employee.code}, function(result) {
                                    console.log(result);
                                    if(result.id>0){
                                        $scope.showApplicationButtion=false;
                                    }
                                    $scope.employee = res.instEmployee;
                                    $scope.employee.id = res.instEmployee.id;
                                    $scope.address = res.instEmpAddress;
                                    $scope.applicantEducation = res.instEmpEduQualisList;
                                    $scope.applicantTraining = res.instEmplTrainings;
                                    $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                                    $scope.instEmplBankInfo = res.instEmplBankInfo;
                                    AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"NameCancle"}, function(result){
                                        $scope.attachments = result;

                                    });
                                });

                            });
                        });
                    }else{

                        NameCnclApplication.get({id:$stateParams.id}, function(result) {
                            $scope.nameCnclApplication = result;
                            if($stateParams.id>0){
                                $scope.showApplicationButtion=false;
                            }
                            if( $scope.checkStatus()!=0 & $scope.checkStatus() ==result.status){
                                $scope.showApproveButton=true;
                                $scope.mpoId = result.id;
                            }else{
                                $scope.showApproveButton=false;
                            }

                            InstEmployeeCode.get({'code':result.instEmployee.code},function(res){
                                $scope.employee = res.instEmployee;
                                $scope.employee.id = res.instEmployee.id;
                                $scope.address = res.instEmpAddress;
                                $scope.applicantEducation = res.instEmpEduQualisList;
                                $scope.applicantTraining = res.instEmplTrainings;
                                $scope.instEmplRecruitInfo = res.instEmplRecruitInfo;
                                $scope.instEmplBankInfo = res.instEmplBankInfo;
                                AttachmentByEmployeeAndName.query({id: $scope.employee.id, applicationName:"NameCancle"}, function(result){
                                    $scope.attachments = result;
                                });
                                NameCnclApplicationLogEmployeeCode.get({'code':$scope.employee.code}, function(result) {
                                    $scope.mpoApplicationlogs = result;
                                });
                            });
                        });
                    }
                };
                $scope.educations = function(){
                    $scope.applicantEducations = $scope.applicantEducation;
                };


                $scope.training = function(){
                };
                $scope.attachment = function(){
                };

                $scope.displayAttachment= function(applicaiton) {

                    AttachmentByName.get({id:applicaiton.id},applicaiton,function(response){
                        var blob = b64toBlob(response.file, applicaiton.fileContentType);
                        $scope.url = (window.URL || window.webkitURL).createObjectURL( blob );
                    });

                }

                function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }

                        var byteArray = new Uint8Array(byteNumbers);

                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {type: contentType});
                    return blob;
                }
                $scope.previewImage = function (content, contentType,name) {
                    var blob = $rootScope.b64toBlob(content, contentType);
                    $rootScope.viewerObject.content = (window.URL || window.webkitURL).createObjectURL(blob);
                    $rootScope.viewerObject.contentType = contentType;
                    $rootScope.viewerObject.pageTitle = name;
                    // call the modal
                    $rootScope.showFilePreviewModal();
                };

            }]);
/* name-cancellation-list.controller.js */
angular.module('stepApp')
    .controller('NameCancelListController',
        ['$scope', 'entity', '$state', '$modal','NameCnclApplicationList','NameCnclApplication',
            function ($scope, entity, $state, $modal,NameCnclApplicationList,NameCnclApplication) {

                $scope.timescaleAppliedLists = entity;

                $scope.employees = [];
                $scope.page = 0;
                NameCnclApplication.query({page: $scope.page, size: 2000}, function(result, headers) {
                    $scope.mpoApplications = result;
                });

                $scope.loadPage = function(page) {
                    $scope.page = page;
                    $scope.loadAll();
                };
                $scope.search = function () {
                    //EmployeeSearch.query({query: $scope.searchQuery}, function(result) {
                    //    $scope.employees = result;
                    //}, function(response) {
                    //    if(response.status === 404) {
                    //        $scope.loadAll();
                    //    }
                    //});
                };
                $scope.refresh = function () {
                    $scope.loadAll();
                    $scope.clear();
                };
                $scope.clear = function () {
                    $scope.employee = {
                        name: null,
                        nameEnglish: null,
                        father: null,
                        mother: null,
                        presentAddress: null,
                        permanentAddress: null,
                        gender: null,
                        dob: null,
                        zipCode: null,
                        registrationCertificateSubject: null,
                        registrationExamYear: null,
                        registrationCetificateNo: null,
                        indexNo: null,
                        bankName: null,
                        bankBranch: null,
                        bankAccountNo: null,
                        designation: null,
                        subject: null,
                        payScale: null,
                        payScaleCode: null,
                        month: null,
                        monthlySalaryGovtProvided: null,
                        monthlySalaryInstituteProvided: null,
                        gbResolutionReceiveDate: null,
                        gbResolutionAgendaNo: null,
                        circularPaperName: null,
                        circularPublishedDate: null,
                        recruitExamDate: null,
                        recruitApproveGBResolutionDate: null,
                        recruitPermitAgendaNo: null,
                        recruitmentDate: null,
                        presentInstituteJoinDate: null,
                        presentPostJoinDate: null,
                        dgRepresentativeType: null,
                        dgRepresentativeName: null,
                        dgRepresentativeDesignation: null,
                        dgRepresentativeAddress: null,
                        representativeType: null,
                        boardRepresentativeName: null,
                        boardRepresentativeDesignation: null,
                        boardRepresentativeAddress: null,
                        salaryCode: null,
                        id: null
                    };
                };
                // bulk operations start
                $scope.areAllEmployeesSelected = false;
                $scope.updateEmployeesSelection = function (employeeArray, selectionValue) {
                    for (var i = 0; i < employeeArray.length; i++)
                    {
                        employeeArray[i].isSelected = selectionValue;
                    }
                };


                //$scope.import = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};
                //$scope.export = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            //Employee.update(employee);
                //            //TODO: handle bulk export
                //        }
                //    }
                //};

                //$scope.deleteSelected = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.delete(employee);
                //        }
                //    }
                //};
                //$scope.sync = function (){
                //    for (var i = 0; i < $scope.employees.length; i++){
                //        var employee = $scope.employees[i];
                //        if(employee.isSelected){
                //            Employee.update(employee);
                //        }
                //    }
                //};
                $scope.order = function (predicate, reverse) {
                    $scope.predicate = predicate;
                    $scope.reverse = reverse;
                    //Employee.query({page: $scope.page, size: 20}, function (result, headers) {
                    //    $scope.links = ParseLinks.parse(headers('link'));
                    //    $scope.employees = result;
                    //    $scope.total = headers('x-total-count');
                    //});
                };
                // bulk operations end
            }]);
/* name-cancellation-list-institute.controller.js */
angular.module('stepApp')
    .controller('NameCancelListForInstituteController',
        ['$scope','NameCnclApplicationListCurrInst',
            function ($scope,NameCnclApplicationListCurrInst) {
                $scope.nameCnclAppliedLists = [];
                NameCnclApplicationListCurrInst.query({page:0, size:100}, function(result){
                    $scope.nameCnclAppliedLists = result;
                });
    }]);
angular.module('stepApp')
    .controller('NameCnclDeclineController',
        ['$scope','$stateParams','Principal','NameCnclApplication', 'entity', 'DateUtils', '$state', '$modalInstance','NameCnclAppDecline',
        function ($scope,$stateParams,Principal,NameCnclApplication, entity, DateUtils, $state, $modalInstance,NameCnclAppDecline) {
            $scope.nameCnclApplication = entity;
            $scope.nameCnclApplicationStatusLog = {};
            $scope.causeDeny = "";
            if($stateParams.id !=null){
                NameCnclApplication.get({id : $stateParams.id},function(nameCnclAppResult){
                    $scope.nameCnclApplication = nameCnclAppResult;
                });
            }

            $scope.confirmDecline = function(){
                NameCnclAppDecline.decline({id: $scope.nameCnclApplication.id, cause: $scope.causeDeny},{},onSaveSuccess, onSaveError);
            };

            var onSaveSuccess = function(result){
                $modalInstance.close();
            };

            var onSaveError = function(result){

            };
            $scope.clear = function(){
                $modalInstance.close();
                window.history.back();
            };

    }]);
