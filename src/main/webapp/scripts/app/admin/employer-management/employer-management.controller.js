'use strict';

angular.module('stepApp')
    .controller('EmployerManagementController',
     ['$scope', 'PendingTempEmployerInstitute', 'RejectedTempEmployerInstitute','User','ApprovedTempEmployerInstitute', 'TempEmployerSearch', 'ParseLinks',  'EmployerByUserId', 'TempEmployer',
          function ($scope, PendingTempEmployerInstitute, RejectedTempEmployerInstitute,User,ApprovedTempEmployerInstitute, TempEmployerSearch, ParseLinks,EmployerByUserId, TempEmployer) {

        $scope.singleView = false;
        $scope.pendingEmployers = [];
        $scope.approvedEmployers = [];
        $scope.rejectedEmployers = [];
        $scope.authorities = ["ROLE_ADMIN","ROLE_JPADMIN"];
        $scope.showOldInfo = true;

        $scope.tempEmployer = {};
        $scope.employer = {};

        $scope.pagePending = 0;
        $scope.pageApproved = 0;
        $scope.pageRejected = 0;
        $scope.perPage = 5000000;

        $scope.pendingEmployer = function () {
            $scope.showOldInfo = true;
            PendingTempEmployerInstitute.query({page: $scope.pagePending, size: $scope.perPage}, function (result, headers) {
                $scope.linksPending = ParseLinks.parse(headers('link'));
                $scope.pendingEmployers = result;
                $scope.totalPending = headers('x-total-count');
            });
            $scope.hideSingleView();
        };

        $scope.loadPagePendingEmployer = function(pagePending) {

            $scope.pagePending = pagePending;
            $scope.pendingEmployer();
        };

        $scope.approvedEmployer = function () {
            $scope.showOldInfo = false;
            ApprovedTempEmployerInstitute.query({page: $scope.pageApproved, size: $scope.perPage}, function (result, headers) {
                $scope.linksApproved = ParseLinks.parse(headers('link'));
                $scope.approvedEmployers = result;
                $scope.totalApproved = headers('x-total-count');
            });
            $scope.hideSingleView();
        };

        $scope.loadPageApprovedEmployer = function(pageApproved) {

            $scope.pageApproved = pageApproved;
            $scope.approvedEmployer();
        };

        $scope.rejectedEmployer = function () {
            RejectedTempEmployerInstitute.query({page: $scope.pageRejected, size: $scope.perPage}, function (result, headers) {
                $scope.linksRejected = ParseLinks.parse(headers('link'));
                $scope.rejectedEmployers = result;
                $scope.totalRejected = headers('x-total-count');
            });
            $scope.hideSingleView();
        };

        $scope.loadPageRejectedEmployer = function(pageRejected) {
            $scope.showOldInfo = false;
            $scope.pageRejected = pageRejected;
            $scope.rejectedEmployer();
        };

        $scope.employerDetail = function (employerId) {

            TempEmployer.get({id: employerId}, function(tempEmployer){
                $scope.tempEmployer = tempEmployer;

                EmployerByUserId.get({id: $scope.tempEmployer.user.id}, function (employer) {
                        $scope.employer = employer;
                    },
                    function (response) {
                        if (response.status === 404) {
                            $scope.employer = {};
                        }
                    }
                );
            });

            $scope.showSingleView();
        };

        $scope.showSingleView = function()
        {
            $scope.singleView = true;
        };

        $scope.hideSingleView = function()
        {
            $scope.singleView = false;
        };

        $scope.setActive = function (user, isActivated) {
            user.activated = isActivated;
            User.update(user, function () {
                $scope.loadAll();
                $scope.clear();
            });
        };

        $scope.order = function (predicate, reverse) {
            $scope.predicate = predicate;
            $scope.reverse = reverse;
            User.query({page: $scope.page, per_page: 20}, function (result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.users = result;
                $scope.total = headers('x-total-count');
            });
        };

        $scope.search = function () {
            TempEmployerSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pendingEmployers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.sync = function (){
            console.log('dfasdf');
            TempEmployer.query(function(results){
                alert('>>>>>>>>>>>>>>>.');
                console.log(results);
            });
            //console.log(all);
            //TempEmployer.update(results);
        };

        $scope.areAllTempEmployersSelected = false;

        $scope.updateTempEmployersSelection = function (TempEmployerArray, selectionValue) {
            for (var i = 0; i < TempEmployerArray.length; i++)
            {
                TempEmployerArray[i].isSelected = selectionValue;
            }
        };

    }]);

/* employer-activate-dialog.controller.js */

angular.module('stepApp')
    .controller('EmployerActivateController',
        ['$scope', 'entity', '$modalInstance', 'User', 'Employer', 'TempEmployer', 'EmployerByUserId',
            function ($scope, entity, $modalInstance, User, Employer, TempEmployer, EmployerByUserId) {

                $scope.employer = entity;
                $scope.tempEmployer = {};
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmActivate = function (id) {
                    TempEmployer.get({id: id}, function (result) {
                        $scope.tempEmployer = result;
                        $scope.tempEmployer.status = 'approved';
                        TempEmployer.update($scope.tempEmployer, afterUpdateTempEmployer);
                    });
                };

                var afterUpdateTempEmployer = function (tempEmployer) {
                    EmployerByUserId.get({id: tempEmployer.user.id}, function (employer) {
                            tempEmployer.status = null;
                            tempEmployer.id = employer.id;
                            Employer.update(tempEmployer, afterUpdateEmployer);
                        },
                        function (response) {
                            if (response.status === 404) {
                                tempEmployer.id = null;
                                tempEmployer.status = null;
                                Employer.save(tempEmployer, afterUpdateEmployer);
                            }
                        }
                    );
                    $scope.$emit('stepApp:tempEmployerUpdate', tempEmployer);
                };

                var afterUpdateEmployer = function (result) {
                    $scope.$emit('stepApp:EmployerUpdate', result);
                    $modalInstance.close(true);
                };

            }]);

/* employer-deactivate-dialog.controller.js */
angular.module('stepApp')
    .controller('EmployerDeactivateController',
        ['$scope', 'entity', '$modalInstance', 'User', 'Employer', 'TempEmployer',
            function ($scope, entity, $modalInstance, User, Employer, TempEmployer) {

                $scope.employer = entity;
                $scope.tempEmployer = {};
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.confirmDeactivate = function (id) {
                    TempEmployer.get({id: id}, function (result) {
                        $scope.tempEmployer = result;
                        $scope.tempEmployer.status = 'deActivated';
                        TempEmployer.update($scope.tempEmployer, afterUpdateTempEmployer);
                    });
                };

                var afterUpdateTempEmployer = function (tempEmployer) {
                    $modalInstance.close(true);
                };

            }]);

/* employer-delete-dialog.controller.js */

angular.module('stepApp')
    .controller('EmployerDeleteController',
    ['$scope', '$modalInstance', 'entity', 'TempEmployer',
    function ($scope, $modalInstance, entity, TempEmployer) {

        $scope.employer = entity;
        $scope.clear = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            TempEmployer.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    }]);

/* employer-reject-dialog.controller.js */

angular.module('stepApp')
    .controller('EmployerRejectController',
        ['$scope', '$modalInstance', 'entity', 'TempEmployer',
            function ($scope, $modalInstance, entity, TempEmployer) {

            $scope.tempEmployer = entity;
            $scope.clear = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.confirmReject = function (id) {
                if (id != null) {
                    $scope.tempEmployer.dateRejected = new Date();
                    if($scope.tempEmployer.status == 'pending')
                        $scope.tempEmployer.status = 'rejected';
                    else if($scope.tempEmployer.status == 'update')
                        $scope.tempEmployer.status = 'updateReject';
                    TempEmployer.update($scope.tempEmployer, afterUpdateTempEmployer);
                };
            };
            var afterUpdateTempEmployer = function (tempEmployer) {
                $scope.$emit('stepApp:tempEmployerUpdate', tempEmployer);
                $modalInstance.close(true);
            };
    }]);

    angular.module('stepApp')
    .controller('JpOrganizationListController',
        ['$scope', '$state', 'Principal','Employer','ParseLinks',
            function ($scope, $state, Principal,Employer,ParseLinks) {

                $scope.pagePending = 0;
                $scope.pageApproved = 0;
                $scope.pageRejected = 0;
                $scope.perPage = 5000000;

            Employer.query({page: $scope.pageApproved, size: $scope.perPage}, function (result, headers) {
                $scope.linksApproved = ParseLinks.parse(headers('link'));
                $scope.approvedEmployers = result;
                $scope.totalApproved = headers('x-total-count');
            });

     }]);

/* employer-approve-dialog.controller.js */

angular.module('stepApp')
    .controller('EmployerApproveController',
        ['$scope', '$modalInstance', 'entity', 'User', 'TempEmployer', 'Employer', 'EmployerByUserId',
            function ($scope, $modalInstance, entity, User, TempEmployer, Employer, EmployerByUserId) {

                $scope.employer = entity;
                $scope.isSaving = false;
                console.log($scope.employer);
                $scope.clear = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.confirmApprove = function (id) {
                    $scope.isSaving = true;
                    $scope.tempEmployer = {};

                    TempEmployer.get({id: id}, function (result) {
                            console.log('print result :'+id);
                            $scope.tempEmployer = result;
                            console.log($scope.tempEmployer);
                            if ($scope.tempEmployer.id != null) {
                                $scope.tempEmployer.status = 'approved';
                                TempEmployer.update($scope.tempEmployer, afterUpdateTempEmployer);
                            }
                        }
                    );
                };

                var afterUpdateTempEmployer = function (tempEmployer) {
                    EmployerByUserId.get({id: tempEmployer.user.id}, function (employer) {
                            tempEmployer.status = null;
                            tempEmployer.id = employer.id;
                            tempEmployer.dateApproved = new Date();
                            Employer.update(tempEmployer, afterUpdateEmployer);
                        },
                        function (response) {
                            if (response.status === 404) {
                                tempEmployer.id = null;
                                tempEmployer.status = null;
                                Employer.save(tempEmployer, afterUpdateEmployer);
                            }
                        }
                    );
                    $scope.$emit('stepApp:tempEmployerUpdate', tempEmployer);
                };

                var afterUpdateEmployer = function (result) {
                    $scope.$emit('stepApp:EmployerUpdate', result);
                    $modalInstance.close(true);
                    $scope.isSaving = false;
                };

            }]);

