/* globals $ */
'use strict';

angular.module('stepApp')
    .directive('showValidation', function($timeout) {
        return {
            restrict: 'A',
            require: 'form',
            link: function (scope, element) {
                element.find('.form-group').each(function() {
                    var $formGroup = $(this);
                    var $inputs = $formGroup.find('input[ng-model],textarea[ng-model],select[ng-model]');

                    if ($inputs.length > 0) {
                        $inputs.each(function() {
                            var $input = $(this);
                            scope.$watch(function() {
                                return $input.hasClass('ng-invalid') && $input.hasClass('ng-dirty');
                            }, function(isInvalid) {
                                $formGroup.toggleClass('has-error', isInvalid);
                            });
                        });
                    }
                });

                $timeout(
                    function() {
                        try{

                            element.find('.form-group').each(function() {
                                var $formGroup = $(this);
                                var $inputs = $formGroup.find('input[required], select[required], textarea[required]');

                                if ($inputs.length > 0) {
                                    $inputs.each(function() {
                                        var $input = $(this);
                                        if($input.prev().prop('tagName') && $input.prev().prop('tagName').toLowerCase() == 'label') {
//                                            console.log('>>>' + $input.attr('name'));
                                            $input.prev().append("<strong style='color:red'> * </strong>");
                                        }
                                        else if($input.parent().parent().children(0).prop('tagName') && $input.parent().parent().children(0).prop('tagName').toLowerCase() == 'label'){
//                                            console.log('===' + $input.attr('name'));
                                            $input.parent().parent().find('label').append("<strong style='color:red'> * </strong>");
                                        }
                                    });
                                }
                            });

                        }catch(e) {console.log(e);}
                    }
                );

            }
        };
    });

/* globals $ */
'use strict';

angular.module('stepApp')
    .directive('minbytes', function ($q) {
        function endsWith(suffix, str) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }

        function paddingSize(base64String) {
            if (endsWith('==', base64String)) {
                return 2;
            }
            if (endsWith('=', base64String)) {
                return 1;
            }
            return 0;
        }

        function numberOfBytes(base64String) {
            return base64String.length / 4 * 3 - paddingSize(base64String);
        }

        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) return;

                ngModel.$validators.minbytes = function (modelValue) {
                    return ngModel.$isEmpty(modelValue) || numberOfBytes(modelValue) >= attrs.minbytes;
                };
            }
        };
    });

/* globals $ */
'use strict';

angular.module('stepApp')
    .directive('stepAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });

/* globals $ */
'use strict';

angular.module('stepApp')
    .directive('stepAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });

'use strict';

angular.module('stepApp')
    .factory('AuditsService', function ($http) {
        return {
            findAll: function () {
                return $http.get('api/audits/').then(function (response) {
                    return response.data;
                });
            },
            findByDates: function (fromDate, toDate) {

                var formatDate =  function (dateToFormat) {
                    if (dateToFormat !== undefined && !angular.isString(dateToFormat)) {
                        return dateToFormat.getYear() + '-' + dateToFormat.getMonth() + '-' + dateToFormat.getDay();
                    }
                    return dateToFormat;
                };

                return $http.get('api/audits/', {params: {fromDate: formatDate(fromDate), toDate: formatDate(toDate)}}).then(function (response) {
                    return response.data;
                });
            }
        };
    });

'use strict';

angular.module('stepApp')
    .factory('LogsService', function ($resource) {
        return $resource('api/logs', {}, {
            'findAll': { method: 'GET', isArray: true},
            'changeLevel': { method: 'PUT'}
        });
    });

'use strict';

angular.module('stepApp')
    .factory('ConfigurationService', function ($rootScope, $filter, $http) {
        return {
            get: function() {
                return $http.get('configprops').then(function (response) {
                    var properties = [];
                    angular.forEach(response.data, function (data) {
                        properties.push(data);
                    });
                    var orderBy = $filter('orderBy');
                    return orderBy(properties, 'prefix');
                });
            }
        };
    });

'use strict';

angular.module('stepApp')
    .factory('MonitoringService', function ($rootScope, $http) {
        return {
            getMetrics: function () {
                return $http.get('metrics/metrics').then(function (response) {
                    return response.data;
                });
            },

            checkHealth: function () {
                return $http.get('health').then(function (response) {
                    return response.data;
                });
            },

            threadDump: function () {
                return $http.get('dump').then(function (response) {
                    return response.data;
                });
            }
        };
    });
