package gov.step.app.service.constnt;

/**
 * Created by yzaman on 1/16/16.
 */
public interface HRMManagementConstant
{
    public final String ROLE_DTE_EMPLOYEE_NAME  = "ROLE_DTE_EMPLOYEE";
    public final String ROLE_HRM_USER_NAME      = "ROLE_HRM_USER";
    public final String ROLE_INST_EMPLOYEE_NAME  = "ROLE_INSTEMP";

    public final String EMP_ORGANIZATION_TYPE_ORG  = "Organization";
    public final String EMP_ORGANIZATION_TYPE_INST  = "Institute";
    public final String EMP_TRANSFER_APPROVAL_APPROVED  = "APPROVED";
    public final String EMP_DATA_UPDATE_APPROVAL_APPROVED  = "APPROVED";

    public final String INST_EMPLOYEE_DEFAULT_PASSWORD  = "123456";

    public final long TRANSFER_INST_TO_ORG_LOG_STATUS   = 11;
    public final long TRANSFER_ORG_TO_INST_LOG_STATUS   = 12;
    public final long TRANSFER_INST_TO_INST_LOG_STATUS  = 13;
    public final long TRANSFER_ORG_TO_ORG_LOG_STATUS    = 14;
    public final long PROMOTION_EMPLOYEE_LOG_STATUS     = 15;

    public final String APPROVAL_LOG_STATUS_ACCEPT   = "accept";
    public final String APPROVAL_LOG_STATUS_REJECT   = "reject";
    public final String EMPLOYEE_DEACTIVATE          = "deactive";
    public final String EMPLOYEE_ACTIVATE            = "activate";
    public final String TRANSFER_TYPE_WORK_PLACE     = "WORK_PLACE";
    public final String TRANSFER_TYPE_DEPUTATION     = "DEPUTATION";
    public final String TRANSFER_TYPE_LIEN  = "LIEN";
    public final String TRANSFER_APPL_ACTION_COMMENTS     = "TRANSFER_APPROVAL";
    public final long TRANSFER_APPLICATION_APPROVE = 21;

    public final long HR_EMPLOYEE_DEACTIVATED        = 5;
    public final long HR_EMPLOYEE_ACTIVATED          = 4;

    public final long APPROVAL_STATUS_LOCKED = 0;
    public final long APPROVAL_STATUS_FREE   = 1;
    public final long APPROVAL_STATUS_APPROVED = 2;
    public final long APPROVAL_STATUS_REJECTED = 3;
    public final long APPROVAL_STATUS_EMPINIT = 5;
    public final long APPROVAL_STATUS_ADMIN_ENTRY = 6;

    public final long APPROVAL_LOG_STATUS_ACTIVE = 1;
    public final long APPROVAL_LOG_STATUS_APPROVED   = 2;
    public final long APPROVAL_LOG_STATUS_ROLLBACKED = 3;
    public final long APPROVAL_LOG_STATUS_REJECTED   = 3;

    public final long APPROVAL_DEPT_HEAD_PENDING = 11;
    public final long APPROVAL_DEPT_HEAD_REJECTED = 22;
    public final long APPROVAL_INST_HEAD_PENDING = 33;
    public final long APPROVAL_INST_HEAD_REJECTED = 44;
    public final long APPROVAL_SYSTEM_ADMIN_PENDING = 55;
    public final long APPROVAL_SYSTEM_ADMIN_REJECTED = 66;

    public final long RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING = 16;
    public final long RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED = 17;
    public final long RELEASE_DOC_INST_HEAD_APPROVAL_PENDING = 18;
    public final long RELEASE_DOC_INST_HEAD_APPROVAL_REJECTED = 19;

    public final int MINIMUM_IMG_NAME_LENGTH        = 4;

    public final String EMPLOYEE_PHOTO_FILE_DIR     = "/backup/hrm/employee/docs/";
    public final String EDUCATION_CERT_FILE_DIR     = "data/hrm/education/cert/";
    public final String EDUCATION_TRANS_FILE_DIR    = "data/hrm/education/trans/";

    public final String TRAINING_CERT_FILE_DIR      = "data/hrm/training/cert/";
    public final String TRAINING_GOORDER_FILE_DIR   = "data/hrm/training/goorder/";

    public final String AWARD_CERT_FILE_DIR         = "data/hrm/award/cert/";
    public final String AWARD_GOORDER_FILE_DIR      = "data/hrm/award/goorder/";

    public final String PUBLICATION_FILE_DIR        = "data/hrm/publication/";
    public final String TRANSFER_FILE_DIR           = "data/hrm/transfer/";
    public final String PROMOTION_FILE_DIR          = "data/hrm/promotion/";
    public final String ADV_INCREMENT_FILE_DIR      = "data/hrm/increment/";
    public final String ACTING_DUTY_FILE_DIR        = "data/hrm/actingduty/";
    public final String FOREIGN_TOUR_FILE_DIR       = "data/hrm/foreigntour/";
    public final String SERV_HISTORY_TOUR_FILE_DIR  = "data/hrm/servhistory/";

    public final String INST_EMP_FILE_DIR = "/backup/teacher_info/";
    public final String INST_EMP_MPO_FILE_DIR = "/backup/mpo/";
}
