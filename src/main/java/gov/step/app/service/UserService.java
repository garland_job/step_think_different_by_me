package gov.step.app.service;

import gov.step.app.domain.Authority;
import gov.step.app.domain.District;
import gov.step.app.domain.User;
import gov.step.app.repository.AuthorityRepository;
import gov.step.app.repository.PersistentTokenRepository;
import gov.step.app.repository.UserRepository;
import gov.step.app.repository.search.UserSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.security.Principal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserSearchRepository userSearchRepository;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private MailService mailService;

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                userSearchRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
        return Optional.empty();
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);

        return userRepository.findOneByResetKey(key)
            .filter(user -> {
                ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
                return user.getResetDate().isAfter(oneDayAgo);
            })
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                userRepository.save(user);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(ZonedDateTime.now());
                userRepository.save(user);
                return user;
            });
    }


    public User createUserInformation(String login, String password, String firstName, String lastName, String email,
                                      String langKey) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne("ROLE_USER");
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        //newUser.setActivated(false);
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createHrmCustomUserInformation(String login, String password, String firstName, String lastName, String email,
                                            String langKey,String userRole) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(userRole);

        if(authority==null){
            authority=new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createOrganizationCustomUserInformation(String login, String password, String firstName, String lastName, String email,
                                               String langKey,String userRole) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(userRole);

        if(authority==null){
            authority=new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createCustomUserInformation(String login, String password, String firstName, String lastName, String email,
                                            String langKey,String userRole) {
        return createCustomUserInformation(login,  password,  firstName,  lastName,  email, langKey, userRole, false,null);
    }
    public User createCustomUserInformation(String login, String password, String firstName, String lastName, String email,
                                            String langKey,String userRole,boolean activated) {
        return createCustomUserInformation(login,  password,  firstName,  lastName,  email, langKey, userRole, activated,null);
    }
    public User createCustomUserInformation(String login, String password, String firstName, String lastName, String email,
                                      String langKey,String userRole,boolean activated,District district) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(userRole);

        if(authority==null){
            authority=new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(activated);
        newUser.setDistrict(district);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }


    //for assigning role to existing emplo0yee.. created for assigning on the already created entity in hremployeeinfo

    public User assignRole(User user, String userRole) {
        Authority authority = authorityRepository.findOne(userRole);
        if (authority == null) {
            authority = new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        Set<Authority> authorities = user.getAuthorities();
        authorities.add(authority);
        //User newUser = new User();
        user = getUserWithAuthorities(user.getId());
        user.setAuthorities(authorities);
        userRepository.save(user);
        userSearchRepository.save(user);
        return user;
    }

    public User assignRoleWithExistingRole(User user, String userRole) {
        Authority authority = authorityRepository.findOne(userRole);
        if (authority == null) {
            authority = new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        user = getUserWithAuthorities(user.getId());
        Set<Authority> authorities = user.getAuthorities();
        authorities.add(authority);
        //User newUser = new User();
        user.setAuthorities(authorities);
        System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> authority "+authority.toString());
        System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> setAuthorities "+user.getAuthorities().toString());
        userRepository.save(user);
        userSearchRepository.save(user);
        return user;
    }

    public User updateAssignedRole(User user, String userRole) {
        Authority authority = authorityRepository.findOne(userRole);
        if (authority == null) {
            authority = new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        user = getUserWithAuthorities(user.getId());
        Set<Authority> authorities = user.getAuthorities();
        authorities.add(authority);
        //User newUser = new User();
        user.setAuthorities(authorities);
        userRepository.save(user);
        userSearchRepository.save(user);
        return user;
    }

    public User updateAssignedRoleWithInactiveChangeRole(User user, String userCurrentRole, String userPreviousRole, Boolean userRoleStatus) {
        Authority authority = authorityRepository.findOne(userCurrentRole);
        if (authority == null) {
            authority = new Authority();
            authority.setName(userCurrentRole);
            authorityRepository.save(authority);
        }
        user = getUserWithAuthorities(user.getId());
        Set<Authority> authorities = user.getAuthorities();
        if(userRoleStatus == false){
            Set<Authority> resultAuths = new HashSet<>();
            for (Iterator<Authority> iterator = authorities.iterator(); iterator.hasNext();) {
                Authority auths =  iterator.next();
                if (auths.getName().equalsIgnoreCase(userPreviousRole)) {
                    iterator.remove();
                } else {
                    resultAuths.add(iterator.next());
                }
            }
            user.setAuthorities(resultAuths);
            userRepository.save(user);
            userSearchRepository.save(user);
        } else if (userRoleStatus == true){
            Set<Authority> resultAuths = new HashSet<>();
            for (Iterator<Authority> iterator = authorities.iterator(); iterator.hasNext();) {
                Authority auths =  iterator.next();
                if (auths.getName().equalsIgnoreCase(userPreviousRole)) {
                    iterator.remove();
                } else {
                    resultAuths.add(iterator.next());
                }
            }
            resultAuths.add(authority);
            user.setAuthorities(resultAuths);
            userRepository.save(user);
            userSearchRepository.save(user);
        }
        /*authorities.add(authority);
        //User newUser = new User();
        user.setAuthorities(authorities);
        userRepository.save(user);
        userSearchRepository.save(user);*/
        return user;
    }

    public void updateUserInformation(String firstName, String lastName, String email, String langKey) {
        userRepository.findOneById(SecurityUtils.getCurrentUserId()).ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setEmail(email);
            u.setLangKey(langKey);
            userRepository.save(u);
            userSearchRepository.save(u);
            log.debug("Changed Information for User: {}", u);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneById(SecurityUtils.getCurrentUserId()).ifPresent(u -> {
            String encryptedPassword = passwordEncoder.encode(password);
            u.setPassword(encryptedPassword);
            userRepository.save(u);
            mailService.sendEmail(u.getEmail(),"Password change","Your password has changed. Check it now.",false,false);
            log.debug("Changed password for User: {}", u);
        });
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            u.getAuthorities().size();
            return u;
        });
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        User user = userRepository.findOne(id);
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        User user = userRepository.findOneById(SecurityUtils.getCurrentUserId()).get();
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }


    public User createCustomUserInformationByUserModule(String login, String password, String firstName, String lastName, String email,
                                                        String langKey,String userRole,boolean activated,Long userRoleId) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(userRole);

        if(authority==null){
            authority=new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(activated);
        //newUser.setDistrict(district);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        //newUser.setRoleId(userRoleId);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p/>
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     * </p>
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).stream().forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p/>
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        ZonedDateTime now = ZonedDateTime.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
            userSearchRepository.delete(user);
        }
    }

    public User createCustomUserInformationByRights(String login, String password, String firstName, String lastName, String email,
                                                    String langKey,Set<Authority> userRoles,boolean activated,Long userRoleId) {

        User newUser = new User();
        /*Authority authority = authorityRepository.findOne(userRole);

        if(authority==null){
            authority=new Authority();
            authority.setName(userRole);
            authorityRepository.save(authority);
        }*/
        //Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(activated);
        //newUser.setDistrict(district);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        /*for(String authoritys: userRole){
            Authority authority = authorityRepository.findOne(authoritys);
            authorities.add(authority);
        }*/
        //authorities.add(authority);
        newUser.setAuthorities(userRoles);
        //newUser.setRoleId(userRoleId);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public void changePasswordByUserId(String password, User user) {

        User u = userRepository.findOneByUserId(user.getId());
        if(u != null){
            try {
                String encryptedPassword = passwordEncoder.encode(password);
                u.setPassword(encryptedPassword);
                userRepository.save(u);
                log.debug("Changed password for User: {}", u);
                System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Changed password for User:" + u);
            } catch (Exception e){
                System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+e);
            }
        }
    }
}
