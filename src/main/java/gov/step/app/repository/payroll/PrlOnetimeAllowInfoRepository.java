package gov.step.app.repository.payroll;

import gov.step.app.domain.payroll.PrlOnetimeAllowInfo;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PrlOnetimeAllowInfo entity.
 */
public interface PrlOnetimeAllowInfoRepository extends JpaRepository<PrlOnetimeAllowInfo,Long> {

    @Query("select modelInfo from PrlOnetimeAllowInfo modelInfo where activeStatus =:stat ")
    List<PrlOnetimeAllowInfo> findAllByStatus(@Param("stat") boolean stat);
}
