package gov.step.app.repository.payroll;

import gov.step.app.domain.payroll.PrlSalaryAllowDeducInfo;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PrlSalaryAllowDeducInfo entity.
 */
public interface PrlSalaryAllowDeducInfoRepository extends JpaRepository<PrlSalaryAllowDeducInfo,Long> {
    @Query("select modelInfo from PrlSalaryAllowDeducInfo modelInfo where salaryStructureInfo.id = :strId and allowDeducType = 'Allowance' ")
    List<PrlSalaryAllowDeducInfo> finAlldBySalaryStructureInfo(@Param("strId") Long salaryStructureInfo);

}
