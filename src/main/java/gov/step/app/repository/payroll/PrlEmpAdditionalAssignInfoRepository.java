package gov.step.app.repository.payroll;

import gov.step.app.domain.payroll.PrlEmpAdditionalAssignInfo;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PrlEmpAdditionalAssignInfo entity.
 */
public interface PrlEmpAdditionalAssignInfoRepository extends JpaRepository<PrlEmpAdditionalAssignInfo,Long>
{
    @Query("select modelInfo from PrlEmpAdditionalAssignInfo modelInfo where employeeInfo.id = :empid AND allowanceInfo.id = :allowId")
    List<PrlEmpAdditionalAssignInfo> findByEmployeeAndAllowance(@Param("empid") long empid, @Param("allowId") long allowId);


}
