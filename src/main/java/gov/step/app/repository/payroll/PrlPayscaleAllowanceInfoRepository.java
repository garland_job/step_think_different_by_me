package gov.step.app.repository.payroll;

import gov.step.app.domain.payroll.PrlPayscaleAllowanceInfo;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PrlPayscaleAllowanceInfo entity.
 */
public interface PrlPayscaleAllowanceInfoRepository extends JpaRepository<PrlPayscaleAllowanceInfo,Long> {

    @Query("select modelInfo from PrlPayscaleAllowanceInfo modelInfo where modelInfo.payscaleInfo.id = :payScaleId and modelInfo.allowanceInfo.name=:allowanceName")
    PrlPayscaleAllowanceInfo findByPayScaleIdAndAllowanceName(@Param("payScaleId") Long payScaleId, @Param("allowanceName") String allowanceName);
}
