package gov.step.app.repository.payroll;

import gov.step.app.domain.payroll.PrlGeneratedSalaryInfo;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PrlGeneratedSalaryInfo entity.
 */
public interface PrlGeneratedSalaryInfoRepository extends JpaRepository<PrlGeneratedSalaryInfo,Long>
{
    @Query("select modelInfo from PrlGeneratedSalaryInfo modelInfo where lower(modelInfo.monthName) = lower(:month) AND modelInfo.yearName =:year AND lower(modelInfo.salaryType) = lower(:salType)")
    List<PrlGeneratedSalaryInfo> findOneByMonthYearType(@Param("month") String month, @Param("year") Long year, @Param("salType") String salType);

    @Query("select modelInfo from PrlGeneratedSalaryInfo modelInfo where lower(modelInfo.monthName) = lower(:month) AND modelInfo.yearName =:year AND lower(modelInfo.salaryType) = lower(:salType) AND modelInfo.onetimeAllowInfo.id = :otaid")
    List<PrlGeneratedSalaryInfo> findOneByMonthYearTypeAllowance(@Param("month") String month, @Param("year") Long year, @Param("salType") String salType, @Param("otaid") Long otaid);

    @Query("select modelInfo from PrlGeneratedSalaryInfo modelInfo where lower(modelInfo.monthName) = lower(:month) AND modelInfo.yearName =:year AND lower(modelInfo.salaryType) = lower(:salType) AND modelInfo.onetimeAllowInfo.id = :otaid AND lower(modelInfo.organizationType) = lower(:orgType) AND modelInfo.orgInstId = :orgInstId")
    List<PrlGeneratedSalaryInfo> findOneByMonthYearTypeAllowanceOrgTypeRefId(@Param("month") String month, @Param("year") Long year, @Param("salType") String salType, @Param("otaid") Long otaid, @Param("orgType") String orgType,  @Param("orgInstId") Long orgInstId);

    @Query("select modelInfo from PrlGeneratedSalaryInfo modelInfo where lower(modelInfo.monthName) = lower(:month) AND modelInfo.yearName =:year AND lower(modelInfo.salaryType) = lower(:salType) AND lower(modelInfo.organizationType) = lower(:orgType) AND modelInfo.orgInstId = :orgInstId")
    List<PrlGeneratedSalaryInfo> findOneByMonthYearTypeOrgTypeRefId(@Param("month") String month, @Param("year") Long year, @Param("salType") String salType, @Param("orgType") String orgType,  @Param("orgInstId") Long orgInstId);

    @Query("select modelInfo from PrlGeneratedSalaryInfo modelInfo order by createDate desc")
    List<PrlGeneratedSalaryInfo> getLastInsertedData();

    @Query("select modelInfo from PrlGeneratedSalaryInfo modelInfo WHERE lower(modelInfo.organizationType) = lower(:orgType) AND modelInfo.orgInstId = :orgInstId order by createDate desc")
    List<PrlGeneratedSalaryInfo> getLastInsertedDataByOrgOrInst(@Param("orgType") String orgType,  @Param("orgInstId") Long orgInstId);

}
