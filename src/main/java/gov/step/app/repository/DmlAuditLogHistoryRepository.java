package gov.step.app.repository;

import gov.step.app.domain.DmlAuditLogHistory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the DmlAuditLogHistory entity.
 */
public interface DmlAuditLogHistoryRepository extends JpaRepository<DmlAuditLogHistory,Long> {

}
