package gov.step.app.repository;

import gov.step.app.domain.PgmsAppFamilyAttach;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the PgmsAppFamilyAttach entity.
 */
public interface PgmsAppFamilyAttachRepository extends JpaRepository<PgmsAppFamilyAttach,Long>
{
    List<PgmsAppFamilyAttach> findAllByAppFamilyPenId(long appFamilyPenId);
}
