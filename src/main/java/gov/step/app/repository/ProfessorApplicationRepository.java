package gov.step.app.repository;

import gov.step.app.domain.APScaleApplication;
import gov.step.app.domain.MpoApplication;
import gov.step.app.domain.ProfessorApplication;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the ProfessorApplication entity.
 */
public interface ProfessorApplicationRepository extends JpaRepository<ProfessorApplication,Long> {


    @Query("select professorApplication from ProfessorApplication professorApplication where professorApplication.instEmployee.code = :code and professorApplication.status > 1")
    ProfessorApplication findByInstEmployeeCode(@org.springframework.data.repository.query.Param("code") String code);

    @Query("select profApplication from ProfessorApplication profApplication where profApplication.status = :status and (profApplication.instEmployee.payScale is not null and profApplication.instEmployee.principleAppStatus =:appStatus) and profApplication.instEmployee.mpoActive=true")
    Page<ProfessorApplication> findPayScaleAssignedList(Pageable var1, @Param("status") int code, @Param("appStatus") Integer appStatus);

    @Query("select profApplication from ProfessorApplication profApplication where profApplication.status = :status and profApplication.instEmployee.principleAppStatus =:appStatus and profApplication.instEmployee.payScale is not null")
    Page<ProfessorApplication> findPayScaleNotAssignedList(Pageable var1, @Param("status") int code,@Param("appStatus") Integer appStatus);
}
