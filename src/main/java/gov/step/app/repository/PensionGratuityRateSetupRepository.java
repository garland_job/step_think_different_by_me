package gov.step.app.repository;

import gov.step.app.domain.PensionGratuityRateSetup;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PensionGratuityRateSetup entity.
 */
public interface PensionGratuityRateSetupRepository extends JpaRepository<PensionGratuityRateSetup,Long> {

    /*
    @Query("select modelInfo from PensionGratuityRateSetup modelInfo where modelInfo.setupVersion = :penGrVersion")
    List<PensionGratuityRateSetup> findAllBySetupVersion (@Param("penGrVersion") String setupVersion);
    */

    @Query("select modelInfo from PensionGratuityRateSetup modelInfo where modelInfo.setupVersion = :penGrVersion")
    PensionGratuityRateSetup findAllBySetupVersion (@Param("penGrVersion") String setupVersion);

    @Query("select max(modelInfo.setupVersion) from PensionGratuityRateSetup modelInfo where modelInfo.setupType =:pSetupType")
    Long findMaxSetupVersionBySetupType (@Param("pSetupType") String pSetupType);

    @Query("select distinct modelInfo.setupVersion from PensionGratuityRateSetup modelInfo")
    List<Long> findTotalVersion();

    List<PensionGratuityRateSetup> findBySetupVersion(Long setupVersion);
    Page<PensionGratuityRateSetup> findBySetupVersionOrderByWorkingYear(Long setupVersion, Pageable pageable);
    Page<PensionGratuityRateSetup> findBySetupVersionAndSetupTypeOrderByWorkingYear(Long setupVersion,String setupType,Pageable pageable);


    @Query("UPDATE PensionGratuityRateSetup modelInfo SET modelInfo.activeStatus = false WHERE modelInfo.setupVersion NOT IN(:pSetupVersion)")
    Boolean deactiveAllSetupVersion(@Param("pSetupVersion") Long pSetupVersion);

}
