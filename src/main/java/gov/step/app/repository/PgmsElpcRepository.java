package gov.step.app.repository;

import gov.step.app.domain.PgmsElpc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PgmsElpc entity.
 */
public interface PgmsElpcRepository extends JpaRepository<PgmsElpc,Long> {

    @Query("select modelInfo from PgmsElpc modelInfo where modelInfo.hrEmployeeInfo.id = :empId")
    List<PgmsElpc> findAllByHrEmpId(@Param("empId") Long employeeId);

    @Query("select modelInfo from PgmsElpc modelInfo where modelInfo.hrEmployeeInfo.id = :empId and modelInfo.approveStatus= :approveStatus")
    PgmsElpc findByHrEmployeeIdAndApproveStatus(@Param("empId") Long employeeId, @Param("approveStatus") Long approveStatus);

}
