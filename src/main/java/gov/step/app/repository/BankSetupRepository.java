package gov.step.app.repository;

import gov.step.app.domain.BankSetup;

import gov.step.app.domain.CmsCurriculum;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the BankSetup entity.
 */
public interface BankSetupRepository extends JpaRepository<BankSetup,Long> {

    @Query("select bankSetup from BankSetup bankSetup where bankSetup.name = :name")
    Optional<BankSetup> findOneByName(@Param("name") String name);

}
