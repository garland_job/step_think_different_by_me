package gov.step.app.repository;

import gov.step.app.domain.InstEmpSalaryAdj;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InstEmpSalaryAdj entity.
 */
public interface InstEmpSalaryAdjRepository extends JpaRepository<InstEmpSalaryAdj,Long> {

}
