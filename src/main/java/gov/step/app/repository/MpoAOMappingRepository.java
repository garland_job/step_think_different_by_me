package gov.step.app.repository;

import gov.step.app.domain.CmsTrade;
import gov.step.app.domain.MpoAOMapping;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the MpoAOMapping entity.
 */
public interface MpoAOMappingRepository extends JpaRepository<MpoAOMapping,Long> {

    @Query("select modelInfo from MpoAOMapping modelInfo where modelInfo.instLevel.id = :instLevelId AND modelInfo.division.id = :divisionId")
    List<MpoAOMapping> findAllByInstLevelAndDivision(@Param("instLevelId") Long instLevelId,@Param("divisionId") Long divisionId);

    @Query("select modelInfo from MpoAOMapping modelInfo where modelInfo.instLevel.id = :instLevelId")
    List<MpoAOMapping> findAllByInstLevel(@Param("instLevelId") Long instLevelId);

}
