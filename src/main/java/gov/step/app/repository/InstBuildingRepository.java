package gov.step.app.repository;

import gov.step.app.domain.InstBuilding;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the InstBuilding entity.
 */
public interface InstBuildingRepository extends JpaRepository<InstBuilding,Long> {


    @Query("select modelInfo from InstBuilding  modelInfo where modelInfo.instInfraInfo.institute.id =:instituteId ")
    List<InstBuilding> findByInsittuteId(@Param("instituteId") Long instituteId);

    @Query("select modelInfo from InstBuilding modelInfo where modelInfo.instBuildingTemp.id =:buildingTempInfoId")
    InstBuilding findByBuildingTempInfoId(@Param("buildingTempInfoId") Long buildingTempInfoId);


}
