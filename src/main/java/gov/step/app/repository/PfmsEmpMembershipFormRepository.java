package gov.step.app.repository;

import gov.step.app.domain.PfmsEmpMembershipForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the PfmsEmpMembershipForm entity.
 */
public interface PfmsEmpMembershipFormRepository extends JpaRepository<PfmsEmpMembershipForm,Long> {
    @Query("select modelInfo from PfmsEmpMembershipForm modelInfo where modelInfo.employeeInfo.id = :employeeInfoId and modelInfo.activeStatus = true")
    List<PfmsEmpMembershipForm> getPfmsEmpMembershipFormListByEmployee(@Param("employeeInfoId") long employeeInfoId);

    @Query("select modelInfo from PfmsEmpMembershipForm modelInfo where lower(modelInfo.accountNo) = :accountNo ")
    Optional<PfmsEmpMembershipForm> findOneByTypeName(@Param("accountNo") String accountNo);

    Page<PfmsEmpMembershipForm> findAllByActiveStatus(Pageable pageable, boolean activeStatus);

    @Query("select modelInfo from PfmsEmpMembershipForm modelInfo where modelInfo.employeeInfo.id = :employeeInfoId and modelInfo.activeStatus = true")
    PfmsEmpMembershipForm getPfmsEmpMembershipFormByEmployee(@Param("employeeInfoId") Long employeeInfoId);
}
