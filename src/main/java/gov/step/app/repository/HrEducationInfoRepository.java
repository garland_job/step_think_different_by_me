package gov.step.app.repository;

import gov.step.app.domain.HrEducationInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the HrEducationInfo entity.
 */
public interface HrEducationInfoRepository extends JpaRepository<HrEducationInfo,Long>
{
    @Query("select modelInfo from HrEducationInfo modelInfo where modelInfo.employeeInfo.user.login = ?#{principal.username} AND modelInfo.employeeInfo.activeAccount=true ")
    List<HrEducationInfo> findAllByEmployeeIsCurrentUser();

    List<HrEducationInfo> findAllByLogStatusAndActiveStatus(Long logStatus, boolean activeStatus);

    @Query("select modelInfo from HrEducationInfo modelInfo where logStatus=:logStatus order by updateDate asc")
    List<HrEducationInfo> findAllByLogStatus(@Param("logStatus") Long logStatus);

    @Query("select modelInfo from HrEducationInfo modelInfo where employeeInfo.departmentInfo.id IN :deptIdList AND logStatus=:lgStatus order by updateDate asc")
    List<HrEducationInfo> findAllModelsByDepartmentIdsAndPendingStatus(@Param("deptIdList") List<Long> deptIdList, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEducationInfo modelInfo where employeeInfo.organizationType=:orgType AND employeeInfo.institute.id=:instId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEducationInfo> findAllModelsByInstituteAndPendingStatus(@Param("orgType") String orgType, @Param("instId") Long instId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEducationInfo modelInfo where logStatus IN :logStatusList order by createDate asc")
    List<HrEducationInfo> findAllModelByLogStatuses(@Param("logStatusList") List<Long> logStatusList);
}
