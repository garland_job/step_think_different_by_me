package gov.step.app.repository;

import gov.step.app.domain.GenderInfo;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GenderInfo entity.
 */
public interface GenderInfoRepository extends JpaRepository<GenderInfo,Long> {

}
