package gov.step.app.repository;

import gov.step.app.domain.PgmsAppRetirmntCalculation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the PgmsAppRetirmntCalculation entity.
 */
public interface PgmsAppRetirmntCalculationRepository extends JpaRepository<PgmsAppRetirmntCalculation,Long> {

    @Query("select modelInfo from PgmsAppRetirmntCalculation modelInfo where modelInfo.appRetirmntPenInfo.id = :pensionId")
    PgmsAppRetirmntCalculation findAllByAppRetirmntPenId(@Param("pensionId") long appRetirmntPenInfo);

    @Modifying
    @Transactional
    @Query("delete from PgmsAppRetirmntCalculation modelInfo where modelInfo.appRetirmntPenInfo.id = :penId")
    void deleteByAppRetirmntPenId(@Param("penId") Long appRetirmntPenInfo);

}
