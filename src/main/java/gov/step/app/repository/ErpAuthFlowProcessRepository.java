package gov.step.app.repository;

import gov.step.app.domain.ErpAuthFlowProcess;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the ErpAuthFlowProcess entity.
 */
public interface ErpAuthFlowProcessRepository extends JpaRepository<ErpAuthFlowProcess,Long> {
    @Query("select modelInfo from ErpAuthFlowProcess modelInfo where modelInfo.refDomainId = :refDomainId and modelInfo.activeStatus = true and modelInfo.refDomainName = :refDomainName" )
    List<ErpAuthFlowProcess> findAllErpAuthFlowProcessByDomain(@Param("refDomainId") long refDomainId, @Param("refDomainName") String refDomainName);
}
