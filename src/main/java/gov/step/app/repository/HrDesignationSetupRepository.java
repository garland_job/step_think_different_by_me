package gov.step.app.repository;

import gov.step.app.domain.HrDesignationSetup;
import gov.step.app.domain.enumeration.designationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the HrDesignationSetup entity.
 */
public interface HrDesignationSetupRepository extends JpaRepository<HrDesignationSetup,Long>
{
    @Query("select modelInfo from HrDesignationSetup modelInfo where activeStatus =:activeStatus")
    Page<HrDesignationSetup> findAllByActiveStatus(Pageable pageable, @Param("activeStatus") boolean activeStatus);

    @Query("select modelInfo from HrDesignationSetup modelInfo where activeStatus =:activeStatus")
    List<HrDesignationSetup> findAllByActiveStatus(@Param("activeStatus") boolean activeStatus);

    @Query("select modelInfo from HrDesignationSetup modelInfo where designationInfo.id =:designationInfo")
    List<HrDesignationSetup> findAllByDesignationInfoId(@Param("designationInfo") Long designationInfo);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.organizationInfo.id =:workDtlId AND modelInfo.designationInfo.id = :desigId ")
    List<HrDesignationSetup> findOneByDesignationHeadAndWorkArea(@Param("workDtlId") Long workDtlId, @Param("desigId") Long desigId);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.institute.id =:instid AND modelInfo.designationInfo.id = :desigId ")
    List<HrDesignationSetup> findOneByDesignationHeadAndInstitute(@Param("instid") Long instid, @Param("desigId") Long desigId);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.desigType =:desigType AND modelInfo.instLevel.id =:levelId AND modelInfo.instCategory.id = :catId AND modelInfo.designationInfo.id = :desigid ")
    List<HrDesignationSetup> findOneByInstLevelAndInstCatAndDesigLevel(@Param("desigType") designationType desigType, @Param("levelId") Long instid, @Param("catId") Long catId, @Param("desigid") Long desigId);

    @Query("select modelInfo from HrDesignationSetup modelInfo where desigType =:desigType AND activeStatus =:activeStatus")
    List<HrDesignationSetup> findAllByDesigType(@Param("desigType") designationType desigType, @Param("activeStatus") boolean activeStatus);

    @Query("select modelInfo from HrDesignationSetup modelInfo where desigType IN ('Teacher','HRM') AND activeStatus =:activeStatus")
    List<HrDesignationSetup> findAllByDesigTypes(@Param("activeStatus") boolean activeStatus);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.desigType =:desigType AND modelInfo.activeStatus =:activeStatus AND modelInfo.designationInfo.designationName=:designationName")
    List<HrDesignationSetup> findByDesigTypeAndDesignationName(@Param("desigType") designationType desigType,@Param("activeStatus") Boolean activeStatus,@Param("designationName") String designationName);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.instLevel.id =:instLevelId")
    List<HrDesignationSetup> findAllByInstituteLevelId(@Param("instLevelId") Long instLevelId);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.organizationInfo.id =:orgId")
    List<HrDesignationSetup> findAllByOrganizationDetailsId(@Param("orgId") Long orgId);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.instCategory.id =:instCatId AND modelInfo.instLevel.id =:instLevelId AND modelInfo.designationInfo.id =:desigId")
    List<HrDesignationSetup> findAllByInstituteCategoryAndLevelWithUniqueDesig(@Param("instCatId") Long instCatId,@Param("instLevelId")  Long instLevelId,@Param("desigId") Long desigId);

    @Query("select modelInfo from HrDesignationSetup modelInfo where modelInfo.organizationInfo.id =:orgId AND modelInfo.designationInfo.id =:desigId")
    List<HrDesignationSetup> findAllByOrganizationDetailsIdWithUniqueDesig(@Param("orgId") Long orgId,@Param("desigId")  Long desigId);
}
