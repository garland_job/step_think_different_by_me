package gov.step.app.repository;

import gov.step.app.domain.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the SisStudentReg entity.
 */
public interface SisStudentRegRepository extends JpaRepository<SisStudentReg,Long> {

     Page<SisStudentReg> findAllByOrderByIdDesc(Pageable pageable);

    @Query("select sisStudentReg from SisStudentReg sisStudentReg where sisStudentReg.applicationId = :applicationId")
    SisStudentReg findAllRegByAppId(@Param("applicationId") String applicationId);

    @Query("select instGenInfo from InstGenInfo instGenInfo where instGenInfo.code = :code")
    InstGenInfo findByGenInfo(@Param("code") String code);

    @Query("select cmsTrade from CmsTrade cmsTrade where cmsTrade.cmsCurriculum.id = :cmsCurriculumId")
    List<CmsTrade> findByTradeInfo(@Param("cmsCurriculumId") Long cmsCurriculumId);

    @Query("select iisCurriculumInfo from IisCurriculumInfo iisCurriculumInfo where iisCurriculumInfo.institute.id = :instituteId")
    List<IisCurriculumInfo> findByCurriculumInfo(@Param("instituteId") Long instituteId);
}
