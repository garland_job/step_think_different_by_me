package gov.step.app.repository;

import gov.step.app.domain.BloodGroup;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the BloodGroup entity.
 */
public interface BloodGroupRepository extends JpaRepository<BloodGroup,Long> {

}
