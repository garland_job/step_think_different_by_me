package gov.step.app.repository;

import gov.step.app.domain.AlmApplicationStatusLog;
import gov.step.app.domain.AlmEmpLeaveApplication;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the AlmEmpLeaveApplication entity.
 */
public interface AlmApplicationStatusLogRepository extends JpaRepository<AlmApplicationStatusLog,Long> {

}
