package gov.step.app.repository;

import gov.step.app.domain.JasperReportPermission;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the JasperReportPermission entity.
 */
public interface JasperReportPermissionRepository extends JpaRepository<JasperReportPermission,Long> {

}
