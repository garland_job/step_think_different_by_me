package gov.step.app.repository;

import gov.step.app.domain.HrEmployeeInfoLog;

import gov.step.app.service.constnt.HRMManagementConstant;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the HrEmployeeInfoLog entity.
 */
public interface HrEmployeeInfoLogRepository extends JpaRepository<HrEmployeeInfoLog,Long>
{
    HrEmployeeInfoLog findOneByIdAndLogStatus(Long id,Long logStatus);

    @Query("select modelInfo from HrEmployeeInfoLog modelInfo where parentId=:emplId AND logStatus IN :logStatusList order by createDate asc")
    List<HrEmployeeInfoLog> findAllByTransferFromLogStatus(@Param("emplId") Long emplId, @Param("logStatusList") List<Long> logStatusList);

    @Query("select modelInfo from HrEmployeeInfoLog modelInfo where parentId=:emplId order by createDate asc")
    List<HrEmployeeInfoLog> findAllChangeHistoryByEmployee(@Param("emplId") Long emplId);

    @Query("select DISTINCT modelInfo.employeeId from HrEmployeeInfoLog modelInfo where modelInfo.logStatus=:eleven OR modelInfo.logStatus =:twelve order by modelInfo.employeeId desc")
    List<String> findAllEmpInfoByLogStatus(@Param("eleven") Long eleven,@Param("twelve") Long twelve);
}
