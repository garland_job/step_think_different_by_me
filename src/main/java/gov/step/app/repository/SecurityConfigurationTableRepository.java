package gov.step.app.repository;

import gov.step.app.domain.SecurityConfigurationTable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the SecurityConfigurationTable entity.
 */
public interface SecurityConfigurationTableRepository extends JpaRepository<SecurityConfigurationTable,Long> {

}
