package gov.step.app.repository;

import gov.step.app.domain.HrEmpProfMemberInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the HrEmpProfMemberInfo entity.
 */
public interface HrEmpProfMemberInfoRepository extends JpaRepository<HrEmpProfMemberInfo,Long>
{
    @Query("select modelInfo from HrEmpProfMemberInfo modelInfo where modelInfo.employeeInfo.user.login = ?#{principal.username} AND modelInfo.employeeInfo.activeAccount=true")
    List<HrEmpProfMemberInfo> findAllByEmployeeIsCurrentUser();

    List<HrEmpProfMemberInfo> findAllByLogStatusAndActiveStatus(Long logStatus, boolean activeStatus);

    @Query("select modelInfo from HrEmpProfMemberInfo modelInfo where logStatus=:logStatus order by updateDate asc")
    List<HrEmpProfMemberInfo> findAllByLogStatus(@Param("logStatus") Long logStatus);

    @Query("select modelInfo from HrEmpProfMemberInfo modelInfo where employeeInfo.departmentInfo.id IN :deptIdList AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmpProfMemberInfo> findAllModelsByDepartmentIdsAndPendingStatus(@Param("deptIdList") List<Long> deptIdList, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmpProfMemberInfo modelInfo where employeeInfo.organizationType=:orgType AND employeeInfo.institute.id=:instId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmpProfMemberInfo> findAllModelsByInstituteAndPendingStatus(@Param("orgType") String orgType, @Param("instId") Long instId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmpProfMemberInfo modelInfo where logStatus IN :logStatusList order by createDate asc")
    List<HrEmpProfMemberInfo> findAllModelByLogStatuses(@Param("logStatusList") List<Long> logStatusList);
}
