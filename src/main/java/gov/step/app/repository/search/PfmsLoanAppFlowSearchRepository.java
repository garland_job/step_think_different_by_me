package gov.step.app.repository.search;

import gov.step.app.domain.PfmsLoanAppFlow;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PfmsLoanAppFlow entity.
 */
public interface PfmsLoanAppFlowSearchRepository extends ElasticsearchRepository<PfmsLoanAppFlow, Long> {
}
