package gov.step.app.repository.search;

import gov.step.app.domain.ErpAuthorityFlow;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ErpAuthorityFlow entity.
 */
public interface ErpAuthorityFlowSearchRepository extends ElasticsearchRepository<ErpAuthorityFlow, Long> {
}
