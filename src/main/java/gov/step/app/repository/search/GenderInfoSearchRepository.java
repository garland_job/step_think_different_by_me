package gov.step.app.repository.search;

import gov.step.app.domain.GenderInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the GenderInfo entity.
 */
public interface GenderInfoSearchRepository extends ElasticsearchRepository<GenderInfo, Long> {
}
