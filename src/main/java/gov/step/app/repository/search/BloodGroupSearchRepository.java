package gov.step.app.repository.search;

import gov.step.app.domain.BloodGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BloodGroup entity.
 */
public interface BloodGroupSearchRepository extends ElasticsearchRepository<BloodGroup, Long> {
}
