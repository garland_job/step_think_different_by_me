package gov.step.app.repository.search;

import gov.step.app.domain.SecurityConfigurationTable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SecurityConfigurationTable entity.
 */
public interface SecurityConfigurationTableSearchRepository extends ElasticsearchRepository<SecurityConfigurationTable, Long> {
}
