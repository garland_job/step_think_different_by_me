package gov.step.app.repository.search;

import gov.step.app.domain.ScholarShipSetUp;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ScholarShipSetUp entity.
 */
public interface ScholarShipSetUpSearchRepository extends ElasticsearchRepository<ScholarShipSetUp, Long> {
}
