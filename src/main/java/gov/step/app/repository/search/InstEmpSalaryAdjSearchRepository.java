package gov.step.app.repository.search;

import gov.step.app.domain.InstEmpSalaryAdj;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the InstEmpSalaryAdj entity.
 */
public interface InstEmpSalaryAdjSearchRepository extends ElasticsearchRepository<InstEmpSalaryAdj, Long> {
}
