package gov.step.app.repository.search;

import gov.step.app.domain.ErpAuthFlowProcess;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ErpAuthFlowProcess entity.
 */
public interface ErpAuthFlowProcessSearchRepository extends ElasticsearchRepository<ErpAuthFlowProcess, Long> {
}
