package gov.step.app.repository.search.payroll;

import gov.step.app.domain.payroll.PrlEmpAdditionalAssignInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PrlEmpAdditionalAssignInfo entity.
 */
public interface PrlEmpAdditionalAssignInfoSearchRepository extends ElasticsearchRepository<PrlEmpAdditionalAssignInfo, Long> {
}
