package gov.step.app.repository.search;

import gov.step.app.domain.MpoAOMapping;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MpoAOMapping entity.
 */
public interface MpoAOMappingSearchRepository extends ElasticsearchRepository<MpoAOMapping, Long> {
}
