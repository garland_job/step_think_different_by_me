package gov.step.app.repository.search;

import gov.step.app.domain.JasperReportPermission;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the JasperReportPermission entity.
 */
public interface JasperReportPermissionSearchRepository extends ElasticsearchRepository<JasperReportPermission, Long> {
}
