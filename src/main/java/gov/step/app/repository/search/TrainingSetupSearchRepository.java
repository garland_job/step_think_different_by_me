package gov.step.app.repository.search;

import gov.step.app.domain.TrainingSetup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TrainingSetup entity.
 */
public interface TrainingSetupSearchRepository extends ElasticsearchRepository<TrainingSetup, Long> {
}
