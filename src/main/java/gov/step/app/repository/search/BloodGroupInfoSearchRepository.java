package gov.step.app.repository.search;

import gov.step.app.domain.BloodGroupInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BloodGroupInfo entity.
 */
public interface BloodGroupInfoSearchRepository extends ElasticsearchRepository<BloodGroupInfo, Long> {
}
