package gov.step.app.repository.search;

import gov.step.app.domain.MaritalInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the MaritalInfo entity.
 */
public interface MaritalInfoSearchRepository extends ElasticsearchRepository<MaritalInfo, Long> {
}
