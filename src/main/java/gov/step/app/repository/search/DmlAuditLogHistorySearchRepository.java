package gov.step.app.repository.search;

import gov.step.app.domain.DmlAuditLogHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the DmlAuditLogHistory entity.
 */
public interface DmlAuditLogHistorySearchRepository extends ElasticsearchRepository<DmlAuditLogHistory, Long> {
}
