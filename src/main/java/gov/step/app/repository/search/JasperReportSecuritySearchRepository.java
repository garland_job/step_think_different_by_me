package gov.step.app.repository.search;

import gov.step.app.domain.JasperReportSecurity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the JasperReportSecurity entity.
 */
public interface JasperReportSecuritySearchRepository extends ElasticsearchRepository<JasperReportSecurity, Long> {
}
