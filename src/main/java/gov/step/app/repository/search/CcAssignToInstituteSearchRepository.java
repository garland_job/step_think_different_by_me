package gov.step.app.repository.search;

import gov.step.app.domain.CcAssignToInstitute;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CcAssignToInstitute entity.
 */
public interface CcAssignToInstituteSearchRepository extends ElasticsearchRepository<CcAssignToInstitute, Long> {
}
