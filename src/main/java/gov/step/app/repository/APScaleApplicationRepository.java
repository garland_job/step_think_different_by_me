package gov.step.app.repository;

import gov.step.app.domain.APScaleApplication;

import gov.step.app.domain.BEdApplication;
import gov.step.app.domain.TimeScaleApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the APScaleApplication entity.
 */
public interface APScaleApplicationRepository extends JpaRepository<APScaleApplication,Long> {

    @Query("select aPScaleApplication from APScaleApplication aPScaleApplication where aPScaleApplication.instEmployee.code = :code and aPScaleApplication.status > 1")
    APScaleApplication findByInstEmployeeCode(@Param("code") String code);

    @Query("select aPScaleApplication from APScaleApplication aPScaleApplication where aPScaleApplication.status = :status and (aPScaleApplication.instEmployee.payScale is not null and aPScaleApplication.instEmployee.aPAppStatus =:appStatus) and aPScaleApplication.instEmployee.mpoActive=true")
    Page<APScaleApplication> findPayScaleAssignedList(Pageable var1, @Param("status") int code, @Param("appStatus") Integer appStatus);

    @Query("select aPScaleApplication from APScaleApplication aPScaleApplication where aPScaleApplication.status = :status and aPScaleApplication.instEmployee.aPAppStatus =:appStatus and aPScaleApplication.instEmployee.payScale is not null")
    Page<APScaleApplication> findPayScaleNotAssignedList(Pageable var1, @Param("status") int code,@Param("appStatus") Integer appStatus);

}
