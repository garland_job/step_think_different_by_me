package gov.step.app.repository;

import gov.step.app.domain.PfmsLoanAppFlow;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PfmsLoanAppFlow entity.
 */
public interface PfmsLoanAppFlowRepository extends JpaRepository<PfmsLoanAppFlow,Long> {
    @Query("select modelInfo from PfmsLoanAppFlow modelInfo where modelInfo.erpAuthorityFlow.actorName = :actorName and modelInfo.activeStatus = true")
    List<PfmsLoanAppFlow> pfmsLoanAppFlowByActorName(@Param("actorName") String actorName);


}
