package gov.step.app.repository;

import gov.step.app.domain.HrEmployeeInfo;
import gov.step.app.domain.InstEmployee;
import gov.step.app.domain.Institute;
import gov.step.app.domain.User;
import gov.step.app.domain.enumeration.designationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the HrEmployeeInfo entity.
 */
public interface HrEmployeeInfoRepository extends JpaRepository<HrEmployeeInfo,Long>
{
    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.user.login = ?#{principal.username} AND modelInfo.activeAccount=true ")
    HrEmployeeInfo findOneByEmployeeUserIsCurrentUser();

    List<HrEmployeeInfo> findAllByLogStatusAndActiveStatus(Long logStatus, boolean activeStatus);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where logStatus=:logStatus order by updateDate asc")
    List<HrEmployeeInfo> findAllByLogStatus(@Param("logStatus") Long logStatus);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.logStatus=0 OR modelInfo.logStatus=5 order by updateDate asc")
    List<HrEmployeeInfo>  findAllEmployeeByStatuses();

    @Query("select modelInfo from HrEmployeeInfo modelInfo where lower(modelInfo.nationalId) = :nationalId ")
    Optional<HrEmployeeInfo> findOneByNationalId(@Param("nationalId") String nationalId);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.employeeId = :employeeId")
    HrEmployeeInfo findByEmployeeId(@Param("employeeId") String employeeId);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.employeeId = :employeeId")
    HrEmployeeInfo findHrmByEmployeeId(@Param ("employeeId")Long employeeId);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.workArea.id = :workAreaId")
    List<HrEmployeeInfo> findEmployeeByWorkarea(@Param("workAreaId") long workAreaId);

    /* For PGMS Family Pension Information */
    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.nationalId = :nid ")
    HrEmployeeInfo findByNationalId(@Param("nid") String nationalId);

    HrEmployeeInfo findByEmployeeIdAndNationalId(String employeeId, String nationalId);

   // @Query("select modelInfo from HrEmployeeInfo modelInfo")
    List<HrEmployeeInfo> findAll();

    /* Asset Requisition */
    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.user = :user AND modelInfo.activeAccount=true")
    HrEmployeeInfo findEmployeeDetailsById(@Param("user") User user);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.designationInfo.designationInfo.designationLevel=:desigLevel")
    List<HrEmployeeInfo> findEmployeeListByDesignationLevel(@Param("desigLevel") int desigLevel);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.activeAccount=true AND employeeType = :employeeType ")
    List<HrEmployeeInfo> findAllEmployeeListByType(@Param("employeeType") designationType employeeType);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.activeAccount=true AND employeeType in (:employeeType) ")
    List<HrEmployeeInfo> findAllEmployeeListByTypeList(@Param("employeeType") String employeeType);

    // Hrm Employee list by institute
    @Query("select hrEmployee from HrEmployeeInfo hrEmployee where hrEmployee.institute.id = :instituteId")
    List<HrEmployeeInfo> findEmployeeListByInstitute(@Param("instituteId")Long instituteId);

    @Query("select hrEmployee from HrEmployeeInfo hrEmployee where hrEmployee.institute.id is not null and hrEmployee.organizationType = 'Institute' and hrEmployee.activeStatus = true")
    List<HrEmployeeInfo> findHrEmployeeOfInstitutes();

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.birthDate > :birthDateMin AND modelInfo.birthDate < :birthDateMax")
    Page<HrEmployeeInfo> findEmployeeListBirthDateRange(@Param("birthDateMin") LocalDate birthDateMin, @Param("birthDateMax") LocalDate birthDateMax,Pageable pageable);

    // Update Employee for DTE Transformation
    @Modifying
    @Transactional
    @Query("update HrEmployeeInfo modelInfo set organizationType =:orgType, employeeType = :emplType, " +
        " workArea.id =:orgCatId, workAreaDtl.id =:orgId, departmentInfo.id =:deptId, designationInfo.id =:desigId, logId=:logId, instCategory.id=null,instLevel.id=null,instEmployee.id = null,institute.id=null where modelInfo.id = :empId")
    void updateEmployeeForDteTransfer(@Param("empId") Long empId,
                                      @Param("emplType") designationType emplType,
                                      @Param("orgType") String orgType,
                                      @Param("orgCatId") Long orgCatId,
                                      @Param("orgId") Long orgId,
                                      @Param("deptId") Long deptId,
                                      @Param("desigId") Long desigId,
                                      @Param("logId") Long logId);

    @Modifying
    @Transactional
    @Query("update HrEmployeeInfo modelInfo set organizationType =:orgType, employeeType = :emplType, " +
        " workArea.id = null, workAreaDtl.id = null, departmentInfo.id = :deptId, designationInfo.id = :desigId, logId=:logId, instCategory.id = :instCatId,  instLevel.id = :instLevelId, institute.id = :instId where modelInfo.id = :empId")
    void updateEmployeeFromDteToInstituteTransfer(@Param("empId") Long empId,
                                      @Param("emplType") designationType emplType,
                                      @Param("orgType") String orgType,
                                      @Param("instCatId") Long instCatId,
                                      @Param("instLevelId") Long instLevelId,
                                      @Param("instId") Long instId,
                                      @Param("deptId") Long deptId,
                                      @Param("desigId") Long desigId,
                                      @Param("logId") Long logId);

    @Modifying
    @Transactional
    @Query("update HrEmployeeInfo modelInfo set organizationType =:orgType, employeeType = :emplType, " +
        " workArea.id = null, workAreaDtl.id = null, departmentInfo.id = :deptId, designationInfo.id = :desigId, logId=:logId, instCategory.id = :instCatId, instLevel.id = :instLevelId, institute.id = :instId," +
        " inst_employee_id = :instEmpId where modelInfo.id = :empId")
    void updateEmployeeFromDteToInstituteTransfer2(@Param("empId") Long empId,
                                                   @Param("emplType") designationType emplType,
                                                   @Param("orgType") String orgType,
                                                   @Param("instCatId") Long instCatId,
                                                   @Param("instLevelId") Long instLevelId,
                                                   @Param("instId") Long instId,
                                                   @Param("deptId") Long deptId,
                                                   @Param("desigId") Long desigId,
                                                   @Param("logId") Long logId,
                                                   @Param("instEmpId") Long instEmpId);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.retirementDate BETWEEN :startDate and :endDate ")
    List<HrEmployeeInfo> findAllByRetirementDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where activeStatus =:activeStatus order by employeeId")
    List<HrEmployeeInfo> findAllByActiveStatus(@Param("activeStatus") boolean activeStatus);



    /*@Query("SELECT hrEmployeeInfo FROM HrEmployeeInfo hrEmployeeInfo WHERE hrEmployeeInfo.fullName LIKE CONCAT('%',:fullName,'%')")
    List<HrEmployeeInfo> findByUserTyping(@Param("fullName") String fullName);
*/
    @Query("select hrEmployeeInfo from HrEmployeeInfo hrEmployeeInfo where hrEmployeeInfo.fullName LIKE ('%' || :fullName )")
    List<HrEmployeeInfo> findByUserTyping( @Param("fullName") String fullName);

    List<HrEmployeeInfo> findByFullNameStartingWithIgnoreCase(@Param("fullName") String fullName);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where departmentInfo.id=:deptId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmployeeInfo> findAllModelsByDepartmentAndPendingStatus(@Param("deptId") Long deptId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where departmentInfo.id IN :deptIdList AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmployeeInfo> findAllModelsByDepartmentIdsAndPendingStatus(@Param("deptIdList") List<Long> deptIdList, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where organizationType=:orgType AND institute.id=:instId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmployeeInfo> findAllModelsByInstituteAndPendingStatus(@Param("orgType") String orgType, @Param("instId") Long instId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where logStatus IN :logStatusList order by createDate asc")
    List<HrEmployeeInfo> findAllModelByLogStatuses(@Param("logStatusList") List<Long> logStatusList);

//    @Query("select employeeInfo from HrEmployeeInfo  employeeInfo where employeeInfo.instEmployee.id =:instEmployeeId")
    HrEmployeeInfo findByInstEmployee(@Param("instEmployee")InstEmployee instEmployee);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.designationInfo.designationInfo.designationName =:designationName AND modelInfo.designationInfo.desigType =:desigType AND modelInfo.activeStatus =:activeStatus")
    List<HrEmployeeInfo> findByDesignationNameAndDesignationType(@Param("designationName")String designationName, @Param("desigType")designationType desigType, @Param("activeStatus")Boolean activeStatus);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where departmentInfo.id=:deptId order by fullName")
    List<HrEmployeeInfo> findAllModelsByDepartment(@Param("deptId") Long deptId);

    @Query("select modelInfo from HrEmployeeInfo modelInfo where modelInfo.workAreaDtl.id=:workAreaDtlId order by fullName")
    List<HrEmployeeInfo> findAllByWorkAreaDetails(@Param("workAreaDtlId") Long workAreaDtlId);
}
