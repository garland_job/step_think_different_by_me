package gov.step.app.repository;

import gov.step.app.domain.AlmEmpLeaveApplication;
import gov.step.app.domain.AlmEmpLeaveCancellation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the AlmEmpLeaveCancellation entity.
 */
public interface AlmEmpLeaveCancellationRepository extends JpaRepository<AlmEmpLeaveCancellation,Long> {
    @Query("select almleave from AlmEmpLeaveCancellation almleave where almleave.nextApprover.user.login =:login order by almleave.id desc")
    Page<AlmEmpLeaveCancellation> findAllByLogin(Pageable pageable, @Param("login") String login);

}
