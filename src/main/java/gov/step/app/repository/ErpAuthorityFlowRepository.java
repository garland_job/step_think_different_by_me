package gov.step.app.repository;

import gov.step.app.domain.ErpAuthorityFlow;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the ErpAuthorityFlow entity.
 */
public interface ErpAuthorityFlowRepository extends JpaRepository<ErpAuthorityFlow,Long> {
    @Query("select modelInfo from ErpAuthorityFlow modelInfo where modelInfo.authorityCategory = :authorityCategory and modelInfo.activeStatus = true and modelInfo.firstAuthority = :orgType")
    ErpAuthorityFlow findOneErpAuthorityFlowByCategoryOrgTypeInitial(@Param("authorityCategory") String authorityCategory, @Param("orgType") String orgType);

    @Query("select modelInfo from ErpAuthorityFlow modelInfo where modelInfo.authorityCategory = :authorityCategory and modelInfo.activeStatus = true")
    List<ErpAuthorityFlow> findAllErpAuthorityFlowByCategory(@Param("authorityCategory") String authorityCategory);

    @Query("select modelInfo from ErpAuthorityFlow modelInfo where modelInfo.authorityCategory = :authorityCategory and modelInfo.actorName = :actorName and modelInfo.activeStatus = true")
    ErpAuthorityFlow findOneErpAuthorityFlowByActorName(@Param("authorityCategory") String authorityCategory, @Param("actorName") String actorName);
}
