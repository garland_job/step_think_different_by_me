package gov.step.app.repository;

import gov.step.app.domain.PgmsAppRetirmntAttach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data JPA repository for the PgmsAppRetirmntAttach entity.
 */
public interface PgmsAppRetirmntAttachRepository extends JpaRepository<PgmsAppRetirmntAttach,Long> {


    @Query("select modelInfo from PgmsAppRetirmntAttach  modelInfo where modelInfo.appRetirmntPenInfo.id = :retirementPenId")
    List<PgmsAppRetirmntAttach> findAllByAppRetirmntPenId(@Param("retirementPenId") Long retirementPenId);

    @Modifying
    @Transactional
    @Query("delete from PgmsAppRetirmntAttach modelInfo where modelInfo.appRetirmntPenInfo.id = :penId")
    void deleteByAppRetirmntPenId(@Param("penId") Long appRetirmntPenId);

}
