package gov.step.app.repository;

import gov.step.app.domain.JasperReportSecurity;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the JasperReportSecurity entity.
 */
public interface JasperReportSecurityRepository extends JpaRepository<JasperReportSecurity,Long> {

}
