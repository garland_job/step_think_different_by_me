package gov.step.app.repository;

import gov.step.app.domain.PgmsAppRetirmntPen;
import gov.step.app.domain.RetirementAppStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the RetirementAppStatusLog entity.
 */
public interface RetirementAppStatusLogRepository extends JpaRepository<RetirementAppStatusLog,Long> {

    @Query("select modelInfo.pgmsAppRetirmntPen from RetirementAppStatusLog modelInfo group by modelInfo.pgmsAppRetirmntPen having max(modelInfo.status)>=:pStatus")
    List<PgmsAppRetirmntPen> findMaxByStatus(@Param("pStatus") Integer pStatus);

}
