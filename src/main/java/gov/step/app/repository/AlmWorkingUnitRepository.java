package gov.step.app.repository;

import gov.step.app.domain.AlmWorkingUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the AlmWorkingUnit entity.
 */
public interface AlmWorkingUnitRepository extends JpaRepository<AlmWorkingUnit,Long> {
    @Query("select modelInfo from AlmWorkingUnit modelInfo where modelInfo.almShiftSetup.id = :shiftId")
    AlmWorkingUnit findAlmWorkingUnitByShift(@Param("shiftId") long shiftId);

}
