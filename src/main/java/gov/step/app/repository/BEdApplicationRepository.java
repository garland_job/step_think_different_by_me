package gov.step.app.repository;

import gov.step.app.domain.BEdApplication;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the BEdApplication entity.
 */
public interface BEdApplicationRepository extends JpaRepository<BEdApplication,Long> {

    @Query("select bEdApplication from BEdApplication bEdApplication where bEdApplication.instEmployee.code = :code")
    BEdApplication findByInstEmployeeCode(@Param("code") String code);

    @Query("select bedApplication from BEdApplication bedApplication where bedApplication.status = :status and (bedApplication.instEmployee.payScale is not null and bedApplication.instEmployee.bEDAppStatus =:appStatus) and bedApplication.instEmployee.mpoActive=true")
    Page<BEdApplication> findPayScaleAssignedList(Pageable var1, @Param("status") int code,@Param("appStatus") Integer appStatus);

    @Query("select bedApplication from BEdApplication bedApplication where bedApplication.status = :status and bedApplication.instEmployee.bEDAppStatus =:appStatus and bedApplication.instEmployee.payScale is not null")
    Page<BEdApplication> findPayScaleNotAssignedList(Pageable var1, @Param("status") int code,@Param("appStatus") Integer appStatus);

}
