package gov.step.app.repository;

import gov.step.app.domain.TrainingSetup;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TrainingSetup entity.
 */
public interface TrainingSetupRepository extends JpaRepository<TrainingSetup,Long> {

}
