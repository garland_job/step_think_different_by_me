package gov.step.app.repository;

import gov.step.app.domain.HrEmpIncrementInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

/**
 * Spring Data JPA repository for the HrEmpIncrementInfo entity.
 */
public interface HrEmpIncrementInfoRepository extends JpaRepository<HrEmpIncrementInfo,Long>
{
    @Query("select modelInfo from HrEmpIncrementInfo modelInfo where modelInfo.employeeInfo.user.login = ?#{principal.username} AND modelInfo.employeeInfo.activeAccount=true")
    HrEmpIncrementInfo findOneByEmployeeIsCurrentUser();

    @Query("select modelInfo from HrEmpIncrementInfo modelInfo where modelInfo.employeeInfo.id =:empId")
    HrEmpIncrementInfo findByHrEmployeeIdIsElpcYearlyPayment(@Param("empId") Long empId);

}
