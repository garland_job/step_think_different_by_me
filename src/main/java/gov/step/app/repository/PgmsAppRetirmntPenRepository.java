package gov.step.app.repository;

import gov.step.app.domain.PgmsAppRetirmntPen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PgmsAppRetirmntPen entity.
 */
public interface PgmsAppRetirmntPenRepository extends JpaRepository<PgmsAppRetirmntPen,Long> {

    @Query("select modelInfo from PgmsAppRetirmntPen modelInfo where modelInfo.approveStatus = :statusType")
    List<PgmsAppRetirmntPen> findAllByAprvStatusOrderByIdAsc(@Param("statusType") Integer aprvStatus);

    @Query("select modelInfo from PgmsAppRetirmntPen modelInfo where modelInfo.hrEmployeeInfo.id = :empId")
    List<PgmsAppRetirmntPen> findByEmployeeInfo(@Param("empId") Long hrEmployeeInfo);

    List<PgmsAppRetirmntPen> findByApproveStatus(Integer approveStatus);

}
