package gov.step.app.repository;

import gov.step.app.domain.Authority;
import gov.step.app.domain.NotificationStep;
import gov.step.app.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(ZonedDateTime dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);

    //Optional<User> findOneByDistrictId(Long districtId);

    Optional<User> findOneById(Long userId);

    //User findOneUserByUserId(Long userId);

    @Query("select user from User user where user.activated = :activated")
    Page<User> findAllBlockedUser(Pageable pageable, @Param("activated") Boolean activated);

    //get all users of a specific authority name /role
    @Query("select user from User user join user.authorities auth where auth.name = :role")
    Page<User> findAllUsersByAuthority(Pageable pageable, @Param("role") String role);
    @Query("select user from User user join user.authorities auth where auth.name = :role")
    List<User> findUsersByAuthority( @Param("role") String role);
    @Query("select user from User user where user.login = :login")
    User findOneUserByUserLogin( @Param("login") String login);
    @Query("select user from User user where user.login = :login")
    User findUserByUserLogin(@Param("login") String login);


    @Query("select user from User user where user.activated = :activated and user.login like %:items%")
    Page<User> findAllUserByLogin(Pageable pageable, @Param("activated") Boolean activated, @Param("items") String items);
    @Query("select user from User user where user.activated = :activated and user.firstName like %:firstName%")
    Page<User> findAllUserByFirstName(Pageable pageable, @Param("activated") Boolean activated, @Param("firstName") String firstName);
    @Query("select user from User user where user.activated = :activated and user.lastName like %:lastName%")
    Page<User> findAllUserByLastName(Pageable pageable, @Param("activated") Boolean activated, @Param("lastName") String lastName);
    @Query("select user from User user where user.activated = :activated and user.email like %:email%")
    Page<User> findAllUserByEmail(Pageable pageable, @Param("activated") Boolean activated, @Param("email") String email);

    @Query("select user from User user where user.activated = :activated")
    Page<User> findAllUserBySearchItems(Pageable pageable, @Param("activated") Boolean activated);

    @Query("select user from User user where user.id = :userId")
    User findOneByUserId(@Param("userId") Long userId);

    @Query("select user from User user where user.login = :login")
    User findOneByUserUserName(@Param("login") String login);

    @Query("select user from User user where user.login like :login")
    List<User> findUserlist(@Param("login") String login);

    @Override
    void delete(User t);

    /*Find and Return List of Jhi_User for matching user.logIn given regex-pattern*/
    @Query("select user from User user where user.login LIKE :userLogin")
    List<User> findByUserLoginStartingWithIgnoreCase(@Param("userLogin") String userLogin);
}
