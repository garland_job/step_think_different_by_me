package gov.step.app.repository;

import gov.step.app.domain.TimeScaleApplication;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the TimeScaleApplication entity.
 */
public interface TimeScaleApplicationRepository extends JpaRepository<TimeScaleApplication,Long> {

    @Query("select timeScaleApplication from TimeScaleApplication timeScaleApplication where timeScaleApplication.instEmployee.code = :code and timeScaleApplication.status > 1")
    TimeScaleApplication findByInstEmployeeCode(@org.springframework.data.repository.query.Param("code") String code);

    @Query("select timeScaleApplication from TimeScaleApplication timeScaleApplication where timeScaleApplication.status = :status and (timeScaleApplication.instEmployee.payScale is not null and timeScaleApplication.instEmployee.timescaleAppStatus =:appStatus)")
    Page<TimeScaleApplication> findPayScaleAssignedList(Pageable var1, @Param("status") int code,@Param("appStatus") Integer appStatus);

    @Query("select timeScaleApplication from TimeScaleApplication timeScaleApplication where timeScaleApplication.status = :status and timeScaleApplication.instEmployee.timescaleAppStatus =:appStatus and timeScaleApplication.instEmployee.mpoActive=true")
    Page<TimeScaleApplication> findPayScaleNotAssignedList(Pageable var1, @Param("status") int code,@Param("appStatus") Integer appStatus);

}
