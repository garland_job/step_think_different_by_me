package gov.step.app.repository;

import gov.step.app.domain.AlmAttendanceInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the AlmAttendanceInformation entity.
 */
public interface AlmAttendanceInformationRepository extends JpaRepository<AlmAttendanceInformation,Long> {
    @Query("select almAttendanceInformation from AlmAttendanceInformation almAttendanceInformation where almAttendanceInformation.almShiftSetup.id = :almShiftSetupId")
    List<AlmAttendanceInformation> findAllByAlmAttendanceInformationByAlmShift(@Param("almShiftSetupId") Long almShiftSetupId);

}
