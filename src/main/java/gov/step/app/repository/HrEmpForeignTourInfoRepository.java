package gov.step.app.repository;

import gov.step.app.domain.HrEmpForeignTourInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the HrEmpForeignTourInfo entity.
 */
public interface HrEmpForeignTourInfoRepository extends JpaRepository<HrEmpForeignTourInfo,Long>
{
    @Query("select modelInfo from HrEmpForeignTourInfo modelInfo where modelInfo.employeeInfo.user.login = ?#{principal.username} AND modelInfo.employeeInfo.activeAccount=true")
    List<HrEmpForeignTourInfo> findAllByEmployeeIsCurrentUser();

    List<HrEmpForeignTourInfo> findAllByLogStatusAndActiveStatus(Long logStatus, boolean activeStatus);

    @Query("select modelInfo from HrEmpForeignTourInfo modelInfo where logStatus=:logStatus order by updateDate asc")
    List<HrEmpForeignTourInfo> findAllByLogStatus(@Param("logStatus") Long logStatus);

    @Query("select modelInfo from HrEmpForeignTourInfo modelInfo where employeeInfo.departmentInfo.id IN :deptIdList AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmpForeignTourInfo> findAllModelsByDepartmentIdsAndPendingStatus(@Param("deptIdList") List<Long> deptIdList, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmpForeignTourInfo modelInfo where employeeInfo.organizationType=:orgType AND employeeInfo.institute.id=:instId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmpForeignTourInfo> findAllModelsByInstituteAndPendingStatus(@Param("orgType") String orgType, @Param("instId") Long instId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmpForeignTourInfo modelInfo where logStatus IN :logStatusList order by createDate asc")
    List<HrEmpForeignTourInfo> findAllModelByLogStatuses(@Param("logStatusList") List<Long> logStatusList);
}
