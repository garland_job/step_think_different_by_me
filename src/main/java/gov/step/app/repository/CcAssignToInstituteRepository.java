package gov.step.app.repository;

import gov.step.app.domain.CcAssignToInstitute;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CcAssignToInstitute entity.
 */
public interface CcAssignToInstituteRepository extends JpaRepository<CcAssignToInstitute,Long> {

}
