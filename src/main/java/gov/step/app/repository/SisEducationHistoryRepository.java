package gov.step.app.repository;

import gov.step.app.domain.JobPlacementInfo;
import gov.step.app.domain.SisEducationHistory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the SisEducationHistory entity.
 */
public interface SisEducationHistoryRepository extends JpaRepository<SisEducationHistory,Long> {

    public Page<SisEducationHistory> findAllByOrderByIdDesc(Pageable pageable);

    @Query("select sisEducationHistory from SisEducationHistory sisEducationHistory where sisEducationHistory.createBy = :createBy")
    Page<SisEducationHistory> findStudentsByUserId(Pageable pageable, @Param("createBy") Long createBy);

    @Query("select jobPlacementInfo from JobPlacementInfo jobPlacementInfo where jobPlacementInfo.accountName = :userId")
    JobPlacementInfo findOrgNameUserId(@Param("userId") String userId);

    @Query("select sisEducationHistory from SisEducationHistory sisEducationHistory where sisEducationHistory.id.createBy.login = ?#{principal.username}")
    Page<SisEducationHistory> findsisEducationHistory(Pageable pageable);
}
