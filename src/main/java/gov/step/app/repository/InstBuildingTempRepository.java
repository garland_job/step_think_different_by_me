package gov.step.app.repository;

import gov.step.app.domain.InstBuildingTemp;

import gov.step.app.domain.InstShopInfoTemp;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the InstBuildingTemp entity.
 */
public interface InstBuildingTempRepository extends JpaRepository<InstBuildingTemp,Long> {

    @Query("select instBuildingTemp from InstBuildingTemp instBuildingTemp where instBuildingTemp.instInfraInfoTemp.institute.id =:instituteId")
    List<InstBuildingTemp> findListByInstituteId(@Param("instituteId") Long instituteId);

    @Query("select instBuildingTemp from InstBuildingTemp instBuildingTemp where instBuildingTemp.instInfraInfoTemp.id =:infraInfoId")
    List<InstBuildingTemp> findByInfraInfoId(@Param("infraInfoId") Long infraInfoId);



}
