package gov.step.app.repository;

import gov.step.app.domain.AlmApplicationStatusLog;
import gov.step.app.domain.AlmEmpLeaveApplication;
import gov.step.app.domain.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the AlmEmpLeaveApplication entity.
 */
public interface AlmEmpLeaveApplicationRepository extends JpaRepository<AlmEmpLeaveApplication,Long> {

    @Query("select almleave from AlmEmpLeaveApplication almleave where almleave.nextApprover.id=:empId order by almleave.id desc")
    Page<AlmEmpLeaveApplication> findAllByEmpId(Pageable pageable, @Param("empId") Long empId);

    @Query("select almleave from AlmEmpLeaveApplication almleave where almleave.nextApprover.user.login =:login order by almleave.id desc")
    Page<AlmEmpLeaveApplication> findAllByLogin(Pageable pageable, @Param("login") String login);
    @Query("select almleave from AlmEmpLeaveApplication almleave where almleave.employeeInfo.id =:id order by almleave.id desc")
    List<AlmEmpLeaveApplication> findAllByEmployee(@Param("id") Long id);

}
