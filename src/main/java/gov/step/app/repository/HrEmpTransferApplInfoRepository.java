package gov.step.app.repository;

import gov.step.app.domain.HrEmpTransferApplInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the HrEmpTransferApplInfo entity.
 */
public interface HrEmpTransferApplInfoRepository extends JpaRepository<HrEmpTransferApplInfo,Long>
{
    @Query("select modelInfo from HrEmpTransferApplInfo modelInfo where modelInfo.employeeInfo.user.login = ?#{principal.username} AND modelInfo.employeeInfo.activeAccount=true")
    List<HrEmpTransferApplInfo> findAllByEmployeeIsCurrentUser();

    List<HrEmpTransferApplInfo> findAllByLogStatusAndActiveStatus(Long logStatus, boolean activeStatus);

    @Query("select modelInfo from HrEmpTransferApplInfo modelInfo where logStatus=:logStatus order by updateDate asc")
    List<HrEmpTransferApplInfo> findAllByLogStatus(@Param("logStatus") Long logStatus);

    @Query("select modelInfo from HrEmpTransferApplInfo modelInfo where employeeInfo.departmentInfo.id IN :deptIdList AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmpTransferApplInfo> findAllModelsByDepartmentIdsAndPendingStatus(@Param("deptIdList") List<Long> deptIdList, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmpTransferApplInfo modelInfo where employeeInfo.organizationType=:orgType AND employeeInfo.institute.id=:instId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmpTransferApplInfo> findAllModelsByInstituteAndPendingStatus(@Param("orgType") String orgType, @Param("instId") Long instId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmpTransferApplInfo modelInfo where logStatus IN :logStatusList order by createDate asc")
    List<HrEmpTransferApplInfo> findAllModelByLogStatuses(@Param("logStatusList") List<Long> logStatusList);
}
