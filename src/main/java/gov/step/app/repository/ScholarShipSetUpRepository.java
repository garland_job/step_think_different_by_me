package gov.step.app.repository;

import gov.step.app.domain.ScholarShipSetUp;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ScholarShipSetUp entity.
 */
public interface ScholarShipSetUpRepository extends JpaRepository<ScholarShipSetUp,Long> {

}
