package gov.step.app.repository;

import gov.step.app.domain.InstVacancy;
import gov.step.app.domain.InstVacancyTemp;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the InstVacancyTemp entity.
 */
public interface InstVacancyTempRepository extends JpaRepository<InstVacancyTemp,Long> {

    @Query("select instVacancyTemp from InstVacancyTemp instVacancyTemp where instVacancyTemp.institute.id = :instId")
    List<InstVacancyTemp> findAllByInstitute(@Param("instId") Long instId);
}
