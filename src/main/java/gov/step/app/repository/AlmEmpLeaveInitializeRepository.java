package gov.step.app.repository;

import gov.step.app.domain.AlmEmpLeaveInitialize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the AlmEmpLeaveInitialize entity.
 */
public interface AlmEmpLeaveInitializeRepository extends JpaRepository<AlmEmpLeaveInitialize,Long> {
    @Query("select modelInfo from AlmEmpLeaveInitialize modelInfo where modelInfo.employeeInfo.id = :employeeInfoId and modelInfo.almLeaveType.id = :leaveTypeId and modelInfo.activeStatus = true")
    List<AlmEmpLeaveInitialize> getAlmLeaveInitializeByEmployeeAndLeaveType(@Param("employeeInfoId") long employeeInfoId, @Param("leaveTypeId") long leaveTypeId);

    @Query("select sum(modelInfo.leaveBalance) as totalLeave from AlmEmpLeaveInitialize modelInfo where almLeaveType.id =:almLeaveType and employeeInfo.id =:empInfo and activeStatus = true")
    AlmEmpLeaveInitialize findAllByAlmLeaveType (@Param("almLeaveType") Long almLeaveType, @Param("empInfo") long employeeInfo);
}
