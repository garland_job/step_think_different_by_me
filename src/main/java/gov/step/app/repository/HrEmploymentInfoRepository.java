package gov.step.app.repository;

import gov.step.app.domain.HrEmploymentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data JPA repository for the HrEmploymentInfo entity.
 */
public interface HrEmploymentInfoRepository extends JpaRepository<HrEmploymentInfo,Long>
{
    @Query("select modelInfo from HrEmploymentInfo modelInfo where modelInfo.employeeInfo.user.login = ?#{principal.username} AND modelInfo.employeeInfo.activeAccount=true ")
    List<HrEmploymentInfo> findAllByEmployeeIsCurrentUser();

    List<HrEmploymentInfo> findAllByLogStatusAndActiveStatus(Long logStatus, boolean activeStatus);

    @Query("select modelInfo from HrEmploymentInfo modelInfo where logStatus=:logStatus order by updateDate asc")
    List<HrEmploymentInfo> findAllByLogStatus(@Param("logStatus") Long logStatus);

    @Query("select modelInfo from HrEmploymentInfo modelInfo where employeeInfo.id =:employeeId ")
    List<HrEmploymentInfo> findAllByEmployee(@Param("employeeId") long employeeId);

    @Query("select modelInfo from HrEmploymentInfo modelInfo where modelInfo.employeeInfo.id =:employeeId AND modelInfo.activeStatus=true")
    HrEmploymentInfo findByEmployeeId(@Param("employeeId") long employeeId);

    @Query("select modelInfo from HrEmploymentInfo modelInfo where employeeInfo.departmentInfo.id IN :deptIdList AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmploymentInfo> findAllModelsByDepartmentIdsAndPendingStatus(@Param("deptIdList") List<Long> deptIdList, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmploymentInfo modelInfo where employeeInfo.organizationType=:orgType AND employeeInfo.institute.id=:instId AND logStatus=:lgStatus order by updateDate asc")
    List<HrEmploymentInfo> findAllModelsByInstituteAndPendingStatus(@Param("orgType") String orgType, @Param("instId") Long instId, @Param("lgStatus") Long lgStatus);

    @Query("select modelInfo from HrEmploymentInfo modelInfo where logStatus IN :logStatusList order by createDate asc")
    List<HrEmploymentInfo> findAllModelByLogStatuses(@Param("logStatusList") List<Long> logStatusList);

    @Modifying
    @Transactional
    @Query("update HrEmploymentInfo modelInfo set organizationType =:orgType, institute.id = null, " +
        " workAreaDtl.id = :workAreaDltId, departmentInfo.id = :departmentId, designationInfo.id = :designationId, " +
        " salaryStructureInfo.id = :salaryStructureId where modelInfo.id = :employmentId")
    void updateCurrentEmploymentInformationOfOrganization(@Param("employmentId") Long employmentId,
                                                  @Param("orgType") String orgType,
                                                  @Param("workAreaDltId") Long workAreaDltId,
                                                  @Param("departmentId") Long departmentId,
                                                  @Param("designationId") Long designationId,
                                                  @Param("salaryStructureId") Long salaryStructureId);

    @Modifying
    @Transactional
    @Query("update HrEmploymentInfo modelInfo set organizationType =:orgType, institute.id = null, " +
        " workAreaDtl.id = :workAreaDltId, departmentInfo.id = :departmentId, designationInfo.id = :designationId " +
        " where modelInfo.id = :employmentId")
    void updateCurrentEmploymentInformationOfOrganization(@Param("employmentId") Long employmentId,
                                                          @Param("orgType") String orgType,
                                                          @Param("workAreaDltId") Long workAreaDltId,
                                                          @Param("departmentId") Long departmentId,
                                                          @Param("designationId") Long designationId);

    @Modifying
    @Transactional
    @Query("update HrEmploymentInfo modelInfo set organizationType =:orgType, institute.id = :instituteId, " +
        " workAreaDtl.id = null, departmentInfo.id = :departmentId, designationInfo.id = :designationId, " +
        " salaryStructureInfo.id = :salaryStructureId where modelInfo.id = :employmentId")
    void updateCurrentEmploymentInformationInsitute(@Param("employmentId") Long employmentId,
                                            @Param("orgType") String orgType,
                                            @Param("instituteId") Long instituteId,
                                            @Param("departmentId") Long departmentId,
                                            @Param("designationId") Long designationId,
                                            @Param("salaryStructureId") Long salaryStructureId);

    @Modifying
    @Transactional
    @Query("update HrEmploymentInfo modelInfo set organizationType =:orgType, institute.id = :instituteId, " +
        " workAreaDtl.id = null, departmentInfo.id = :departmentId, designationInfo.id = :designationId " +
        " where modelInfo.id = :employmentId")
    void updateCurrentEmploymentInformationInsitute(@Param("employmentId") Long employmentId,
                                                    @Param("orgType") String orgType,
                                                    @Param("instituteId") Long instituteId,
                                                    @Param("departmentId") Long departmentId,
                                                    @Param("designationId") Long designationId);
}
