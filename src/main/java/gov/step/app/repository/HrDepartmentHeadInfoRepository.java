package gov.step.app.repository;

import gov.step.app.domain.HrDepartmentHeadInfo;

import gov.step.app.domain.HrDepartmentHeadSetup;
import gov.step.app.domain.HrEmployeeInfo;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data JPA repository for the HrDepartmentHeadInfo entity.
 */
public interface HrDepartmentHeadInfoRepository extends JpaRepository<HrDepartmentHeadInfo,Long>
{
    @Query("select modelInfo from HrDepartmentHeadInfo modelInfo where departmentInfo.id = :deptId ")
    List<HrDepartmentHeadInfo> findAllByDepartment(@Param("deptId") Long deptId);

    @Query("select modelInfo from HrDepartmentHeadInfo modelInfo where departmentInfo.id = :deptId AND activeHead = true")
    HrDepartmentHeadInfo findDepartmentHeadByActiveDepartmentHead(@Param("deptId") Long deptId);

    @Modifying
    @Transactional
    @Query("update HrDepartmentHeadInfo modelInfo set modelInfo.activeHead=:stat where modelInfo.departmentInfo.id = :deptId")
    void updateAllDepartmentHeadActiveStatus(@Param("deptId") Long deptId, @Param("stat") boolean stat);

    @Query("select modelInfo from HrDepartmentHeadInfo modelInfo where modelInfo.departmentInfo.institute.id = :instId ")
    List<HrDepartmentHeadInfo> findAllByInstitute(@Param("instId") Long instId);

    @Query("select modelInfo from HrDepartmentHeadInfo modelInfo where modelInfo.headInfo.user.login = ?#{principal.username} AND modelInfo.activeHead = true ")
    List<HrDepartmentHeadInfo> findAllByEmployeeIsCurrentUser();

    HrDepartmentHeadInfo findByHeadInfo(@Param("headInfo")HrEmployeeInfo headInfo);

    @Query("select modelInfo.headInfo from HrDepartmentHeadInfo modelInfo where modelInfo.departmentInfo.id=:deptId AND modelInfo.activeHead = true ")
    HrEmployeeInfo findHrDepartmentHeadInfoByDepartmentInfo(@Param("deptId") Long deptId);

    @Query("SELECT deptInfo FROM HrDepartmentHeadInfo deptInfo WHERE deptInfo.departmentInfo.id = :deptSetupId")
    HrDepartmentHeadInfo findByDepartmentSetupId(@Param("deptSetupId") Long deptSetupId);

}
