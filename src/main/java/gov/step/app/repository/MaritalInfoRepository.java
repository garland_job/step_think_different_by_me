package gov.step.app.repository;

import gov.step.app.domain.MaritalInfo;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the MaritalInfo entity.
 */
public interface MaritalInfoRepository extends JpaRepository<MaritalInfo,Long> {

}
