package gov.step.app.repository;

import gov.step.app.domain.BloodGroupInfo;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the BloodGroupInfo entity.
 */
public interface BloodGroupInfoRepository extends JpaRepository<BloodGroupInfo,Long> {

}
