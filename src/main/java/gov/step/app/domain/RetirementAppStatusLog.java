package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A RetirementAppStatusLog.
 */
@Entity
@Table(name = "pgms_retpen_app_status_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "retirementappstatuslog")
public class RetirementAppStatusLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "status")
    private Integer status;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "approved_date")
    private LocalDate approvedDate;

    @Column(name = "cause")
    private String cause;

    @ManyToOne
    @JoinColumn(name = "pgmsAppRetirmntPen_id")
    private PgmsAppRetirmntPen pgmsAppRetirmntPen;

    @ManyToOne
    @JoinColumn(name="action_by")
    private User user;

    @Column(name="create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private  Long createBy;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public PgmsAppRetirmntPen getPgmsAppRetirmntPen() {
        return pgmsAppRetirmntPen;
    }

    public void setPgmsAppRetirmntPen(PgmsAppRetirmntPen pgmsAppRetirmntPen) {
        this.pgmsAppRetirmntPen = pgmsAppRetirmntPen;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RetirementAppStatusLog retirementAppStatusLog = (RetirementAppStatusLog) o;

        if ( ! Objects.equals(id, retirementAppStatusLog.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RetirementAppStatusLog{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", remarks='" + remarks + "'" +
            ", approvedDate='" + approvedDate + "'" +
            ", cause='" + cause + "'" +
            '}';
    }
}
