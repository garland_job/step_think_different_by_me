package gov.step.app.domain;

import gov.step.app.domain.enumeration.jobQuota;
import gov.step.app.domain.payroll.PrlLocalitySetInfo;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;

import org.hibernate.annotations.Nationalized;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import gov.step.app.domain.enumeration.EmpType;

import gov.step.app.domain.enumeration.maritalStatus;

import gov.step.app.domain.enumeration.bloodGroup;

/**
 * A InstEmployee.
 */
@Entity
@Table(name = "inst_employee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "instemployee")
public class InstEmployee implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="inst_employee_seq")
    @SequenceGenerator(name="inst_employee_seq", sequenceName="inst_employee_seq")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Nationalized
    @Column(name = "bn_name", nullable = true)
    private String bnName;

    @Nationalized
    @Column(name = "father_name_bn", nullable = true)
    private String fatherNameBn;

    @Nationalized
    @Column(name = "mother_name_bn", nullable = true)
    private String motherNameBn;

    @NotNull
    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "contact_no", nullable = true)
    private String contactNo;

    @Column(name = "alt_contactNo", nullable = true)
    private String altContactNo;

    @Column(name = "father_name")
    private String fatherName;

    @Column(name = "job_category")
    private String jobCategory;

    @Column(name = "mother_name")
    private String motherName;

    @Column(name = "cos_of_dell")
    private String cosOfDell;

    @Column(name = "employment_type")
    private String employmentType;

    @NotNull
    @Column(name = "dob", nullable = false)
    private LocalDate dob;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(name = "category")
    private EmpType category;

    @Column(name = "gender")
    private String gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "marital_status")
    private maritalStatus maritalStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "blood_group")
    private bloodGroup bloodGroup;

    @Column(name = "tin")
    private String tin;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_name")
    private String imageName;


    @Column(name = "image_content_type")
    private String imageContentType;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "nid")
    private String nid;

    @Lob
    @Column(name = "nid_image")
    private byte[] nidImage;

    @Column(name = "nid_image_name")
    private String nidImageName;


    @Column(name = "nid_image_content_type")
    private String nidImageContentType;

    @Column(name = "birth_cert_no")
    private String birthCertNo;

    @Column(name = "birth_cert_no_name")
    private String birthCertNoName;

    @Lob
    @Column(name = "birth_cert_image")
    private byte[] birthCertImage;


    @Column(name = "birth_cert_image_content_type")
    private String birthCertImageContentType;

    @Column(name = "con_per_name")
    private String conPerName;

    @Column(name = "con_per_mobile")
    private String conPerMobile;

    @Column(name = "con_per_address")
    private String conPerAddress;


    @Column(name = "code",unique=true)
    private String code;

    @Column(name = "mpo_app_status")
    private  Integer mpoAppStatus = 0;

    @Column(name = "timescale_app_status")
    private  Integer timescaleAppStatus= 0;

    @Column(name = "bed_app_status")
    private  Integer bEDAppStatus= 0;

    @Column(name = "ap_app_status")
    private  Integer aPAppStatus= 0;

    @Column(name = "principle_app_status")
    private  Integer principleAppStatus = 0;

    @Column(name = "name_cncl_app_status")
    private  Integer nameCnclAppStatus = 0;

    @Column(name = "index_no", unique=true)
    private  String indexNo;

    @Column(name = "withheld_ammount")
    private  Double withheldAmount;

    @Column(name = "withheld_start_date")
    private  LocalDate withheldStartDate;

    @Column(name = "withheld_end_date")
    private  LocalDate withheldEndDate;

    @Column(name = "pay_scale_changed_date")
    private  LocalDate payScaleChangedDate;

    @Column(name = "withheld_active")
    private  Boolean withheldActive;

    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Institute institute;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "teacher_level", nullable = true)
    private String level;

    @Column(name = "teacher_quota")
    private String quota;

    @Column(name = "apply_date", nullable = true)
    private LocalDate applyDate;

    @Column(name = "mpo_active_date")
    private LocalDate mpoActiveDate;

    @ManyToOne
    @JoinColumn(name = "religion_id")
    private Religion religion;

    @Column(name = "status")
    private  Integer status = 0;

    @Column(name = "entry_status")
    private String entryStatus;

    @Column(name = "remarks", nullable = false)
    private String remarks;

    @Column(name = "type_of_position")
    private String typeOfPosition;

    @Column(name = "registered_Certificate_Sub")
    private String registeredCertificateSubject;

    @Column(name = "roll_no")
    private String rollNo;

    @Column(name = "result_publish_date")
    private LocalDate resultPublishDate;

    @Column(name = "passing_year")
    private String passingYear;

    @Column(name = "ordinal_no")
    private String ordinalNo;

    @Column(name = "registration_exam_year")
    private String registrationExamYear;

    //new field for merging with hrm
    @NotNull
    @Column(name = "joining_date",nullable = false)
    private LocalDate joiningDate;

    @Column(name = "registered_Certificate_no")
    private String registeredCertificateNo;

    @Column(name = "mpo_active")
    private Boolean mpoActive;

    @Column(name = "is_jp_admin")
    private Boolean isJPAdmin;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(name = "job_quota")
    private jobQuota jobQuota;

    @Lob
    @Column(name = "quota_cert")
    private byte[] quotaCert;

    @Column(name = "quota_cert_content_type")
    private String quotaCertContentType;

    @Column(name = "quota_cert_name")
    private String quotaCertName;

    @ManyToOne
    @JoinColumn(name = "iis_course_info_id")
    private IisCourseInfo iisCourseInfo;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "inst_level_id",nullable = false)
    private InstLevel instLevel;

//    @NotNull
    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district;

    @ManyToOne
    @JoinColumn(name = "pay_scale_id")
    private PayScale payScale;

    //common for teacher and hrm
    @NotNull
    @ManyToOne
    @JoinColumn(name = "designation_setup_id",nullable = false)
    private HrDesignationSetup designationSetup;

    //common for teacher and hrm
    // Work as a Recruitment type/Employment Type
    @ManyToOne
    @JoinColumn(name = "employee_type_id")
    private HrEmplTypeInfo employeeType;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name="is_head_of_dept")
    private String isHeadOfDept;

    @ManyToOne
    @JoinColumn(name="hr_department_setup")
    private  HrDepartmentSetup hrdepartmentSetup;

    @ManyToOne
    @JoinColumn(name = "cms_curriculum_id")
    private CmsCurriculum cmsCurriculum;

    @ManyToOne
    @JoinColumn(name = "cms_trade_id")
    private CmsTrade cmsTrade;

    @ManyToOne
    @JoinColumn(name = "cms_subject_id")
    private CmsSubject cmsSubject;

    @ManyToOne
    @JoinColumn(name="employment_status")
    private MiscTypeSetup employmentStatus;

    @ManyToOne
    @JoinColumn(name="job_status")
    private MiscTypeSetup jobStatus;

    @Column(name = "appointment_go_date")
    private LocalDate appointmentGoDate;

    @Column(name = "prl_date")
    private LocalDate prlDate;

    @Column(name = "retirement_date")
    private LocalDate retirementDate;

    @ManyToOne
    @JoinColumn(name = "encadrement_id")
    private MiscTypeSetup encadrement;

    @PrePersist
    protected void onCreate() {
        this.setMpoAppStatus(0);

    }

    public gov.step.app.domain.enumeration.jobQuota getJobQuota() {
        return jobQuota;
    }

    public void setJobQuota(gov.step.app.domain.enumeration.jobQuota jobQuota) {
        this.jobQuota = jobQuota;
    }

    public byte[] getQuotaCert() {
        return quotaCert;
    }

    public void setQuotaCert(byte[] quotaCert) {
        this.quotaCert = quotaCert;
    }

    public String getQuotaCertContentType() {
        return quotaCertContentType;
    }

    public void setQuotaCertContentType(String quotaCertContentType) {
        this.quotaCertContentType = quotaCertContentType;
    }

    public String getQuotaCertName() {
        return quotaCertName;
    }

    public void setQuotaCertName(String quotaCertName) {
        this.quotaCertName = quotaCertName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCosOfDell() {
        return cosOfDell;
    }

    public void setCosOfDell(String cosOfDell) {
        this.cosOfDell = cosOfDell;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAltContactNo() {
        return altContactNo;
    }

    public void setAltContactNo(String altContactNo) {
        this.altContactNo = altContactNo;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(String jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }




    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public EmpType getCategory() {
        return category;
    }

    public void setCategory(EmpType category) {
        this.category = category;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public maritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(maritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEntryStatus() {
        return entryStatus;
    }

    public void setEntryStatus(String entryStatus) {
        this.entryStatus = entryStatus;
    }

    public bloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(bloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMotherNameBn() {
        return motherNameBn;
    }

    public void setMotherNameBn(String motherNameBn) {
        this.motherNameBn = motherNameBn;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getNidImageName() {
        return nidImageName;
    }

    public void setNidImageName(String nidImageName) {
        this.nidImageName = nidImageName;
    }

    public byte[] getNidImage() {
        return nidImage;
    }

    public void setNidImage(byte[] nidImage) {
        this.nidImage = nidImage;
    }

    public String getNidImageContentType() {
        return nidImageContentType;
    }

    public void setNidImageContentType(String nidImageContentType) {
        this.nidImageContentType = nidImageContentType;
    }

    public String getBirthCertNo() {
        return birthCertNo;
    }

    public void setBirthCertNo(String birthCertNo) {
        this.birthCertNo = birthCertNo;
    }

    public String getBirthCertNoName() {
        return birthCertNoName;
    }

    public void setBirthCertNoName(String birthCertNoName) {
        this.birthCertNoName = birthCertNoName;
    }

    public byte[] getBirthCertImage() {
        return birthCertImage;
    }

    public void setBirthCertImage(byte[] birthCertImage) {
        this.birthCertImage = birthCertImage;
    }

    public String getBirthCertImageContentType() {
        return birthCertImageContentType;
    }

    public void setBirthCertImageContentType(String birthCertImageContentType) {
        this.birthCertImageContentType = birthCertImageContentType;
    }

    public String getConPerName() {
        return conPerName;
    }

    public void setConPerName(String conPerName) {
        this.conPerName = conPerName;
    }

    public String getConPerMobile() {
        return conPerMobile;
    }

    public void setConPerMobile(String conPerMobile) {
        this.conPerMobile = conPerMobile;
    }

    public String getConPerAddress() {
        return conPerAddress;
    }

    public void setConPerAddress(String conPerAddress) {
        this.conPerAddress = conPerAddress;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getMpoAppStatus() {
        return mpoAppStatus;
    }

    public void setMpoAppStatus(Integer mpoAppStatus) {
        this.mpoAppStatus = mpoAppStatus;
    }

    public Integer getTimescaleAppStatus() {
        return timescaleAppStatus;
    }

    public void setTimescaleAppStatus(Integer timescaleAppStatus) {
        this.timescaleAppStatus = timescaleAppStatus;
    }

    public Integer getbEDAppStatus() {
        return bEDAppStatus;
    }

    public void setbEDAppStatus(Integer bEDAppStatus) {
        this.bEDAppStatus = bEDAppStatus;
    }

    public Integer getaPAppStatus() {
        return aPAppStatus;
    }

    public void setaPAppStatus(Integer aPAppStatus) {
        this.aPAppStatus = aPAppStatus;
    }

    public Integer getPrincipleAppStatus() {
        return principleAppStatus;
    }

    public void setPrincipleAppStatus(Integer principleAppStatus) {
        this.principleAppStatus = principleAppStatus;
    }

    public Integer getNameCnclAppStatus() {
        return nameCnclAppStatus;
    }

    public void setNameCnclAppStatus(Integer nameCnclAppStatus) {
        this.nameCnclAppStatus = nameCnclAppStatus;
    }

    public String getIndexNo() {
        return indexNo;
    }

    public void setIndexNo(String indexNo) {
        this.indexNo = indexNo;
    }

    public Double getWithheldAmount() {
        return withheldAmount;
    }

    public void setWithheldAmount(Double withheldAmount) {
        this.withheldAmount = withheldAmount;
    }

    public LocalDate getWithheldStartDate() {
        return withheldStartDate;
    }

    public void setWithheldStartDate(LocalDate withheldStartDate) {
        this.withheldStartDate = withheldStartDate;
    }

    public LocalDate getWithheldEndDate() {
        return withheldEndDate;
    }

    public void setWithheldEndDate(LocalDate withheldEndDate) {
        this.withheldEndDate = withheldEndDate;
    }

    public LocalDate getPayScaleChangedDate() {
        return payScaleChangedDate;
    }

    public void setPayScaleChangedDate(LocalDate payScaleChangedDate) {
        this.payScaleChangedDate = payScaleChangedDate;
    }

    public String getBnName() {
        return bnName;
    }

    public void setBnName(String bnName) {
        this.bnName = bnName;
    }

    public String getFatherNameBn() {
        return fatherNameBn;
    }

    public void setFatherNameBn(String fatherNameBn) {
        this.fatherNameBn = fatherNameBn;
    }

    public LocalDate getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(LocalDate joiningDate) {
        this.joiningDate = joiningDate;
    }

    public HrEmplTypeInfo getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(HrEmplTypeInfo employeeType) {
        this.employeeType = employeeType;
    }

    public Boolean getWithheldActive() {
        return withheldActive;
    }

    public void setWithheldActive(Boolean withheldActive) {
        this.withheldActive = withheldActive;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public PayScale getPayScale() {
        return payScale;
    }

    public void setPayScale(PayScale payScale) {
        this.payScale = payScale;
    }

//    public InstEmplDesignation getInstEmplDesignation() {
//        return instEmplDesignation;
//    }
//
//    public void setInstEmplDesignation(InstEmplDesignation instEmplDesignation) {
//        this.instEmplDesignation = instEmplDesignation;
//    }

    public HrDesignationSetup getDesignationSetup() {
        return designationSetup;
    }

    public void setDesignationSetup(HrDesignationSetup designationSetup) {
        this.designationSetup = designationSetup;
    }

//    public CourseSub getCourseSub() {
//        return courseSub;
//    }
//
//    public void setCourseSub(CourseSub courseSub) {
//        this.courseSub = courseSub;
//    }

    public Religion getReligion() {
        return religion;
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTypeOfPosition() {
        return typeOfPosition;
    }

    public void setTypeOfPosition(String typeOfPosition) {
        this.typeOfPosition = typeOfPosition;
    }

    public String getRegisteredCertificateSubject() {
        return registeredCertificateSubject;
    }

    public void setRegisteredCertificateSubject(String registeredCertificateSubject) {
        this.registeredCertificateSubject = registeredCertificateSubject;
    }

    public String getRegistrationExamYear() {
        return registrationExamYear;
    }

    public void setRegistrationExamYear(String registrationExamYear) {
        this.registrationExamYear = registrationExamYear;
    }

    public LocalDate getMpoActiveDate() {
        return mpoActiveDate;
    }

    public void setMpoActiveDate(LocalDate mpoActiveDate) {
        this.mpoActiveDate = mpoActiveDate;
    }


    public String getRegisteredCertificateNo() {
        return registeredCertificateNo;
    }

    public void setRegisteredCertificateNo(String registeredCertificateNo) {
        this.registeredCertificateNo = registeredCertificateNo;
    }

    public Boolean getMpoActive() {
        return mpoActive;
    }

    public void setMpoActive(Boolean mpoActive) {
        this.mpoActive = mpoActive;
    }

    public Boolean getIsJPAdmin() {
        return isJPAdmin;
    }

    public void setIsJPAdmin(Boolean isJPAdmin) {
        this.isJPAdmin = isJPAdmin;
    }
    public LocalDate getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(LocalDate applyDate) {
        this.applyDate = applyDate;
    }

    public IisCourseInfo getIisCourseInfo() {
        return iisCourseInfo;
    }

    public void setIisCourseInfo(IisCourseInfo iisCourseInfo) {
        this.iisCourseInfo = iisCourseInfo;
    }

    public InstLevel getInstLevel() {
        return instLevel;
    }

    public void setInstLevel(InstLevel instLevel) {
        this.instLevel = instLevel;
    }

//    public CmsSubAssign getCmsSubAssign() {
//        return cmsSubAssign;
//    }
//
//    public void setCmsSubAssign(CmsSubAssign cmsSubAssign) {
//        this.cmsSubAssign = cmsSubAssign;
//    }


    public Boolean getJPAdmin() {
        return isJPAdmin;
    }

    public void setJPAdmin(Boolean JPAdmin) {
        isJPAdmin = JPAdmin;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public LocalDate getResultPublishDate() {
        return resultPublishDate;
    }

    public void setResultPublishDate(LocalDate resultPublishDate) {
        this.resultPublishDate = resultPublishDate;
    }

    public String getPassingYear() {
        return passingYear;
    }

    public void setPassingYear(String passingYear) {
        this.passingYear = passingYear;
    }

    public String getOrdinalNo() {
        return ordinalNo;
    }

    public void setOrdinalNo(String ordinalNo) {
        this.ordinalNo = ordinalNo;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }


    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public HrDepartmentSetup getHrdepartmentSetup() {
        return hrdepartmentSetup;
    }

    public void setHrdepartmentSetup(HrDepartmentSetup hrdepartmentSetup) {
        this.hrdepartmentSetup = hrdepartmentSetup;
    }

    public String getIsHeadOfDept() {
        return isHeadOfDept;
    }

    public void setIsHeadOfDept(String isHeadOfDept) {
        this.isHeadOfDept = isHeadOfDept;
    }

    public CmsSubject getCmsSubject() {
        return cmsSubject;
    }

    public void setCmsSubject(CmsSubject cmsSubject) {
        this.cmsSubject = cmsSubject;
    }

    public CmsTrade getCmsTrade() {
        return cmsTrade;
    }

    public void setCmsTrade(CmsTrade cmsTrade) {
        this.cmsTrade = cmsTrade;
    }

    public CmsCurriculum getCmsCurriculum() {
        return cmsCurriculum;
    }

    public void setCmsCurriculum(CmsCurriculum cmsCurriculum) {
        this.cmsCurriculum = cmsCurriculum;
    }

    public MiscTypeSetup getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(MiscTypeSetup employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public MiscTypeSetup getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(MiscTypeSetup jobStatus) {
        this.jobStatus = jobStatus;
    }

    public LocalDate getAppointmentGoDate() {
        return appointmentGoDate;
    }

    public void setAppointmentGoDate(LocalDate appointmentGoDate) {
        this.appointmentGoDate = appointmentGoDate;
    }

    public LocalDate getPrlDate() {
        return prlDate;
    }

    public void setPrlDate(LocalDate prlDate) {
        this.prlDate = prlDate;
    }

    public LocalDate getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(LocalDate retirementDate) {
        this.retirementDate = retirementDate;
    }

    public MiscTypeSetup getEncadrement() {
        return encadrement;
    }

    public void setEncadrement(MiscTypeSetup encadrement) {
        this.encadrement = encadrement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InstEmployee instEmployee = (InstEmployee) o;

        if ( ! Objects.equals(id, instEmployee.id)) return false;

        return true;
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    @Override
    public String toString() {
        return "InstEmployee{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", bnName='" + bnName + '\'' +
            ", fatherNameBn='" + fatherNameBn + '\'' +
            ", motherNameBn='" + motherNameBn + '\'' +
            ", email='" + email + '\'' +
            ", contactNo='" + contactNo + '\'' +
            ", altContactNo='" + altContactNo + '\'' +
            ", fatherName='" + fatherName + '\'' +
            ", jobCategory='" + jobCategory + '\'' +
            ", motherName='" + motherName + '\'' +
            ", cosOfDell='" + cosOfDell + '\'' +
            ", employmentType='" + employmentType + '\'' +
            ", dob=" + dob +
            ", category=" + category +
            ", gender='" + gender + '\'' +
            ", maritalStatus=" + maritalStatus +
            ", bloodGroup=" + bloodGroup +
            ", tin='" + tin + '\'' +
            ", image=" + Arrays.toString(image) +
            ", imageName='" + imageName + '\'' +
            ", imageContentType='" + imageContentType + '\'' +
            ", nationality='" + nationality + '\'' +
            ", nid='" + nid + '\'' +
            ", nidImage=" + Arrays.toString(nidImage) +
            ", nidImageName='" + nidImageName + '\'' +
            ", nidImageContentType='" + nidImageContentType + '\'' +
            ", birthCertNo='" + birthCertNo + '\'' +
            ", birthCertNoName='" + birthCertNoName + '\'' +
            ", birthCertImage=" + Arrays.toString(birthCertImage) +
            ", birthCertImageContentType='" + birthCertImageContentType + '\'' +
            ", conPerName='" + conPerName + '\'' +
            ", conPerMobile='" + conPerMobile + '\'' +
            ", conPerAddress='" + conPerAddress + '\'' +
            ", code='" + code + '\'' +
            ", mpoAppStatus=" + mpoAppStatus +
            ", timescaleAppStatus=" + timescaleAppStatus +
            ", bEDAppStatus=" + bEDAppStatus +
            ", aPAppStatus=" + aPAppStatus +
            ", principleAppStatus=" + principleAppStatus +
            ", nameCnclAppStatus=" + nameCnclAppStatus +
            ", indexNo='" + indexNo + '\'' +
            ", withheldAmount=" + withheldAmount +
            ", withheldStartDate=" + withheldStartDate +
            ", withheldEndDate=" + withheldEndDate +
            ", payScaleChangedDate=" + payScaleChangedDate +
            ", withheldActive=" + withheldActive +
            ", institute=" + institute +
            ", user=" + user +
            ", level='" + level + '\'' +
            ", quota='" + quota + '\'' +
            ", applyDate=" + applyDate +
            ", mpoActiveDate=" + mpoActiveDate +
            ", religion=" + religion +
            ", status=" + status +
            ", entryStatus='" + entryStatus + '\'' +
            ", remarks='" + remarks + '\'' +
            ", typeOfPosition='" + typeOfPosition + '\'' +
            ", registeredCertificateSubject='" + registeredCertificateSubject + '\'' +
            ", rollNo='" + rollNo + '\'' +
            ", resultPublishDate=" + resultPublishDate +
            ", passingYear='" + passingYear + '\'' +
            ", ordinalNo='" + ordinalNo + '\'' +
            ", registrationExamYear='" + registrationExamYear + '\'' +
            ", joiningDate=" + joiningDate +
            ", registeredCertificateNo='" + registeredCertificateNo + '\'' +
            ", mpoActive=" + mpoActive +
            ", isJPAdmin=" + isJPAdmin +
            ", jobQuota=" + jobQuota +
            ", quotaCert=" + Arrays.toString(quotaCert) +
            ", quotaCertContentType='" + quotaCertContentType + '\'' +
            ", quotaCertName='" + quotaCertName + '\'' +
            ", iisCourseInfo=" + iisCourseInfo +
            ", instLevel=" + instLevel +
            ", district=" + district +
            ", payScale=" + payScale +
            ", designationSetup=" + designationSetup +
            ", employeeType=" + employeeType +
            ", createDate=" + createDate +
            ", createBy=" + createBy +
            ", updateDate=" + updateDate +
            ", updateBy=" + updateBy +
            ", isHeadOfDept='" + isHeadOfDept + '\'' +
            ", hrdepartmentSetup=" + hrdepartmentSetup +
            ", cmsCurriculum=" + cmsCurriculum +
            ", cmsTrade=" + cmsTrade +
            ", cmsSubject=" + cmsSubject +
            ", employmentStatus=" + employmentStatus +
            ", jobStatus=" + jobStatus +
            '}';
    }
}
