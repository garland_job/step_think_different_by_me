package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SecurityConfigurationTable.
 */
@Entity
@Table(name = "security_configuration_table")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "securityconfigurationtable")
public class SecurityConfigurationTable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_login_pwd_change")
    private Boolean firstLoginPwdChange;

    @Column(name = "max_inv_pwd_attempt")
    private Long maxInvPwdAttempt;

    @Column(name = "max_pwd_repatation")
    private Long maxPwdRepatation;

    @Column(name = "no_of_date_pwd_exp")
    private Long noOfDatePwdExp;

    @Column(name = "req_pwd_len")
    private Long reqPwdLen;

    @Column(name = "req_pwd_nalpha_char")
    private Long reqPwdNalphaChar;

    @Column(name = "pwd_nalpha_char")
    private String pwdNalphaChar;

    @Column(name = "user_name_length")
    private Long userNameLength;

    @Column(name = "usr_name_nalpha_allow")
    private Boolean userNameNalphaAllow;

    @Column(name = "usr_name_nalpha_char_allow")
    private Long userNameNalphaCharAllow;

    @Column(name = "usr_name_nalpha_char")
    private String userNameNalphaChar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getFirstLoginPwdChange() {
        return firstLoginPwdChange;
    }

    public void setFirstLoginPwdChange(Boolean firstLoginPwdChange) {
        this.firstLoginPwdChange = firstLoginPwdChange;
    }

    public Long getMaxInvPwdAttempt() {
        return maxInvPwdAttempt;
    }

    public void setMaxInvPwdAttempt(Long maxInvPwdAttempt) {
        this.maxInvPwdAttempt = maxInvPwdAttempt;
    }

    public Long getMaxPwdRepatation() {
        return maxPwdRepatation;
    }

    public void setMaxPwdRepatation(Long maxPwdRepatation) {
        this.maxPwdRepatation = maxPwdRepatation;
    }

    public Long getNoOfDatePwdExp() {
        return noOfDatePwdExp;
    }

    public void setNoOfDatePwdExp(Long noOfDatePwdExp) {
        this.noOfDatePwdExp = noOfDatePwdExp;
    }

    public Long getReqPwdLen() {
        return reqPwdLen;
    }

    public void setReqPwdLen(Long reqPwdLen) {
        this.reqPwdLen = reqPwdLen;
    }

    public Long getReqPwdNalphaChar() {
        return reqPwdNalphaChar;
    }

    public void setReqPwdNalphaChar(Long reqPwdNalphaChar) {
        this.reqPwdNalphaChar = reqPwdNalphaChar;
    }

    public String getPwdNalphaChar() {
        return pwdNalphaChar;
    }

    public void setPwdNalphaChar(String pwdNalphaChar) {
        this.pwdNalphaChar = pwdNalphaChar;
    }

    public Long getUserNameLength() {
        return userNameLength;
    }

    public void setUserNameLength(Long userNameLength) {
        this.userNameLength = userNameLength;
    }

    public Boolean getUserNameNalphaAllow() {
        return userNameNalphaAllow;
    }

    public void setUserNameNalphaAllow(Boolean userNameNalphaAllow) {
        this.userNameNalphaAllow = userNameNalphaAllow;
    }

    public Long getUserNameNalphaCharAllow() {
        return userNameNalphaCharAllow;
    }

    public void setUserNameNalphaCharAllow(Long userNameNalphaCharAllow) {
        this.userNameNalphaCharAllow = userNameNalphaCharAllow;
    }

    public String getUserNameNalphaChar() {
        return userNameNalphaChar;
    }

    public void setUserNameNalphaChar(String userNameNalphaChar) {
        this.userNameNalphaChar = userNameNalphaChar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityConfigurationTable securityConfigurationTable = (SecurityConfigurationTable) o;

        if ( ! Objects.equals(id, securityConfigurationTable.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SecurityConfigurationTable{" +
            "id=" + id +
            ", firstLoginPwdChange='" + firstLoginPwdChange + "'" +
            ", maxInvPwdAttempt='" + maxInvPwdAttempt + "'" +
            ", maxPwdRepatation='" + maxPwdRepatation + "'" +
            ", noOfDatePwdExp='" + noOfDatePwdExp + "'" +
            ", reqPwdLen='" + reqPwdLen + "'" +
            ", reqPwdNalphaChar='" + reqPwdNalphaChar + "'" +
            ", pwdNalphaChar='" + pwdNalphaChar + "'" +
            ", userNameLength='" + userNameLength + "'" +
            ", userNameNalphaAllow='" + userNameNalphaAllow + "'" +
            ", userNameNalphaCharAllow='" + userNameNalphaCharAllow + "'" +
            ", userNameNalphaChar='" + userNameNalphaChar + "'" +
            '}';
    }
}
