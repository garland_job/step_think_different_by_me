package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import gov.step.app.domain.enumeration.AuthorityStatus;

import gov.step.app.domain.enumeration.ErpAuthorityAction;

/**
 * A ErpAuthFlowProcess.
 */
@Entity
@Table(name = "erp_auth_flow_process")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "erpauthflowprocess")
public class ErpAuthFlowProcess implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "ref_domain_id", nullable = false)
    private Long refDomainId;

    @Column(name = "ref_domain_name")
    private String refDomainName;

    @Enumerated(EnumType.STRING)
    @Column(name = "authority_status")
    private AuthorityStatus authorityStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "erp_authority_action")
    private ErpAuthorityAction erpAuthorityAction;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date", nullable = false)
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JoinColumn(name = "erp_authority_flow_id")
    private ErpAuthorityFlow erpAuthorityFlow;

    @ManyToOne
    @JoinColumn(name = "authorised_employee_id")
    private HrEmployeeInfo authorizedEmployee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRefDomainId() {
        return refDomainId;
    }

    public void setRefDomainId(Long refDomainId) {
        this.refDomainId = refDomainId;
    }

    public String getRefDomainName() {
        return refDomainName;
    }

    public void setRefDomainName(String refDomainName) {
        this.refDomainName = refDomainName;
    }

    public AuthorityStatus getAuthorityStatus() {
        return authorityStatus;
    }

    public void setAuthorityStatus(AuthorityStatus authorityStatus) {
        this.authorityStatus = authorityStatus;
    }

    public ErpAuthorityAction getErpAuthorityAction() {
        return erpAuthorityAction;
    }

    public void setErpAuthorityAction(ErpAuthorityAction erpAuthorityAction) {
        this.erpAuthorityAction = erpAuthorityAction;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public ErpAuthorityFlow getErpAuthorityFlow() {
        return erpAuthorityFlow;
    }

    public void setErpAuthorityFlow(ErpAuthorityFlow ErpAuthorityFlow) {
        this.erpAuthorityFlow = ErpAuthorityFlow;
    }

    public HrEmployeeInfo getAuthorizedEmployee() {
        return authorizedEmployee;
    }

    public void setAuthorizedEmployee(HrEmployeeInfo HrEmployeeInfo) {
        this.authorizedEmployee = HrEmployeeInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ErpAuthFlowProcess erpAuthFlowProcess = (ErpAuthFlowProcess) o;

        if ( ! Objects.equals(id, erpAuthFlowProcess.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ErpAuthFlowProcess{" +
            "id=" + id +
            ", refDomainId='" + refDomainId + "'" +
            ", refDomainName='" + refDomainName + "'" +
            ", authorityStatus='" + authorityStatus + "'" +
            ", erpAuthorityAction='" + erpAuthorityAction + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
