package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PensionGratuityRateSetup.
 */
@Entity
@Table(name = "pgms_pen_gra_rate_setup")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pgmspengrsetup")
public class PensionGratuityRateSetup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "setup_type", nullable = false)
    private String setupType;

    @NotNull
    @Column(name = "working_year", nullable = false)
    private Long workingYear;

    @NotNull
    @Column(name = "rate_of_pen_gr", nullable = false)
    private Long rateOfPenGr;

    @NotNull
    @Column(name = "effective_date", nullable = false)
    private LocalDate effectiveDate;


    @Column(name = "setup_version")
    private Long setupVersion;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSetupType() {
        return setupType;
    }

    public void setSetupType(String setupType) {
        this.setupType = setupType;
    }

    public Long getWorkingYear() {
        return workingYear;
    }

    public void setWorkingYear(Long workingYear) {
        this.workingYear = workingYear;
    }

    public Long getRateOfPenGr() {
        return rateOfPenGr;
    }

    public void setRateOfPenGr(Long rateOfPenGr) {
        this.rateOfPenGr = rateOfPenGr;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Long getSetupVersion() {
        return setupVersion;
    }

    public void setSetupVersion(Long setupVersion) {
        this.setupVersion = setupVersion;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PensionGratuityRateSetup pensionGratuityRateSetup = (PensionGratuityRateSetup) o;

        if ( ! Objects.equals(id, pensionGratuityRateSetup.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PensionGratuityRateSetup{" +
            "id=" + id +
            ", setupType='" + setupType + "'" +
            ", effectiveDate='" + effectiveDate + "'" +
            ", setupVersion='" + setupVersion + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
