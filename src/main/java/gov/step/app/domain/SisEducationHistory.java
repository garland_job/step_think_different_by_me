package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import gov.step.app.domain.enumeration.AchievedCertificate;

/**
 * A SisEducationHistory.
 */
@Entity
@Table(name = "sis_education_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "siseducationhistory")
public class SisEducationHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SIS_EDUCATION_HISTORY_SEQ")
    @SequenceGenerator(name="SIS_EDUCATION_HISTORY_SEQ", sequenceName="SIS_EDUCATION_HISTORY_SEQ")
    private Long id;

    @NotNull
    @Column(name = "year_or_semester", nullable = false)
    private String yearOrSemester;

    @Column(name = "roll_no")
    private String rollNo;

    @NotNull
    @Column(name = "major_or_dept", nullable = false)
    private String majorOrDept;

    @NotNull
    @Column(name = "division_or_gpa", nullable = false)
    private String divisionOrGpa;

    @NotNull
    @Column(name = "passing_year", nullable = false)
    private String passingYear;

    @NotNull
    //@Enumerated(EnumType.STRING)
    @Column(name = "achieved_certificate", nullable = false)
    //private AchievedCertificate achievedCertificate;
    private String achievedCertificate;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @OneToOne
    @JoinColumn(name = "create_by")
    private User createBy;

    @Column(name = "update_date", nullable = false)
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "org_name")
    private String orgName;

    @Column(name = "uni_name")
    private String uniName;

    @ManyToOne
    @JoinColumn(name = "edu_level_id")
    private EduLevel eduLevel;

    @ManyToOne
    @JoinColumn(name = "edu_board_id")
    private EduBoard eduBoard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYearOrSemester() {
        return yearOrSemester;
    }

    public void setYearOrSemester(String yearOrSemester) {
        this.yearOrSemester = yearOrSemester;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getMajorOrDept() {
        return majorOrDept;
    }

    public void setMajorOrDept(String majorOrDept) {
        this.majorOrDept = majorOrDept;
    }

    public String getDivisionOrGpa() {
        return divisionOrGpa;
    }

    public void setDivisionOrGpa(String divisionOrGpa) {
        this.divisionOrGpa = divisionOrGpa;
    }

    public String getPassingYear() {
        return passingYear;
    }

    public void setPassingYear(String passingYear) {
        this.passingYear = passingYear;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }

    public String getAchievedCertificate() {
        return achievedCertificate;
    }

    public void setAchievedCertificate(String achievedCertificate) {
        this.achievedCertificate = achievedCertificate;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public EduLevel getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(EduLevel EduLevel) {
        this.eduLevel = EduLevel;
    }

    public EduBoard getEduBoard() {
        return eduBoard;
    }

    public void setEduBoard(EduBoard EduBoard) {
        this.eduBoard = EduBoard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SisEducationHistory sisEducationHistory = (SisEducationHistory) o;

        if ( ! Objects.equals(id, sisEducationHistory.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SisEducationHistory{" +
            "id=" + id +
            ", yearOrSemester='" + yearOrSemester + "'" +
            ", rollNo='" + rollNo + "'" +
            ", orgName='" + orgName + "'" +
            ", uniName='" + uniName + "'" +
            ", majorOrDept='" + majorOrDept + "'" +
            ", divisionOrGpa='" + divisionOrGpa + "'" +
            ", passingYear='" + passingYear + "'" +
            ", achievedCertificate='" + achievedCertificate + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
