package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A AssetTransfer.
 */
@Entity
@Table(name = "asset_transfer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "assettransfer")
public class AssetTransfer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "transfer_date")
    private LocalDate transferDate;

    //@NotNull
    @Size(max = 100)
    @Column(name = "transfer_code", length = 100)
    private String transferCode;

    @Column(name = "emp_to_dept")
    private String empToDept;

    @Column(name = "emp_from_dept")
    private String empFromDept;

    @Column(name = "asset_code")
    private String assetCode;

    @Column(name = "asset_name")
    private String assetName;

    @Column(name = "available_qty")
    private Long availableQty;

    @Column(name = "assign_qty")
    private Long assignQty;

    @Column(name = "employee_from")
    private String employeeFrom;

    @Column(name = "employee_to")
    private String employeeTo;

    @Column(name = "employee_id_from")
    private String employeeIdFrom;

    @Column(name = "employee_id_to")
    private String employeeIdTo;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransferCode() {
        return transferCode;
    }

    public void setTransferCode(String transferCode) {
        this.transferCode = transferCode;
    }

    public String getEmpToDept() {
        return empToDept;
    }

    public void setEmpToDept(String empToDept) {
        this.empToDept = empToDept;
    }

    public String getEmpFromDept() {
        return empFromDept;
    }

    public void setEmpFromDept(String empFromDept) {
        this.empFromDept = empFromDept;
    }

    public String getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(String assetCode) {
        this.assetCode = assetCode;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public Long getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(Long availableQty) {
        this.availableQty = availableQty;
    }

    public Long getAssignQty() {
        return assignQty;
    }

    public void setAssignQty(Long assignQty) {
        this.assignQty = assignQty;
    }

    public LocalDate getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(LocalDate transferDate) {
        this.transferDate = transferDate;
    }

    public String getEmployeeFrom() {
        return employeeFrom;
    }

    public void setEmployeeFrom(String employeeFrom) {
        this.employeeFrom = employeeFrom;
    }

    public String getEmployeeTo() {
        return employeeTo;
    }

    public void setEmployeeTo(String employeeTo) {
        this.employeeTo = employeeTo;
    }

    public String getEmployeeIdFrom() {
        return employeeIdFrom;
    }

    public void setEmployeeIdFrom(String employeeIdFrom) {
        this.employeeIdFrom = employeeIdFrom;
    }

    public String getEmployeeIdTo() {
        return employeeIdTo;
    }

    public void setEmployeeIdTo(String employeeIdTo) {
        this.employeeIdTo = employeeIdTo;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AssetTransfer assetTransfer = (AssetTransfer) o;

        if ( ! Objects.equals(id, assetTransfer.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AssetTransfer{" +
            "id=" + id +
            ", transferDate='" + transferDate + "'" +
            ", transferCode='" + transferCode + "'" +
            ", empToDept='" + empToDept + "'" +
            ", empFromDept='" + empFromDept + "'" +
            ", assetCode='" + assetCode + "'" +
            ", assetName='" + assetName + "'" +
            ", availableQty='" + availableQty + "'" +
            ", assignQty='" + assignQty + "'" +
            ", employeeFrom='" + employeeFrom + "'" +
            ", employeeTo='" + employeeTo + "'" +
            ", employeeIdFrom='" + employeeIdFrom + "'" +
            ", employeeIdTo='" + employeeIdTo + "'" +
            '}';
    }
}
