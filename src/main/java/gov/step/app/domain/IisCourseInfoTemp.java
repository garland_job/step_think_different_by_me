package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;

import org.hibernate.annotations.Nationalized;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A IisCourseInfoTemp.
 */
@Entity
@Table(name = "iis_course_info_temp")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "iiscourseinfotemp")
public class IisCourseInfoTemp implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "per_date_edu")
    private LocalDate perDateEdu;

    @Column(name = "per_date_bteb")
    private LocalDate perDateBteb;

    @Column(name = "mpo_enlisted")
    private Boolean mpoEnlisted;

    @Column(name = "date_mpo")
    private String dateMpo;

    @Column(name = "seat_no")
    private Integer seatNo;

    @Column(name = "shift")
    private String shift;

    @Column(name = "no_of_post_t")
    private String noOfPostT;

    @Column(name = "total_post_t")
    private String totalPostT;

    @Column(name = "no_of_post_staff")
    private String noOfPostStaff;

    @Column(name = "total_post_staff")
    private String totalPostStaff;


    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Lob
    @Column(name = "minis_memo_file")
    private byte[] minisMemoFile;


    @Column(name = "minis_memo_file_cnt_type")
    private String minisMemoFileCntType;

    @Column(name = "minis_memo_file_name")
    private String minisMemoFileName;

    @Lob
    @Column(name = "bteb_memo_file")
    private byte[] btebMemoFile;


    @Column(name = "bteb_memo_file_cnt_type")
    private String btebMemoFileCntType;

    @Column(name = "bteb_memo_file_name")
    private String btebMemoFileName;


    @Nationalized
    @Column(name = "memo_no")
    private String memoNo;

    @Nationalized
    @Column(name = "memo_no_ministry")
    private String memoNoMinistry;

    //status 0=pending, 1=rejected, 2=approved
    @Column(name = "status")
    private Integer status;

    @ManyToOne
    @JoinColumn(name = "iiscurriculuminfo_id")
    private IisCurriculumInfo iisCurriculumInfo;

    @ManyToOne
    @JoinColumn(name = "cmstrade_id")
    private CmsTrade cmsTrade;

    @ManyToOne
    @JoinColumn(name = "cmssubject_id")
    private CmsSubject cmsSubject;

    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Institute institute;

    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;

    @ManyToOne
    @JoinColumn(name = "update_by")
    private User updateBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPerDateEdu() {
        return perDateEdu;
    }

    public void setPerDateEdu(LocalDate perDateEdu) {
        this.perDateEdu = perDateEdu;
    }

    public LocalDate getPerDateBteb() {
        return perDateBteb;
    }

    public void setPerDateBteb(LocalDate perDateBteb) {
        this.perDateBteb = perDateBteb;
    }

    public Boolean getMpoEnlisted() {
        return mpoEnlisted;
    }

    public void setMpoEnlisted(Boolean mpoEnlisted) {
        this.mpoEnlisted = mpoEnlisted;
    }

    public String getDateMpo() {
        return dateMpo;
    }

    public void setDateMpo(String dateMpo) {
        this.dateMpo = dateMpo;
    }

    public Integer getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(Integer seatNo) {
        this.seatNo = seatNo;
    }

    public byte[] getMinisMemoFile() {
        return minisMemoFile;
    }

    public void setMinisMemoFile(byte[] minisMemoFile) {
        this.minisMemoFile = minisMemoFile;
    }

    public String getMinisMemoFileCntType() {
        return minisMemoFileCntType;
    }

    public void setMinisMemoFileCntType(String minisMemoFileCntType) {
        this.minisMemoFileCntType = minisMemoFileCntType;
    }

    public String getMinisMemoFileName() {
        return minisMemoFileName;
    }

    public void setMinisMemoFileName(String minisMemoFileName) {
        this.minisMemoFileName = minisMemoFileName;
    }

    public byte[] getBtebMemoFile() {
        return btebMemoFile;
    }

    public void setBtebMemoFile(byte[] btebMemoFile) {
        this.btebMemoFile = btebMemoFile;
    }

    public String getBtebMemoFileCntType() {
        return btebMemoFileCntType;
    }

    public void setBtebMemoFileCntType(String btebMemoFileCntType) {
        this.btebMemoFileCntType = btebMemoFileCntType;
    }

    public String getBtebMemoFileName() {
        return btebMemoFileName;
    }

    public void setBtebMemoFileName(String btebMemoFileName) {
        this.btebMemoFileName = btebMemoFileName;
    }

    public String getNoOfPostT() {
        return noOfPostT;
    }

    public void setNoOfPostT(String noOfPostT) {
        this.noOfPostT = noOfPostT;
    }

    public String getNoOfPostStaff() {
        return noOfPostStaff;
    }

    public void setNoOfPostStaff(String noOfPostStaff) {
        this.noOfPostStaff = noOfPostStaff;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }


    public String getMemoNo() {
        return memoNo;
    }

    public void setMemoNo(String memoNo) {
        this.memoNo = memoNo;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public IisCurriculumInfo getIisCurriculumInfo() {
        return iisCurriculumInfo;
    }

    public void setIisCurriculumInfo(IisCurriculumInfo iisCurriculumInfo) {
        this.iisCurriculumInfo = iisCurriculumInfo;
    }

    public CmsTrade getCmsTrade() {
        return cmsTrade;
    }

    public void setCmsTrade(CmsTrade cmsTrade) {
        this.cmsTrade = cmsTrade;
    }

    public CmsSubject getCmsSubject() {
        return cmsSubject;
    }

    public void setCmsSubject(CmsSubject cmsSubject) {
        this.cmsSubject = cmsSubject;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User user) {
        this.createBy = user;
    }

    public String getTotalPostT() {
        return totalPostT;
    }

    public void setTotalPostT(String totalPostT) {
        this.totalPostT = totalPostT;
    }

    public String getTotalPostStaff() {
        return totalPostStaff;
    }

    public void setTotalPostStaff(String totalPostStaff) {
        this.totalPostStaff = totalPostStaff;
    }

    public String getMemoNoMinistry() {
        return memoNoMinistry;
    }

    public void setMemoNoMinistry(String memoNoMinistry) {
        this.memoNoMinistry = memoNoMinistry;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User user) {
        this.updateBy = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IisCourseInfoTemp iisCourseInfoTemp = (IisCourseInfoTemp) o;

        if ( ! Objects.equals(id, iisCourseInfoTemp.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "IisCourseInfoTemp{" +
            "id=" + id +
            ", perDateEdu='" + perDateEdu + "'" +
            ", perDateBteb='" + perDateBteb + "'" +
            ", mpoEnlisted='" + mpoEnlisted + "'" +
            ", dateMpo='" + dateMpo + "'" +
            ", seatNo='" + seatNo + "'" +
            ", shift='" + shift + "'" +
            ", createDate='" + createDate + "'" +
            ", updateDate='" + updateDate + "'" +
            '}';
    }
}
