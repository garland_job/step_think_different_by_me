package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A AssetAuctionInformation.
 */
@Entity
@Table(name = "asset_auction_information")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "assetauctioninformation")
public class AssetAuctionInformation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "last_repair_date")
    private LocalDate lastRepairDate;

    @Column(name = "is_auction_before")
    private Long isAuctionBefore;


    @Column(name = "ref_no")
    private String refNo;

    @ManyToOne
    private AssetRecord assetCode;

    @Column(name = "create_date")
    private LocalDate createDate;

    @OneToOne
    @JoinColumn(name = "create_by")
    private User createBy;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @ManyToOne
    @JoinColumn(name = "asset_distribution_id")
    private AssetDistribution assetDistribution;

    public AssetRecord getAssetCode() {
        return assetCode;
    }

    public void setAssetCode(AssetRecord assetCode) {
        this.assetCode = assetCode;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getLastRepairDate() {
        return lastRepairDate;
    }

    public void setLastRepairDate(LocalDate lastRepairDate) {
        this.lastRepairDate = lastRepairDate;
    }

    public AssetDistribution getAssetDistribution() {
        return assetDistribution;
    }

    public void setAssetDistribution(AssetDistribution assetDistribution) {
        this.assetDistribution = assetDistribution;
    }

    public Long getIsAuctionBefore() {
        return isAuctionBefore;
    }

    public void setIsAuctionBefore(Long isAuctionBefore) {
        this.isAuctionBefore = isAuctionBefore;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AssetAuctionInformation assetAuctionInformation = (AssetAuctionInformation) o;

        if ( ! Objects.equals(id, assetAuctionInformation.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AssetAuctionInformation{" +
            "id=" + id +
            ", lastRepairDate='" + lastRepairDate + "'" +
            ", isAuctionBefore='" + isAuctionBefore + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateBy='" + updateBy + "'" +
            ", updateDate='" + updateDate + "'" +
            '}';




    }
}
