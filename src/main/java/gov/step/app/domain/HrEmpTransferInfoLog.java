package gov.step.app.domain;

import gov.step.app.domain.enumeration.designationType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A HrEmpTransferInfoLog.
 */
@Entity
@Table(name = "hr_emp_transfer_info_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "hremptransferinfolog")
public class HrEmpTransferInfoLog implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="HR_EMP_TRANSFER_INFO_LOG_SEQ")
    @SequenceGenerator(name="HR_EMP_TRANSFER_INFO_LOG_SEQ", sequenceName="HR_EMP_TRANSFER_INFO_LOG_SEQ")
    private Long id;

    @Column(name = "from_organization_type")
    private String fromOrganizationType;

    @ManyToOne
    @JoinColumn(name = "from_inst_category_id")
    private InstCategory fromInstCategory;

    @ManyToOne
    @JoinColumn(name = "from_inst_level_id")
    private InstLevel fromInstLevel;

    @ManyToOne
    @JoinColumn(name = "from_institute_id")
    private Institute fromInstitute;

    @ManyToOne
    @JoinColumn(name = "from_work_area_id")
    private MiscTypeSetup fromOrgCategory;

    @ManyToOne
    @JoinColumn(name = "from_work_area_dtl_id")
    private HrEmpWorkAreaDtlInfo fromOrganization;

    @ManyToOne
    @JoinColumn(name = "from_department_info_id")
    private HrDepartmentSetup fromDepartmentInfo;

    @ManyToOne
    @JoinColumn(name = "from_designation_info_id")
    private HrDesignationSetup fromDesignationInfo;

    @Enumerated(EnumType.STRING)
    @Column(name = "from_employee_type")
    private designationType fromEmployeeType;

    @Column(name = "to_organization_type")
    private String toOrganizationType;

    @ManyToOne
    @JoinColumn(name = "to_inst_category_id")
    private InstCategory toInstCategory;

    @ManyToOne
    @JoinColumn(name = "to_institute_id")
    private Institute toInstitute;

    @ManyToOne
    @JoinColumn(name = "to_inst_level_id")
    private InstLevel toInstLevel;
    @ManyToOne
    @JoinColumn(name = "to_work_area_id")
    private MiscTypeSetup toOrgCategory;

    @ManyToOne
    @JoinColumn(name = "to_work_area_dtl_id")
    private HrEmpWorkAreaDtlInfo toOrganization;

    @ManyToOne
    @JoinColumn(name = "to_department_info_id")
    private HrDepartmentSetup toDepartmentInfo;

    @ManyToOne
    @JoinColumn(name = "to_designation_info_id")
    private HrDesignationSetup toDesignationInfo;

    @Enumerated(EnumType.STRING)
    @Column(name = "to_employee_type")
    private designationType toEmployeeType;

    @Column(name = "transfer_category")
    private String transferCategory;

    @Column(name = "office_order_number")
    private String officeOrderNumber;

    @Column(name = "office_order_date")
    private LocalDate officeOrderDate;

    @Column(name = "date_of_joining")
    private LocalDate dateOfJoining;

    @Column(name = "job_release_date")
    private LocalDate jobReleaseDate;

    @Lob
    @Column(name = "release_letter_doc")
    private byte[] releaseLetterDoc;

    @Column(name = "release_letter_doc_name")
    private String releaseLetterDocName;

    @Column(name = "release_letter_content_type")
    private String releaseLetterContentType;

    @Column(name = "transfer_comments")
    private String transferComments;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "log_status")
    private Long logStatus;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "action_date")
    private LocalDate actionDate;

    @Column(name = "action_by")
    private Long actionBy;

    @Column(name = "action_comments")
    private String actionComments;

    @ManyToOne
    @JoinColumn(name = "employee_info_id")
    private HrEmployeeInfo employeeInfo;

    @ManyToOne
    @JoinColumn(name = "transfer_type_id")
    private MiscTypeSetup transferType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Long logStatus) {
        this.logStatus = logStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDate actionDate) {
        this.actionDate = actionDate;
    }

    public Long getActionBy() {
        return actionBy;
    }

    public void setActionBy(Long actionBy) {
        this.actionBy = actionBy;
    }

    public String getActionComments() {
        return actionComments;
    }

    public void setActionComments(String actionComments) {
        this.actionComments = actionComments;
    }

    public HrEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.employeeInfo = HrEmployeeInfo;
    }

    public MiscTypeSetup getTransferType() {
        return transferType;
    }

    public void setTransferType(MiscTypeSetup MiscTypeSetup) {
        this.transferType = MiscTypeSetup;
    }

    public String getFromOrganizationType() {return fromOrganizationType;}

    public void setFromOrganizationType(String fromOrganizationType) {this.fromOrganizationType = fromOrganizationType;}

    public InstCategory getFromInstCategory() {return fromInstCategory;}

    public void setFromInstCategory(InstCategory fromInstCategory) {this.fromInstCategory = fromInstCategory;}

    public Institute getFromInstitute() {return fromInstitute;}

    public void setFromInstitute(Institute fromInstitute) {this.fromInstitute = fromInstitute;}

    public MiscTypeSetup getFromOrgCategory() {return fromOrgCategory;}

    public void setFromOrgCategory(MiscTypeSetup fromOrgCategory) {this.fromOrgCategory = fromOrgCategory;}

    public HrEmpWorkAreaDtlInfo getFromOrganization() {return fromOrganization;}

    public void setFromOrganization(HrEmpWorkAreaDtlInfo fromOrganization) {this.fromOrganization = fromOrganization;}

    public HrDepartmentSetup getFromDepartmentInfo() {return fromDepartmentInfo;}

    public void setFromDepartmentInfo(HrDepartmentSetup fromDepartmentInfo) {this.fromDepartmentInfo = fromDepartmentInfo;}

    public HrDesignationSetup getFromDesignationInfo() {return fromDesignationInfo;}

    public void setFromDesignationInfo(HrDesignationSetup fromDesignationInfo) {this.fromDesignationInfo = fromDesignationInfo;}

    public designationType getFromEmployeeType() {return fromEmployeeType;}

    public void setFromEmployeeType(designationType fromEmployeeType) {this.fromEmployeeType = fromEmployeeType;}

    public String getToOrganizationType() {return toOrganizationType;}

    public void setToOrganizationType(String toOrganizationType) {this.toOrganizationType = toOrganizationType;}

    public InstCategory getToInstCategory() {return toInstCategory;}

    public void setToInstCategory(InstCategory toInstCategory) {this.toInstCategory = toInstCategory;}

    public Institute getToInstitute() {return toInstitute;}

    public void setToInstitute(Institute toInstitute) {this.toInstitute = toInstitute;}

    public MiscTypeSetup getToOrgCategory() {return toOrgCategory;}

    public void setToOrgCategory(MiscTypeSetup toOrgCategory) {this.toOrgCategory = toOrgCategory;}

    public HrEmpWorkAreaDtlInfo getToOrganization() {return toOrganization;}

    public void setToOrganization(HrEmpWorkAreaDtlInfo toOrganization) {this.toOrganization = toOrganization;}

    public HrDepartmentSetup getToDepartmentInfo() {return toDepartmentInfo;}

    public void setToDepartmentInfo(HrDepartmentSetup toDepartmentInfo) {this.toDepartmentInfo = toDepartmentInfo;}

    public HrDesignationSetup getToDesignationInfo() {return toDesignationInfo;}

    public void setToDesignationInfo(HrDesignationSetup toDesignationInfo) {this.toDesignationInfo = toDesignationInfo;}

    public designationType getToEmployeeType() {return toEmployeeType;}

    public void setToEmployeeType(designationType toEmployeeType) {this.toEmployeeType = toEmployeeType;}

    public String getTransferCategory() {return transferCategory;}

    public void setTransferCategory(String transferCategory) {this.transferCategory = transferCategory;}

    public String getOfficeOrderNumber() {return officeOrderNumber;}

    public void setOfficeOrderNumber(String officeOrderNumber) {this.officeOrderNumber = officeOrderNumber;}

    public LocalDate getOfficeOrderDate() {return officeOrderDate;}

    public void setOfficeOrderDate(LocalDate officeOrderDate) {this.officeOrderDate = officeOrderDate;}

    public LocalDate getDateOfJoining() {return dateOfJoining;}

    public void setDateOfJoining(LocalDate dateOfJoining) {this.dateOfJoining = dateOfJoining;}

    public LocalDate getJobReleaseDate() {return jobReleaseDate;}

    public void setJobReleaseDate(LocalDate jobReleaseDate) {this.jobReleaseDate = jobReleaseDate;}

    public byte[] getReleaseLetterDoc() {return releaseLetterDoc;}

    public void setReleaseLetterDoc(byte[] releaseLetterDoc) {this.releaseLetterDoc = releaseLetterDoc;}

    public String getReleaseLetterDocName() {return releaseLetterDocName;}

    public void setReleaseLetterDocName(String releaseLetterDocName) {this.releaseLetterDocName = releaseLetterDocName;}

    public String getReleaseLetterContentType() {return releaseLetterContentType;}

    public void setReleaseLetterContentType(String releaseLetterContentType) {this.releaseLetterContentType = releaseLetterContentType;}

    public String getTransferComments() {return transferComments;}

    public void setTransferComments(String transferComments) {this.transferComments = transferComments;}


    public InstLevel getFromInstLevel() {
        return fromInstLevel;
    }

    public void setFromInstLevel(InstLevel fromInstLevel) {
        this.fromInstLevel = fromInstLevel;
    }

    public InstLevel getToInstLevel() {
        return toInstLevel;
    }

    public void setToInstLevel(InstLevel toInstLevel) {
        this.toInstLevel = toInstLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HrEmpTransferInfoLog hrEmpTransferInfoLog = (HrEmpTransferInfoLog) o;
        return Objects.equals(id, hrEmpTransferInfoLog.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HrEmpTransferInfoLog{" +
            "id=" + id +

            ", activeStatus='" + activeStatus + "'" +
            ", parentId='" + parentId + "'" +
            ", logStatus='" + logStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", actionDate='" + actionDate + "'" +
            ", actionBy='" + actionBy + "'" +
            ", actionComments='" + actionComments + "'" +
            '}';
    }
}
