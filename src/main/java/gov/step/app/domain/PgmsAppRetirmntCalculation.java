package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PgmsAppRetirmntCalculation.
 */
@Entity
@Table(name = "pgms_app_retirmnt_calculation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pgmsappretirmntcalculation")
public class PgmsAppRetirmntCalculation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "surrender_type", nullable = false)
    private String surrenderType;

    @NotNull
    @Column(name = "rate_of_pension", nullable = false)
    private Long rateOfPension;

    @NotNull
    @Column(name = "total_pen_or_gr", nullable = false)
    private Long totalPenOrGr;

    @Column(name = "first_half_of_pen")
    private Long firstHalfOfPen;

    @NotNull
    @Column(name = "exchange_retirmnt_al", nullable = false)
    private Long exchangeRetirmntAl;

    @NotNull
    @Column(name = "deserved_retirmnt_al", nullable = false)
    private Long deservedRetirmntAl;

    @NotNull
    @Column(name = "monthly_retirmnt_al", nullable = false)
    private Long monthlyRetirmntAl;

    @Column(name = "due_amount_info")
    private Long dueAmountInfo;

//    @NotNull
//    @Column(name = "remaining_total", nullable = false)
//    private Long remainingTotal;

//    @ManyToOne
//    @JoinColumn(name = "last_pay_code_elpc_info_id")
    @Column(name = "last_pay_code_elpc_info_id")
    private Long lastPayCodeElpcInfo;

    @ManyToOne
    @JoinColumn(name = "app_retirmnt_pen_info_id")
    private PgmsAppRetirmntPen appRetirmntPenInfo;

//    private Long rateOfExForSecondHalf;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurrenderType() {
        return surrenderType;
    }

    public void setSurrenderType(String surrenderType) {
        this.surrenderType = surrenderType;
    }

    public Long getRateOfPension() {
        return rateOfPension;
    }

    public void setRateOfPension(Long rateOfPension) {
        this.rateOfPension = rateOfPension;
    }

    public Long getTotalPenOrGr() {
        return totalPenOrGr;
    }

    public void setTotalPenOrGr(Long totalPenOrGr) {
        this.totalPenOrGr = totalPenOrGr;
    }

    public Long getFirstHalfOfPen() {
        return firstHalfOfPen;
    }

    public void setFirstHalfOfPen(Long firstHalfOfPen) {
        this.firstHalfOfPen = firstHalfOfPen;
    }

    public Long getExchangeRetirmntAl() {
        return exchangeRetirmntAl;
    }

    public void setExchangeRetirmntAl(Long exchangeRetirmntAl) {
        this.exchangeRetirmntAl = exchangeRetirmntAl;
    }

    public Long getDeservedRetirmntAl() {
        return deservedRetirmntAl;
    }

    public void setDeservedRetirmntAl(Long deservedRetirmntAl) {
        this.deservedRetirmntAl = deservedRetirmntAl;
    }

    public Long getMonthlyRetirmntAl() {
        return monthlyRetirmntAl;
    }

    public void setMonthlyRetirmntAl(Long monthlyRetirmntAl) {
        this.monthlyRetirmntAl = monthlyRetirmntAl;
    }

    public Long getDueAmountInfo() {
        return dueAmountInfo;
    }

    public void setDueAmountInfo(Long dueAmountInfo) {
        this.dueAmountInfo = dueAmountInfo;
    }

//    public Long getRemainingTotal() {
//        return remainingTotal;
//    }
//
//    public void setRemainingTotal(Long remainingTotal) {
//        this.remainingTotal = remainingTotal;
//    }

    public Long getLastPayCodeElpcInfo() {
        return lastPayCodeElpcInfo;
    }

    public void setLastPayCodeElpcInfo(Long lastPayCodeElpcInfo) {
        this.lastPayCodeElpcInfo = lastPayCodeElpcInfo;
    }

    public PgmsAppRetirmntPen getAppRetirmntPenInfo() {
        return appRetirmntPenInfo;
    }

    public void setAppRetirmntPenInfo(PgmsAppRetirmntPen PgmsAppRetirmntPen) {
        this.appRetirmntPenInfo = PgmsAppRetirmntPen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PgmsAppRetirmntCalculation pgmsAppRetirmntCalculation = (PgmsAppRetirmntCalculation) o;

        if ( ! Objects.equals(id, pgmsAppRetirmntCalculation.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PgmsAppRetirmntCalculation{" +
            "id=" + id +
            ", surrenderType='" + surrenderType + "'" +
            ", rateOfPension='" + rateOfPension + "'" +
            ", totalPenOrGr='" + totalPenOrGr + "'" +
            ", firstHalfOfPen='" + firstHalfOfPen + "'" +
            ", exchangeRetirmntAl='" + exchangeRetirmntAl + "'" +
            ", deservedRetirmntAl='" + deservedRetirmntAl + "'" +
            ", monthlyRetirmntAl='" + monthlyRetirmntAl + "'" +
            ", dueAmountInfo='" + dueAmountInfo + "'" +
//            ", remainingTotal='" + remainingTotal + "'" +
            '}';
    }
}
