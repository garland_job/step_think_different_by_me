package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import gov.step.app.domain.enumeration.AuthorityStatus;

import gov.step.app.domain.enumeration.ErpAuthorityAction;

/**
 * A PfmsLoanAppFlow.
 */
@Entity
@Table(name = "pfms_loan_app_flow")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pfmsloanappflow")
public class PfmsLoanAppFlow implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "authority_status")
    private AuthorityStatus authorityStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "erp_authority_action")
    private ErpAuthorityAction erpAuthorityAction;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JoinColumn(name = "erp_authority_flow_id")
    private ErpAuthorityFlow erpAuthorityFlow;

    @ManyToOne
    @JoinColumn(name = "new_authority_flow_id")
    private ErpAuthorityFlow newAuthorityFlow;

    @ManyToOne
    @JoinColumn(name = "hr_employee_info_id")
    private HrEmployeeInfo hrEmployeeInfo;

    @ManyToOne
    @JoinColumn(name = "pfms_loan_app_id")
    private PfmsLoanApplication pfmsLoanApp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthorityStatus getAuthorityStatus() {
        return authorityStatus;
    }

    public void setAuthorityStatus(AuthorityStatus authorityStatus) {
        this.authorityStatus = authorityStatus;
    }

    public ErpAuthorityAction getErpAuthorityAction() {
        return erpAuthorityAction;
    }

    public void setErpAuthorityAction(ErpAuthorityAction erpAuthorityAction) {
        this.erpAuthorityAction = erpAuthorityAction;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public ErpAuthorityFlow getErpAuthorityFlow() {
        return erpAuthorityFlow;
    }

    public void setErpAuthorityFlow(ErpAuthorityFlow ErpAuthorityFlow) {
        this.erpAuthorityFlow = ErpAuthorityFlow;
    }

    public ErpAuthorityFlow getNewAuthorityFlow() {
        return newAuthorityFlow;
    }

    public void setNewAuthorityFlow(ErpAuthorityFlow newAuthorityFlow) {
        this.newAuthorityFlow = newAuthorityFlow;
    }

    public HrEmployeeInfo getHrEmployeeInfo() {
        return hrEmployeeInfo;
    }

    public void setHrEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.hrEmployeeInfo = HrEmployeeInfo;
    }

    public PfmsLoanApplication getPfmsLoanApp() {
        return pfmsLoanApp;
    }

    public void setPfmsLoanApp(PfmsLoanApplication PfmsLoanApplication) {
        this.pfmsLoanApp = PfmsLoanApplication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PfmsLoanAppFlow pfmsLoanAppFlow = (PfmsLoanAppFlow) o;

        if ( ! Objects.equals(id, pfmsLoanAppFlow.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PfmsLoanAppFlow{" +
            "id=" + id +
            ", authorityStatus='" + authorityStatus + "'" +
            ", erpAuthorityAction='" + erpAuthorityAction + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
