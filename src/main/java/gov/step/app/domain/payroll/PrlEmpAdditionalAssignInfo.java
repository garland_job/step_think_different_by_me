package gov.step.app.domain.payroll;

import gov.step.app.domain.HrEmployeeInfo;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A PrlEmpAdditionalAssignInfo.
 */
@Entity
@Table(name = "prl_emp_additional_assign_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "prlempadditionalassigninfo")
public class PrlEmpAdditionalAssignInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "effected_date_from")
    private LocalDate effectedDateFrom;

    @Column(name = "effected_date_to")
    private LocalDate effectedDateTo;

    @Column(name = "allowance_amount", precision=10, scale=2)
    private BigDecimal allowanceAmount;

    @NotNull
    @Column(name = "active_status", nullable = false)
    private Boolean activeStatus;

    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date", nullable = false)
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JoinColumn(name = "employee_info_id")
    private HrEmployeeInfo employeeInfo;

    @ManyToOne
    @JoinColumn(name = "allowance_info_id")
    private PrlAllowDeductInfo allowanceInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getEffectedDateFrom() {
        return effectedDateFrom;
    }

    public void setEffectedDateFrom(LocalDate effectedDateFrom) {
        this.effectedDateFrom = effectedDateFrom;
    }

    public LocalDate getEffectedDateTo() {
        return effectedDateTo;
    }

    public void setEffectedDateTo(LocalDate effectedDateTo) {
        this.effectedDateTo = effectedDateTo;
    }

    public BigDecimal getAllowanceAmount() {
        return allowanceAmount;
    }

    public void setAllowanceAmount(BigDecimal allowanceAmount) {
        this.allowanceAmount = allowanceAmount;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public HrEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.employeeInfo = HrEmployeeInfo;
    }

    public PrlAllowDeductInfo getAllowanceInfo() {
        return allowanceInfo;
    }

    public void setAllowanceInfo(PrlAllowDeductInfo PrlAllowDeductInfo) {
        this.allowanceInfo = PrlAllowDeductInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PrlEmpAdditionalAssignInfo prlEmpAdditionalAssignInfo = (PrlEmpAdditionalAssignInfo) o;

        if ( ! Objects.equals(id, prlEmpAdditionalAssignInfo.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PrlEmpAdditionalAssignInfo{" +
            "id=" + id +
            ", effectedDateFrom='" + effectedDateFrom + "'" +
            ", effectedDateTo='" + effectedDateTo + "'" +
            ", allowanceAmount='" + allowanceAmount + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
