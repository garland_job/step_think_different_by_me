package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;

import org.hibernate.annotations.Nationalized;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A IisCurriculumInfo.
 */
@Entity
@Table(name = "iis_curriculum_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "iiscurriculuminfo")
public class IisCurriculumInfo implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="iis_curriculum_info_seq")
    @SequenceGenerator(name="iis_curriculum_info_seq", sequenceName="iis_curriculum_info_seq")
    private Long id;

    @NotNull
    @Column(name = "first_date", nullable = false)
    private LocalDate firstDate;

    @NotNull
    @Column(name = "last_date", nullable = false)
    private LocalDate lastDate;

    @Column(name = "mpo_enlisted")
    private Boolean mpoEnlisted;

    @Nationalized
    @Column(name = "rec_no")
    private String recNo;

    @Column(name = "ac_session")
    private String acSession;

    @Column(name = "mpo_date")
    private LocalDate mpoDate;

    @Nationalized
    @Column(name = "last_rec_no")
    private String lastRecNo;

    @Column(name = "last_mpo_date")
    private LocalDate lastMpoDate;

    //status 0=pending, 1=rejected, 2=approved
    @Column(name = "status")
    private Integer status;

    @Column(name = "cur_id")
    private String curId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;


    @Column(name = "update_date")
    private LocalDate updateDate;


    @ManyToOne
    @JoinColumn(name = "update_by")
    private User updateBy;

    @Lob
    @Column(name = "last_acdr_attach")
    private byte[] lastAcdrAttach;


    @Column(name = "last_acdr_attach_cnt_type")
    private String lastAcdrAttachCntType;

    @Column(name = "last_acdr_attach_name")
    private String lastAcdrAttachName;

    @Lob
    @Column(name = "first_acdr_attach")
    private byte[] firstAcdrAttach;


    @Column(name = "first_acdr_attach_cnt_type")
    private String firstAcdrAttachCntType;

    @Column(name = "first_acdr_attach_name")
    private String firstAcdrAttachName;


    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Institute institute;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "cms_curriculum_id", nullable = false)
    private CmsCurriculum cmsCurriculum;

    @Column(name = "vacancy_assigned")
    private Boolean vacancyAssigned;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(LocalDate firstDate) {
        this.firstDate = firstDate;
    }

    public LocalDate getLastDate() {
        return lastDate;
    }

    public void setLastDate(LocalDate lastDate) {
        this.lastDate = lastDate;
    }

    public Boolean getMpoEnlisted() {
        return mpoEnlisted;
    }

    public void setMpoEnlisted(Boolean mpoEnlisted) {
        this.mpoEnlisted = mpoEnlisted;
    }

    public String getRecNo() {
        return recNo;
    }

    public void setRecNo(String recNo) {
        this.recNo = recNo;
    }

    public String getCurId() {
        return curId;
    }

    public void setCurId(String curId) {
        this.curId = curId;
    }

    public LocalDate getMpoDate() {
        return mpoDate;
    }

    public void setMpoDate(LocalDate mpoDate) {
        this.mpoDate = mpoDate;
    }

    public Integer getStatus() {
        return status;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getAcSession() {
        return acSession;
    }

    public void setAcSession(String acSession) {
        this.acSession = acSession;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public byte[] getLastAcdrAttach() {
        return lastAcdrAttach;
    }

    public void setLastAcdrAttach(byte[] lastAcdrAttach) {
        this.lastAcdrAttach = lastAcdrAttach;
    }

    public String getLastAcdrAttachCntType() {
        return lastAcdrAttachCntType;
    }

    public void setLastAcdrAttachCntType(String lastAcdrAttachCntType) {
        this.lastAcdrAttachCntType = lastAcdrAttachCntType;
    }

    public String getLastAcdrAttachName() {
        return lastAcdrAttachName;
    }

    public void setLastAcdrAttachName(String lastAcdrAttachName) {
        this.lastAcdrAttachName = lastAcdrAttachName;
    }

    public byte[] getFirstAcdrAttach() {
        return firstAcdrAttach;
    }

    public void setFirstAcdrAttach(byte[] firstAcdrAttach) {
        this.firstAcdrAttach = firstAcdrAttach;
    }

    public String getFirstAcdrAttachCntType() {
        return firstAcdrAttachCntType;
    }

    public void setFirstAcdrAttachCntType(String firstAcdrAttachCntType) {
        this.firstAcdrAttachCntType = firstAcdrAttachCntType;
    }

    public String getFirstAcdrAttachName() {
        return firstAcdrAttachName;
    }

    public void setFirstAcdrAttachName(String firstAcdrAttachName) {
        this.firstAcdrAttachName = firstAcdrAttachName;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    public CmsCurriculum getCmsCurriculum() {
        return cmsCurriculum;
    }

    public void setCmsCurriculum(CmsCurriculum cmsCurriculum) {
        this.cmsCurriculum = cmsCurriculum;
    }

    public String getLastRecNo() {
        return lastRecNo;
    }

    public void setLastRecNo(String lastRecNo) {
        this.lastRecNo = lastRecNo;
    }

    public LocalDate getLastMpoDate() {
        return lastMpoDate;
    }

    public void setLastMpoDate(LocalDate lastMpoDate) {
        this.lastMpoDate = lastMpoDate;
    }

    public Boolean getVacancyAssigned() {
        return vacancyAssigned;
    }

    public void setVacancyAssigned(Boolean vacancyAssigned) {
        this.vacancyAssigned = vacancyAssigned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IisCurriculumInfo iisCurriculumInfo = (IisCurriculumInfo) o;

        if ( ! Objects.equals(id, iisCurriculumInfo.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "IisCurriculumInfo{" +
            "id=" + id +
            ", firstDate=" + firstDate +
            ", lastDate=" + lastDate +
            ", mpoEnlisted=" + mpoEnlisted +
            ", recNo='" + recNo + '\'' +
            ", acSession='" + acSession + '\'' +
            ", mpoDate=" + mpoDate +
            ", lastRecNo='" + lastRecNo + '\'' +
            ", lastMpoDate=" + lastMpoDate +
            ", status=" + status +
            ", curId='" + curId + '\'' +
            ", createDate=" + createDate +
            ", createBy=" + createBy +
            ", updateDate=" + updateDate +
            ", updateBy=" + updateBy +
            ", lastAcdrAttach=" + Arrays.toString(lastAcdrAttach) +
            ", lastAcdrAttachCntType='" + lastAcdrAttachCntType + '\'' +
            ", lastAcdrAttachName='" + lastAcdrAttachName + '\'' +
            ", firstAcdrAttach=" + Arrays.toString(firstAcdrAttach) +
            ", firstAcdrAttachCntType='" + firstAcdrAttachCntType + '\'' +
            ", firstAcdrAttachName='" + firstAcdrAttachName + '\'' +
            ", institute=" + institute +
            ", cmsCurriculum=" + cmsCurriculum +
            ", vacancyAssigned=" + vacancyAssigned +
            '}';
    }
}
