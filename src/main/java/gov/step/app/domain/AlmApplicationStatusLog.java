package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A MpoApplicationStatusLog.
 */
@Entity
@Table(name = "alm_application_status_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "almapplicationstatuslog")
public class AlmApplicationStatusLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "status")
    private String status;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "application_type")
    private String applicationType;

    @Column(name="application_id")
    private Long applicationId;

    @Column(name="employee_id")
    private Long employeeId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;

    @ManyToOne
    @JoinColumn(name = "update_by")
    private User updateBy;

    @Transient
    private HrEmployeeInfo nextApprover;

    @Transient
    private String currentApproverRemarks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public HrEmployeeInfo getNextApprover() {
        return nextApprover;
    }

    public void setNextApprover(HrEmployeeInfo nextApprover) {
        this.nextApprover = nextApprover;
    }

    public String getCurrentApproverRemarks() {
        return currentApproverRemarks;
    }

    public void setCurrentApproverRemarks(String currentApproverRemarks) {
        this.currentApproverRemarks = currentApproverRemarks;
    }

    @Override
    public String toString() {
        return "AlmApplicationStatusLog{" +
            "id=" + id +
            ", status='" + status + '\'' +
            ", remarks='" + remarks + '\'' +
            ", applicationType='" + applicationType + '\'' +
            ", applicationId=" + applicationId +
            ", employeeId=" + employeeId +
            ", createDate=" + createDate +
            ", updateDate=" + updateDate +
            ", createBy=" + createBy +
            ", updateBy=" + updateBy +
            ", nextApprover=" + nextApprover +
            ", currentApproverRemarks='" + currentApproverRemarks + '\'' +
            '}';
    }
}
