package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PgmsAppRetirmntNmine.
 */
@Entity
@Table(name = "pgms_app_retirmnt_nmine")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pgmsappretirmntnmine")
public class PgmsAppRetirmntNmine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "app_retirmnt_pen_id")
    private PgmsAppRetirmntPen appRetirmntPenId;

    @Column(name = "nominee_status")
    private Boolean nomineeStatus;

    @NotNull
    @Column(name = "nominee_name", nullable = false)
    private String nomineeName;

    @NotNull
    @Column(name = "gender")
    private String gender;

    @NotNull
    @Column(name = "relation")
    private String relation;

    @NotNull
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @NotNull
    @Column(name = "present_address")
    private String presentAddress;

    //    @NotNull
    @Column(name = "nid")
    private String nid;

    @NotNull
    @Column(name = "occupation")
    private String occupation;

    @Column(name = "designation")
    private String designation;

    @NotNull
    @Column(name = "marital_status")
    private String maritalStatus;

    @NotNull
    @Column(name = "mobile_no")
    private Long mobileNo;

    @NotNull
    @Column(name = "get_pension")
    private Long getPension;

    @Column(name = "hr_nominee_info")
    private Boolean hrNomineeInfo;

    @Lob
    @Column(name = "birth_certificate")
    private byte[] birthCertificate;


    @Column(name = "certificate_content_type")
    private String certificateContentType;


    @Column(name = "certificate_doc_name")
    private String certificateDocName;


    @Lob
    @Column(name = "nominee_upload")
    private byte[] nomineeUpload;


    @Column(name = "nominee_content_type")
    private String nomineeContentType;


    @Column(name = "nominee_doc_name")
    private String nomineeDocName;

    @Column(name = "nominee_disable")
    private String nomineeDisable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PgmsAppRetirmntPen getAppRetirmntPenId() {
        return appRetirmntPenId;
    }

    public void setAppRetirmntPenId(PgmsAppRetirmntPen appRetirmntPenId) {
        this.appRetirmntPenId = appRetirmntPenId;
    }

    public Boolean getNomineeStatus() {
        return nomineeStatus;
    }

    public void setNomineeStatus(Boolean nomineeStatus) {
        this.nomineeStatus = nomineeStatus;
    }

    public String getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Long getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(Long mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Long getGetPension() {
        return getPension;
    }

    public void setGetPension(Long getPension) {
        this.getPension = getPension;
    }

    public Boolean getHrNomineeInfo() {
        return hrNomineeInfo;
    }

    public void setHrNomineeInfo(Boolean hrNomineeInfo) {
        this.hrNomineeInfo = hrNomineeInfo;
    }

    public byte[] getBirthCertificate() {
        return birthCertificate;
    }

    public void setBirthCertificate(byte[] birthCertificate) {
        this.birthCertificate = birthCertificate;
    }

    public String getCertificateContentType() {
        return certificateContentType;
    }

    public void setCertificateContentType(String certificateContentType) {
        this.certificateContentType = certificateContentType;
    }

    public String getCertificateDocName() {
        return certificateDocName;
    }

    public void setCertificateDocName(String certificateDocName) {
        this.certificateDocName = certificateDocName;
    }

    public byte[] getNomineeUpload() {
        return nomineeUpload;
    }

    public void setNomineeUpload(byte[] nomineeUpload) {
        this.nomineeUpload = nomineeUpload;
    }

    public String getNomineeContentType() {
        return nomineeContentType;
    }

    public void setNomineeContentType(String nomineeContentType) {
        this.nomineeContentType = nomineeContentType;
    }

    public String getNomineeDocName() {
        return nomineeDocName;
    }

    public void setNomineeDocName(String nomineeDocName) {
        this.nomineeDocName = nomineeDocName;
    }

    public String getNomineeDisable() {
        return nomineeDisable;
    }

    public void setNomineeDisable(String nomineeDisable) {
        this.nomineeDisable = nomineeDisable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PgmsAppRetirmntNmine pgmsAppRetirmntNmine = (PgmsAppRetirmntNmine) o;

        if ( ! Objects.equals(id, pgmsAppRetirmntNmine.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PgmsAppRetirmntNmine{" +
            "id=" + id +
            ", appRetirmntPenId='" + appRetirmntPenId + "'" +
            ", nomineeStatus='" + nomineeStatus + "'" +
            ", nomineeDisable='" + nomineeDisable + "'" +
            ", nomineeName='" + nomineeName + "'" +
            ", gender='" + gender + "'" +
            ", relation='" + relation + "'" +
            ", dateOfBirth='" + dateOfBirth + "'" +
            ", presentAddress='" + presentAddress + "'" +
            ", nid='" + nid + "'" +
            ", occupation='" + occupation + "'" +
            ", designation='" + designation + "'" +
            ", maritalStatus='" + maritalStatus + "'" +
            ", mobileNo='" + mobileNo + "'" +
            ", getPension='" + getPension + "'" +
            ", hrNomineeInfo='" + hrNomineeInfo + "'" +
            ", birthCertificate='" + birthCertificate + "'" +
            ", certificateContentType='" + certificateContentType + "'" +
            ", certificateDocName='" + certificateDocName + "'" +
            ", nomineeUpload='" + nomineeUpload + "'" +
            ", nomineeContentType='" + nomineeContentType + "'" +
            ", nomineeDocName='" + nomineeDocName + "'" +
            '}';
    }
}
