package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SisStudentReg.
 */
@Entity
@Table(name = "sis_student_reg")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sisstudentreg")
public class SisStudentReg implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SIS_STUDENT_REG_SEQ")
    @SequenceGenerator(name="SIS_STUDENT_REG_SEQ", sequenceName="SIS_STUDENT_REG_SEQ")
    private Long id;

    @Column(name = "application_id")
    private String applicationId;

    @Column(name = "code")
    private String code;

    @Column(name = "inst_category")
    private String instCategory;

    @Column(name = "institute_name")
    private String instituteName;

    @ManyToOne
    @JoinColumn(name = "subject1")
    private CmsSubAssign subject1;

    @ManyToOne
    @JoinColumn(name = "subject2")
    private CmsSubAssign subject2;

    @ManyToOne
    @JoinColumn(name = "subject3")
    private CmsSubAssign subject3;

    @ManyToOne
    @JoinColumn(name = "subject4")
    private CmsSubAssign subject4;

    @ManyToOne
    @JoinColumn(name = "subject5")
    private CmsSubAssign subject5;

    @Column(name = "optional")
    private String optional;

    @Column(name = "student_name")
    private String studentName;

    @Column(name = "father_name")
    private String fatherName;

    @Column(name = "mother_name")
    private String motherName;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "mobile_no")
    private String mobileNo;

    @Column(name = "contact_no_home")
    private String contactNoHome;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "present_address")
    private String presentAddress;

    @Column(name = "permanent_address")
    private String permanentAddress;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Institute instituteId;

    @ManyToOne
    @JoinColumn(name = "curriculum_id")
    private IisCurriculumInfo curriculumId;

    @ManyToOne
    @JoinColumn(name = "marital_status")
    private MaritalInfo maritalStatus;

    @ManyToOne
    @JoinColumn(name = "religion_id")
    private Religion religionId;

    @ManyToOne
    @JoinColumn(name = "quota_id")
    private SisQuota quota;

    @ManyToOne
    @JoinColumn(name = "shift_id")
    private MiscTypeSetup shiftId;

    @ManyToOne
    @JoinColumn(name = "section_id")
    private MiscTypeSetup sectionId;

    @ManyToOne
    @JoinColumn(name = "gender_info")
    private GenderInfo genderInfo;

    @ManyToOne
    @JoinColumn(name = "semester_id")
    private CmsSemester semester;

    @ManyToOne
    @JoinColumn(name = "trade_id")
    private CmsTrade tradeId;

    @ManyToOne
    @JoinColumn(name = "blood_group_id")
    private BloodGroupInfo bloodGroupId;

    @ManyToOne
    @JoinColumn(name = "division_present")
    private Division divisionPresent;

    @ManyToOne
    @JoinColumn(name = "district_present")
    private District districtPresent;

    @ManyToOne
    @JoinColumn(name = "thana_upozila")
    private Upazila thanaUpozila;

    @ManyToOne
    @JoinColumn(name = "division_permanent")
    private Division divisionPermanent;

    @ManyToOne
    @JoinColumn(name = "district_permanent")
    private District districtPermanent;

    @ManyToOne
    @JoinColumn(name = "thana_upozilaPer")
    private Upazila thanaUpozilaPer;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInstCategory() {
        return instCategory;
    }

    public void setInstCategory(String instCategory) {
        this.instCategory = instCategory;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }


    public IisCurriculumInfo getCurriculumId() {
        return curriculumId;
    }

    public void setCurriculumId(IisCurriculumInfo curriculumId) {
        this.curriculumId = curriculumId;
    }

    public MaritalInfo getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalInfo maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Religion getReligionId() {
        return religionId;
    }

    public void setReligionId(Religion religionId) {
        this.religionId = religionId;
    }

    public Institute getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Institute instituteId) {
        this.instituteId = instituteId;
    }

    public CmsTrade getTradeId() {
        return tradeId;
    }

    public void setTradeId(CmsTrade tradeId) {
        this.tradeId = tradeId;
    }

    public CmsSubAssign getSubject1() {
        return subject1;
    }

    public void setSubject1(CmsSubAssign subject1) {
        this.subject1 = subject1;
    }

    public CmsSubAssign getSubject2() {
        return subject2;
    }

    public void setSubject2(CmsSubAssign subject2) {
        this.subject2 = subject2;
    }

    public CmsSubAssign getSubject3() {
        return subject3;
    }

    public void setSubject3(CmsSubAssign subject3) {
        this.subject3 = subject3;
    }

    public CmsSubAssign getSubject4() {
        return subject4;
    }

    public void setSubject4(CmsSubAssign subject4) {
        this.subject4 = subject4;
    }

    public CmsSubAssign getSubject5() {
        return subject5;
    }

    public void setSubject5(CmsSubAssign subject5) {
        this.subject5 = subject5;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }

    public void setQuota(SisQuota quota) {
        this.quota = quota;
    }

    public MiscTypeSetup getShiftId() {
        return shiftId;
    }

    public void setShiftId(MiscTypeSetup shiftId) {
        this.shiftId = shiftId;
    }

    public MiscTypeSetup getSectionId() {
        return sectionId;
    }

    public void setSectionId(MiscTypeSetup sectionId) {
        this.sectionId = sectionId;
    }

    public GenderInfo getGenderInfo() {
        return genderInfo;
    }

    public void setGenderInfo(GenderInfo genderInfo) {
        this.genderInfo = genderInfo;
    }

    public CmsSemester getSemester() {
        return semester;
    }

    public void setSemester(CmsSemester semester) {
        this.semester = semester;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public SisQuota getQuota() {
        return quota;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getContactNoHome() {
        return contactNoHome;
    }

    public void setContactNoHome(String contactNoHome) {
        this.contactNoHome = contactNoHome;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public BloodGroupInfo getBloodGroupId() {
        return bloodGroupId;
    }

    public void setBloodGroupId(BloodGroupInfo bloodGroupId) {
        this.bloodGroupId = bloodGroupId;
    }

    public Division getDivisionPresent() {
        return divisionPresent;
    }

    public void setDivisionPresent(Division divisionPresent) {
        this.divisionPresent = divisionPresent;
    }

    public District getDistrictPresent() {
        return districtPresent;
    }

    public void setDistrictPresent(District districtPresent) {
        this.districtPresent = districtPresent;
    }

    public Upazila getThanaUpozila() {
        return thanaUpozila;
    }

    public void setThanaUpozila(Upazila thanaUpozila) {
        this.thanaUpozila = thanaUpozila;
    }

    public Division getDivisionPermanent() {
        return divisionPermanent;
    }

    public void setDivisionPermanent(Division divisionPermanent) {
        this.divisionPermanent = divisionPermanent;
    }

    public District getDistrictPermanent() {
        return districtPermanent;
    }

    public void setDistrictPermanent(District districtPermanent) {
        this.districtPermanent = districtPermanent;
    }

    public Upazila getThanaUpozilaPer() {
        return thanaUpozilaPer;
    }

    public void setThanaUpozilaPer(Upazila thanaUpozilaPer) {
        this.thanaUpozilaPer = thanaUpozilaPer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SisStudentReg sisStudentReg = (SisStudentReg) o;

        if ( ! Objects.equals(id, sisStudentReg.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SisStudentReg{" +
            "id=" + id +
            ", applicationId='" + applicationId + "'" +
            ", instituteId='" + instituteId + "'" +
            ", code='" + code + "'" +
            ", instCategory='" + instCategory + "'" +
            ", instituteName='" + instituteName + "'" +
            ", subject1='" + subject1 + "'" +
            ", subject2='" + subject2 + "'" +
            ", subject3='" + subject3 + "'" +
            ", subject4='" + subject4 + "'" +
            ", subject5='" + subject5 + "'" +
            ", optional='" + optional + "'" +
            ", semester='" + semester + "'" +
            ", studentName='" + studentName + "'" +
            ", fatherName='" + fatherName + "'" +
            ", motherName='" + motherName + "'" +
            ", dateOfBirth='" + dateOfBirth + "'" +
            ", quota='" + quota + "'" +
            ", curriculumId='" + curriculumId + "'" +
            ", religionId='" + religionId + "'" +
            ", shiftId='" + shiftId + "'" +
            ", sectionId='" + sectionId + "'" +
            ", genderInfo='" + genderInfo + "'" +
            ", tradeId='" + tradeId + "'" +
            ", bloodGroupId='" + bloodGroupId + "'" +
            ", user='" + user + "'" +
            ", nationality='" + nationality + "'" +
            ", mobileNo='" + mobileNo + "'" +
            ", contactNoHome='" + contactNoHome + "'" +
            ", emailAddress='" + emailAddress + "'" +
            ", presentAddress='" + presentAddress + "'" +
            ", permanentAddress='" + permanentAddress + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
