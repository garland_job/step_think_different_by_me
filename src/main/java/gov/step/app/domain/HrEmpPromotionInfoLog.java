package gov.step.app.domain;

import gov.step.app.domain.payroll.PrlPayscaleBasicInfo;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A HrEmpPromotionInfoLog.
 */
@Entity
@Table(name = "hr_emp_promotion_info_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "hremppromotioninfolog")
public class HrEmpPromotionInfoLog implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="HR_EMP_PROMOTION_INFO_LOG_SEQ")
    @SequenceGenerator(name="HR_EMP_PROMOTION_INFO_LOG_SEQ", sequenceName="HR_EMP_PROMOTION_INFO_LOG_SEQ")
    private Long id;

//    @Column(name = "institute_from")
//    private String instituteFrom;
//
//    @Column(name = "institute_to")
//    private String instituteTo;
//
//    @Column(name = "department_from")
//    private String departmentFrom;
//
//    @Column(name = "department_to")
//    private String departmentTo;
//
//    @Column(name = "designation_from")
//    private String designationFrom;
//
//    @Column(name = "designation_to")
//    private String designationTo;

    @Column(name = "payscale_from", precision=10, scale=2)
    private BigDecimal payscaleFrom;

    @Column(name = "payscale_to", precision=10, scale=2)
    private BigDecimal payscaleTo;

    @Column(name = "joining_date")
    private LocalDate joiningDate;

    @Column(name = "office_order_no")
    private String officeOrderNo;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "go_date")
    private LocalDate goDate;

    @Lob
    @Column(name = "go_doc")
    private byte[] goDoc;

    @Column(name = "go_doc_content_type")
    private String goDocContentType;

    @Column(name = "go_doc_name")
    private String goDocName;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "log_status")
    private Long logStatus;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "action_date")
    private LocalDate actionDate;

    @Column(name = "action_by")
    private Long actionBy;

    @Column(name = "action_comments")
    private String actionComments;

    @Column(name="from_org_type")
    private String fromOrganizationType;

    @Column(name="to_org_type")
    private String toOrganizationType;

    @ManyToOne
    @JoinColumn(name = "employee_info_id")
    private HrEmployeeInfo employeeInfo;

    @ManyToOne
    @JoinColumn(name = "job_category_id")
    private MiscTypeSetup jobCategory;

    @ManyToOne
    @JoinColumn(name = "promotion_type_id")
    private MiscTypeSetup promotionType;

    @ManyToOne
    @JoinColumn(name = "institute_from_id")
    private Institute instituteFromId;

    @ManyToOne
    @JoinColumn(name = "institute_to_id")
    private Institute instituteToId;

    @ManyToOne
    @JoinColumn(name = "organization_from_id")
    private MiscTypeSetup organizationFromId;

    @ManyToOne
    @JoinColumn(name = "organization_to_id")
    private MiscTypeSetup organizationToId;

    @ManyToOne
    @JoinColumn(name = "department_from_id")
    private HrDepartmentSetup departmentFromId;

    @ManyToOne
    @JoinColumn(name = "department_to_id")
    private HrDepartmentSetup departmentToId;

    @ManyToOne
    @JoinColumn(name = "designation_from_id")
    private HrDesignationSetup designationFromId;

    @ManyToOne
    @JoinColumn(name = "designation_to_id")
    private HrDesignationSetup designationToId;

    @ManyToOne
    @JoinColumn(name = "org_details_id")
    private HrEmpWorkAreaDtlInfo orgDetailsId;

    @ManyToOne
    @JoinColumn(name="prl_payscale_basic_info_id")
    private PrlPayscaleBasicInfo prlPayscaleBasicInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public String getInstituteFrom() {
//        return instituteFrom;
//    }
//
//    public void setInstituteFrom(String instituteFrom) {
//        this.instituteFrom = instituteFrom;
//    }
//
//    public String getInstituteTo() {
//        return instituteTo;
//    }
//
//    public void setInstituteTo(String instituteTo) {
//        this.instituteTo = instituteTo;
//    }
//
//    public String getDepartmentFrom() {
//        return departmentFrom;
//    }
//
//    public void setDepartmentFrom(String departmentFrom) {
//        this.departmentFrom = departmentFrom;
//    }
//
//    public String getDepartmentTo() {
//        return departmentTo;
//    }
//
//    public void setDepartmentTo(String departmentTo) {
//        this.departmentTo = departmentTo;
//    }
//
//    public String getDesignationFrom() {
//        return designationFrom;
//    }
//
//    public void setDesignationFrom(String designationFrom) {
//        this.designationFrom = designationFrom;
//    }
//
//    public String getDesignationTo() {
//        return designationTo;
//    }
//
//    public void setDesignationTo(String designationTo) {
//        this.designationTo = designationTo;
//    }

    public BigDecimal getPayscaleFrom() {
        return payscaleFrom;
    }

    public void setPayscaleFrom(BigDecimal payscaleFrom) {
        this.payscaleFrom = payscaleFrom;
    }

    public BigDecimal getPayscaleTo() {
        return payscaleTo;
    }

    public void setPayscaleTo(BigDecimal payscaleTo) {
        this.payscaleTo = payscaleTo;
    }

    public LocalDate getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(LocalDate joiningDate) {
        this.joiningDate = joiningDate;
    }

    public MiscTypeSetup getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(MiscTypeSetup promotionType) {
        this.promotionType = promotionType;
    }

    public String getOfficeOrderNo() {
        return officeOrderNo;
    }

    public void setOfficeOrderNo(String officeOrderNo) {
        this.officeOrderNo = officeOrderNo;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDate getGoDate() {
        return goDate;
    }

    public void setGoDate(LocalDate goDate) {
        this.goDate = goDate;
    }

    public byte[] getGoDoc() {
        return goDoc;
    }

    public void setGoDoc(byte[] goDoc) {
        this.goDoc = goDoc;
    }

    public String getGoDocContentType() {
        return goDocContentType;
    }

    public void setGoDocContentType(String goDocContentType) {
        this.goDocContentType = goDocContentType;
    }

    public String getGoDocName() {
        return goDocName;
    }

    public void setGoDocName(String goDocName) {
        this.goDocName = goDocName;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getFromOrganizationType() {
        return fromOrganizationType;
    }

    public void setFromOrganizationType(String fromOrganizationType) {
        this.fromOrganizationType = fromOrganizationType;
    }

    public String getToOrganizationType() {
        return toOrganizationType;
    }

    public void setToOrganizationType(String toOrganizationType) {
        this.toOrganizationType = toOrganizationType;
    }

    public Long getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Long logStatus) {
        this.logStatus = logStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDate actionDate) {
        this.actionDate = actionDate;
    }

    public Long getActionBy() {
        return actionBy;
    }

    public void setActionBy(Long actionBy) {
        this.actionBy = actionBy;
    }

    public String getActionComments() {
        return actionComments;
    }

    public void setActionComments(String actionComments) {
        this.actionComments = actionComments;
    }

    public HrEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.employeeInfo = HrEmployeeInfo;
    }

    public MiscTypeSetup getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(MiscTypeSetup MiscTypeSetup) {
        this.jobCategory = MiscTypeSetup;
    }

    public Institute getInstituteFromId() {
        return instituteFromId;
    }

    public void setInstituteFromId(Institute instituteFromId) {
        this.instituteFromId = instituteFromId;
    }

    public Institute getInstituteToId() {
        return instituteToId;
    }

    public void setInstituteToId(Institute instituteToId) {
        this.instituteToId = instituteToId;
    }

    public MiscTypeSetup getOrganizationFromId() {
        return organizationFromId;
    }

    public void setOrganizationFromId(MiscTypeSetup organizationFromId) {
        this.organizationFromId = organizationFromId;
    }

    public MiscTypeSetup getOrganizationToId() {
        return organizationToId;
    }

    public void setOrganizationToId(MiscTypeSetup organizationToId) {
        this.organizationToId = organizationToId;
    }

    public HrDepartmentSetup getDepartmentFromId() {
        return departmentFromId;
    }

    public void setDepartmentFromId(HrDepartmentSetup departmentFromId) {
        this.departmentFromId = departmentFromId;
    }

    public HrDepartmentSetup getDepartmentToId() {
        return departmentToId;
    }

    public void setDepartmentToId(HrDepartmentSetup departmentToId) {
        this.departmentToId = departmentToId;
    }

    public HrDesignationSetup getDesignationFromId() {
        return designationFromId;
    }

    public void setDesignationFromId(HrDesignationSetup designationFromId) {
        this.designationFromId = designationFromId;
    }

    public HrDesignationSetup getDesignationToId() {
        return designationToId;
    }

    public void setDesignationToId(HrDesignationSetup designationToId) {
        this.designationToId = designationToId;
    }


    public HrEmpWorkAreaDtlInfo getOrgDetailsId() {
        return orgDetailsId;
    }

    public void setOrgDetailsId(HrEmpWorkAreaDtlInfo orgDetailsId) {
        this.orgDetailsId = orgDetailsId;
    }

    public PrlPayscaleBasicInfo getPrlPayscaleBasicInfo() {
        return prlPayscaleBasicInfo;
    }

    public void setPrlPayscaleBasicInfo(PrlPayscaleBasicInfo prlPayscaleBasicInfo) {
        this.prlPayscaleBasicInfo = prlPayscaleBasicInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HrEmpPromotionInfoLog hrEmpPromotionInfoLog = (HrEmpPromotionInfoLog) o;
        return Objects.equals(id, hrEmpPromotionInfoLog.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HrEmpPromotionInfoLog{" +
            "id=" + id +
//            ", instituteFrom='" + instituteFrom + "'" +
//            ", instituteTo='" + instituteTo + "'" +
//            ", departmentFrom='" + departmentFrom + "'" +
//            ", departmentTo='" + departmentTo + "'" +
//            ", designationFrom='" + designationFrom + "'" +
//            ", designationTo='" + designationTo + "'" +
            ", payscaleFrom='" + payscaleFrom + "'" +
            ", payscaleTo='" + payscaleTo + "'" +
            ", joiningDate='" + joiningDate + "'" +
            ", promotionType='" + promotionType + "'" +
            ", officeOrderNo='" + officeOrderNo + "'" +
            ", orderDate='" + orderDate + "'" +
            ", goDate='" + goDate + "'" +
            ", goDoc='" + goDoc + "'" +
            ", goDocContentType='" + goDocContentType + "'" +
            ", goDocName='" + goDocName + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", parentId='" + parentId + "'" +
            ", logStatus='" + logStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", actionDate='" + actionDate + "'" +
            ", actionBy='" + actionBy + "'" +
            ", actionComments='" + actionComments + "'" +
            '}';
    }
}
