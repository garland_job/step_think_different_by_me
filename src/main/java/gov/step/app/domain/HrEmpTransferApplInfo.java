package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A HrEmpTransferApplInfo.
 */
@Entity
@Table(name = "hr_emp_transfer_appl_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "hremptransferapplinfo")
public class HrEmpTransferApplInfo implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="HR_EMP_TRANSFER_APPL_INFO_SEQ")
    @SequenceGenerator(name="HR_EMP_TRANSFER_APPL_INFO_SEQ", sequenceName="HR_EMP_TRANSFER_APPL_INFO_SEQ")
    private Long id;

    @NotNull
    @Column(name = "transfer_reason", nullable = false)
    private String transferReason;

    @NotNull
    @Column(name = "office_order_number", nullable = false)
    private String officeOrderNumber;

    @Column(name = "office_order_date")
    private LocalDate officeOrderDate;

    @Column(name = "transfer_release_date")
    private LocalDate transferReleaseDate;

    @NotNull
    @Column(name = "active_status", nullable = false)
    private Boolean activeStatus;

    @Column(name = "log_id")
    private Long logId;

    @Column(name = "selected_option")
    private Long selectedOption;

    @Column(name = "log_status")
    private Long logStatus;

    @Column(name = "log_comments")
    private String logComments;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JoinColumn(name = "transfer_type_id")
    private MiscTypeSetup transferType;

    @ManyToOne
    @JoinColumn(name = "employee_info_id")
    private HrEmployeeInfo employeeInfo;

    @Column(name = "org_type_opt_one")
    private String orgTypeOptOne;

    @ManyToOne
    @JoinColumn(name = "department_opt_one_id")
    private HrDepartmentSetup departInfoOptOne;

    @ManyToOne
    @JoinColumn(name = "desig_option_one_id")
    private HrDesignationSetup desigOptionOne;

    @ManyToOne
    @JoinColumn(name = "org_option_one_id")
    private HrEmpWorkAreaDtlInfo orgOptionOne;

    @ManyToOne
    @JoinColumn(name = "institute_opt_one_id")
    private Institute instituteOptOne;


    @Column(name = "org_type_opt_two")
    private String orgTypeOptTwo;

    @ManyToOne
    @JoinColumn(name = "department_opt_two_id")
    private HrDepartmentSetup departInfoOptTwo;

    @ManyToOne
    @JoinColumn(name = "desig_option_two_id")
    private HrDesignationSetup desigOptionTwo;

    @ManyToOne
    @JoinColumn(name = "org_option_two_id")
    private HrEmpWorkAreaDtlInfo orgOptionTwo;

    @ManyToOne
    @JoinColumn(name = "institute_opt_two_id")
    private Institute instituteOptTwo;


    @Column(name = "org_type_opt_three")
    private String orgTypeOptThree;

    @ManyToOne
    @JoinColumn(name = "department_opt_three_id")
    private HrDepartmentSetup departInfoOptThree;

    @ManyToOne
    @JoinColumn(name = "desig_option_three_id")
    private HrDesignationSetup desigOptionThree;

    @ManyToOne
    @JoinColumn(name = "org_option_three_id")
    private HrEmpWorkAreaDtlInfo orgOptionThree;

    @ManyToOne
    @JoinColumn(name = "institute_opt_three_id")
    private Institute instituteOptThree;


    @Column(name = "org_type_opt_four")
    private String orgTypeOptFour;

    @ManyToOne
    @JoinColumn(name = "department_opt_four_id")
    private HrDepartmentSetup departInfoOptFour;

    @ManyToOne
    @JoinColumn(name = "desig_option_four_id")
    private HrDesignationSetup desigOptionFour;

    @ManyToOne
    @JoinColumn(name = "org_option_four_id")
    private HrEmpWorkAreaDtlInfo orgOptionFour;

    @ManyToOne
    @JoinColumn(name = "institute_opt_four_id")
    private Institute instituteOptFour;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransferReason() {
        return transferReason;
    }

    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    public String getOfficeOrderNumber() {
        return officeOrderNumber;
    }

    public void setOfficeOrderNumber(String officeOrderNumber) {
        this.officeOrderNumber = officeOrderNumber;
    }

    public LocalDate getOfficeOrderDate() {
        return officeOrderDate;
    }

    public void setOfficeOrderDate(LocalDate officeOrderDate) {
        this.officeOrderDate = officeOrderDate;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Long logStatus) {
        this.logStatus = logStatus;
    }

    public String getLogComments() {
        return logComments;
    }

    public void setLogComments(String logComments) {
        this.logComments = logComments;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public HrEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.employeeInfo = HrEmployeeInfo;
    }

    public Long getSelectedOption() {return selectedOption;}

    public void setSelectedOption(Long selectedOption) {this.selectedOption = selectedOption;}

    public HrEmpWorkAreaDtlInfo getOrgOptionOne() {return orgOptionOne;}

    public void setOrgOptionOne(HrEmpWorkAreaDtlInfo orgOptionOne) {this.orgOptionOne = orgOptionOne;}

    public HrDesignationSetup getDesigOptionOne() {return desigOptionOne;}

    public void setDesigOptionOne(HrDesignationSetup desigOptionOne) {this.desigOptionOne = desigOptionOne;}

    public HrEmpWorkAreaDtlInfo getOrgOptionTwo() {return orgOptionTwo;}

    public void setOrgOptionTwo(HrEmpWorkAreaDtlInfo orgOptionTwo) {this.orgOptionTwo = orgOptionTwo;}

    public HrDesignationSetup getDesigOptionTwo() {return desigOptionTwo;}

    public void setDesigOptionTwo(HrDesignationSetup desigOptionTwo) {this.desigOptionTwo = desigOptionTwo;}

    public HrEmpWorkAreaDtlInfo getOrgOptionThree() {return orgOptionThree;}

    public void setOrgOptionThree(HrEmpWorkAreaDtlInfo orgOptionThree) {this.orgOptionThree = orgOptionThree;}

    public HrDesignationSetup getDesigOptionThree() {return desigOptionThree;}

    public void setDesigOptionThree(HrDesignationSetup desigOptionThree) {this.desigOptionThree = desigOptionThree;}

    public LocalDate getTransferReleaseDate() {return transferReleaseDate;}

    public void setTransferReleaseDate(LocalDate transferReleaseDate) {this.transferReleaseDate = transferReleaseDate;}

    public String getOrgTypeOptOne() {return orgTypeOptOne;}

    public HrDepartmentSetup getDepartInfoOptOne() {return departInfoOptOne;}

    public Institute getInstituteOptOne() {return instituteOptOne;}

    public String getOrgTypeOptTwo() {return orgTypeOptTwo;}

    public HrDepartmentSetup getDepartInfoOptTwo() {return departInfoOptTwo;}

    public String getOrgTypeOptThree() {return orgTypeOptThree;}

    public HrDepartmentSetup getDepartInfoOptThree() {return departInfoOptThree;}

    public MiscTypeSetup getTransferType() {
        return transferType;
    }

    public void setTransferType(MiscTypeSetup MiscTypeSetup) {
        this.transferType = MiscTypeSetup;
    }

    public void setOrgTypeOptOne(String orgTypeOptOne) {
        this.orgTypeOptOne = orgTypeOptOne;
    }

    public void setDepartInfoOptOne(HrDepartmentSetup departInfoOptOne) {
        this.departInfoOptOne = departInfoOptOne;
    }

    public void setInstituteOptOne(Institute instituteOptOne) {
        this.instituteOptOne = instituteOptOne;
    }

    public void setOrgTypeOptTwo(String orgTypeOptTwo) {
        this.orgTypeOptTwo = orgTypeOptTwo;
    }

    public void setDepartInfoOptTwo(HrDepartmentSetup departInfoOptTwo) {
        this.departInfoOptTwo = departInfoOptTwo;
    }

    public Institute getInstituteOptTwo() {
        return instituteOptTwo;
    }

    public void setInstituteOptTwo(Institute instituteOptTwo) {
        this.instituteOptTwo = instituteOptTwo;
    }

    public void setOrgTypeOptThree(String orgTypeOptThree) {
        this.orgTypeOptThree = orgTypeOptThree;
    }

    public void setDepartInfoOptThree(HrDepartmentSetup departInfoOptThree) {
        this.departInfoOptThree = departInfoOptThree;
    }

    public Institute getInstituteOptThree() {
        return instituteOptThree;
    }

    public void setInstituteOptThree(Institute instituteOptThree) {
        this.instituteOptThree = instituteOptThree;
    }

    public String getOrgTypeOptFour() {
        return orgTypeOptFour;
    }

    public void setOrgTypeOptFour(String orgTypeOptFour) {
        this.orgTypeOptFour = orgTypeOptFour;
    }

    public HrDepartmentSetup getDepartInfoOptFour() {
        return departInfoOptFour;
    }

    public void setDepartInfoOptFour(HrDepartmentSetup departInfoOptFour) {
        this.departInfoOptFour = departInfoOptFour;
    }

    public HrDesignationSetup getDesigOptionFour() {
        return desigOptionFour;
    }

    public void setDesigOptionFour(HrDesignationSetup desigOptionFour) {
        this.desigOptionFour = desigOptionFour;
    }

    public HrEmpWorkAreaDtlInfo getOrgOptionFour() {
        return orgOptionFour;
    }

    public void setOrgOptionFour(HrEmpWorkAreaDtlInfo orgOptionFour) {
        this.orgOptionFour = orgOptionFour;
    }

    public Institute getInstituteOptFour() {
        return instituteOptFour;
    }

    public void setInstituteOptFour(Institute instituteOptFour) {
        this.instituteOptFour = instituteOptFour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HrEmpTransferApplInfo hrEmpTransferApplInfo = (HrEmpTransferApplInfo) o;
        return Objects.equals(id, hrEmpTransferApplInfo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HrEmpTransferApplInfo{" +
            "id=" + id +
            ", transferReason='" + transferReason + "'" +
            ", officeOrderNumber='" + officeOrderNumber + "'" +
            ", officeOrderDate='" + officeOrderDate + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", logId='" + logId + "'" +
            ", logStatus='" + logStatus + "'" +
            ", logComments='" + logComments + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
