package gov.step.app.domain.enumeration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leads on 12/10/15.
 */
public class PensionApplicationStatusType {

    private final int code;
    private final String remarks;
    private final String declineRemarks;
    private final String role;
    private final String viewMessage;

    public static final int TOTAL = 12;

    private PensionApplicationStatusType(int c, String n, String r, String declineRemarks, String viewMessage) {
        code = c;
        remarks = n;
        role = r;
        this.declineRemarks = declineRemarks;
        this.viewMessage = viewMessage;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("code = [");
        buf.append(this.code);
        buf.append("]");
        buf.append(", name = [");
        buf.append(this.remarks);
        buf.append("]");
        return buf.toString();
    }

    public static final PensionApplicationStatusType REJECTEED
        = new PensionApplicationStatusType(0, "Rejected ","ROLE_ALL","","");

    public static final PensionApplicationStatusType DECLINED
        = new PensionApplicationStatusType(1, "Application Declined","ROLE_BLANK","","");

    public static final PensionApplicationStatusType COMPLETE
        = new PensionApplicationStatusType(2, "Application Completed","ROLE_HRM_USER","","");

    public static final PensionApplicationStatusType FORWARDBYDG
        = new PensionApplicationStatusType(3, "Forwarded From DG","ROLE_DG","Declined by DG","Director General");

    public static final PensionApplicationStatusType APPROVEDBYADMIN
        = new PensionApplicationStatusType(4, "Forwarded From ADMIN","ROLE_ADMIN","Decline by ADMIN","ADMIN");

    public static final PensionApplicationStatusType APPROVEDBYAD
        = new PensionApplicationStatusType(5, "Forwarded From Assistant Director","ROLE_AD","Declined by AD","Assistant Director");

    public static final PensionApplicationStatusType APPROVEDBYDG
        = new PensionApplicationStatusType(6, "Approved From Assistant Director","ROLE_DG","Declined by DG","Director General");

//    public static final PensionApplicationStatusType FRONTDESKLOOKUP
//        = new PensionApplicationStatusType(6, "Forwarded From Front Desk","ROLE_FRONTDESK","Declined by DEO","MPO Front Desk");
//
//    public static final PensionApplicationStatusType APPROVEDBYAD
//        = new PensionApplicationStatusType(7, "Forwarded From Assistant Director","ROLE_AD","Decline by Assistant Director","Assistant Director");
//
//    public static final PensionApplicationStatusType SUMMARIZEDBYAD
//        = new PensionApplicationStatusType(8, "Forwarded From Assistant Director","ROLE_AD","Decline by Assistant Director","Assistant Director");
//
//    public static final PensionApplicationStatusType APPROVEDBYDIRECTOR
//        = new PensionApplicationStatusType(9, "Forwarded From Director","ROLE_DIRECTOR","Decline by Director","Director");
//
//    public static final PensionApplicationStatusType APPROVEDBYDG
//        = new PensionApplicationStatusType(10, "Forwarded From Director General","ROLE_DG","Decline by Director General","Director General");


    /*public static final MpoApplicationStatusType APPROVEDBYMPOCOMMITTEE
        = new MpoApplicationStatusType(10, "Approved By Director General","ROLE_MPOCOMMITTEE","Decline by Director General","MPO Committee");


    public static final MpoApplicationStatusType FINALLYAPPROVEDBYDG
        = new MpoApplicationStatusType(11, "Approved By Director General","ROLE_DG","Decline by Director General","Director General");*/

    private static final PensionApplicationStatusType[] list = {
        REJECTEED,
        DECLINED,
        COMPLETE,
        FORWARDBYDG,
        APPROVEDBYADMIN,
        APPROVEDBYAD
    };
//    private static final PensionApplicationStatusType[] getSchoolApproveList = {
//        APPROVEDBYINSTITUTE,
//        APPROVEDBYMANEGINGCOMMITTEE,
//        APPROVEDBYDEO,
//        FRONTDESKLOOKUP,
//        APPROVEDBYAD,
//        APPROVEDBYDIRECTOR,
//        APPROVEDBYDG
//    };
    private static final PensionApplicationStatusType[] getCollageApproveList = {
        FORWARDBYDG,
        APPROVEDBYADMIN,
        APPROVEDBYAD
    };
    public static List<PensionApplicationStatusType> getPensionApplicationApproveList(String type, String role){
        List<PensionApplicationStatusType> pensionApplicationStatusTypes= new ArrayList<PensionApplicationStatusType>();
        if(type !=null && type.equals("General")){
            for(PensionApplicationStatusType pensionApplicationStatusType: PensionApplicationStatusType.getCollageApproveList){
                if(!getPensionApplicationStatusTypeByRole(role).equals(pensionApplicationStatusType)){
                    pensionApplicationStatusTypes.add(pensionApplicationStatusType);
                }
            }
        }
//        else{
//            for(PensionApplicationStatusType mpoApplicationStatusType: PensionApplicationStatusType.getSchoolApproveList){
//                if(!getMpoApplicationStatusTypeByRole(role).equals(mpoApplicationStatusType)){
//                    mpoApplicationStatusTypes.add(mpoApplicationStatusType);
//                }
//            }
//        }
        /*if(getMpoApplicationStatusTypeByRole(role).equals(getMpoApplicationStatusTypeByRole("ROLE_DG"))){
            mpoApplicationStatusTypes.add(getMpoApplicationStatusTypeByRole("ROLE_MPOCOMMITTEE"));
        }*/
        return pensionApplicationStatusTypes;
    }

    public int toInt() {
        return code;
    }

    public int getCode() {
        return code;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getDeclineRemarks() {
        return declineRemarks;
    }

    public String getRole() {
        return role;
    }

    public String getViewMessage() {
        return viewMessage;
    }

    public static PensionApplicationStatusType fromInt(int c) {
        if (c < 1 || c > TOTAL)
            throw new RuntimeException("Unknown code for fromInt in PensionApplicationStatusType");
        return list[c];
    }
    public static PensionApplicationStatusType nextMpoApplicationStatusType(int c) {
        if (c < 0 || c > TOTAL)
            throw new RuntimeException("Unknown code for fromInt in PensionApplicationStatusType");
        return list[c+1];
    }
    public static PensionApplicationStatusType prevPensionApplicationStatusType(int c) {
        if (c < 0 || c > TOTAL)
            throw new RuntimeException("Unknown code for fromInt in PensionApplicationStatusType");
        return list[c-1];
    }
    public static PensionApplicationStatusType previousRoleMpoApplicationStatusType(String role) {

        if(role==null){
            return null;
        }

        if(role.equals("")){
        return null;
        }
        else{
            return  prevPensionApplicationStatusType(getPensionApplicationStatusTypeByRole(role).getCode());
        }
    }

    public static PensionApplicationStatusType currentRoleMpoApplicationStatusType(String role) {

        if(role==null){
            return null;
        }

        if(role.equals("")){
            return null;
        }
        else{
            return  getPensionApplicationStatusTypeByRole(role);
        }
    }


    public static PensionApplicationStatusType getPensionApplicationStatusTypeByRole(String role){

        switch(role) {

            case "ROLE_DG": return FORWARDBYDG;

            case "ROLE_ADMIN": return APPROVEDBYADMIN;

            case "ROLE_AD": return APPROVEDBYAD;

//            case "ROLE_FRONTDESK": return FRONTDESKLOOKUP;
//
//            case "ROLE_AD": return APPROVEDBYAD;
//
//            case "ROLE_DIRECTOR": return APPROVEDBYDIRECTOR;

//            case "ROLE_DG": return APPROVEDBYDG;

            //case "ROLE_MPOCOMMITTEE": return APPROVEDBYMPOCOMMITTEE;

            default: return null;
            // etc...
        }

    }

    /*public static List getMpoApplicationStatusList() {
        return Arrays.asList(list);
    }*/
}
