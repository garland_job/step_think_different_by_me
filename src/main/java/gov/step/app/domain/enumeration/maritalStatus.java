package gov.step.app.domain.enumeration;

/**
 * The maritalStatus enumeration.
 */
public enum maritalStatus {

    Unmarried, Married, Divorce, Widow, Single
}
