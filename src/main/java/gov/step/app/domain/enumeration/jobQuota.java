package gov.step.app.domain.enumeration;

/**
 * The jobQuota enumeration.
 */
public enum jobQuota {
    General,FreedomFighter, ChildOfFreedomFighter, GrandChildOfFreedomFighter, Tribal,Disable,Others
}
