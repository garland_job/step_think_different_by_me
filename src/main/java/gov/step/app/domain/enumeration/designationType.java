package gov.step.app.domain.enumeration;

/**
 * The designationType enumeration.
 */
public enum designationType
{
    Teacher,Employee,Staff,HRM,DTE_Employee,Non_Govt_Employee,Officer
}
