package gov.step.app.domain.enumeration;

/**
 * The ErpAuthorityAction enumeration.
 */
public enum ErpAuthorityAction {
    Pending, Reject, Approved, OnAuthorization, Forwarded
}
