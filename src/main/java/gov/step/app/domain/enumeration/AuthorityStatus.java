package gov.step.app.domain.enumeration;

/**
 * The AuthorityStatus enumeration.
 */
public enum AuthorityStatus {
    Pending, Reject, Approved
}
