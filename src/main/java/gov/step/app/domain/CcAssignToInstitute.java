package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CcAssignToInstitute.
 */
@Entity
@Table(name = "cc_assign_to_institute")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ccassigntoinstitute")
public class CcAssignToInstitute implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "center_code", nullable = false)
    private String centerCode;

    @OneToOne
    @JoinColumn(name = "cc_institute_id")
    private InstGenInfo instGenInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public InstGenInfo getInstGenInfo() {
        return instGenInfo;
    }

    public void setInstGenInfo(InstGenInfo instGenInfo) {
        this.instGenInfo = instGenInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CcAssignToInstitute ccAssignToInstitute = (CcAssignToInstitute) o;

        if ( ! Objects.equals(id, ccAssignToInstitute.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CcAssignToInstitute{" +
            "id=" + id +
            ", centerCode='" + centerCode + "'" +
            '}';
    }
}
