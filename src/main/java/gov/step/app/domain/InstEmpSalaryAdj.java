package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A InstEmpSalaryAdj.
 */
@Entity
@Table(name = "INST_EMP_SALARY_ADJUSTMENT")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "instempsalaryadj")
public class InstEmpSalaryAdj implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type_adjustment")
    private String typeAdjustment;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "salary_adjusted")
    private Long salaryAdjusted;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "type")
    private Long type;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "salary_adjusted_date")
    private LocalDate salary_adjusted_date;

    @ManyToOne
    @JoinColumn(name = "INST_EMPLOYEE_ID")
    private InstEmployee instEmployee;

    @ManyToOne
    @JoinColumn(name = "PAY_SCALE_ID")
    private PayScale payScale;

    @ManyToOne
    @JoinColumn(name = "GAZETTE_ID")
    private GazetteSetup gazetteSetup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeAdjustment() {
        return typeAdjustment;
    }

    public void setTypeAdjustment(String typeAdjustment) {
        this.typeAdjustment = typeAdjustment;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getSalaryAdjusted() {
        return salaryAdjusted;
    }

    public void setSalaryAdjusted(Long salaryAdjusted) {
        this.salaryAdjusted = salaryAdjusted;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public LocalDate getSalary_adjusted_date() {
        return salary_adjusted_date;
    }

    public void setSalary_adjusted_date(LocalDate salary_adjusted_date) {
        this.salary_adjusted_date = salary_adjusted_date;
    }

    public InstEmployee getInstEmployee() {
        return instEmployee;
    }

    public void setInstEmployee(InstEmployee instEmployee) {
        this.instEmployee = instEmployee;
    }

    public PayScale getPayScale() {
        return payScale;
    }

    public void setPayScale(PayScale payScale) {
        this.payScale = payScale;
    }

    public GazetteSetup getGazetteSetup() {
        return gazetteSetup;
    }

    public void setGazetteSetup(GazetteSetup gazetteSetup) {
        this.gazetteSetup = gazetteSetup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InstEmpSalaryAdj instEmpSalaryAdj = (InstEmpSalaryAdj) o;

        if ( ! Objects.equals(id, instEmpSalaryAdj.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InstEmpSalaryAdj{" +
            "id=" + id +
            ", typeAdjustment='" + typeAdjustment + "'" +
            ", amount='" + amount + "'" +
            ", startDate='" + startDate + "'" +
            ", endDate='" + endDate + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", salaryAdjusted='" + salaryAdjusted + "'" +
            ", createDate='" + createDate + "'" +
            ", updateDate='" + updateDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateBy='" + updateBy + "'" +
            ", type='" + type + "'" +
            ", remarks='" + remarks + "'" +
            ", salary_adjusted_date='" + salary_adjusted_date + "'" +
            '}';
    }
}
