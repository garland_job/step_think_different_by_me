package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A AlmAttendanceInformation.
 */
@Entity
@Table(name = "alm_attendance_information")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "almattendanceinformation")
public class AlmAttendanceInformation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "office_in_time", nullable = false)
    private String officeInTime;

    @Column(name = "office_delay_time", nullable = false)
    private String officeDelayTime;

    @Column(name = "office_out_time", nullable = false)
    private String officeOutTime;

    @Column(name = "punch_in_time", nullable = false)
    private String punchInTime;

    @Column(name = "punch_in_note")
    private String punchInNote;

    @Column(name = "punch_out_time", nullable = false)
    private String punchOutTime;

    @Column(name = "punch_out_note")
    private String punchOutNote;

    @Column(name = "day_name")
    private String dayName;

    @Column(name = "process_date")
    private LocalDate processDate;

    @Column(name = "attendence_date")
    private LocalDate attendenceDate;

    @Column(name = "is_processed")
    private Boolean isProcessed;

    @NotNull
    @Column(name = "active_status", nullable = false)
    private Boolean activeStatus;

    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date", nullable = false)
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JoinColumn(name = "employee_info_id")
    private HrEmployeeInfo employeeInfo;

    @ManyToOne
    @JoinColumn(name = "alm_attendance_conf_id")
    private AlmAttendanceConfiguration almAttendanceConfiguration;

    @ManyToOne
    @JoinColumn(name = "alm_shift_setup_id")
    private AlmShiftSetup almShiftSetup;

    @ManyToOne
    @JoinColumn(name = "alm_attendance_status_id")
    private AlmAttendanceStatus almAttendanceStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOfficeInTime() {
        return officeInTime;
    }

    public void setOfficeInTime(String officeInTime) {
        this.officeInTime = officeInTime;
    }

    public String getOfficeDelayTime() {
        return officeDelayTime;
    }

    public void setOfficeDelayTime(String officeDelayTime) {
        this.officeDelayTime = officeDelayTime;
    }

    public String getOfficeOutTime() {
        return officeOutTime;
    }

    public void setOfficeOutTime(String officeOutTime) {
        this.officeOutTime = officeOutTime;
    }

    public String getPunchInTime() {
        return punchInTime;
    }

    public void setPunchInTime(String punchInTime) {
        this.punchInTime = punchInTime;
    }

    public String getPunchInNote() {
        return punchInNote;
    }

    public void setPunchInNote(String punchInNote) {
        this.punchInNote = punchInNote;
    }

    public String getPunchOutTime() {
        return punchOutTime;
    }

    public void setPunchOutTime(String punchOutTime) {
        this.punchOutTime = punchOutTime;
    }

    public String getPunchOutNote() {
        return punchOutNote;
    }

    public void setPunchOutNote(String punchOutNote) {
        this.punchOutNote = punchOutNote;
    }

    public LocalDate getProcessDate() {
        return processDate;
    }

    public void setProcessDate(LocalDate processDate) {
        this.processDate = processDate;
    }

    public LocalDate getAttendenceDate(){
        return attendenceDate;
    }
    public void setAttendenceDate(LocalDate attendenceDate){
        this.attendenceDate = attendenceDate;
    }

    public String getDayName(){
        return dayName;
    }

    public void setDayName(String dayName){
        this.dayName = dayName;
    }

    public Boolean getIsProcessed() {
        return isProcessed;
    }

    public void setIsProcessed(Boolean isProcessed) {
        this.isProcessed = isProcessed;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public HrEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.employeeInfo = HrEmployeeInfo;
    }

    public AlmAttendanceConfiguration getAlmAttendanceConfiguration() {
        return almAttendanceConfiguration;
    }

    public void setAlmAttendanceConfiguration(AlmAttendanceConfiguration AlmAttendanceConfiguration) {
        this.almAttendanceConfiguration = AlmAttendanceConfiguration;
    }

    public AlmShiftSetup getAlmShiftSetup() {
        return almShiftSetup;
    }

    public void setAlmShiftSetup(AlmShiftSetup AlmShiftSetup) {
        this.almShiftSetup = AlmShiftSetup;
    }

    public AlmAttendanceStatus getAlmAttendanceStatus() {
        return almAttendanceStatus;
    }

    public void setAlmAttendanceStatus(AlmAttendanceStatus AlmAttendanceStatus) {
        this.almAttendanceStatus = AlmAttendanceStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AlmAttendanceInformation almAttendanceInformation = (AlmAttendanceInformation) o;

        if ( ! Objects.equals(id, almAttendanceInformation.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AlmAttendanceInformation{" +
            "id=" + id +
            ", officeInTime='" + officeInTime + "'" +
            ", officeOutTime='" + officeOutTime + "'" +
            ", punchInTime='" + punchInTime + "'" +
            ", punchInNote='" + punchInNote + "'" +
            ", punchOutTime='" + punchOutTime + "'" +
            ", punchOutNote='" + punchOutNote + "'" +
            ", processDate='" + processDate + "'" +
            ", isProcessed='" + isProcessed + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            '}';
    }
}
