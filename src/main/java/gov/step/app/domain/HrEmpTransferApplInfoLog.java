package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A HrEmpTransferApplInfoLog.
 */
@Entity
@Table(name = "hr_emp_transfer_appl_info_log")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "hremptransferapplinfolog")
public class HrEmpTransferApplInfoLog implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="HR_EMP_TRANSFER_APPL_INFO_SEQ")
    @SequenceGenerator(name="HR_EMP_TRANSFER_APPL_INFO_SEQ", sequenceName="HR_EMP_TRANSFER_APPL_INFO_SEQ")
    private Long id;

    @Column(name = "transfer_reason")
    private String transferReason;

    @Column(name = "office_order_number")
    private String officeOrderNumber;

    @Column(name = "office_order_date")
    private LocalDate officeOrderDate;

    @Column(name = "transfer_release_date")
    private LocalDate transferReleaseDate;

    @Column(name = "selected_option")
    private Long selectedOption;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "log_status")
    private Long logStatus;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "action_date")
    private LocalDate actionDate;

    @Column(name = "action_by")
    private Long actionBy;

    @Column(name = "action_comments")
    private String actionComments;

    @ManyToOne
    @JoinColumn(name = "transfer_type_id")
    private MiscTypeSetup transferType;

    @ManyToOne
    @JoinColumn(name = "employee_info_id")
    private HrEmployeeInfo employeeInfo;

    @Column(name = "org_type_opt_one")
    private String orgTypeOptOne;

    @ManyToOne
    @JoinColumn(name = "department_opt_one_id")
    private HrDepartmentSetup departInfoOptOne;

    @ManyToOne
    @JoinColumn(name = "desig_option_one_id")
    private HrDesignationSetup desigOptionOne;

    @ManyToOne
    @JoinColumn(name = "org_option_one_id")
    private HrEmpWorkAreaDtlInfo orgOptionOne;

    @ManyToOne
    @JoinColumn(name = "institute_opt_one_id")
    private Institute instituteOptOne;


    @Column(name = "org_type_opt_two")
    private String orgTypeOptTwo;

    @ManyToOne
    @JoinColumn(name = "department_opt_two_id")
    private HrDepartmentSetup departInfoOptTwo;

    @ManyToOne
    @JoinColumn(name = "desig_option_two_id")
    private HrDesignationSetup desigOptionTwo;

    @ManyToOne
    @JoinColumn(name = "org_option_two_id")
    private HrEmpWorkAreaDtlInfo orgOptionTwo;

    @ManyToOne
    @JoinColumn(name = "institute_opt_two_id")
    private Institute instituteTwoOne;


    @Column(name = "org_type_opt_three")
    private String orgTypeOptThree;

    @ManyToOne
    @JoinColumn(name = "department_opt_three_id")
    private HrDepartmentSetup departInfoOptThree;

    @ManyToOne
    @JoinColumn(name = "desig_option_three_id")
    private HrDesignationSetup desigOptionThree;

    @ManyToOne
    @JoinColumn(name = "org_option_three_id")
    private HrEmpWorkAreaDtlInfo orgOptionThree;

    @ManyToOne
    @JoinColumn(name = "institute_opt_three_id")
    private Institute instituteThreeOne;

    @Column(name = "org_type_opt_four")
    private String orgTypeOptFour;

    @ManyToOne
    @JoinColumn(name = "department_opt_four_id")
    private HrDepartmentSetup departInfoOptFour;

    @ManyToOne
    @JoinColumn(name = "desig_option_four_id")
    private HrDesignationSetup desigOptionFour;

    @ManyToOne
    @JoinColumn(name = "org_option_four_id")
    private HrEmpWorkAreaDtlInfo orgOptionFour;

    @ManyToOne
    @JoinColumn(name = "institute_opt_four_id")
    private Institute instituteOptFour;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransferReason() {
        return transferReason;
    }

    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    public String getOfficeOrderNumber() {
        return officeOrderNumber;
    }

    public void setOfficeOrderNumber(String officeOrderNumber) {
        this.officeOrderNumber = officeOrderNumber;
    }

    public LocalDate getOfficeOrderDate() {
        return officeOrderDate;
    }

    public void setOfficeOrderDate(LocalDate officeOrderDate) {
        this.officeOrderDate = officeOrderDate;
    }

    public Long getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(Long selectedOption) {
        this.selectedOption = selectedOption;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Long logStatus) {
        this.logStatus = logStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDate actionDate) {
        this.actionDate = actionDate;
    }

    public Long getActionBy() {
        return actionBy;
    }

    public void setActionBy(Long actionBy) {
        this.actionBy = actionBy;
    }

    public String getActionComments() {
        return actionComments;
    }

    public void setActionComments(String actionComments) {
        this.actionComments = actionComments;
    }

    public HrEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(HrEmployeeInfo HrEmployeeInfo) {
        this.employeeInfo = HrEmployeeInfo;
    }

    public HrEmpWorkAreaDtlInfo getOrgOptionOne() {
        return orgOptionOne;
    }

    public void setOrgOptionOne(HrEmpWorkAreaDtlInfo HrEmpWorkAreaDtlInfo) {
        this.orgOptionOne = HrEmpWorkAreaDtlInfo;
    }

    public HrDesignationSetup getDesigOptionOne() {
        return desigOptionOne;
    }

    public void setDesigOptionOne(HrDesignationSetup HrDesignationSetup) {
        this.desigOptionOne = HrDesignationSetup;
    }

    public HrEmpWorkAreaDtlInfo getOrgOptionTwo() {
        return orgOptionTwo;
    }

    public void setOrgOptionTwo(HrEmpWorkAreaDtlInfo HrEmpWorkAreaDtlInfo) {
        this.orgOptionTwo = HrEmpWorkAreaDtlInfo;
    }

    public HrDesignationSetup getDesigOptionTwo() {
        return desigOptionTwo;
    }

    public void setDesigOptionTwo(HrDesignationSetup HrDesignationSetup) {
        this.desigOptionTwo = HrDesignationSetup;
    }

    public HrEmpWorkAreaDtlInfo getOrgOptionThree() {
        return orgOptionThree;
    }

    public void setOrgOptionThree(HrEmpWorkAreaDtlInfo HrEmpWorkAreaDtlInfo) {this.orgOptionThree = HrEmpWorkAreaDtlInfo;}

    public HrDesignationSetup getDesigOptionThree() {
        return desigOptionThree;
    }

    public void setDesigOptionThree(HrDesignationSetup desigOptionThree) {this.desigOptionThree = desigOptionThree;}

    public LocalDate getTransferReleaseDate() {return transferReleaseDate;}

    public void setTransferReleaseDate(LocalDate transferReleaseDate) {this.transferReleaseDate = transferReleaseDate;}

    public String getOrgTypeOptOne() {return orgTypeOptOne;}

    public void setOrgTypeOptOne(String orgTypeOptOne) {this.orgTypeOptOne = orgTypeOptOne;}

    public HrDepartmentSetup getDepartInfoOptOne() {return departInfoOptOne;}

    public void setDepartInfoOptOne(HrDepartmentSetup departInfoOptOne) {this.departInfoOptOne = departInfoOptOne;}

    public Institute getInstituteOptOne() {return instituteOptOne;}

    public void setInstituteOptOne(Institute instituteOptOne) {this.instituteOptOne = instituteOptOne;}

    public String getOrgTypeOptTwo() {return orgTypeOptTwo;}

    public void setOrgTypeOptTwo(String orgTypeOptTwo) {this.orgTypeOptTwo = orgTypeOptTwo;}

    public HrDepartmentSetup getDepartInfoOptTwo() {return departInfoOptTwo;}

    public void setDepartInfoOptTwo(HrDepartmentSetup departInfoOptTwo) {this.departInfoOptTwo = departInfoOptTwo;}

    public Institute getInstituteTwoOne() {return instituteTwoOne;}

    public void setInstituteTwoOne(Institute instituteTwoOne) {this.instituteTwoOne = instituteTwoOne;}

    public String getOrgTypeOptThree() {return orgTypeOptThree;}

    public void setOrgTypeOptThree(String orgTypeOptThree) {this.orgTypeOptThree = orgTypeOptThree;}

    public HrDepartmentSetup getDepartInfoOptThree() {return departInfoOptThree;}

    public void setDepartInfoOptThree(HrDepartmentSetup departInfoOptThree) {this.departInfoOptThree = departInfoOptThree;}

    public Institute getInstituteThreeOne() {return instituteThreeOne;}

    public void setInstituteThreeOne(Institute instituteThreeOne) {this.instituteThreeOne = instituteThreeOne;}

    public MiscTypeSetup getTransferType() {
        return transferType;
    }

    public void setTransferType(MiscTypeSetup MiscTypeSetup) {
        this.transferType = MiscTypeSetup;
    }

    public String getOrgTypeOptFour() {
        return orgTypeOptFour;
    }

    public void setOrgTypeOptFour(String orgTypeOptFour) {
        this.orgTypeOptFour = orgTypeOptFour;
    }

    public HrDepartmentSetup getDepartInfoOptFour() {
        return departInfoOptFour;
    }

    public void setDepartInfoOptFour(HrDepartmentSetup departInfoOptFour) {
        this.departInfoOptFour = departInfoOptFour;
    }

    public HrDesignationSetup getDesigOptionFour() {
        return desigOptionFour;
    }

    public void setDesigOptionFour(HrDesignationSetup desigOptionFour) {
        this.desigOptionFour = desigOptionFour;
    }

    public HrEmpWorkAreaDtlInfo getOrgOptionFour() {
        return orgOptionFour;
    }

    public void setOrgOptionFour(HrEmpWorkAreaDtlInfo orgOptionFour) {
        this.orgOptionFour = orgOptionFour;
    }

    public Institute getInstituteOptFour() {
        return instituteOptFour;
    }

    public void setInstituteOptFour(Institute instituteOptFour) {
        this.instituteOptFour = instituteOptFour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HrEmpTransferApplInfoLog hrEmpTransferApplInfoLog = (HrEmpTransferApplInfoLog) o;
        return Objects.equals(id, hrEmpTransferApplInfoLog.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HrEmpTransferApplInfoLog{" +
            "id=" + id +
            ", transferReason='" + transferReason + "'" +
            ", officeOrderNumber='" + officeOrderNumber + "'" +
            ", officeOrderDate='" + officeOrderDate + "'" +
            ", selectedOption='" + selectedOption + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", parentId='" + parentId + "'" +
            ", logStatus='" + logStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", actionDate='" + actionDate + "'" +
            ", actionBy='" + actionBy + "'" +
            ", actionComments='" + actionComments + "'" +
            '}';
    }
}
