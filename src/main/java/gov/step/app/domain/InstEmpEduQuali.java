package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A InstEmpEduQuali.
 */
@Entity
@Table(name = "inst_emp_edu_quali")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "instempeduquali")
public class InstEmpEduQuali implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="inst_emp_edu_quali_seq")
    @SequenceGenerator(name="inst_emp_edu_quali_seq", sequenceName="inst_emp_edu_quali_seq")
    private Long id;

    @Column(name = "certificate_name")
    private String certificateName;

    @Column(name = "board")
    private String board;

    @Column(name = "edu_session")
    private String session;

    @Column(name = "semester")
    private String semester;

    @NotNull
    @Column(name = "reg_no", nullable = false)
    private String regNo;

    @NotNull
    @Column(name = "roll_no", nullable = false)
    private String rollNo;

    @NotNull
    @Column(name = "passing_year", nullable = false)
    private Integer passingYear;

    @NotNull
    @Column(name = "cgpa", nullable = false)
    private String cgpa;

    @NotNull
    @Column(name = "board_uni", nullable = false)
    private Boolean boardUni;

    @Column(name = "uni_name")
    private String uniName;

    //groups : science, arts, commerce
    @Column(name = "exam_group")
    private String group;

    @Lob
    @Column(name = "certificate_copy")
    private byte[] certificateCopy;

    @Column(name = "CERT_CPY_NAME")
    private String certificateCopyName;

    @Column(name = "CERT_CPY_CNT_TYPE")
    private String certificateCopyContentType;

    @Column(name = "status")
    private Integer status;

    //@NotNull
    @Column(name = "group_subject")
    private String groupSubject;

    @Column(name = "subject")
    private String Subject;


    @Column(name = "duration")
    private String duration;

    @Column(name = "result_publish_date")
    private LocalDate resultPublishDate;

    @Column(name = "from_university")
    private Boolean fromUniversity;

    @NotNull
    @Column(name = "is_gpa_result", nullable = false)
    private Boolean isGpaResult;

    @Column(name = "university_name")
    private String universityName;


    @Column(name = "govt_or_not")
    private String govtOrNot;

    @Column(name = "gpa_scale")
    private String gpaScale;

    @Column(name = "create_date")
    private LocalDate dateCrated;

    @Column(name = "update_date")
    private LocalDate dateModified;

    @Lob
    @Column(name = "ugc_attach_file")
    private byte[] ugcAttachFile;


    @Column(name = "ugc_attach_file_cnt_type")
    private String ugcAttachFileContentType;

    @Column(name = "ugc_attach_file_name")
    private String ugcAttachFileName;

    @Lob
    @Column(name = "course_atch_file")
    private byte[] courseAtchFile;


    @Column(name = "course_atch_file_cnt_type")
    private String courseAtchFileCntType;

    @Column(name = "course_atch_file_name")
    private String courseAtchFileName;

    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;

    @ManyToOne
    @JoinColumn(name = "update_by")
    private User updateBy;

    @ManyToOne
    @JoinColumn(name = "edu_board_id", nullable = false)
    private EduBoard eduBoard;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "edu_level_id", nullable = false)
    private EduLevel eduLevel ;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "inst_employee_id")
    private InstEmployee instEmployee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public Boolean getBoardUni() {
        return boardUni;
    }

    public void setBoardUni(Boolean boardUni) {
        this.boardUni = boardUni;
    }

    public Boolean getGpaResult() {
        return isGpaResult;
    }

    public void setGpaResult(Boolean gpaResult) {
        isGpaResult = gpaResult;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public Integer getPassingYear() {
        return passingYear;
    }

    public void setPassingYear(Integer passingYear) {
        this.passingYear = passingYear;
    }

    public String getCgpa() {
        return cgpa;
    }

    public void setCgpa(String cgpa) {
        this.cgpa = cgpa;
    }

    public byte[] getCertificateCopy() {
        return certificateCopy;
    }

    public void setCertificateCopy(byte[] certificateCopy) {
        this.certificateCopy = certificateCopy;
    }

    public String getCertificateCopyName() {
        return certificateCopyName;
    }

    public void setCertificateCopyName(String certificateCopyName) {
        this.certificateCopyName = certificateCopyName;
    }

    public String getCertificateCopyContentType() {
        return certificateCopyContentType;
    }

    public void setCertificateCopyContentType(String certificateCopyContentType) {
        this.certificateCopyContentType = certificateCopyContentType;
    }

    public byte[] getCourseAtchFile() {
        return courseAtchFile;
    }

    public void setCourseAtchFile(byte[] courseAtchFile) {
        this.courseAtchFile = courseAtchFile;
    }

    public String getCourseAtchFileCntType() {
        return courseAtchFileCntType;
    }

    public void setCourseAtchFileCntType(String courseAtchFileCntType) {
        this.courseAtchFileCntType = courseAtchFileCntType;
    }

    public String getCourseAtchFileName() {
        return courseAtchFileName;
    }

    public void setCourseAtchFileName(String courseAtchFileName) {
        this.courseAtchFileName = courseAtchFileName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }

    public String getGroupSubject() {
        return groupSubject;
    }

    public void setGroupSubject(String groupSubject) {
        this.groupSubject = groupSubject;
    }

    public LocalDate getResultPublishDate() {
        return resultPublishDate;
    }

    public void setResultPublishDate(LocalDate resultPublishDate) {
        this.resultPublishDate = resultPublishDate;
    }

    public Boolean getFromUniversity() {
        return fromUniversity;
    }

    public void setFromUniversity(Boolean fromUniversity) {
        this.fromUniversity = fromUniversity;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public Boolean getIsGpaResult() {
        return isGpaResult;
    }

    public void setIsGpaResult(Boolean isGpaResult) {
        this.isGpaResult = isGpaResult;
    }

    public String getGpaScale() {
        return gpaScale;
    }

    public void setGpaScale(String gpaScale) {
        this.gpaScale = gpaScale;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getGovtOrNot() {
        return govtOrNot;
    }

    public void setGovtOrNot(String govtOrNot) {
        this.govtOrNot = govtOrNot;
    }

    public byte[] getUgcAttachFile() {
        return ugcAttachFile;
    }

    public void setUgcAttachFile(byte[] ugcAttachFile) {
        this.ugcAttachFile = ugcAttachFile;
    }

    public String getUgcAttachFileContentType() {
        return ugcAttachFileContentType;
    }

    public void setUgcAttachFileContentType(String ugcAttachFileContentType) {
        this.ugcAttachFileContentType = ugcAttachFileContentType;
    }

    public String getUgcAttachFileName() {
        return ugcAttachFileName;
    }

    public void setUgcAttachFileName(String ugcAttachFileName) {
        this.ugcAttachFileName = ugcAttachFileName;
    }

    public LocalDate getDateCrated() {
        return dateCrated;
    }

    public void setDateCrated(LocalDate dateCrated) {
        this.dateCrated = dateCrated;
    }

    public LocalDate getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDate dateModified) {
        this.dateModified = dateModified;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public EduBoard getEduBoard() {
        return eduBoard;
    }

    public void setEduBoard(EduBoard eduBoard) {
        this.eduBoard = eduBoard;
    }

    public EduLevel getEduLevel() {
        return eduLevel;
    }

    public void setEduLevel(EduLevel eduLevel) {
        this.eduLevel = eduLevel;
    }

    public InstEmployee getInstEmployee() {
        return instEmployee;
    }

    public void setInstEmployee(InstEmployee instEmployee) {
        this.instEmployee = instEmployee;
    }

    public String getSubject() { return Subject;}

    public void setSubject(String subject) { Subject = subject; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InstEmpEduQuali instEmpEduQuali = (InstEmpEduQuali) o;

        if ( ! Objects.equals(id, instEmpEduQuali.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InstEmpEduQuali{" +
            "id=" + id +
            ", certificateName='" + certificateName + "'" +
            ", board='" + board + "'" +
            ", session='" + session + "'" +
            ", semester='" + semester + "'" +
            ", rollNo='" + rollNo + "'" +
            ", passingYear='" + passingYear + "'" +
            ", cgpa='" + cgpa + "'" +
            ", certificateCopy='" + certificateCopy + "'" +
            ", certificateCopyContentType='" + certificateCopyContentType + "'" +
            ", status='" + status + "'" +
            ", groupSubject='" + groupSubject + "'" +
            ", resultPublishDate='" + resultPublishDate + "'" +
            '}';
    }
}
