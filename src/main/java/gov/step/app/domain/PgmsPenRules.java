package gov.step.app.domain;

import gov.step.app.domain.enumeration.penQuota;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PgmsPenRules.
 */
@Entity
@Table(name = "pgms_pen_rules")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pgmspenrules")
public class PgmsPenRules implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name="quota_status")
    private String quotaStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "quota_type")
    private penQuota quotaType;

    @Column(name = "min_age_limit")
    private Long minAgeLimit;

    @NotNull
    @Column(name = "max_age_limit", nullable = false)
    private Long maxAgeLimit;

    @NotNull
    @Column(name = "min_work_duration", nullable = false)
    private Long minWorkDuration;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date", nullable = false)
    private LocalDate createDate;

    @Column(name = "create_by" ,nullable = false)
    private Long createBy;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public penQuota getQuotaType() {
        return quotaType;
    }

    public void setQuotaType(penQuota quotaType) {
        this.quotaType = quotaType;
    }

    public Long getMinAgeLimit() {
        return minAgeLimit;
    }

    public void setMinAgeLimit(Long minAgeLimit) {
        this.minAgeLimit = minAgeLimit;
    }

    public Long getMaxAgeLimit() {
        return maxAgeLimit;
    }

    public void setMaxAgeLimit(Long maxAgeLimit) {
        this.maxAgeLimit = maxAgeLimit;
    }

    public Long getMinWorkDuration() {
        return minWorkDuration;
    }

    public void setMinWorkDuration(Long minWorkDuration) {
        this.minWorkDuration = minWorkDuration;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getQuotaStatus() {
        return quotaStatus;
    }

    public void setQuotaStatus(String quotaStatus) {
        this.quotaStatus = quotaStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PgmsPenRules pgmsPenRules = (PgmsPenRules) o;

        if ( ! Objects.equals(id, pgmsPenRules.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PgmsPenRules{" +
            "id=" + id +
            ", quotaType='" + quotaType + "'" +
            ", minAgeLimit='" + minAgeLimit + "'" +
            ", maxAgeLimit='" + maxAgeLimit + "'" +
            ", minWorkDuration='" + minWorkDuration + "'" +
            ", activeStatus='" + activeStatus + "'" +
            ", createDate='" + createDate + "'" +
            ", createBy='" + createBy + "'" +
            ", updateDate='" + updateDate + "'" +
            ", updateBy='" + updateBy + "'" +
            ",quotaStatus ='"+quotaStatus+"'"+
            '}';
    }
}
