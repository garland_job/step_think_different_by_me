package gov.step.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DmlAuditLogHistory.
 */
@Entity
@Table(name = "dml_audit_log_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dmlauditloghistory")
public class DmlAuditLogHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "col_name")
    private String colName;

    @Column(name = "value_before")
    private String valueBefore;

    @Column(name = "value_after")
    private String valueAfter;

    @Column(name = "app_user_id")
    private Long appUserId;

    @Column(name = "user_ip_address")
    private String userIpAddress;

    @Column(name = "user_hostname")
    private String userHostname;

    @Column(name = "os_user")
    private String osUser;

    @Column(name = "sessionid")
    private String sessionId;

    @Column(name = "event_dttm")
    private LocalDate eventDttm;

    @Column(name = "event_type")
    private String eventType;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "pk_col_value")
    private String pkColValue;

    @Column(name = "dml_sequence")
    private Long dmlSequence;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getValueBefore() {
        return valueBefore;
    }

    public void setValueBefore(String valueBefore) {
        this.valueBefore = valueBefore;
    }

    public String getValueAfter() {
        return valueAfter;
    }

    public void setValueAfter(String valueAfter) {
        this.valueAfter = valueAfter;
    }

    public Long getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(Long appUserId) {
        this.appUserId = appUserId;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }

    public void setUserIpAddress(String userIpAddress) {
        this.userIpAddress = userIpAddress;
    }

    public String getUserHostname() {
        return userHostname;
    }

    public void setUserHostname(String userHostname) {
        this.userHostname = userHostname;
    }

    public String getOsUser() {
        return osUser;
    }

    public void setOsUser(String osUser) {
        this.osUser = osUser;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDate getEventDttm() {
        return eventDttm;
    }

    public void setEventDttm(LocalDate eventDttm) {
        this.eventDttm = eventDttm;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getPkColValue() {
        return pkColValue;
    }

    public void setPkColValue(String pkColValue) {
        this.pkColValue = pkColValue;
    }

    public Long getDmlSequence() {
        return dmlSequence;
    }

    public void setDmlSequence(Long dmlSequence) {
        this.dmlSequence = dmlSequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DmlAuditLogHistory dmlAuditLogHistory = (DmlAuditLogHistory) o;

        if ( ! Objects.equals(id, dmlAuditLogHistory.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DmlAuditLogHistory{" +
            "id=" + id +
            ", tableName='" + tableName + "'" +
            ", colName='" + colName + "'" +
            ", valueBefore='" + valueBefore + "'" +
            ", valueAfter='" + valueAfter + "'" +
            ", appUserId='" + appUserId + "'" +
            ", userIpAddress='" + userIpAddress + "'" +
            ", userHostname='" + userHostname + "'" +
            ", osUser='" + osUser + "'" +
            ", sessionId='" + sessionId + "'" +
            ", eventDttm='" + eventDttm + "'" +
            ", eventType='" + eventType + "'" +
            ", eventName='" + eventName + "'" +
            ", pkColValue='" + pkColValue + "'" +
            ", dmlSequence='" + dmlSequence + "'" +
            '}';
    }
}
