package gov.step.app.web.rest.util;

/**
 * Created by shuvo on 5/14/16.
 */
public class Constants {

        public static final String LIBRARY_FILE_DIR_CONTENTUPLOAD = "assets/dlms/";
        public static final String FILE_DIR_JOBPOSTING = "/backup/jobPostingInfo/";
        public static final String ATTACHMENT_FILE_DIR_DIGITALLIBRARY = "/backup/dlImages/";
    }

