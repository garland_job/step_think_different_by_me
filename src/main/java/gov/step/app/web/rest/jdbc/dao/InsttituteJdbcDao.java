package gov.step.app.web.rest.jdbc.dao;

import gov.step.app.domain.Institute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by rana on 2/10/16.
 */
@Component
public class InsttituteJdbcDao
{
    private final Logger logger = LoggerFactory.getLogger(InsttituteJdbcDao.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InsttituteJdbcDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Map<String,Object>> findInstgenInfoByStatus(Integer status1,Integer status2){
        return jdbcTemplate.queryForList(" select code,name,submited_date,CASE WHEN status is null THEN 0 ELSE status END AS status " +
            "from inst_gen_info where status=? or status=? order by submited_date ",status1,status2);
    }
    public List<Map<String,Object>> findInstgenInfopendingList(Integer status1){
        return jdbcTemplate.queryForList(" select code,name,submited_date,CASE WHEN status is null THEN 0 ELSE status END AS status " +
            "from inst_gen_info where status is null or status =0 or status>? order by submited_date ",status1);
    }

    private final String SQL_SELECT_ALL_INST         = "SELECT * FROM INSTITUTE";

    public List<Institute> getCompactInstituteList()
    {
        logger.info("getCompactInstituteList : "+SQL_SELECT_ALL_INST);
        List<Institute> instituteList = null;
        try
        {
            instituteList = jdbcTemplate.query(SQL_SELECT_ALL_INST,
                new RowMapper<Institute>() {
                    public Institute mapRow(ResultSet result, int rowNum) throws SQLException {
                        Institute instInfo = new Institute();
                        try
                        {
                            instInfo.setId(result.getLong("ID"));
                            instInfo.setName(result.getString("NAME"));
                            instInfo.setMpoCode(result.getString("MPO_CODE"));
                            instInfo.setCode(result.getString("CODE"));
                            instInfo.setMobile(result.getString("MOBILE"));

                        } catch (Exception ex) {
                            logger.error("ColumnObjectConversion Error: " + ex.getMessage());
                        }
                        return instInfo;
                    }
                });
        }
        catch(Exception ex)
        {
            logger.error("getCompactInstituteList Msg: "+ex.getMessage());
        }
        return instituteList;
    }

}
