package gov.step.app.web.rest.jdbc.dao;

import gov.step.app.domain.HrEmployeeInfo;
import org.springframework.http.HttpHeaders;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by rana on 1/1/18.
 */
@Service
public class EntityManagerService {

    @PersistenceContext
    private EntityManager em;

    public static String ALM_ATTENDANCE_CONFIGURATIONS = "SELECT  ass.shift_name, ei.full_name, aac.effected_date_from, " +
        " aac.effected_date_to, aac.is_until_further_notice, aac.active_status, aac.id conf_id, ass.id shift_id , ei.id emp_id" +
        " FROM alm_attendance_configuration aac " +
        " JOIN alm_shift_setup ass on (ass.id = aac.alm_shift_setup_id)" +
        " JOIN hr_employee_info ei on (aac.employee_info_id = ei.id) " +
        " ORDER BY ass.shift_name";

    public static String ALM_APPLICATION_APPROVER = "SELECT emp.* FROM HR_EMPLOYEE_INFO emp " +
        "  JOIN JHI_USER_AUTHORITY jua ON (emp.USER_ID = jua.USER_ID) " +
        "  JOIN HR_GRADE_SETUP hr_grade ON (hr_grade.id = emp.GRADE_INFO_ID) " +
        "  WHERE (jua.AUTHORITY_NAME  = 'ROLE_LEAVE_APPROVAL' AND hr_grade.GRADE_CODE >= :gradeCode)  " +
        "      OR jua.AUTHORITY_NAME  = 'ROLE_DG'";

    public List<Object[]> getObjects(Pageable pageable, String query) {
        Query q = em.createNativeQuery(query);
        if(pageable!=null && pageable.getPageNumber() > 0){
            q.setFirstResult(( pageable.getPageNumber()-1)* pageable.getPageSize());
            q.setMaxResults(pageable.getPageSize());
        }
        return q.getResultList();
    }
    public List<HrEmployeeInfo> getHrEmployeeByDept(Long deptId) {
        String query = "select hre.* from HR_EMPLOYEE_INFO hre " +
            "left join ALM_ATTENDANCE_CONFIGURATION aac on (aac.EMPLOYEE_INFO_ID = hre.id) " +
            "where aac.id is null and hre.department_info_id = :deptId ";

        Query q = em.createNativeQuery(query, HrEmployeeInfo.class);
        q.setParameter("deptId",deptId);
        return  (List<HrEmployeeInfo>) q.getResultList();
    }
    public int getTotal(String query) {
        Query q = em.createNativeQuery(query);
        return q.getMaxResults();
    }
    public HttpHeaders generatePaginationHttpHeaders(Pageable pageable, String query, String baseUrl)
        throws URISyntaxException {
        Query q = em.createNativeQuery(query);
        HttpHeaders headers = new HttpHeaders();
        int totalResults = q.getMaxResults();
        headers.add("X-Total-Count", "" + totalResults);
        String link = "";
        int pageNumber = 0;
        int totalPages = 0;
        int pageSize = 0;
        if (pageable != null) {
            pageNumber = pageable.getPageNumber();
            pageSize = pageable.getPageSize();
            totalPages = totalResults / pageable.getPageSize();
        }

        if ((pageNumber + 1) < totalPages) {
            link = "<" + (new URI(baseUrl + "?page=" + (pageNumber + 1) + "&size=" + pageSize)).toString() + ">; rel=\"next\",";
        }
        if (pageNumber > 0) {
            link += "<" + (new URI(baseUrl + "?page=" + (pageNumber - 1) + "&size=" + pageSize)).toString() + ">; rel=\"prev\",";
        }
        // last and first link
        link += "<" + (new URI(baseUrl + "?page=" + (totalPages - 1) + "&size=" + pageSize)).toString() + ">; rel=\"last\",";
        link += "<" + (new URI(baseUrl + "?page=" + 0 + "&size=" + pageSize)).toString() + ">; rel=\"first\"";
        headers.add(HttpHeaders.LINK, link);
        return headers;
    }


    public List<HrEmployeeInfo> getAlmLeaveApprovar(String gradeCode) {
        Query q = em.createNativeQuery(ALM_APPLICATION_APPROVER, HrEmployeeInfo.class);
        q.setParameter("gradeCode", gradeCode);
        return  (List<HrEmployeeInfo>) q.getResultList();
    }
}
