package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.designationType;
import gov.step.app.domain.payroll.PrlSalaryStructureInfo;
import gov.step.app.repository.*;
import gov.step.app.repository.payroll.PrlSalaryStructureInfoRepository;
import gov.step.app.repository.search.HrEmpPromotionInfoSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.HrmConversionService;
import gov.step.app.service.constnt.HRMManagementConstant;
import gov.step.app.service.util.MiscFileInfo;
import gov.step.app.service.util.MiscFileUtilities;
import gov.step.app.web.rest.dto.HrmApprovalDto;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import gov.step.app.web.rest.util.DateResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing HrEmpPromotionInfo.
 */
@RestController
@RequestMapping("/api")
public class HrEmpPromotionInfoResource
{
    private final Logger log = LoggerFactory.getLogger(HrEmpPromotionInfoResource.class);

    @Inject
    private HrEmpPromotionInfoRepository hrEmpPromotionInfoRepository;

    @Inject
    private HrEmpPromotionInfoSearchRepository hrEmpPromotionInfoSearchRepository;

    @Inject
    private HrEmpPromotionInfoLogRepository hrEmpPromotionInfoLogRepository;

    @Inject
    private HrEmployeeInfoRepository hrEmployeeInfoRepository;

    @Inject
    private HrEmployeeInfoLogRepository hrEmployeeInfoLogRepository;

    @Inject
    private HrmConversionService conversionService;

    @Inject
    private InstituteRepository instituteRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private HrEmploymentInfoRepository hrEmploymentInfoRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private InstEmployeeRepository instEmployeeRepository;

    @Inject
    private PrlSalaryStructureInfoRepository prlSalaryStructureInfoRepository;




    MiscFileUtilities fileUtils = new MiscFileUtilities();

    DateResource dateResrc = new DateResource();

    /**
     * POST  /hrEmpPromotionInfos -> Create a new hrEmpPromotionInfo.
     */
    @RequestMapping(value = "/hrEmpPromotionInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<HrEmpPromotionInfo> createHrEmpPromotionInfo(@Valid @RequestBody HrEmpPromotionInfo hrEmpPromotionInfo) throws URISyntaxException {
        log.debug("REST request to save HrEmpPromotionInfo : {}", hrEmpPromotionInfo);
        if (hrEmpPromotionInfo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("hrEmpPromotionInfo", "idexists", "A new hrEmpPromotionInfo cannot already have an ID")).body(null);
        }
        if(hrEmpPromotionInfo.getLogStatus() < HRMManagementConstant.APPROVAL_STATUS_ADMIN_ENTRY)
        {
            hrEmpPromotionInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_LOCKED);
        }


        setHrmEmployeeInfoProperties(hrEmpPromotionInfo);
        //Saving Go Order Document to Dir.
        MiscFileInfo goFile = new MiscFileInfo();
        goFile.fileData(hrEmpPromotionInfo.getGoDoc())
            .fileName(hrEmpPromotionInfo.getGoDocName())
            .contentType(hrEmpPromotionInfo.getGoDocContentType())
            .filePath(HRMManagementConstant.PROMOTION_FILE_DIR);

        goFile = fileUtils.saveFileAsByte(goFile);
        hrEmpPromotionInfo.setGoDoc(new byte[1]);
        hrEmpPromotionInfo.setGoDocName(goFile.fileName());

        HrEmpPromotionInfo result = hrEmpPromotionInfoRepository.save(hrEmpPromotionInfo);
        hrEmpPromotionInfoSearchRepository.save(result);

        // Please enable these code from your ide, check whther it give any error or not.

        // Saving employee current information to LOG before updating.
        // With specific LogStatus value for this operation.

        HrEmployeeInfoLog empLogInfo = new HrEmployeeInfoLog();
        HrEmployeeInfo dbEmpModelInfo = hrEmployeeInfoRepository.findOne(hrEmpPromotionInfo.getEmployeeInfo().getId());
        empLogInfo = conversionService.getLogFromSource(dbEmpModelInfo);
        empLogInfo.setLogStatus(HRMManagementConstant.PROMOTION_EMPLOYEE_LOG_STATUS);
        // in this ActionComments, you can add current employeeType (designationType) to target employeeType.

        empLogInfo.setActionComments(designationType.Teacher.toString()+"-"+designationType.DTE_Employee.toString());

        empLogInfo.setCreateDate(dateResrc.getDateAsLocalDate());

        empLogInfo = hrEmployeeInfoLogRepository.save(empLogInfo);

        //if(hrEmpPromotionInfo.getEmployeeInfo().getOrganizationType().equalsIgnoreCase("organization"))
        if(hrEmpPromotionInfo.getOrganizationToId() != null && hrEmpPromotionInfo.getInstituteToId() == null)
        {
              // update fields value for organization type employee
              hrEmployeeInfoRepository.updateEmployeeForDteTransfer(dbEmpModelInfo.getId(), designationType.DTE_Employee, dbEmpModelInfo.getOrganizationType(),
                  result.getOrganizationToId().getId(), result.getOrgDetailsId().getId(), result.getDepartmentToId().getId(), result.getDesignationToId().getId(), dbEmpModelInfo.getLogId());
        }
        else if(hrEmpPromotionInfo.getOrganizationToId() == null && hrEmpPromotionInfo.getInstituteToId() != null)
        {
              Institute institute = instituteRepository.findOne(result.getInstituteToId().getId());
              // update fields value for institute type employee
              hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(dbEmpModelInfo.getId(), designationType.Teacher, dbEmpModelInfo.getOrganizationType(),
                  institute.getInstCategory().getId(),institute.getInstLevel().getId() ,result.getInstituteToId().getId(), result.getDepartmentToId().getId(), result.getDesignationToId().getId(), dbEmpModelInfo.getLogId());
        }



        return ResponseEntity.created(new URI("/api/hrEmpPromotionInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("hrEmpPromotionInfo", result.getId().toString()))
            .body(result);
    }


    /**
     * PUT  /hrEmpPromotionInfos -> Updates an existing hrEmpPromotionInfo.
     */
    @RequestMapping(value = "/hrEmpPromotionInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpPromotionInfo> updateHrEmpPromotionInfo(@Valid @RequestBody HrEmpPromotionInfo hrEmpPromotionInfo) throws URISyntaxException {
        log.debug("REST request to update HrEmpPromotionInfo : {}", hrEmpPromotionInfo);
        if (hrEmpPromotionInfo.getId() == null) {
            return createHrEmpPromotionInfo(hrEmpPromotionInfo);
        }

        //Saving Go Order Document to Dir.
        MiscFileInfo goFile = new MiscFileInfo();
        goFile.fileData(hrEmpPromotionInfo.getGoDoc())
            .fileName(hrEmpPromotionInfo.getGoDocName())
            .contentType(hrEmpPromotionInfo.getGoDocContentType())
            .filePath(HRMManagementConstant.PROMOTION_FILE_DIR);

        goFile = fileUtils.updateFileAsByte(goFile);
        hrEmpPromotionInfo.setGoDoc(new byte[1]);
        hrEmpPromotionInfo.setGoDocName(goFile.fileName());

        // Add LOG info for Approval Purpose.
        HrEmpPromotionInfoLog logInfo = new HrEmpPromotionInfoLog();
        HrEmpPromotionInfo dbModelInfo = hrEmpPromotionInfoRepository.findOne(hrEmpPromotionInfo.getId());
        logInfo = conversionService.getLogFromSource(dbModelInfo);
        logInfo.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
        logInfo = hrEmpPromotionInfoLogRepository.save(logInfo);
        hrEmpPromotionInfo.setLogId(logInfo.getId());
        hrEmpPromotionInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_LOCKED);
        if(hrEmpPromotionInfo.getInstituteFromId()==null){
            hrEmpPromotionInfo.setInstituteFromId(null);
            hrEmpPromotionInfo.setInstituteToId(null);

        }
        if(hrEmpPromotionInfo.getOrganizationFromId()==null){
            hrEmpPromotionInfo.setOrganizationFromId(null);
            hrEmpPromotionInfo.setOrganizationToId(null);

        }

        HrEmpPromotionInfo result = hrEmpPromotionInfoRepository.save(hrEmpPromotionInfo);
        hrEmpPromotionInfoSearchRepository.save(result);

        //Please enable these code from your ide, check whther it give any error or not.

        // Saving employee current information to LOG before updating.
        // With specific LogStatus value for this operation.

       /* HrEmployeeInfoLog empLogInfo = new HrEmployeeInfoLog();
        HrEmployeeInfo dbEmpModelInfo = hrEmployeeInfoRepository.findOne(hrEmpPromotionInfo.getEmployeeInfo().getId());
        empLogInfo = conversionService.getLogFromSource(dbEmpModelInfo);
        empLogInfo.setLogStatus(HRMManagementConstant.PROMOTION_EMPLOYEE_LOG_STATUS);

        // in this ActionComments, you can add current employeeType (designationType) to target employeeType.

        empLogInfo.setActionComments(designationType.Teacher.toString()+"-"+designationType.DTE_Employee.toString());

        empLogInfo.setCreateDate(dateResrc.getDateAsLocalDate());

        empLogInfo = hrEmployeeInfoLogRepository.save(empLogInfo);

        if(hrEmpPromotionInfo.getEmployeeInfo().getOrganizationType().equalsIgnoreCase("organization"))
        {
              // update fields value for organization type employee
              hrEmployeeInfoRepository.updateEmployeeForDteTransfer(hrEmpPromotionInfo.getEmployeeInfo().getId(), designationType.DTE_Employee, dbEmpModelInfo.getOrganizationType(),
                  dbEmpModelInfo.getWorkArea().getId(), dbEmpModelInfo.getWorkAreaDtl().getId(), dbEmpModelInfo.getDepartmentInfo().getId(), dbEmpModelInfo.getDesignationInfo().getId(), dbEmpModelInfo.getLogId());
        }
        else
        {
            // update fields value for institute type employee
              hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(hrEmpPromotionInfo.getEmployeeInfo().getId(), designationType.Teacher, dbEmpModelInfo.getOrganizationType(),
                  dbEmpModelInfo.getInstCategory().getId(), dbEmpModelInfo.getInstitute().getId(), dbEmpModelInfo.getDepartmentInfo().getId(), dbEmpModelInfo.getDesignationInfo().getId(), dbEmpModelInfo.getLogId());
        }*/

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("hrEmpPromotionInfo", hrEmpPromotionInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hrEmpPromotionInfos -> get all the hrEmpPromotionInfos.
     */
    @RequestMapping(value = "/hrEmpPromotionInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmpPromotionInfo>> getAllHrEmpPromotionInfos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrEmpPromotionInfos");
        Page<HrEmpPromotionInfo> page = hrEmpPromotionInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrEmpPromotionInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrEmpPromotionInfos/:id -> get the "id" hrEmpPromotionInfo.
     */
    @RequestMapping(value = "/hrEmpPromotionInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpPromotionInfo> getHrEmpPromotionInfo(@PathVariable Long id) {
        log.debug("REST request to get HrEmpPromotionInfo : {}", id);
        HrEmpPromotionInfo hrEmpPromotionInfo = hrEmpPromotionInfoRepository.findOne(id);

        MiscFileInfo goFile = new MiscFileInfo();
        goFile.fileName(hrEmpPromotionInfo.getGoDocName())
            .contentType(hrEmpPromotionInfo.getGoDocContentType())
            .filePath(HRMManagementConstant.PROMOTION_FILE_DIR);
        goFile = fileUtils.readFileAsByte(goFile);
        hrEmpPromotionInfo.setGoDoc(goFile.fileData());

        return Optional.ofNullable(hrEmpPromotionInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /hrEmpPromotionInfos/:id -> delete the "id" hrEmpPromotionInfo.
     */
    @RequestMapping(value = "/hrEmpPromotionInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteHrEmpPromotionInfo(@PathVariable Long id) {
        log.debug("REST request to delete HrEmpPromotionInfo : {}", id);
        hrEmpPromotionInfoRepository.delete(id);
        hrEmpPromotionInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("hrEmpPromotionInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/hrEmpPromotionInfos/:query -> search for the hrEmpPromotionInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/hrEmpPromotionInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpPromotionInfo> searchHrEmpPromotionInfos(@PathVariable String query) {
        log.debug("REST request to search HrEmpPromotionInfos for query {}", query);
        return StreamSupport
            .stream(hrEmpPromotionInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /hrEmpTransferInfos -> get all the hrEmpTransferInfos/my.
     */
    @RequestMapping(value = "/hrEmpPromotionInfos/my",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpPromotionInfo> getAllHrTransferInfosByCurrentEmployee()
        throws URISyntaxException
    {
        log.debug("REST request to get a page of HrPromotion by LoggedIn Employee");
        List<HrEmpPromotionInfo> promotionList = hrEmpPromotionInfoRepository.findAllByEmployeeIsCurrentUser();
        for(HrEmpPromotionInfo hrEmpPromotionInfo : promotionList)
        {
            MiscFileInfo goFile = new MiscFileInfo();
            goFile.fileName(hrEmpPromotionInfo.getGoDocName())
                .contentType(hrEmpPromotionInfo.getGoDocContentType())
                .filePath(HRMManagementConstant.PROMOTION_FILE_DIR);
            goFile = fileUtils.readFileAsByte(goFile);
            hrEmpPromotionInfo.setGoDoc(goFile.fileData());
        }
        return promotionList;
    }

    /**
     * GET  /hrEmpPromotionInfosApprover/ -> process the approval request.
     */
    @RequestMapping(value = "/hrEmpPromotionInfosApprover",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> updateModelApproval(@Valid @RequestBody HrmApprovalDto approvalDto) {
        log.debug("REST request to Approve hrEmpPromotionInfo POST: Type: {} ID: {}, comment : {}",approvalDto.getActionType(), approvalDto.getEntityId(), approvalDto.getLogComments());

        HrEmpPromotionInfo modelInfo = hrEmpPromotionInfoRepository.findOne(approvalDto.getEntityId());

        if(approvalDto.getActionType().equalsIgnoreCase(HRMManagementConstant.APPROVAL_LOG_STATUS_ACCEPT))
        {
            log.debug("REST request to APROVING ID: {}", approvalDto.getEntityId());
            modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            modelInfo.setActiveStatus(true);
            modelInfo = hrEmpPromotionInfoRepository.save(modelInfo);
            if(modelInfo.getLogId() != 0)
            {
                HrEmpPromotionInfoLog modelLog = hrEmpPromotionInfoLogRepository.findOne(modelInfo.getLogId());
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmpPromotionInfoLogRepository.save(modelLog);
            }
        }
        else
        {
            log.debug("REST request to REJECTING ID: {}", approvalDto.getEntityId());
            if(modelInfo.getLogId() != 0)
            {
                HrEmpPromotionInfoLog modelLog = hrEmpPromotionInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmpPromotionInfoLogRepository.save(modelLog);

                modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo.setActiveStatus(false);
                modelInfo = hrEmpPromotionInfoRepository.save(modelInfo);
            }
            else
            {
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setActiveStatus(false);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo = hrEmpPromotionInfoRepository.save(modelInfo);
            }
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("hrEmpPromotionInfo", approvalDto.getEntityId().toString())).build();
    }

    /**
     * GET  /hrEmpPromotionInfosApprover/:logStatus -> get address list by log status.
     */
    @RequestMapping(value = "/hrEmpPromotionInfosApprover/{logStatus}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getModelListByLogStatus(@PathVariable Long logStatus)
    {
        log.debug("REST request to Approve hrEmpPromotionInfo List : logStatus: {} ",logStatus);
        List<HrEmpPromotionInfo> modelList = hrEmpPromotionInfoRepository.findAllByLogStatus(logStatus);

        HrEmpPromotionInfoLog modelLogInfo = null;
        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        HrmApprovalDto dtoInfo = null;
        for(HrEmpPromotionInfo modelInfo : modelList)
        {
            dtoInfo = new HrmApprovalDto();
            dtoInfo.setEntityObject(modelInfo);
            if(modelInfo.getLogId() != 0)
            {
                modelLogInfo = hrEmpPromotionInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                dtoInfo.setEntityLogObject(modelLogInfo);
            }
            modelDtoList.add(dtoInfo);
        }
        return modelDtoList;
    }

    /**
     * GET  /hrEmpPromotionInfosApprover/log/:entityId -> load model and LogModel by entity id
     */
    @RequestMapping(value = "/hrEmpPromotionInfosApprover/log/{entityId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public HrmApprovalDto getModelAndLogObjectByModelId(@PathVariable String entityId) {
        log.debug("REST request to Log hrEmpPromotionInfo Model and Log of id: {} ",entityId);

        Long id = Long.parseLong(entityId);
        HrmApprovalDto approvalDto = new HrmApprovalDto();
        HrEmpPromotionInfo modelInfo = hrEmpPromotionInfoRepository.findOne(id);
        approvalDto.setEntityObject(modelInfo);
        approvalDto.setEntityId(id);
        approvalDto.setEntityName("Education");
        if(modelInfo.getLogId() != 0)
        {
            HrEmpPromotionInfoLog modelLog = hrEmpPromotionInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            approvalDto.setEntityLogObject(modelLog);
        }
        return approvalDto;
    }

    private void setHrmEmployeeInfoProperties(HrEmpPromotionInfo hrEmpPromotionInfo) {
        HrEmployeeInfo employeeInfo = hrEmployeeInfoRepository.findOne(hrEmpPromotionInfo.getEmployeeInfo().getId());
        employeeInfo.setJobCategory(hrEmpPromotionInfo.getJobCategory());
        employeeInfo.setDepartmentInfo(hrEmpPromotionInfo.getDepartmentToId());
        employeeInfo.setDesignationInfo(hrEmpPromotionInfo.getDesignationToId());
        employeeInfo.setOfficeOrderDate(hrEmpPromotionInfo.getOrderDate());
        employeeInfo.setOfficeOrderNumber(hrEmpPromotionInfo.getOfficeOrderNo());
        employeeInfo.setEmployeeType(hrEmpPromotionInfo.getEmployeeType());
        employeeInfo.setApointmentGoDate(hrEmpPromotionInfo.getGoDate());

        PrlSalaryStructureInfo prlSalaryStructureInfo=prlSalaryStructureInfoRepository.finAlldByEmployeeId(employeeInfo.getId());

        employeeInfo.setOrganizationType(hrEmpPromotionInfo.getToOrganizationType());
        prlSalaryStructureInfo.setBasicAmount(hrEmpPromotionInfo.getPayscaleTo());
        prlSalaryStructureInfo.setEffectiveDate(hrEmpPromotionInfo.getJoiningDate());
        prlSalaryStructureInfo.setUpdateBy(SecurityUtils.getCurrentUserId());
        prlSalaryStructureInfo.setUpdateDate(LocalDate.now());
        prlSalaryStructureInfo.setPayscaleBasicInfo(hrEmpPromotionInfo.getPrlPayscaleBasicInfo());
        prlSalaryStructureInfo.setPayscaleInfo(hrEmpPromotionInfo.getPrlPayscaleBasicInfo().getPayscaleInfo());
        prlSalaryStructureInfo.setEmployeeInfo(employeeInfo);
        if(hrEmpPromotionInfo.getPrlPayscaleBasicInfo().getPayscaleInfo()!=null)
            prlSalaryStructureInfo.setGazetteInfo(hrEmpPromotionInfo.getPrlPayscaleBasicInfo().getPayscaleInfo().getGazetteInfo());


        if(!hrEmpPromotionInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpPromotionInfo.getToOrganizationType())){
            employeeInfo.setDateOfJoining(hrEmpPromotionInfo.getJoiningDate());
            if(hrEmpPromotionInfo.getToOrganizationType().equalsIgnoreCase("Institute")) {      /*Emp->Promotion: ORG->INST*/
                changeInstitute(hrEmpPromotionInfo,employeeInfo);


                try{
                    InstEmployee instEmployee = conversionService.hrEmpToInstEmpConversion(employeeInfo);
                    InstEmployee result = instEmployeeRepository.save(instEmployee);
                    employeeInfo.setInstEmployee(result);
                    hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(employeeInfo.getId(), hrEmpPromotionInfo.getDesignationToId().getDesigType(), hrEmpPromotionInfo.getToOrganizationType(),
                        hrEmpPromotionInfo.getInstituteToId().getInstCategory().getId(),hrEmpPromotionInfo.getInstituteToId().getInstLevel().getId() ,hrEmpPromotionInfo.getInstituteToId().getId(),
                        hrEmpPromotionInfo.getDepartmentToId().getId(), hrEmpPromotionInfo.getDesignationToId().getId(), employeeInfo.getLogId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {  /*IF INST-> ORG*/
                changeOrganization(hrEmpPromotionInfo,employeeInfo);
                InstEmployee instEmployee= employeeInfo.getInstEmployee();
                if(instEmployee!=null) {
                    instEmployee.setStatus(-5);
                    employeeInfo.setInstEmployee(null);
                    instEmployeeRepository.save(instEmployee);
                }
            }
        }
        else {
            if(hrEmpPromotionInfo.getToOrganizationType().equalsIgnoreCase("Institute")) { /*IF INST->INST*/
                if(hrEmpPromotionInfo.getInstituteFromId().getId()!=hrEmpPromotionInfo.getInstituteToId().getId()) {
                    employeeInfo.setDateOfJoining(hrEmpPromotionInfo.getJoiningDate());
                    changeInstitute(hrEmpPromotionInfo,employeeInfo);

                    try {
                        InstEmployee instEmployee=conversionService.hrEmpToInstEmpConversion(employeeInfo);
                        instEmployeeRepository.save(instEmployee);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
            else {
                if(hrEmpPromotionInfo.getOrganizationFromId().getId()!=hrEmpPromotionInfo.getOrganizationToId().getId()) {
                    employeeInfo.setDateOfJoining(hrEmpPromotionInfo.getJoiningDate());
                    changeOrganization(hrEmpPromotionInfo,employeeInfo);
                }
            }
        }

        Authority govtNonGovtInstAuthority  = authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
        Authority hrmEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME);
        Authority dteEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);

        if(govtNonGovtInstAuthority==null)
        {
            govtNonGovtInstAuthority=new Authority();
            govtNonGovtInstAuthority.setName(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            authorityRepository.save(govtNonGovtInstAuthority);
        }
        if(hrmEmployeeAuthority==null)
        {
            hrmEmployeeAuthority=new Authority();
            hrmEmployeeAuthority.setName(HRMManagementConstant.ROLE_HRM_USER_NAME);
            authorityRepository.save(hrmEmployeeAuthority);
        }
        if(dteEmployeeAuthority==null)
        {
            dteEmployeeAuthority=new Authority();
            dteEmployeeAuthority.setName(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
            authorityRepository.save(dteEmployeeAuthority);
        }


        User user=userRepository.findOne(employeeInfo.getUser().getId());
        Set<Authority> authorities=user.getAuthorities();

        if(hrEmpPromotionInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)) {
            if(! hrEmpPromotionInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpPromotionInfo.getToOrganizationType())) {
                if (!authorities.contains(govtNonGovtInstAuthority) && !authorities.contains(hrmEmployeeAuthority)) {
                    authorities.add(govtNonGovtInstAuthority);
                    authorities.add(hrmEmployeeAuthority);
                    authorities.remove(dteEmployeeAuthority);
                    user.setAuthorities(authorities);
                    userRepository.save(user);
                } else if (!authorities.contains(govtNonGovtInstAuthority) && authorities.contains(hrmEmployeeAuthority)) {
                    authorities.add(govtNonGovtInstAuthority);
                    authorities.remove(dteEmployeeAuthority);
                    user.setAuthorities(authorities);
                    userRepository.save(user);
                } else if (authorities.contains(govtNonGovtInstAuthority) && !authorities.contains(hrmEmployeeAuthority)) {
                    authorities.add(hrmEmployeeAuthority);
                    authorities.remove(dteEmployeeAuthority);
                    user.setAuthorities(authorities);
                    userRepository.save(user);
                }
            }

        }
        else
        {
            if(! hrEmpPromotionInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpPromotionInfo.getToOrganizationType()))
            {
                if(!authorities.contains(dteEmployeeAuthority))
                {
                    authorities.add(dteEmployeeAuthority);
                    authorities.remove(hrmEmployeeAuthority);
                    authorities.remove(govtNonGovtInstAuthority);
                    user.setAuthorities(authorities);
                    userRepository.save(user);
                }
                else
                {
                    authorities.remove(hrmEmployeeAuthority);
                    authorities.remove(govtNonGovtInstAuthority);
                    user.setAuthorities(authorities);
                    userRepository.save(user);
                }
            }
        }

        HrEmploymentInfo hrEmploymentInfo=conversionService.convertEmployeeInfoToEmploymentInfo(employeeInfo);
        hrEmploymentInfo.setSalaryStructureInfo(prlSalaryStructureInfo);
        hrEmploymentInfoRepository.save(hrEmploymentInfo);

        prlSalaryStructureInfoRepository.save(prlSalaryStructureInfo);
        hrEmployeeInfoRepository.save(employeeInfo);
    }

    private void changeOrganization(HrEmpPromotionInfo hrEmpPromotionInfo,HrEmployeeInfo employeeInfo) {
        employeeInfo.setWorkAreaDtl(hrEmpPromotionInfo.getOrgDetailsId());
        employeeInfo.setWorkArea(hrEmpPromotionInfo.getOrgDetailsId().getWorkArea());

        employeeInfo.setInstitute(null);
        employeeInfo.setInstLevel(null);
        employeeInfo.setInstCategory(null);
        employeeInfo.setInstEmployee(null);

        HrEmploymentInfo hrEmploymentInfo = null;
        PrlSalaryStructureInfo salaryStructureInfo = null;
        Long employmentId = 0l;
        Long salaryStructureId = 0l;

        try{
            hrEmploymentInfo = hrEmploymentInfoRepository.findByEmployeeId(hrEmploymentInfo.getEmployeeInfo().getId());
            salaryStructureInfo = prlSalaryStructureInfoRepository.finAlldByEmployeeId(hrEmploymentInfo.getEmployeeInfo().getId());
            employmentId    = hrEmploymentInfo.getId();
            salaryStructureId   = salaryStructureInfo.getId();
        }catch(Exception ex){
            employmentId = 0l;
        }

        try{
            hrEmploymentInfoRepository.updateCurrentEmploymentInformationOfOrganization(employmentId, hrEmpPromotionInfo.getToOrganizationType(),
                hrEmpPromotionInfo.getOrganizationToId().getId(), hrEmpPromotionInfo.getDepartmentToId().getId(),
                hrEmpPromotionInfo.getDesignationToId().getId(), salaryStructureId);
        }catch(Exception ex){
            log.error("Organization Employment Update Exception", ex);
        }
    }

    private void changeInstitute(HrEmpPromotionInfo hrEmpPromotionInfo,HrEmployeeInfo employeeInfo) {
        employeeInfo.setInstitute(hrEmpPromotionInfo.getInstituteToId());
        employeeInfo.setInstLevel(hrEmpPromotionInfo.getInstituteToId().getInstLevel());
        employeeInfo.setInstCategory(hrEmpPromotionInfo.getInstituteToId().getInstCategory());

        employeeInfo.setWorkAreaDtl(null);
        employeeInfo.setWorkArea(null);

        HrEmploymentInfo hrEmploymentInfo = null;
        PrlSalaryStructureInfo salaryStructureInfo = null;
        Long employmentId = 0l;
        Long salaryStructureId = 0l;

        try{
            hrEmploymentInfo = hrEmploymentInfoRepository.findByEmployeeId(hrEmploymentInfo.getEmployeeInfo().getId());
            salaryStructureInfo = prlSalaryStructureInfoRepository.finAlldByEmployeeId(hrEmploymentInfo.getEmployeeInfo().getId());
            employmentId    = hrEmploymentInfo.getId();
            salaryStructureId   = salaryStructureInfo.getId();
        }catch(Exception ex){
            employmentId = 0l;
        }


        try{
            hrEmploymentInfoRepository.updateCurrentEmploymentInformationInsitute(employmentId, hrEmpPromotionInfo.getToOrganizationType(),
                hrEmpPromotionInfo.getInstituteToId().getId(), hrEmpPromotionInfo.getDepartmentToId().getId(),
                hrEmpPromotionInfo.getDesignationToId().getId(), salaryStructureId);
        }catch(Exception ex){
            log.error("Institute Employment Update Exception", ex);
        }

    }
}
