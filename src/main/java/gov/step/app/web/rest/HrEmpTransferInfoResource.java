package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.designationType;
import gov.step.app.domain.payroll.PrlSalaryStructureInfo;
import gov.step.app.repository.*;
import gov.step.app.repository.payroll.PrlSalaryStructureInfoRepository;
import gov.step.app.repository.search.HrEmpTransferInfoSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.HrmConversionService;
import gov.step.app.service.MailService;
import gov.step.app.service.constnt.HRMManagementConstant;
import gov.step.app.service.util.MiscFileInfo;
import gov.step.app.service.util.MiscFileUtilities;
import gov.step.app.web.rest.dto.HrmApprovalDto;
import gov.step.app.web.rest.util.DateResource;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing HrEmpTransferInfo.
 */
@RestController
@RequestMapping("/api")
public class HrEmpTransferInfoResource {

    private final Logger log = LoggerFactory.getLogger(HrEmpTransferInfoResource.class);

    @Inject
    private HrEmpTransferInfoRepository hrEmpTransferInfoRepository;

    @Inject
    private HrEmpTransferInfoSearchRepository hrEmpTransferInfoSearchRepository;

    @Inject
    private HrEmpTransferInfoLogRepository hrEmpTransferInfoLogRepository;

    @Inject
    private HrmConversionService conversionService;

    MiscFileUtilities fileUtils = new MiscFileUtilities();

    @Inject
    private HrEmployeeInfoRepository hrEmployeeInfoRepository;

    @Inject
    private HrEmployeeInfoLogRepository hrEmployeeInfoLogRepository;

    @Inject
    private HrEmploymentInfoRepository hrEmploymentInfoRepository;

    @Inject
    private PrlSalaryStructureInfoRepository prlSalaryStructureInfoRepository;

    @Inject
    private MailService mailService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private HrDepartmentHeadInfoRepository hrDepartmentHeadInfoRepository;

    @Inject
    private InstituteRepository instituteRepository;

    DateResource dateResrc = new DateResource();

    @Inject
    private InstEmployeeRepository instEmployeeRepository;


    /**
     * POST  /hrEmpTransferInfos -> Create a new hrEmpTransferInfo.
     */
    @RequestMapping(value = "/hrEmpTransferInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<HrEmpTransferInfo> createHrEmpTransferInfo(@Valid @RequestBody HrEmpTransferInfo hrEmpTransferInfo) throws URISyntaxException {
        log.debug("REST request to save HrEmpTransferInfo : {}", hrEmpTransferInfo);
        if (hrEmpTransferInfo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("hrEmpTransferInfo", "idexists", "A new hrEmpTransferInfo cannot already have an ID")).body(null);
        }
        if(hrEmpTransferInfo.getLogStatus() < HRMManagementConstant.APPROVAL_STATUS_ADMIN_ENTRY)
        {
            hrEmpTransferInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_LOCKED);
        }

        HrEmpTransferInfo result = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);

        String ACTION_COMMENTS = "";
        String FROM_ROLE ="";
        String TO_ROLE = "";
        long ACTION_LOG_STATUS = 0;
        Map map = new HashMap();

        try {
//            // Save employee information into LOG .
//            HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
//            HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(hrEmpTransferInfo.getEmployeeInfo().getId());
//            logInfo = conversionService.getLogFromSource(dbModelInfo);
//
            if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType())
                &&  hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_INST_TO_INST_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }
            else if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType())
                &&  hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_ORG_TO_ORG_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }
            else if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)
                && hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_ORG_TO_INST_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }
            else if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)
                && hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_INST_TO_ORG_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }

            if ( hrEmpTransferInfo.getTransferType().getTypeCode()!=null && hrEmpTransferInfo.getToOrgCategory() !=null
                && (hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_DEPUTATION) || hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_LIEN))
                && hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG) && !hrEmpTransferInfo.getToOrgCategory().getTypeCode().equals("DTE"))
            {
                // final approval Task
                hrTransferEmployeeFinalApprove(hrEmpTransferInfo);
                hrEmpTransferInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
                hrEmpTransferInfo.setActiveStatus(true);
                hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
            }
//
//            logInfo.setLogStatus(ACTION_LOG_STATUS);
//            logInfo.setActionComments(ACTION_COMMENTS);
//            logInfo.setCreateDate(dateResrc.getDateAsLocalDate());
//            //logInfo.setActionDate(logInfo.getCreateDate());
//
//            logInfo = hrEmployeeInfoLogRepository.save(logInfo);
//
//            Authority govtNonGovtInstAuthority  = authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
//            Authority hrmEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME);
//            Authority dteEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
//
//            if(govtNonGovtInstAuthority==null)
//            {
//                log.debug("REST Adding GOVT_NOGOVT ROLE Authority, as not exist : {} ", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
//                govtNonGovtInstAuthority=new Authority();
//                govtNonGovtInstAuthority.setName(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
//                authorityRepository.save(govtNonGovtInstAuthority);
//            }
//            if(hrmEmployeeAuthority==null)
//            {
//                log.debug("REST Adding HRM for all GOVT Authority, as not exist : {} ", HRMManagementConstant.ROLE_HRM_USER_NAME);
//                hrmEmployeeAuthority=new Authority();
//                hrmEmployeeAuthority.setName(HRMManagementConstant.ROLE_HRM_USER_NAME);
//                authorityRepository.save(hrmEmployeeAuthority);
//            }
//            if(dteEmployeeAuthority==null)
//            {
//                log.debug("REST Adding DTE Employee Authority, as not exist : {} ", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
//                dteEmployeeAuthority=new Authority();
//                dteEmployeeAuthority.setName(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
//                authorityRepository.save(dteEmployeeAuthority);
//            }
//
//            //Retrieve all user authorities for the user.
//            User employeeUser = userRepository.findOne(dbModelInfo.getUser().getId());
//            Set<Authority> employeeAuthorities = employeeUser.getAuthorities();
//
//            HrEmploymentInfo hrEmploymentInfo = null;
//            PrlSalaryStructureInfo salaryStructureInfo = null;
//            Long employmentId = 0l;
//            Long salaryStructureId = 0l;
//
//            try{
//                hrEmploymentInfo = hrEmploymentInfoRepository.findByEmployeeId(hrEmpTransferInfo.getEmployeeInfo().getId());
//                salaryStructureInfo = prlSalaryStructureInfoRepository.finAlldByEmployeeId(hrEmpTransferInfo.getEmployeeInfo().getId());
//                employmentId    = hrEmploymentInfo.getId();
//                salaryStructureId   = salaryStructureInfo.getId();
//            }catch(Exception ex){
//                employmentId = 0l;
//                salaryStructureId = 0l;
//                log.error("Employee and Salary Structure Update Exception", ex);
//            }
//
//            if(hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
//            {
//                // If transfer type not equals WORK_PLACE
//                if ( hrEmpTransferInfo.getTransferType().getTypeCode()!=null
//                    && ! hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_WORK_PLACE))
//                {
//                    // update fields value for transfer within employee entity
//                    hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(hrEmpTransferInfo.getEmployeeInfo().getId(), hrEmpTransferInfo.getToEmployeeType(), hrEmpTransferInfo.getToOrganizationType(),
//                        hrEmpTransferInfo.getToInstCategory().getId(),hrEmpTransferInfo.getToInstLevel().getId() ,hrEmpTransferInfo.getToInstitute().getId(),
//                        hrEmpTransferInfo.getToDepartmentInfo().getId(), hrEmpTransferInfo.getToDesignationInfo().getId(), logInfo.getId());
//                }
//                try{
//                    hrEmploymentInfoRepository.updateCurrentEmploymentInformationInsitute(employmentId, hrEmpTransferInfo.getToOrganizationType(),
//                        hrEmpTransferInfo.getToInstitute().getId(), hrEmpTransferInfo.getToDepartmentInfo().getId(),
//                        hrEmpTransferInfo.getToDesignationInfo().getId(), salaryStructureId);
//                }catch(Exception ex){
//                    log.error("Institute Employment Update Exception", ex);
//                }
//
//                // Check whether From and TO OrganizationType is same or not.
//                // If same, no need to update role information.
//                if(! hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType()))
//                {
//                    if(!employeeAuthorities.contains(govtNonGovtInstAuthority) && !employeeAuthorities.contains(hrmEmployeeAuthority))
//                    {
//                        log.debug("REST mapping user authority - adding HRM and INSTEMP : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
//                        employeeAuthorities.add(govtNonGovtInstAuthority);
//                        employeeAuthorities.add(hrmEmployeeAuthority);
//                        employeeAuthorities.remove(dteEmployeeAuthority);
//                        employeeUser.setAuthorities(employeeAuthorities);
//                        userRepository.save(employeeUser);
//                    }
//                    else if(!employeeAuthorities.contains(govtNonGovtInstAuthority) && employeeAuthorities.contains(hrmEmployeeAuthority))
//                    {
//                        log.debug("REST mapping user authority - Adding INSTEMP role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
//                        employeeAuthorities.add(govtNonGovtInstAuthority);
//                        employeeAuthorities.remove(dteEmployeeAuthority);
//                        employeeUser.setAuthorities(employeeAuthorities);
//                        userRepository.save(employeeUser);
//                    }
//                    else if(employeeAuthorities.contains(govtNonGovtInstAuthority) && !employeeAuthorities.contains(hrmEmployeeAuthority))
//                    {
//                        log.debug("REST mapping user authority - Adding HRM role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
//                        employeeAuthorities.add(hrmEmployeeAuthority);
//                        employeeAuthorities.remove(dteEmployeeAuthority);
//                        employeeUser.setAuthorities(employeeAuthorities);
//                        userRepository.save(employeeUser);
//                    }
//                }
//                FROM_ROLE = HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME;
//                TO_ROLE = HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME;
//            }
//            else
//            {
//                // If transfer type not equals WORK_PLACE
//                if ( hrEmpTransferInfo.getTransferType().getTypeCode()!=null
//                    && ! hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_WORK_PLACE))
//                {
//                    // update fields value for transfer within employee entity
//                    hrEmployeeInfoRepository.updateEmployeeForDteTransfer(hrEmpTransferInfo.getEmployeeInfo().getId(), hrEmpTransferInfo.getToEmployeeType(), hrEmpTransferInfo.getToOrganizationType(),
//                        hrEmpTransferInfo.getToOrgCategory().getId(), hrEmpTransferInfo.getToOrganization().getId(),
//                        hrEmpTransferInfo.getToDepartmentInfo().getId(), hrEmpTransferInfo.getToDesignationInfo().getId(), logInfo.getId());
//                }
//
//                // Update employment information
//                try{
//                    hrEmploymentInfoRepository.updateCurrentEmploymentInformationOfOrganization(employmentId, hrEmpTransferInfo.getToOrganizationType(),
//                        hrEmpTransferInfo.getToOrganization().getId(), hrEmpTransferInfo.getToDepartmentInfo().getId(),
//                        hrEmpTransferInfo.getToDesignationInfo().getId(), salaryStructureId);
//                }catch(Exception ex){
//                    log.error("Organization Employment Update Exception", ex);
//                }
//
//                // Check whether From and TO OrganizationType is same or not.
//                // If same, no need to update role information.
//                if(! hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType()))
//                {
//                    if(!employeeAuthorities.contains(dteEmployeeAuthority) )
//                    {
//                        log.debug("REST mapping user authority - new dte role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
//                        employeeAuthorities.add(dteEmployeeAuthority);
//                        employeeAuthorities.remove(hrmEmployeeAuthority);
//                        employeeAuthorities.remove(govtNonGovtInstAuthority);
//                        employeeUser.setAuthorities(employeeAuthorities);
//                        userRepository.save(employeeUser);
//                    }
//                    else
//                    {
//                        log.debug("REST mapping user authority - dte role already exit : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
//                        employeeAuthorities.remove(hrmEmployeeAuthority);
//                        employeeAuthorities.remove(govtNonGovtInstAuthority);
//                        employeeUser.setAuthorities(employeeAuthorities);
//                        userRepository.save(employeeUser);
//                    }
//                }
//                FROM_ROLE = HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME;
//                TO_ROLE = HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME;
//            }

            map.put("FromDesigType", hrEmpTransferInfo.getFromEmployeeType().name());
            map.put("ToDesigType", hrEmpTransferInfo.getToEmployeeType().name());
            map.put("transferTo",ACTION_COMMENTS);
            map.put("fromRole", FROM_ROLE);
            map.put("targetRole", TO_ROLE);
            map.put("success", "true");
            map.put("message", "");

            if(! hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType()))
            {
                String msilContent="Dear " +hrEmpTransferInfo.getEmployeeInfo().getFullName()+","+
                    "\n" +
                    "You are transferred to "+hrEmpTransferInfo.getFromOrganizationType()+". \n" +
                    "\n" +
                    "this is a system generated mail." +
                    "Please contract with DIRECTORATE OF TECHNICAL EDUCATION for any query.";
                mailService.sendEmail(hrEmpTransferInfo.getEmployeeInfo().getEmailAddress(),"Employee Transfer to "+hrEmpTransferInfo.getFromOrganizationType()
                    ,msilContent,false,false);
            }
        }
        catch (Exception ex)
        {
            log.error("EmployeeLog saving error:" + ex);
            map.put("fromRole", FROM_ROLE);
            map.put("targetRole", TO_ROLE);
            map.put("success", "false");
            map.put("transferTo",ACTION_COMMENTS);
            map.put("message", ex.getLocalizedMessage());
        }
        return ResponseEntity.created(new URI("/api/hrEmpTransferInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("hrEmpTransferInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hrEmpTransferInfos -> Updates an existing hrEmpTransferInfo.
     */
    @RequestMapping(value = "/hrEmpTransferInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpTransferInfo> updateHrEmpTransferInfo(@Valid @RequestBody HrEmpTransferInfo hrEmpTransferInfo) throws URISyntaxException {
        log.debug("REST request to update HrEmpTransferInfo : {}", hrEmpTransferInfo);
        if (hrEmpTransferInfo.getId() == null) {
            return createHrEmpTransferInfo(hrEmpTransferInfo);
        }

        /*
        //Saving Go Order Document to Dir.
        MiscFileInfo goFile = new MiscFileInfo();
        goFile.fileData(hrEmpTransferInfo.getReleaseLetterDoc())
            .fileName(hrEmpTransferInfo.getReleaseLetterDocName())
            .contentType(hrEmpTransferInfo.getReleaseLetterContentType())
            .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);

        goFile = fileUtils.updateFileAsByte(goFile);
        hrEmpTransferInfo.setReleaseLetterDoc(new byte[1]);
        hrEmpTransferInfo.setReleaseLetterDocName(goFile.fileName());
        */

        // Add LOG info for Approval Purpose.
        HrEmpTransferInfoLog logInfo = new HrEmpTransferInfoLog();
        HrEmpTransferInfo dbModelInfo = hrEmpTransferInfoRepository.findOne(hrEmpTransferInfo.getId());
        logInfo = conversionService.getTransferLogFromSource(dbModelInfo);
        logInfo.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
        logInfo = hrEmpTransferInfoLogRepository.save(logInfo);
        hrEmpTransferInfo.setLogId(logInfo.getId());
        hrEmpTransferInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_LOCKED);


        HrEmpTransferInfo result = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
        hrEmpTransferInfoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("hrEmpTransferInfo", hrEmpTransferInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hrEmpTransferInfos -> get all the hrEmpTransferInfos.
     */
    @RequestMapping(value = "/hrEmpTransferInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmpTransferInfo>> getAllHrEmpTransferInfos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrEmpTransferInfos");
        Page<HrEmpTransferInfo> page = hrEmpTransferInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrEmpTransferInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrEmpTransferInfos/:id -> get the "id" hrEmpTransferInfo.
     */
    @RequestMapping(value = "/hrEmpTransferInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpTransferInfo> getHrEmpTransferInfo(@PathVariable Long id) {
        log.debug("REST request to get HrEmpTransferInfo : {}", id);
        HrEmpTransferInfo hrEmpTransferInfo = hrEmpTransferInfoRepository.findOne(id);

        MiscFileInfo releaseDoc = new MiscFileInfo();
        releaseDoc.fileName(hrEmpTransferInfo.getReleaseLetterDocName())
            .contentType(hrEmpTransferInfo.getReleaseLetterContentType())
            .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);
        releaseDoc = fileUtils.readFileAsByte(releaseDoc);
        hrEmpTransferInfo.setReleaseLetterDoc(releaseDoc.fileData());

        return Optional.ofNullable(hrEmpTransferInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /hrEmpTransferInfos/:id -> delete the "id" hrEmpTransferInfo.
     */
    @RequestMapping(value = "/hrEmpTransferInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteHrEmpTransferInfo(@PathVariable Long id) {
        log.debug("REST request to delete HrEmpTransferInfo : {}", id);
        hrEmpTransferInfoRepository.delete(id);
        hrEmpTransferInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("hrEmpTransferInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/hrEmpTransferInfos/:query -> search for the hrEmpTransferInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/hrEmpTransferInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpTransferInfo> searchHrEmpTransferInfos(@PathVariable String query) {
        log.debug("REST request to search HrEmpTransferInfos for query {}", query);
        return StreamSupport
            .stream(hrEmpTransferInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /hrEmpTransferInfos -> get all the hrEmpTransferInfos/my.
     */
    @RequestMapping(value = "/hrEmpTransferInfos/my",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpTransferInfo> getAllHrTransferInfosByCurrentEmployee()
        throws URISyntaxException
    {
        log.debug("REST request to get a page of hrTransfer by LoggedIn Employee");
        List<HrEmpTransferInfo> transferList = hrEmpTransferInfoRepository.findAllByEmployeeIsCurrentUser();
        for(HrEmpTransferInfo transferInfo : transferList)
        {
            MiscFileInfo releaseDoc = new MiscFileInfo();
            releaseDoc.fileName(transferInfo.getReleaseLetterDocName())
                .contentType(transferInfo.getReleaseLetterContentType())
                .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);
            releaseDoc = fileUtils.readFileAsByte(releaseDoc);
            transferInfo.setReleaseLetterDoc(releaseDoc.fileData());

        }
        return transferList;
    }

    /**
     * GET  /hrEmpTransferInfosApprover/ -> process the approval request.
     */
    @RequestMapping(value = "/hrEmpTransferInfosApprover",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> updateModelApproval(@Valid @RequestBody HrmApprovalDto approvalDto) {
        log.debug("REST request to Approve hrEmpTransferInfos POST: Type: {} ID: {}, comment : {}",approvalDto.getActionType(), approvalDto.getEntityId(), approvalDto.getLogComments());

        HrEmpTransferInfo modelInfo = hrEmpTransferInfoRepository.findOne(approvalDto.getEntityId());

        if(approvalDto.getActionType().equalsIgnoreCase(HRMManagementConstant.APPROVAL_LOG_STATUS_ACCEPT))
        {
            log.debug("REST request to APROVING ID: {}", approvalDto.getEntityId());
            modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            modelInfo.setActiveStatus(true);
            modelInfo = hrEmpTransferInfoRepository.save(modelInfo);
            if(modelInfo.getLogId() != 0)
            {
                HrEmpTransferInfoLog modelLog = hrEmpTransferInfoLogRepository.findOne(modelInfo.getLogId());
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmpTransferInfoLogRepository.save(modelLog);
            }
        }
        else
        {
            log.debug("REST request to REJECTING ID: {}", approvalDto.getEntityId());
            if(modelInfo.getLogId() != 0)
            {
                HrEmpTransferInfoLog modelLog = hrEmpTransferInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmpTransferInfoLogRepository.save(modelLog);
                modelInfo = conversionService.getTransferModelFromLog(modelLog, modelInfo);
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo.setActiveStatus(false);
                modelInfo = hrEmpTransferInfoRepository.save(modelInfo);
            }else{
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setActiveStatus(false);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo = hrEmpTransferInfoRepository.save(modelInfo);
            }
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("hrEmpTransferInfo", approvalDto.getEntityId().toString())).build();
    }

    /**
     * GET  /hrEmpTransferInfosApprover/:logStatus -> get address list by log status.
     */
    @RequestMapping(value = "/hrEmpTransferInfosApprover/{logStatus}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getModelListByLogStatus(@PathVariable Long logStatus) {
        log.debug("REST request to Approve hrEmpTransferInfos List : logStatus: {} ",logStatus);
        List<HrEmpTransferInfo> modelList = hrEmpTransferInfoRepository.findAllByLogStatus(logStatus);

        HrEmpTransferInfoLog modelLogInfo = null;
        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        HrmApprovalDto dtoInfo = null;
        for(HrEmpTransferInfo modelInfo : modelList)
        {
            dtoInfo = new HrmApprovalDto();
            dtoInfo.setEntityObject(modelInfo);
            if(modelInfo.getLogId() != 0)
            {
                modelLogInfo = hrEmpTransferInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                dtoInfo.setEntityLogObject(modelLogInfo);
            }
            modelDtoList.add(dtoInfo);
        }
        return modelDtoList;
    }

    /**
     * GET  /hrEmpTransferInfosApprover/log/:entityId -> load model and LogModel by entity id
     */
    @RequestMapping(value = "/hrEmpTransferInfosApprover/log/{entityId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public HrmApprovalDto getModelAndLogObjectByModelId(@PathVariable String entityId) {
        log.debug("REST request to Log hrEmpTransferInfos Model and Log of id: {} ",entityId);

        Long id = Long.parseLong(entityId);
        HrmApprovalDto approvalDto = new HrmApprovalDto();
        HrEmpTransferInfo modelInfo = hrEmpTransferInfoRepository.findOne(id);
        approvalDto.setEntityObject(modelInfo);
        approvalDto.setEntityId(id);
        approvalDto.setEntityName("Transfer");
        if(modelInfo.getLogId() != 0)
        {
            HrEmpTransferInfoLog modelLog = hrEmpTransferInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            approvalDto.setEntityLogObject(modelLog);
        }
        return approvalDto;
    }

    /**
     * POST  /hrEmpTransferInfosJobReleaseUpdate -> Transferred EmployeeInfo Release Data Update.
     */
    @RequestMapping(value = "/hrEmpTransferInfosJobReleaseUpdate",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> updateReleaseLeaterJobDOc(@Valid @RequestBody HrEmpTransferInfo hrEmpTransferInfo) throws URISyntaxException {
        log.debug("REST hrEmpTransferInfosJobReleaseUpdate employee: {}", hrEmpTransferInfo.getJobReleaseDate());
        Map map = new HashMap();

        try {
            // Save employee information into LOG .
            HrEmpTransferInfoLog logInfo = new HrEmpTransferInfoLog();
            HrEmpTransferInfo dbModelInfo = hrEmpTransferInfoRepository.findOne(hrEmpTransferInfo.getId());
            logInfo = conversionService.getTransferLogFromSource(dbModelInfo);
            logInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            logInfo.setActionComments("Release Letter Update");
            logInfo.setCreateDate(dateResrc.getDateAsLocalDate());

            logInfo = hrEmpTransferInfoLogRepository.save(logInfo);

            //Saving Employee Release Document to Dir.
            MiscFileInfo releaseLtrDoc = new MiscFileInfo();
            try{
                releaseLtrDoc.fileData(hrEmpTransferInfo.getReleaseLetterDoc())
                    .fileName(hrEmpTransferInfo.getReleaseLetterDocName())
                    .contentType(hrEmpTransferInfo.getReleaseLetterContentType())
                    .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);

                if (dbModelInfo.getReleaseLetterDocName() == null ||
                    dbModelInfo.getReleaseLetterDocName().length() < HRMManagementConstant.MINIMUM_IMG_NAME_LENGTH)
                {
                    releaseLtrDoc = fileUtils.saveFileAsByte(releaseLtrDoc);
                } else
                {
                    releaseLtrDoc.fileName(dbModelInfo.getReleaseLetterDocName())
                        .contentType(hrEmpTransferInfo.getReleaseLetterContentType());
                    releaseLtrDoc = fileUtils.updateFileAsByte(releaseLtrDoc);
                }
            }catch(Exception ex){
                log.error("Exception: UpdateHrInfo releaseDoc "+ex.getMessage());
            }

            dbModelInfo.setLogStatus(HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING);
            dbModelInfo.setLogComments("Release document approval pending");
            dbModelInfo.setDateOfJoining(hrEmpTransferInfo.getDateOfJoining());
            dbModelInfo.setJobReleaseDate(hrEmpTransferInfo.getJobReleaseDate());
            dbModelInfo.setReleaseLetterContentType(hrEmpTransferInfo.getReleaseLetterContentType());
            dbModelInfo.setReleaseLetterDoc(new byte[1]);
            dbModelInfo.setReleaseLetterDocName(releaseLtrDoc.fileName());
            dbModelInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
            dbModelInfo.setUpdateBy(SecurityUtils.getCurrentUserId());

            HrEmpTransferInfo result = hrEmpTransferInfoRepository.save(dbModelInfo);

            try {
                // Save employee information into LOG .
                HrEmployeeInfoLog hrLogInfo = new HrEmployeeInfoLog();
                HrEmployeeInfo hrDbModelInfo = hrEmployeeInfoRepository.findOne(dbModelInfo.getEmployeeInfo().getId());
                hrLogInfo = conversionService.getLogFromSource(hrDbModelInfo);
                hrLogInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
                hrLogInfo.setActionComments("Release Letter Update");
                hrLogInfo.setCreateDate(dateResrc.getDateAsLocalDate());

                hrLogInfo = hrEmployeeInfoLogRepository.save(hrLogInfo);

                //Update Employee Release Document

                //hrDbModelInfo.setLogStatus(HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING);
                hrDbModelInfo.setLogComments("Release document approval pending");
                hrDbModelInfo.setDateOfJoining(hrEmpTransferInfo.getDateOfJoining());
                hrDbModelInfo.setJobReleaseDate(hrEmpTransferInfo.getJobReleaseDate());
                hrDbModelInfo.setReleaseLetterContentType(hrEmpTransferInfo.getReleaseLetterContentType());
                hrDbModelInfo.setReleaseLetterDoc(new byte[1]);
                hrDbModelInfo.setReleaseLetterDocName(releaseLtrDoc.fileName());
                hrDbModelInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
                hrDbModelInfo.setUpdateBy(SecurityUtils.getCurrentUserId());

                HrEmployeeInfo hrResult = hrEmployeeInfoRepository.save(hrDbModelInfo);

                map.put("success", "true");
                map.put("message", "");
            }
            catch (Exception ex)
            {
                log.error("EmployeeLog saving error:" + ex);
                map.put("success", "false");
                map.put("message", ex.getLocalizedMessage());
            }

            map.put("success", "true");
            map.put("message", "");
        }
        catch (Exception ex)
        {
            log.error("EmployeeLog saving error:" + ex);
            map.put("success", "false");
            map.put("message", ex.getLocalizedMessage());
        }
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/hrEmpTransferInfoDepartmentHeadPendingApprovalList",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> getDepartmentHeadPendingApprovalList()
    {
        log.debug("hrEmpTransferInfoDepartmentHeadPendingApprovalList");
        List<HrDepartmentHeadInfo> deptHeadInfoList = hrDepartmentHeadInfoRepository.findAllByEmployeeIsCurrentUser();

        Map map =new HashMap();
        if(deptHeadInfoList!=null && deptHeadInfoList.size()>0)
        {
            map.put("isHead", true);
            map.put("departmentHeadCount", deptHeadInfoList.size());
            List<HrEmpTransferInfo> pendingTransferList = new ArrayList<HrEmpTransferInfo>();
            List<HrDepartmentSetup> departmentList = new ArrayList<HrDepartmentSetup>();
            for(HrDepartmentHeadInfo headInfo : deptHeadInfoList)
            {
                departmentList.add(headInfo.getDepartmentInfo());

                List<HrEmpTransferInfo> tempList = hrEmpTransferInfoRepository.findAllEmployeeByDepartmentAndPendingStatus(
                    headInfo.getDepartmentInfo().getId(),
                    HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING);
                log.debug(" department headinfo: {}, listsize: {}", headInfo.getId(), tempList.size());
                pendingTransferList.addAll(tempList);
            }

            for(HrEmpTransferInfo transferInfo : pendingTransferList)
            {
                MiscFileInfo releaseDoc = new MiscFileInfo();
                releaseDoc.fileName(transferInfo.getReleaseLetterDocName())
                    .contentType(transferInfo.getReleaseLetterContentType())
                    .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);
                releaseDoc = fileUtils.readFileAsByte(releaseDoc);
                transferInfo.setReleaseLetterDoc(releaseDoc.fileData());
            }
            map.put("departmentList", departmentList);
            map.put("pendingList", pendingTransferList);
        }
        else
        {
            map.put("isHead", false);
        }

        return new ResponseEntity<Map>(map,HttpStatus.OK);
    }

    @RequestMapping(value = "/hrEmpTransferInfoInstituteHeadPendingApprovalList",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpTransferInfo> getInstituteHeadPendingApprovalList()
    {
        log.debug("hrEmpTransferInfoInstituteHeadPendingApprovalList");
        Institute institute = instituteRepository.findOneByUserIsCurrentUser();

        log.debug("institute id {}", institute.getId());
        List<HrEmpTransferInfo> pendingTransferList = hrEmpTransferInfoRepository.findAllEmployeeByInstituteAndPendingStatus(
                institute.getId(),
                HRMManagementConstant.RELEASE_DOC_INST_HEAD_APPROVAL_PENDING);

        log.debug("institute transfer list: {}", pendingTransferList.size());
        for(HrEmpTransferInfo transferInfo : pendingTransferList)
        {
            MiscFileInfo releaseDoc = new MiscFileInfo();
            releaseDoc.fileName(transferInfo.getReleaseLetterDocName())
                .contentType(transferInfo.getReleaseLetterContentType())
                .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);
            releaseDoc = fileUtils.readFileAsByte(releaseDoc);
            transferInfo.setReleaseLetterDoc(releaseDoc.fileData());
        }

        return pendingTransferList;
    }

    /**
     * GET  /hrEmpTransferInfoReleaseApprove/ -> To organization Corresponding Head approve the release document request.
     */
    @RequestMapping(value = "/hrEmpTransferInfoReleaseApprove",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<Void> transferInfoReleaseApprove(@Valid @RequestBody HrEmpTransferInfo hrEmpTransferInfo)
    {
        log.debug("REST hrEmpTransferInfoReleaseApprove toType: id: {}",hrEmpTransferInfo.getToOrganizationType());

        hrEmpTransferInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
        hrEmpTransferInfo.setUpdateBy(SecurityUtils.getCurrentUserId());

        // For Transfer to Organization
        if(hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
        {
            if(hrEmpTransferInfo.getLogComments().equalsIgnoreCase(HRMManagementConstant.EMP_TRANSFER_APPROVAL_APPROVED))
            {
                log.debug("REST TransferApproval -> ORG-> DEPT_HEAD APPROVED LogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_FREE);
                hrTransferEmployeeFinalApprove(hrEmpTransferInfo);
                hrEmpTransferInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
                hrEmpTransferInfo.setActiveStatus(true);
                hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
            } else {
                log.debug("REST TransferApproval -> ORG -> DEPT_HEAD REJECTED LogStatus: {} ", HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED);
                hrEmpTransferInfo.setLogStatus(HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED);
                hrEmpTransferInfo.setActiveStatus(false);
                hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
            }
            // For Transfer to Institute
        }else if(hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
            if(hrEmpTransferInfo.getLogComments().equalsIgnoreCase(HRMManagementConstant.EMP_TRANSFER_APPROVAL_APPROVED))
            {
                if(hrEmpTransferInfo.getLogStatus()==HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING)
                {
                    log.debug("REST TransferApproval -> INST -> DEPT_HEAD -> APPROVED LogStatus: {} ", HRMManagementConstant.RELEASE_DOC_INST_HEAD_APPROVAL_PENDING);
                    hrEmpTransferInfo.setLogStatus(HRMManagementConstant.RELEASE_DOC_INST_HEAD_APPROVAL_PENDING);
                    hrEmpTransferInfo.setActiveStatus(false);
                    hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
                }else if(hrEmpTransferInfo.getLogStatus()== HRMManagementConstant.RELEASE_DOC_INST_HEAD_APPROVAL_PENDING)
                {
                    log.debug("REST TransferApproval -> INST -> INST_HEAD -> APPROVED LogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_FREE);
                    hrTransferEmployeeFinalApprove(hrEmpTransferInfo);
                    hrEmpTransferInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
                    hrEmpTransferInfo.setActiveStatus(true);
                    hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
                }
            }
            else
            {
                if(hrEmpTransferInfo.getLogStatus()==HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING)
                {
                    log.debug("REST TransferApproval -> INST -> DEPT_HEAD -> REJECTED LogStatus: {} ", HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED);
                    hrEmpTransferInfo.setLogStatus(HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED);
                    hrEmpTransferInfo.setActiveStatus(false);
                    hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
                }
                else if(hrEmpTransferInfo.getLogStatus()==HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_PENDING)
                {
                    log.debug("REST TransferApproval -> INST -> INST_HEAD -> REJECTED LogStatus: {} ", HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED);
                    hrEmpTransferInfo.setLogStatus(HRMManagementConstant.RELEASE_DOC_DEPT_HEAD_APPROVAL_REJECTED);
                    hrEmpTransferInfo.setActiveStatus(false);
                    hrEmpTransferInfo = hrEmpTransferInfoRepository.save(hrEmpTransferInfo);
                }
            }
        }

        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("hrEmpTransferInfo", hrEmpTransferInfo.getId().toString())).build();
    }

    /*
        Update Employee Information for Transfer
     */
    public void updateEmployeeTransferInformation(HrEmpTransferInfo hrEmpTransferInfo)
    {
        try {
            // Save employee information into LOG .
            HrEmployeeInfoLog hrLogInfo = new HrEmployeeInfoLog();
            HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(hrEmpTransferInfo.getEmployeeInfo().getId());
            hrLogInfo = conversionService.getLogFromSource(hrEmployeeInfo);
            hrLogInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            hrLogInfo.setActionComments("TRANSFER_APPROVED");
            hrLogInfo.setCreateDate(dateResrc.getDateAsLocalDate());

            hrLogInfo = hrEmployeeInfoLogRepository.save(hrLogInfo);

            //Update Employee Transfer Related Information

            hrEmployeeInfo.setOrganizationType(hrEmpTransferInfo.getToOrganizationType());
            hrEmployeeInfo.setInstCategory(hrEmpTransferInfo.getToInstCategory());
            hrEmployeeInfo.setInstitute(hrEmpTransferInfo.getToInstitute());
            hrEmployeeInfo.setWorkArea(hrEmpTransferInfo.getToOrgCategory());
            hrEmployeeInfo.setWorkAreaDtl(hrEmpTransferInfo.getToOrganization());
            hrEmployeeInfo.setDepartmentInfo(hrEmpTransferInfo.getToDepartmentInfo());
            hrEmployeeInfo.setDesignationInfo(hrEmpTransferInfo.getToDesignationInfo());
            hrEmployeeInfo.setEmployeeType(hrEmpTransferInfo.getToEmployeeType());
            hrEmployeeInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
            hrEmployeeInfo.setUpdateBy(SecurityUtils.getCurrentUserId());
            HrEmployeeInfo hrResult = hrEmployeeInfoRepository.save(hrEmployeeInfo);

        }
        catch (Exception ex)
        {
            log.error("EmployeeLog saving error:" + ex);

        }
    }

    @Transactional
    public void  hrTransferEmployeeFinalApprove(HrEmpTransferInfo hrEmpTransferInfo){

        String ACTION_COMMENTS = "";
        String FROM_ROLE ="";
        String TO_ROLE = "";
        long ACTION_LOG_STATUS = 0;
        Map map = new HashMap();

        try {
            // Save employee information into LOG .
            HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
            HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(hrEmpTransferInfo.getEmployeeInfo().getId());
            logInfo = conversionService.getLogFromSource(dbModelInfo);

            if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType())
                &&  hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_INST_TO_INST_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }
            else if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType())
                &&  hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_ORG_TO_ORG_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }
            else if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)
                && hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_ORG_TO_INST_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }
            else if(hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)
                && hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
            {
                ACTION_LOG_STATUS = HRMManagementConstant.TRANSFER_INST_TO_ORG_LOG_STATUS;
                ACTION_COMMENTS = hrEmpTransferInfo.getFromOrganizationType().toUpperCase()+"_TO_"+hrEmpTransferInfo.getToOrganizationType().toUpperCase();
            }

            logInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            logInfo.setActionComments(ACTION_COMMENTS);
            logInfo.setCreateDate(dateResrc.getDateAsLocalDate());
            //logInfo.setActionDate(logInfo.getCreateDate());

            logInfo = hrEmployeeInfoLogRepository.save(logInfo);

            Authority govtNonGovtInstAuthority  = authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            Authority hrmEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME);
            Authority dteEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);

            if(govtNonGovtInstAuthority==null)
            {
                log.debug("REST Adding GOVT_NOGOVT ROLE Authority, as not exist : {} ", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
                govtNonGovtInstAuthority=new Authority();
                govtNonGovtInstAuthority.setName(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
                authorityRepository.save(govtNonGovtInstAuthority);
            }
            if(hrmEmployeeAuthority==null)
            {
                log.debug("REST Adding HRM for all GOVT Authority, as not exist : {} ", HRMManagementConstant.ROLE_HRM_USER_NAME);
                hrmEmployeeAuthority=new Authority();
                hrmEmployeeAuthority.setName(HRMManagementConstant.ROLE_HRM_USER_NAME);
                authorityRepository.save(hrmEmployeeAuthority);
            }
            if(dteEmployeeAuthority==null)
            {
                log.debug("REST Adding DTE Employee Authority, as not exist : {} ", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
                dteEmployeeAuthority=new Authority();
                dteEmployeeAuthority.setName(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
                authorityRepository.save(dteEmployeeAuthority);
            }

            //Retrieve all user authorities for the user.
            User employeeUser = userRepository.findOne(dbModelInfo.getUser().getId());
            Set<Authority> employeeAuthorities = employeeUser.getAuthorities();

            HrEmploymentInfo hrEmploymentInfo = null;
            PrlSalaryStructureInfo salaryStructureInfo = null;
            Long employmentId = 0l;
            Long salaryStructureId = 0l;

            try{
                hrEmploymentInfo = hrEmploymentInfoRepository.findByEmployeeId(hrEmpTransferInfo.getEmployeeInfo().getId());
                salaryStructureInfo = prlSalaryStructureInfoRepository.finAlldByEmployeeId(hrEmpTransferInfo.getEmployeeInfo().getId());
                employmentId    = hrEmploymentInfo.getId();
                salaryStructureId   = salaryStructureInfo.getId();
            }catch(Exception ex){
                employmentId = 0l;
                salaryStructureId = 0l;
                log.error("Employee and Salary Structure Update Exception", ex);
            }

            if(hrEmpTransferInfo.getToOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
            {
                // If transfer type not equals WORK_PLACE
                if ( hrEmpTransferInfo.getTransferType().getTypeCode()!=null
                    && ! hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_WORK_PLACE) && ! hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_DEPUTATION))
                {
                    // update fields value for transfer within employee entity

                    HrEmployeeInfo hrEmployeeInfo = conversionService.convertHrTransDataToHrEmployee(hrEmpTransferInfo);
                    InstEmployee instEmployee = conversionService.hrEmpToInstEmpConversion(hrEmployeeInfo);
                    InstEmployee result = instEmployeeRepository.save(instEmployee);
                    hrEmployeeInfo.setInstEmployee(result);
                    hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(hrEmpTransferInfo.getEmployeeInfo().getId(), hrEmpTransferInfo.getToEmployeeType(), hrEmpTransferInfo.getToOrganizationType(),
                        hrEmpTransferInfo.getToInstCategory().getId(),hrEmpTransferInfo.getToInstLevel().getId() ,hrEmpTransferInfo.getToInstitute().getId(),
                        hrEmpTransferInfo.getToDepartmentInfo().getId(), hrEmpTransferInfo.getToDesignationInfo().getId(), logInfo.getId());
                    hrEmployeeInfoRepository.save(hrEmployeeInfo);
                }
                try{
                    hrEmploymentInfoRepository.updateCurrentEmploymentInformationInsitute(employmentId, hrEmpTransferInfo.getToOrganizationType(),
                        hrEmpTransferInfo.getToInstitute().getId(), hrEmpTransferInfo.getToDepartmentInfo().getId(),
                        hrEmpTransferInfo.getToDesignationInfo().getId(), salaryStructureId);
                }catch(Exception ex){
                    log.error("Institute Employment Update Exception", ex);
                }
                // Check whether From and TO OrganizationType is same or not.
                // If same, no need to update role information.
                if(! hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType()))
                {
                    if(!employeeAuthorities.contains(govtNonGovtInstAuthority) && !employeeAuthorities.contains(hrmEmployeeAuthority))
                    {
                        log.debug("REST mapping user authority - adding HRM and INSTEMP : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                        employeeAuthorities.add(govtNonGovtInstAuthority);
                        employeeAuthorities.add(hrmEmployeeAuthority);
                        employeeAuthorities.remove(dteEmployeeAuthority);
                        employeeUser.setAuthorities(employeeAuthorities);
                        userRepository.save(employeeUser);
                    }
                    else if(!employeeAuthorities.contains(govtNonGovtInstAuthority) && employeeAuthorities.contains(hrmEmployeeAuthority))
                    {
                        log.debug("REST mapping user authority - Adding INSTEMP role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                        employeeAuthorities.add(govtNonGovtInstAuthority);
                        employeeAuthorities.remove(dteEmployeeAuthority);
                        employeeUser.setAuthorities(employeeAuthorities);
                        userRepository.save(employeeUser);
                    }
                    else if(employeeAuthorities.contains(govtNonGovtInstAuthority) && !employeeAuthorities.contains(hrmEmployeeAuthority))
                    {
                        log.debug("REST mapping user authority - Adding HRM role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                        employeeAuthorities.add(hrmEmployeeAuthority);
                        employeeAuthorities.remove(dteEmployeeAuthority);
                        employeeUser.setAuthorities(employeeAuthorities);
                        userRepository.save(employeeUser);
                    }
                }
                FROM_ROLE = HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME;
                TO_ROLE = HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME;
            }
            else
            {
                // If transfer type not equals WORK_PLACE
                if ( hrEmpTransferInfo.getTransferType().getTypeCode()!= null
                    && ! hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_WORK_PLACE) && ! hrEmpTransferInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_DEPUTATION))
                {
                    if(ACTION_LOG_STATUS == HRMManagementConstant.TRANSFER_INST_TO_ORG_LOG_STATUS){
                       InstEmployee instEmployee= hrEmpTransferInfo.getEmployeeInfo().getInstEmployee();
                        instEmployee.setStatus(-5);
                        instEmployee.setInstitute(null);
                        instEmployeeRepository.save(instEmployee);
                    }
                    // update fields value for transfer within employee entity
                    hrEmployeeInfoRepository.updateEmployeeForDteTransfer(hrEmpTransferInfo.getEmployeeInfo().getId(), hrEmpTransferInfo.getToEmployeeType(), hrEmpTransferInfo.getToOrganizationType(),
                        hrEmpTransferInfo.getToOrgCategory().getId(), hrEmpTransferInfo.getToOrganization().getId(),
                        hrEmpTransferInfo.getToDepartmentInfo().getId(), hrEmpTransferInfo.getToDesignationInfo().getId(), logInfo.getId());

                }
                // Update employment information
                try{
                    hrEmploymentInfoRepository.updateCurrentEmploymentInformationOfOrganization(employmentId, hrEmpTransferInfo.getToOrganizationType(),
                        hrEmpTransferInfo.getToOrganization().getId(), hrEmpTransferInfo.getToDepartmentInfo().getId(),
                        hrEmpTransferInfo.getToDesignationInfo().getId(), salaryStructureId);
                }catch(Exception ex){
                    log.error("Organization Employment Update Exception", ex);
                }

                // Check whether From and TO OrganizationType is same or not.
                // If same, no need to update role information.
                if(! hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType()))
                {
                    if(!employeeAuthorities.contains(dteEmployeeAuthority))
                    {
                        log.debug("REST mapping user authority - new dte role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                        employeeAuthorities.add(dteEmployeeAuthority);
                        employeeAuthorities.remove(hrmEmployeeAuthority);
                        employeeAuthorities.remove(govtNonGovtInstAuthority);
                        employeeUser.setAuthorities(employeeAuthorities);
                        userRepository.save(employeeUser);
                    }
                    else
                    {
                        log.debug("REST mapping user authority - dte role already exit : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                        employeeAuthorities.remove(hrmEmployeeAuthority);
                        employeeAuthorities.remove(govtNonGovtInstAuthority);
                        employeeUser.setAuthorities(employeeAuthorities);
                        userRepository.save(employeeUser);
                    }
                }
                FROM_ROLE = HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME;
                TO_ROLE = HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME;
            }

        map.put("FromDesigType", hrEmpTransferInfo.getFromEmployeeType().name());
        map.put("ToDesigType", hrEmpTransferInfo.getToEmployeeType().name());
        map.put("transferTo",ACTION_COMMENTS);
        map.put("fromRole", FROM_ROLE);
        map.put("targetRole", TO_ROLE);
        map.put("success", "true");
        map.put("message", "");

        if(! hrEmpTransferInfo.getFromOrganizationType().equalsIgnoreCase(hrEmpTransferInfo.getToOrganizationType()))
        {
            String msilContent="Dear " +dbModelInfo.getFullName()+","+
                "\n" +
                "You are transferred to "+hrEmpTransferInfo.getFromOrganizationType()+". \n" +
                " are sucessfully completed\n" +
                "this is a system generated mail." +
                "Please contract with DIRECTORATE OF TECHNICAL EDUCATION for any query.";
            mailService.sendEmail(dbModelInfo.getEmailAddress(),"Employee Transfer to "+hrEmpTransferInfo.getFromOrganizationType()
                ,msilContent,false,false);
        }
    }catch (Exception ex){
        log.error("EmployeeLog saving error:" + ex);
        map.put("fromRole", FROM_ROLE);
        map.put("targetRole", TO_ROLE);
        map.put("success", "false");
        map.put("transferTo",ACTION_COMMENTS);
        map.put("message", ex.getLocalizedMessage());
    }

    }

    }
