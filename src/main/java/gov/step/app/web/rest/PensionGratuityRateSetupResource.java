package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.PensionGratuityRateSetup;
import gov.step.app.domain.PgmsPenGrRate;
import gov.step.app.repository.PensionGratuityRateSetupRepository;
import gov.step.app.repository.PgmsPenGrRateRepository;
//import gov.step.app.repository.search.PensionGratuityRateSetupSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.web.rest.jdbc.dao.PGMSJdbcDao;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing PensionGratuityRateSetup.
 */
@RestController
@RequestMapping("/api")
public class PensionGratuityRateSetupResource {

    private final Logger log = LoggerFactory.getLogger(PensionGratuityRateSetupResource.class);

    @Inject
    private PensionGratuityRateSetupRepository pgmsPenGrSetupRepository;

    @Inject
    private PgmsPenGrRateRepository pgmsPenGrRateRepository;

//    @Inject
//    private PgmsPenGrSetupSearchRepository pgmsPenGrSetupSearchRepository;

    @Inject
    private PensionGratuityRateSetupRepository pensionGratuityRateSetupRepository;

    @Inject
    PGMSJdbcDao pgmsJdbcDao;

    /**
     * POST  /pgmsPenGrSetups -> Create a new pensionGratuityRateSetup.
     */
    @RequestMapping(value = "/pensionGratuityRateSetups",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PensionGratuityRateSetup> createPgmsPenGrSetup(@Valid @RequestBody PensionGratuityRateSetup pensionGratuityRateSetup) throws URISyntaxException {
        log.debug("REST request to save PensionGratuityRateSetup : {}", pensionGratuityRateSetup);
        if (pensionGratuityRateSetup.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pensionGratuityRateSetup cannot already have an ID").body(null);
        }
        if(pensionGratuityRateSetup.getActiveStatus() == true){
            pgmsJdbcDao.deActiveOtherVersion(pensionGratuityRateSetup.getSetupVersion());
        }

//        pensionGratuityRateSetup.setActiveStatus(true);
        pensionGratuityRateSetup.setCreateBy(SecurityUtils.getCurrentUserId());
        pensionGratuityRateSetup.setCreateDate(LocalDate.now());

        PensionGratuityRateSetup result = pgmsPenGrSetupRepository.save(pensionGratuityRateSetup);



//        pgmsPenGrSetupSearchRepository.save(result);


        return ResponseEntity.created(new URI("/api/pgmsPenGrSetups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pensionGratuityRateSetup", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pgmsPenGrSetups -> Updates an existing pensionGratuityRateSetup.
     */
    @RequestMapping(value = "/pensionGratuityRateSetups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PensionGratuityRateSetup> updatePgmsPenGrSetup(@Valid @RequestBody PensionGratuityRateSetup pensionGratuityRateSetup) throws URISyntaxException {
        log.debug("REST request to update PensionGratuityRateSetup : {}", pensionGratuityRateSetup);
        if (pensionGratuityRateSetup.getId() == null) {
            return createPgmsPenGrSetup(pensionGratuityRateSetup);
        }
        if(pensionGratuityRateSetup.getActiveStatus() == true){
            pgmsJdbcDao.deActiveOtherVersion(pensionGratuityRateSetup.getSetupVersion());
        }
        pensionGratuityRateSetup.setUpdateBy(SecurityUtils.getCurrentUserId());
        pensionGratuityRateSetup.setUpdateDate(LocalDate.now());
        PensionGratuityRateSetup result = pgmsPenGrSetupRepository.save(pensionGratuityRateSetup);
//        pgmsPenGrSetupSearchRepository.save(pensionGratuityRateSetup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pensionGratuityRateSetup", pensionGratuityRateSetup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pgmsPenGrSetups -> get all the pgmsPenGrSetups.
     */
    @RequestMapping(value = "/pensionGratuityRateSetups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PensionGratuityRateSetup>> getAllPgmsPenGrSetups(Pageable pageable)
        throws URISyntaxException {
        Page<PensionGratuityRateSetup> page = pgmsPenGrSetupRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pgmsPenGrSetups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pgmsPenGrSetups/:id -> get the "id" pgmsPenGrSetup.
     */
    @RequestMapping(value = "/pensionGratuityRateSetups/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PensionGratuityRateSetup> getPgmsPenGrSetup(@PathVariable Long id) {
        log.debug("REST request to get PensionGratuityRateSetup : {}", id);
        return Optional.ofNullable(pgmsPenGrSetupRepository.findOne(id))
            .map(pgmsPenGrSetup -> new ResponseEntity<>(
                pgmsPenGrSetup,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pgmsPenGrSetups/:id -> delete the "id" pgmsPenGrSetup.
     */
    @RequestMapping(value = "/pensionGratuityRateSetups/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePgmsPenGrSetup(@PathVariable Long id) {
        log.debug("REST request to delete PensionGratuityRateSetup : {}", id);
        pgmsPenGrSetupRepository.delete(id);
//        pgmsPenGrSetupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pgmsPenGrSetup", id.toString())).build();
    }

    /**
     * SEARCH  /_search/pgmsPenGrSetups/:query -> search for the pgmsPenGrSetup corresponding
     * to the query.
     */
//    @RequestMapping(value = "/_search/pensionGratuityRateSetups/{query}",
//        method = RequestMethod.GET,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public List<PensionGratuityRateSetup> searchPgmsPenGrSetups(@PathVariable String query) {
//        return StreamSupport
//            .stream(pgmsPenGrSetupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
//            .collect(Collectors.toList());
//    }

    /**
     * Search Application No from Putup List
     * to the query.
     */
    @RequestMapping(value = "/pensionGratuityRateSetups/pgmsPenGrRateLists/{penGrSetId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsPenGrRate> getPgmsPenGrRateByFilter(@PathVariable long penGrSetId) {
        log.debug("REST request to get iisPutupListsWithApplicant : penGrSetId: {}", penGrSetId);
        List<PgmsPenGrRate> pgmsPrRatelist = pgmsPenGrRateRepository.findAllByPenGrSetIdOrderByWorkingYearAsc(penGrSetId);
        return pgmsPrRatelist;
    }

    @RequestMapping(value = "/pensionGratuityRateSetups/ByWorkingYear/{workYear}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PensionGratuityRateSetup>> getPensionAndGratuityByWorkingYear(@PathVariable Long workYear) {
        log.debug("REST request to get PensionAndGratuityByWorkingYear : {}", workYear);

        if(workYear > 25){
            workYear = 25L;
        }

        List<PensionGratuityRateSetup> pgmsPrRatelist = pgmsPenGrRateRepository.findAllByWorkingYear(workYear);

        return Optional.ofNullable(pgmsPrRatelist)
            .map(pensionGratuityRateSetup -> new ResponseEntity<>(
                pensionGratuityRateSetup,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/pensionGratuityRateSetups/bySetupType/{pSetupType}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Map getMaxVersionBySetupType(@PathVariable String pSetupType) {
        log.debug("REST request to get Pensigit onAndGratuityByWorkingYear : {}", pSetupType);
        Map mapVal = new HashMap<>();
        Long maxVersion = pensionGratuityRateSetupRepository.findMaxSetupVersionBySetupType(pSetupType);
        mapVal.put("version",maxVersion);

       return mapVal;
    }


    @RequestMapping(value = "/pensionGratuityRateSetups/getAllVersionInfo",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PensionGratuityRateSetup> getAllVersionInfo(){

        log.debug("REST request to get getAllPensionAndGratuityRate ");
        List<PensionGratuityRateSetup>  pensionGratuityRateSetups = new ArrayList<PensionGratuityRateSetup>();
        List<Long> totalVersion = pensionGratuityRateSetupRepository.findTotalVersion();
//        Map rateSetupMap = new HashMap<>();

        for(Long i : totalVersion){
            System.out.println("Version :" + i);
            pensionGratuityRateSetups.add(pensionGratuityRateSetupRepository.findBySetupVersion(i).get(0));
//            pensionGratuityRateSetups.add(pensionGratuityRateSetupRepository.findBySetupVersionAndSetupType(i,"Gratuity").get(0));
//            rateSetupMap.put(i.toString(),pensionGratuityRateSetups);
        }
        return  pensionGratuityRateSetups;
    }


    @RequestMapping(value = "/pensionGratuityRateSetups/getAllByVersion/{version}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PensionGratuityRateSetup>> getAllByVersion(@PathVariable Long version,Pageable pageable) throws URISyntaxException {

        log.debug("REST request to get getAllByVersion " + version);
//        List<PensionGratuityRateSetup>  pensionGratuityRateSetups = new ArrayList<PensionGratuityRateSetup>();

        Page<PensionGratuityRateSetup> page  = pensionGratuityRateSetupRepository.findBySetupVersionOrderByWorkingYear(version,pageable);

//        Page<PensionGratuityRateSetup> page = pgmsPenGrSetupRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pgmsPenGrSetups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

    }

    @RequestMapping(value = "/pensionGratuityRateSetups/getAllByVersionAndSetupType/{version}/{setupType}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PensionGratuityRateSetup>> getAllByVersionAndSetupType(@PathVariable Long version,@PathVariable String setupType,Pageable pageable) throws URISyntaxException {

        log.debug("REST request to get getAllByVersion " + version + " "+setupType);
//        List<PensionGratuityRateSetup>  pensionGratuityRateSetups = new ArrayList<PensionGratuityRateSetup>();

        Page<PensionGratuityRateSetup> page  = pensionGratuityRateSetupRepository.findBySetupVersionAndSetupTypeOrderByWorkingYear(version,setupType,pageable);

//        Page<PensionGratuityRateSetup> page = pgmsPenGrSetupRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pgmsPenGrSetups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);

    }

}
