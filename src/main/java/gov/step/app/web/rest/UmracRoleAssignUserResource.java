package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.repository.*;
import gov.step.app.repository.search.UmracRoleAssignUserSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.MailService;
import gov.step.app.service.UserService;
import gov.step.app.web.rest.dto.JhiUserAuthorityDto;
import gov.step.app.web.rest.jdbc.dao.RptJdbcDao;
import gov.step.app.web.rest.util.Emails;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import gov.step.app.web.rest.util.TransactionIdResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing UmracRoleAssignUser.
 */
@RestController
@RequestMapping("/api")
public class UmracRoleAssignUserResource {

    private final Logger log = LoggerFactory.getLogger(UmracRoleAssignUserResource.class);

    @Inject
    private UmracRoleAssignUserRepository umracRoleAssignUserRepository;

    @Inject
    private UmracRoleAssignUserSearchRepository umracRoleAssignUserSearchRepository;

    @Inject
    private UmracIdentitySetupRepository umracIdentitySetupRepository;

    @Inject
    private UmracRoleSetupRepository umracRoleSetupRepository;

    @Inject
    private UmracRightsSetupRepository umracRightsSetupRepository;

    @Inject
    private UmracSubmoduleSetupRepository umracSubmoduleSetupRepository;

    @Inject
    private UmracActionSetupRepository umracActionSetupRepository;

    /*@Inject
    private JhiUserAuthorityRepository jhiUserAuthorityRepository;*/

    @Inject
    private RptJdbcDao rptJdbcDao;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;


    @Inject
    private MailService mailService;

    @Inject
    private UserService userService;

    /**
     * POST  /umracRoleAssignUsers -> Create a new umracRoleAssignUser.
     */
    @RequestMapping(value = "/umracRoleAssignUsers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<UmracRoleAssignUser> createUmracRoleAssignUser(@RequestBody UmracRoleAssignUser umracRoleAssignUser) throws URISyntaxException {

        log.debug("REST request to save UmracRoleAssignUser : {}", umracRoleAssignUser);
        if (umracRoleAssignUser.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new umracRoleAssignUser cannot already have an ID").body(null);
        }

        UmracIdentitySetup umracIdentitySetup = new UmracIdentitySetup();
        if(umracRoleAssignUser.getUserId() != null){
            umracIdentitySetup = umracIdentitySetupRepository.findOne(umracRoleAssignUser.getUserId().getId());
        }

        TransactionIdResource transactionIdResource = new TransactionIdResource();
        String transactionId = transactionIdResource.getGeneratedid("UID");
        umracRoleAssignUser.setAssignTrackId(transactionId);

        System.out.println("\n umracRoleAssignUser : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + umracRoleAssignUser.toString());
        UmracRoleAssignUser result = umracRoleAssignUserRepository.save(umracRoleAssignUser);
        //User userRoleSave=null;
        System.out.println("\n Role ID : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + result.getRoleId());


        if (umracRoleAssignUser.getUserId() != null && umracRoleAssignUser.getRoleId() != null) {
            System.out.println("\n umracRoleAssignUser.getUserId() + umracRoleAssignUser.getRoleId() : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + umracRoleAssignUser.getRoleId().toString());
            try {
                List<UmracRightsSetup> umracRightsSetupList = umracRightsSetupRepository.findByRoleId(result.getRoleId().getId());
                Boolean emailStatus = false;
                Emails emails = new Emails();
                emails.setTo(umracIdentitySetup.getEmail().trim());
                //emails.setTo("zoynobtumpa@gmail.com");
                //emails.setCc("ranaarchive99@gmail.com");
                emails.setSubject("Your requested user created and role assigned");
                //emails.setEmailBody("Dear User Name : " + umracIdentitySetup.getUserName().toString() + ", Password : " + umracIdentitySetup.getuPw().toString() + " <br><br> Your requested user created and role assigned. <br><br> Regards, <br>STEP Admin");
                emails.setEmailBody("Dear User Name : " + umracIdentitySetup.getUserName().toString() + " <br><br> Your requested user created and role assigned. <br><br> Regards, <br>STEP Admin");
                emails.setLogged_user_id(SecurityUtils.getCurrentUserId());
                emails.setEmail_type("Text Email");

                //String emailBuilder = umracIdentitySetup.getUserName()+"@gmail.com";
                String emailBuilder = umracIdentitySetup.getEmail().trim();

                boolean roleProcess = false;
                Set<Authority> authoritiesAre = new HashSet<>();
                for (UmracRightsSetup rights : umracRightsSetupList) {
                    System.out.println("\n rights : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + rights.getSubModule_id());

                    List<UmracActionSetup> umracActionSetups = umracActionSetupRepository.findBySubModuleId(rights.getSubModule_id());
                    for (UmracActionSetup umracActionSetup : umracActionSetups) {
                        //String[] rightsAre = umracRoleAssignUser.getRoleId().getRoleContext().split(",");
                        //System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> rights in roleId"+rightsAre);
                        System.out.println("\n >>>>>> "+umracRoleAssignUser.getRoleId().getRoleContext()+" >>>>> "+umracActionSetup.getSubmoduleId().getId().toString().trim()+"-"+umracActionSetup.getActionId().toString().trim());
                        if(umracRoleAssignUser.getRoleId().getRoleContext().contains(umracActionSetup.getSubmoduleId().getId().toString().trim()+"-"+umracActionSetup.getActionId().toString().trim())){
                            System.out.println("\n umracActionSetup : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + umracActionSetup.toString());
                            System.out.println("\n umracRoleAssignUser.getUserId().getUserName() : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + umracRoleAssignUser.getUserId().getUserName().toString());

                            User user = userRepository.findUserByUserLogin(umracRoleAssignUser.getUserId().getUserName());
                            if (user != null) {
                                System.out.println("\n user.getId() >>>>>>>>>>>>>>>>>>>>>> " + user.getId().toString());
                                List<Map<String, Object>> authorities = rptJdbcDao.GetAuthoritiesByUserIdAndRole(user.getId(), umracActionSetup.getActionUrl());
                                System.out.println("\n authorities >>>>>>>>>>>>>>>>>>>>>> " + authorities.toString());
                                if (authorities != null && authorities.size() > 0) {
                                    System.out.println("\n This role already exist for this user >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + user.toString());
                                } else {
                                    Authority authority = authorityRepository.findOne(umracActionSetup.getActionUrl());
                                    authoritiesAre.add(authority);
                                    roleProcess = true;
                                    System.out.println("\n role name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + umracActionSetup.getActionUrl());
                                }
                            } else {
                                Authority authority = authorityRepository.findOne(umracActionSetup.getActionUrl());
                                authoritiesAre.add(authority);
                                roleProcess = true;
                                System.out.println("\n role name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + umracActionSetup.getActionUrl());
                            }
                        } else {
                            System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> this action is not part of this role");
                        }

                    }

                    //Previous Role assign codes
                    /*System.out.println("\n Sub module id : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+a.getSubModule_id().toString());
                    String submoduleId = a.getSubModule_id().toString();
                    UmracSubmoduleSetup submodules = umracSubmoduleSetupRepository.findOne(a.getSubModule_id());
                    String submodulesName = submodules.getSubModuleName();
                    System.out.println("\n ROLE NAME >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+submodulesName);
                    if(submodulesName.toUpperCase().contains("SMS")){
                        roleProcess = true;
                        roleName="ROLE_SMS";
                    }if(submodulesName.toUpperCase().contains("FEE")){
                        roleProcess = true;
                        roleName="ROLE_USER";
                    }if(submodulesName.toUpperCase().contains("REGISTRATION")){
                        roleProcess = true;
                        roleName="ROLE_USER";
                    }if(submodulesName.toUpperCase().contains("INSPECADMIN")){
                        roleProcess = true;
                        roleName="ROLE_ADMIN";
                    }if(submodulesName.toUpperCase().contains("INSPECTOR")){
                        roleProcess = true;
                        roleName="ROLE_INSPECTOR";
                    }if(submodulesName.toUpperCase().contains("PRINCIPAL")){
                        roleProcess = true;
                        roleName="ROLE_PRINCIPAL";
                    }if(submodulesName.toUpperCase().contains("VICEPRINCIPAL")){
                        roleProcess = true;
                        roleName="ROLE_VICEPRINCIPAL";
                    }*/

                }
                System.out.println("\n roleProcess : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + roleProcess);
                System.out.println("\n roleName : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + authoritiesAre.toString());
                User user = userService.createCustomUserInformationByRights(umracIdentitySetup.getUserName(), umracIdentitySetup.getuPw(), umracIdentitySetup.getEmpId(), null, emailBuilder, "en", authoritiesAre, true, result.getRoleId().getId());
                if (user != null) {
                    try{
                        for(Authority authority:authoritiesAre){
                            rptJdbcDao.updateAuthorityByRolesIdAndTrackId(user.getId(), authority.getName(), umracRoleAssignUser.getId(), umracRoleAssignUser.getAssignTrackId());
                        }
                    }catch (Exception exceptionJHI){
                        System.out.println("\n exceptionJHI : "+exceptionJHI);
                    }
                    mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                    log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                }
            } catch (Exception e) {
                log.debug("Exception in save role at JHI_USER entity");
                System.out.println("\n Exception in save role at JHI_USER entity >>>>>>>>>>>>>>>> " + e);
            }
        } else if(umracRoleAssignUser.getProcUserId() != null && umracRoleAssignUser.getRoleId() != null){
            System.out.println("\n umracRoleAssignUser.getProcUserId() + umracRoleAssignUser.getRoleId() : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getRoleId().toString());
            try {
                System.out.println("\n >>>>>>>>>>>>>>>>>>>>> traced 1 : "+result.toString());
                List<UmracRightsSetup> umracRightsSetupList = umracRightsSetupRepository.findByRoleId(result.getRoleId().getId());
                Boolean emailStatus = false;
                Emails emails =  new Emails();
                emails.setTo(umracRoleAssignUser.getProcUserId().getEmail());
                emails.setSubject("Your requested role assigned");
                emails.setEmailBody("Dear "+ umracRoleAssignUser.getProcUserId().getLogin().toString() + ",<br><br> Your requested role assigned. <br><br> Regards, <br>STEP Admin");
                emails.setLogged_user_id(SecurityUtils.getCurrentUserId());
                emails.setEmail_type("Text Email");

                String emailBuilder = umracRoleAssignUser.getProcUserId().getEmail().trim();
                System.out.println("\n >>>>>>>>>>>>>>>>>>>>> traced 2 :"+emailBuilder.toString());
                boolean roleProcess = false;
                Set<Authority> authoritiesAre = new HashSet<>();
                for(UmracRightsSetup rights :umracRightsSetupList){
                    System.out.println("\n rights : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+rights.getSubModule_id());

                    List<UmracActionSetup> umracActionSetups =  umracActionSetupRepository.findBySubModuleId(rights.getSubModule_id());
                    for(UmracActionSetup umracActionSetup : umracActionSetups){
                        System.out.println("\n umracRoleAssignUser.getRoleId().getRoleContext().contains : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getRoleId().getRoleContext().toString());
                        if(umracRoleAssignUser.getRoleId().getRoleContext().contains(umracActionSetup.getSubmoduleId().getId().toString().trim()+"-"+umracActionSetup.getActionId().toString().trim())){
                            System.out.println("\n umracActionSetup : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracActionSetup.toString());
                            System.out.println("\n umracRoleAssignUser.getUserId().getUserName() : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getProcUserId().getLogin().toString());

                            User user =  userRepository.findUserByUserLogin(umracRoleAssignUser.getProcUserId().getLogin());
                            if(user != null){
                                System.out.println("\n user.getId() >>>>>>>>>>>>>>>>>>>>>> "+user.getId().toString());
                                List<Map<String, Object>> authorities=rptJdbcDao.GetAuthoritiesByUserIdAndRole(user.getId(), umracActionSetup.getActionUrl());
                                System.out.println("\n authorities >>>>>>>>>>>>>>>>>>>>>> "+authorities.toString());
                                if(authorities !=null && authorities.size() >0){
                                    System.out.println("\n This role already exist for this user >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+user.toString());
                                }else{
                                    Authority authority = authorityRepository.findOne(umracActionSetup.getActionUrl());
                                    authoritiesAre.add(authority);
                                    roleProcess = true;
                                    System.out.println("\n role name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracActionSetup.getActionUrl());
                                }
                            }else{
                                Authority authority = authorityRepository.findOne(umracActionSetup.getActionUrl());
                                authoritiesAre.add(authority);
                                roleProcess = true;
                                System.out.println("\n role name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracActionSetup.getActionUrl());
                            }
                        } else {
                            System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> this action is not part of this role");
                        }
                    }
                }
                System.out.println("\n roleProcess existing : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+roleProcess);
                System.out.println("\n roleName existing : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+authoritiesAre.toString());
                System.out.println("\n getProcUserId : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getProcUserId().toString());
                /*User user = userService.createCustomUserInformationByRights(umracIdentitySetup.getUserName(), umracIdentitySetup.getuPw(), umracIdentitySetup.getEmpId(), null, emailBuilder, "en", authoritiesAre, true, result.getRoleId().getId());
                if(user != null){
                    mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                    log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                }*/
                try {
                    String responseCode = "200";
                    for (Authority authoritiesLoop : authoritiesAre) {
                        /*JhiUserAuthorityDto jhiUserAuthorityDto = new JhiUserAuthorityDto();
                        jhiUserAuthorityDto.setUser_id(umracRoleAssignUser.getProcUserId().getId());
                        jhiUserAuthorityDto.setAuthority_name(authoritiesLoop.getName());
                        jhiUserAuthorityDto.setRole_ids(result.getId());
                        jhiUserAuthorityDto.setAssign_track_id(result.getAssignTrackId());*/

                        //responseCode = rptJdbcDao.updateAuthorityByUserId(jhiUserAuthorityDto);
                        System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> jhiUserAuthorityDto : "+umracRoleAssignUser.getProcUserId().toString()+" --- "+authoritiesLoop.getName());
                        User responseUser = userService.assignRoleWithExistingRole(umracRoleAssignUser.getProcUserId(), authoritiesLoop.getName());
                        if (responseUser != null) {
                            responseCode = "200";
                        } else {
                            responseCode = "400";
                        }

                    }
                    if (responseCode == "200") {
                        mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                        log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                    }
                } catch (Exception ex){
                    System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> responseCode : "+ex);
                }

            }catch (Exception e){
                log.debug("Exception in save role at JHI_USER entity getProcUserId");
                System.out.println("\n Exception in save role at JHI_USER entity getProcUserId >>>>>>>>>>>>>>>> "+e);
            }
        }else if(umracRoleAssignUser.getUserId() != null && umracRoleAssignUser.getRoles() != null){
            try {
                Boolean emailStatus = false;
                Emails emails =  new Emails();
                emails.setTo(umracIdentitySetup.getEmail().trim());
                //emails.setTo("zoynobtumpa@gmail.com");
                //emails.setCc("ranaarchive99@gmail.com");
                emails.setSubject("Your requested user created and role assigned");
                emails.setEmailBody("Dear "+ umracIdentitySetup.getUserName().toString() + ",<br><br> Your requested user created and role assigned. <br><br> Regards, <br>STEP Admin");
                emails.setLogged_user_id(SecurityUtils.getCurrentUserId());
                emails.setEmail_type("Text Email");

                //String emailBuilder = umracIdentitySetup.getUserName()+"@gmail.com";
                String emailBuilder = umracIdentitySetup.getEmail().trim();

                User user = userService.createCustomUserInformationByUserModule(umracIdentitySetup.getUserName(), umracIdentitySetup.getuPw(), umracIdentitySetup.getEmpId(), null, emailBuilder, "en", umracRoleAssignUser.getRoles().toString(), true, result.getRoleId().getId());
                if(user != null){
                    mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                    log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                }
            }catch (Exception e){
                log.debug("Exception in save role at JHI_USER entity");
            }
        } else if(umracRoleAssignUser.getProcUserId() != null && umracRoleAssignUser.getRoles() != null){
            try {
                List<Map<String, Object>> authorities = rptJdbcDao.getAuthoritiesByUserId(umracRoleAssignUser.getProcUserId().getId(), umracRoleAssignUser.getRoles().toString());
                System.out.println("\n authorities "+authorities.toString());
                if(authorities != null && authorities.size() > 0){
                    System.out.println("\n authorities validate : "+authorities.toString());

                } else {
                    try {
                        User user = userRepository.findOne(umracRoleAssignUser.getProcUserId().getId());
                        System.out.println("\n user authorities "+user.toString());
                        Boolean emailStatus = false;
                        Emails emails =  new Emails();
                        emails.setTo(user.getEmail().trim());
                        //emails.setTo("zoynobtumpa@gmail.com");
                        //emails.setCc("ranaarchive99@gmail.com");
                        emails.setSubject("Your requested user role assigned");
                        emails.setEmailBody("Dear "+ user.getLogin().toString() + ",<br><br> Your requested user role "+ umracRoleAssignUser.getRoles().toString() +" assigned. <br><br> Regards, <br>STEP Admin");
                        emails.setLogged_user_id(SecurityUtils.getCurrentUserId());
                        emails.setEmail_type("Text Email");

                        //String emailBuilder = umracIdentitySetup.getUserName()+"@gmail.com";
                        String emailBuilder = user.getEmail().trim();

                        JhiUserAuthorityDto jhiUserAuthorityDto = new JhiUserAuthorityDto();
                        jhiUserAuthorityDto.setUser_id(umracRoleAssignUser.getProcUserId().getId());
                        jhiUserAuthorityDto.setAuthority_name(umracRoleAssignUser.getRoles());
                        jhiUserAuthorityDto.setRole_ids(result.getId());
                        jhiUserAuthorityDto.setAssign_track_id(result.getAssignTrackId());

                        //String responseCode = rptJdbcDao.updateAuthorityByUserId(jhiUserAuthorityDto);
                        User responseUser = userService.assignRole(user, umracRoleAssignUser.getRoles());
                        //if(responseCode == "200" && responseUser != null){
                        if(responseUser != null){
                            mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                            log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                        }
                    }catch (Exception e){
                        log.debug("Exception in save role at JHI_USER entity");
                    }
                }
            }catch (Exception e){
                log.debug("Exception in save role at JHI_USER entity");
            }
        }


        umracRoleAssignUserSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/umracRoleAssignUsers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("umracRoleAssignUser", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /umracRoleAssignUsers -> Updates an existing umracRoleAssignUser.
     */
    @RequestMapping(value = "/umracRoleAssignUsers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<UmracRoleAssignUser> updateUmracRoleAssignUser(@RequestBody UmracRoleAssignUser umracRoleAssignUser) throws URISyntaxException {
        log.debug("REST request to update UmracRoleAssignUser : {}", umracRoleAssignUser);
        if (umracRoleAssignUser.getId() == null) {
            return createUmracRoleAssignUser(umracRoleAssignUser);
        }
        /*if(umracRoleAssignUser.getProcUserId() != null && umracRoleAssignUser.getRoleId() != null){
            System.out.println("\n umracRoleAssignUser.getProcUserId() + umracRoleAssignUser.getRoleId() : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getRoleId().toString());
            try {
                System.out.println("\n >>>>>>>>>>>>>>>>>>>>> traced 1 : "+umracRoleAssignUser.toString());
                List<UmracRightsSetup> umracRightsSetupList = umracRightsSetupRepository.findByRoleId(umracRoleAssignUser.getRoleId().getId());
                Boolean emailStatus = false;
                Emails emails =  new Emails();
                emails.setTo(umracRoleAssignUser.getProcUserId().getEmail());
                emails.setSubject("Your requested role assigned");
                emails.setEmailBody("Dear "+ umracRoleAssignUser.getProcUserId().getLogin().toString() + ",<br><br> Your requested role assigned. <br><br> Regards, <br>STEP Admin");
                emails.setLogged_user_id(SecurityUtils.getCurrentUserId());
                emails.setEmail_type("Text Email");

                String emailBuilder = umracRoleAssignUser.getProcUserId().getEmail().trim();
                System.out.println("\n >>>>>>>>>>>>>>>>>>>>> traced 2 :"+emailBuilder.toString());
                boolean roleProcess = false;
                Set<Authority> authoritiesAre = new HashSet<>();
                for(UmracRightsSetup rights :umracRightsSetupList){
                    System.out.println("\n rights : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+rights.getSubModule_id());

                    List<UmracActionSetup> umracActionSetups =  umracActionSetupRepository.findBySubModuleId(rights.getSubModule_id());
                    for(UmracActionSetup umracActionSetup : umracActionSetups){
                        System.out.println("\n umracRoleAssignUser.getRoleId().getRoleContext().contains : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getRoleId().getRoleContext().toString());
                        if(umracRoleAssignUser.getRoleId().getRoleContext().contains(umracActionSetup.getSubmoduleId().getId().toString().trim()+"-"+umracActionSetup.getActionId().toString().trim())){
                            System.out.println("\n umracActionSetup : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracActionSetup.toString());
                            System.out.println("\n umracRoleAssignUser.getUserId().getUserName() : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getProcUserId().getLogin().toString());

                            User user =  userRepository.findUserByUserLogin(umracRoleAssignUser.getProcUserId().getLogin());
                            if(user != null){
                                System.out.println("\n user.getId() >>>>>>>>>>>>>>>>>>>>>> "+user.getId().toString());
                                List<Map<String, Object>> authorities=rptJdbcDao.GetAuthoritiesByUserIdAndRole(user.getId(), umracActionSetup.getActionUrl());
                                System.out.println("\n authorities >>>>>>>>>>>>>>>>>>>>>> "+authorities.toString());
                                if(authorities !=null && authorities.size() >0){
                                    System.out.println("\n This role already exist for this user >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+user.toString());
                                }else{
                                    Authority authority = authorityRepository.findOne(umracActionSetup.getActionUrl());
                                    authoritiesAre.add(authority);
                                    roleProcess = true;
                                    System.out.println("\n role name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracActionSetup.getActionUrl());
                                }
                            }else{
                                Authority authority = authorityRepository.findOne(umracActionSetup.getActionUrl());
                                authoritiesAre.add(authority);
                                roleProcess = true;
                                System.out.println("\n role name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracActionSetup.getActionUrl());
                            }
                        } else {
                            //User responseUser = userService.updateAssignedRoleWithInactiveChangeRole(user,)
                            System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> this action is not part of this role");
                        }
                    }
                }
                System.out.println("\n roleProcess existing : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+roleProcess);
                System.out.println("\n roleName existing : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+authoritiesAre.toString());
                System.out.println("\n getProcUserId : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+umracRoleAssignUser.getProcUserId().toString());
                *//*User user = userService.createCustomUserInformationByRights(umracIdentitySetup.getUserName(), umracIdentitySetup.getuPw(), umracIdentitySetup.getEmpId(), null, emailBuilder, "en", authoritiesAre, true, result.getRoleId().getId());
                if(user != null){
                    mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                    log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                }*//*
                try {
                    String responseCode = "200";
                    for (Authority authoritiesLoop : authoritiesAre) {
                        *//*JhiUserAuthorityDto jhiUserAuthorityDto = new JhiUserAuthorityDto();
                        jhiUserAuthorityDto.setUser_id(umracRoleAssignUser.getProcUserId().getId());
                        jhiUserAuthorityDto.setAuthority_name(authoritiesLoop.getName());
                        jhiUserAuthorityDto.setRole_ids(result.getId());
                        jhiUserAuthorityDto.setAssign_track_id(result.getAssignTrackId());*//*

                        //responseCode = rptJdbcDao.updateAuthorityByUserId(jhiUserAuthorityDto);
                        System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> jhiUserAuthorityDto : "+umracRoleAssignUser.getProcUserId().toString()+" --- "+authoritiesLoop.getName());
                        User responseUser = userService.assignRoleWithExistingRole(umracRoleAssignUser.getProcUserId(), authoritiesLoop.getName());
                        if (responseUser != null) {
                            responseCode = "200";
                        } else {
                            responseCode = "400";
                        }

                    }
                    if (responseCode == "200") {
                        mailService.sendEmail(emails.getTo(), emails.getSubject(), emails.getEmailBody(), false, true);
                        log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                    }
                } catch (Exception ex){
                    System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> responseCode : "+ex);
                }

            }catch (Exception e){
                log.debug("Exception in save role at JHI_USER entity getProcUserId");
                System.out.println("\n Exception in save role at JHI_USER entity getProcUserId >>>>>>>>>>>>>>>> "+e);
            }
        }*/
        UmracRoleAssignUser result = umracRoleAssignUserRepository.save(umracRoleAssignUser);
        umracRoleAssignUserSearchRepository.save(umracRoleAssignUser);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("umracRoleAssignUser", umracRoleAssignUser.getId().toString()))
            .body(result);
    }

    /**
     * GET  /umracRoleAssignUsers -> get all the umracRoleAssignUsers.
     */
    @RequestMapping(value = "/umracRoleAssignUsers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UmracRoleAssignUser>> getAllUmracRoleAssignUsers(Pageable pageable)
        throws URISyntaxException {
        Page<UmracRoleAssignUser> page = umracRoleAssignUserRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/umracRoleAssignUsers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /umracRoleAssignUsers/:id -> get the "id" umracRoleAssignUser.
     */
    @RequestMapping(value = "/umracRoleAssignUsers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UmracRoleAssignUser> getUmracRoleAssignUser(@PathVariable Long id) {
        log.debug("REST request to get UmracRoleAssignUser : {}", id);
        return Optional.ofNullable(umracRoleAssignUserRepository.findOne(id))
            .map(umracRoleAssignUser -> new ResponseEntity<>(
                umracRoleAssignUser,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /umracRoleAssignUsers/:id -> delete the "id" umracRoleAssignUser.
     */
    @RequestMapping(value = "/umracRoleAssignUsers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUmracRoleAssignUser(@PathVariable Long id) {
        log.debug("REST request to delete UmracRoleAssignUser : {}", id);

        umracRoleAssignUserRepository.delete(id);
        umracRoleAssignUserSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("umracRoleAssignUser", id.toString())).build();
    }

    /**
     * SEARCH  /_search/umracRoleAssignUsers/:query -> search for the umracRoleAssignUser corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/umracRoleAssignUsers/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UmracRoleAssignUser> searchUmracRoleAssignUsers(@PathVariable String query) {
        return StreamSupport
            .stream(umracRoleAssignUserSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

//    @RequestMapping(value = "/umracRoleAssignUsers/userName/",
//        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<Map> getRoleByName(@RequestParam Long value) {
//
//        log.debug("REST request to get umracRoleAssignUsers by name : {}", value);
//
//        Optional<UmracRoleAssignUser> umracRoleSetup = umracRoleAssignUserRepository.findOneByUser(value);
//
//       /* log.debug("user on check for----"+value);
//        log.debug("cmsCurriculum on check code----"+Optional.empty().equals(cmsCurriculum));*/
//        //generateAllRightsJson();
//        Map map = new HashMap();
//
//        map.put("value", value);
//
//        if (Optional.empty().equals(umracRoleSetup)) {
//            map.put("isValid", true);
//            return new ResponseEntity<Map>(map, HttpStatus.OK);
//
//        } else {
//            map.put("isValid", false);
//            return new ResponseEntity<Map>(map, HttpStatus.OK);
//        }
//    }

    @RequestMapping(value = "/umracRoleAssignUsers/userId/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UmracRoleAssignUser> getUsedUserById(@RequestParam Long value) {

        log.debug("REST request to get umracRoleAssignUsers by name : {}", value);

        return Optional.ofNullable(umracRoleAssignUserRepository.findOne(value))
            .map(umracRoleAssignUser -> new ResponseEntity<>(
                umracRoleAssignUser,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
