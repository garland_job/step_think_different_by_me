package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.DmlAuditLogHistory;
import gov.step.app.repository.DmlAuditLogHistoryRepository;
import gov.step.app.repository.search.DmlAuditLogHistorySearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DmlAuditLogHistory.
 */
@RestController
@RequestMapping("/api")
public class DmlAuditLogHistoryResource {

    private final Logger log = LoggerFactory.getLogger(DmlAuditLogHistoryResource.class);

    @Inject
    private DmlAuditLogHistoryRepository dmlAuditLogHistoryRepository;

    @Inject
    private DmlAuditLogHistorySearchRepository dmlAuditLogHistorySearchRepository;

    /**
     * POST  /dmlAuditLogHistorys -> Create a new dmlAuditLogHistory.
     */
    @RequestMapping(value = "/dmlAuditLogHistorys",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DmlAuditLogHistory> createDmlAuditLogHistory(@RequestBody DmlAuditLogHistory dmlAuditLogHistory) throws URISyntaxException {
        log.debug("REST request to save DmlAuditLogHistory : {}", dmlAuditLogHistory);
        if (dmlAuditLogHistory.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new dmlAuditLogHistory cannot already have an ID").body(null);
        }
        DmlAuditLogHistory result = dmlAuditLogHistoryRepository.save(dmlAuditLogHistory);
        dmlAuditLogHistorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/dmlAuditLogHistorys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("dmlAuditLogHistory", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dmlAuditLogHistorys -> Updates an existing dmlAuditLogHistory.
     */
    @RequestMapping(value = "/dmlAuditLogHistorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DmlAuditLogHistory> updateDmlAuditLogHistory(@RequestBody DmlAuditLogHistory dmlAuditLogHistory) throws URISyntaxException {
        log.debug("REST request to update DmlAuditLogHistory : {}", dmlAuditLogHistory);
        if (dmlAuditLogHistory.getId() == null) {
            return createDmlAuditLogHistory(dmlAuditLogHistory);
        }
        DmlAuditLogHistory result = dmlAuditLogHistoryRepository.save(dmlAuditLogHistory);
        dmlAuditLogHistorySearchRepository.save(dmlAuditLogHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("dmlAuditLogHistory", dmlAuditLogHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dmlAuditLogHistorys -> get all the dmlAuditLogHistorys.
     */
    @RequestMapping(value = "/dmlAuditLogHistorys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<DmlAuditLogHistory>> getAllDmlAuditLogHistorys(Pageable pageable)
        throws URISyntaxException {
        Page<DmlAuditLogHistory> page = dmlAuditLogHistoryRepository.findAll(pageable);
        System.out.println("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> sout : "+page.toString());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dmlAuditLogHistorys");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dmlAuditLogHistorys/:id -> get the "id" dmlAuditLogHistory.
     */
    @RequestMapping(value = "/dmlAuditLogHistorys/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DmlAuditLogHistory> getDmlAuditLogHistory(@PathVariable Long id) {
        log.debug("REST request to get DmlAuditLogHistory : {}", id);
        return Optional.ofNullable(dmlAuditLogHistoryRepository.findOne(id))
            .map(dmlAuditLogHistory -> new ResponseEntity<>(
                dmlAuditLogHistory,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dmlAuditLogHistorys/:id -> delete the "id" dmlAuditLogHistory.
     */
    @RequestMapping(value = "/dmlAuditLogHistorys/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDmlAuditLogHistory(@PathVariable Long id) {
        log.debug("REST request to delete DmlAuditLogHistory : {}", id);
        dmlAuditLogHistoryRepository.delete(id);
        dmlAuditLogHistorySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("dmlAuditLogHistory", id.toString())).build();
    }

    /**
     * SEARCH  /_search/dmlAuditLogHistorys/:query -> search for the dmlAuditLogHistory corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/dmlAuditLogHistorys/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DmlAuditLogHistory> searchDmlAuditLogHistorys(@PathVariable String query) {
        return StreamSupport
            .stream(dmlAuditLogHistorySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
