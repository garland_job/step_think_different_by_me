package gov.step.app.web.rest.jdbc.dao;

import gov.step.app.domain.HrEmployeeInfo;
import gov.step.app.domain.enumeration.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by yzaman on 3/16/17.
 */
public class HrEmployeeInfoCompactRowMapper implements RowMapper
{
    public HrEmployeeInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        HrEmployeeInfo empInfo = new HrEmployeeInfo();

        empInfo.setId(rs.getLong("ID"));
        empInfo.setEmployeeId(IsNull(rs, "EMPLOYEE_ID"));
        empInfo.setFullName(IsNull(rs, "FULL_NAME"));
        empInfo.setFullNameBn(IsNull(rs, "FULL_NAME_BN"));
        empInfo.setFatherName(IsNull(rs, "FATHER_NAME"));
        empInfo.setFatherNameBn(IsNull(rs, "FATHER_NAME_BN"));
        empInfo.setMotherName(IsNull(rs, "MOTHER_NAME"));
        empInfo.setMotherNameBn(IsNull(rs, "MOTHER_NAME_BN"));

        empInfo.setBirthDate(IsNullDate(rs, "BIRTH_DATE"));
        empInfo.setApointmentGoDate(IsNullDate(rs, "APOINTMENT_GO_DATE"));

        empInfo.setPresentId(IsNull(rs, "PRESENT_ID"));
        empInfo.setNationalId(IsNull(rs, "NATIONAL_ID"));
        empInfo.setEmailAddress(IsNull(rs, "EMAIL_ADDRESS"));
        empInfo.setMobileNumber(IsNull(rs, "MOBILE_NUMBER"));
        //empInfo.setGender(Gender.valueOf(IsNull(rs, "GENDER")));
        empInfo.setBirthPlace(IsNull(rs, "BIRTH_PLACE"));
        empInfo.setAnyDisease(IsNull(rs, "ANY_DISEASE"));
        empInfo.setOfficerStuff(IsNull(rs, "OFFICER_STUFF"));
        empInfo.setTinNumber(IsNull(rs, "TIN_NUMBER"));
        //empInfo.setMaritalStatus(maritalStatus.valueOf(IsNull(rs, "MARITAL_STATUS")));
        //empInfo.setBloodGroup(bloodGroup.valueOf(IsNull(rs, "BLOOD_GROUP")));
        //empInfo.setNationality(IsNull(rs, "NATIONALITY"));
        //empInfo.setQuota(jobQuota.valueOf(IsNull(rs, "QUOTA")));
        //empInfo.setBirthCertificateNo(IsNull(rs, "BIRTH_CERTIFICATE_NO"));
        //empInfo.setReligion(Religions.valueOf(IsNull(rs, "RELIGION")));
        //empInfo.setActiveStatus(IsNullLong(rs,"ACTIVE_STATUS") > 0 ? true: false );

        empInfo.setDateOfJoining(IsNullDate(rs, "DATE_OF_JOINING"));
        //empInfo.setPrlDate(IsNullDate(rs, "PRL_DATE"));
        empInfo.setRetirementDate(IsNullDate(rs, "RETIREMENT_DATE"));
        //empInfo.setLastWorkingDay(IsNullDate(rs, "LAST_WORKING_DAY"));
        return empInfo;
    }

    private String IsNull(ResultSet rs, String fieldName)
    {
        try
        {
            return rs.getString(fieldName);
        }
        catch(Exception ex){return "";}
    }

    private Long IsNullLong(ResultSet rs, String fieldName)
    {
        try
        {
            return rs.getLong(fieldName);
        }
        catch(Exception ex){return 0l;}
    }

    private LocalDate IsNullDate(ResultSet rs, String fieldName)
    {
        try
        {
            return rs.getDate(fieldName).toLocalDate();
        }
        catch(Exception ex){return null;}
    }
}
