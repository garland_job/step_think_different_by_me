package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.ErpAuthorityFlow;
import gov.step.app.domain.PfmsLoanAppFlow;
import gov.step.app.domain.PfmsLoanApplication;
import gov.step.app.domain.enumeration.ApprovalStatus;
import gov.step.app.domain.enumeration.ErpAuthorityAction;
import gov.step.app.repository.ErpAuthorityFlowRepository;
import gov.step.app.repository.PfmsLoanAppFlowRepository;
import gov.step.app.repository.PfmsLoanApplicationRepository;
import gov.step.app.repository.search.PfmsLoanAppFlowSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.apache.catalina.security.SecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PfmsLoanAppFlow.
 */
@RestController
@RequestMapping("/api")
public class PfmsLoanAppFlowResource {

    private final Logger log = LoggerFactory.getLogger(PfmsLoanAppFlowResource.class);

    @Inject
    private PfmsLoanAppFlowRepository pfmsLoanAppFlowRepository;

    @Inject
    private PfmsLoanAppFlowSearchRepository pfmsLoanAppFlowSearchRepository;

    @Inject
    private ErpAuthorityFlowRepository erpAuthorityFlowRepository;

    @Inject
    private PfmsLoanApplicationRepository pfmsLoanApplicationRepository;

    /**
     * POST  /pfmsLoanAppFlows -> Create a new pfmsLoanAppFlow.
     */
    @RequestMapping(value = "/pfmsLoanAppFlows",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PfmsLoanAppFlow> createPfmsLoanAppFlow(@RequestBody PfmsLoanAppFlow pfmsLoanAppFlow) throws URISyntaxException {
        log.debug("REST request to save PfmsLoanAppFlow : {}", pfmsLoanAppFlow);
        if (pfmsLoanAppFlow.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pfmsLoanAppFlow cannot already have an ID").body(null);
        }
        PfmsLoanAppFlow result = pfmsLoanAppFlowRepository.save(pfmsLoanAppFlow);
        pfmsLoanAppFlowSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pfmsLoanAppFlows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pfmsLoanAppFlow", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pfmsLoanAppFlows -> Updates an existing pfmsLoanAppFlow.
     */
    @RequestMapping(value = "/pfmsLoanAppFlows",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PfmsLoanAppFlow> updatePfmsLoanAppFlow(@RequestBody PfmsLoanAppFlow pfmsLoanAppFlow) throws URISyntaxException {
        log.debug("REST request to update PfmsLoanAppFlow : {}", pfmsLoanAppFlow);

        if (pfmsLoanAppFlow.getId() == null) {
            return createPfmsLoanAppFlow(pfmsLoanAppFlow);
        }
        PfmsLoanApplication pfmsLoanApplication = pfmsLoanAppFlow.getPfmsLoanApp();


        if(pfmsLoanAppFlow.getErpAuthorityFlow().getIsFinal() && pfmsLoanAppFlow.getErpAuthorityAction() != ErpAuthorityAction.Reject){
            pfmsLoanAppFlow.setErpAuthorityAction(ErpAuthorityAction.Approved);
            pfmsLoanApplication.setApprovalStatus(ApprovalStatus.Approved);
        }else if(pfmsLoanAppFlow.getErpAuthorityAction() == ErpAuthorityAction.Reject){
            pfmsLoanApplication.setApprovalStatus(ApprovalStatus.Reject);
            pfmsLoanAppFlow.setErpAuthorityAction(ErpAuthorityAction.Reject);
        }else{
            pfmsLoanApplication.setApprovalStatus(ApprovalStatus.OnAuthorization);
        }
        PfmsLoanAppFlow result = pfmsLoanAppFlowRepository.save(pfmsLoanAppFlow);
        pfmsLoanAppFlowSearchRepository.save(pfmsLoanAppFlow);
        pfmsLoanApplicationRepository.save(pfmsLoanApplication);
        if(!pfmsLoanAppFlow.getErpAuthorityFlow().getIsFinal() || pfmsLoanAppFlow.getErpAuthorityAction() != ErpAuthorityAction.Reject){
            PfmsLoanAppFlow pfmsLoanAppFlow1 = null;
            pfmsLoanAppFlow1 = pfmsLoanAppFlow;
            pfmsLoanAppFlow1.setErpAuthorityFlow(pfmsLoanAppFlow.getNewAuthorityFlow());
            pfmsLoanAppFlow1.setErpAuthorityAction(ErpAuthorityAction.OnAuthorization);
            pfmsLoanAppFlow1.setNewAuthorityFlow(null);
            pfmsLoanAppFlow1.setId(null);
            if (pfmsLoanAppFlow1.getId() == null) {
                return createPfmsLoanAppFlow(pfmsLoanAppFlow1);
            }
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pfmsLoanAppFlow", pfmsLoanAppFlow.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pfmsLoanAppFlows -> get all the pfmsLoanAppFlows.
     */
    @RequestMapping(value = "/pfmsLoanAppFlows",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PfmsLoanAppFlow>> getAllPfmsLoanAppFlows(Pageable pageable)
        throws URISyntaxException {
        Page<PfmsLoanAppFlow> page = pfmsLoanAppFlowRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pfmsLoanAppFlows");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pfmsLoanAppFlows/:id -> get the "id" pfmsLoanAppFlow.
     */
    @RequestMapping(value = "/pfmsLoanAppFlows/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PfmsLoanAppFlow> getPfmsLoanAppFlow(@PathVariable Long id) {
        log.debug("REST request to get PfmsLoanAppFlow : {}", id);
        return Optional.ofNullable(pfmsLoanAppFlowRepository.findOne(id))
            .map(pfmsLoanAppFlow -> new ResponseEntity<>(
                pfmsLoanAppFlow,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pfmsLoanAppFlows/:id -> delete the "id" pfmsLoanAppFlow.
     */
    @RequestMapping(value = "/pfmsLoanAppFlows/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePfmsLoanAppFlow(@PathVariable Long id) {
        log.debug("REST request to delete PfmsLoanAppFlow : {}", id);
        pfmsLoanAppFlowRepository.delete(id);
        pfmsLoanAppFlowSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pfmsLoanAppFlow", id.toString())).build();
    }

    /**
     * SEARCH  /_search/pfmsLoanAppFlows/:query -> search for the pfmsLoanAppFlow corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/pfmsLoanAppFlows/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PfmsLoanAppFlow> searchPfmsLoanAppFlows(@PathVariable String query) {
        return StreamSupport
            .stream(pfmsLoanAppFlowSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


    /**
     * GET  /pfmsLoanApplicationsByFilter/:fieldName/:fieldValue -> get employee by filter.
     */
    @RequestMapping(value = "/pfmsLoanAppFlowByActorName",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PfmsLoanAppFlow> pfmsLoanAppFlowByActorName() throws Exception {
//        log.debug("REST request to pfmsLoanAppFlowByActorName List : employeeInfo: {} ", actorName);
        List<PfmsLoanAppFlow> pfmsLoanAppFlowList = new ArrayList<>();
        List<ErpAuthorityFlow> erpAuthorityFlowList = erpAuthorityFlowRepository.findAllErpAuthorityFlowByCategory("PfmsLoanApplication");
        for(ErpAuthorityFlow erpAuthorityFlow: erpAuthorityFlowList){
            if(SecurityUtils.isCurrentUserInRole(erpAuthorityFlow.getActorName())){
                pfmsLoanAppFlowList.addAll(pfmsLoanAppFlowRepository.pfmsLoanAppFlowByActorName(erpAuthorityFlow.getActorName()));
            }
        }
        return pfmsLoanAppFlowList;


    }
}
