package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.HrDesignationSetup;
import gov.step.app.domain.enumeration.designationType;
import gov.step.app.domain.payroll.PrlPayscaleBasicInfo;
import gov.step.app.domain.payroll.PrlPayscaleInfo;
import gov.step.app.repository.HrDesignationSetupRepository;
import gov.step.app.repository.payroll.PrlPayscaleBasicInfoRepository;
import gov.step.app.repository.payroll.PrlPayscaleInfoRepository;
import gov.step.app.repository.search.HrDesignationSetupSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing HrDesignationSetup.
 */
@RestController
@RequestMapping("/api")
public class HrDesignationSetupResource {

    private final Logger log = LoggerFactory.getLogger(HrDesignationSetupResource.class);

    @Inject
    private HrDesignationSetupRepository hrDesignationSetupRepository;

    @Inject
    private HrDesignationSetupSearchRepository hrDesignationSetupSearchRepository;

    @Inject
    private PrlPayscaleInfoRepository prlPayscaleInfoRepository;

    @Inject
    private PrlPayscaleBasicInfoRepository prlPayscaleBasicInfoRepository;

    /**
     * POST  /hrDesignationSetups -> Create a new hrDesignationSetup.
     */
    @RequestMapping(value = "/hrDesignationSetups",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrDesignationSetup> createHrDesignationSetup(@Valid @RequestBody HrDesignationSetup hrDesignationSetup) throws URISyntaxException {
        log.debug("REST request to save HrDesignationSetup : {}", hrDesignationSetup);
        if (hrDesignationSetup.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("hrDesignationSetup", "idexists", "A new hrDesignationSetup cannot already have an ID")).body(null);
        }
        HrDesignationSetup result = hrDesignationSetupRepository.save(hrDesignationSetup);
        hrDesignationSetupSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/hrDesignationSetups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("hrDesignationSetup", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hrDesignationSetups -> Updates an existing hrDesignationSetup.
     */
    @RequestMapping(value = "/hrDesignationSetups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrDesignationSetup> updateHrDesignationSetup(@Valid @RequestBody HrDesignationSetup hrDesignationSetup) throws URISyntaxException {
        log.debug("REST request to update HrDesignationSetup : {}", hrDesignationSetup);
        if (hrDesignationSetup.getId() == null) {
            return createHrDesignationSetup(hrDesignationSetup);
        }
        HrDesignationSetup result = hrDesignationSetupRepository.save(hrDesignationSetup);
        hrDesignationSetupSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("hrDesignationSetup", hrDesignationSetup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hrDesignationSetups -> get all the hrDesignationSetups.
     */
    @RequestMapping(value = "/hrDesignationSetups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrDesignationSetup>> getAllHrDesignationSetups(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrDesignationSetups");
        Page<HrDesignationSetup> page = hrDesignationSetupRepository.findAll(pageable);
        System.out.println(" \n >>>>>>>>>>>>> Designation :: " +page.toString());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrDesignationSetups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrDesignationSetups -> get all the hrDesignationSetups.
     */
    @RequestMapping(value = "/hrDesignationSetups/bystat",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrDesignationSetup>> getAllHrDesignationSetupByStatus(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of hrDesignationSetups by status");
        Page<HrDesignationSetup> page = hrDesignationSetupRepository.findAllByActiveStatus(pageable, true);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrDesignationSetups/bystat");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrDepartmentSetupsByStat/:stat -> get the all department by active status.
     */
    @RequestMapping(value = "/hrDesignationSetupsByStat/{stat}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrDesignationSetup> getAllDesignationByActiveStatus(@PathVariable boolean stat)
        throws URISyntaxException
    {
        log.debug("REST all designation by status : {}", stat);
        List<HrDesignationSetup> designationList = hrDesignationSetupRepository.findAllByActiveStatus(stat);
        return  designationList;
    }

    /**
     * GET  /hrAllPayscaleByDesignationId/:id -> get the all department by active status.
     */
    @RequestMapping(value = "/hrAllPayscaleByDesignationId/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PrlPayscaleBasicInfo> getAllPayscaleByDesignationId(@PathVariable Long id)
        throws URISyntaxException
    {
        List<PrlPayscaleBasicInfo> prlPayscaleBasicInfos = new ArrayList<>();
        log.debug("REST all designation by status : {}", id);
        List<HrDesignationSetup> designationList = hrDesignationSetupRepository.findAllByDesignationInfoId(id);
        System.out.println("\n >>>>>>>>>>>>>designationList : "+designationList);
        for(HrDesignationSetup a:designationList){
            List<PrlPayscaleInfo> prlPayscaleInfos = null;
            prlPayscaleInfos = prlPayscaleInfoRepository.findByGradeId(a.getGradeInfo().getId());
            System.out.println("\n >>>>>>>>>>>>>prlPayscaleInfos : "+prlPayscaleInfos);
            for(PrlPayscaleInfo b:prlPayscaleInfos){
                List<PrlPayscaleBasicInfo> prlPayscaleBasicInfosProcessList = null;
                prlPayscaleBasicInfosProcessList = prlPayscaleBasicInfoRepository.findAllByPayScaleId(b.getId());
                System.out.println("\n >>>>>>>>>>>>>prlPayscaleBasicInfosProcessList : "+prlPayscaleBasicInfosProcessList);
                for(PrlPayscaleBasicInfo c:prlPayscaleBasicInfosProcessList){
                    System.out.println("\n >>>>>>>>>>>>>c : "+c);
                    try {
                        PrlPayscaleBasicInfo prlPayscaleBasicInfo = new PrlPayscaleBasicInfo();
                        prlPayscaleBasicInfo.setId(c.getId());
                        prlPayscaleBasicInfo.setBasicAmount(c.getBasicAmount());
                        prlPayscaleBasicInfos.add(prlPayscaleBasicInfo);
                        System.out.println("\n >>>>>>>>>>>>>c : " + c);
                    } catch (Exception e){
                        System.out.println("\n >>>>>>>>>>>>>>> Exception : "+e);
                    }
                }
            }
        }

        return  prlPayscaleBasicInfos;
    }

    /**
     * GET  /hrDesignationSetups/:id -> get the "id" hrDesignationSetup.
     */
    @RequestMapping(value = "/hrDesignationSetups/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrDesignationSetup> getHrDesignationSetup(@PathVariable Long id) {
        log.debug("REST request to get HrDesignationSetup : {}", id);
        HrDesignationSetup hrDesignationSetup = hrDesignationSetupRepository.findOne(id);
        return Optional.ofNullable(hrDesignationSetup)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /hrDesignationSetups/:id -> delete the "id" hrDesignationSetup.
     */
    @RequestMapping(value = "/hrDesignationSetups/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteHrDesignationSetup(@PathVariable Long id) {
        log.debug("REST request to delete HrDesignationSetup : {}", id);
        hrDesignationSetupRepository.delete(id);
        hrDesignationSetupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("hrDesignationSetup", id.toString())).build();
    }

    /**
     * SEARCH  /_search/hrDesignationSetups/:query -> search for the hrDesignationSetup corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/hrDesignationSetups/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrDesignationSetup> searchHrDesignationSetups(@PathVariable String query) {
        log.debug("REST request to search HrDesignationSetups for query {}", query);
        return StreamSupport
            .stream(hrDesignationSetupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @RequestMapping(value = "/hrDesignationSetupsByOrgTypeAndFilters/{orgtype}/{desigid}/{refid}",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> getDesignationByOrgCategoryAndFilters(@PathVariable String orgtype, @PathVariable Long desigid, @PathVariable Long refid)
    {
        List<HrDesignationSetup> modelInfoList = null;
        if(orgtype.equalsIgnoreCase("Institute"))
        {
            modelInfoList = hrDesignationSetupRepository.findOneByDesignationHeadAndInstitute(refid, desigid);
        }
        else
        {
            modelInfoList = hrDesignationSetupRepository.findOneByDesignationHeadAndWorkArea(refid, desigid);
        }
        log.debug("hrDesignationSetupsByOrgTypeAndFilters by refid: "+refid+", desigid: "+desigid+", orgtype: "+orgtype);
        Map map =new HashMap();
        map.put("orgtype", orgtype);
        map.put("refid", refid);
        map.put("desigid", desigid);
        if(modelInfoList!=null) map.put("total", modelInfoList.size());
        if(modelInfoList!=null && modelInfoList.size()>0)
        {
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }else{
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/hrDesignationSetupsByTypeLevelCat/{desigid}/{dtype}/{lvlid}/{catId}/{selfid}",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> hrDesignationSetupsByTypeLevelCat(@PathVariable Long desigid, @PathVariable String dtype, @PathVariable Long lvlid, @PathVariable Long catId, @PathVariable Long selfid)
    {
        designationType desigType = designationType.valueOf(dtype);

        log.debug("hrDesignationSetupsByTypeLevelCat by desigType: "+dtype+", levelId: "+lvlid+", catid: "+catId+", desigid: "+desigid+", selfid: "+selfid);

        List<HrDesignationSetup> modelInfoList = hrDesignationSetupRepository.findOneByInstLevelAndInstCatAndDesigLevel(desigType, lvlid, catId,desigid);

        Map map =new HashMap();
        map.put("desigType", dtype);
        map.put("levelId", lvlid);
        map.put("catid", catId);
        map.put("parentDesigId", desigid);
        map.put("selfid", selfid);
        HrDesignationSetup desigInfo = null;

        if(modelInfoList!=null)
            map.put("total", modelInfoList.size());

        if(modelInfoList!=null && modelInfoList.size()>0 )
        {
            log.debug("list size: {} ",modelInfoList.size());
            if(modelInfoList.size()>1)
            {
                map.put("isValid",false);
            }
            else if(modelInfoList.size()==1)
            {
                desigInfo = modelInfoList.get(0);
                if(selfid>0 && desigInfo.getId()==selfid)
                {
                    map.put("total", 1);
                    map.put("isValid",true);
                }
                else
                {
                    map.put("isValid",false);
                }
            }
            else
            {
                map.put("isValid",true);
            }
        }
        else
        {
            log.debug("list is null ");
            map.put("total", 0);
            map.put("isValid",true);
        }
        return new ResponseEntity<Map>(map,HttpStatus.OK);
    }

    /**
     * GET  /hrDesignationSetupsByType/:stat -> get the all designation by type.
     */
    @RequestMapping(value = "/hrDesignationSetupsByType/{type}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrDesignationSetup> getAllDesignationByActiveStatus(@PathVariable String type)
        throws URISyntaxException
    {
        log.debug("REST all designation by type : {}", type);
        designationType desigType = designationType.valueOf(type);
        List<HrDesignationSetup> designationSetupList = hrDesignationSetupRepository.findAllByDesigType(desigType, true);
        return  designationSetupList;
    }



 /**
     * GET  /hrDesignationSetupsByType/:stat -> get the all designation by type.
     */
    @RequestMapping(value = "/hrDesignationSetupsByTypes/types",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrDesignationSetup> getAllDesignationByActiveStatus( )
        throws URISyntaxException
    {
        log.debug("REST all designation by type : {}");
        List<HrDesignationSetup> designationSetupList = hrDesignationSetupRepository.findAllByDesigTypes(true);
        return  designationSetupList;
    }


    @RequestMapping(value = "/hrDesignationSetups/designationByTypeStatusName/{desigType}/{status}/{designationName}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrDesignationSetup>> getHrDesignationSetupByName(@PathVariable String desigType,@PathVariable Boolean activeStatus , @PathVariable String designationName){
        log.debug("REST request to get getHrDesignationSetupByName : {}", designationName);

        List<HrDesignationSetup> hrDesignationSetup = hrDesignationSetupRepository.findByDesigTypeAndDesignationName( designationType.valueOf(desigType),activeStatus,designationName);

        return Optional.ofNullable(hrDesignationSetup)
            .map(hrDesignationSetups -> new ResponseEntity<>(
                hrDesignationSetups,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

//        return Optional.ofNullable(employeeLoanRequisitionFormRepository.findReqDataByHrEmpID(employeeInfoID))
//            .map(employeeLoanRequisitionForm -> new ResponseEntity<>(
//                employeeLoanRequisitionForm,
//                HttpStatus.OK))
//            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



    @RequestMapping(value = "/HrDesignationSetupByOrgOrInst/{orgType}/{orgInstId}/{instLevelId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrDesignationSetup>> getHrDesignationSetupByOrgOrInst(@PathVariable String orgType,@PathVariable Long orgInstId ,@PathVariable Long instLevelId){
        List<HrDesignationSetup> hrDesignationSetups=null;
        if(orgType.equalsIgnoreCase("Organization"))
            hrDesignationSetups=hrDesignationSetupRepository.findAllByOrganizationDetailsId(orgInstId);
        else
            hrDesignationSetups=hrDesignationSetupRepository.findAllByInstituteLevelId(instLevelId);

        System.err.println(hrDesignationSetups);

        return new ResponseEntity<>(hrDesignationSetups,HttpStatus.OK);
    }


    @RequestMapping(value = "/HrDesignationSetupValidateByInstOrg/{orgType}/{orgInstId}/{instCatId}/{instLevelId}/{desigId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map<String,String>> isHrDesignationSetupValidateByInstOrg(@PathVariable String orgType,@PathVariable Long orgInstId,@PathVariable Long instCatId,@PathVariable Long instLevelId,@PathVariable Long desigId ){
        Boolean isValid=false;
        List<HrDesignationSetup> all=new ArrayList<>();

        if(orgType.equalsIgnoreCase("Institute"))
            all=hrDesignationSetupRepository.findAllByInstituteCategoryAndLevelWithUniqueDesig(instCatId,instLevelId,desigId);
        else
            all=hrDesignationSetupRepository.findAllByOrganizationDetailsIdWithUniqueDesig(orgInstId,desigId);
        isValid=all.size()==0;
        Map<String,String> map=new HashMap<>();
        map.put("isValid",isValid?"TRUE":"FALSE");
        return new ResponseEntity<>(map,HttpStatus.OK);
    }

}
