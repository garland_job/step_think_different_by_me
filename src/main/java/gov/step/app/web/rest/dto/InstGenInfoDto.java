package gov.step.app.web.rest.dto;

import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.InstituteType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by bappi on 11/6/17.
 */
public class InstGenInfoDto {

    private Long id;

    private String code;

    private String name;

    private String dateOfEstablishment;

    private String type;

    private String village;

    private String postOffice;

    private String postCode;

    private String landPhone;

    private String mobileNo;

    private String altMobileNo;

    private String email;

    private String consArea;

    private Integer status;

    private String website;

    private String faxNum;

    private String eiin;

    private String centerCode;

    private Boolean mpoEnlisted;

    private LocalDate dateOfMpo;

    private LocalDate firstRecognitionDate;

    private String firstAffiliationDate;

    private LocalDate lastExpireDateOfAffiliation;

    private String locality;

    private Boolean ownerType;

    private LocalDate dateCrated;

    private LocalDate dateModified;

    private Institute institute;

    private String upazila;

    private String instCategory;

    private String instLevel;

    private Date submitedDate;

    private String mpoCode;

    private String rejectComments;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDateOfEstablishment() {
        return dateOfEstablishment;
    }

    public void setDateOfEstablishment(String dateOfEstablishment) {
        this.dateOfEstablishment = dateOfEstablishment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getLandPhone() {
        return landPhone;
    }

    public void setLandPhone(String landPhone) {
        this.landPhone = landPhone;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAltMobileNo() {
        return altMobileNo;
    }

    public void setAltMobileNo(String altMobileNo) {
        this.altMobileNo = altMobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConsArea() {
        return consArea;
    }

    public void setConsArea(String consArea) {
        this.consArea = consArea;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFaxNum() {
        return faxNum;
    }

    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    public String getEiin() {
        return eiin;
    }

    public void setEiin(String eiin) {
        this.eiin = eiin;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public Boolean getMpoEnlisted() {
        return mpoEnlisted;
    }

    public void setMpoEnlisted(Boolean mpoEnlisted) {
        this.mpoEnlisted = mpoEnlisted;
    }

    public LocalDate getDateOfMpo() {
        return dateOfMpo;
    }

    public void setDateOfMpo(LocalDate dateOfMpo) {
        this.dateOfMpo = dateOfMpo;
    }

    public LocalDate getFirstRecognitionDate() {
        return firstRecognitionDate;
    }

    public void setFirstRecognitionDate(LocalDate firstRecognitionDate) {
        this.firstRecognitionDate = firstRecognitionDate;
    }

    public String getFirstAffiliationDate() {
        return firstAffiliationDate;
    }

    public void setFirstAffiliationDate(String firstAffiliationDate) {
        this.firstAffiliationDate = firstAffiliationDate;
    }

    public LocalDate getLastExpireDateOfAffiliation() {
        return lastExpireDateOfAffiliation;
    }

    public void setLastExpireDateOfAffiliation(LocalDate lastExpireDateOfAffiliation) {
        this.lastExpireDateOfAffiliation = lastExpireDateOfAffiliation;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public Boolean getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Boolean ownerType) {
        this.ownerType = ownerType;
    }

    public LocalDate getDateCrated() {
        return dateCrated;
    }

    public void setDateCrated(LocalDate dateCrated) {
        this.dateCrated = dateCrated;
    }

    public LocalDate getDateModified() {
        return dateModified;
    }

    public void setDateModified(LocalDate dateModified) {
        this.dateModified = dateModified;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    public String getUpazila() {
        return upazila;
    }

    public void setUpazila(String upazila) {
        this.upazila = upazila;
    }

    public String getInstCategory() {
        return instCategory;
    }

    public void setInstCategory(String instCategory) {
        this.instCategory = instCategory;
    }

    public String getInstLevel() {
        return instLevel;
    }

    public void setInstLevel(String instLevel) {
        this.instLevel = instLevel;
    }

    public Date getSubmitedDate() {
        return submitedDate;
    }

    public void setSubmitedDate(Date submitedDate) {
        this.submitedDate = submitedDate;
    }

    public String getMpoCode() {
        return mpoCode;
    }

    public void setMpoCode(String mpoCode) {
        this.mpoCode = mpoCode;
    }

    public String getRejectComments() {
        return rejectComments;
    }

    public void setRejectComments(String rejectComments) {
        this.rejectComments = rejectComments;
    }
}
