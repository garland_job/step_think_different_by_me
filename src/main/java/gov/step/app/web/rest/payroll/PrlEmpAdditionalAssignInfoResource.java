package gov.step.app.web.rest.payroll;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.payroll.PrlEmpAdditionalAssignInfo;
import gov.step.app.repository.payroll.PrlEmpAdditionalAssignInfoRepository;
import gov.step.app.repository.search.payroll.PrlEmpAdditionalAssignInfoSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing PrlEmpAdditionalAssignInfo.
 */
@RestController
@RequestMapping("/api")
public class PrlEmpAdditionalAssignInfoResource {

    private final Logger log = LoggerFactory.getLogger(PrlEmpAdditionalAssignInfoResource.class);

    @Inject
    private PrlEmpAdditionalAssignInfoRepository prlEmpAdditionalAssignInfoRepository;

    @Inject
    private PrlEmpAdditionalAssignInfoSearchRepository prlEmpAdditionalAssignInfoSearchRepository;

    /**
     * POST  /prlEmpAdditionalAssignInfos -> Create a new prlEmpAdditionalAssignInfo.
     */
    @RequestMapping(value = "/prlEmpAdditionalAssignInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PrlEmpAdditionalAssignInfo> createPrlEmpAdditionalAssignInfo(@Valid @RequestBody PrlEmpAdditionalAssignInfo prlEmpAdditionalAssignInfo) throws URISyntaxException {
        log.debug("REST request to save PrlEmpAdditionalAssignInfo : {}", prlEmpAdditionalAssignInfo);
        if (prlEmpAdditionalAssignInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new prlEmpAdditionalAssignInfo cannot already have an ID").body(null);
        }
        PrlEmpAdditionalAssignInfo result = prlEmpAdditionalAssignInfoRepository.save(prlEmpAdditionalAssignInfo);
        prlEmpAdditionalAssignInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/prlEmpAdditionalAssignInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("prlEmpAdditionalAssignInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /prlEmpAdditionalAssignInfos -> Updates an existing prlEmpAdditionalAssignInfo.
     */
    @RequestMapping(value = "/prlEmpAdditionalAssignInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PrlEmpAdditionalAssignInfo> updatePrlEmpAdditionalAssignInfo(@Valid @RequestBody PrlEmpAdditionalAssignInfo prlEmpAdditionalAssignInfo) throws URISyntaxException {
        log.debug("REST request to update PrlEmpAdditionalAssignInfo : {}", prlEmpAdditionalAssignInfo);
        if (prlEmpAdditionalAssignInfo.getId() == null) {
            return createPrlEmpAdditionalAssignInfo(prlEmpAdditionalAssignInfo);
        }
        PrlEmpAdditionalAssignInfo result = prlEmpAdditionalAssignInfoRepository.save(prlEmpAdditionalAssignInfo);
        prlEmpAdditionalAssignInfoSearchRepository.save(prlEmpAdditionalAssignInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("prlEmpAdditionalAssignInfo", prlEmpAdditionalAssignInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /prlEmpAdditionalAssignInfos -> get all the prlEmpAdditionalAssignInfos.
     */
    @RequestMapping(value = "/prlEmpAdditionalAssignInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PrlEmpAdditionalAssignInfo>> getAllPrlEmpAdditionalAssignInfos(Pageable pageable)
        throws URISyntaxException {
        Page<PrlEmpAdditionalAssignInfo> page = prlEmpAdditionalAssignInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/prlEmpAdditionalAssignInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /prlEmpAdditionalAssignInfos/:id -> get the "id" prlEmpAdditionalAssignInfo.
     */
    @RequestMapping(value = "/prlEmpAdditionalAssignInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PrlEmpAdditionalAssignInfo> getPrlEmpAdditionalAssignInfo(@PathVariable Long id) {
        log.debug("REST request to get PrlEmpAdditionalAssignInfo : {}", id);
        return Optional.ofNullable(prlEmpAdditionalAssignInfoRepository.findOne(id))
            .map(prlEmpAdditionalAssignInfo -> new ResponseEntity<>(
                prlEmpAdditionalAssignInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /prlEmpAdditionalAssignInfos/:id -> delete the "id" prlEmpAdditionalAssignInfo.
     */
    @RequestMapping(value = "/prlEmpAdditionalAssignInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePrlEmpAdditionalAssignInfo(@PathVariable Long id) {
        log.debug("REST request to delete PrlEmpAdditionalAssignInfo : {}", id);
        prlEmpAdditionalAssignInfoRepository.delete(id);
        prlEmpAdditionalAssignInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("prlEmpAdditionalAssignInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/prlEmpAdditionalAssignInfos/:query -> search for the prlEmpAdditionalAssignInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/prlEmpAdditionalAssignInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PrlEmpAdditionalAssignInfo> searchPrlEmpAdditionalAssignInfos(@PathVariable String query) {
        return StreamSupport
            .stream(prlEmpAdditionalAssignInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /prlSalaryStructureInfosByFilter/:psid/:grdid/:empid -> get address list by psid.
     */
    @RequestMapping(value = "/prlEmpAdditionalAssignInfosByEmpAllow/{empid}/{allowid}/{curid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> checkAdditionAssignByFilter(@PathVariable Long empid, @PathVariable Long allowid, @PathVariable Long curid)
    {
        log.debug("REST load additional assign by empid: {}, allowid: {}, current id: {} ",empid, allowid, curid);

        List<PrlEmpAdditionalAssignInfo> additinalAssignList = prlEmpAdditionalAssignInfoRepository.findByEmployeeAndAllowance(empid, allowid);

        PrlEmpAdditionalAssignInfo additionAssignInfo = null;
        Map map =new HashMap();
        if(additinalAssignList!=null )
        {
            log.debug("list size: {} ",additinalAssignList.size());
            if(additinalAssignList.size()>1)
            {
                map.put("total", additinalAssignList.size());
                map.put("empid", empid);
                map.put("currentid", curid);
                map.put("allowid", allowid);
                map.put("isValid",false);
            }
            else if(additinalAssignList.size()==1)
            {
                additionAssignInfo = additinalAssignList.get(0);
                log.debug("list size[0] , currentId: "+curid+", addId:"+additionAssignInfo.getId());
                if(curid > 0l && additionAssignInfo.getId()==curid)
                {
                    map.put("total", 1);
                    map.put("empid", empid);
                    map.put("allowid", allowid);
                    map.put("currentid", curid);
                    map.put("isValid",true);
                }
                else
                {
                    map.put("total", additinalAssignList.size());
                    map.put("empid", empid);
                    map.put("allowid", allowid);
                    map.put("currentid", curid);
                    map.put("isValid",false);
                }
            }
            else
            {
                map.put("total", 0);
                map.put("empid", empid);
                map.put("allowid", allowid);
                map.put("currentid", curid);
                map.put("isValid",true);
            }
        }
        else
        {
            log.debug("list is null ");
            map.put("total", 0);
            map.put("empid", empid);
            map.put("allowid", allowid);
            map.put("currentid", curid);
            map.put("isValid",true);
        }
        return new ResponseEntity<Map>(map,HttpStatus.OK);
    }
}
