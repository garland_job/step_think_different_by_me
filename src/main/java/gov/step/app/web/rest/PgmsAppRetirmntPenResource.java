package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.PensionApplicationStatusType;
import gov.step.app.repository.*;
import gov.step.app.repository.search.PgmsAppRetirmntPenSearchRepository;
import gov.step.app.security.AuthoritiesConstants;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.constnt.PGMSManagementConstant;
import gov.step.app.service.util.MiscFileInfo;
import gov.step.app.service.util.MiscFileUtilities;
import gov.step.app.web.rest.jdbc.dao.PGMSJdbcDao;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing PgmsAppRetirmntPen.
 */
@RestController
@RequestMapping("/api")
public class PgmsAppRetirmntPenResource {

    private final Logger log = LoggerFactory.getLogger(PgmsAppRetirmntPenResource.class);

    @Inject
    private PgmsAppRetirmntPenRepository pgmsAppRetirmntPenRepository;

    @Inject
    private PgmsAppRetirmntPenSearchRepository pgmsAppRetirmntPenSearchRepository;

    @Inject
    private PgmsAppRetirmntNmineRepository pgmsAppRetirmntNmineRepository;

    @Inject
    private HrNomineeInfoRepository hrNomineeInfoRepository;

    @Inject
    private HrEmpBankAccountInfoRepository hrEmpBankAccountInfoRepository;


    @Inject
    private PgmsRetirmntAttachInfoRepository pgmsRetirmntAttachInfoRepository;

    @Inject
    private PgmsAppRetirmntAttachRepository pgmsAppRetirmntAttachRepository;

    @Inject
    private PgmsAppRetirmntCalculationRepository pgmsAppRetirmntCalculationRepository;

    @Inject
    private PgmsElpcRepository pgmsElpcRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private  RetirementAppStatusLogRepository retirementAppStatusLogRepository;

    @Inject
    private PGMSJdbcDao pgmsJdbcDao;

    @Inject
    private NotificationStepRepository notificationStepRepository;

    MiscFileUtilities fileUtils = new MiscFileUtilities();
    /**
     * POST  /pgmsAppRetirmntPens -> Create a new pgmsAppRetirmntPen.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntPen> createPgmsAppRetirmntPen(@Valid @RequestBody PgmsAppRetirmntPen pgmsAppRetirmntPen) throws URISyntaxException {
        log.debug("REST request to save PgmsAppRetirmntPen : {}", pgmsAppRetirmntPen);
        if (pgmsAppRetirmntPen.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pgmsAppRetirmntPen cannot already have an ID").body(null);
        }
        pgmsAppRetirmntPen.setCreateDate(LocalDate.now());
        pgmsAppRetirmntPen.setCreateBy(SecurityUtils.getCurrentUserId());
        pgmsAppRetirmntPen.setApproveStatus(2);
        PgmsAppRetirmntPen result = pgmsAppRetirmntPenRepository.save(pgmsAppRetirmntPen);
        pgmsAppRetirmntPenSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pgmsAppRetirmntPens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pgmsAppRetirmntPen", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pgmsAppRetirmntPens -> Updates an existing pgmsAppRetirmntPen.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntPen> updatePgmsAppRetirmntPen(@Valid @RequestBody PgmsAppRetirmntPen pgmsAppRetirmntPen) throws URISyntaxException {
        log.debug("REST request to update PgmsAppRetirmntPen : {}", pgmsAppRetirmntPen);
        if (pgmsAppRetirmntPen.getId() == null) {
            return createPgmsAppRetirmntPen(pgmsAppRetirmntPen);
        }
        PgmsAppRetirmntPen result = pgmsAppRetirmntPenRepository.save(pgmsAppRetirmntPen);
        if(result.getApproveStatus() == 6 && SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DG)){
            NotificationStep notificationSteps = new NotificationStep();
            notificationSteps.setNotification(pgmsAppRetirmntPen.getHrEmployeeInfo().getFullName()+" Your Pension application is approved.");
            notificationSteps.setStatus(true);
            notificationSteps.setUrls("pgmsAppRetirmntPen");
            notificationSteps.setUserId(pgmsAppRetirmntPen.getHrEmployeeInfo().getUser().getId());
            notificationSteps.setNotifyDate(LocalDate.now());
            notificationSteps.setCreateBy(SecurityUtils.getCurrentUserId());
            notificationSteps.setCreateDate(LocalDate.now());
            notificationStepRepository.save(notificationSteps);
        }

        pgmsAppRetirmntPenSearchRepository.save(pgmsAppRetirmntPen);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pgmsAppRetirmntPen", pgmsAppRetirmntPen.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pgmsAppRetirmntPens -> get all the pgmsAppRetirmntPens.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PgmsAppRetirmntPen>> getAllPgmsAppRetirmntPens(Pageable pageable)
        throws URISyntaxException {
        Page<PgmsAppRetirmntPen> page = pgmsAppRetirmntPenRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pgmsAppRetirmntPens");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pgmsAppRetirmntPens/:id -> get the "id" pgmsAppRetirmntPen.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntPen> getPgmsAppRetirmntPen(@PathVariable Long id) {
        log.debug("REST request to get PgmsAppRetirmntPen : {}", id);
        return Optional.ofNullable(pgmsAppRetirmntPenRepository.findOne(id))
            .map(pgmsAppRetirmntPen -> new ResponseEntity<>(
                pgmsAppRetirmntPen,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pgmsAppRetirmntPens/:id -> delete the "id" pgmsAppRetirmntPen.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePgmsAppRetirmntPen(@PathVariable Long id) {
        log.debug("REST request to delete PgmsAppRetirmntPen : {}", id);
        pgmsAppRetirmntPenRepository.delete(id);
        pgmsAppRetirmntPenSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pgmsAppRetirmntPen", id.toString())).build();
    }

    /**
     * SEARCH  /_search/pgmsAppRetirmntPens/:query -> search for the pgmsAppRetirmntPen corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/pgmsAppRetirmntPens/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntPen> searchPgmsAppRetirmntPens(@PathVariable String query) {
        return StreamSupport
            .stream(pgmsAppRetirmntPenSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
    /**
     * Hr Nominee Infor List for PGMS Retirement Pension Application
     * to the query.
     */
    @RequestMapping(value = "/HrRetirmntNminesInfos/{empId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrNomineeInfo> getHrNomineeInfoByemployeeInfoId(@PathVariable long empId) {
        log.debug("REST request to get HrRetirmntNminesInfos : empId: {}", empId );
        List<HrNomineeInfo> hrNomineInfos = hrNomineeInfoRepository.findAllByEmployeeInfo(empId);
        return hrNomineInfos;
    }

    /**
     * Hr Nominee Infor List for PGMS Retirement Pension Application
     * to the query.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens/retirementNomineesInfos/{penId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PgmsAppRetirmntNmine>> getRetiremntNomineInfosInfoByPensionId(@PathVariable Long penId) {
        log.debug("REST request to get RetirementNomineesInfos : penId: {}", penId );
        List<PgmsAppRetirmntNmine> retiremntNomineInfos = pgmsAppRetirmntNmineRepository.findAllByRetirementPensionAppId(penId);

        for (PgmsAppRetirmntNmine cerModelInfo : retiremntNomineInfos) {
            MiscFileInfo empPhoto = new MiscFileInfo();
            empPhoto.fileName(cerModelInfo.getCertificateDocName())
                .contentType(cerModelInfo.getCertificateContentType())
                .filePath(PGMSManagementConstant.PGM_NOMINEE_CERTIFICATE_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            cerModelInfo.setBirthCertificate(empPhoto.fileData());
        }
        return Optional.ofNullable(retiremntNomineInfos)
            .map(pgmsAppRetirmntNmine -> new ResponseEntity<>(
                pgmsAppRetirmntNmine,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    /**
     * GET  /pgmsAppRetirementAttachsByTypeAndPension -> get all the pgmsAppRetiremeintAttachsByTypeAndPension.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens/byTypeAndPen/{attacheType}/{retirementPenId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntAttach> getRetirementAttachementListByTypeAndPensionId(@PathVariable String attacheType, @PathVariable Long retirementPenId)
    {
        log.debug("REST request to pgmsAppRetirmntPensByTypeAndPen : attachType: {} , retirementPenId: {}", attacheType, retirementPenId);

        List<PgmsRetirmntAttachInfo> attachmentList = pgmsRetirmntAttachInfoRepository.findAllByAttachType(attacheType);

        List<PgmsAppRetirmntAttach> retirmntAttachList = null;
        if (retirementPenId !=0) {
            retirmntAttachList = pgmsAppRetirmntAttachRepository.findAllByAppRetirmntPenId(retirementPenId);
            for (PgmsAppRetirmntAttach attachModelInfo : retirmntAttachList) {
                MiscFileInfo attachPhoto = new MiscFileInfo();
                attachPhoto.fileName(attachModelInfo.getAttachDocName())
                    .contentType(attachModelInfo.getAttachmentContentType())
                    .filePath(PGMSManagementConstant.PGM_RETIREMENT_ATTACH_FILE_DIR);
                attachPhoto = fileUtils.readFileAsByte(attachPhoto);
                attachModelInfo.setAttachment(attachPhoto.fileData());
            }
        }

        List<PgmsAppRetirmntAttach> retirmntAttachListNew = new ArrayList<PgmsAppRetirmntAttach>();

        for(PgmsRetirmntAttachInfo attachInfo: attachmentList)
        {
            boolean hasAttachment = false;
            if (retirementPenId !=0) {
                for (PgmsAppRetirmntAttach retireAttach : retirmntAttachList) {
                    if (retireAttach.getAttachName().equals(attachInfo)) {
                        retirmntAttachListNew.add(retireAttach);
                        hasAttachment = true;
                    }
                }
            }
            if(hasAttachment==false)
            {
                PgmsAppRetirmntAttach tempRetireAttach = new PgmsAppRetirmntAttach();
                tempRetireAttach.setAttachName(attachInfo);
                retirmntAttachListNew.add(tempRetireAttach);
            }
        }
        log.debug("ListSize: "+retirmntAttachListNew.size());
        return retirmntAttachListNew;
    }

    /**
     *   Delete Retirement Nominee Infos by AppRetirementPenId
     */
    @RequestMapping(value = "/deleteRetirementNomineesInfos/{penId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    void deleteRetirementNomineeInfoByAppRetirmntPenId(@PathVariable Long penId) {
        log.debug("Delete Retirement Nominee Info by AppRetirmntPenId: {} ",penId);
        pgmsAppRetirmntNmineRepository.deleteByAppRetirmntPenId(penId);
    }

    /**
     *   Delete Retirement Attach Infos by AppRetirementPenId
     */
    @RequestMapping(value = "/deleteRetirementAttachInfos/{penId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    void deleteRetirementAttachInfoByAppRetirmntPenId(@PathVariable Long penId) {
        log.debug("delete Retirement Attach Infos: {} ",penId);
        pgmsAppRetirmntAttachRepository.deleteByAppRetirmntPenId(penId);
    }

    /**
     *   Delete Retirement Calculation Infos by AppRetirementPenId
     */
    @RequestMapping(value = "/deleteRetirementCalculationInfos/{penId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    void deleteRetirementCalculationInfoByAppRetirmntPenId(@PathVariable Long penId) {
        log.debug("delete Retirement Attach Infos: {} ",penId);
        pgmsAppRetirmntCalculationRepository.deleteByAppRetirmntPenId(penId);
    }

    /**
     * Search Retirement Application Pending  Information
     * to the query.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens/applicationPendingList/{statusType}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntPen> getRetirementPensionApplicatinPendingList(@PathVariable Integer statusType) {
        log.debug("REST request to get pgmsAppRetirementPemdings : statusType: {}", statusType);
//        List<PgmsAppRetirmntPen> retPendingList = pgmsAppRetirmntPenRepository.findAllByAprvStatusOrderByIdAsc(statusType);
        PensionApplicationStatusType  pensionApplicationStatusType = null;
        List<PgmsAppRetirmntPen> retPendingList = new ArrayList<PgmsAppRetirmntPen>();

        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DG)){
            pensionApplicationStatusType = PensionApplicationStatusType.getPensionApplicationStatusTypeByRole(AuthoritiesConstants.DG);
            PensionApplicationStatusType applicationPreStatus = PensionApplicationStatusType.prevPensionApplicationStatusType(pensionApplicationStatusType.getCode());
            retPendingList = pgmsAppRetirmntPenRepository.findByApproveStatus(applicationPreStatus.getCode());
        }else if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            pensionApplicationStatusType = PensionApplicationStatusType.getPensionApplicationStatusTypeByRole(AuthoritiesConstants.ADMIN);
            PensionApplicationStatusType applicationPreStatus = PensionApplicationStatusType.prevPensionApplicationStatusType(pensionApplicationStatusType.getCode());
            retPendingList = pgmsAppRetirmntPenRepository.findByApproveStatus(applicationPreStatus.getCode());
        }else if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.AD)) {
            pensionApplicationStatusType = PensionApplicationStatusType.getPensionApplicationStatusTypeByRole(AuthoritiesConstants.AD);
            PensionApplicationStatusType applicationPreStatus = PensionApplicationStatusType.prevPensionApplicationStatusType(pensionApplicationStatusType.getCode());
            retPendingList = pgmsAppRetirmntPenRepository.findByApproveStatus(applicationPreStatus.getCode());
        }else{
            retPendingList = null;
        }

        return retPendingList;
    }

    /**
     * GET  /pgmsAppRetirementAttachsByTypeAndPension -> get all the pgmsAppRetiremeintAttachsByTypeAndPension.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPensByPenId/{retirementPenId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntAttach> getRetirementAttachementListByPensionId(@PathVariable Long retirementPenId)
    {
        log.debug("REST request to pgmsAppRetirmntPensByPenId : retirementPenId: {}", retirementPenId);
        List<PgmsAppRetirmntAttach> retirmntAttachList = pgmsAppRetirmntAttachRepository.findAllByAppRetirmntPenId(retirementPenId);
        for (PgmsAppRetirmntAttach modelInfo : retirmntAttachList) {
            MiscFileInfo attachPhoto = new MiscFileInfo();
            attachPhoto.fileName(modelInfo.getAttachDocName())
                .contentType(modelInfo.getAttachmentContentType())
                .filePath(PGMSManagementConstant.PGM_RETIREMENT_ATTACH_FILE_DIR);
            attachPhoto = fileUtils.readFileAsByte(attachPhoto);
            modelInfo.setAttachment(attachPhoto.fileData());
        }
        return retirmntAttachList;
    }

    /**
     * Hr Employee Bank Info for PGMS Retirement Pension Application
     * to the query.
     */
    @RequestMapping(value = "/pgmsAppRetirmntPens/bankInfoByEmpId/{empId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpBankAccountInfo> getEmpBankInfoByEmpId(@PathVariable long empId) {
        log.debug("REST request to get BankInfoByEmpId : empId: {}", empId);
        List<HrEmpBankAccountInfo> bankInfo = hrEmpBankAccountInfoRepository.findByEmployeeInfos(empId);
        return bankInfo;
    }

    /**
     * Hr Employee Bank Info for PGMS Retirement Pension Application
     * to the query.
     */
    @RequestMapping(value = "/empRetirmentAppByEmpId/{empId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntPen> getEmpRetirmentAppByEmpIdByEmpId(@PathVariable Long empId) {
        log.debug("REST request to get RmpRetirmentAppByEmpId : empId: {}", empId);
        List<PgmsAppRetirmntPen> empInfo = pgmsAppRetirmntPenRepository.findByEmployeeInfo(empId);
        return empInfo;
    }

    /**
     * Employee retirement calculation info for PGMS Retirement Pension Application
     * to the query.
     */
    @RequestMapping(value = "/retirmentCalculationInfos/{pensionId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public PgmsAppRetirmntCalculation getRetirmentCalculationInfosBypensionId(@PathVariable long pensionId) {
        log.debug("REST request to get RetirementNomineesInfos : pensionId: {}", pensionId );
        PgmsAppRetirmntCalculation calculationInfos = pgmsAppRetirmntCalculationRepository.findAllByAppRetirmntPenId(pensionId);
        return calculationInfos;
    }

    /**
     * Calculation Last Pay CodeInfo By EmpId From ELPC for PGMS Retirement Pension Application
     * to the query.
     */

    @RequestMapping(value = "/calculationLastPayCodeInfoByEmpId/{empId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsElpc> getCalculationLastPayCodeInfoByEmpId(@PathVariable Long empId) {
        log.debug("REST request to get CalculationLastPayCodeInfoByEmpId : empId: {}", empId);
        List<PgmsElpc> lastPayCode = pgmsElpcRepository.findAllByHrEmpId(empId);
        return lastPayCode;
    }


    public String getCurrentUserRole() {
        if (SecurityUtils.isCurrentUserInRole("ROLE_DG")) {
            return "ROLE_DG";
        } else if (SecurityUtils.isCurrentUserInRole("ROLE_ADMIN")) {
            return "ROLE_ADMIN";
        } else if (SecurityUtils.isCurrentUserInRole("ROLE_AD")) {
            return "ROLE_AD";
        }else {
            return null;
        }

    }
    @RequestMapping(value = "/pgmsAppRetirmntPens/forwardToList/{type}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PensionApplicationStatusType> getApplicationApproveList(@PathVariable String type) {
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + type);
//        if (type != null && (type.equals("BM") || type.equals("VOC"))) {
            log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> comes to if  ");
            return PensionApplicationStatusType.getPensionApplicationApproveList(type, getCurrentUserRole());
//        }
//        else {
//            log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> comes to else  ");
//            return new ArrayList<PensionApplicationStatusType>();
//        }
    }


    @RequestMapping(value = "/pgmsAppRetirmntPens/forward/{forwardTo}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntPen> forwardRetirementPensionApplication(@Valid @RequestBody PgmsAppRetirmntPen pensionApplication, @PathVariable Integer forwardTo, @RequestParam(required = false, name = "cause") String cause) throws URISyntaxException {
        log.debug("REST request to forwardRetirementPensionApplication : {}", pensionApplication);
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> printing remark comment :"+cause);

        pensionApplication.setApproveStatus(PensionApplicationStatusType.prevPensionApplicationStatusType(forwardTo).getCode());
        pensionApplication.setUpdateBy(SecurityUtils.getCurrentUserId());
        pensionApplication.setUpdateDate(LocalDate.now());
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.AD)){
            if(forwardTo == PensionApplicationStatusType.FORWARDBYDG.getCode()){
                pensionApplication.setApproveComment("Waiting For Final Approve");
            }
        }
        PgmsAppRetirmntPen result = pgmsAppRetirmntPenRepository.save(pensionApplication);

        RetirementAppStatusLog  applicationStatusLog = new RetirementAppStatusLog();
        applicationStatusLog.setApprovedDate((LocalDate.now()));
        applicationStatusLog.setPgmsAppRetirmntPen(result);
        applicationStatusLog.setStatus(result.getApproveStatus());
        applicationStatusLog.setCause(cause);
        applicationStatusLog.setRemarks(
            PensionApplicationStatusType.getPensionApplicationStatusTypeByRole(getCurrentUserRole()).getViewMessage() +
                " Forwarded to " +
                PensionApplicationStatusType.fromInt(forwardTo).getViewMessage());
        applicationStatusLog.setUser(userRepository.findOneByLogin(SecurityUtils.getCurrentUser().getUsername()).get());
        retirementAppStatusLogRepository.save(applicationStatusLog);

//        retirementAppStatusLogRepository.save(mpoApplication);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pgmsAppRetirmntPen", pensionApplication.getId().toString()))
            .body(result);
    }

    @RequestMapping(value = "/pgmsAppRetirmntPens/applicationListByApproveStatus/{statusType}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntPen> getPensionApplicatinByApproveStatus(@PathVariable Integer statusType) {
        log.debug("REST request to get getPensionApplicatinByApproveStatus : statusType: {}", statusType);
//        List<PgmsAppRetirmntPen> retPendingList = pgmsAppRetirmntPenRepository.findAllByAprvStatusOrderByIdAsc(statusType);

        List<PgmsAppRetirmntPen>    retPendingList = pgmsAppRetirmntPenRepository.findByApproveStatus(statusType);

        return retPendingList;
    }

    @RequestMapping(value = "/pgmsAppRetirmntPens/approvedListByRole",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PgmsAppRetirmntPen> getApprovedListByRole() {
        log.debug("REST request to getApprovedListByRole");
        List<PgmsAppRetirmntPen> retPendingList = new ArrayList<PgmsAppRetirmntPen>();
        if(getCurrentUserRole() !=null){
        PensionApplicationStatusType statusType = PensionApplicationStatusType.getPensionApplicationStatusTypeByRole(getCurrentUserRole());

            List<Long> statusLogs =    pgmsJdbcDao.findMaxStatusDataFromStatusLog(statusType.getCode());
//        List<PgmsAppRetirmntPen> statusLogs = retirementAppStatusLogRepository.findMaxByStatus(statusType.getCode());
            for(Long id : statusLogs){
               PgmsAppRetirmntPen pgms =  pgmsAppRetirmntPenRepository.findOne(id);
               retPendingList.add(pgms);
            }
        }
        return retPendingList;
    }
}
