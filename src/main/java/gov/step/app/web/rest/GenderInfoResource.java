package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.GenderInfo;
import gov.step.app.repository.GenderInfoRepository;
import gov.step.app.repository.search.GenderInfoSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GenderInfo.
 */
@RestController
@RequestMapping("/api")
public class GenderInfoResource {

    private final Logger log = LoggerFactory.getLogger(GenderInfoResource.class);

    @Inject
    private GenderInfoRepository genderInfoRepository;

    @Inject
    private GenderInfoSearchRepository genderInfoSearchRepository;

    /**
     * POST  /genderInfos -> Create a new genderInfo.
     */
    @RequestMapping(value = "/genderInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GenderInfo> createGenderInfo(@RequestBody GenderInfo genderInfo) throws URISyntaxException {
        log.debug("REST request to save GenderInfo : {}", genderInfo);
        if (genderInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new genderInfo cannot already have an ID").body(null);
        }
        GenderInfo result = genderInfoRepository.save(genderInfo);
        genderInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/genderInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("genderInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /genderInfos -> Updates an existing genderInfo.
     */
    @RequestMapping(value = "/genderInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GenderInfo> updateGenderInfo(@RequestBody GenderInfo genderInfo) throws URISyntaxException {
        log.debug("REST request to update GenderInfo : {}", genderInfo);
        if (genderInfo.getId() == null) {
            return createGenderInfo(genderInfo);
        }
        GenderInfo result = genderInfoRepository.save(genderInfo);
        genderInfoSearchRepository.save(genderInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("genderInfo", genderInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /genderInfos -> get all the genderInfos.
     */
    @RequestMapping(value = "/genderInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GenderInfo>> getAllGenderInfos(Pageable pageable)
        throws URISyntaxException {
        Page<GenderInfo> page = genderInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/genderInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /genderInfos/:id -> get the "id" genderInfo.
     */
    @RequestMapping(value = "/genderInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GenderInfo> getGenderInfo(@PathVariable Long id) {
        log.debug("REST request to get GenderInfo : {}", id);
        return Optional.ofNullable(genderInfoRepository.findOne(id))
            .map(genderInfo -> new ResponseEntity<>(
                genderInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /genderInfos/:id -> delete the "id" genderInfo.
     */
    @RequestMapping(value = "/genderInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGenderInfo(@PathVariable Long id) {
        log.debug("REST request to delete GenderInfo : {}", id);
        genderInfoRepository.delete(id);
        genderInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("genderInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/genderInfos/:query -> search for the genderInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/genderInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<GenderInfo> searchGenderInfos(@PathVariable String query) {
        return StreamSupport
            .stream(genderInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
