package gov.step.app.web.rest.dto;

/**
 * Created by rana on 10/11/17.
 */
public class JhiUserAuthorityDto {

    private Long user_id;
    private String authority_name;
    private Long role_ids;
    private String assign_track_id;

    public JhiUserAuthorityDto(){}

    public JhiUserAuthorityDto(Long user_id, String authority_name, Long role_ids, String assign_track_id) {
        this.user_id = user_id;
        this.authority_name = authority_name;
        this.role_ids = role_ids;
        this.assign_track_id = assign_track_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getAuthority_name() {
        return authority_name;
    }

    public void setAuthority_name(String authority_name) {
        this.authority_name = authority_name;
    }

    public Long getRole_ids() {
        return role_ids;
    }

    public void setRole_ids(Long role_ids) {
        this.role_ids = role_ids;
    }

    public String getAssign_track_id() {
        return assign_track_id;
    }

    public void setAssign_track_id(String assign_track_id) {
        this.assign_track_id = assign_track_id;
    }

    @Override
    public String toString() {
        return "JhiUserAuthorityDto{" +
            "user_id=" + user_id +
            ", authority_name='" + authority_name + '\'' +
            ", role_ids=" + role_ids +
            ", assign_track_id='" + assign_track_id + '\'' +
            '}';
    }
}
