package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.ErpAuthFlowProcess;
import gov.step.app.repository.ErpAuthFlowProcessRepository;
import gov.step.app.repository.search.ErpAuthFlowProcessSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ErpAuthFlowProcess.
 */
@RestController
@RequestMapping("/api")
public class ErpAuthFlowProcessResource {

    private final Logger log = LoggerFactory.getLogger(ErpAuthFlowProcessResource.class);

    @Inject
    private ErpAuthFlowProcessRepository erpAuthFlowProcessRepository;

    @Inject
    private ErpAuthFlowProcessSearchRepository erpAuthFlowProcessSearchRepository;

    /**
     * POST  /erpAuthFlowProcesss -> Create a new erpAuthFlowProcess.
     */
    @RequestMapping(value = "/erpAuthFlowProcesss",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErpAuthFlowProcess> createErpAuthFlowProcess(@Valid @RequestBody ErpAuthFlowProcess erpAuthFlowProcess) throws URISyntaxException {
        log.debug("REST request to save ErpAuthFlowProcess : {}", erpAuthFlowProcess);
        if (erpAuthFlowProcess.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new erpAuthFlowProcess cannot already have an ID").body(null);
        }
        ErpAuthFlowProcess result = erpAuthFlowProcessRepository.save(erpAuthFlowProcess);
        erpAuthFlowProcessSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/erpAuthFlowProcesss/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("erpAuthFlowProcess", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /erpAuthFlowProcesss -> Updates an existing erpAuthFlowProcess.
     */
    @RequestMapping(value = "/erpAuthFlowProcesss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErpAuthFlowProcess> updateErpAuthFlowProcess(@Valid @RequestBody ErpAuthFlowProcess erpAuthFlowProcess) throws URISyntaxException {
        log.debug("REST request to update ErpAuthFlowProcess : {}", erpAuthFlowProcess);
        if (erpAuthFlowProcess.getId() == null) {
            return createErpAuthFlowProcess(erpAuthFlowProcess);
        }
        ErpAuthFlowProcess result = erpAuthFlowProcessRepository.save(erpAuthFlowProcess);
        erpAuthFlowProcessSearchRepository.save(erpAuthFlowProcess);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("erpAuthFlowProcess", erpAuthFlowProcess.getId().toString()))
            .body(result);
    }

    /**
     * GET  /erpAuthFlowProcesss -> get all the erpAuthFlowProcesss.
     */
    @RequestMapping(value = "/erpAuthFlowProcesss",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ErpAuthFlowProcess>> getAllErpAuthFlowProcesss(Pageable pageable)
        throws URISyntaxException {
        Page<ErpAuthFlowProcess> page = erpAuthFlowProcessRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/erpAuthFlowProcesss");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /erpAuthFlowProcesss/:id -> get the "id" erpAuthFlowProcess.
     */
    @RequestMapping(value = "/erpAuthFlowProcesss/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErpAuthFlowProcess> getErpAuthFlowProcess(@PathVariable Long id) {
        log.debug("REST request to get ErpAuthFlowProcess : {}", id);
        return Optional.ofNullable(erpAuthFlowProcessRepository.findOne(id))
            .map(erpAuthFlowProcess -> new ResponseEntity<>(
                erpAuthFlowProcess,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /erpAuthFlowProcesss/:id -> delete the "id" erpAuthFlowProcess.
     */
    @RequestMapping(value = "/erpAuthFlowProcesss/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteErpAuthFlowProcess(@PathVariable Long id) {
        log.debug("REST request to delete ErpAuthFlowProcess : {}", id);
        erpAuthFlowProcessRepository.delete(id);
        erpAuthFlowProcessSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("erpAuthFlowProcess", id.toString())).build();
    }

    /**
     * SEARCH  /_search/erpAuthFlowProcesss/:query -> search for the erpAuthFlowProcess corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/erpAuthFlowProcesss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ErpAuthFlowProcess> searchErpAuthFlowProcesss(@PathVariable String query) {
        return StreamSupport
            .stream(erpAuthFlowProcessSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /erpAuthFlowProcessListByDomain/:fieldName/:fieldValue -> get employee by filter.
     */
    @RequestMapping(value = "/erpAuthFlowProcessListByDomain/{refDomainId, refDomainName}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ErpAuthFlowProcess> erpAuthFlowProcessListByDomain(@PathVariable long refDomainId, @PathVariable String refDomainName) throws Exception {
        log.debug("REST request to erpAuthFlowProcessListByDomain List : employeeInfo: {} ", refDomainId);
        return erpAuthFlowProcessRepository.findAllErpAuthFlowProcessByDomain(refDomainId, refDomainName);

    }
}
