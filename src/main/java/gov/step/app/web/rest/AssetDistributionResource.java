package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.repository.*;
import gov.step.app.repository.search.AssetDistributionSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.MailService;
import gov.step.app.web.rest.jdbc.dao.AssetDao;
import gov.step.app.web.rest.util.DateResource;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import gov.step.app.web.rest.util.TransactionIdResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AssetDistribution.
 */
@RestController
@RequestMapping("/api")
public class AssetDistributionResource {

    private final Logger log = LoggerFactory.getLogger(AssetDistributionResource.class);

    @Inject
    private AssetDistributionRepository assetDistributionRepository;

    @Inject
    private AssetDistributionSearchRepository assetDistributionSearchRepository;

    @Inject
    private AssetRequisitionRepository assetRequisitionRepository;

    @Inject
    private AssetDao assetDao;

    @Inject
    private NotificationStepRepository notificationStepRepository;

    @Inject
    private MailService mailService;

    @Inject
    private HrEmployeeInfoRepository hrEmployeeInfoRepository;

   /* @Inject
    private UserRepository userRepository;*/

    DateResource dr=new DateResource();
    TransactionIdResource transactionId =new TransactionIdResource();
    /**
     * POST  /assetDistributions -> Create a new assetDistribution.
     */
    @RequestMapping(value = "/assetDistributions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AssetDistribution> createAssetDistribution(@Valid @RequestBody AssetDistribution assetDistribution) throws URISyntaxException {
        log.debug("REST request to save AssetDistribution : {}", assetDistribution);
        if (assetDistribution.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new assetDistribution cannot already have an ID").body(null);
        }
        System.out.println("\n Got here");

        assetDistribution.setCreateBy(SecurityUtils.getCurrentUserId());
        assetDistribution.setCreateDate(DateResource.getDateAsLocalDate());
        assetDistribution.setAvailableQuantity(null);
        AssetDistribution result = assetDistributionRepository.save(assetDistribution);

        HrEmployeeInfo hrEmployeeInfo = new HrEmployeeInfo();
        hrEmployeeInfo = hrEmployeeInfoRepository.findByEmployeeId(assetDistribution.getEmploeeIds().toString());

        if(result != null){
            //User user = userRepository.findOneByUserId(assetDistribution.getHrEmployeeInfo().getUser().getId());
            NotificationStep notificationSteps = new NotificationStep();
            notificationSteps.setNotification(" Your requested requisition approved by Admin Id :" + SecurityUtils.getCurrentUserId());
            notificationSteps.setStatus(true);
            notificationSteps.setUrls("assetRequisition");
            notificationSteps.setUserId(hrEmployeeInfo.getUser().getId());
            notificationSteps.setCreateBy(SecurityUtils.getCurrentUserId());
            notificationSteps.setCreateDate(DateResource.getDateAsLocalDate());
            notificationSteps.setRemarks("Asset Module");
            notificationStepRepository.save(notificationSteps);
        }
            return ResponseEntity.created(new URI("/api/assetDistributions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("assetDistribution", result.getId().toString()))
                .body(result);

    }

    /**
     * PUT  /assetDistributions -> Updates an existing assetDistribution.
     */
    @RequestMapping(value = "/assetDistributions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AssetDistribution> updateAssetDistribution(@Valid @RequestBody AssetDistribution assetDistribution) throws URISyntaxException {
        log.debug("REST request to update AssetDistribution : {}", assetDistribution);
        if (assetDistribution.getId() == null) {
            return createAssetDistribution(assetDistribution);
        }

        assetDistribution.setUpdateBy(SecurityUtils.getCurrentUserId());
        assetDistribution.setUpdateDate(dr.getDateAsLocalDate());

        AssetDistribution result = assetDistributionRepository.save(assetDistribution);
        assetDistributionSearchRepository.save(assetDistribution);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("assetDistribution", assetDistribution.getId().toString()))
            .body(result);
    }

    /**
     * GET  /assetDistributions -> get all the assetDistributions.
     */
    @RequestMapping(value = "/assetDistributions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<AssetDistribution>> getAllAssetDistributions(Pageable pageable)
        throws URISyntaxException {
        Page<AssetDistribution> page = assetDistributionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/assetDistributions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /assetDistributions/:id -> get the "id" assetDistribution.
     */
    @RequestMapping(value = "/assetDistributions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AssetDistribution> getAssetDistribution(@PathVariable Long id) {
        log.debug("REST request to get AssetDistribution : {}", id);
        return Optional.ofNullable(assetDistributionRepository.findOne(id))
            .map(assetDistribution -> new ResponseEntity<>(
                assetDistribution,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /assetDistributions/:id -> delete the "id" assetDistribution.
     */
    @RequestMapping(value = "/assetDistributions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAssetDistribution(@PathVariable Long id) {
        log.debug("REST request to delete AssetDistribution : {}", id);
        assetDistributionRepository.delete(id);
        assetDistributionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("assetDistribution", id.toString())).build();
    }

    /**
     * SEARCH  /_search/assetDistributions/:query -> search for the assetDistribution corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/assetDistributions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AssetDistribution> searchAssetDistributions(@PathVariable String query) {
        return StreamSupport
            .stream(assetDistributionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @RequestMapping(value = "/assetDistributions/assetByCode/{employeeId}/{assetCode}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public AssetDistribution AssetRecordCode(@PathVariable Long employeeId,@PathVariable String assetCode) {
        log.debug("REST request to get Employee ID : {}", assetCode);
        return assetDistributionRepository.findAllByAssetCode(employeeId,assetCode);

    }

    @RequestMapping(value = "/assetDistributions/assetByCodeQuantity/{id}/{assetCode}/{quantity}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void AssetDistributionQuantity(@PathVariable Long id,@PathVariable String assetCode,@PathVariable Long quantity) {
        log.debug("REST request to get Asset Code : {}", assetCode);
        assetDao.assetDistributionQuantityUpdate(id,assetCode,quantity);

    }

    @RequestMapping(value = "/assetDistributions/assetRequisitionStatus/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void AssetRequisitionUpdatedStatus(@PathVariable Long id) {
        log.debug("REST request to get AssetRequisition : {}", id);
        assetDao.AssetRequisitionUpdateStatus(id);
    }

}
