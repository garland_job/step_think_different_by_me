package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.JasperReportSecurity;
import gov.step.app.repository.JasperReportSecurityRepository;
import gov.step.app.repository.search.JasperReportSecuritySearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JasperReportSecurity.
 */
@RestController
@RequestMapping("/api")
public class JasperReportSecurityResource {

    private final Logger log = LoggerFactory.getLogger(JasperReportSecurityResource.class);

    @Inject
    private JasperReportSecurityRepository jasperReportSecurityRepository;

    @Inject
    private JasperReportSecuritySearchRepository jasperReportSecuritySearchRepository;

    /**
     * POST  /jasperReportSecuritys -> Create a new jasperReportSecurity.
     */
    @RequestMapping(value = "/jasperReportSecuritys",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JasperReportSecurity> createJasperReportSecurity(@Valid @RequestBody JasperReportSecurity jasperReportSecurity) throws URISyntaxException {
        log.debug("REST request to save JasperReportSecurity : {}", jasperReportSecurity);
        if (jasperReportSecurity.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new jasperReportSecurity cannot already have an ID").body(null);
        }
        JasperReportSecurity result = jasperReportSecurityRepository.save(jasperReportSecurity);
        jasperReportSecuritySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/jasperReportSecuritys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("jasperReportSecurity", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jasperReportSecuritys -> Updates an existing jasperReportSecurity.
     */
    @RequestMapping(value = "/jasperReportSecuritys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JasperReportSecurity> updateJasperReportSecurity(@Valid @RequestBody JasperReportSecurity jasperReportSecurity) throws URISyntaxException {
        log.debug("REST request to update JasperReportSecurity : {}", jasperReportSecurity);
        if (jasperReportSecurity.getId() == null) {
            return createJasperReportSecurity(jasperReportSecurity);
        }
        JasperReportSecurity result = jasperReportSecurityRepository.save(jasperReportSecurity);
        jasperReportSecuritySearchRepository.save(jasperReportSecurity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("jasperReportSecurity", jasperReportSecurity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jasperReportSecuritys -> get all the jasperReportSecuritys.
     */
    @RequestMapping(value = "/jasperReportSecuritys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<JasperReportSecurity>> getAllJasperReportSecuritys(Pageable pageable)
        throws URISyntaxException {
        Page<JasperReportSecurity> page = jasperReportSecurityRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jasperReportSecuritys");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jasperReportSecuritys/:id -> get the "id" jasperReportSecurity.
     */
    @RequestMapping(value = "/jasperReportSecuritys/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JasperReportSecurity> getJasperReportSecurity(@PathVariable Long id) {
        log.debug("REST request to get JasperReportSecurity : {}", id);
        return Optional.ofNullable(jasperReportSecurityRepository.findOne(id))
            .map(jasperReportSecurity -> new ResponseEntity<>(
                jasperReportSecurity,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /jasperReportSecuritys/:id -> delete the "id" jasperReportSecurity.
     */
    @RequestMapping(value = "/jasperReportSecuritys/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteJasperReportSecurity(@PathVariable Long id) {
        log.debug("REST request to delete JasperReportSecurity : {}", id);
        jasperReportSecurityRepository.delete(id);
        jasperReportSecuritySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("jasperReportSecurity", id.toString())).build();
    }

    /**
     * SEARCH  /_search/jasperReportSecuritys/:query -> search for the jasperReportSecurity corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/jasperReportSecuritys/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<JasperReportSecurity> searchJasperReportSecuritys(@PathVariable String query) {
        return StreamSupport
            .stream(jasperReportSecuritySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
