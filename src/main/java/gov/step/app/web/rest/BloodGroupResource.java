package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.BloodGroup;
import gov.step.app.repository.BloodGroupRepository;
import gov.step.app.repository.search.BloodGroupSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BloodGroup.
 */
@RestController
@RequestMapping("/api")
public class BloodGroupResource {

    private final Logger log = LoggerFactory.getLogger(BloodGroupResource.class);

    @Inject
    private BloodGroupRepository bloodGroupRepository;

    @Inject
    private BloodGroupSearchRepository bloodGroupSearchRepository;

    /**
     * POST  /bloodGroups -> Create a new bloodGroup.
     */
    @RequestMapping(value = "/bloodGroups",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BloodGroup> createBloodGroup(@RequestBody BloodGroup bloodGroup) throws URISyntaxException {
        log.debug("REST request to save BloodGroup : {}", bloodGroup);
        if (bloodGroup.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new bloodGroup cannot already have an ID").body(null);
        }
        BloodGroup result = bloodGroupRepository.save(bloodGroup);
        bloodGroupSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/bloodGroups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("bloodGroup", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bloodGroups -> Updates an existing bloodGroup.
     */
    @RequestMapping(value = "/bloodGroups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BloodGroup> updateBloodGroup(@RequestBody BloodGroup bloodGroup) throws URISyntaxException {
        log.debug("REST request to update BloodGroup : {}", bloodGroup);
        if (bloodGroup.getId() == null) {
            return createBloodGroup(bloodGroup);
        }
        BloodGroup result = bloodGroupRepository.save(bloodGroup);
        bloodGroupSearchRepository.save(bloodGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("bloodGroup", bloodGroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bloodGroups -> get all the bloodGroups.
     */
    @RequestMapping(value = "/bloodGroups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<BloodGroup>> getAllBloodGroups(Pageable pageable)
        throws URISyntaxException {
        Page<BloodGroup> page = bloodGroupRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bloodGroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bloodGroups/:id -> get the "id" bloodGroup.
     */
    @RequestMapping(value = "/bloodGroups/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BloodGroup> getBloodGroup(@PathVariable Long id) {
        log.debug("REST request to get BloodGroup : {}", id);
        return Optional.ofNullable(bloodGroupRepository.findOne(id))
            .map(bloodGroup -> new ResponseEntity<>(
                bloodGroup,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /bloodGroups/:id -> delete the "id" bloodGroup.
     */
    @RequestMapping(value = "/bloodGroups/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBloodGroup(@PathVariable Long id) {
        log.debug("REST request to delete BloodGroup : {}", id);
        bloodGroupRepository.delete(id);
        bloodGroupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("bloodGroup", id.toString())).build();
    }

    /**
     * SEARCH  /_search/bloodGroups/:query -> search for the bloodGroup corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/bloodGroups/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<BloodGroup> searchBloodGroups(@PathVariable String query) {
        return StreamSupport
            .stream(bloodGroupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
