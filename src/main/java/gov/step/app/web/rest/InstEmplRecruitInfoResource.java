package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.config.JHipsterProperties;
import gov.step.app.domain.InstEmplRecruitInfo;
import gov.step.app.domain.InstEmployee;
import gov.step.app.domain.PayScale;
import gov.step.app.repository.InstEmplRecruitInfoRepository;
import gov.step.app.repository.InstEmployeeRepository;
import gov.step.app.repository.UserRepository;
import gov.step.app.repository.search.InstEmplRecruitInfoSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.constnt.HRMManagementConstant;
import gov.step.app.web.rest.util.AttachmentUtil;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstEmplRecruitInfo.
 */
@RestController
@RequestMapping("/api")
public class InstEmplRecruitInfoResource {

    private final Logger log = LoggerFactory.getLogger(InstEmplRecruitInfoResource.class);

    @Inject
    private InstEmplRecruitInfoRepository instEmplRecruitInfoRepository;

//    @Inject
//    private InstEmplRecruitInfoSearchRepository instEmplRecruitInfoSearchRepository;

    @Inject
    private InstEmployeeRepository instEmployeeRepository;

    @Inject
    private UserRepository userRepository;
    /**
     * POST  /instEmplRecruitInfos -> Create a new instEmplRecruitInfo.
     */
    @RequestMapping(value = "/instEmplRecruitInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmplRecruitInfo> createInstEmplRecruitInfo(@Valid @RequestBody InstEmplRecruitInfo instEmplRecruitInfo) throws URISyntaxException,Exception {
        log.debug("REST request to save InstEmplRecruitInfo : {}", instEmplRecruitInfo);
        if (instEmplRecruitInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new instEmplRecruitInfo cannot already have an ID").body(null);
        }
        String userName = SecurityUtils.getCurrentUserLogin();
        InstEmployee instEmployeeresult=instEmployeeRepository.findOneByEmployeeCode(userName);
        instEmplRecruitInfo.setInstEmployee(instEmployeeresult);

        String filepath = HRMManagementConstant.INST_EMP_FILE_DIR;
        /*###### Start image post #####*/
        if (instEmplRecruitInfo.getDgApproveFileName() != null && instEmplRecruitInfo.getDgApproveFileName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setDgApproveFileName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getDgApproveFileName().replace("/", "_"), instEmplRecruitInfo.getDgApproveFile()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setDgApproveFile(null);

        if (instEmplRecruitInfo.getPblcCircularName() != null && instEmplRecruitInfo.getPblcCircularName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setPblcCircularName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getPblcCircularName().replace("/", "_"), instEmplRecruitInfo.getPblcCircular()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setPblcCircular(null);

        if (instEmplRecruitInfo.getMcMemoAttachName() != null && instEmplRecruitInfo.getMcMemoAttachName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setMcMemoAttachName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getMcMemoAttachName().replace("/", "_"), instEmplRecruitInfo.getMcMemoAttach()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setMcMemoAttach(null);

        if (instEmplRecruitInfo.getSelectionCopyName() != null && instEmplRecruitInfo.getSelectionCopyName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setSelectionCopyName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getSelectionCopyName().replace("/", "_"), instEmplRecruitInfo.getSelectionCopy()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setSelectionCopy(null);

        if (instEmplRecruitInfo.getConfirmLetterName() != null && instEmplRecruitInfo.getConfirmLetterName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setConfirmLetterName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getConfirmLetterName().replace("/", "_"), instEmplRecruitInfo.getConfirmLetter()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setConfirmLetter(null);

        if (instEmplRecruitInfo.getNtrcaCertName() != null && instEmplRecruitInfo.getNtrcaCertName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setNtrcaCertName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getNtrcaCertName().replace("/", "_"), instEmplRecruitInfo.getNtrcaCert()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setNtrcaCert(null);



        if (instEmplRecruitInfo.getRequiLetterName() != null && instEmplRecruitInfo.getRequiLetterName().length() > 0) {
            log.debug(" image replace trigger----------------------------------------------");
            try {
                instEmplRecruitInfo.setRequiLetterName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRequiLetterName().replace("/", "_"), instEmplRecruitInfo.getRequiLetter()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        instEmplRecruitInfo.setRequiLetter(null);
        /*###### Start image post End #####*/

        /*-------------------saving AppoinmentLtr to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getAppoinmentLtr());
        log.debug("file image test--"+instEmplRecruitInfo.getAppoinmentLtrName());
        if(instEmplRecruitInfo.getAppoinmentLtr() !=null && instEmplRecruitInfo.getAppoinmentLtrName() !=null){
            instEmplRecruitInfo.setAppoinmentLtrName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getAppoinmentLtrName().replace("/", "_"), instEmplRecruitInfo.getAppoinmentLtr()));
        }
        instEmplRecruitInfo.setAppoinmentLtr(null);

        /*-------------------saving JoiningLetter to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getJoiningLetter());
        log.debug("file image test--"+instEmplRecruitInfo.getJoiningLetterName());
        if(instEmplRecruitInfo.getJoiningLetter() !=null && instEmplRecruitInfo.getJoiningLetterName() !=null){
            instEmplRecruitInfo.setJoiningLetterName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getJoiningLetterName().replace("/", "_"), instEmplRecruitInfo.getJoiningLetter()));
        }
        instEmplRecruitInfo.setJoiningLetter(null);


        /*-------------------saving RecruitResult to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getRecruitResult());
        log.debug("file image test--"+instEmplRecruitInfo.getRecruitResultName());
        if(instEmplRecruitInfo.getRecruitResult() !=null && instEmplRecruitInfo.getRecruitResultName() !=null){
            instEmplRecruitInfo.setRecruitResultName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecruitResultName().replace("/", "_"), instEmplRecruitInfo.getRecruitResult()));
        }
        instEmplRecruitInfo.setRecruitResult(null);


        /*-------------------saving RecruitNewsLocal to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getRecruitNewsLocal());
        log.debug("file image test--"+instEmplRecruitInfo.getRecruitNewsLocalName());
        if(instEmplRecruitInfo.getRecruitNewsLocal() !=null && instEmplRecruitInfo.getRecruitNewsLocalName() !=null){
            instEmplRecruitInfo.setRecruitNewsLocalName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecruitNewsLocalName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsLocal()));
        }
        instEmplRecruitInfo.setRecruitNewsLocal(null);

        /*-------------------saving RecruitNewsDaily to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getRecruitNewsDaily());
        log.debug("file image test--"+instEmplRecruitInfo.getRecruitNewsDailyName());
        if(instEmplRecruitInfo.getRecruitNewsDaily() !=null && instEmplRecruitInfo.getRecruitNewsDailyName() !=null){
            instEmplRecruitInfo.setRecruitNewsDailyName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecruitNewsDailyName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsDaily()));
        }
        instEmplRecruitInfo.setRecruitNewsDaily(null);


        /*-------------------saving GoResulaton to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getGoResulaton());
        log.debug("file image test--"+instEmplRecruitInfo.getGoResulatonName());
        if(instEmplRecruitInfo.getGoResulaton() !=null && instEmplRecruitInfo.getGoResulatonName() !=null){
            instEmplRecruitInfo.setGoResulatonName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getGoResulatonName().replace("/", "_"), instEmplRecruitInfo.getGoResulaton()));
        }
        instEmplRecruitInfo.setGoResulaton(null);

        /*-------------------saving Committee to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getCommittee());
        log.debug("file image test--"+instEmplRecruitInfo.getCommitteeName());
        if(instEmplRecruitInfo.getCommittee() !=null && instEmplRecruitInfo.getCommitteeName() !=null){
            instEmplRecruitInfo.setCommitteeName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getCommitteeName().replace("/", "_"), instEmplRecruitInfo.getCommittee()));
        }
        instEmplRecruitInfo.setCommittee(null);

        /*-------------------saving Recommendation to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getRecommendation());
        log.debug("file image test--"+instEmplRecruitInfo.getRecommendationName());
        if(instEmplRecruitInfo.getRecommendation() !=null && instEmplRecruitInfo.getRecommendationName() !=null){
            instEmplRecruitInfo.setRecommendationName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecommendationName().replace("/", "_"), instEmplRecruitInfo.getRecommendation()));
        }
        instEmplRecruitInfo.setRecommendation(null);
        if(instEmplRecruitInfo.getAppointAprvAtch() !=null && instEmplRecruitInfo.getAppointAprvAtchName() !=null){
            //instEmplRecruitInfo.setRecruitNewsLocalName(AttachmentUtil.saveAttachment(filepath, instEmplRecruitInfo.getRecruitNewsLocalName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsLocal()));

                instEmplRecruitInfo.setAppointAprvAtchName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getAppointAprvAtchName().replace("/", "_"), instEmplRecruitInfo.getAppointAprvAtch()));
        }
        instEmplRecruitInfo.setAppointAprvAtch(null);

        /*-------------------saving Sanction to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getSanction());
        log.debug("file image test--"+instEmplRecruitInfo.getSanctionName());
        if(instEmplRecruitInfo.getSanction() !=null && instEmplRecruitInfo.getSanctionName() !=null){
            instEmplRecruitInfo.setSanctionName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getSanctionName().replace("/", "_"), instEmplRecruitInfo.getSanction()));
        }
        instEmplRecruitInfo.setSanction(null);



        InstEmplRecruitInfo result = instEmplRecruitInfoRepository.save(instEmplRecruitInfo);
//        instEmplRecruitInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/instEmplRecruitInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("instEmplRecruitInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /instEmplRecruitInfos -> Updates an existing instEmplRecruitInfo.
     */
    @RequestMapping(value = "/instEmplRecruitInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmplRecruitInfo> updateInstEmplRecruitInfo(@Valid @RequestBody InstEmplRecruitInfo instEmplRecruitInfo) throws URISyntaxException, Exception {
        log.debug("REST request to update InstEmplRecruitInfo : {}", instEmplRecruitInfo);
        if (instEmplRecruitInfo.getId() == null) {
            return createInstEmplRecruitInfo(instEmplRecruitInfo);
        }
        String filepath = HRMManagementConstant.INST_EMP_FILE_DIR;

        InstEmplRecruitInfo prevInstEmplRecruitInfo = instEmplRecruitInfoRepository.findOne(instEmplRecruitInfo.getId());


        /*-------------------saving AppoinmentLtr to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getAppoinmentLtr());
        log.debug("file image test--"+instEmplRecruitInfo.getAppoinmentLtrName());
        //    instEmplRecruitInfo.setAppoinmentLtrName(AttachmentUtil.saveAttachment(filepath, instEmplRecruitInfo.getAppoinmentLtrName().replace("/", "_"), instEmplRecruitInfo.getAppoinmentLtr()));
            if(instEmplRecruitInfo.getAppoinmentLtr() !=null && instEmplRecruitInfo.getAppoinmentLtrName() !=null){
                if(prevInstEmplRecruitInfo.getAppoinmentLtrName() !=null && prevInstEmplRecruitInfo.getAppoinmentLtrName().length()>0){
                    log.debug(" image replace trigger----------------------------------------------");
                    instEmplRecruitInfo.setAppoinmentLtrName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getAppoinmentLtrName(),instEmplRecruitInfo.getAppoinmentLtrName().replace("/", "_"), instEmplRecruitInfo.getAppoinmentLtr()));
                } else {
                    instEmplRecruitInfo.setAppoinmentLtrName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getAppoinmentLtrName().replace("/", "_"), instEmplRecruitInfo.getAppoinmentLtr()));
                }
            }
            instEmplRecruitInfo.setAppoinmentLtr(null);

        /*-------------------saving JoiningLetter to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getJoiningLetter());
        log.debug("file image test--"+instEmplRecruitInfo.getJoiningLetterName());

        if(instEmplRecruitInfo.getJoiningLetter() !=null && instEmplRecruitInfo.getJoiningLetterName() !=null) {
            //instEmplRecruitInfo.setJoiningLetterName(AttachmentUtil.saveAttachment(filepath, instEmplRecruitInfo.getJoiningLetterName().replace("/", "_"), instEmplRecruitInfo.getJoiningLetter()));
            if (prevInstEmplRecruitInfo.getJoiningLetterName() != null && prevInstEmplRecruitInfo.getJoiningLetterName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setJoiningLetterName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getJoiningLetterName(), instEmplRecruitInfo.getJoiningLetterName().replace("/", "_"), instEmplRecruitInfo.getJoiningLetter()));
            } else {
                instEmplRecruitInfo.setJoiningLetterName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getJoiningLetterName().replace("/", "_"), instEmplRecruitInfo.getJoiningLetter()));
            }
        }
        instEmplRecruitInfo.setJoiningLetter(null);


        /*-------------------saving RecruitResult to local disk------ */
        log.debug("file name test--"+instEmplRecruitInfo.getRecruitResult());
        log.debug("file image test--"+instEmplRecruitInfo.getRecruitResultName());
            //instEmplRecruitInfo.setRecruitResultName(AttachmentUtil.saveAttachment(filepath, instEmplRecruitInfo.getRecruitResultName().replace("/", "_"), instEmplRecruitInfo.getRecruitResult()));

        if(instEmplRecruitInfo.getRecruitResult() !=null && instEmplRecruitInfo.getRecruitResultName()!=null){
            if(prevInstEmplRecruitInfo.getRecruitResultName() !=null && prevInstEmplRecruitInfo.getRecruitResultName().length()>0){
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setRecruitResultName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getRecruitResultName(),instEmplRecruitInfo.getRecruitResultName().replace("/", "_"), instEmplRecruitInfo.getRecruitResult()));
            } else {
                instEmplRecruitInfo.setRecruitResultName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecruitResultName().replace("/", "_"), instEmplRecruitInfo.getRecruitResult()));
            }
        }
        instEmplRecruitInfo.setRecruitResult(null);

        if(instEmplRecruitInfo.getAppointAprvAtch() != null && instEmplRecruitInfo.getAppointAprvAtchName()!= null) {
            //instEmplRecruitInfo.setRecruitNewsLocalName(AttachmentUtil.saveAttachment(filepath, instEmplRecruitInfo.getRecruitNewsLocalName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsLocal()));
            if (prevInstEmplRecruitInfo.getAppointAprvAtchName() != null && prevInstEmplRecruitInfo.getAppointAprvAtchName().length() > 0) {
                instEmplRecruitInfo.setAppointAprvAtchName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getAppointAprvAtchName(), instEmplRecruitInfo.getAppointAprvAtchName().replace("/", "_"), instEmplRecruitInfo.getAppointAprvAtch()));
            } else {
                instEmplRecruitInfo.setAppointAprvAtchName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getAppointAprvAtchName().replace("/", "_"), instEmplRecruitInfo.getAppointAprvAtch()));
            }
        }
        instEmplRecruitInfo.setAppointAprvAtch(null);


            //instEmplRecruitInfo.setRecruitNewsLocalName(AttachmentUtil.saveAttachment(filepath, instEmplRecruitInfo.getRecruitNewsLocalName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsLocal()));
           if(instEmplRecruitInfo.getRecruitNewsLocal() !=null && instEmplRecruitInfo.getRecruitNewsLocalName() !=null){
               if(prevInstEmplRecruitInfo.getRecruitNewsLocalName() !=null && prevInstEmplRecruitInfo.getRecruitNewsLocalName().length()>0){
                   log.debug(" image replace trigger----------------------------------------------");
                   instEmplRecruitInfo.setRecruitNewsLocalName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getRecruitNewsLocalName(),instEmplRecruitInfo.getRecruitNewsLocalName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsLocal()));
               } else {
                   instEmplRecruitInfo.setRecruitNewsLocalName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecruitNewsLocalName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsLocal()));
               }
           }
           instEmplRecruitInfo.setRecruitNewsLocal(null);

        if(instEmplRecruitInfo.getMcMemoAttach() !=null && instEmplRecruitInfo.getMcMemoAttachName() !=null) {
            if (prevInstEmplRecruitInfo.getMcMemoAttachName() != null && prevInstEmplRecruitInfo.getMcMemoAttachName().length() > 0) {
                instEmplRecruitInfo.setMcMemoAttachName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getMcMemoAttachName(), instEmplRecruitInfo.getMcMemoAttachName().replace("/", "_"), instEmplRecruitInfo.getMcMemoAttach()));
            } else {
                instEmplRecruitInfo.setMcMemoAttachName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getMcMemoAttachName().replace("/", "_"), instEmplRecruitInfo.getMcMemoAttach()));
            }
        }
        instEmplRecruitInfo.setRecruitNewsLocal(null);
        if(instEmplRecruitInfo.getRecruitNewsDaily() !=null && instEmplRecruitInfo.getRecruitNewsDailyName() !=null) {
            if (prevInstEmplRecruitInfo.getRecruitNewsDailyName() != null && prevInstEmplRecruitInfo.getRecruitNewsDailyName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setRecruitNewsDailyName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getRecruitNewsDailyName(), instEmplRecruitInfo.getRecruitNewsDailyName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsDaily()));
            } else {
                instEmplRecruitInfo.setRecruitNewsDailyName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecruitNewsDailyName().replace("/", "_"), instEmplRecruitInfo.getRecruitNewsDaily()));
            }
        }
        instEmplRecruitInfo.setRecruitNewsDaily(null);
        if(instEmplRecruitInfo.getGoResulaton() !=null && instEmplRecruitInfo.getGoResulatonName() !=null) {
            if (prevInstEmplRecruitInfo.getGoResulatonName() != null && prevInstEmplRecruitInfo.getGoResulatonName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setGoResulatonName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getGoResulatonName(), instEmplRecruitInfo.getGoResulatonName().replace("/", "_"), instEmplRecruitInfo.getGoResulaton()));
            } else {
                instEmplRecruitInfo.setGoResulatonName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getGoResulatonName().replace("/", "_"), instEmplRecruitInfo.getGoResulaton()));
            }
        }
        instEmplRecruitInfo.setGoResulaton(null);
        if(instEmplRecruitInfo.getCommittee() !=null && instEmplRecruitInfo.getCommitteeName() !=null) {
            if (prevInstEmplRecruitInfo.getCommitteeName() != null && prevInstEmplRecruitInfo.getCommitteeName().length() > 0) {
                instEmplRecruitInfo.setCommitteeName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getCommitteeName(), instEmplRecruitInfo.getCommitteeName().replace("/", "_"), instEmplRecruitInfo.getCommittee()));
            } else {
                instEmplRecruitInfo.setCommitteeName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getCommitteeName().replace("/", "_"), instEmplRecruitInfo.getCommittee()));
            }
        }
        instEmplRecruitInfo.setCommittee(null);

        if(instEmplRecruitInfo.getRecommendation() !=null && instEmplRecruitInfo.getRecommendationName() !=null) {
            if (prevInstEmplRecruitInfo.getRecommendationName() != null && prevInstEmplRecruitInfo.getRecommendationName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setRecommendationName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getRecommendationName(), instEmplRecruitInfo.getRecommendationName().replace("/", "_"), instEmplRecruitInfo.getRecommendation()));
            } else {
                instEmplRecruitInfo.setRecommendationName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRecommendationName().replace("/", "_"), instEmplRecruitInfo.getRecommendation()));
            }
        }
        instEmplRecruitInfo.setRecommendation(null);
        if(instEmplRecruitInfo.getSanction() !=null && instEmplRecruitInfo.getSanctionName() !=null) {
            if (prevInstEmplRecruitInfo.getSanctionName() != null && prevInstEmplRecruitInfo.getSanctionName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setSanctionName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getSanctionName(), instEmplRecruitInfo.getSanctionName().replace("/", "_"), instEmplRecruitInfo.getSanction()));
            } else {
                instEmplRecruitInfo.setSanctionName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getSanctionName().replace("/", "_"), instEmplRecruitInfo.getSanction()));
            }
        }
        instEmplRecruitInfo.setSanction(null);
        if(instEmplRecruitInfo.getNtrcaCert() !=null && instEmplRecruitInfo.getNtrcaCertName() !=null) {
            if (prevInstEmplRecruitInfo.getNtrcaCertName() != null && prevInstEmplRecruitInfo.getNtrcaCertName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setNtrcaCertName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getNtrcaCertName(), instEmplRecruitInfo.getNtrcaCertName().replace("/", "_"), instEmplRecruitInfo.getNtrcaCert()));
            } else {
                instEmplRecruitInfo.setNtrcaCertName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getNtrcaCertName().replace("/", "_"), instEmplRecruitInfo.getNtrcaCert()));
            }
        }
        instEmplRecruitInfo.setNtrcaCert(null);
        if(instEmplRecruitInfo.getPblcCircular() !=null && instEmplRecruitInfo.getPblcCircularName() !=null) {
            if (prevInstEmplRecruitInfo.getPblcCircularName() != null && prevInstEmplRecruitInfo.getPblcCircularName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setPblcCircularName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getPblcCircularName(), instEmplRecruitInfo.getPblcCircularName().replace("/", "_"), instEmplRecruitInfo.getPblcCircular()));
            } else {
                instEmplRecruitInfo.setPblcCircularName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getPblcCircularName().replace("/", "_"), instEmplRecruitInfo.getPblcCircular()));
            }
        }
        instEmplRecruitInfo.setPblcCircular(null);

        if(instEmplRecruitInfo.getRequiLetter() !=null && instEmplRecruitInfo.getRequiLetterName() !=null) {
            if (prevInstEmplRecruitInfo.getRequiLetterName() != null && prevInstEmplRecruitInfo.getRequiLetterName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setRequiLetterName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getRequiLetterName(), instEmplRecruitInfo.getRequiLetterName().replace("/", "_"), instEmplRecruitInfo.getRequiLetter()));
            } else {
                instEmplRecruitInfo.setRequiLetterName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getRequiLetterName().replace("/", "_"), instEmplRecruitInfo.getRequiLetter()));
            }
        }
        instEmplRecruitInfo.setRequiLetter(null);

        if(instEmplRecruitInfo.getConfirmLetter() !=null && instEmplRecruitInfo.getConfirmLetterName() !=null) {
            if (prevInstEmplRecruitInfo.getConfirmLetterName() != null && prevInstEmplRecruitInfo.getConfirmLetterName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setConfirmLetterName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getConfirmLetterName(), instEmplRecruitInfo.getConfirmLetterName().replace("/", "_"), instEmplRecruitInfo.getConfirmLetter()));
            }else {
                instEmplRecruitInfo.setConfirmLetterName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getConfirmLetterName().replace("/", "_"), instEmplRecruitInfo.getConfirmLetter()));
            }
        }
        instEmplRecruitInfo.setConfirmLetter(null);

        if(instEmplRecruitInfo.getSelectionCopy() !=null && instEmplRecruitInfo.getSelectionCopyName() !=null) {
            if (prevInstEmplRecruitInfo.getSelectionCopyName() != null && prevInstEmplRecruitInfo.getSelectionCopyName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setSelectionCopyName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getSelectionCopyName(), instEmplRecruitInfo.getSelectionCopyName().replace("/", "_"), instEmplRecruitInfo.getSelectionCopy()));
            } else {
                instEmplRecruitInfo.setSelectionCopyName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getSelectionCopyName().replace("/", "_"), instEmplRecruitInfo.getSelectionCopy()));
            }
        }
        instEmplRecruitInfo.setSelectionCopy(null);

        if(instEmplRecruitInfo.getDgApproveFile() !=null && instEmplRecruitInfo.getDgApproveFileName() !=null) {
            if (prevInstEmplRecruitInfo.getDgApproveFileName() != null && prevInstEmplRecruitInfo.getDgApproveFileName().length() > 0) {
                log.debug(" image replace trigger----------------------------------------------");
                instEmplRecruitInfo.setDgApproveFileName(AttachmentUtil.replaceAttachment(filepath, prevInstEmplRecruitInfo.getDgApproveFileName(), instEmplRecruitInfo.getDgApproveFileName().replace("/", "_"), instEmplRecruitInfo.getDgApproveFile()));
            } else {
                instEmplRecruitInfo.setDgApproveFileName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmplRecruitInfo.getDgApproveFileName().replace("/", "_"), instEmplRecruitInfo.getDgApproveFile()));
            }
        }
        instEmplRecruitInfo.setDgApproveFile(null);


        InstEmplRecruitInfo result = instEmplRecruitInfoRepository.save(instEmplRecruitInfo);
//        instEmplRecruitInfoSearchRepository.save(instEmplRecruitInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("instEmplRecruitInfo", instEmplRecruitInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /instEmplRecruitInfos -> get all the instEmplRecruitInfos.
     */
    @RequestMapping(value = "/instEmplRecruitInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmplRecruitInfo>> getAllInstEmplRecruitInfos(Pageable pageable)
        throws URISyntaxException {
        Page<InstEmplRecruitInfo> page = instEmplRecruitInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instEmplRecruitInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /instEmplRecruitInfos/:id -> get the "id" instEmplRecruitInfo.
     */
    @RequestMapping(value = "/instEmplRecruitInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmplRecruitInfo> getInstEmplRecruitInfo(@PathVariable Long id) {
        log.debug("REST request to get InstEmplRecruitInfo : {}", id);
        return Optional.ofNullable(instEmplRecruitInfoRepository.findOne(id))
            .map(instEmplRecruitInfo -> new ResponseEntity<>(
                instEmplRecruitInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /instEmplRecruitInfos/:id -> get the "id" instEmplRecruitInfo.
     */
    @RequestMapping(value = "/instEmplRecruitInfos/instEmployee/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmplRecruitInfo> getInstEmplRecruitInfoByEmployee(@PathVariable Long id) throws Exception {
        log.debug("REST request to get InstEmplRecruitInfo : {}", id);
        String filepath = HRMManagementConstant.INST_EMP_FILE_DIR;
        InstEmplRecruitInfo instEmplRecruitInfoResult= instEmplRecruitInfoRepository.findByInstEmployeeId(id);

        if(instEmplRecruitInfoResult !=null){
            if(instEmplRecruitInfoResult.getAppoinmentLtrName() !=null && instEmplRecruitInfoResult.getAppoinmentLtrName().length()>0){
                instEmplRecruitInfoResult.setAppoinmentLtr(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppoinmentLtrName()));
                instEmplRecruitInfoResult.setAppoinmentLtrName(instEmplRecruitInfoResult.getAppoinmentLtrName().substring(0, (instEmplRecruitInfoResult.getAppoinmentLtrName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getJoiningLetterName() !=null && instEmplRecruitInfoResult.getJoiningLetterName().length()>0){
                instEmplRecruitInfoResult.setJoiningLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getJoiningLetterName()));
                instEmplRecruitInfoResult.setJoiningLetterName(instEmplRecruitInfoResult.getJoiningLetterName().substring(0, (instEmplRecruitInfoResult.getJoiningLetterName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecruitResultName() !=null && instEmplRecruitInfoResult.getRecruitResultName().length()>0){
                instEmplRecruitInfoResult.setRecruitResult(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitResultName()));
                instEmplRecruitInfoResult.setRecruitResultName(instEmplRecruitInfoResult.getRecruitResultName().substring(0, (instEmplRecruitInfoResult.getRecruitResultName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecruitNewsLocalName() !=null && instEmplRecruitInfoResult.getRecruitNewsLocalName().length()>0){
                instEmplRecruitInfoResult.setRecruitNewsLocal(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsLocalName()));
                instEmplRecruitInfoResult.setRecruitNewsLocalName(instEmplRecruitInfoResult.getRecruitNewsLocalName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsLocalName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecruitNewsDailyName() !=null && instEmplRecruitInfoResult.getRecruitNewsDailyName().length()>0){
                instEmplRecruitInfoResult.setRecruitNewsDaily(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsDailyName()));
                instEmplRecruitInfoResult.setRecruitNewsDailyName(instEmplRecruitInfoResult.getRecruitNewsDailyName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsDailyName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getGoResulatonName() !=null && instEmplRecruitInfoResult.getGoResulatonName().length()>0){
                instEmplRecruitInfoResult.setGoResulaton(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getGoResulatonName()));
                instEmplRecruitInfoResult.setGoResulatonName(instEmplRecruitInfoResult.getGoResulatonName().substring(0, (instEmplRecruitInfoResult.getGoResulatonName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getCommitteeName() !=null && instEmplRecruitInfoResult.getCommitteeName().length()>0){
                instEmplRecruitInfoResult.setCommittee(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getCommitteeName()));
                instEmplRecruitInfoResult.setCommitteeName(instEmplRecruitInfoResult.getCommitteeName().substring(0, (instEmplRecruitInfoResult.getCommitteeName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecommendationName() !=null && instEmplRecruitInfoResult.getRecommendationName().length()>0){
                instEmplRecruitInfoResult.setRecommendation(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecommendationName()));
                instEmplRecruitInfoResult.setRecommendationName(instEmplRecruitInfoResult.getRecommendationName().substring(0, (instEmplRecruitInfoResult.getRecommendationName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getSanctionName() !=null && instEmplRecruitInfoResult.getSanctionName().length()>0){
                instEmplRecruitInfoResult.setSanction(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSanctionName()));
                instEmplRecruitInfoResult.setSanctionName(instEmplRecruitInfoResult.getSanctionName().substring(0, (instEmplRecruitInfoResult.getSanctionName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getDgApproveFileName() !=null && instEmplRecruitInfoResult.getDgApproveFileName().length()>0){
                instEmplRecruitInfoResult.setDgApproveFile(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getDgApproveFileName()));
                instEmplRecruitInfoResult.setDgApproveFileName(instEmplRecruitInfoResult.getDgApproveFileName().substring(0, (instEmplRecruitInfoResult.getDgApproveFileName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getPblcCircularName() !=null && instEmplRecruitInfoResult.getPblcCircularName().length()>0){
                instEmplRecruitInfoResult.setPblcCircular(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getPblcCircularName()));
                instEmplRecruitInfoResult.setPblcCircularName(instEmplRecruitInfoResult.getPblcCircularName().substring(0, (instEmplRecruitInfoResult.getPblcCircularName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRequiLetterName() !=null && instEmplRecruitInfoResult.getRequiLetterName().length()>0){
                instEmplRecruitInfoResult.setRequiLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRequiLetterName()));
                instEmplRecruitInfoResult.setRequiLetterName(instEmplRecruitInfoResult.getRequiLetterName().substring(0, (instEmplRecruitInfoResult.getRequiLetterName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getSelectionCopyName() !=null && instEmplRecruitInfoResult.getSelectionCopyName().length()>0){
                instEmplRecruitInfoResult.setSelectionCopy(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSelectionCopyName()));
                instEmplRecruitInfoResult.setSelectionCopyName(instEmplRecruitInfoResult.getSelectionCopyName().substring(0, (instEmplRecruitInfoResult.getSelectionCopyName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getConfirmLetterName() !=null && instEmplRecruitInfoResult.getConfirmLetterName().length()>0){
                instEmplRecruitInfoResult.setConfirmLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getConfirmLetterName()));
                instEmplRecruitInfoResult.setConfirmLetterName(instEmplRecruitInfoResult.getConfirmLetterName().substring(0, (instEmplRecruitInfoResult.getConfirmLetterName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getNtrcaCertName() !=null && instEmplRecruitInfoResult.getNtrcaCertName().length()>0){
                instEmplRecruitInfoResult.setNtrcaCert(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getNtrcaCertName()));
                instEmplRecruitInfoResult.setNtrcaCertName(instEmplRecruitInfoResult.getNtrcaCertName().substring(0, (instEmplRecruitInfoResult.getNtrcaCertName().length() - 4)));
            }


            if(instEmplRecruitInfoResult.getMcMemoAttachName() !=null && instEmplRecruitInfoResult.getMcMemoAttachName().length()>0){
                instEmplRecruitInfoResult.setMcMemoAttach(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getMcMemoAttachName()));
                instEmplRecruitInfoResult.setMcMemoAttachName(instEmplRecruitInfoResult.getMcMemoAttachName().substring(0, (instEmplRecruitInfoResult.getMcMemoAttachName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getAppointAprvAtchName() !=null && instEmplRecruitInfoResult.getAppointAprvAtchName().length()>0){
                instEmplRecruitInfoResult.setAppointAprvAtch(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppointAprvAtchName()));
                instEmplRecruitInfoResult.setAppointAprvAtchName(instEmplRecruitInfoResult.getAppointAprvAtchName().substring(0, (instEmplRecruitInfoResult.getAppointAprvAtchName().length() - 4)));
            }
        }

        return Optional.ofNullable(instEmplRecruitInfoResult)
            .map(instEmplRecruitInfo -> new ResponseEntity<>(
                instEmplRecruitInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    /**
     * GET  /instEmplRecruitInfos/:id -> get the "id" instEmplRecruitInfo.
     */
    @RequestMapping(value = "/instEmplRecruitInfosCurrent",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmplRecruitInfo> getInstEmplRecruitInfoCurrent() throws Exception{
        log.debug("REST request to get InstEmplRecruitInfo : {}");
        String filepath = HRMManagementConstant.INST_EMP_FILE_DIR;
        InstEmplRecruitInfo instEmplRecruitInfoResult=instEmplRecruitInfoRepository.findOneByCurrent();

        if(instEmplRecruitInfoResult !=null){
            if(instEmplRecruitInfoResult.getAppoinmentLtrName() !=null && instEmplRecruitInfoResult.getAppoinmentLtrName().length()>0){
                instEmplRecruitInfoResult.setAppoinmentLtr(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppoinmentLtrName()));
                instEmplRecruitInfoResult.setAppoinmentLtrName(instEmplRecruitInfoResult.getAppoinmentLtrName().substring(0, (instEmplRecruitInfoResult.getAppoinmentLtrName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getJoiningLetterName() !=null && instEmplRecruitInfoResult.getJoiningLetterName().length()>0){
                instEmplRecruitInfoResult.setJoiningLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getJoiningLetterName()));
                instEmplRecruitInfoResult.setJoiningLetterName(instEmplRecruitInfoResult.getJoiningLetterName().substring(0, (instEmplRecruitInfoResult.getJoiningLetterName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecruitResultName() !=null && instEmplRecruitInfoResult.getRecruitResultName().length()>0){
                instEmplRecruitInfoResult.setRecruitResult(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitResultName()));
                instEmplRecruitInfoResult.setRecruitResultName(instEmplRecruitInfoResult.getRecruitResultName().substring(0, (instEmplRecruitInfoResult.getRecruitResultName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecruitNewsLocalName() !=null && instEmplRecruitInfoResult.getRecruitNewsLocalName().length()>0){
                instEmplRecruitInfoResult.setRecruitNewsLocal(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsLocalName()));
                instEmplRecruitInfoResult.setRecruitNewsLocalName(instEmplRecruitInfoResult.getRecruitNewsLocalName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsLocalName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecruitNewsDailyName() !=null && instEmplRecruitInfoResult.getRecruitNewsDailyName().length()>0){
                instEmplRecruitInfoResult.setRecruitNewsDaily(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsDailyName()));
                instEmplRecruitInfoResult.setRecruitNewsDailyName(instEmplRecruitInfoResult.getRecruitNewsDailyName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsDailyName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getGoResulatonName() !=null && instEmplRecruitInfoResult.getGoResulatonName().length()>0){
                instEmplRecruitInfoResult.setGoResulaton(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getGoResulatonName()));
                instEmplRecruitInfoResult.setGoResulatonName(instEmplRecruitInfoResult.getGoResulatonName().substring(0, (instEmplRecruitInfoResult.getGoResulatonName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getCommitteeName() !=null && instEmplRecruitInfoResult.getCommitteeName().length()>0){
                instEmplRecruitInfoResult.setCommittee(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getCommitteeName()));
                instEmplRecruitInfoResult.setCommitteeName(instEmplRecruitInfoResult.getCommitteeName().substring(0, (instEmplRecruitInfoResult.getCommitteeName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRecommendationName() !=null && instEmplRecruitInfoResult.getRecommendationName().length()>0){
                instEmplRecruitInfoResult.setRecommendation(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecommendationName()));
                instEmplRecruitInfoResult.setRecommendationName(instEmplRecruitInfoResult.getRecommendationName().substring(0, (instEmplRecruitInfoResult.getRecommendationName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getSanctionName() !=null && instEmplRecruitInfoResult.getSanctionName().length()>0){
                instEmplRecruitInfoResult.setSanction(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSanctionName()));
                instEmplRecruitInfoResult.setSanctionName(instEmplRecruitInfoResult.getSanctionName().substring(0, (instEmplRecruitInfoResult.getSanctionName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getDgApproveFileName() !=null && instEmplRecruitInfoResult.getDgApproveFileName().length()>0){
                instEmplRecruitInfoResult.setDgApproveFile(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getDgApproveFileName()));
                instEmplRecruitInfoResult.setDgApproveFileName(instEmplRecruitInfoResult.getDgApproveFileName().substring(0, (instEmplRecruitInfoResult.getDgApproveFileName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getPblcCircularName() !=null && instEmplRecruitInfoResult.getPblcCircularName().length()>0){
                instEmplRecruitInfoResult.setPblcCircular(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getPblcCircularName()));
                instEmplRecruitInfoResult.setPblcCircularName(instEmplRecruitInfoResult.getPblcCircularName().substring(0, (instEmplRecruitInfoResult.getPblcCircularName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getRequiLetterName() !=null && instEmplRecruitInfoResult.getRequiLetterName().length()>0){
                instEmplRecruitInfoResult.setRequiLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRequiLetterName()));
                instEmplRecruitInfoResult.setRequiLetterName(instEmplRecruitInfoResult.getRequiLetterName().substring(0, (instEmplRecruitInfoResult.getRequiLetterName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getSelectionCopyName() !=null && instEmplRecruitInfoResult.getSelectionCopyName().length()>0){
                instEmplRecruitInfoResult.setSelectionCopy(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSelectionCopyName()));
                instEmplRecruitInfoResult.setSelectionCopyName(instEmplRecruitInfoResult.getSelectionCopyName().substring(0, (instEmplRecruitInfoResult.getSelectionCopyName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getConfirmLetterName() !=null && instEmplRecruitInfoResult.getConfirmLetterName().length()>0){
                instEmplRecruitInfoResult.setConfirmLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getConfirmLetterName()));
                instEmplRecruitInfoResult.setConfirmLetterName(instEmplRecruitInfoResult.getConfirmLetterName().substring(0, (instEmplRecruitInfoResult.getConfirmLetterName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getNtrcaCertName() !=null && instEmplRecruitInfoResult.getNtrcaCertName().length()>0){
                instEmplRecruitInfoResult.setNtrcaCert(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getNtrcaCertName()));
                instEmplRecruitInfoResult.setNtrcaCertName(instEmplRecruitInfoResult.getNtrcaCertName().substring(0, (instEmplRecruitInfoResult.getNtrcaCertName().length() - 4)));
            }


            if(instEmplRecruitInfoResult.getMcMemoAttachName() !=null && instEmplRecruitInfoResult.getMcMemoAttachName().length()>0){
                instEmplRecruitInfoResult.setMcMemoAttach(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getMcMemoAttachName()));
                instEmplRecruitInfoResult.setMcMemoAttachName(instEmplRecruitInfoResult.getMcMemoAttachName().substring(0, (instEmplRecruitInfoResult.getMcMemoAttachName().length() - 4)));
            }

            if(instEmplRecruitInfoResult.getAppointAprvAtchName() !=null && instEmplRecruitInfoResult.getAppointAprvAtchName().length()>0){
                instEmplRecruitInfoResult.setAppointAprvAtch(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppointAprvAtchName()));
                instEmplRecruitInfoResult.setAppointAprvAtchName(instEmplRecruitInfoResult.getAppointAprvAtchName().substring(0, (instEmplRecruitInfoResult.getAppointAprvAtchName().length() - 4)));
            }
        }

        return Optional.ofNullable(instEmplRecruitInfoResult)
            .map(instEmplRecruitInfo -> new ResponseEntity<>(
                instEmplRecruitInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }


    @RequestMapping(value = "/instEmplRecruitInfos/updatePayScale/{id}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmplRecruitInfo> UpdatePayScaleByInstEmployee(@PathVariable Long id, @RequestBody PayScale payScale) {

        InstEmplRecruitInfo recruitInfo = instEmplRecruitInfoRepository.findByInstEmployeeId(id);
        recruitInfo.setPayScale(payScale);
        recruitInfo.setUpdateBy(userRepository.findUserByUserLogin(SecurityUtils.getCurrentUserLogin()));
        recruitInfo.setDateModified(LocalDate.now());
        InstEmplRecruitInfo result = instEmplRecruitInfoRepository.save(recruitInfo);

        return Optional.ofNullable(result)
            .map(instEmplRecruitInfo -> new ResponseEntity<>(
                instEmplRecruitInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    /**
     * DELETE  /instEmplRecruitInfos/:id -> delete the "id" instEmplRecruitInfo.
     */
    @RequestMapping(value = "/instEmplRecruitInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstEmplRecruitInfo(@PathVariable Long id) {
        log.debug("REST request to delete InstEmplRecruitInfo : {}", id);
        instEmplRecruitInfoRepository.delete(id);
//        instEmplRecruitInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("instEmplRecruitInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/instEmplRecruitInfos/:query -> search for the instEmplRecruitInfo corresponding
     * to the query.
     */
//    @RequestMapping(value = "/_search/instEmplRecruitInfos/{query}",
//        method = RequestMethod.GET,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public List<InstEmplRecruitInfo> searchInstEmplRecruitInfos(@PathVariable String query) {
//        return StreamSupport
//            .stream(instEmplRecruitInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
//            .collect(Collectors.toList());
//    }
}
