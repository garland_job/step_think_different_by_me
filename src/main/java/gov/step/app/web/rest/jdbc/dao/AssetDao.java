package gov.step.app.web.rest.jdbc.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import oracle.jdbc.OracleConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
/*import gov.step.app.web.rest.dto.AssetDTO;*/
import org.springframework.stereotype.Component;

/**
 * Created by leads on 6/3/17.
 */
@Component
public class AssetDao {

    Connection connection = null;
    private final Logger logger = LoggerFactory.getLogger(AssetDao.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AssetDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void assetRecordQuantityUpdate(Long id,String assetCode,Long quantity) {
        String query = "update ASSET_RECORD set QUANTITY = "+quantity+ " where id= "+id +" and RECORD_CODE = '"+assetCode+"'";
        System.out.println("\n Asset Record Quantity updated" + query);
        int update = this.jdbcTemplate.update(query);
        System.out.println("update :" + update);
    }

    public void assetDistributionQuantityUpdate(Long id,String assetCode,Long quantity) {
        String query = "update ASSET_DISTRIBUTION set APPROVE_QUANTITY = "+quantity+ " where id= "+id +" and ASSET_CODE = '"+assetCode+"'";
        System.out.println("\n Asset Record Quantity updated" + query);
        int update = this.jdbcTemplate.update(query);
        System.out.println("update :" + update);
    }

    public void AssetRequisitionUpdateStatus(Long id) {
        String aprStatus ="4";
        String query = "update ASSET_REQUISITION set REQ_STATUS = "+aprStatus+ " where ID = "+id;
        System.out.println("\n Asset Requisition status update" + query);
        int update = this.jdbcTemplate.update(query);
        System.out.println("update :" + update);
    }
}
