package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.BloodGroupInfo;
import gov.step.app.repository.BloodGroupInfoRepository;
import gov.step.app.repository.search.BloodGroupInfoSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing BloodGroupInfo.
 */
@RestController
@RequestMapping("/api")
public class BloodGroupInfoResource {

    private final Logger log = LoggerFactory.getLogger(BloodGroupInfoResource.class);

    @Inject
    private BloodGroupInfoRepository bloodGroupInfoRepository;

    @Inject
    private BloodGroupInfoSearchRepository bloodGroupInfoSearchRepository;

    /**
     * POST  /bloodGroupInfos -> Create a new bloodGroupInfo.
     */
    @RequestMapping(value = "/bloodGroupInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BloodGroupInfo> createBloodGroupInfo(@RequestBody BloodGroupInfo bloodGroupInfo) throws URISyntaxException {
        log.debug("REST request to save BloodGroupInfo : {}", bloodGroupInfo);
        if (bloodGroupInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new bloodGroupInfo cannot already have an ID").body(null);
        }
        BloodGroupInfo result = bloodGroupInfoRepository.save(bloodGroupInfo);
        bloodGroupInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/bloodGroupInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("bloodGroupInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bloodGroupInfos -> Updates an existing bloodGroupInfo.
     */
    @RequestMapping(value = "/bloodGroupInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BloodGroupInfo> updateBloodGroupInfo(@RequestBody BloodGroupInfo bloodGroupInfo) throws URISyntaxException {
        log.debug("REST request to update BloodGroupInfo : {}", bloodGroupInfo);
        if (bloodGroupInfo.getId() == null) {
            return createBloodGroupInfo(bloodGroupInfo);
        }
        BloodGroupInfo result = bloodGroupInfoRepository.save(bloodGroupInfo);
        bloodGroupInfoSearchRepository.save(bloodGroupInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("bloodGroupInfo", bloodGroupInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bloodGroupInfos -> get all the bloodGroupInfos.
     */
    @RequestMapping(value = "/bloodGroupInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<BloodGroupInfo>> getAllBloodGroupInfos(Pageable pageable)
        throws URISyntaxException {
        Page<BloodGroupInfo> page = bloodGroupInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bloodGroupInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bloodGroupInfos/:id -> get the "id" bloodGroupInfo.
     */
    @RequestMapping(value = "/bloodGroupInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<BloodGroupInfo> getBloodGroupInfo(@PathVariable Long id) {
        log.debug("REST request to get BloodGroupInfo : {}", id);
        return Optional.ofNullable(bloodGroupInfoRepository.findOne(id))
            .map(bloodGroupInfo -> new ResponseEntity<>(
                bloodGroupInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /bloodGroupInfos/:id -> delete the "id" bloodGroupInfo.
     */
    @RequestMapping(value = "/bloodGroupInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBloodGroupInfo(@PathVariable Long id) {
        log.debug("REST request to delete BloodGroupInfo : {}", id);
        bloodGroupInfoRepository.delete(id);
        bloodGroupInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("bloodGroupInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/bloodGroupInfos/:query -> search for the bloodGroupInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/bloodGroupInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<BloodGroupInfo> searchBloodGroupInfos(@PathVariable String query) {
        return StreamSupport
            .stream(bloodGroupInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
