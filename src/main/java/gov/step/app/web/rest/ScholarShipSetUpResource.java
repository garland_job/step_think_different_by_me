package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.ScholarShipSetUp;
import gov.step.app.domain.User;
import gov.step.app.repository.ScholarShipSetUpRepository;
import gov.step.app.repository.UserRepository;
import gov.step.app.repository.search.ScholarShipSetUpSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.web.rest.util.DateResource;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import gov.step.app.web.rest.util.TransactionIdResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ScholarShipSetUp.
 */
@RestController
@RequestMapping("/api")
public class ScholarShipSetUpResource {

    private final Logger log = LoggerFactory.getLogger(ScholarShipSetUpResource.class);

    @Inject
    private ScholarShipSetUpRepository scholarShipSetUpRepository;

    @Inject
    private ScholarShipSetUpSearchRepository scholarShipSetUpSearchRepository;

    @Inject
    private UserRepository userRepository;

    DateResource dr=new DateResource();
    TransactionIdResource transactionId =new TransactionIdResource();

    /**
     * POST  /scholarShipSetUps -> Create a new scholarShipSetUp.
     */
    @RequestMapping(value = "/scholarShipSetUps",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ScholarShipSetUp> createScholarShipSetUp(@RequestBody ScholarShipSetUp scholarShipSetUp) throws URISyntaxException {
        log.debug("REST request to save ScholarShipSetUp : {}", scholarShipSetUp);
        if (scholarShipSetUp.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new scholarShipSetUp cannot already have an ID").body(null);
        }

        User user = userRepository.getOne(SecurityUtils.getCurrentUserId());
        scholarShipSetUp.setCreateBy(user);
        scholarShipSetUp.setCreateDate(dr.getDateAsLocalDate());
        scholarShipSetUp.setScholarId(transactionId.getGeneratedid("Scholar-"));

        ScholarShipSetUp result = scholarShipSetUpRepository.save(scholarShipSetUp);
        scholarShipSetUpSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/scholarShipSetUps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("scholarShipSetUp", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /scholarShipSetUps -> Updates an existing scholarShipSetUp.
     */
    @RequestMapping(value = "/scholarShipSetUps",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ScholarShipSetUp> updateScholarShipSetUp(@RequestBody ScholarShipSetUp scholarShipSetUp) throws URISyntaxException {
        log.debug("REST request to update ScholarShipSetUp : {}", scholarShipSetUp);
        if (scholarShipSetUp.getId() == null) {
            return createScholarShipSetUp(scholarShipSetUp);
        }

        scholarShipSetUp.setUpdateBy(SecurityUtils.getCurrentUserId());
        scholarShipSetUp.setUpdateDate(dr.getDateAsLocalDate());

        ScholarShipSetUp result = scholarShipSetUpRepository.save(scholarShipSetUp);
        scholarShipSetUpSearchRepository.save(scholarShipSetUp);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("scholarShipSetUp", scholarShipSetUp.getId().toString()))
            .body(result);
    }

    /**
     * GET  /scholarShipSetUps -> get all the scholarShipSetUps.
     */
    @RequestMapping(value = "/scholarShipSetUps",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ScholarShipSetUp>> getAllScholarShipSetUps(Pageable pageable)
        throws URISyntaxException {
        Page<ScholarShipSetUp> page = scholarShipSetUpRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/scholarShipSetUps");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /scholarShipSetUps/:id -> get the "id" scholarShipSetUp.
     */
    @RequestMapping(value = "/scholarShipSetUps/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ScholarShipSetUp> getScholarShipSetUp(@PathVariable Long id) {
        log.debug("REST request to get ScholarShipSetUp : {}", id);
        return Optional.ofNullable(scholarShipSetUpRepository.findOne(id))
            .map(scholarShipSetUp -> new ResponseEntity<>(
                scholarShipSetUp,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /scholarShipSetUps/:id -> delete the "id" scholarShipSetUp.
     */
    @RequestMapping(value = "/scholarShipSetUps/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteScholarShipSetUp(@PathVariable Long id) {
        log.debug("REST request to delete ScholarShipSetUp : {}", id);
        scholarShipSetUpRepository.delete(id);
        scholarShipSetUpSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("scholarShipSetUp", id.toString())).build();
    }

    /**
     * SEARCH  /_search/scholarShipSetUps/:query -> search for the scholarShipSetUp corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/scholarShipSetUps/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ScholarShipSetUp> searchScholarShipSetUps(@PathVariable String query) {
        return StreamSupport
            .stream(scholarShipSetUpSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
