package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.*;
import gov.step.app.repository.*;
import gov.step.app.repository.search.InstEmployeeSearchRepository;
import gov.step.app.repository.search.UserSearchRepository;
import gov.step.app.security.AuthoritiesConstants;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.MailService;
import gov.step.app.service.UserService;
import gov.step.app.service.constnt.HRMManagementConstant;
import gov.step.app.service.util.InstEmployeeInfo;
import gov.step.app.service.util.MiscFileInfo;
import gov.step.app.service.util.MiscFileUtilities;
import gov.step.app.web.rest.jdbc.dao.CommonFolderCreator;
import gov.step.app.web.rest.jdbc.dao.RptJdbcDao;
import gov.step.app.web.rest.util.AttachmentUtil;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import gov.step.app.web.rest.util.TransactionIdResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.thymeleaf.util.StringUtils.substring;

/**
 * REST controller for managing InstEmployee.
 */
@RestController
@RequestMapping("/api")
public class InstEmployeeResource {

    private final Logger log = LoggerFactory.getLogger(InstEmployeeResource.class);

    @Inject
    private InstEmployeeRepository instEmployeeRepository;
    @Inject
    private InstEmployeeSearchRepository instEmployeeSearchRepository;
    @Inject
    private UserService userService;
    @Inject
    private InstEmpAddressRepository instEmpAddressRepository;
    @Inject
    private InstEmpEduQualiRepository instEmpEduQualiRepository;
    @Inject
    private InstEmplTrainingRepository instEmplTrainingRepository;
    @Inject
    private InstEmplRecruitInfoRepository instEmplRecruitInfoRepository;
    @Inject
    private InstEmplBankInfoRepository instEmplBankInfoRepository;
    @Inject
    private InstEmplExperienceRepository instEmplExperienceRepository;
    @Inject
    private InstEmpSpouseInfoRepository instEmpSpouseInfoRepository;
    @Inject
    private RptJdbcDao rptJdbcDao;
    @Inject
    private UserRepository userRepository;
    @Inject
    private MailService mailService;
    @Inject
    private AuthorityRepository authorityRepository;
    @Inject
    private UserSearchRepository userSearchRepository;
    @Inject
    private InstituteRepository instituteRepository;
    @Inject
    NotificationStepRepository notificationStepRepository;
    @Inject
    HrEmployeeInfoRepository hrEmployeeInfoRepository;
    @Inject
    CommonFolderCreator commonFolderCreator;
    @Inject
    HrDepartmentHeadInfoRepository hrDepartmentHeadInfoRepository;
    @Inject
    private HrEmploymentInfoRepository hrEmploymentInfoRepository;

    MiscFileUtilities fileUtils = new MiscFileUtilities();

    String filepath = HRMManagementConstant.INST_EMP_FILE_DIR;

    /**
     * POST  /instEmployees -> Create a new instEmployee.
     */
    @RequestMapping(value = "/instEmployees",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<InstEmployee> createInstEmployee(@Valid @RequestBody InstEmployee instEmployee) throws URISyntaxException, Exception {
        log.debug("REST request to save InstEmployee : {}", instEmployee);
        Institute institute = null;

        String isDeptHead = instEmployee.getRollNo();
        if(isDeptHead.equals(null) || isDeptHead.equals("NO") ){
            instEmployee.setIsHeadOfDept("NO");
        }

        if (instEmployee.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new instEmployee cannot already have an ID").body(null);
        }
        Integer instTypeCode=0;
        String year = Integer.toString(instEmployee.getDob().getYear());
        String yy = year.substring(year.length() - 2);

        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTITUTE)){

            institute = instituteRepository.findOneByUserIsCurrentUser();

            if(institute.getType()==InstituteType.Government){
                if(instEmployee.getIsHeadOfDept().equals("YES")){
                    InstEmployee deptHead= instEmployeeRepository.findDeptHeadByDeptAndInstitute(instEmployee.getHrdepartmentSetup().getId(),institute.getId());
                    if(deptHead!=null){
                        deptHead.setIsHeadOfDept("NO");
                        instEmployeeRepository.save(deptHead);
                    }
                }
                if(instEmployee.getIsHeadOfDept().equals("YES")){
                    log.debug("enter into dept status YES ");
                    instEmployee.setStatus(2);
                    instEmployee.setEntryStatus("NEW");
                }
                else if(instEmployee.getIsHeadOfDept().equals("NO")){
                    log.debug("enter into dept status NO ");
                    instEmployee.setStatus(1);
                    instEmployee.setEntryStatus("NEW");
                }
                instTypeCode=1;
            }else{
                log.debug("current institute is non govt inst");
                instEmployee.setStatus(2);
                instEmployee.setEntryStatus("NEW");
                instTypeCode=2;
            }
        }

        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ROLE_HRM_USER_NAME)){
            log.debug("enter into role hrm user and is dept head yes");
            institute = instituteRepository.findOneWithEagerRelationships(instEmployeeRepository.findCurrentOne().getInstitute().getId());
            InstEmployee currentEmployee=instEmployeeRepository.findCurrentOne();
            log.debug("employee's hr dept"+instEmployee.getHrdepartmentSetup()+"employees inst type"+institute.getType()+"current employees is dept head"+currentEmployee.getIsHeadOfDept());
            if( instEmployee.getHrdepartmentSetup() ==null && institute.getType()==InstituteType.Government ){
                if(currentEmployee.getIsHeadOfDept().equals("YES")){
                    instTypeCode=1;
                    instEmployee.setStatus(0);
                    instEmployee.setEntryStatus("NEW");
                    instEmployee.setHrdepartmentSetup(instEmployeeRepository.findCurrentOne().getHrdepartmentSetup());
                }
            }
         }
        instEmployee.setInstitute(institute);
        log.debug(" institute's information"+institute);
//        HrDepartmentHeadInfo headInfo = new HrDepartmentHeadInfo();
        String code= yy+"-"+instTypeCode+"-"+instEmployee.getDistrict().getCode()+"-"+((int)(Math.random()*9000)+1000);
        instEmployee.setCode(code);
        log.debug("file name test--"+instEmployee.getQuotaCertName());
        if(instEmployee.getQuotaCertName() !=null && instEmployee.getQuotaCert() !=null){
            if(instEmployee.getQuotaCertName() !=null && instEmployee.getQuotaCertName().length()>0){
                instEmployee.setQuotaCertName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmployee.getQuotaCertName().replace("/", "_"), instEmployee.getQuotaCert()));
            }else{
                //instEmployee.setImageName(AttachmentUtil.saveAttachment(filepath, instEmployee.getImageName().replace("/", "_"), instEmployee.getImage()));
                instEmployee.setQuotaCertName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmployee.getQuotaCertName().replace("/", "_"), instEmployee.getQuotaCert()));
            }
        }
        instEmployee.setQuotaCert(null);
        instEmployee.setCreateBy(SecurityUtils.getCurrentUserId());
        InstEmployee result = instEmployeeRepository.save(instEmployee);
        log.debug("Inst employee saved result "+result);
        //log.debug("Current user login info "+instEmployeeRepository.findCurrentOne().getIsHeadOfDept());
        //log.debug("get result's head of dept "+result.getIsHeadOfDept());

        if(result.getInstitute().getType()==InstituteType.Government && SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTITUTE)){

            if(result.getIsHeadOfDept().equals("NO")){
                User user=userService.createCustomUserInformation(result.getCode().toLowerCase(),
                    HRMManagementConstant.INST_EMPLOYEE_DEFAULT_PASSWORD,
                    result.getName(), null, result.getEmail(),
                    "en", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME,false);
                instEmployee.setUser(user);
                instEmployeeRepository.save(instEmployee);
            }
        }
        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTEMP) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ROLE_HRM_USER_NAME)){
            if(instEmployeeRepository.findCurrentOne().getIsHeadOfDept().equals("YES")){
                log.debug("enter into hr dept setup");
                User user=userService.createCustomUserInformation(result.getCode().toLowerCase(),
                    HRMManagementConstant.INST_EMPLOYEE_DEFAULT_PASSWORD,
                    result.getName(), null, result.getEmail(),
                    "en", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME,false);
                instEmployee.setUser(user);
                instEmployeeRepository.save(instEmployee);
            }
        }

        if(result.getInstitute().getType() == InstituteType.NonGovernment || result.getIsHeadOfDept().equals("YES")){
            User user=userService.createCustomUserInformation(result.getCode().toLowerCase(),
                HRMManagementConstant.INST_EMPLOYEE_DEFAULT_PASSWORD,
                result.getName(), null, result.getEmail(),
                "en", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME,true);
            instEmployee.setUser(user);
            instEmployeeRepository.save(instEmployee);

            String msilContent="Hello " +result.getName()+","+
                "\n" +
                "You are registered. \n" +
                "Signing in DIRECTORATE OF TECHNICAL EDUCATION Website. Your User Name:"+result.getCode()+" and password: 123456" +
                "\n" +
                "this is a system generated mail.Please contrct DIRECTORATE OF TECHNICAL EDUCATION for any query.";
            mailService.sendEmail(result.getEmail(),"Employee User login Credential",msilContent,false,false);
        }


//        if (result.getInstitute().getType() == InstituteType.Government || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ROLE_HRM_USER_NAME) ||  SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTEMP)){
//            HrEmployeeInfo hrEmployeeInfo = new HrEmployeeInfo();
//
//            TransactionIdResource transactionIdResource = new TransactionIdResource();
//            hrEmployeeInfo.setFullName(instEmployee.getName());
//            hrEmployeeInfo.setFatherName(instEmployee.getFatherName());
//            hrEmployeeInfo.setMotherName(instEmployee.getMotherName());
//            hrEmployeeInfo.setBirthDate(instEmployee.getDob());
//            hrEmployeeInfo.setApointmentGoDate(instEmployee.getJoiningDate());
//            hrEmployeeInfo.setPresentId("");
//            hrEmployeeInfo.setEmailAddress(instEmployee.getEmail());
//            hrEmployeeInfo.setMobileNumber(instEmployee.getContactNo());
//            hrEmployeeInfo.setEmployeeType(designationType.Teacher);
//            if(instEmployee.getGender() != null) {
//                hrEmployeeInfo.setGender(Gender.valueOf(instEmployee.getGender()));
//            }
//            hrEmployeeInfo.setBirthPlace("NONE");
//            hrEmployeeInfo.setAnyDisease("NONE");
//            hrEmployeeInfo.setOfficerStuff("NONE");
//            hrEmployeeInfo.setTinNumber("NONE");
////            hrEmployeeInfo.setMaritalStatus(result.getMaritalStatus());
//            if(instEmployee.getBloodGroup() != null) {
//                hrEmployeeInfo.setBloodGroup(instEmployee.getBloodGroup());
//            }
//            if(instEmployee.getNationality() != null) {
//                hrEmployeeInfo.setNationality(instEmployee.getNationality());
//
//            }
//            if(instEmployee.getJobQuota() != null) {
//                hrEmployeeInfo.setQuota(instEmployee.getJobQuota());
//            }
//            hrEmployeeInfo.setBirthCertificateNo(instEmployee.getBirthCertNo());
//            hrEmployeeInfo.setEmployeeId(code); // use generated code
//            hrEmployeeInfo.setDateOfJoining(result.getJoiningDate());
//            hrEmployeeInfo.setActiveStatus(true);
//            hrEmployeeInfo.setCreateDate(LocalDate.now());
//            hrEmployeeInfo.setCreateBy(SecurityUtils.getCurrentUserId());
//            hrEmployeeInfo.setUpdateDate(LocalDate.now());
//            hrEmployeeInfo.setUpdateBy(SecurityUtils.getCurrentUserId());
//            hrEmployeeInfo.setDepartmentInfo(instEmployee.getHrdepartmentSetup());
//            hrEmployeeInfo.setDesignationInfo(result.getDesignationSetup());
//            hrEmployeeInfo.setOrganizationType("Institute");
//            hrEmployeeInfo.setInstCategory(result.getInstitute().getInstCategory());
//            hrEmployeeInfo.setInstitute(result.getInstitute());
//            hrEmployeeInfo.setInstEmployee(result);
//            hrEmployeeInfo.setUser(result.getUser());
//            hrEmployeeInfo.setActiveAccount(true);
//            hrEmployeeInfo.setApointmentGoDate(result.getAppointmentGoDate());
//            hrEmployeeInfo.setPrlDate(result.getPrlDate());
//            hrEmployeeInfo.setRetirementDate(result.getRetirementDate());
//            hrEmployeeInfo.setEncadrement(result.getEncadrement());
//            hrEmployeeInfo.setNationalId("11111111111");
//
//            // Set Hr Employee Image
//            MiscFileInfo employeePhoto = new MiscFileInfo();
//            try{
//                if(instEmployee.getQuotaCertName() != null) {
//                    if (AttachmentUtil.retriveAttachment(filepath, instEmployee.getQuotaCertName()) != null) {
//                        instEmployee.setQuotaCert(AttachmentUtil.retriveAttachment(filepath, instEmployee.getQuotaCertName()));
//                    }
//                }
//                employeePhoto.fileData(AttachmentUtil.retriveAttachment(filepath, instEmployee.getQuotaCertName()))
//                    .fileName(instEmployee.getQuotaCertName())
//                    .contentType(instEmployee.getQuotaCertContentType())
//                    .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
//                System.out.println("enter photo1 ");
//
//                employeePhoto = fileUtils.saveFileAsByte(employeePhoto);
//                employeePhoto.fileName(instEmployee.getQuotaCertName());
//                employeePhoto = fileUtils.updateFileAsByte(employeePhoto);
//            }catch(Exception ex){
//                System.out.println("enter photo error exception ");
//                log.error("Exception: UpdateHrInfo empPhoto "+ex.getMessage());
//            }
//            hrEmployeeInfo.setEmpPhotoContentType(employeePhoto.contentType());
//            hrEmployeeInfo.setEmpPhoto(new byte[1]);
//            hrEmployeeInfo.setImageName(employeePhoto.fileName());
//            HrEmployeeInfo hrEmp = hrEmployeeInfoRepository.save(hrEmployeeInfo);
//
//            HrEmploymentInfo hrEmploymentInfo = new HrEmploymentInfo();
//            hrEmploymentInfo.setEmployeeInfo(hrEmp);
//            hrEmploymentInfo.setOrganizationType(hrEmp.getOrganizationType());
//            hrEmploymentInfo.setDepartmentInfo(hrEmp.getDepartmentInfo());
//            hrEmploymentInfo.setDesignationInfo(hrEmp.getDesignationInfo());
//            hrEmploymentInfo.setCreateBy(SecurityUtils.getCurrentUserId());
//            hrEmploymentInfo.setCreateDate(LocalDate.now());
//            hrEmploymentInfo.setActiveStatus(true);
//            hrEmploymentInfo.setDesignationInfo(hrEmp.getDesignationInfo());
//            hrEmploymentInfo.setInstitute(hrEmp.getInstitute());
//            hrEmploymentInfo.setWorkAreaDtl(hrEmp.getWorkAreaDtl());
//            hrEmploymentInfo.setEmployeeType(hrEmp.getEmployementType());
//            hrEmploymentInfo.setJoiningDate(hrEmp.getDateOfJoining());
//            hrEmploymentInfoRepository.save(hrEmploymentInfo);
//
//            User user1 = hrEmployeeInfo.getUser();
//
//            Set<Authority> authorities = new HashSet<>();
//            authorities.add(authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME));
//            authorities.add(authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME));
//            if(instEmployee.getIsJPAdmin()){
//                authorities.add(authorityRepository.findOne(AuthoritiesConstants.JPADMIN));
//            }
//            user1.setAuthorities(authorities);
//            userRepository.save(user1);
//
//            if(isDeptHead.equals("YES") && instEmployee.getHrdepartmentSetup() !=null){
//                System.out.println("enter into  hr Department HeadInfo Repository save portion");
//
//                hrDepartmentHeadInfoRepository.updateAllDepartmentHeadActiveStatus(instEmployee.getHrdepartmentSetup().getId(),false);
//                headInfo.setHeadInfo(hrEmp);
//                headInfo.setDepartmentInfo(instEmployee.getHrdepartmentSetup());
//                headInfo.setActiveHead(true);
//                headInfo.setActiveStatus(true);
//                headInfo.setCreateBy(SecurityUtils.getCurrentUserId());
//                headInfo.setCreateDate(LocalDate.now());
//                headInfo.setJoinDate(hrEmp.getDateOfJoining());
//                hrDepartmentHeadInfoRepository.save(headInfo);
//            }
//        }


        return ResponseEntity.created(new URI("/api/instEmployees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("instEmployee", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /instEmployees -> Updates an existing instEmployee.
     */
    @RequestMapping(value = "/instEmployees",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> updateInstEmployee(@Valid @RequestBody InstEmployee instEmployee) throws URISyntaxException, Exception {
        log.debug("REST request to update InstEmployee : {}", instEmployee);
        if (instEmployee.getId() == null) {
            return createInstEmployee(instEmployee);
        }
        InstEmployee previnstEmployee= instEmployeeRepository.findOne(instEmployee.getId());
        if(instEmployee.getImageName() !=null && instEmployee.getImage() !=null){
            if(previnstEmployee.getImageName() !=null && previnstEmployee.getImageName().length()>0){
                log.debug(" image replace trigger----------------------------------------------");
                instEmployee.setImageName(AttachmentUtil.replaceAttachment(filepath, previnstEmployee.getImageName(),instEmployee.getImageName().replace("/", "_"), instEmployee.getImage()));
            }else{
                //instEmployee.setImageName(AttachmentUtil.saveAttachment(filepath, instEmployee.getImageName().replace("/", "_"), instEmployee.getImage()));
                instEmployee.setImageName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmployee.getImageName().replace("/", "_"), instEmployee.getImage()));
            }
        }
        instEmployee.setImage(null);

        if(instEmployee.getNidImageName() !=null && instEmployee.getNidImage() !=null){

            if(previnstEmployee.getNidImageName() !=null && previnstEmployee.getNidImageName().length()>0){
                log.debug(" NidImageName replace trigger----------------------------------------------");
                instEmployee.setNidImageName(AttachmentUtil.replaceAttachment(filepath, previnstEmployee.getNidImageName(),instEmployee.getNidImageName().replace("/", "_"), instEmployee.getNidImage()));
            }else{
                //instEmployee.setNidImageName(AttachmentUtil.saveAttachment(filepath, instEmployee.getNidImageName().replace("/", "_"), instEmployee.getNidImage()));
                instEmployee.setNidImageName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmployee.getNidImageName().replace("/", "_"), instEmployee.getNidImage()));
            }
        }
        instEmployee.setNidImage(null);
//        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
//           User user= userRepository.findOne(instEmployee.getUser().getId());
//            user.setActivated(true);
//           userRepository.save(user);
//        }

        log.debug(" birth file name test--"+instEmployee.getBirthCertNoName());
        if(instEmployee.getBirthCertNoName() !=null && instEmployee.getBirthCertImage() !=null){
            if(previnstEmployee.getBirthCertNoName() !=null && previnstEmployee.getBirthCertNoName().length()>0){
                log.debug(" BirthCertNoName replace trigger----------------------------------------------");
                instEmployee.setBirthCertNoName(AttachmentUtil.replaceAttachment(filepath, previnstEmployee.getBirthCertNoName(),instEmployee.getBirthCertNoName().replace("/", "_"), instEmployee.getBirthCertImage()));
            }else{
                instEmployee.setBirthCertNoName(AttachmentUtil.saveAttachmentWithoutExtension(filepath, instEmployee.getBirthCertNoName().replace("/", "_"), instEmployee.getBirthCertImage()));
            }
        }
        instEmployee.setBirthCertImage(null);
        InstEmployee result = instEmployeeRepository.save(instEmployee);
        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {

            if (result.getInstitute().getType() == InstituteType.Government){
                HrEmployeeInfo hrEmployeeInfo = new HrEmployeeInfo();
//                TransactionIdResource transactionIdResource = new TransactionIdResource();
                hrEmployeeInfo.setFullName(instEmployee.getName());
                hrEmployeeInfo.setFatherName(instEmployee.getFatherName());
                hrEmployeeInfo.setMotherName(instEmployee.getMotherName());
                hrEmployeeInfo.setBirthDate(instEmployee.getDob());
                hrEmployeeInfo.setApointmentGoDate(instEmployee.getJoiningDate());
                hrEmployeeInfo.setPresentId("");
                hrEmployeeInfo.setEmailAddress(instEmployee.getEmail());
                hrEmployeeInfo.setMobileNumber(instEmployee.getContactNo());
                hrEmployeeInfo.setEmployeeType(designationType.Teacher);
                if(instEmployee.getGender() != null) {
                    hrEmployeeInfo.setGender(Gender.valueOf(instEmployee.getGender()));
                }
                hrEmployeeInfo.setBirthPlace("NONE");
                hrEmployeeInfo.setAnyDisease("NONE");
                hrEmployeeInfo.setOfficerStuff("NONE");
                hrEmployeeInfo.setTinNumber("NONE");
//            hrEmployeeInfo.setMaritalStatus(result.getMaritalStatus());
                if(instEmployee.getBloodGroup() != null) {
                    hrEmployeeInfo.setBloodGroup(instEmployee.getBloodGroup());
                }
                if(instEmployee.getNationality() != null) {
                    hrEmployeeInfo.setNationality(instEmployee.getNationality());

                }
                if(instEmployee.getJobQuota() != null) {
                    hrEmployeeInfo.setQuota(instEmployee.getJobQuota());
                }
                hrEmployeeInfo.setBirthCertificateNo(instEmployee.getBirthCertNo());
                hrEmployeeInfo.setEmployeeId(instEmployee.getCode()); // use generated code
                hrEmployeeInfo.setDateOfJoining(result.getJoiningDate());
                hrEmployeeInfo.setActiveStatus(true);
                hrEmployeeInfo.setCreateDate(LocalDate.now());
                hrEmployeeInfo.setCreateBy(SecurityUtils.getCurrentUserId());
                hrEmployeeInfo.setUpdateDate(LocalDate.now());
                hrEmployeeInfo.setUpdateBy(SecurityUtils.getCurrentUserId());
                hrEmployeeInfo.setDepartmentInfo(instEmployee.getHrdepartmentSetup());
                hrEmployeeInfo.setDesignationInfo(result.getDesignationSetup());
                hrEmployeeInfo.setOrganizationType("Institute");
                hrEmployeeInfo.setInstCategory(result.getInstitute().getInstCategory());
                hrEmployeeInfo.setInstitute(result.getInstitute());
                hrEmployeeInfo.setInstEmployee(result);
                hrEmployeeInfo.setUser(result.getUser());
                hrEmployeeInfo.setActiveAccount(true);
                hrEmployeeInfo.setApointmentGoDate(result.getAppointmentGoDate());
                hrEmployeeInfo.setPrlDate(result.getPrlDate());
                hrEmployeeInfo.setRetirementDate(result.getRetirementDate());
                hrEmployeeInfo.setEncadrement(result.getEncadrement());
                hrEmployeeInfo.setNationalId("11111111111");

                // Set Hr Employee Image
                MiscFileInfo employeePhoto = new MiscFileInfo();
                try{
                    if(instEmployee.getQuotaCertName() != null) {
                        if (AttachmentUtil.retriveAttachment(filepath, instEmployee.getQuotaCertName()) != null) {
                            instEmployee.setQuotaCert(AttachmentUtil.retriveAttachment(filepath, instEmployee.getQuotaCertName()));
                        }
                    }
                    employeePhoto.fileData(AttachmentUtil.retriveAttachment(filepath, instEmployee.getQuotaCertName()))
                        .fileName(instEmployee.getQuotaCertName())
                        .contentType(instEmployee.getQuotaCertContentType())
                        .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
                    System.out.println("enter photo1 ");

                    employeePhoto = fileUtils.saveFileAsByte(employeePhoto);
                    employeePhoto.fileName(instEmployee.getQuotaCertName());
                    employeePhoto = fileUtils.updateFileAsByte(employeePhoto);
                }catch(Exception ex){
                    System.out.println("enter photo error exception ");
                    log.error("Exception: UpdateHrInfo empPhoto "+ex.getMessage());
                }
                hrEmployeeInfo.setEmpPhotoContentType(employeePhoto.contentType());
                hrEmployeeInfo.setEmpPhoto(new byte[1]);
                hrEmployeeInfo.setImageName(employeePhoto.fileName());
                HrEmployeeInfo hrEmp = hrEmployeeInfoRepository.save(hrEmployeeInfo);

                HrEmploymentInfo hrEmploymentInfo = new HrEmploymentInfo();
                hrEmploymentInfo.setEmployeeInfo(hrEmp);
                hrEmploymentInfo.setOrganizationType(hrEmp.getOrganizationType());
                hrEmploymentInfo.setDepartmentInfo(hrEmp.getDepartmentInfo());
                hrEmploymentInfo.setDesignationInfo(hrEmp.getDesignationInfo());
                hrEmploymentInfo.setCreateBy(SecurityUtils.getCurrentUserId());
                hrEmploymentInfo.setCreateDate(LocalDate.now());
                hrEmploymentInfo.setActiveStatus(true);
                hrEmploymentInfo.setDesignationInfo(hrEmp.getDesignationInfo());
                hrEmploymentInfo.setInstitute(hrEmp.getInstitute());
                hrEmploymentInfo.setWorkAreaDtl(hrEmp.getWorkAreaDtl());
                hrEmploymentInfo.setEmployeeType(hrEmp.getEmployementType());
                hrEmploymentInfo.setJoiningDate(hrEmp.getDateOfJoining());
                hrEmploymentInfoRepository.save(hrEmploymentInfo);

                User user1 = hrEmployeeInfo.getUser();
                Set<Authority> authorities = new HashSet<>();
                authorities.add(authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME));
                authorities.add(authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME));

                if(instEmployee.getIsJPAdmin()){
                    authorities.add(authorityRepository.findOne(AuthoritiesConstants.JPADMIN));
                }
                user1.setAuthorities(authorities);
                user1.setActivated(true);
                userRepository.save(user1);

                if(instEmployee.getIsHeadOfDept().equals("YES") && instEmployee.getHrdepartmentSetup() !=null){
                    System.out.println("enter into  hr Department HeadInfo Repository save portion");
                    HrDepartmentHeadInfo headInfo = new HrDepartmentHeadInfo();
                    hrDepartmentHeadInfoRepository.updateAllDepartmentHeadActiveStatus(instEmployee.getHrdepartmentSetup().getId(),false);
                    headInfo.setHeadInfo(hrEmp);
                    headInfo.setDepartmentInfo(instEmployee.getHrdepartmentSetup());
                    headInfo.setActiveHead(true);
                    headInfo.setActiveStatus(true);
                    headInfo.setCreateBy(SecurityUtils.getCurrentUserId());
                    headInfo.setCreateDate(LocalDate.now());
                    headInfo.setJoinDate(hrEmp.getDateOfJoining());
                    hrDepartmentHeadInfoRepository.save(headInfo);
                }
            }
            String msilContent="Hello " +result.getName()+","+
                "\n" +
                "You are registered. \n" +
                "Signing in DIRECTORATE OF TECHNICAL EDUCATION Website. Your User Name:"+result.getCode()+" and password: 123456" +
                "\n" +
                "this is a system generated mail.Please contrct DIRECTORATE OF TECHNICAL EDUCATION for any query.";
            mailService.sendEmail(result.getEmail(),"Employee User login Credential",msilContent,false,false);
        }

//        instEmployeeSearchRepository.save(instEmployee);
        NotificationStep notificationSteps = new NotificationStep();
        notificationSteps.setNotification(instEmployee.getName()+" has updated his information.");
        notificationSteps.setStatus(true);
        notificationSteps.setUrls("employeeInfo.approve");
        notificationSteps.setUserId(instEmployee.getInstitute().getUser().getId());
        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTITUTE) && result.getStatus()==-2){
            notificationSteps.setUrls("notificationStep");
            notificationSteps.setUserId(instEmployee.getCreateBy());
            notificationSteps.setNotification(instEmployee.getName()+" has has been declined by institute for "+instEmployee.getCosOfDell());
        }
        notificationStepRepository.save(notificationSteps);

        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN) && result.getStatus()==-3){
            NotificationStep notificationStepss = new NotificationStep();
            notificationStepss.setStatus(true);
            notificationSteps.setUrls("notificationStep");
            notificationStepss.setUserId(instEmployee.getInstitute().getUser().getId());
            notificationStepss.setNotification(instEmployee.getName()+" has has been declined by Admin for "+instEmployee.getCosOfDell());
            notificationStepRepository.save(notificationStepss);
            NotificationStep notificationStepsss = new NotificationStep();
            notificationStepsss.setStatus(true);
            notificationStepsss.setUrls("notificationStep");
            notificationStepsss.setUserId(instEmployee.getCreateBy());
            notificationStepsss.setNotification(instEmployee.getName()+" has  been declined by Admin for "+instEmployee.getCosOfDell());
            notificationStepRepository.save(notificationStepsss);
        }

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("instEmployee", instEmployee.getId().toString()))
            .body(result);
    }

    /**
     * GET  /instEmployees -> get all the instEmployees.
     */
    @RequestMapping(value = "/instEmployees",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> getAllInstEmployees(Pageable pageable)
          throws Exception{
        Page<InstEmployee> page = instEmployeeRepository.findAll(pageable);

//        for(InstEmployee each: page){
//
//            if(each.getQuotaCertName() != null && AttachmentUtil.retriveAttachment(filepath, each.getQuotaCertName()) != null){
//                each.setQuotaCert(AttachmentUtil.retriveAttachment(filepath, each.getQuotaCertName()));
//            }
//            instEmployees.add(each);
//        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instEmployees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
   /* @RequestMapping(value = "/instEmployees/genInfoByCurrentUser",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> getAllInstEmployees(Pageable pageable)
        throws URISyntaxException {
        Page<InstEmployee> page = instEmployeeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instEmployees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }*/

    /**
     * GET  /instEmployees/:id -> get the "id" instEmployee.
     */
    @RequestMapping(value = "/instEmployees/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getInstEmployee(@PathVariable Long id) {
        log.debug("REST request to get InstEmployee : {}", id);
        return Optional.ofNullable(instEmployeeRepository.findOne(id))
            .map(instEmployee -> new ResponseEntity<>(
                instEmployee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/instEmployees/nid/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> getInstEmployeeNid( @RequestParam String value) {
        log.debug("REST request to get instEmployee nid by code : {}", value);
        Optional<InstEmployee> instEmployee = instEmployeeRepository.findOneByNid(value);
        Map map =new HashMap();
        map.put("value",value);
        if(Optional.empty().equals(instEmployee)){
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }else{
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/instEmployee/indexNo/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> findUniqueIndexNo(@RequestParam String value) {
        log.debug("REST request to get instEmployee nid by code : {}", value);
        Optional<InstEmployee> indexNo = instEmployeeRepository.findUniqueIndexNo(value);
        Map map =new HashMap();
        map.put("value",value);
        if(Optional.empty().equals(indexNo)){
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }else{
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/instEmployees/birthCertNo/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> getInstEmployeeBirthCertNo( @RequestParam String value) {
        log.debug("REST request to get instEmployee nid by code : {}", value);
        Optional<InstEmployee> instEmployee = instEmployeeRepository.findOneByBirthCertNo(value);
        Map map =new HashMap();
        map.put("value",value);
        if(Optional.empty().equals(instEmployee)){
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }else{
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }
    }

    /**
     * GET  /instEmployees/:id -> get the "id" instEmployee.
     */
    @RequestMapping(value = "/instEmployees/institute/employee/{code:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getInstEmployeeByInstituteAndCode(@PathVariable String code) {
        log.debug("REST request to get InstEmployee by institute and code: {}", code);
        Institute institute = instituteRepository.findOneByUserIsCurrentUser();
        if(institute == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return Optional.ofNullable(instEmployeeRepository.findByInstituteAndEmployeecode(institute.getId(),code))
            .map(instEmployee -> new ResponseEntity<>(
                instEmployee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



    /**
     * GET  /instEmployees/:id -> get the "id" instEmployee.
     */
    @RequestMapping(value = "/instEmployees/institute/employeeByDeptId/{deptId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getDeptHeadByDeptAndInstitute(@PathVariable Long deptId) {
        log.debug("REST request to get InstEmployee by institute and code: {}", deptId);
        Institute institute = instituteRepository.findOneByUserIsCurrentUser();
        if(institute == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return Optional.ofNullable(instEmployeeRepository.findDeptHeadByDeptAndInstitute(deptId,institute.getId()))
            .map(instEmployee -> new ResponseEntity<>(
                instEmployee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /instEmployees/:id -> get the "id" instEmployee.
     */
    @RequestMapping(value = "/instEmployees/institute/current/employee/{index}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getInstEmployeeByInstituteAndIndex(@PathVariable String index) {
        log.debug("REST request to get InstEmployee by institute and index: {}", index);
        Institute institute = instituteRepository.findOneByUserIsCurrentUser();
        if(institute == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return Optional.ofNullable(instEmployeeRepository.findByInstituteAndEmployeeIndex(institute.getId(), index))
            .map(instEmployee -> new ResponseEntity<>(
                instEmployee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    /**
     * GET  /instEmployees/:index -> get the "index" instEmployee.
     */
    @RequestMapping(value = "/instEmployees/employee/{indexNo}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getInstEmployeeByIndex(@PathVariable String indexNo) {
        log.debug("REST request to get InstEmployee by institute and index: {}", indexNo);

        return Optional.ofNullable(instEmployeeRepository.findOneByEmployeeIndexNo(indexNo))
            .map(instEmployee -> new ResponseEntity<>(
                instEmployee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    /**
     * GET  /instEmployees/current -> Current Logged in Employee.
     */
    @RequestMapping(value = "/instEmployees/current",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getCurrentInstEmployee() {
        log.debug("REST request to get InstEmployee : {}");
        String userName = SecurityUtils.getCurrentUserLogin();
        InstEmployee curInstEmployee = instEmployeeRepository.findCurrentOneByLogin(userName);
        InstEmployee instEmployeeresult=instEmployeeRepository.findOneByEmployeeCode(curInstEmployee.getCode());

        return Optional.ofNullable(instEmployeeresult)
            .map(instEmployee -> new ResponseEntity<>(
                instEmployee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /*
    * GET  /instEmployees/:id -> get the "id" instEmployee.
    */
    @RequestMapping(value = "/instEmployees/code/{code:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployeeInfo> getInstEmployeeByCode(@PathVariable String code) throws Exception{
        log.debug("REST request to get InstEmployee : {}", code);
        InstEmployee instEmployeeresult=instEmployeeRepository.findOneByEmployeeCode(code);
        InstEmployeeInfo instEmployeeInfores= new InstEmployeeInfo();

        if(instEmployeeresult.getImageName() != null) {
            if(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getImageName()) != null){
                instEmployeeresult.setImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getImageName()));
                instEmployeeresult.setImageName(instEmployeeresult.getImageName().substring(0, (instEmployeeresult.getImageName().length() - 1)));

            }
//            instEmployeeresult.setImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getImageName()));
//            instEmployeeresult.setImageName(instEmployeeresult.getImageName().substring(0, (instEmployeeresult.getImageName().length() - 17)));
        }

        if(instEmployeeresult.getQuotaCertName() != null) {
            if(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getQuotaCertName()) != null){
                instEmployeeresult.setQuotaCert(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getQuotaCertName()));
                instEmployeeresult.setQuotaCertName(instEmployeeresult.getQuotaCertName().substring(0, (instEmployeeresult.getQuotaCertName().length() - 1)));

            }

        }
        if(instEmployeeresult.getNidImageName() != null && AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getNidImageName()) != null) {
            instEmployeeresult.setNidImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getNidImageName()));
            instEmployeeresult.setNidImageName(instEmployeeresult.getNidImageName().substring(0, (instEmployeeresult.getNidImageName().length() - 1)));
        }
        if(instEmployeeresult.getBirthCertNoName() !=null) {
            if(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getBirthCertNoName()) != null){
                instEmployeeresult.setBirthCertImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getBirthCertNoName()));

            }
           /* if(instEmployeeresult.getBirthCertNoName() != null){
                instEmployeeresult.setBirthCertNoName(instEmployeeresult.getBirthCertNoName().substring(0, (instEmployeeresult.getBirthCertNoName().length() - 17)));

            }*/
        }

        if(instEmployeeresult !=null){
            instEmployeeInfores.setInstEmployee(instEmployeeresult);
            instEmployeeInfores.setInstEmpAddress(instEmpAddressRepository.findOneByInstEmployeeId(instEmployeeresult.getId()));
            //instEmployeeInfores.setInstEmpEduQualis(instEmpEduQualiRepository.findListByInstEmployeeId(instEmployeeresult.getId()));
            log.debug("REST request to get InstEmployee : {}");
            List<InstEmpEduQuali> instEmpEduQualilist=null;
            instEmpEduQualilist=instEmpEduQualiRepository.findListByInstEmployeeId(instEmployeeresult.getId());
            for(InstEmpEduQuali instEmpEduQuali:instEmpEduQualilist){
                if(instEmpEduQuali.getCertificateCopyName()!=null && AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmpEduQuali.getCertificateCopyName()) != null) {
                    instEmpEduQuali.setCertificateCopy(AttachmentUtil.retriveAttachment("/backup/teacher_info/", instEmpEduQuali.getCertificateCopyName()));
                    instEmpEduQuali.setCertificateCopyName(instEmpEduQuali.getCertificateCopyName().substring(0, (instEmpEduQuali.getCertificateCopyName().length() - 17)));
                }
                if(instEmpEduQuali.getCourseAtchFileName()!=null){
                    instEmpEduQuali.setCourseAtchFile(AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmpEduQuali.getCourseAtchFileName()));
                    //instEmpEduQuali.setCertificateCopyName(instEmpEduQuali.getCertificateCopyName().substring(0, (instEmpEduQuali.getCertificateCopyName().length() - 17)));
                    instEmpEduQuali.setCourseAtchFileName(instEmpEduQuali.getCourseAtchFileName());
                }
                if(instEmpEduQuali.getUgcAttachFileName()!=null){
                    instEmpEduQuali.setUgcAttachFile(AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmpEduQuali.getUgcAttachFileName()));
                    //instEmpEduQuali.setCertificateCopyName(instEmpEduQuali.getCertificateCopyName().substring(0, (instEmpEduQuali.getCertificateCopyName().length() - 17)));
                    instEmpEduQuali.setUgcAttachFileName(instEmpEduQuali.getUgcAttachFileName());
                }
            }

            // Commented out by yzaman 19th April 2016
            //instEmployeeInfores.setInstEmpEduQualis(rptJdbcDao.findListByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmpEduQualis(instEmpEduQualilist);

            List<InstEmplTraining> instEmpTraininglist=instEmplTrainingRepository.findInstEmplTrainingsByEmployeeId(instEmployeeresult.getId());

            for(InstEmplTraining instEmplTraining:instEmpTraininglist){
                if(instEmplTraining.getAttachmentName() !=null && AttachmentUtil.retriveAttachment("/backup/teacher_info/", instEmplTraining.getAttachmentName()) != null){
                instEmplTraining.setAttachment(AttachmentUtil.retriveAttachment("/backup/teacher_info/", instEmplTraining.getAttachmentName()));
                instEmplTraining.setAttachmentName(instEmplTraining.getAttachmentName().substring(0, (instEmplTraining.getAttachmentName().length() - 17)));
                }
            }

            instEmployeeInfores.setInstEmplTrainings(instEmpTraininglist);
            //instEmployeeInfores.setInstEmplTrainings(instEmplTrainingRepository.findInstEmplTrainingsByEmployeeId(instEmployeeresult.getId()));

            InstEmplRecruitInfo instEmplRecruitInfoResult=instEmplRecruitInfoRepository.findByInstEmployeeId(instEmployeeresult.getId());

            if(instEmplRecruitInfoResult !=null ) {
                if (instEmplRecruitInfoResult.getAppoinmentLtrName() != null && instEmplRecruitInfoResult.getAppoinmentLtrName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppoinmentLtrName()) != null) {
                    instEmplRecruitInfoResult.setAppoinmentLtr(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppoinmentLtrName()));
                    instEmplRecruitInfoResult.setAppoinmentLtrName(instEmplRecruitInfoResult.getAppoinmentLtrName().substring(0, (instEmplRecruitInfoResult.getAppoinmentLtrName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getJoiningLetterName() != null && instEmplRecruitInfoResult.getJoiningLetterName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getJoiningLetterName()) != null) {
                    instEmplRecruitInfoResult.setJoiningLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getJoiningLetterName()));
                    instEmplRecruitInfoResult.setJoiningLetterName(instEmplRecruitInfoResult.getJoiningLetterName().substring(0, (instEmplRecruitInfoResult.getJoiningLetterName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecruitResultName() != null && instEmplRecruitInfoResult.getRecruitResultName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitResultName()) != null) {
                    instEmplRecruitInfoResult.setRecruitResult(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitResultName()));
                    instEmplRecruitInfoResult.setRecruitResultName(instEmplRecruitInfoResult.getRecruitResultName().substring(0, (instEmplRecruitInfoResult.getRecruitResultName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecruitNewsLocalName() != null && instEmplRecruitInfoResult.getRecruitNewsLocalName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsLocalName()) != null) {
                    instEmplRecruitInfoResult.setRecruitNewsLocal(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsLocalName()));
                    instEmplRecruitInfoResult.setRecruitNewsLocalName(instEmplRecruitInfoResult.getRecruitNewsLocalName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsLocalName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecruitNewsDailyName() != null && instEmplRecruitInfoResult.getRecruitNewsDailyName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsDailyName()) != null) {
                    instEmplRecruitInfoResult.setRecruitNewsDaily(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsDailyName()));
                    instEmplRecruitInfoResult.setRecruitNewsDailyName(instEmplRecruitInfoResult.getRecruitNewsDailyName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsDailyName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getGoResulatonName() != null && instEmplRecruitInfoResult.getGoResulatonName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getGoResulatonName()) != null) {
                    instEmplRecruitInfoResult.setGoResulaton(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getGoResulatonName()));
                    instEmplRecruitInfoResult.setGoResulatonName(instEmplRecruitInfoResult.getGoResulatonName().substring(0, (instEmplRecruitInfoResult.getGoResulatonName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getCommitteeName() != null && instEmplRecruitInfoResult.getCommitteeName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getCommitteeName()) != null) {
                    instEmplRecruitInfoResult.setCommittee(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getCommitteeName()));
                    instEmplRecruitInfoResult.setCommitteeName(instEmplRecruitInfoResult.getCommitteeName().substring(0, (instEmplRecruitInfoResult.getCommitteeName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecommendationName() != null && instEmplRecruitInfoResult.getRecommendationName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecommendationName()) != null) {
                    instEmplRecruitInfoResult.setRecommendation(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecommendationName()));
                    instEmplRecruitInfoResult.setRecommendationName(instEmplRecruitInfoResult.getRecommendationName().substring(0, (instEmplRecruitInfoResult.getRecommendationName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getSanctionName() != null && instEmplRecruitInfoResult.getSanctionName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSanctionName()) != null) {
                    instEmplRecruitInfoResult.setSanction(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSanctionName()));
                    instEmplRecruitInfoResult.setSanctionName(instEmplRecruitInfoResult.getSanctionName().substring(0, (instEmplRecruitInfoResult.getSanctionName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getSelectionCopyName() != null && instEmplRecruitInfoResult.getSelectionCopyName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSelectionCopyName()) != null) {
                    instEmplRecruitInfoResult.setSelectionCopy(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSelectionCopyName()));
                    instEmplRecruitInfoResult.setSelectionCopyName(instEmplRecruitInfoResult.getSelectionCopyName().substring(0, (instEmplRecruitInfoResult.getSelectionCopyName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getRequiLetterName() != null && instEmplRecruitInfoResult.getRequiLetterName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRequiLetterName()) != null) {
                    instEmplRecruitInfoResult.setRequiLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRequiLetterName()));
                    instEmplRecruitInfoResult.setRequiLetterName(instEmplRecruitInfoResult.getRequiLetterName().substring(0, (instEmplRecruitInfoResult.getRequiLetterName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getPblcCircularName() != null && instEmplRecruitInfoResult.getPblcCircularName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getPblcCircularName()) != null) {
                    instEmplRecruitInfoResult.setPblcCircular(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getPblcCircularName()));
                    instEmplRecruitInfoResult.setPblcCircularName(instEmplRecruitInfoResult.getPblcCircularName().substring(0, (instEmplRecruitInfoResult.getPblcCircularName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getNtrcaCertName() != null && instEmplRecruitInfoResult.getNtrcaCertName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getNtrcaCertName()) != null) {
                    instEmplRecruitInfoResult.setNtrcaCert(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getNtrcaCertName()));
                    instEmplRecruitInfoResult.setNtrcaCertName(instEmplRecruitInfoResult.getNtrcaCertName().substring(0, (instEmplRecruitInfoResult.getNtrcaCertName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getDgApproveFileName() != null && instEmplRecruitInfoResult.getDgApproveFileName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getDgApproveFileName()) != null) {
                    instEmplRecruitInfoResult.setDgApproveFile(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getDgApproveFileName()));
                    instEmplRecruitInfoResult.setDgApproveFileName(instEmplRecruitInfoResult.getDgApproveFileName().substring(0, (instEmplRecruitInfoResult.getDgApproveFileName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getConfirmLetterName() != null && instEmplRecruitInfoResult.getConfirmLetterName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getConfirmLetterName()) != null) {
                    instEmplRecruitInfoResult.setConfirmLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getConfirmLetterName()));
                    instEmplRecruitInfoResult.setConfirmLetterName(instEmplRecruitInfoResult.getConfirmLetterName().substring(0, (instEmplRecruitInfoResult.getConfirmLetterName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getAttachmentName() != null && instEmplRecruitInfoResult.getAttachmentName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAttachmentName()) != null) {
                    instEmplRecruitInfoResult.setAttachment(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAttachmentName()));
                    instEmplRecruitInfoResult.setAttachmentName(instEmplRecruitInfoResult.getAttachmentName().substring(0, (instEmplRecruitInfoResult.getAttachmentName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getAppointAprvAtchName() != null && instEmplRecruitInfoResult.getAppointAprvAtchName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppointAprvAtchName()) != null) {
                    instEmplRecruitInfoResult.setAppointAprvAtch(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppointAprvAtchName()));
                    instEmplRecruitInfoResult.setAppointAprvAtchName(instEmplRecruitInfoResult.getAppointAprvAtchName().substring(0, (instEmplRecruitInfoResult.getAppointAprvAtchName().length() - 17)));
                }
                if (instEmplRecruitInfoResult.getMcMemoAttachName() != null && instEmplRecruitInfoResult.getMcMemoAttachName().length() > 0 && AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getMcMemoAttachName()) != null) {
                    instEmplRecruitInfoResult.setMcMemoAttach(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getMcMemoAttachName()));
                    instEmplRecruitInfoResult.setMcMemoAttachName(instEmplRecruitInfoResult.getMcMemoAttachName().substring(0, (instEmplRecruitInfoResult.getMcMemoAttachName().length() - 17)));
                }
            }

            instEmployeeInfores.setInstEmplRecruitInfo(instEmplRecruitInfoResult);
            //instEmployeeInfores.setInstEmplRecruitInfo(instEmplRecruitInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            //instEmployeeInfores.setInstEmplBankInfo(instEmplBankInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplBankInfo(instEmplBankInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmpSpouseInfo(instEmpSpouseInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));

            List<InstEmplExperience> instEmplExperiences = instEmplExperienceRepository.findByInstEmployeeId(instEmployeeresult.getId());
            for(InstEmplExperience instEmplExperience : instEmplExperiences){

                if(instEmplExperience.getReleaseFileName()!=null && AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmplExperience.getReleaseFileName()) != null){
                    instEmplExperience.setReleaseFile(AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmplExperience.getReleaseFileName()));
                    instEmplExperience.setReleaseFileName(instEmplExperience.getReleaseFileName().substring(0, (instEmplExperience.getReleaseFileName().length() - 17)));
                }

                if(instEmplExperience.getAttachmentName()!=null && AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmplExperience.getAttachmentName()) != null){
                    instEmplExperience.setAttachment(AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmplExperience.getAttachmentName()));
                    instEmplExperience.setAttachmentName(instEmplExperience.getAttachmentName().substring(0, (instEmplExperience.getAttachmentName().length() - 17)));
                }

                if(instEmplExperience.getResignFileName()!=null && AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmplExperience.getResignFileName()) != null){
                    instEmplExperience.setResignFile(AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmplExperience.getResignFileName()));
                    instEmplExperience.setResignFileName(instEmplExperience.getResignFileName().substring(0, (instEmplExperience.getResignFileName().length() - 17)));
                }
            }
            instEmployeeInfores.setInstEmplExperiences(instEmplExperiences);

            instEmployeeInfores.setSalaryMap(rptJdbcDao.generateSalary());
        }
        return Optional.ofNullable(instEmployeeInfores)
            .map(instEmployeeInfo -> new ResponseEntity<>(
                    instEmployeeInfo,
                    HttpStatus.OK)
            )
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



    /*
    * GET  /instEmployees/:id -> get the "id" instEmployee.
    */
    @RequestMapping(value = "/instEmployees/indexNo/{indexNo}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployeeInfo> getInstEmployeeByIndexNo(@PathVariable String indexNo) throws Exception{
        log.debug("REST request to get InstEmployee : {}", indexNo);
        InstEmployee instEmployeeresult=instEmployeeRepository.findOneByEmployeeIndexNo(indexNo);
        InstEmployeeInfo instEmployeeInfores= new InstEmployeeInfo();
//        String filepath="/backup/teacher_info/";

        if(instEmployeeresult.getImageName() !=null) {
            instEmployeeresult.setImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getImageName()));
            instEmployeeresult.setImageName(instEmployeeresult.getImageName().substring(0, (instEmployeeresult.getImageName().length() - 17)));
        }
        if(instEmployeeresult.getNidImageName() !=null) {
            instEmployeeresult.setNidImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getNidImageName()));
            instEmployeeresult.setNidImageName(instEmployeeresult.getNidImageName().substring(0, (instEmployeeresult.getNidImageName().length() - 17)));
        }
        if(instEmployeeresult.getImageName() !=null) {
            instEmployeeresult.setBirthCertImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getBirthCertNoName()));
            instEmployeeresult.setBirthCertNoName(instEmployeeresult.getBirthCertNoName().substring(0, (instEmployeeresult.getBirthCertNoName().length() - 17)));
        }

        if(instEmployeeresult !=null){
            instEmployeeInfores.setInstEmployee(instEmployeeresult);
            instEmployeeInfores.setInstEmpAddress(instEmpAddressRepository.findOneByInstEmployeeId(instEmployeeresult.getId()));
            //instEmployeeInfores.setInstEmpEduQualis(instEmpEduQualiRepository.findListByInstEmployeeId(instEmployeeresult.getId()));
            log.debug("REST request to get InstEmployee : {}");
            List<InstEmpEduQuali> instEmpEduQualilist=null;
            instEmpEduQualilist=instEmpEduQualiRepository.findListByInstEmployeeId(instEmployeeresult.getId());
            for(InstEmpEduQuali instEmpEduQuali:instEmpEduQualilist){
                if(instEmpEduQuali.getCertificateCopyName()!=null){
                instEmpEduQuali.setCertificateCopy(AttachmentUtil.retriveAttachment("/backup/teacher_info/",instEmpEduQuali.getCertificateCopyName()));
                instEmpEduQuali.setCertificateCopyName(instEmpEduQuali.getCertificateCopyName().substring(0, (instEmpEduQuali.getCertificateCopyName().length() - 17)));
                }
            }

            // Commented out by yzaman 19th April 2016
            //instEmployeeInfores.setInstEmpEduQualis(rptJdbcDao.findListByInstEmployeeId(instEmployeeresult.getId()));
            //instEmployeeInfores.setInstEmpEduQualisList(instEmpEduQualilist);

            List<InstEmplTraining> instEmpTraininglist=instEmplTrainingRepository.findInstEmplTrainingsByEmployeeId(instEmployeeresult.getId());

            for(InstEmplTraining instEmplTraining:instEmpTraininglist){
                if(instEmplTraining.getAttachmentName()!=null){
                instEmplTraining.setAttachment(AttachmentUtil.retriveAttachment("/backup/teacher_info/", instEmplTraining.getAttachmentName()));
                instEmplTraining.setAttachmentName(instEmplTraining.getAttachmentName().substring(0, (instEmplTraining.getAttachmentName().length() - 17)));
                }
            }

            instEmployeeInfores.setInstEmplTrainings(instEmpTraininglist);
            //instEmployeeInfores.setInstEmplTrainings(instEmplTrainingRepository.findInstEmplTrainingsByEmployeeId(instEmployeeresult.getId()));

            InstEmplRecruitInfo instEmplRecruitInfoResult=instEmplRecruitInfoRepository.findByInstEmployeeId(instEmployeeresult.getId());

            if(instEmplRecruitInfoResult !=null) {
                if (instEmplRecruitInfoResult.getAppoinmentLtrName() != null && instEmplRecruitInfoResult.getAppoinmentLtrName().length() > 0) {
                    instEmplRecruitInfoResult.setAppoinmentLtr(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getAppoinmentLtrName()));
                    instEmplRecruitInfoResult.setAppoinmentLtrName(instEmplRecruitInfoResult.getAppoinmentLtrName().substring(0, (instEmplRecruitInfoResult.getAppoinmentLtrName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getJoiningLetterName() != null && instEmplRecruitInfoResult.getJoiningLetterName().length() > 0) {
                    instEmplRecruitInfoResult.setJoiningLetter(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getJoiningLetterName()));
                    instEmplRecruitInfoResult.setJoiningLetterName(instEmplRecruitInfoResult.getJoiningLetterName().substring(0, (instEmplRecruitInfoResult.getJoiningLetterName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecruitResultName() != null && instEmplRecruitInfoResult.getRecruitResultName().length() > 0) {
                    instEmplRecruitInfoResult.setRecruitResult(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitResultName()));
                    instEmplRecruitInfoResult.setRecruitResultName(instEmplRecruitInfoResult.getRecruitResultName().substring(0, (instEmplRecruitInfoResult.getRecruitResultName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecruitNewsLocalName() != null && instEmplRecruitInfoResult.getRecruitNewsLocalName().length() > 0) {
                    instEmplRecruitInfoResult.setRecruitNewsLocal(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsLocalName()));
                    instEmplRecruitInfoResult.setRecruitNewsLocalName(instEmplRecruitInfoResult.getRecruitNewsLocalName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsLocalName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecruitNewsDailyName() != null && instEmplRecruitInfoResult.getRecruitNewsDailyName().length() > 0) {
                    instEmplRecruitInfoResult.setRecruitNewsDaily(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecruitNewsDailyName()));
                    instEmplRecruitInfoResult.setRecruitNewsDailyName(instEmplRecruitInfoResult.getRecruitNewsDailyName().substring(0, (instEmplRecruitInfoResult.getRecruitNewsDailyName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getGoResulatonName() != null && instEmplRecruitInfoResult.getGoResulatonName().length() > 0) {
                    instEmplRecruitInfoResult.setGoResulaton(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getGoResulatonName()));
                    instEmplRecruitInfoResult.setGoResulatonName(instEmplRecruitInfoResult.getGoResulatonName().substring(0, (instEmplRecruitInfoResult.getGoResulatonName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getCommitteeName() != null && instEmplRecruitInfoResult.getCommitteeName().length() > 0) {
                    instEmplRecruitInfoResult.setCommittee(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getCommitteeName()));
                    instEmplRecruitInfoResult.setCommitteeName(instEmplRecruitInfoResult.getCommitteeName().substring(0, (instEmplRecruitInfoResult.getCommitteeName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getRecommendationName() != null && instEmplRecruitInfoResult.getRecommendationName().length() > 0) {
                    instEmplRecruitInfoResult.setRecommendation(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getRecommendationName()));
                    instEmplRecruitInfoResult.setRecommendationName(instEmplRecruitInfoResult.getRecommendationName().substring(0, (instEmplRecruitInfoResult.getRecommendationName().length() - 17)));
                }

                if (instEmplRecruitInfoResult.getSanctionName() != null && instEmplRecruitInfoResult.getSanctionName().length() > 0) {
                    instEmplRecruitInfoResult.setSanction(AttachmentUtil.retriveAttachment(filepath, instEmplRecruitInfoResult.getSanctionName()));
                    instEmplRecruitInfoResult.setSanctionName(instEmplRecruitInfoResult.getSanctionName().substring(0, (instEmplRecruitInfoResult.getSanctionName().length() - 17)));
                }
            }

            instEmployeeInfores.setInstEmplRecruitInfo(instEmplRecruitInfoResult);
            //instEmployeeInfores.setInstEmplRecruitInfo(instEmplRecruitInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplBankInfo(instEmplBankInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplExperiences(instEmplExperienceRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setSalaryMap(rptJdbcDao.generateSalary());
        }
        return Optional.ofNullable(instEmployeeInfores)
            .map(instEmployeeInfo -> new ResponseEntity<>(
                    instEmployeeInfo,
                    HttpStatus.OK)
            )
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    /*
   * GET  /instEmployees/:id -> get the "id" instEmployee.
   */




    @RequestMapping(value = "/instEmployeesAllForEmployee/code/{code:.+}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployeeInfo> getInstEmployeByCodeForEmployeeApprove(@PathVariable String code) throws Exception{
        log.debug("REST request to get InstEmployee : {}", code);
        InstEmployee instEmployeeresult=instEmployeeRepository.findOneByEmployeeCode(code);

//        String filepath="/backup/teacher_info/";

        instEmployeeresult.setImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getImageName() ));
        instEmployeeresult.setBirthCertImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getBirthCertNoName() ));
        instEmployeeresult.setNidImage(AttachmentUtil.retriveAttachment(filepath, instEmployeeresult.getNidImageName() ));

        instEmployeeresult.setImageName(instEmployeeresult.getImageName().substring(0, (instEmployeeresult.getImageName().length()-21)));
        instEmployeeresult.setNidImageName(instEmployeeresult.getNidImageName().substring(0, (instEmployeeresult.getNidImageName().length()-21)));
        instEmployeeresult.setBirthCertNoName(instEmployeeresult.getBirthCertNoName().substring(0, (instEmployeeresult.getBirthCertNoName().length() - 21)));

        InstEmployeeInfo instEmployeeInfores= new InstEmployeeInfo();

        if(instEmployeeresult !=null){
            instEmployeeInfores.setInstEmployee(instEmployeeresult);
            instEmployeeInfores.setInstEmpAddress(instEmpAddressRepository.findOneByInstEmployeeId(instEmployeeresult.getId()));
            //instEmployeeInfores.setInstEmpEduQualis(instEmpEduQualiRepository.findListByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplTrainings(instEmplTrainingRepository.findInstEmplTrainingsByEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplRecruitInfo(instEmplRecruitInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplBankInfo(instEmplBankInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));
            instEmployeeInfores.setInstEmplExperiences(instEmplExperienceRepository.findByInstEmployeeId(instEmployeeresult.getId()));

            // Commented out by yzaman 19th April 2016
            //instEmployeeInfores.setInstEmpEduQualis(rptJdbcDao.findListByInstEmployeeId(instEmployeeresult.getId()));
            //instEmployeeInfores.setInstEmpSpouseInfo(instEmpSpouseInfoRepository.findByInstEmployeeId(instEmployeeresult.getId()));

            instEmployeeInfores.setSalaryMap(rptJdbcDao.generateSalary());
        }
        return Optional.ofNullable(instEmployeeInfores)
            .map(instEmployeeInfo -> new ResponseEntity<>(
                    instEmployeeInfo,
                    HttpStatus.OK)
            )
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    /*
   * GET  /instEmployees/:id -> get the "id" instEmployee.
   */
    @RequestMapping(value = "/instEmployees/institute/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> getInstEmployeeByInstitute(Pageable pageable,@PathVariable Long id)  throws URISyntaxException {
        log.debug("REST request to get InstEmployee : {}", id);

        return Optional.ofNullable(instEmployeeRepository.findOneByInstitueId(id))
            .map(instEmployeelist -> new ResponseEntity<>(
                instEmployeelist,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

     /*
   * GET  /instEmployees/:id -> get the "id" instEmployee.
   */
    @RequestMapping(value = "/instEmployees/jpadmin/institute/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getJpAdminsByInstitute(Pageable pageable,@PathVariable Long id)  throws URISyntaxException {
        log.debug("REST request to get jpadmin of institute : {}", id);

        return instEmployeeRepository.findAllJpAdminOfInstitute(id);
    }


    /**
     * DELETE  /instEmployees/:id -> delete the "id" instEmployee.
     */
    @RequestMapping(value = "/instEmployees/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstEmployee(@PathVariable Long id) {
        log.debug("REST request to delete InstEmployee : {}", id);
        instEmployeeRepository.delete(id);
        instEmployeeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("instEmployee", id.toString())).build();
    }

    /**
     * SEARCH  /_search/instEmployees/:query -> search for the instEmployee corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/instEmployees/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> searchInstEmployees(@PathVariable String query) {
        return StreamSupport
            .stream(instEmployeeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET /account/sessions -> get the current open sessions.
     */
    @RequestMapping(value = "/instEmployee/checkDuplicateCode/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> checkLogin( @RequestParam String value) {
        Optional<User> user = userRepository.findOneByLogin(value);
        Optional<InstEmployee> instEmployee = instEmployeeRepository.findBycode(value);
        log.debug("user on check for----"+value);
        log.debug("user on check login----"+Optional.empty().equals(user));
        Map map =new HashMap();
        map.put("value",value);
        if(Optional.empty().equals(user) || Optional.empty().equals(instEmployee)){
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }else{
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/instEmployee/checkDuplicateEmail/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> checkEmail(@RequestParam String value) {

        Optional<User> user = userRepository.findOneByEmail(value);
        log.debug("user on check email----"+Optional.empty().equals(user));

        Optional<InstEmployee> instGenInfo = instEmployeeRepository.findByEmail(value);

        Map map =new HashMap();

        map.put("value", value);

        if(Optional.empty().equals(user) || Optional.empty().equals(instGenInfo)){
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);

        }else{
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }

    }
    @RequestMapping(value = "/instEmployees/pendingList/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getPendingEmployeesByINstitute(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        return instEmployeeRepository.findPendingListByInstitueId(id);
    }
    @RequestMapping(value = "/instEmployees/declinedList/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getDeclinedEmployeesByINstitute(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        return instEmployeeRepository.findDeclinedListByInstitueId(id);
    }


    @RequestMapping(value = "/instEmployees/approveList/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getApprovedEmployeesByINstitute(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTITUTE)){
            return instEmployeeRepository.findApprovedListByInstitueId(id);
        }else if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTEMP) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ROLE_HRM_USER_NAME)){

            return instEmployeeRepository.findApprovedInstByDeptHead(id,instEmployeeRepository.findCurrentOne().getHrdepartmentSetup().getId());
        }else
            return null;

    }

    @RequestMapping(value = "/instEmployees/govtapproveList/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getGovtApprovedEmployeesByINstitute(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        return instEmployeeRepository.findGovtApprovedListByInstitueId(id);
    }


    @RequestMapping(value = "/instEmployees/findteacherListByDeptHead/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getTeacherListByDeptHead(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        return instEmployeeRepository.findteacherListByDeptHead(id);
    }


    @RequestMapping(value = "/instEmployees/findTeacherListByInstitute",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getTeacherListByInstitute()
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>");
        return instEmployeeRepository.findteacherListByInstitute();
    }

    @RequestMapping(value = "/instEmployees/findApprovedTeacherList",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getApprovedTeacherList()
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>");
        return instEmployeeRepository.findApprovedTeacherList();
    }

    @RequestMapping(value = "/instEmployees/findApprovedStaffList",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getApprovedStaffList()
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>");
        return instEmployeeRepository.findApprovedStaffList();
    }

    @RequestMapping(value = "/instEmployees/findstaffListByDeptHead/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getStaffListByDeptHead(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        return instEmployeeRepository.findstaffListByDeptHead(id);
    }

    @RequestMapping(value = "/instEmployees/findStaffsListByInstitute",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getStaffListByInstitute()
        throws URISyntaxException {
        log.debug("Institute employeee for institute>>>>>>>>>>>>>>>");
        return instEmployeeRepository.findstaffListByInstitute();
    }


    @RequestMapping(value = "/instEmployees/nongovtapproveList/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getNonGovtApprovedEmployeesByINstitute(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);
        return instEmployeeRepository.findNonGovtApprovedListByInstitueId(id);
    }

    //find all mpo enlisted employee of current institute
    @RequestMapping(value = "/instEmployees/current/mpoEnlisted",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> getMpoEnlistedEmployeesOfCurrent()
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>");
        return Optional.ofNullable(instEmployeeRepository.findListByInstitueIdAndMpoStatus(instituteRepository.findOneByUserIsCurrentUser().getId(),EmployeeMpoApplicationStatus.APPROVED.getCode()))
            .map(employeeLoanRequisitionForm -> new ResponseEntity<>(
                employeeLoanRequisitionForm,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @RequestMapping(value = "/instEmployees/staffApproveList/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmployee> getStasffApprovedEmployeesByINstitute(@PathVariable Long id)
        throws URISyntaxException {
        log.debug("Inst titute employeee for institute>>>>>>>>>>>>>>>"+id);

        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTITUTE)){
            return instEmployeeRepository.findApprovedStaffListByInstitueId(id);
        }else if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTEMP) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ROLE_HRM_USER_NAME)){

            return instEmployeeRepository.findApprovedStaffByDeptHead(id,instEmployeeRepository.findCurrentOne().getHrdepartmentSetup().getId());
        }else
            return null;
    }
    /**
     * PUT  /mpoApplications -> Updates an existing mpoApplication.
     */
    @RequestMapping(value = "/instEmployees/decline/{id}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> declineEmplyee(@PathVariable Long id,@RequestBody String cause)
        throws URISyntaxException {
        InstEmployee instEmployee=null;
        log.debug("REST request to update instEmployee : {}--------", id);
        if(id>0){
            instEmployee=instEmployeeRepository.findOne(id);
        }else{
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee cannot decline ").body(null);
        }
        log.debug("REST request to update instEmployee : {}--------", instEmployee);
        log.debug("REST request to update cause : {}--------", cause);
        if (instEmployee == null) {
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee cannot decline ").body(null);
        }else{
            instEmployee.setRemarks(cause);
            instEmployee.setStatus(InstEmployeeDocumentStatus.DECLINED.getCode());
            InstEmployee result= instEmployeeRepository.save(instEmployee);
            instEmployeeSearchRepository.save(result);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("result", result.getId().toString()))
                .body(null);
        }
    }
    /**
     * PUT  /instEmployees -> Updates an existing mpoApplication.
     */
    @RequestMapping(value = "/instEmployees/approve/{id}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> approveEmployee(@PathVariable Long id)
        throws URISyntaxException {
        InstEmployee instEmployee=null;
        log.debug("REST request to update instEmployee : {}--------", id);
        if(id>0){
            instEmployee=instEmployeeRepository.findOne(id);
        }else{
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee cannot Approved ").body(null);
        }
        log.debug("REST request to update instEmployee : {}--------", instEmployee);
        if (instEmployee == null) {
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee cannot Approved ").body(null);
        }else{

            if(instEmployee.getIsJPAdmin()){
                User user = userService.getUserWithAuthoritiesByLogin(instEmployee.getCode()).get();

                Authority authority = authorityRepository.findOne(AuthoritiesConstants.JPADMIN);
                boolean addAuthority=true;
                if(authority==null){
                    authority=new Authority();
                    authority.setName(AuthoritiesConstants.JPADMIN);
                    authorityRepository.save(authority);
                }

                user.getAuthorities().add(authority);

                user=userRepository.save(user);
                userSearchRepository.save(user);
            }


            instEmployee.setStatus(InstEmployeeDocumentStatus.APPROVED.getCode());
            InstEmployee result= instEmployeeRepository.save(instEmployee);
            instEmployeeSearchRepository.save(result);

            NotificationStep notificationSteps = new NotificationStep();
            notificationSteps.setNotification("You information has approved by institute");
            notificationSteps.setStatus(true);
            notificationSteps.setUrls("employeeInfo.personalInfo");
            notificationSteps.setUserId(instEmployee.getUser().getId());
            notificationStepRepository.save(notificationSteps);

            log.debug("ststud after save------------------------------------"+result.getStatus());
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("result", result.getId().toString()))
                .body(null);
        }
    }
    /**
     * PUT  instEmployees/eligibleForMpo -> Updates an existing mpoApplication.
     */
    @RequestMapping(value = "/instEmployees/eligibleForMpo/{id}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> eligibleEmployeeForMpo(@PathVariable Long id)
        throws URISyntaxException {
        InstEmployee instEmployee=null;
        log.debug("REST request to update instEmployee : {}--------", id);
        if(id>0){
            instEmployee=instEmployeeRepository.findOne(id);
        }else{
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee failed to eligible ").body(null);
        }
        log.debug("REST request to update instEmployee : {}--------", instEmployee);
        if (instEmployee == null) {
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee failed to eligible ").body(null);
        }else{
            instEmployee.setMpoAppStatus(EmployeeMpoApplicationStatus.ELIGIBLE.getCode());
            InstEmployee result= instEmployeeRepository.save(instEmployee);
            instEmployeeSearchRepository.save(result);

            NotificationStep notificationSteps = new NotificationStep();
            notificationSteps.setNotification("You are eligible for mpo application");
            notificationSteps.setStatus(true);
            notificationSteps.setUrls("mpo.application");
            notificationSteps.setUserId(instEmployee.getUser().getId());
            notificationStepRepository.save(notificationSteps);

            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("result", result.getId().toString()))
                .body(null);
        }
    }

    @RequestMapping(value = "/instEmployees/activeInactive/{status}/{code}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> eligibleEmployeeForMpo(@PathVariable Integer status,@PathVariable String code)
        throws URISyntaxException {
        InstEmployee instEmployee=null;
        log.debug("REST request to update instEmployee : {}--------", code);
        if(code!=null){
            instEmployee=instEmployeeRepository.findBycodeOne(code);
        }else{
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee failed to Update ").body(null);
        }
        log.debug("REST request to update instEmployee : {}--------", instEmployee);
        if (instEmployee == null) {
            return ResponseEntity.badRequest().header("Failure", "A new inst Employee failed to Update ").body(null);
        }else{
            if (status!=null){
                instEmployee.setStatus(status);
                InstEmployee result= instEmployeeRepository.save(instEmployee);

                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("result", result.getId().toString()))
                    .body(null);
//                instEmployeeSearchRepository.save(result);
            }
        }

           return null;
        }

    //get empl info by code
    @RequestMapping(value = "/instEmployees/getEmplInfoByCode/{code}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmployee> getEmplInfoByCode(@PathVariable String code)
        throws URISyntaxException {
        return Optional.ofNullable(instEmployeeRepository.findEmplInfoBycode(code))
            .map(dlBookInfo -> new ResponseEntity<>(
                dlBookInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/instEmployees/findInstituteEmpByInstitute/{instituteID}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> findInstituteEmpByInstitute(@PathVariable Long instituteID)
        throws URISyntaxException {
        return Optional.ofNullable(instEmployeeRepository.findInstituteEmpByInstitute(instituteID))
            .map(dlBookInfo -> new ResponseEntity<>(
                dlBookInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



    @RequestMapping(value = "/instEmployees/findActiveInactiveInstituteEmpByStatus",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> findActiveInactiveInstituteEmpByStatus()
        throws URISyntaxException {
        return Optional.ofNullable(instEmployeeRepository.findActiveInactiveInstituteEmpByStatus())
            .map(dlBookInfo -> new ResponseEntity<>(
                dlBookInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/instEmployees/getHeadOfInstituteByHeadStatus/{headStatus}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> getAllHeadOfInstituteByHeadStatus(@PathVariable Boolean headStatus)
        throws URISyntaxException {
        List<InstEmployee> instEmployeeList = new ArrayList<InstEmployee>();
        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)){
            instEmployeeList.addAll(instEmployeeRepository.findByHeadStatus(headStatus));
        }else if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.INSTITUTE)){
            instEmployeeList.addAll(instEmployeeRepository.findByHeadStatusAndInstitute(headStatus,instituteRepository.findOneByUserIsCurrentUserID()));
        }else{
            instEmployeeList = null;
        }

        return Optional.ofNullable(instEmployeeList)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @RequestMapping(value = "/instEmployees/getAllInstEmpByDept/{deptSetupId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> getAllInstEmployeeByDept(@PathVariable Long deptSetupId)
        throws URISyntaxException {
        log.debug("REST request to getAllInstEmployeeByDept by deptSetupId:{}"+deptSetupId);
        List<InstEmployee> instEmployeeList = new ArrayList<InstEmployee>();
        instEmployeeList.addAll(instEmployeeRepository.findAllByDepartmentSetupId(deptSetupId));

        return Optional.ofNullable(instEmployeeList)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @RequestMapping(value = "/instEmployees/assignNewHeadOfDept",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> assignNewHeadOfDept(@Valid @RequestBody InstEmployee instEmployee) throws URISyntaxException, Exception {
        log.debug("REST request to Assign New head of dept : {}", instEmployee);
        Map map = new HashMap();
        HrDepartmentHeadInfo headInfo;
        if (instEmployee.getId() == null) {
            map.put("Assign","Failed");
        }
        if(instEmployee.getInstitute().getType() == InstituteType.Government){

            if(instEmployee.getIsHeadOfDept().equals("YES")){
                log.debug("Already Assigned");
                map.put("Assign","AlreadyAssigned");
            }else {
                InstEmployee currentHeadOfDept= instEmployeeRepository.findDeptHeadByDeptAndInstitute(instEmployee.getHrdepartmentSetup().getId(),instEmployee.getInstitute().getId());
                if(currentHeadOfDept != null) {
                    currentHeadOfDept.setIsHeadOfDept("NO");
                    instEmployeeRepository.save(currentHeadOfDept);
                }
                if(instEmployee.getHrdepartmentSetup() != null){
                    instEmployee.setIsHeadOfDept("YES");
                    InstEmployee result = instEmployeeRepository.save(instEmployee);

                    hrDepartmentHeadInfoRepository.updateAllDepartmentHeadActiveStatus(result.getHrdepartmentSetup().getId(),false);

                    HrEmployeeInfo hrEmployeeInfo =  hrEmployeeInfoRepository.findByInstEmployee(result);
                    if(hrDepartmentHeadInfoRepository.findByHeadInfo(hrEmployeeInfo) == null){
                        headInfo = new HrDepartmentHeadInfo();
                    }else {
                        headInfo = hrDepartmentHeadInfoRepository.findByHeadInfo(hrEmployeeInfo);
                    }
                    headInfo.setHeadInfo(hrEmployeeInfo);
                    headInfo.setDepartmentInfo(result.getHrdepartmentSetup());
                    headInfo.setActiveHead(true);
                    headInfo.setActiveStatus(true);
                    headInfo.setCreateBy(SecurityUtils.getCurrentUserId());
                    headInfo.setCreateDate(LocalDate.now());
                    headInfo.setJoinDate(hrEmployeeInfo.getDateOfJoining());
                    hrDepartmentHeadInfoRepository.save(headInfo);
                    map.put("Assign","Assigned");
                }
            }
        }
        return new ResponseEntity<Map>(map,HttpStatus.OK);
    }


    /**
     * GET  /hrEmployeeInfos/findByUserTyping/:emplId-> get employee by firstName.
     */
    @RequestMapping(value = "/instEmployee/findByUserTyping/{firstName}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> findByUserTyping(@PathVariable String firstName) throws Exception{
        log.debug("REST request to findByUserTyping List Name: {}", firstName);
        List<InstEmployee> instEmployeeInfos =  instEmployeeRepository.findByNameStartingWithIgnoreCase(firstName);

        return Optional.ofNullable(instEmployeeInfos)
            .map(instEmployeeInfo -> new ResponseEntity<>(
                instEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/instEmployee/findByInstituteAndDesignation/{instituteId}/{desigName}/{mpoStatus}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmployee>> findInstituteAndDesignation(@PathVariable Long instituteId,@PathVariable String desigName,@PathVariable Boolean mpoStatus) throws Exception{
        log.debug("REST request to findInstituteAndDesignation  Name: {}", instituteId);
        List<InstEmployee> instEmployeeInfos =  instEmployeeRepository.findByInstituteAndDesignationName(instituteId,desigName,mpoStatus);

        return Optional.ofNullable(instEmployeeInfos)
            .map(instEmployeeInfo -> new ResponseEntity<>(
                instEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
