package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.designationType;
import gov.step.app.repository.*;
import gov.step.app.repository.search.HrEmployeeInfoSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.HrmConversionService;
import gov.step.app.service.MailService;
import gov.step.app.service.constnt.HRMManagementConstant;
import gov.step.app.service.util.MiscFileInfo;
import gov.step.app.service.util.MiscFileUtilities;
import gov.step.app.service.util.MiscUtilities;
import gov.step.app.web.rest.dto.HrmApprovalDto;
import gov.step.app.web.rest.dto.HrmDashboardDto;
import gov.step.app.web.rest.dto.MiscParamDto;
import gov.step.app.web.rest.jdbc.dao.EntityManagerService;
import gov.step.app.web.rest.jdbc.dao.HRMJdbcDao;
import gov.step.app.web.rest.util.AttachmentUtil;
import gov.step.app.web.rest.util.DateResource;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.springframework.data.jpa.domain.Specifications.where;

/**
 * REST controller for managing HrEmployeeInfo.
 */
@RestController
@RequestMapping("/api")
public class HrEmployeeInfoResource {

    private final Logger log = LoggerFactory.getLogger(HrEmployeeInfoResource.class);

    @Inject
    private HrEmployeeInfoRepository hrEmployeeInfoRepository;

    @Inject
    private HrEmployeeInfoSearchRepository hrEmployeeInfoSearchRepository;

    @Inject
    private HrEmployeeInfoLogRepository hrEmployeeInfoLogRepository;

    MiscFileUtilities fileUtils = new MiscFileUtilities();

    MiscUtilities miscUtils = new MiscUtilities();

    @Inject
    private HrmConversionService conversionService;

    @Inject
    private HRMJdbcDao hrmJdbcDao;

    @Inject
    private MailService mailService;

    @Inject
    private HrDesignationSetupRepository hrDesignationSetupRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private HrDepartmentHeadInfoRepository hrDepartmentHeadInfoRepository;

    @Inject
    private InstituteRepository instituteRepository;
    @Inject
    private EntityManagerService entityManagerService;

    @Inject
    private HrEmploymentInfoRepository hrEmploymentInfoRepository;

    DateResource dateResrc = new DateResource();

    /**
     * POST  /hrEmployeeInfos -> Create a new hrEmployeeInfo.
     */
    @RequestMapping(value = "/hrEmployeeInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> createHrEmployeeInfo(@Valid @RequestBody HrEmployeeInfo hrEmployeeInfo) throws URISyntaxException {
        log.debug("REST request to save HrEmployeeInfo : {}", hrEmployeeInfo);

        if (hrEmployeeInfo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("hrEmployeeInfo", "idexists", "A new hrEmployeeInfo cannot already have an ID")).body(null);
        }

        String employeeCode = "";
        HrEmployeeInfo tempEmpInfo = null;
        boolean isEmployeeIdUnique = false;
        do
        {
            String strYear      = Integer.toString(hrEmployeeInfo.getBirthDate().getYear());
            String year2D       = strYear.substring(strYear.length() - 2);
            int instTypeCode    = 1; //Govt Institute
            employeeCode        = year2D+""+instTypeCode+""+hrEmployeeInfo.getDistrict().getCode()+""+((int)(Math.random()*9000)+1000);

            tempEmpInfo = hrEmployeeInfoRepository.findByEmployeeId(employeeCode);
            if( tempEmpInfo != null)
            {
                isEmployeeIdUnique = false;
            }
            else
            {
                isEmployeeIdUnique = true;
            }
            log.debug("employeeCode: "+employeeCode+", isUnique: "+isEmployeeIdUnique);

        } while (isEmployeeIdUnique == false);

        hrEmployeeInfo.setEmployeeId(employeeCode);
        hrEmployeeInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
        String tempPass = hrEmployeeInfo.getLogComments();
        hrEmployeeInfo.setLogComments("");
        HrEmployeeInfo result = hrEmployeeInfoRepository.save(hrEmployeeInfo);

        //Set Employment Information
        HrEmploymentInfo hrEmploymentInfo = new HrEmploymentInfo();
        hrEmploymentInfo.setEmployeeInfo(hrEmployeeInfo);
        hrEmploymentInfo.setOrganizationType(hrEmployeeInfo.getOrganizationType());
        hrEmploymentInfo.setDepartmentInfo(hrEmployeeInfo.getDepartmentInfo());
        hrEmploymentInfo.setDesignationInfo(hrEmployeeInfo.getDesignationInfo());
        hrEmploymentInfo.setCreateBy(SecurityUtils.getCurrentUserId());
        hrEmploymentInfo.setCreateDate(LocalDate.now());
        hrEmploymentInfo.setActiveStatus(true);
        hrEmploymentInfo.setDesignationInfo(hrEmploymentInfo.getDesignationInfo());
        hrEmploymentInfo.setInstitute(hrEmploymentInfo.getInstitute());
        hrEmploymentInfo.setWorkAreaDtl(hrEmployeeInfo.getWorkAreaDtl());
        hrEmploymentInfo.setEmployeeType(hrEmployeeInfo.getEmployementType());
        hrEmploymentInfo.setJoiningDate(hrEmployeeInfo.getDateOfJoining());
        hrEmploymentInfoRepository.save(hrEmploymentInfo);


//        hrEmployeeInfoSearchRepository.save(result);

        //Sent Confirmation Email to Employee Email.
        hrEmployeeInfo.setLogComments(tempPass);
        mailService.sendEmployeeBasicInfoEmail(hrEmployeeInfo);

        return ResponseEntity.created(new URI("/api/hrEmployeeInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("hrEmployeeInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hrEmployeeInfos -> Updates an existing hrEmployeeInfo.
     */
    @RequestMapping(value = "/hrEmployeeInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> updateHrEmployeeInfo(@Valid @RequestBody HrEmployeeInfo hrEmployeeInfo) throws URISyntaxException {
        log.debug("REST request to update HrEmployeeInfo : {}", hrEmployeeInfo);
        if (hrEmployeeInfo.getId() == null) {
            return createHrEmployeeInfo(hrEmployeeInfo);
        }

        HrEmployeeInfo tempEmpInfo = hrEmployeeInfoRepository.findOne(hrEmployeeInfo.getId());

        //Saving Employee Image Document to Dir.
        MiscFileInfo empPhoto = new MiscFileInfo();
        try{
            empPhoto.fileData(hrEmployeeInfo.getEmpPhoto())
                .fileName(hrEmployeeInfo.getImageName())
                .contentType(hrEmployeeInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            if (tempEmpInfo.getImageName() == null ||
                tempEmpInfo.getImageName().length() < HRMManagementConstant.MINIMUM_IMG_NAME_LENGTH) {
                empPhoto = fileUtils.saveFileAsByte(empPhoto);
            } else {
                empPhoto.fileName(tempEmpInfo.getImageName());
                empPhoto = fileUtils.updateFileAsByte(empPhoto);
            }
        }catch(Exception ex){
            log.error("Exception: UpdateHrInfo empPhoto "+ex.getMessage());
        }

        hrEmployeeInfo.setEmpPhoto(new byte[1]);
        hrEmployeeInfo.setImageName(empPhoto.fileName());

        //Saving Quota Centificate Image Document to Dir.
        MiscFileInfo quotaCert = new MiscFileInfo();
        try{
            quotaCert.fileData(hrEmployeeInfo.getQuotaCert())
                .fileName(hrEmployeeInfo.getQuotaCertName())
                .contentType(hrEmployeeInfo.getQuotaCertContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);

            if (tempEmpInfo.getQuotaCertName() == null ||
                tempEmpInfo.getQuotaCertName().length() < HRMManagementConstant.MINIMUM_IMG_NAME_LENGTH) {
                quotaCert = fileUtils.saveFileAsByte(quotaCert);
            } else {
                quotaCert.fileName(tempEmpInfo.getQuotaCertName());
                quotaCert = fileUtils.updateFileAsByte(quotaCert);
            }
        }catch(Exception ex){
            log.error("Exception: UpdateHrInfo quotaCertImg "+ex.getMessage());
        }

        hrEmployeeInfo.setQuotaCert(new byte[1]);
        hrEmployeeInfo.setQuotaCertName(quotaCert.fileName());

        try {
            // Add LOG info for Approval Purpose.
            HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
            HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(hrEmployeeInfo.getId());
            logInfo = conversionService.getLogFromSource(dbModelInfo);
            logInfo.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            logInfo = hrEmployeeInfoLogRepository.save(logInfo);
            hrEmployeeInfo.setLogId(logInfo.getId());
            hrEmployeeInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
        } catch (Exception ex) {
            log.error("EmployeeLog saving error:" + ex);
            hrEmployeeInfo.setLogId(-1l);
        }
        hrEmployeeInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);

        HrEmployeeInfo result = hrEmployeeInfoRepository.save(hrEmployeeInfo);
        hrEmployeeInfoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("hrEmployeeInfo", hrEmployeeInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hrEmployeeInfos -> get all the hrEmployeeInfos.
     */
    @RequestMapping(value = "/hrEmployeeInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> getAllHrEmployeeInfos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrEmployeeInfos");
        Page<HrEmployeeInfo> page = hrEmployeeInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrEmployeeInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/hrEmployeeInfos/getAll",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> getAllHrEmployeeInfosWithoutPagable(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrEmployeeInfos");
        List<HrEmployeeInfo> pageList = hrEmployeeInfoRepository.findAllByActiveStatus(true);
        return Optional.ofNullable(pageList)
            .map(hrEmployeeInfo -> new ResponseEntity<>(
                hrEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageList, "/api/hrEmployeeInfos");
//        return new ResponseEntity<>(pageList.getContent(), headers, HttpStatus.OK);
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrEmployeeInfos -> get all the hrEmployeeInfos.
     */
    @RequestMapping(value = "/hrEmployeeInfosDetail",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> getAllHrEmployeeInfosDetail(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrEmployeeInfos");
        Page<HrEmployeeInfo> page = hrEmployeeInfoRepository.findAll(pageable);
        for (HrEmployeeInfo hrEmployeeInfo : page.getContent()) {
            MiscFileInfo empPhoto = new MiscFileInfo();
            empPhoto.fileName(hrEmployeeInfo.getImageName())
                .contentType(hrEmployeeInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            hrEmployeeInfo.setEmpPhoto(empPhoto.fileData());
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrEmployeeInfosDetail");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrEmployeeInfos/:id -> get the "id" hrEmployeeInfo.
     */
    @RequestMapping(value = "/hrEmployeeInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> getHrEmployeeInfo(@PathVariable Long id) {
        log.debug("REST request to get HrEmployeeInfo : {}", id);
        HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(id);

//        if(hrEmployeeInfo.getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
//        {
//            hrEmployeeInfo.setInstCategory(hrEmployeeInfo.getInstEmployee().getInstitute().getInstCategory());
//        }

        MiscFileInfo empPhoto = new MiscFileInfo();
        try{
            empPhoto.fileName(hrEmployeeInfo.getImageName())
                .contentType(hrEmployeeInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            hrEmployeeInfo.setEmpPhoto(empPhoto.fileData());
        }
        catch(Exception ex){
            log.error("Exception: empPhoto "+ex.getMessage());
        }

        MiscFileInfo quotaCert = new MiscFileInfo();
        try{
            quotaCert.fileName(hrEmployeeInfo.getQuotaCertName())
                .contentType(hrEmployeeInfo.getQuotaCertContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            quotaCert = fileUtils.readFileAsByte(quotaCert);
            hrEmployeeInfo.setQuotaCert(quotaCert.fileData());

        }catch(Exception ex){
            log.error("Exception: quotaCert "+ex.getMessage());
        }

        MiscFileInfo releaseDoc = new MiscFileInfo();
        try{
            quotaCert.fileName(hrEmployeeInfo.getReleaseLetterDocName())
                .contentType(hrEmployeeInfo.getReleaseLetterContentType())
                .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);
            releaseDoc = fileUtils.readFileAsByte(releaseDoc);
            hrEmployeeInfo.setReleaseLetterDoc(releaseDoc.fileData());
        }catch(Exception ex){
            log.error("Exception: releaseDoc "+ex.getMessage());
        }

        return Optional.ofNullable(hrEmployeeInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /hrEmployeeInfos/:id -> delete the "id" hrEmployeeInfo.
     */
    @RequestMapping(value = "/hrEmployeeInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteHrEmployeeInfo(@PathVariable Long id) {
        log.debug("REST request to delete HrEmployeeInfo : {}", id);
        hrEmployeeInfoRepository.delete(id);
        hrEmployeeInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("hrEmployeeInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/hrEmployeeInfos/:query -> search for the hrEmployeeInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/hrEmployeeInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> searchHrEmployeeInfos(@PathVariable String query) {
        log.debug("REST request to search HrEmployeeInfos for query {}", query);
        return StreamSupport
            .stream(hrEmployeeInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /hrEmployeeInfos/my -> get my employee.
     */
    @RequestMapping(value = "/hrEmployeeInfos/my",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> getMyEmployee() {
        log.debug("REST request to search HrEmployeeInfos for current user");

        HrEmployeeInfo modelInfo = hrEmployeeInfoRepository.findOneByEmployeeUserIsCurrentUser();

//        if(modelInfo.getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
//        {
//            modelInfo.setInstCategory(modelInfo.getInstEmployee().getInstitute().getInstCategory());
//        }

        MiscFileInfo empPhoto = new MiscFileInfo();
        try{
            empPhoto.fileName(modelInfo.getImageName())
                .contentType(modelInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            modelInfo.setEmpPhoto(empPhoto.fileData());
        }
        catch(Exception ex){
            log.error("Exception: empPhoto "+ex.getMessage());
        }

        MiscFileInfo quotaCert = new MiscFileInfo();
        try{
            quotaCert.fileName(modelInfo.getQuotaCertName())
                .contentType(modelInfo.getQuotaCertContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            quotaCert = fileUtils.readFileAsByte(quotaCert);
            modelInfo.setQuotaCert(quotaCert.fileData());
        }
        catch(Exception ex){
            log.error("Exception: quotaCert "+ex.getMessage());
        }

        MiscFileInfo releaseDoc = new MiscFileInfo();
        try{
            quotaCert.fileName(modelInfo.getReleaseLetterDocName())
                .contentType(modelInfo.getReleaseLetterContentType())
                .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);
            releaseDoc = fileUtils.readFileAsByte(releaseDoc);
            modelInfo.setReleaseLetterDoc(releaseDoc.fileData());
        }catch(Exception ex){
            log.error("Exception: releaseDoc "+ex.getMessage());
        }

        return Optional.ofNullable(modelInfo)
            .map(employee -> new ResponseEntity<>(
                employee,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /hrEmployeeInfos/newempid -> get my newempid.
     */
    @RequestMapping(value = "/hrEmployeeInfos/newempid",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> getUniqueEmpId() {
        log.debug("REST new Employee ID hrEmployeeInfos for new user");

        String newEmpId = miscUtils.getUniqueEmployeeId();

        return new ResponseEntity<String>(newEmpId, HttpStatus.OK);
    }

    @RequestMapping(value = "/hrEmployeeInfos/checknid/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> classByTypeCode(@RequestParam String value) {
        Optional<HrEmployeeInfo> modelInfo = hrEmployeeInfoRepository.findOneByNationalId(value.toLowerCase());
        log.debug("hrEmployeeInfos by code: " + value + ", Stat: " + Optional.empty().equals(modelInfo));
        Map map = new HashMap();
        map.put("value", value);
        if (Optional.empty().equals(modelInfo)) {
            map.put("isValid", true);
            return new ResponseEntity<Map>(map, HttpStatus.OK);
        } else {
            map.put("isValid", false);
            return new ResponseEntity<Map>(map, HttpStatus.OK);
        }
    }

    /**
     * GET  /hrEmployeeInfosApprover/ -> process the approval request.
     */
    @RequestMapping(value = "/hrEmployeeInfosApprover",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> updateModelApproval(@Valid @RequestBody HrmApprovalDto approvalDto) {
        log.debug("REST request to Approve hrEmployeeInfo POST: Type: {} ID: {}, comment : {}", approvalDto.getActionType(), approvalDto.getEntityId(), approvalDto.getLogComments());

        HrEmployeeInfo modelInfo = hrEmployeeInfoRepository.findOne(approvalDto.getEntityId());

        if (approvalDto.getActionType().equalsIgnoreCase(HRMManagementConstant.APPROVAL_LOG_STATUS_ACCEPT)) {
            log.debug("REST request to APROVING ID: {}", approvalDto.getEntityId());
            modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            modelInfo.setActiveStatus(true);
            modelInfo = hrEmployeeInfoRepository.save(modelInfo);
            if (modelInfo.getLogId() != 0) {
                HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOne(modelInfo.getLogId());
                if (modelLog != null) {
                    modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                    modelLog.setActionBy(modelInfo.getCreateBy());
                    modelLog.setActionComments(approvalDto.getLogComments());
                    modelLog = hrEmployeeInfoLogRepository.save(modelLog);
                }
            }
        } else {
            log.debug("REST request to REJECTING ID: {}", approvalDto.getEntityId());
            if (modelInfo.getLogId() != 0) {
                HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmployeeInfoLogRepository.save(modelLog);

                modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo.setActiveStatus(false);
                modelInfo = hrEmployeeInfoRepository.save(modelInfo);
            } else {
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setActiveStatus(false);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo = hrEmployeeInfoRepository.save(modelInfo);
            }
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("hrEmployeeInfo", approvalDto.getEntityId().toString())).build();
    }

    /**
     * GET  /hrEmployeeInfosApprover/:logStatus -> get address list by log status.
     */
    @RequestMapping(value = "/hrEmployeeInfosApprover/{logStatus}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getModelListByLogStatus(@PathVariable Long logStatus) {
        log.debug("REST request to Approve hrEmployeeInfo List : logStatus: 0 or 5 ");
        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findAllEmployeeByStatuses();

        HrEmployeeInfoLog modelLogInfo = null;
        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        HrmApprovalDto dtoInfo = null;
        for (HrEmployeeInfo modelInfo : modelList) {
            dtoInfo = new HrmApprovalDto();
            dtoInfo.setEntityObject(modelInfo);
            if (modelInfo.getLogId() != 0) {
                modelLogInfo = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                dtoInfo.setEntityLogObject(modelLogInfo);
            }
            modelDtoList.add(dtoInfo);
        }
        return modelDtoList;
    }

    /**
     * GET  /hrEmployeeInfosApprover/log/:entityId -> load model and LogModel by entity id
     */
    @RequestMapping(value = "/hrEmployeeInfosApprover/log/{entityId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public HrmApprovalDto getModelAndLogObjectByModelId(@PathVariable String entityId) {
        log.debug("REST request to Log hrEmployeeInfo Model and Log of id: {} ", entityId);

        Long id = Long.parseLong(entityId);
        HrmApprovalDto approvalDto = new HrmApprovalDto();
        HrEmployeeInfo modelInfo = hrEmployeeInfoRepository.findOne(id);
        approvalDto.setEntityObject(modelInfo);
        approvalDto.setEntityId(id);
        approvalDto.setEntityName("Employee");
        if (modelInfo.getLogId() != 0) {
            HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            approvalDto.setEntityLogObject(modelLog);
        }
        return approvalDto;
    }

    @RequestMapping(value = "/hrEmployeeInfos/findEmploy/getEmp/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> getHrEmployeeInfoByIds(@PathVariable Long id) {
        log.debug("REST request to get HrEmploymentInfo : {}", id);
        HrEmployeeInfo hrEmployeeInfo = new HrEmployeeInfo();
        if (hrEmployeeInfoRepository.findOne(id) != null) {
            hrEmployeeInfo = hrEmployeeInfoRepository.findOne(id);
        }/*else{
            hrEmployeeInfo.setFullName("Not Found");
        }*/
        //HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(id);
        return Optional.ofNullable(hrEmployeeInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/hrEmployeeInfos/findEmployees/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> getHrEmployeeInfoById(@PathVariable String id) {
        log.debug("REST request to get HrEmploymentInfo : {}", id);
        HrEmployeeInfo hrEmployeeInfo = new HrEmployeeInfo();
        if (hrEmployeeInfoRepository.findByEmployeeId(id) != null) {
            hrEmployeeInfo = hrEmployeeInfoRepository.findByEmployeeId(id);
        }/*else{
            hrEmployeeInfo.setFullName("Not Found");
        }*/
        //HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(id);
        return Optional.ofNullable(hrEmployeeInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /hrEmployeeInfosDashboard -> get approved and rejected list through view.
     */
    @RequestMapping(value = "/hrEmployeeInfosDashboard",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmDashboardDto> getApprovedRejectedDashboardData(Pageable pageable) {
        log.debug("REST request to Get Approved and Rejected List Data by View  ");
        List<HrmDashboardDto> approvalRejectedList = hrmJdbcDao.findApproveRejectedList();
        return approvalRejectedList;
    }

    /**
     * Check Total hrEmployeeInfos desiglimitcheck by designation id
     *
     * @param desigId
     * @return
     */
    @RequestMapping(value = "/hrEmployeeInfosDesigCheck/{orgtype}/{desigId}/{refid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> checkEmployeeDepartmentLimit(@PathVariable String orgtype, @PathVariable Long desigId, @PathVariable Long refid) {
        log.debug("hrEmployeeInfosDesigCheck by designation :" + desigId+", orgtype: "+orgtype+", refid:"+refid);

        int totalEmployeeByDesig = 0;

        if(orgtype.equalsIgnoreCase("Institute"))
        {
            totalEmployeeByDesig = hrmJdbcDao.getEmployeeWiseDesignationCountByInstitute(desigId, refid);
        }
        else
        {
            totalEmployeeByDesig = hrmJdbcDao.getEmployeeWiseDesignationCountByOrganization(desigId, refid);
        }

        Map map = new HashMap();
        map.put("orgtype", orgtype);
        map.put("desigId", desigId);
        map.put("refid", refid);
        HrDesignationSetup designationInfo = hrDesignationSetupRepository.findOne(desigId);
        map.put("totalEmployee", totalEmployeeByDesig);
        if (designationInfo.getElocattedPosition() > totalEmployeeByDesig) {
            map.put("isValid", true);
        } else map.put("isValid", false);
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    /**
     * GET  /hrEmployeeInfosByFilter/:fieldName/:fieldValue -> get employee by filter.
     */
    @RequestMapping(value = "/hrEmployeeInfosByFilter/{fieldName}/{fieldValue}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> getModelListByFilter(@PathVariable String fieldName, @PathVariable String fieldValue) {
        log.debug("REST request to hrEmployeeInfosByFilter List : Filed: {} , Value: {} ", fieldName, fieldValue);

        List<HrEmployeeInfo> modelList = hrmJdbcDao.findAllEmployeeByFilter(fieldName, fieldValue);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfosByFilter/:fieldName/:fieldValue -> get employee by filter.
     */
    @RequestMapping(value = "/hrEmployeeInfosListByWorkArea/{areaid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosListByWorkArea(@PathVariable long areaid) {
        log.debug("REST request to hrEmployeeInfosListByWorkArea List : WorkArea: {} ", areaid);

        //List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findEmployeeByWorkarea(areaid);
        List<HrEmployeeInfo> modelList = hrmJdbcDao.findAllEmployeeCompactList("workArea", areaid);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfosListByInstitute/:instid/ -> get employee by institute.
     */
    @RequestMapping(value = "/hrEmployeeInfosListByInstitute/{instid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosListByInstitute(@PathVariable long instid) {
        log.debug("REST request to hrEmployeeInfosListByInstitute List : institute: {} ", instid);

        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findEmployeeListByInstitute(instid);
        //List<HrEmployeeInfo> modelList = hrmJdbcDao.findAllEmployeeCompactList("instituteId", instId);

        for (HrEmployeeInfo hrEmployeeInfo : modelList) {
            MiscFileInfo empPhoto = new MiscFileInfo();
            empPhoto.fileName(hrEmployeeInfo.getImageName())
                .contentType(hrEmployeeInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            hrEmployeeInfo.setEmpPhoto(empPhoto.fileData());
        }

        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    @RequestMapping(value = "/hrEmployeeOfInstitutes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeOfInstitutes() {
        log.debug("REST request to hrEmployeeInfosListByInstitute List : institute: {} ");

        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findHrEmployeeOfInstitutes();

        return modelList;
    }

    /**
     * POST  /hrEmployeeInfosOffOn -> Create a new hrEmployeeInfosOffOn.
     */
    @RequestMapping(value = "/hrEmployeeInfosOffOn",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void activateDeactiveEmployeeUser(@Valid @RequestBody HrmApprovalDto approvalDto) throws URISyntaxException
    {
        log.debug("REST request to save hrEmployeeInfosOffOn : {}", approvalDto.getActionType());
        if (approvalDto.getActionType().equalsIgnoreCase(HRMManagementConstant.EMPLOYEE_DEACTIVATE))
        {
            HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(approvalDto.getEntityId());
            //HrEmployeeInfo hrEmployeeInfo = (HrEmployeeInfo) approvalDto.getEntityObject();
            log.debug("Deactivating employee : {}", hrEmployeeInfo.getFullName());
            //Employee Deactivation
            if (hrEmployeeInfo.getId() != null) {
                hrEmployeeInfo.setLogStatus(HRMManagementConstant.HR_EMPLOYEE_DEACTIVATED);
                hrEmployeeInfo.setActiveStatus(false);
                hrEmployeeInfo.setLogComments(approvalDto.getLogComments());
                HrEmployeeInfo result = hrEmployeeInfoRepository.save(hrEmployeeInfo);
            }
            log.debug("Deactivating User : {}", hrEmployeeInfo.getUser().getLogin());
            //User Deactivation
            if (hrEmployeeInfo.getUser().getId() != null) {
                User user = hrEmployeeInfo.getUser();
                user.setActivated(false);
                userRepository.save(user);
            }
        }
        else if (approvalDto.getActionType().equalsIgnoreCase(HRMManagementConstant.EMPLOYEE_ACTIVATE))
        {
            HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(approvalDto.getEntityId());
            //HrEmployeeInfo hrEmployeeInfo = (HrEmployeeInfo) approvalDto.getEntityObject();
            log.debug("Activating employee : {}", hrEmployeeInfo.getFullName());
            //Employee Deactivation
            if (hrEmployeeInfo.getId() != null) {
                hrEmployeeInfo.setLogStatus(HRMManagementConstant.HR_EMPLOYEE_ACTIVATED);
                hrEmployeeInfo.setActiveStatus(true);
                hrEmployeeInfo.setLogComments("");
                HrEmployeeInfo result = hrEmployeeInfoRepository.save(hrEmployeeInfo);
            }

            log.debug("Activating User : {}", hrEmployeeInfo.getUser().getLogin());
            //User Deactivation
            if (hrEmployeeInfo.getUser().getId() != null) {
                User user = hrEmployeeInfo.getUser();
                user.setActivated(true);
                userRepository.save(user);
            }
        }
    }

    /**
     * GET  /hrEmployeeInfosByDateRange -> get all the hrEmployeeInfosByDateRange by BirthDate and Active Status
     */
    @RequestMapping(value = "/hrEmployeeInfosByDateRange",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> getEmployeeByDateRange(
        @Valid @RequestBody MiscParamDto miscParamDto , Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of hrEmployeeInfosByDateRange by dateMin:{} and dateMax:{}: ",miscParamDto.getMinDate().toString(), miscParamDto.getMaxDate().toString());

        Page<HrEmployeeInfo> page = hrEmployeeInfoRepository.findEmployeeListBirthDateRange(miscParamDto.getMinDate(), miscParamDto.getMaxDate(), pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrEmployeeInfosByDateRange");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrEmployeeInfosListByDesigLevel/:desigLevel-> get employee by desigLevel.
     */
    @RequestMapping(value = "/hrEmployeeInfosListByDesigLevel/{desigLevel}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosListByDesigLevel(@PathVariable int desigLevel) throws Exception {
        log.debug("REST request to hrEmployeeInfosListByDesigLevl List : desigLevel: {} ", desigLevel);

        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findEmployeeListByDesignationLevel(desigLevel);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfosListByDesigLevel/:desigLevel-> get employee by desigLevel.
     */
    @RequestMapping(value = "/hrEmployeeInfosListByDesigType/{desigType}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosListByDesigType(@PathVariable String desigType) throws Exception
    {
        log.debug("REST request to hrEmployeeInfosListByDesigType List type: {}", desigType);
        designationType employeeType = designationType.valueOf(desigType);
        if(employeeType==null) employeeType = designationType.HRM;

        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findAllEmployeeListByType(employeeType);
        //List<HrEmployeeInfo> modelList = hrmJdbcDao.findAllEmployeeCompactList("employeeType", desigType);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfosDetailListByDesigType/:desigType-> get employee by desigType.
     */
    @RequestMapping(value = "/hrEmployeeInfosDetailListByDesigType/{desigType}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosDetailListByDesigType(@PathVariable String desigType) throws Exception
    {
        log.debug("REST request to hrEmployeeInfosDetailListByDesigType List desigType: {}", desigType);
        designationType employeeType = designationType.valueOf(desigType);
        if(employeeType==null) employeeType = designationType.HRM;
        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findAllEmployeeListByType(employeeType);
        for (HrEmployeeInfo hrEmployeeInfo : modelList)
        {
            MiscFileInfo empPhoto = new MiscFileInfo();
            empPhoto.fileName(hrEmployeeInfo.getImageName())
                .contentType(hrEmployeeInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            hrEmployeeInfo.setEmpPhoto(empPhoto.fileData());
        }
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfosDetailListByDesigTypeList-> get employee by desigType List.
     */
    @RequestMapping(value = "/hrEmployeeInfosDetailListByDesigTypeList",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosDetailListByTypeList() throws Exception
    {
        log.debug("REST request to hrEmployeeInfosDetailListByTypeList List: {}");
        /*String employeeType = "'"+designationType.Staff+"', '"+designationType.Teacher+"'";

        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findAllEmployeeListByTypeList(employeeType);
        for (HrEmployeeInfo hrEmployeeInfo : modelList)
        {
            MiscFileInfo empPhoto = new MiscFileInfo();
            empPhoto.fileName(hrEmployeeInfo.getImageName())
                .contentType(hrEmployeeInfo.getEmpPhotoContentType())
                .filePath(HRMManagementConstant.EMPLOYEE_PHOTO_FILE_DIR);
            empPhoto = fileUtils.readFileAsByte(empPhoto);
            hrEmployeeInfo.setEmpPhoto(empPhoto.fileData());
        }
        log.debug("List Len:  ", modelList.size());
        return modelList;*/
        Long areaId = 1L;
        List<HrEmployeeInfo> modelList = hrmJdbcDao.findAllEmployeeCompactList("designationType", areaId);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfoCompactList -> get all employee by key value.
     */
    @RequestMapping(value = "/hrEmployeeInfoCompactList/{key}/{value}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> getAllEmployeeList(@PathVariable String key, @PathVariable String value)
    {
        log.debug("REST request to all employee list key: {}, value: {}",key, value);
        List<HrEmployeeInfo> modelList = hrmJdbcDao.findAllEmployeeCompactList(key, value);
        log.debug("List Len: ", modelList.size());
        return modelList;
    }

    /**
     * POST  /hrEmployeeInfoTransferToDte -> Transfer a new hrEmployeeInfo.
     */
    @RequestMapping(value = "/hrEmployeeInfoTransferToDte",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> transferEmployeeToDte(@Valid @RequestBody HrEmployeeInfo hrEmployeeInfo) throws URISyntaxException {
        log.debug("REST request to save transferEmployee to dte: {}", hrEmployeeInfo);
        Map map = new HashMap();

        try {
            // Save employee information into LOG .
            HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
            HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(hrEmployeeInfo.getId());
            logInfo = conversionService.getLogFromSource(dbModelInfo);
            logInfo.setLogStatus(HRMManagementConstant.TRANSFER_INST_TO_ORG_LOG_STATUS);
            logInfo.setActionComments(designationType.Teacher.toString()+"-"+designationType.DTE_Employee.toString());
            //logInfo.setCreateBy(SecurityUtils.getCurrentUserId());
            logInfo.setCreateDate(dateResrc.getDateAsLocalDate());

            logInfo = hrEmployeeInfoLogRepository.save(logInfo);

            // update fields value for transfer within employee entity
            hrEmployeeInfoRepository.updateEmployeeForDteTransfer(hrEmployeeInfo.getId(), designationType.DTE_Employee, hrEmployeeInfo.getOrganizationType(),
                hrEmployeeInfo.getWorkArea().getId(), hrEmployeeInfo.getWorkAreaDtl().getId(), hrEmployeeInfo.getDepartmentInfo().getId(), hrEmployeeInfo.getDesignationInfo().getId(), hrEmployeeInfo.getLogId());

            // Update authority
            // Remove ROLE_HRM_USER role for the employee user.
            // Add ROLE_DTE_EMPLOYEE role to employee user. If ROLE_DTE_EMPLOYEE not available, create new role for ROLE_DTE_EMPLOYEE.

            Authority dteEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
            Authority hrmEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME);
            Authority teacherEmployeeAuthority  = authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);

            if(dteEmployeeAuthority==null)
            {
                log.debug("REST Adding Authority, as not exist : {} ", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
                dteEmployeeAuthority=new Authority();
                dteEmployeeAuthority.setName(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
                authorityRepository.save(dteEmployeeAuthority);
            }

            User employeeUser = userRepository.findOne(hrEmployeeInfo.getUser().getId());
            System.out.println("\n >>>>>>>>>>>>>>>>  Authority :: "+employeeUser.getAuthorities().toString());
            Set<Authority> employeeAuthorities = employeeUser.getAuthorities();

            if(! employeeAuthorities.contains(dteEmployeeAuthority))
            {
                log.debug("REST mapping user authority - new dte role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                employeeAuthorities.add(dteEmployeeAuthority);
                employeeAuthorities.remove(hrmEmployeeAuthority);
                employeeAuthorities.remove(teacherEmployeeAuthority);
                employeeUser.setAuthorities(employeeAuthorities);
                userRepository.save(employeeUser);
            }
            else
            {
                log.debug("REST mapping user authority - dte role already exit : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                employeeAuthorities.remove(hrmEmployeeAuthority);
                employeeAuthorities.remove(teacherEmployeeAuthority);
                employeeUser.setAuthorities(employeeAuthorities);
                userRepository.save(employeeUser);
            }

            map.put("desigType", designationType.DTE_Employee);
            map.put("fromRole", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            map.put("targetRole", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
            map.put("success", "true");
            map.put("message", "");

            String msilContent="Dear " +dbModelInfo.getFullName()+","+
                "\n" +
                "You are transferred to DTE. \n" +
                "\n" +
                "this is a system generated mail." +
                "Please contract with DIRECTORATE OF TECHNICAL EDUCATION for any query.";
            mailService.sendEmail(dbModelInfo.getEmailAddress(),"Employee Transfer to DTE",msilContent,false,false);

        }
        catch (Exception ex)
        {
            log.error("EmployeeLog saving error:" + ex);
            map.put("fromRole", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            map.put("targetRole", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
            map.put("success", "false");
            map.put("message", ex.getLocalizedMessage());
        }
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    /**
     * POST  /hrEmployeeInfoTransferToTeacher -> Transfer a new hrEmployeeInfo.
     */
    @RequestMapping(value = "/hrEmployeeInfoTransferToTeacher",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> transferEmployeeToHRMTeacher(@Valid @RequestBody HrEmployeeInfo hrEmployeeInfo) throws URISyntaxException {
        log.debug("REST request to save transferEmployee to teacher : {}", hrEmployeeInfo.getFullName());

        Map map = new HashMap();

        try {
            // Save employee information into LOG .
            HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
            HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(hrEmployeeInfo.getId());
            logInfo = conversionService.getLogFromSource(dbModelInfo);
            logInfo.setLogStatus(HRMManagementConstant.TRANSFER_ORG_TO_INST_LOG_STATUS);
            logInfo.setActionComments(designationType.DTE_Employee.toString()+"-"+designationType.Teacher.toString());
            logInfo.setCreateDate(dateResrc.getDateAsLocalDate());
            //logInfo.setActionDate(logInfo.getCreateDate());

            logInfo = hrEmployeeInfoLogRepository.save(logInfo);

            // update fields value for transfer within employee entity
            hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(hrEmployeeInfo.getId(), designationType.Teacher, hrEmployeeInfo.getOrganizationType(),
                hrEmployeeInfo.getInstCategory().getId(),hrEmployeeInfo.getInstLevel().getId() ,hrEmployeeInfo.getInstitute().getId(), hrEmployeeInfo.getDepartmentInfo().getId(), hrEmployeeInfo.getDesignationInfo().getId(), hrEmployeeInfo.getLogId());

            // Update authority
            // Remove ROLE_HRM_USER role for the employee user.
            // Add ROLE_DTE_EMPLOYEE role to employee user. If ROLE_DTE_EMPLOYEE not available, create new role for ROLE_DTE_EMPLOYEE.

            Authority teacherEmployeeAuthority  = authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            Authority hrmEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME);
            Authority dteEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);

            if(teacherEmployeeAuthority==null)
            {
                log.debug("REST Adding Authority, as not exist : {} ", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
                teacherEmployeeAuthority=new Authority();
                teacherEmployeeAuthority.setName(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
                authorityRepository.save(teacherEmployeeAuthority);
            }

            User employeeUser = userRepository.findOne(hrEmployeeInfo.getUser().getId());
            Set<Authority> employeeAuthorities = employeeUser.getAuthorities();

            if(!employeeAuthorities.contains(teacherEmployeeAuthority))
            {
                log.debug("REST mapping user authority - new teacher role : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                employeeAuthorities.add(teacherEmployeeAuthority);
                employeeAuthorities.add(hrmEmployeeAuthority);
                employeeAuthorities.remove(dteEmployeeAuthority);
                employeeUser.setAuthorities(employeeAuthorities);
                userRepository.save(employeeUser);
            }
            else
            {
                log.debug("REST mapping user authority - teacher role already exit : {} with user {}", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME, employeeUser.getLogin());
                employeeAuthorities.add(hrmEmployeeAuthority);
                employeeAuthorities.remove(dteEmployeeAuthority);
                employeeUser.setAuthorities(employeeAuthorities);
                userRepository.save(employeeUser);
            }

            map.put("desigType", designationType.Teacher);
            map.put("fromRole", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
            map.put("targetRole", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            map.put("success", "true");
            map.put("message", "");


            String msilContent="Dear " +dbModelInfo.getFullName()+","+
                "\n" +
                "You are transferred to institute. \n" +
                "\n" +
                "this is a system generated mail." +
                "Please contract with DIRECTORATE OF TECHNICAL EDUCATION for any query.";
            mailService.sendEmail(dbModelInfo.getEmailAddress(),"Employee Transfer to Institute",msilContent,false,false);
        }
        catch (Exception ex)
        {
            log.error("EmployeeLog saving error:" + ex);
            map.put("fromRole", HRMManagementConstant.ROLE_DTE_EMPLOYEE_NAME);
            map.put("targetRole", HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
            map.put("success", "false");
            map.put("message", ex.getLocalizedMessage());
        }
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    /**
     * GET  /hrEmployeeInfosTransferList/:emplId-> get employee by log history.
     */
    @RequestMapping(value = "/hrEmployeeInfoTransferHistory/{emplId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfoLog> hrEmployeeInfosTransferHistory(@PathVariable Long emplId) throws Exception
    {
        log.debug("REST request to hrEmployeeInfoTransferHistory List emplid: {}", emplId);

        List<Long> logStatusList = new ArrayList<Long>();
        logStatusList.add(HRMManagementConstant.TRANSFER_ORG_TO_INST_LOG_STATUS);
        logStatusList.add(HRMManagementConstant.TRANSFER_INST_TO_ORG_LOG_STATUS);
        logStatusList.add(HRMManagementConstant.TRANSFER_INST_TO_INST_LOG_STATUS);
        logStatusList.add(HRMManagementConstant.TRANSFER_ORG_TO_ORG_LOG_STATUS);
        logStatusList.add(HRMManagementConstant.PROMOTION_EMPLOYEE_LOG_STATUS);

        List<HrEmployeeInfoLog> modelList = hrEmployeeInfoLogRepository.findAllByTransferFromLogStatus(emplId, logStatusList);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfosChangeHistory/:emplId-> get employee by log history.
     */
    @RequestMapping(value = "/hrEmployeeInfoChangeHistory/{emplId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfoLog> hrEmployeeInfoChangeHistory(@PathVariable Long emplId) throws Exception
    {
        log.debug("REST request to hrEmployeeInfoChangeHistory List emplid: {}", emplId);

        List<HrEmployeeInfoLog> modelList = hrEmployeeInfoLogRepository.findAllChangeHistoryByEmployee(emplId);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    @RequestMapping(value = "/hrEmployeeInfoChangeHistory/employeetransferInfo",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> hrEmployeeInfoChangeHistory() throws Exception
    {
        log.debug("REST request to hrEmployeeInfoChangeHistory List emplid: {}");
        List<HrEmployeeInfo> hrEmployeeList = new ArrayList<HrEmployeeInfo>();


       // String str = "'"+HRMManagementConstant.TRANSFER_TO_DTE_LOG_STATUS +"','"+HRMManagementConstant.TRANSFER_TO_TEACHER_LOG_STATUS +"'";
        List<String> employeeIdList = hrEmployeeInfoLogRepository.findAllEmpInfoByLogStatus(HRMManagementConstant.TRANSFER_INST_TO_ORG_LOG_STATUS,HRMManagementConstant.TRANSFER_ORG_TO_INST_LOG_STATUS);

        for(String employeeId:employeeIdList){
            hrEmployeeList.add(hrEmployeeInfoRepository.findByEmployeeId(employeeId));
        }
        return Optional.ofNullable(hrEmployeeList)
            .map(hrEmployeeInfos -> new ResponseEntity<>(
                hrEmployeeInfos,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));    }



    /**
     * POST  /hrEmployeeInfoTransferJobReleaseUpdate -> Transferred EmployeeInfo Release Data Update.
     */
    @RequestMapping(value = "/hrEmployeeInfoTransferJobReleaseUpdate",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> updateReleaseLeaterJobDOc(@Valid @RequestBody HrEmployeeInfo hrEmployeeInfo) throws URISyntaxException {
        log.debug("REST hrEmployeeInfoTransferJobReleaseUpdate employee: {}", hrEmployeeInfo.getJobReleaseDate());
        Map map = new HashMap();

        try {
            // Save employee information into LOG .
            HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
            HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(hrEmployeeInfo.getId());
            logInfo = conversionService.getLogFromSource(dbModelInfo);
            logInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            logInfo.setActionComments("Release Letter Update");
            logInfo.setCreateDate(dateResrc.getDateAsLocalDate());

            logInfo = hrEmployeeInfoLogRepository.save(logInfo);


            //Saving Employee Release Document to Dir.
            MiscFileInfo releaseLtrDoc = new MiscFileInfo();
            try{
                releaseLtrDoc.fileData(hrEmployeeInfo.getReleaseLetterDoc())
                    .fileName(hrEmployeeInfo.getReleaseLetterDocName())
                    .contentType(hrEmployeeInfo.getReleaseLetterContentType())
                    .filePath(HRMManagementConstant.TRANSFER_FILE_DIR);

                if (dbModelInfo.getReleaseLetterDocName() == null ||
                    dbModelInfo.getReleaseLetterDocName().length() < HRMManagementConstant.MINIMUM_IMG_NAME_LENGTH)
                {
                    releaseLtrDoc = fileUtils.saveFileAsByte(releaseLtrDoc);
                } else
                    {
                    releaseLtrDoc.fileName(dbModelInfo.getReleaseLetterDocName())
                        .contentType(hrEmployeeInfo.getReleaseLetterContentType());
                    releaseLtrDoc = fileUtils.updateFileAsByte(releaseLtrDoc);
                }
            }catch(Exception ex){
                log.error("Exception: UpdateHrInfo releaseDoc "+ex.getMessage());
            }

            dbModelInfo.setDateOfJoining(hrEmployeeInfo.getDateOfJoining());
            dbModelInfo.setJobReleaseDate(hrEmployeeInfo.getJobReleaseDate());
            dbModelInfo.setReleaseLetterContentType(hrEmployeeInfo.getReleaseLetterContentType());
            dbModelInfo.setReleaseLetterDoc(new byte[1]);
            dbModelInfo.setReleaseLetterDocName(releaseLtrDoc.fileName());
            dbModelInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
            dbModelInfo.setUpdateBy(SecurityUtils.getCurrentUserId());

            HrEmployeeInfo result = hrEmployeeInfoRepository.save(dbModelInfo);

            map.put("success", "true");
            map.put("message", "");
        }
        catch (Exception ex)
        {
            log.error("EmployeeLog saving error:" + ex);
            map.put("success", "false");
            map.put("message", ex.getLocalizedMessage());
        }
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/hrEmployeeInfos/findByNationalId/{nid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> findByNationalId(@PathVariable String nid) {
        log.debug("REST request to get findByNationalId : {}", nid);

        HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findByNationalId(nid);

        return Optional.ofNullable(hrEmployeeInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/hrEmployeeInfos/getAllEmployee",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> getAllHrEmployeeInfo()
        throws URISyntaxException {
        log.debug("REST request to get a List of HrEmployeeInfo");
//        List<HrEmployeeInfo> page = hrEmployeeInfoRepository.findAllByActiveStatus(true);


        return Optional.ofNullable(hrEmployeeInfoRepository.findAllByActiveStatus(true))
            .map(hrEmployeeInfo -> new ResponseEntity<>(
                hrEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /hrEmployeeInfos/findByUserTyping/:emplId-> get employee by firstName.
     */
    @RequestMapping(value = "/hrEmployeeInfos/findByUserTyping/{firstName}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> findByUserTyping(@PathVariable String firstName) throws Exception{
        log.debug("REST request to findByUserTyping List firstName: {}", firstName);
        List<HrEmployeeInfo> hrEmployeeInfos = hrEmployeeInfoRepository.findByFullNameStartingWithIgnoreCase(firstName);

        return Optional.ofNullable(hrEmployeeInfos)
            .map(hrEmployeeInfo -> new ResponseEntity<>(
                hrEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * Approval Process Methods start from here.
     * @return
     */

    @RequestMapping(value = "/hrEmployeeInfoGetDeptHeadStatus",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> checkForDepartmentHead()
    {
        log.debug("hrEmployeeInfoCheckDeptHeadStatus");
        List<HrDepartmentHeadInfo> deptHeadInfoList = hrDepartmentHeadInfoRepository.findAllByEmployeeIsCurrentUser();

        Map map =new HashMap();
        String departIds = "";
        if(deptHeadInfoList!=null && deptHeadInfoList.size()>0)
        {
            map.put("isHead", true);
            map.put("departmentCount", deptHeadInfoList.size());
            List<HrDepartmentSetup> departmentList = new ArrayList<HrDepartmentSetup>();
            for(HrDepartmentHeadInfo headInfo : deptHeadInfoList)
            {
                departmentList.add(headInfo.getDepartmentInfo());
                departIds += headInfo.getDepartmentInfo().getId()+"#";
            }
            if(departIds.length()>2)
                departIds = departIds.substring(0, departIds.length()-1);
            map.put("departmentList", departmentList);
            map.put("departmentIds", departIds);
        }
        else
        {
            map.put("isHead", false);
        }
        return new ResponseEntity<Map>(map,HttpStatus.OK);
    }

    @RequestMapping(value = "/hrEmployeeInfoDeptHeadPendingApprovalList/{deptIds}",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getDepartmentHeadPendingApprovalList(@PathVariable String deptIds)
    {
        log.debug("hrEmployeeInfoDeptHeadPendingApprovalList deptIdList: {}", deptIds);

        List<Long> deptIdList = miscUtils.splitStringByKeyAndConvertLongList(deptIds, "#");
        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        if(deptIdList.size()>0)
        {
            List<HrEmployeeInfo> pendingModelList = hrEmployeeInfoRepository.findAllModelsByDepartmentIdsAndPendingStatus(deptIdList,
                HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
            log.debug(" model deptIds: {}, pendingListSize: {}, logStatus: {}",deptIds, pendingModelList.size(), HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);

            HrmApprovalDto dtoInfo = null;
            HrEmployeeInfoLog modelLogInfo = null;
            for (HrEmployeeInfo modelInfo : pendingModelList) {
                dtoInfo = new HrmApprovalDto();
                dtoInfo.setEntityObject(modelInfo);
                if (modelInfo.getLogId() != 0) {
                    modelLogInfo = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                    dtoInfo.setEntityLogObject(modelLogInfo);
                }
                modelDtoList.add(dtoInfo);
            }

        }

        return modelDtoList;
    }

    @RequestMapping(value = "/hrEmployeeInfoInstituteHeadPendingApprovalList",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getInstituteHeadPendingApprovalList()
    {
        log.debug("hrEmployeeInfoInstituteHeadPendingApprovalList");
        Institute institute = instituteRepository.findOneByUserIsCurrentUser();

        log.debug("institute id {}", institute.getId());
        List<HrEmployeeInfo> pendingModelList = hrEmployeeInfoRepository.findAllModelsByInstituteAndPendingStatus(
            HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST,
            institute.getId(),
            HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);

        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        HrmApprovalDto dtoInfo = null;
        HrEmployeeInfoLog modelLogInfo = null;
        for (HrEmployeeInfo modelInfo : pendingModelList) {
            dtoInfo = new HrmApprovalDto();
            dtoInfo.setEntityObject(modelInfo);
            if (modelInfo.getLogId() != 0) {
                modelLogInfo = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                dtoInfo.setEntityLogObject(modelLogInfo);
            }
            modelDtoList.add(dtoInfo);
        }

        log.debug("institute employee list: {}", modelDtoList.size());

        return modelDtoList;
    }

    /**
     * GET  /hrEmployeeInfoAdminApprovalListByLogStatus
     */
    @RequestMapping(value = "/hrEmployeeInfoAdminApprovalListByLogStatus",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmployeeInfo> hrEmployeeInfosAdminApprovalListByLogStatus() throws Exception
    {
        log.debug("REST request to hrEmployeeInfoAdminApprovalListByLogStatus List status  DeptPend: {}, DeptRej: {}, InstPend: {}, InstRej: {} ",
            HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING,
            HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED,
            HRMManagementConstant.APPROVAL_INST_HEAD_PENDING,
            HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);

        List<Long> logStatusList = new ArrayList<Long>();
        logStatusList.add(HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
        logStatusList.add(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
        logStatusList.add(HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
        logStatusList.add(HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);

        List<HrEmployeeInfo> modelList = hrEmployeeInfoRepository.findAllModelByLogStatuses(logStatusList);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmployeeInfoUpdateRequestApprove/ -> approve the employee informatipn update request.
     */
    @RequestMapping(value = "/hrEmployeeInfoUpdateRequestApprove",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> employeeInfoUpdateRequestApprove(@Valid @RequestBody HrEmployeeInfo modelInfo)
    {
        log.debug("REST hrEmployeeInfoUpdateRequestApprove orgType: {}",modelInfo.getOrganizationType());

        modelInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
        modelInfo.setUpdateBy(SecurityUtils.getCurrentUserId());

        if(modelInfo.getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
        {
            if(modelInfo.getLogComments().equalsIgnoreCase(HRMManagementConstant.EMP_DATA_UPDATE_APPROVAL_APPROVED))
            {
                log.debug("REST TransferApproval -> ORG-> DEPT_HEAD APPROVED NewLogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_APPROVED);
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
                modelInfo.setActiveStatus(true);
                modelInfo = hrEmployeeInfoRepository.save(modelInfo);

                if (modelInfo.getLogId() != 0) {
                    HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOne(modelInfo.getLogId());
                    if (modelLog != null) {
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmployeeInfoLogRepository.save(modelLog);
                    }
                }

            }
            else {
                log.debug("REST EmployeeUpdate -> ORG -> DEPT_HEAD REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);

                if (modelInfo.getLogId() != 0) {
                    HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                    modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                    modelLog.setActionBy(modelInfo.getCreateBy());
                    modelLog.setActionComments(modelInfo.getLogComments());
                    modelLog = hrEmployeeInfoLogRepository.save(modelLog);

                    modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                    modelInfo.setLogComments(modelInfo.getLogComments());
                    modelInfo.setActiveStatus(false);
                    modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                } else {
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                    modelInfo.setActiveStatus(false);
                    modelInfo.setLogComments(modelInfo.getLogComments());
                    modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                }

            }
        }
        else if(modelInfo.getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
        {
            if(modelInfo.getLogComments().equalsIgnoreCase(HRMManagementConstant.EMP_DATA_UPDATE_APPROVAL_APPROVED))
            {
                if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING)
                {
                    log.debug("REST EmployeeUpdateApproval -> INST -> DEPT_HEAD -> APPROVED LogStatus: {} ", HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
                    modelInfo.setActiveStatus(false);
                    modelInfo = hrEmployeeInfoRepository.save(modelInfo);


                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_INST_HEAD_PENDING)
                {
                    log.debug("REST EmployeeUpdateApproval -> INST -> INST_HEAD -> APPROVED LogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setActiveStatus(true);
                    modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                }

                if (modelInfo.getLogId() != 0) {
                    HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOne(modelInfo.getLogId());
                    if (modelLog != null) {
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmployeeInfoLogRepository.save(modelLog);
                    }
                }
            }
            else
            {
                if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING)
                {
                    log.debug("REST EmployeeUpdateApproval -> INST -> DEPT_HEAD -> REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmployeeInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                    }

                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_INST_HEAD_PENDING)
                {
                    log.debug("REST EmployeeUpdateApproval -> INST -> INST_HEAD -> REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmployeeInfoLog modelLog = hrEmployeeInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmployeeInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmployeeInfoRepository.save(modelInfo);
                    }
                }


            }
        }

        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("hrEmployeeInfo", modelInfo.getId().toString())).build();
    }


    /**
     * GET  /hrEmployeeInfos/findByUserTyping/:emplId-> get employee by firstName.
     */
    @RequestMapping(value = "/hrEmployeeInfos/byDesignationNameAndType/{designationName}/{desigType}/{status}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> findByDesignationName(@PathVariable String designationName,@PathVariable String desigType,@PathVariable boolean status) throws Exception{
        log.debug("REST request to findByDesignationName: {}", designationName);
        List<HrEmployeeInfo> hrEmployeeInfos = hrEmployeeInfoRepository.findByDesignationNameAndDesignationType(designationName,designationType.valueOf(desigType),status);

        return Optional.ofNullable(hrEmployeeInfos)
            .map(hrEmployeeInfo -> new ResponseEntity<>(
                hrEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @RequestMapping(value = "/hrEmployeeInfos/byLogin",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmployeeInfo> findByLogin() throws Exception{
        log.debug("REST request to findByLogin: {}");
        HrEmployeeInfo hrEmployeeInfos = hrEmployeeInfoRepository.findOneByEmployeeUserIsCurrentUser();
        return Optional.ofNullable(hrEmployeeInfos)
            .map(hrEmployeeInfo -> new ResponseEntity<>(
                hrEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    @Timed
    @RequestMapping(value = "/hr-employees/department/{deptId}")
    public List<HrEmployeeInfo> findByLogin(@PathVariable("deptId") Long deptId) throws Exception{
        log.debug("REST request to findByLogin: {}");
        //List<HrEmployeeInfo> hrEmployeeInfos = hrEmployeeInfoRepository.findAllModelsByDepartment(deptId);
        List<HrEmployeeInfo> hrEmployeeInfos = entityManagerService.getHrEmployeeByDept(deptId);
        return (hrEmployeeInfos!=null || hrEmployeeInfos.size() > 0) ? hrEmployeeInfos : new ArrayList<>();
    }
    @Timed
    @RequestMapping(value = "/hr-employees/gradeCode/{gradeCode}")
    public List<HrEmployeeInfo> getLeaveApprover(@PathVariable("gradeCode") String gradeCode) throws Exception{
        log.debug("REST request to findByLogin: {}");
        List<HrEmployeeInfo> hrEmployeeInfos = entityManagerService.getAlmLeaveApprovar(gradeCode);
        return (hrEmployeeInfos!=null || hrEmployeeInfos.size() > 0) ? hrEmployeeInfos : new ArrayList<>();
    }

    @RequestMapping(value = "/hrEmployeeInfos/byHrWorkAreaDetail/{workAreaDtlId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmployeeInfo>> findByWorkAreaDtlName(@PathVariable Long workAreaDtlId) throws Exception{
        log.debug("REST request to findByDesignationName: {}", workAreaDtlId);

        List<HrEmployeeInfo> hrEmployeeInfos = hrEmployeeInfoRepository.findAllByWorkAreaDetails(workAreaDtlId);

        return Optional.ofNullable(hrEmployeeInfos)
            .map(hrEmployeeInfo -> new ResponseEntity<>(
                hrEmployeeInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
