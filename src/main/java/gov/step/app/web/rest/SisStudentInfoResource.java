package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.repository.InstituteRepository;
import gov.step.app.repository.NotificationStepRepository;
import gov.step.app.repository.SisStudentInfoRepository;
import gov.step.app.repository.search.SisStudentInfoSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.MailService;
import gov.step.app.service.UserService;
import gov.step.app.web.rest.util.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SisStudentInfo.
 */
@RestController
@RequestMapping("/api")
public class SisStudentInfoResource {

    private final Logger log = LoggerFactory.getLogger(SisStudentInfoResource.class);

    @Inject
    private InstituteRepository instituteRepository;
    @Inject
    private SisStudentInfoRepository sisStudentInfoRepository;

    @Inject
    private SisStudentInfoSearchRepository sisStudentInfoSearchRepository;

    @Inject
    private MailService mailService;

    @Inject
    private UserService userService;

    @Inject
    private NotificationStepRepository notificationStepRepository;

    String filepath = "/backup/stupicImages/";

    DateResource dr=new DateResource();
    TransactionIdResource transactionId =new TransactionIdResource();

    /**
     * POST  /sisStudentInfos -> Create a new sisStudentInfo.
     */
    @RequestMapping(value = "/sisStudentInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SisStudentInfo> createSisStudentInfo(@Valid @RequestBody SisStudentInfo sisStudentInfo) throws URISyntaxException {
        log.debug("REST request to save SisStudentInfo : {}", sisStudentInfo);
        if (sisStudentInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new sisStudentInfo cannot already have an ID").body(null);
        }
        if (SecurityUtils.isCurrentUserInRole("ROLE_INSTITUTE")) {
            Institute institute = instituteRepository.findOneByUserIsCurrentUser();
            sisStudentInfo.setInstitute(institute);
        }


        sisStudentInfo.setCreateBy(SecurityUtils.getCurrentUserId());
        sisStudentInfo.setCreateDate(dr.getDateAsLocalDate());

        DateTime dateTime = new DateTime();
        String names = String.valueOf(dateTime.getMillis() % 10000000);
        String filename = "stupicImages";
        String extension = ".png";

        if (sisStudentInfo.getStuPictureContentType() != null && sisStudentInfo.getStuPictureContentType().equals("application/pdf")) {
            extension = ".pdf";
        } else {
            extension = ".png";
        }
        if (sisStudentInfo.getStuPicture() != null) {

            try {
                sisStudentInfo.setStupicName(AttachmentUtil.saveAttachmentForStu(filepath, names, extension, sisStudentInfo.getStuPicture()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sisStudentInfo.setStuPicture(null);
        SisStudentInfo result = sisStudentInfoRepository.save(sisStudentInfo);

        if(result != null){
            try {
                Boolean emailStatus = false;

                if(sisStudentInfo.getEmailAddress() != null){
                    String emailBuilder = sisStudentInfo.getEmailAddress();
                    User user = userService.createCustomUserInformationByUserModule(sisStudentInfo.getEmailAddress(), "123456", sisStudentInfo.getName(), null, emailBuilder, "en", "ROLE_GOVT_STUDENT", true, 1L);

                    sisStudentInfo.setUser(user);
                    sisStudentInfoRepository.save(sisStudentInfo);

                    if(user != null){
                        //Email email = new Email();
                        //emailStatus = email.Send(emails);
                        String emailSubject="Student Registration";
                        String emailBody="Dear "+sisStudentInfo.getName()+", <br><br> Please find your credentials. " + "<br><br> User Name : " + sisStudentInfo.getEmailAddress() + "<br><br> User Password : 123456 <br><br> Regards, <br>DTE Admin";
                        mailService.sendEmail(sisStudentInfo.getEmailAddress(), emailSubject, emailBody, false, true);
                        log.debug("Email Send Status of USER ROLE ASSIGN : " + emailStatus);
                    }
                }
            }catch (Exception e){
                log.debug("Exception in save role at JHI_USER entity");
            }
        }

        /*if(result != null){
            NotificationStep notificationSteps = new NotificationStep();
            notificationSteps.setNotification("Student Information approved by Admin Id :" + SecurityUtils.getCurrentUserId());
            notificationSteps.setStatus(true);
            notificationSteps.setUrls("sisStudentInfo");
            notificationSteps.setCreateBy(SecurityUtils.getCurrentUserId());
            notificationSteps.setCreateDate(DateResource.getDateAsLocalDate());
            notificationSteps.setRemarks("Student Information System ");
            notificationStepRepository.save(notificationSteps);
        }*/


        sisStudentInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/sisStudentInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("sisStudentInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sisStudentInfos -> Updates an existing sisStudentInfo.
     */
    @RequestMapping(value = "/sisStudentInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SisStudentInfo> updateSisStudentInfo(@Valid @RequestBody SisStudentInfo sisStudentInfo) throws URISyntaxException {
        log.debug("REST request to update SisStudentInfo : {}", sisStudentInfo);
        if (sisStudentInfo.getId() == null) {
            return createSisStudentInfo(sisStudentInfo);
        }

        DateTime dateTime = new DateTime();
        String names = String.valueOf(dateTime.getMillis() % 10000000);
        String filename = "stupicImages";
        String extension = ".png";

        if (sisStudentInfo.getStuPictureContentType() != null && sisStudentInfo.getStuPictureContentType().equals("application/pdf")) {
            extension = ".pdf";
        } else {
            extension = ".png";
        }
        if (sisStudentInfo.getStuPicture() != null) {

            try {
                sisStudentInfo.setStupicName(AttachmentUtil.saveAttachmentForStu(filepath, names, extension, sisStudentInfo.getStuPicture()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sisStudentInfo.setStuPicture(null);

        sisStudentInfo.setUpdateBy(SecurityUtils.getCurrentUserId());
        sisStudentInfo.setUpdateDate(dr.getDateAsLocalDate());
        SisStudentInfo result = sisStudentInfoRepository.save(sisStudentInfo);
        sisStudentInfoSearchRepository.save(sisStudentInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("sisStudentInfo", sisStudentInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sisStudentInfos -> get all the sisStudentInfos.
     */
    @RequestMapping(value = "/sisStudentInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SisStudentInfo>> getAllSisStudentInfos(Pageable pageable)
        throws URISyntaxException {
        Page<SisStudentInfo> page = sisStudentInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sisStudentInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sisStudentInfos/:id -> get the "id" sisStudentInfo.
     */
    @RequestMapping(value = "/sisStudentInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SisStudentInfo> getSisStudentInfo(@PathVariable Long id) {
        log.debug("REST request to get SisStudentInfo : {}", id);
       // return Optional.ofNullable(sisStudentInfoRepository.findOne(id))
        SisStudentInfo sisStudentInfo = sisStudentInfoRepository.findOne(id);
       if(sisStudentInfo.getStupicName() !=null){
           try {
               sisStudentInfo.setStuPicture(AttachmentUtil.retrivedAttachment(filepath,sisStudentInfo.getStudentName()));
           } catch (Exception e) {
               e.printStackTrace();
           }
       }

           /* .map(sisStudentInfo -> new ResponseEntity<>(
                sisStudentInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));*/
        if (sisStudentInfo != null) {
            return new ResponseEntity<>(sisStudentInfo, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @RequestMapping(value = "/sisStudentInfos/findStudentInfoByInstitute/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SisStudentInfo> findStudentInfoByInstituteWise(@PathVariable Long id) {

        Long instId = instituteRepository.findOneByUserIsCurrentUser().getId();

        return Optional.ofNullable(sisStudentInfoRepository.findStudentInfoByInstituteWise(id, instId))
            .map(sisStudentInfo -> new ResponseEntity<>(
                sisStudentInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @RequestMapping(value = "/sisStudentInfos/findStudentInfoUser",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SisStudentInfo>> findStudentInfoByCurrentUser(Pageable pageable)
        throws URISyntaxException {
        Page<SisStudentInfo> page = sisStudentInfoRepository.findByUserIsCurrentUser(pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sisStudentInfos/findStudentInfoUser");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    // Find Student information by sid No
    @RequestMapping(value = "/sisStudentInfos/findStudentInfoById/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SisStudentInfo> findStudentInfoById(@PathVariable Long id) {

        Long instId = instituteRepository.findOneByUserIsCurrentUser().getId();
        log.debug(">>>>>>>>>>>>>>> instId >>>>>>>>>>> :"+instId);
        return Optional.ofNullable(sisStudentInfoRepository.findStudentInfoById(id,instId))
            .map(sisStudentInfo -> new ResponseEntity<>(
                sisStudentInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /sisStudentInfos/:id -> delete the "id" sisStudentInfo.
     */
    @RequestMapping(value = "/sisStudentInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSisStudentInfo(@PathVariable Long id) {
        log.debug("REST request to delete SisStudentInfo : {}", id);
        sisStudentInfoRepository.delete(id);
        sisStudentInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("sisStudentInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/sisStudentInfos/:query -> search for the sisStudentInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/sisStudentInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SisStudentInfo> searchSisStudentInfos(@PathVariable String query) {
        return StreamSupport
            .stream(sisStudentInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


    @RequestMapping(value = "/sisStudentInfos/getAllStudentListByIdFilter/{mobileNo}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SisStudentInfo> getAllStudentListByIdFilter(@PathVariable String mobileNo)
        throws URISyntaxException {
        String StudentMobileNo = "%" + mobileNo + "%";
        Long instId = instituteRepository.findOneByUserIsCurrentUser().getId();

        List<SisStudentInfo> listStudent = sisStudentInfoRepository.findStudentInfoByStudentId(StudentMobileNo,instId);


        return listStudent;



}



/*  AreaName Checking method */

    @RequestMapping(value = "/sisStudentInfo/checkByApplicationId/",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Map> checkApplication(@RequestParam String value) {

        SisStudentInfo application = sisStudentInfoRepository.findByApplicationId(value);

        Map map =new HashMap();

        map.put("value", value);

        if(application == null){
            map.put("isValid",true);
            return new ResponseEntity<Map>(map,HttpStatus.OK);

        }else{
            map.put("isValid",false);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }

    }
}
