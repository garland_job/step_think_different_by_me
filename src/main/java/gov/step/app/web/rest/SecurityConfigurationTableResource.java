package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.SecurityConfigurationTable;
import gov.step.app.repository.SecurityConfigurationTableRepository;
import gov.step.app.repository.search.SecurityConfigurationTableSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing SecurityConfigurationTable.
 */
@RestController
@RequestMapping("/api")
public class SecurityConfigurationTableResource {

    private final Logger log = LoggerFactory.getLogger(SecurityConfigurationTableResource.class);

    @Inject
    private SecurityConfigurationTableRepository securityConfigurationTableRepository;

    @Inject
    private SecurityConfigurationTableSearchRepository securityConfigurationTableSearchRepository;

    /**
     * POST  /securityConfigurationTables -> Create a new securityConfigurationTable.
     */
    @RequestMapping(value = "/securityConfigurationTables",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecurityConfigurationTable> createSecurityConfigurationTable(@RequestBody SecurityConfigurationTable securityConfigurationTable) throws URISyntaxException {
        log.debug("REST request to save SecurityConfigurationTable : {}", securityConfigurationTable);
        if (securityConfigurationTable.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new securityConfigurationTable cannot already have an ID").body(null);
        }
        SecurityConfigurationTable result = securityConfigurationTableRepository.save(securityConfigurationTable);
        securityConfigurationTableSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/securityConfigurationTables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("securityConfigurationTable", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /securityConfigurationTables -> Updates an existing securityConfigurationTable.
     */
    @RequestMapping(value = "/securityConfigurationTables",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecurityConfigurationTable> updateSecurityConfigurationTable(@RequestBody SecurityConfigurationTable securityConfigurationTable) throws URISyntaxException {
        log.debug("REST request to update SecurityConfigurationTable : {}", securityConfigurationTable);
        if (securityConfigurationTable.getId() == null) {
            return createSecurityConfigurationTable(securityConfigurationTable);
        }
        SecurityConfigurationTable result = securityConfigurationTableRepository.save(securityConfigurationTable);
        securityConfigurationTableSearchRepository.save(securityConfigurationTable);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("securityConfigurationTable", securityConfigurationTable.getId().toString()))
            .body(result);
    }

    /**
     * GET  /securityConfigurationTables -> get all the securityConfigurationTables.
     */
    @RequestMapping(value = "/securityConfigurationTables",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SecurityConfigurationTable>> getAllSecurityConfigurationTables(Pageable pageable)
        throws URISyntaxException {
        Page<SecurityConfigurationTable> page = securityConfigurationTableRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/securityConfigurationTables");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /securityConfigurationTables/:id -> get the "id" securityConfigurationTable.
     */
    @RequestMapping(value = "/securityConfigurationTables/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecurityConfigurationTable> getSecurityConfigurationTable(@PathVariable Long id) {
        log.debug("REST request to get SecurityConfigurationTable : {}", id);
        return Optional.ofNullable(securityConfigurationTableRepository.findOne(id))
            .map(securityConfigurationTable -> new ResponseEntity<>(
                securityConfigurationTable,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /securityConfigurationTables/:id -> delete the "id" securityConfigurationTable.
     */
    @RequestMapping(value = "/securityConfigurationTables/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSecurityConfigurationTable(@PathVariable Long id) {
        log.debug("REST request to delete SecurityConfigurationTable : {}", id);
        securityConfigurationTableRepository.delete(id);
        securityConfigurationTableSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("securityConfigurationTable", id.toString())).build();
    }

    /**
     * SEARCH  /_search/securityConfigurationTables/:query -> search for the securityConfigurationTable corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/securityConfigurationTables/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SecurityConfigurationTable> searchSecurityConfigurationTables(@PathVariable String query) {
        return StreamSupport
            .stream(securityConfigurationTableSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
