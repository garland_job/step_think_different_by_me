package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.ErpAuthorityFlow;
import gov.step.app.repository.ErpAuthorityFlowRepository;
import gov.step.app.repository.search.ErpAuthorityFlowSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ErpAuthorityFlow.
 */
@RestController
@RequestMapping("/api")
public class ErpAuthorityFlowResource {

    private final Logger log = LoggerFactory.getLogger(ErpAuthorityFlowResource.class);

    @Inject
    private ErpAuthorityFlowRepository erpAuthorityFlowRepository;

    @Inject
    private ErpAuthorityFlowSearchRepository erpAuthorityFlowSearchRepository;

    /**
     * POST  /erpAuthorityFlows -> Create a new erpAuthorityFlow.
     */
    @RequestMapping(value = "/erpAuthorityFlows",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErpAuthorityFlow> createErpAuthorityFlow(@Valid @RequestBody ErpAuthorityFlow erpAuthorityFlow) throws URISyntaxException {
        log.debug("REST request to save ErpAuthorityFlow : {}", erpAuthorityFlow);
        if (erpAuthorityFlow.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new erpAuthorityFlow cannot already have an ID").body(null);
        }
        ErpAuthorityFlow result = erpAuthorityFlowRepository.save(erpAuthorityFlow);
        erpAuthorityFlowSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/erpAuthorityFlows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("erpAuthorityFlow", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /erpAuthorityFlows -> Updates an existing erpAuthorityFlow.
     */
    @RequestMapping(value = "/erpAuthorityFlows",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErpAuthorityFlow> updateErpAuthorityFlow(@Valid @RequestBody ErpAuthorityFlow erpAuthorityFlow) throws URISyntaxException {
        log.debug("REST request to update ErpAuthorityFlow : {}", erpAuthorityFlow);
        if (erpAuthorityFlow.getId() == null) {
            return createErpAuthorityFlow(erpAuthorityFlow);
        }
        ErpAuthorityFlow result = erpAuthorityFlowRepository.save(erpAuthorityFlow);
        erpAuthorityFlowSearchRepository.save(erpAuthorityFlow);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("erpAuthorityFlow", erpAuthorityFlow.getId().toString()))
            .body(result);
    }

    /**
     * GET  /erpAuthorityFlows -> get all the erpAuthorityFlows.
     */
    @RequestMapping(value = "/erpAuthorityFlows",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ErpAuthorityFlow>> getAllErpAuthorityFlows(Pageable pageable)
        throws URISyntaxException {
        Page<ErpAuthorityFlow> page = erpAuthorityFlowRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/erpAuthorityFlows");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /erpAuthorityFlows/:id -> get the "id" erpAuthorityFlow.
     */
    @RequestMapping(value = "/erpAuthorityFlows/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ErpAuthorityFlow> getErpAuthorityFlow(@PathVariable Long id) {
        log.debug("REST request to get ErpAuthorityFlow : {}", id);
        return Optional.ofNullable(erpAuthorityFlowRepository.findOne(id))
            .map(erpAuthorityFlow -> new ResponseEntity<>(
                erpAuthorityFlow,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /erpAuthorityFlows/:id -> delete the "id" erpAuthorityFlow.
     */
    @RequestMapping(value = "/erpAuthorityFlows/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteErpAuthorityFlow(@PathVariable Long id) {
        log.debug("REST request to delete ErpAuthorityFlow : {}", id);
        erpAuthorityFlowRepository.delete(id);
        erpAuthorityFlowSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("erpAuthorityFlow", id.toString())).build();
    }

    /**
     * SEARCH  /_search/erpAuthorityFlows/:query -> search for the erpAuthorityFlow corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/erpAuthorityFlows/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ErpAuthorityFlow> searchErpAuthorityFlows(@PathVariable String query) {
        return StreamSupport
            .stream(erpAuthorityFlowSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /findAllErpAuthorityFlowByCategory/:fieldName/:fieldValue -> get employee by filter.
     */
    @RequestMapping(value = "/findAllErpAuthorityFlowByCategory/{authorityCategory}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ErpAuthorityFlow> findAllErpAuthorityFlowByCategory(@PathVariable String authorityCategory) throws Exception {
        log.debug("REST request to erpAuthorityFlowByActorName List : employeeInfo: {} ", authorityCategory);
        return erpAuthorityFlowRepository.findAllErpAuthorityFlowByCategory(authorityCategory);

    }
}
