package gov.step.app.web.rest.dto;

import gov.step.app.domain.*;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class JasperReportDTO {

    private User currentUser;
    private String currentRole;
    private Integer currentUserStatus;
    private InstEmployee currentEmployee;
    private Institute currentInstitute;

    public JasperReportDTO() {
    }

    public JasperReportDTO(User currentUser, String currentRole, Integer currentUserStatus, InstEmployee currentEmployee) {
        this.currentUser = currentUser;
        this.currentRole = currentRole;
        this.currentUserStatus = currentUserStatus;
        this.currentEmployee = currentEmployee;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public Integer getCurrentUserStatus() {
        return currentUserStatus;
    }

    public void setCurrentUserStatus(Integer currentUserStatus) {
        this.currentUserStatus = currentUserStatus;
    }

    public InstEmployee getCurrentEmployee() {
        return currentEmployee;
    }

    public void setCurrentEmployee(InstEmployee currentEmployee) {
        this.currentEmployee = currentEmployee;
    }

    public Institute getCurrentInstitute() {
        return currentInstitute;
    }

    public void setCurrentInstitute(Institute currentInstitute) {
        this.currentInstitute = currentInstitute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JasperReportDTO that = (JasperReportDTO) o;

        if (currentUser != null ? !currentUser.equals(that.currentUser) : that.currentUser != null) return false;
        if (currentRole != null ? !currentRole.equals(that.currentRole) : that.currentRole != null) return false;
        if (currentUserStatus != null ? !currentUserStatus.equals(that.currentUserStatus) : that.currentUserStatus != null)
            return false;
        if (currentEmployee != null ? !currentEmployee.equals(that.currentEmployee) : that.currentEmployee != null)
            return false;
        return !(currentInstitute != null ? !currentInstitute.equals(that.currentInstitute) : that.currentInstitute != null);

    }

    @Override
    public int hashCode() {
        int result = currentUser != null ? currentUser.hashCode() : 0;
        result = 31 * result + (currentRole != null ? currentRole.hashCode() : 0);
        result = 31 * result + (currentUserStatus != null ? currentUserStatus.hashCode() : 0);
        result = 31 * result + (currentEmployee != null ? currentEmployee.hashCode() : 0);
        result = 31 * result + (currentInstitute != null ? currentInstitute.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "JasperReportDTO{" +
            "currentUser=" + currentUser +
            ", currentRole='" + currentRole + '\'' +
            ", currentUserStatus=" + currentUserStatus +
            ", currentEmployee=" + currentEmployee +
            ", currentInstitute=" + currentInstitute +
            '}';
    }
}
