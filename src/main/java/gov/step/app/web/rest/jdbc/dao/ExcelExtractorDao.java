package gov.step.app.web.rest.jdbc.dao;

import gov.step.app.domain.DlBookInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuvo on 4/9/17.
 */
@Component
public class ExcelExtractorDao
{
    private final Logger logger = LoggerFactory.getLogger(ExcelExtractorDao.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ExcelExtractorDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void uploadExcelData(List<DlBookInfo> bookInfoList)
    {
        try
        {
            String sql = "INSERT INTO DL_BOOK_INFO (CUST_ID, NAME, AGE) VALUES (?, ?, ?)";

            for (DlBookInfo bookInfo : bookInfoList)
            {
                jdbcTemplate.update(sql, new Object[] { bookInfo.getAuthorName(), bookInfo.getBookId(),bookInfo.getCallNo()});
            }
        }
        catch(Exception ex)
        {
            logger.error("getEmployeeWiseDesignationCountByOrganization Msg: "+ex.getMessage());
        }

    }
}
