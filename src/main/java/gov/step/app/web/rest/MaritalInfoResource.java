package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.MaritalInfo;
import gov.step.app.repository.MaritalInfoRepository;
import gov.step.app.repository.search.MaritalInfoSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MaritalInfo.
 */
@RestController
@RequestMapping("/api")
public class MaritalInfoResource {

    private final Logger log = LoggerFactory.getLogger(MaritalInfoResource.class);

    @Inject
    private MaritalInfoRepository maritalInfoRepository;

    @Inject
    private MaritalInfoSearchRepository maritalInfoSearchRepository;

    /**
     * POST  /maritalInfos -> Create a new maritalInfo.
     */
    @RequestMapping(value = "/maritalInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MaritalInfo> createMaritalInfo(@RequestBody MaritalInfo maritalInfo) throws URISyntaxException {
        log.debug("REST request to save MaritalInfo : {}", maritalInfo);
        if (maritalInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new maritalInfo cannot already have an ID").body(null);
        }
        MaritalInfo result = maritalInfoRepository.save(maritalInfo);
        maritalInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/maritalInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("maritalInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /maritalInfos -> Updates an existing maritalInfo.
     */
    @RequestMapping(value = "/maritalInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MaritalInfo> updateMaritalInfo(@RequestBody MaritalInfo maritalInfo) throws URISyntaxException {
        log.debug("REST request to update MaritalInfo : {}", maritalInfo);
        if (maritalInfo.getId() == null) {
            return createMaritalInfo(maritalInfo);
        }
        MaritalInfo result = maritalInfoRepository.save(maritalInfo);
        maritalInfoSearchRepository.save(maritalInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("maritalInfo", maritalInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /maritalInfos -> get all the maritalInfos.
     */
    @RequestMapping(value = "/maritalInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<MaritalInfo>> getAllMaritalInfos(Pageable pageable)
        throws URISyntaxException {
        Page<MaritalInfo> page = maritalInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/maritalInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /maritalInfos/:id -> get the "id" maritalInfo.
     */
    @RequestMapping(value = "/maritalInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MaritalInfo> getMaritalInfo(@PathVariable Long id) {
        log.debug("REST request to get MaritalInfo : {}", id);
        return Optional.ofNullable(maritalInfoRepository.findOne(id))
            .map(maritalInfo -> new ResponseEntity<>(
                maritalInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /maritalInfos/:id -> delete the "id" maritalInfo.
     */
    @RequestMapping(value = "/maritalInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteMaritalInfo(@PathVariable Long id) {
        log.debug("REST request to delete MaritalInfo : {}", id);
        maritalInfoRepository.delete(id);
        maritalInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("maritalInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/maritalInfos/:query -> search for the maritalInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/maritalInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<MaritalInfo> searchMaritalInfos(@PathVariable String query) {
        return StreamSupport
            .stream(maritalInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
