package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.JasperReportPermission;
import gov.step.app.repository.JasperReportPermissionRepository;
import gov.step.app.repository.search.JasperReportPermissionSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JasperReportPermission.
 */
@RestController
@RequestMapping("/api")
public class JasperReportPermissionResource {

    private final Logger log = LoggerFactory.getLogger(JasperReportPermissionResource.class);

    @Inject
    private JasperReportPermissionRepository jasperReportPermissionRepository;

    @Inject
    private JasperReportPermissionSearchRepository jasperReportPermissionSearchRepository;

    /**
     * POST  /jasperReportPermissions -> Create a new jasperReportPermission.
     */
    @RequestMapping(value = "/jasperReportPermissions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JasperReportPermission> createJasperReportPermission(@RequestBody JasperReportPermission jasperReportPermission) throws URISyntaxException {
        log.debug("REST request to save JasperReportPermission : {}", jasperReportPermission);
        if (jasperReportPermission.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new jasperReportPermission cannot already have an ID").body(null);
        }
        JasperReportPermission result = jasperReportPermissionRepository.save(jasperReportPermission);
        jasperReportPermissionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/jasperReportPermissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("jasperReportPermission", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jasperReportPermissions -> Updates an existing jasperReportPermission.
     */
    @RequestMapping(value = "/jasperReportPermissions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JasperReportPermission> updateJasperReportPermission(@RequestBody JasperReportPermission jasperReportPermission) throws URISyntaxException {
        log.debug("REST request to update JasperReportPermission : {}", jasperReportPermission);
        if (jasperReportPermission.getId() == null) {
            return createJasperReportPermission(jasperReportPermission);
        }
        JasperReportPermission result = jasperReportPermissionRepository.save(jasperReportPermission);
        jasperReportPermissionSearchRepository.save(jasperReportPermission);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("jasperReportPermission", jasperReportPermission.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jasperReportPermissions -> get all the jasperReportPermissions.
     */
    @RequestMapping(value = "/jasperReportPermissions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<JasperReportPermission>> getAllJasperReportPermissions(Pageable pageable)
        throws URISyntaxException {
        Page<JasperReportPermission> page = jasperReportPermissionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jasperReportPermissions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jasperReportPermissions/:id -> get the "id" jasperReportPermission.
     */
    @RequestMapping(value = "/jasperReportPermissions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<JasperReportPermission> getJasperReportPermission(@PathVariable Long id) {
        log.debug("REST request to get JasperReportPermission : {}", id);
        return Optional.ofNullable(jasperReportPermissionRepository.findOne(id))
            .map(jasperReportPermission -> new ResponseEntity<>(
                jasperReportPermission,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /jasperReportPermissions/:id -> delete the "id" jasperReportPermission.
     */
    @RequestMapping(value = "/jasperReportPermissions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteJasperReportPermission(@PathVariable Long id) {
        log.debug("REST request to delete JasperReportPermission : {}", id);
        jasperReportPermissionRepository.delete(id);
        jasperReportPermissionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("jasperReportPermission", id.toString())).build();
    }

    /**
     * SEARCH  /_search/jasperReportPermissions/:query -> search for the jasperReportPermission corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/jasperReportPermissions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<JasperReportPermission> searchJasperReportPermissions(@PathVariable String query) {
        return StreamSupport
            .stream(jasperReportPermissionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
