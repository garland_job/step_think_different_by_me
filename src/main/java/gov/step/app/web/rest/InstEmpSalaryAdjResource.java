package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.InstEmpSalaryAdj;
import gov.step.app.repository.InstEmpSalaryAdjRepository;
import gov.step.app.repository.search.InstEmpSalaryAdjSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing InstEmpSalaryAdj.
 */
@RestController
@RequestMapping("/api")
public class InstEmpSalaryAdjResource {

    private final Logger log = LoggerFactory.getLogger(InstEmpSalaryAdjResource.class);

    @Inject
    private InstEmpSalaryAdjRepository instEmpSalaryAdjRepository;

    @Inject
    private InstEmpSalaryAdjSearchRepository instEmpSalaryAdjSearchRepository;

    /**
     * POST  /instEmpSalaryAdjs -> Create a new instEmpSalaryAdj.
     */
    @RequestMapping(value = "/instEmpSalaryAdjs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmpSalaryAdj> createInstEmpSalaryAdj(@RequestBody InstEmpSalaryAdj instEmpSalaryAdj) throws URISyntaxException {
        log.debug("REST request to save InstEmpSalaryAdj : {}", instEmpSalaryAdj);
        if (instEmpSalaryAdj.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new instEmpSalaryAdj cannot already have an ID").body(null);
        }
        InstEmpSalaryAdj result = instEmpSalaryAdjRepository.save(instEmpSalaryAdj);
        instEmpSalaryAdjSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/instEmpSalaryAdjs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("instEmpSalaryAdj", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /instEmpSalaryAdjs -> Updates an existing instEmpSalaryAdj.
     */
    @RequestMapping(value = "/instEmpSalaryAdjs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmpSalaryAdj> updateInstEmpSalaryAdj(@RequestBody InstEmpSalaryAdj instEmpSalaryAdj) throws URISyntaxException {
        log.debug("REST request to update InstEmpSalaryAdj : {}", instEmpSalaryAdj);
        if (instEmpSalaryAdj.getId() == null) {
            return createInstEmpSalaryAdj(instEmpSalaryAdj);
        }
        InstEmpSalaryAdj result = instEmpSalaryAdjRepository.save(instEmpSalaryAdj);
        instEmpSalaryAdjSearchRepository.save(instEmpSalaryAdj);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("instEmpSalaryAdj", instEmpSalaryAdj.getId().toString()))
            .body(result);
    }

    /**
     * GET  /instEmpSalaryAdjs -> get all the instEmpSalaryAdjs.
     */
    @RequestMapping(value = "/instEmpSalaryAdjs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<InstEmpSalaryAdj>> getAllInstEmpSalaryAdjs(Pageable pageable)
        throws URISyntaxException {
        Page<InstEmpSalaryAdj> page = instEmpSalaryAdjRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instEmpSalaryAdjs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /instEmpSalaryAdjs/:id -> get the "id" instEmpSalaryAdj.
     */
    @RequestMapping(value = "/instEmpSalaryAdjs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<InstEmpSalaryAdj> getInstEmpSalaryAdj(@PathVariable Long id) {
        log.debug("REST request to get InstEmpSalaryAdj : {}", id);
        return Optional.ofNullable(instEmpSalaryAdjRepository.findOne(id))
            .map(instEmpSalaryAdj -> new ResponseEntity<>(
                instEmpSalaryAdj,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /instEmpSalaryAdjs/:id -> delete the "id" instEmpSalaryAdj.
     */
    @RequestMapping(value = "/instEmpSalaryAdjs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteInstEmpSalaryAdj(@PathVariable Long id) {
        log.debug("REST request to delete InstEmpSalaryAdj : {}", id);
        instEmpSalaryAdjRepository.delete(id);
        instEmpSalaryAdjSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("instEmpSalaryAdj", id.toString())).build();
    }

    /**
     * SEARCH  /_search/instEmpSalaryAdjs/:query -> search for the instEmpSalaryAdj corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/instEmpSalaryAdjs/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<InstEmpSalaryAdj> searchInstEmpSalaryAdjs(@PathVariable String query) {
        return StreamSupport
            .stream(instEmpSalaryAdjSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
