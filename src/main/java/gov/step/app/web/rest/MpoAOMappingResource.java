package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.MpoAOMapping;
import gov.step.app.repository.MpoAOMappingRepository;
import gov.step.app.repository.search.MpoAOMappingSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MpoAOMapping.
 */
@RestController
@RequestMapping("/api")
public class MpoAOMappingResource {

    private final Logger log = LoggerFactory.getLogger(MpoAOMappingResource.class);

    @Inject
    private MpoAOMappingRepository mpoAOMappingRepository;

    @Inject
    private MpoAOMappingSearchRepository mpoAOMappingSearchRepository;

    /**
     * POST  /mpoAOMappings -> Create a new mpoAOMapping.
     */
    @RequestMapping(value = "/mpoAOMappings",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MpoAOMapping> createMpoAOMapping(@RequestBody MpoAOMapping mpoAOMapping) throws URISyntaxException {
        log.debug("REST request to save MpoAOMapping : {}", mpoAOMapping);
        if (mpoAOMapping.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new mpoAOMapping cannot already have an ID").body(null);
        }
        MpoAOMapping result = mpoAOMappingRepository.save(mpoAOMapping);
        mpoAOMappingSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/mpoAOMappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("mpoAOMapping", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /mpoAOMappings -> Updates an existing mpoAOMapping.
     */
    @RequestMapping(value = "/mpoAOMappings",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MpoAOMapping> updateMpoAOMapping(@RequestBody MpoAOMapping mpoAOMapping) throws URISyntaxException {
        log.debug("REST request to update MpoAOMapping : {}", mpoAOMapping);
        if (mpoAOMapping.getId() == null) {
            return createMpoAOMapping(mpoAOMapping);
        }
        MpoAOMapping result = mpoAOMappingRepository.save(mpoAOMapping);
        mpoAOMappingSearchRepository.save(mpoAOMapping);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("mpoAOMapping", mpoAOMapping.getId().toString()))
            .body(result);
    }

    /**
     * GET  /mpoAOMappings -> get all the mpoAOMappings.
     */
    @RequestMapping(value = "/mpoAOMappings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<MpoAOMapping>> getAllMpoAOMappings(Pageable pageable)
        throws URISyntaxException {
        Page<MpoAOMapping> page = mpoAOMappingRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/mpoAOMappings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /mpoAOMappings/:id -> get the "id" mpoAOMapping.
     */
    @RequestMapping(value = "/mpoAOMappings/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MpoAOMapping> getMpoAOMapping(@PathVariable Long id) {
        log.debug("REST request to get MpoAOMapping : {}", id);
        return Optional.ofNullable(mpoAOMappingRepository.findOne(id))
            .map(mpoAOMapping -> new ResponseEntity<>(
                mpoAOMapping,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /mpoAOMappings/:id -> delete the "id" mpoAOMapping.
     */
    @RequestMapping(value = "/mpoAOMappings/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteMpoAOMapping(@PathVariable Long id) {
        log.debug("REST request to delete MpoAOMapping : {}", id);
        mpoAOMappingRepository.delete(id);
        mpoAOMappingSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("mpoAOMapping", id.toString())).build();
    }

    /**
     * SEARCH  /_search/mpoAOMappings/:query -> search for the mpoAOMapping corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/mpoAOMappings/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<MpoAOMapping> searchMpoAOMappings(@PathVariable String query) {
        return StreamSupport
            .stream(mpoAOMappingSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
