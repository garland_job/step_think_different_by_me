package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.PgmsAppRetirmntCalculation;
import gov.step.app.repository.PgmsAppRetirmntCalculationRepository;
//import gov.step.app.repository.search.PgmsAppRetirmntCalculationSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing PgmsAppRetirmntCalculation.
 */
@RestController
@RequestMapping("/api")
public class PgmsAppRetirmntCalculationResource {

    private final Logger log = LoggerFactory.getLogger(PgmsAppRetirmntCalculationResource.class);

    @Inject
    private PgmsAppRetirmntCalculationRepository pgmsAppRetirmntCalculationRepository;

//    @Inject
//    private PgmsAppRetirmntCalculationSearchRepository pgmsAppRetirmntCalculationSearchRepository;

    /**
     * POST  /pgmsAppRetirmntCalculations -> Create a new pgmsAppRetirmntCalculation.
     */
    @RequestMapping(value = "/pgmsAppRetirmntCalculations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntCalculation> createPgmsAppRetirmntCalculation(@Valid @RequestBody PgmsAppRetirmntCalculation pgmsAppRetirmntCalculation) throws URISyntaxException {
        log.debug("REST request to save PgmsAppRetirmntCalculation : {}", pgmsAppRetirmntCalculation);
        if (pgmsAppRetirmntCalculation.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pgmsAppRetirmntCalculation cannot already have an ID").body(null);
        }
        PgmsAppRetirmntCalculation result = pgmsAppRetirmntCalculationRepository.save(pgmsAppRetirmntCalculation);
//        pgmsAppRetirmntCalculationSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pgmsAppRetirmntCalculations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pgmsAppRetirmntCalculation", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pgmsAppRetirmntCalculations -> Updates an existing pgmsAppRetirmntCalculation.
     */
    @RequestMapping(value = "/pgmsAppRetirmntCalculations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntCalculation> updatePgmsAppRetirmntCalculation(@Valid @RequestBody PgmsAppRetirmntCalculation pgmsAppRetirmntCalculation) throws URISyntaxException {
        log.debug("REST request to update PgmsAppRetirmntCalculation : {}", pgmsAppRetirmntCalculation);
        if (pgmsAppRetirmntCalculation.getId() == null) {
            return createPgmsAppRetirmntCalculation(pgmsAppRetirmntCalculation);
        }
        PgmsAppRetirmntCalculation result = pgmsAppRetirmntCalculationRepository.save(pgmsAppRetirmntCalculation);
//        pgmsAppRetirmntCalculationSearchRepository.save(pgmsAppRetirmntCalculation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pgmsAppRetirmntCalculation", pgmsAppRetirmntCalculation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pgmsAppRetirmntCalculations -> get all the pgmsAppRetirmntCalculations.
     */
    @RequestMapping(value = "/pgmsAppRetirmntCalculations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PgmsAppRetirmntCalculation>> getAllPgmsAppRetirmntCalculations(Pageable pageable)
        throws URISyntaxException {
        Page<PgmsAppRetirmntCalculation> page = pgmsAppRetirmntCalculationRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pgmsAppRetirmntCalculations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pgmsAppRetirmntCalculations/:id -> get the "id" pgmsAppRetirmntCalculation.
     */
    @RequestMapping(value = "/pgmsAppRetirmntCalculations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PgmsAppRetirmntCalculation> getPgmsAppRetirmntCalculation(@PathVariable Long id) {
        log.debug("REST request to get PgmsAppRetirmntCalculation : {}", id);
        return Optional.ofNullable(pgmsAppRetirmntCalculationRepository.findOne(id))
            .map(pgmsAppRetirmntCalculation -> new ResponseEntity<>(
                pgmsAppRetirmntCalculation,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pgmsAppRetirmntCalculations/:id -> delete the "id" pgmsAppRetirmntCalculation.
     */
    @RequestMapping(value = "/pgmsAppRetirmntCalculations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePgmsAppRetirmntCalculation(@PathVariable Long id) {
        log.debug("REST request to delete PgmsAppRetirmntCalculation : {}", id);
        pgmsAppRetirmntCalculationRepository.delete(id);
//        pgmsAppRetirmntCalculationSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pgmsAppRetirmntCalculation", id.toString())).build();
    }

    /**
     * SEARCH  /_search/pgmsAppRetirmntCalculations/:query -> search for the pgmsAppRetirmntCalculation corresponding
     * to the query.
     */
//    @RequestMapping(value = "/_search/pgmsAppRetirmntCalculations/{query}",
//        method = RequestMethod.GET,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public List<PgmsAppRetirmntCalculation> searchPgmsAppRetirmntCalculations(@PathVariable String query) {
//        return StreamSupport
//            .stream(pgmsAppRetirmntCalculationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
//            .collect(Collectors.toList());
//    }
}
