package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.*;
import gov.step.app.domain.enumeration.designationType;
import gov.step.app.domain.payroll.PrlSalaryStructureInfo;
import gov.step.app.repository.*;
import gov.step.app.repository.payroll.PrlSalaryStructureInfoRepository;
import gov.step.app.repository.search.HrEmpTransferApplInfoSearchRepository;
import gov.step.app.security.AuthoritiesConstants;
import gov.step.app.security.SecurityUtils;
import gov.step.app.service.EmployeeService;
import gov.step.app.service.HrmConversionService;
import gov.step.app.service.constnt.HRMManagementConstant;
import gov.step.app.service.util.MiscUtilities;
import gov.step.app.web.rest.dto.HrmApprovalDto;
import gov.step.app.web.rest.util.DateResource;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing HrEmpTransferApplInfo.
 */
@RestController
@RequestMapping("/api")
public class HrEmpTransferApplInfoResource {

    private final Logger log = LoggerFactory.getLogger(HrEmpTransferApplInfoResource.class);

    @Inject
    private HrEmpTransferApplInfoRepository hrEmpTransferApplInfoRepository;

    @Inject
    private HrEmpTransferApplInfoSearchRepository hrEmpTransferApplInfoSearchRepository;

    @Inject
    private HrEmpTransferApplInfoLogRepository hrEmpTransferApplInfoLogRepository;

    @Inject
    private HrmConversionService conversionService;

    @Inject
    private InstituteRepository instituteRepository;

    @Inject
    private EmployeeService employeeService;

    @Inject
    private HrEmployeeInfoRepository hrEmployeeInfoRepository;

    @Inject
    private HrEmploymentInfoRepository hrEmploymentInfoRepository;

    @Inject
    private HrEmployeeInfoLogRepository hrEmployeeInfoLogRepository;

    @Inject
    private PrlSalaryStructureInfoRepository prlSalaryStructureInfoRepository;

    MiscUtilities miscUtils = new MiscUtilities();

    DateResource dateResrc = new DateResource();

    @Inject
    InstEmployeeRepository instEmployeeRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private UserRepository userRepository;

    /**
     * POST  /hrEmpTransferApplInfos -> Create a new hrEmpTransferApplInfo.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpTransferApplInfo> createHrEmpTransferApplInfo(@Valid @RequestBody HrEmpTransferApplInfo hrEmpTransferApplInfo) throws URISyntaxException {
        log.debug("REST request to save HrEmpTransferApplInfo : {}", hrEmpTransferApplInfo);
        if (hrEmpTransferApplInfo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("hrEmpTransferApplInfo", "idexists", "A new hrEmpTransferApplInfo cannot already have an ID")).body(null);
        }

        // Check for Active Head of own department.
        boolean isActiveHead = employeeService.checkForEmployeeIsTheActiveHeadOfOwnDepartment(hrEmpTransferApplInfo.getEmployeeInfo());
        if(isActiveHead) {
            if(hrEmpTransferApplInfo.getEmployeeInfo().getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)) {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
            } else {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING);
            }
        } else {
            if(hrEmpTransferApplInfo.getLogStatus().equals(HRMManagementConstant.APPROVAL_STATUS_ADMIN_ENTRY)) {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            } else {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
            }
        }

        HrEmpTransferApplInfo result = hrEmpTransferApplInfoRepository.save(hrEmpTransferApplInfo);
        hrEmpTransferApplInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/hrEmpTransferApplInfos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("hrEmpTransferApplInfo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hrEmpTransferApplInfos -> Updates an existing hrEmpTransferApplInfo.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpTransferApplInfo> updateHrEmpTransferApplInfo(@Valid @RequestBody HrEmpTransferApplInfo hrEmpTransferApplInfo) throws URISyntaxException {
        log.debug("REST request to update HrEmpTransferApplInfo : {}", hrEmpTransferApplInfo);
        if (hrEmpTransferApplInfo.getId() == null) {
            return createHrEmpTransferApplInfo(hrEmpTransferApplInfo);
        }

        // Add LOG info for Approval Purpose.
        HrEmpTransferApplInfoLog logInfo = new HrEmpTransferApplInfoLog();
        HrEmpTransferApplInfo dbModelInfo = hrEmpTransferApplInfoRepository.findOne(hrEmpTransferApplInfo.getId());
        logInfo = conversionService.getLogFromSource(dbModelInfo);
        logInfo.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
        logInfo = hrEmpTransferApplInfoLogRepository.save(logInfo);

        hrEmpTransferApplInfo.setLogId(logInfo.getId());

        // Check for Active Head of own department.
        boolean isActiveHead = employeeService.checkForEmployeeIsTheActiveHeadOfOwnDepartment(hrEmpTransferApplInfo.getEmployeeInfo());
        if(isActiveHead) {
            if(hrEmpTransferApplInfo.getEmployeeInfo().getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)) {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
            } else {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING);
            }
        } else {
            if(hrEmpTransferApplInfo.getLogStatus().equals(HRMManagementConstant.APPROVAL_STATUS_ADMIN_ENTRY)) {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            } else {
                hrEmpTransferApplInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
            }
        }

        HrEmpTransferApplInfo result = hrEmpTransferApplInfoRepository.save(hrEmpTransferApplInfo);
        hrEmpTransferApplInfoSearchRepository.save(result);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("hrEmpTransferApplInfo", hrEmpTransferApplInfo.getId().toString()))
            .body(hrEmpTransferApplInfo);
    }

    /**
     * GET  /hrEmpTransferApplInfos -> get all the hrEmpTransferApplInfos.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<HrEmpTransferApplInfo>> getAllHrEmpTransferApplInfos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of HrEmpTransferApplInfos");
        Page<HrEmpTransferApplInfo> page = hrEmpTransferApplInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/hrEmpTransferApplInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /hrEmpTransferApplInfos/:id -> get the "id" hrEmpTransferApplInfo.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<HrEmpTransferApplInfo> getHrEmpTransferApplInfo(@PathVariable Long id) {
        log.debug("REST request to get HrEmpTransferApplInfo : {}", id);
        HrEmpTransferApplInfo hrEmpTransferApplInfo = hrEmpTransferApplInfoRepository.findOne(id);
        return Optional.ofNullable(hrEmpTransferApplInfo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /hrEmpTransferApplInfos/:id -> delete the "id" hrEmpTransferApplInfo.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteHrEmpTransferApplInfo(@PathVariable Long id) {
        log.debug("REST request to delete HrEmpTransferApplInfo : {}", id);
        hrEmpTransferApplInfoRepository.delete(id);
        hrEmpTransferApplInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("hrEmpTransferApplInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/hrEmpTransferApplInfos/:query -> search for the hrEmpTransferApplInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/hrEmpTransferApplInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpTransferApplInfo> searchHrEmpTransferApplInfos(@PathVariable String query) {
        log.debug("REST request to search HrEmpTransferApplInfos for query {}", query);
        return StreamSupport
            .stream(hrEmpTransferApplInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * GET  /hrEmpTransferApplInfos -> get all the hrEmpTransferApplInfo.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfos/my",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpTransferApplInfo> getAllHrModelInfosByCurrentEmployee()
        throws URISyntaxException
    {
        log.debug("REST request to get a page of hrEmpTransferApplInfos by LoggedIn Employee");
        List<HrEmpTransferApplInfo> modelInfoList = hrEmpTransferApplInfoRepository.findAllByEmployeeIsCurrentUser();

        return modelInfoList;
    }

    /**
     * GET  /HrEmpTransferApplInfosApprover/ -> process the approval request.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfosApprover",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> updateModelApproval(@Valid @RequestBody HrmApprovalDto approvalDto) {
        log.debug("REST request to Approve HrEmpTransferApplInfo POST: Type: {} ID: {}, comment : {}",approvalDto.getActionType(), approvalDto.getEntityId(), approvalDto.getLogComments());

        HrEmpTransferApplInfo modelInfo = hrEmpTransferApplInfoRepository.findOne(approvalDto.getEntityId());

        if(approvalDto.getActionType().equalsIgnoreCase(HRMManagementConstant.APPROVAL_LOG_STATUS_ACCEPT))
        {
            log.debug("REST request to APROVING ID: {}", approvalDto.getEntityId());
            modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_APPROVED);
            modelInfo.setActiveStatus(true);
            modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
            if(modelInfo.getLogId() != 0)
            {
                HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOne(modelInfo.getLogId());
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);
            }
        }
        else
        {
            log.debug("REST request to REJECTING ID: {}", approvalDto.getEntityId());
            if(modelInfo.getLogId() != 0)
            {
                HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                modelLog.setActionBy(modelInfo.getCreateBy());
                modelLog.setActionComments(approvalDto.getLogComments());
                modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);

                modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo.setActiveStatus(false);
                modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
            }
            else
            {
                modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_REJECTED);
                modelInfo.setActiveStatus(false);
                modelInfo.setLogComments(approvalDto.getLogComments());
                modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
            }
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("HrEmpTransferApplInfo", approvalDto.getEntityId().toString())).build();
    }

    /**
     * GET  /HrEmpTransferApplInfosApprover/:logStatus -> get address list by log status.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfosApprover/{logStatus}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getModelListByLogStatus(@PathVariable Long logStatus) {
        log.debug("REST request to Approve HrEmpTransferApplInfo List : logStatus: {} ",logStatus);
        List<HrEmpTransferApplInfo> modelList = hrEmpTransferApplInfoRepository.findAllByLogStatus(logStatus);

        HrEmpTransferApplInfoLog modelLogInfo = null;
        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        HrmApprovalDto dtoInfo = null;
        for(HrEmpTransferApplInfo modelInfo : modelList)
        {
            dtoInfo = new HrmApprovalDto();
            dtoInfo.setEntityObject(modelInfo);
            if(modelInfo.getLogId() != 0)
            {
                modelLogInfo = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                dtoInfo.setEntityLogObject(modelLogInfo);
            }
            modelDtoList.add(dtoInfo);
        }
        return modelDtoList;
    }

    /**
     * GET  /HrEmpTransferApplInfosApprover/log/:entityId -> load model and LogModel by entity id
     */
    @RequestMapping(value = "/hrEmpTransferApplInfosApprover/log/{entityId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public HrmApprovalDto getModelAndLogObjectByModelId(@PathVariable String entityId) {
        log.debug("REST request to Log HrEmpTransferApplInfo Model and Log of id: {} ",entityId);

        Long id = Long.parseLong(entityId);
        HrmApprovalDto approvalDto = new HrmApprovalDto();
        HrEmpTransferApplInfo modelInfo = hrEmpTransferApplInfoRepository.findOne(id);
        approvalDto.setEntityObject(modelInfo);
        approvalDto.setEntityId(id);
        approvalDto.setEntityName("TransferApplication");
        if(modelInfo.getLogId() != 0)
        {
            HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
            approvalDto.setEntityLogObject(modelLog);
        }
        return approvalDto;
    }

    /*
        New Approval Flow Start From here
     */

    @RequestMapping(value = "/hrEmpTransferApplInfoDeptHeadPendingApprovalList/{deptIds}",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getDepartmentHeadPendingApprovalList(@PathVariable String deptIds)
    {
        log.debug("hrEmpTransferApplInfoDeptHeadPendingApprovalList deptIdList: {}", deptIds);

        List<Long> deptIdList = miscUtils.splitStringByKeyAndConvertLongList(deptIds, "#");
        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        if(deptIdList.size()>0)
        {
            List<HrEmpTransferApplInfo> pendingModelList = hrEmpTransferApplInfoRepository.findAllModelsByDepartmentIdsAndPendingStatus(deptIdList,
                HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
            log.debug(" model deptIds: {}, pendingListSize: {}, logStatus: {}",deptIds, pendingModelList.size(), HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);

            HrmApprovalDto dtoInfo = null;
            HrEmpTransferApplInfoLog modelLogInfo = null;
            for (HrEmpTransferApplInfo modelInfo : pendingModelList) {
                dtoInfo = new HrmApprovalDto();
                dtoInfo.setEntityObject(modelInfo);
                if (modelInfo.getLogId() != 0) {
                    modelLogInfo = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                    dtoInfo.setEntityLogObject(modelLogInfo);
                }
                modelDtoList.add(dtoInfo);
            }
        }
        return modelDtoList;
    }

    @RequestMapping(value = "/hrEmpTransferApplInfoInstituteHeadPendingApprovalList",
        method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrmApprovalDto> getInstituteHeadPendingApprovalList()
    {
        log.debug("hrEmpTransferApplInfoInstituteHeadPendingApprovalList");
        Institute institute = instituteRepository.findOneByUserIsCurrentUser();

        log.debug("institute id {}", institute.getId());
        List<HrEmpTransferApplInfo> pendingModelList = hrEmpTransferApplInfoRepository.findAllModelsByInstituteAndPendingStatus(
            HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST,
            institute.getId(),
            HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);

        List<HrmApprovalDto> modelDtoList = new ArrayList<HrmApprovalDto>();
        HrmApprovalDto dtoInfo = null;
        HrEmpTransferApplInfoLog modelLogInfo = null;
        for (HrEmpTransferApplInfo modelInfo : pendingModelList) {
            dtoInfo = new HrmApprovalDto();
            dtoInfo.setEntityObject(modelInfo);
            if (modelInfo.getLogId() != 0) {
                modelLogInfo = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                dtoInfo.setEntityLogObject(modelLogInfo);
            }
            modelDtoList.add(dtoInfo);
        }

        log.debug("institute employee list: {}", modelDtoList.size());

        return modelDtoList;
    }

    /**
     * GET  /hrEmpTransferApplInfoAdminApprovalListByLogStatus
     */
    @RequestMapping(value = "/hrEmpTransferApplInfoAdminApprovalListByLogStatus",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<HrEmpTransferApplInfo> hrEmployeeInfosAdminApprovalListByLogStatus() throws Exception
    {
        log.debug("REST request to hrEmpTransferApplInfoAdminApprovalListByLogStatus List status  DeptPend: {}, DeptRej: {}, InstPend: {}, InstRej: {} ",
            HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING,
            HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED,
            HRMManagementConstant.APPROVAL_INST_HEAD_PENDING,
            HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);

        List<Long> logStatusList = new ArrayList<Long>();
        logStatusList.add(HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING);
        logStatusList.add(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
        logStatusList.add(HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
        logStatusList.add(HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);

        List<HrEmpTransferApplInfo> modelList = hrEmpTransferApplInfoRepository.findAllModelByLogStatuses(logStatusList);
        log.debug("List Len:  ", modelList.size());
        return modelList;
    }

    /**
     * GET  /hrEmpTransferApplInfoUpdateRequestApprove/ -> approve the TransferApplication information update request.
     */
    @RequestMapping(value = "/hrEmpTransferApplInfoUpdateRequestApprove",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> modelInfoUpdateRequestApproveHandler(@Valid @RequestBody HrEmpTransferApplInfo modelInfo)
    {
        log.debug("REST hrEmpTransferApplInfoUpdateRequestApprove orgType: {}",modelInfo.getEmployeeInfo().getOrganizationType());

        modelInfo.setUpdateDate(dateResrc.getDateAsLocalDate());
        modelInfo.setUpdateBy(SecurityUtils.getCurrentUserId());

        HrEmploymentInfo hrEmploymentInfo = null;
        PrlSalaryStructureInfo salaryStructureInfo = null;
        Long employmentId = 0l;
        Long salaryStructureId = 0l;
        Long instituteCatId=0l, instituteId=0l, orgCatId=0l, orgId=0l, deptId=0l, desigId=0l,instituteLevelId=0l;
        String organizationType = "";
        designationType emplType = designationType.DTE_Employee;
        InstEmployee instEmployee = new InstEmployee();

        Authority govtNonGovtInstAuthority  = authorityRepository.findOne(HRMManagementConstant.ROLE_INST_EMPLOYEE_NAME);
        Authority hrmEmployeeAuthority      = authorityRepository.findOne(HRMManagementConstant.ROLE_HRM_USER_NAME);

        if(modelInfo.getEmployeeInfo().getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG))
        {
            if(modelInfo.getLogComments().equalsIgnoreCase(HRMManagementConstant.EMP_DATA_UPDATE_APPROVAL_APPROVED))
            {
                log.debug("REST TransferAppl Approval -> ORG-> DEPT_HEAD APPROVED logStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_APPROVED);
                if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> ORG -> DEPT_HEAD TO SYS_ADMIN Approval LogStatus: {} ", HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING);
                    modelInfo.setActiveStatus(true);
                    modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> ORG -> DEPT_HEAD -> SYS_ADMIN - APPROVED LogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setActiveStatus(true);
                    modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);

                    try{
                        hrEmploymentInfo = hrEmploymentInfoRepository.findByEmployeeId(modelInfo.getEmployeeInfo().getId());
                        //salaryStructureInfo = prlSalaryStructureInfoRepository.finAlldByEmployeeId(modelInfo.getEmployeeInfo().getId());
                        employmentId    = hrEmploymentInfo.getId();
                        //salaryStructureId   = salaryStructureInfo.getId();
                    }catch(Exception ex){
                        employmentId = 0l;
                        salaryStructureId = 0l;
                        log.error("Employee and Salary Structure Update Exception", ex);
                    }

                    if(modelInfo.getSelectedOption()==1)
                    {
                        if(modelInfo.getOrgTypeOptOne() != null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                            instituteLevelId = modelInfo.getInstituteOptOne().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptOne().getId();
                        }else if(modelInfo.getOrgTypeOptOne() != null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionOne().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionOne().getId();
                        }
                        deptId = modelInfo.getDepartInfoOptOne().getId();
                        desigId = modelInfo.getDesigOptionOne().getId();
                        organizationType = modelInfo.getOrgTypeOptOne();
                        emplType        = designationType.DTE_Employee;
                    }
                    else if(modelInfo.getSelectedOption()==2)
                    {
                        if(modelInfo.getOrgTypeOptTwo() !=null && modelInfo.getOrgTypeOptTwo().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptTwo().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptTwo().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptTwo().getId();
                        }else if(modelInfo.getOrgTypeOptTwo() !=null && modelInfo.getOrgTypeOptTwo().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionTwo().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionTwo().getId();
                        }
                        //instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                        //instituteId     = modelInfo.getInstituteOptOne().getId();
//                        orgCatId        = modelInfo.getOrgOptionTwo().getWorkArea().getId();
//                        orgId           = modelInfo.getOrgOptionTwo().getId();
                        deptId          = modelInfo.getDepartInfoOptTwo().getId();
                        desigId         = modelInfo.getDesigOptionTwo().getId();
                        organizationType = modelInfo.getOrgTypeOptTwo();
                        emplType        = designationType.DTE_Employee;
                    }
                    else if(modelInfo.getSelectedOption()==3)
                    {
                        if(modelInfo.getOrgTypeOptThree() !=null && modelInfo.getOrgTypeOptThree().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptThree().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptThree().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptThree().getId();
                        }else if(modelInfo.getOrgTypeOptThree() !=null && modelInfo.getOrgTypeOptThree().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionThree().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionThree().getId();
                        }
                        //instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                        //instituteId     = modelInfo.getInstituteOptOne().getId();
//                        orgCatId        = modelInfo.getOrgOptionThree().getWorkArea().getId();
//                        orgId           = modelInfo.getOrgOptionThree().getId();
                        deptId          = modelInfo.getDepartInfoOptThree().getId();
                        desigId         = modelInfo.getDesigOptionThree().getId();
                        organizationType = modelInfo.getOrgTypeOptThree();
                        emplType        = designationType.DTE_Employee;
                    } else if(modelInfo.getSelectedOption()==4)
                    {
                        if(modelInfo.getOrgTypeOptFour() !=null && modelInfo.getOrgTypeOptFour().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptFour().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptFour().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptFour().getId();
                        }else if(modelInfo.getOrgTypeOptFour() !=null && modelInfo.getOrgTypeOptFour().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionFour().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionFour().getId();
                        }
                        //instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                        //instituteId     = modelInfo.getInstituteOptOne().getId();
//                        orgCatId        = modelInfo.getOrgOptionThree().getWorkArea().getId();
//                        orgId           = modelInfo.getOrgOptionThree().getId();
                        deptId          = modelInfo.getDepartInfoOptFour().getId();
                        desigId         = modelInfo.getDesigOptionFour().getId();
                        organizationType = modelInfo.getOrgTypeOptFour();
                        emplType        = designationType.DTE_Employee;
                    }
                    else
                    {
                        if(modelInfo.getOrgTypeOptOne() !=null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptOne().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptOne().getId();
                        }else if(modelInfo.getOrgTypeOptThree() !=null && modelInfo.getOrgTypeOptThree().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionOne().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionOne().getId();
                        }
                        //instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                        //instituteId     = modelInfo.getInstituteOptOne().getId();
//                        orgCatId        = modelInfo.getOrgOptionOne().getWorkArea().getId();
//                        orgId           = modelInfo.getOrgOptionOne().getId();
                        deptId          = modelInfo.getDepartInfoOptOne().getId();
                        desigId         = modelInfo.getDesigOptionOne().getId();
                        organizationType = modelInfo.getOrgTypeOptOne();
                        emplType        = designationType.DTE_Employee;
                    }

                    // Add Transfer related updates here.
                    // Update employee information
                    // Update employment information

                    // If transfer type not equals WORK_PLACE
                    if ( modelInfo.getTransferType().getTypeCode()!=null
                        && ! modelInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_WORK_PLACE))
                    {

                        // Save employee information into LOG .
                        HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
                        HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(modelInfo.getEmployeeInfo().getId());
                        logInfo = conversionService.getLogFromSource(dbModelInfo);

                        logInfo.setLogStatus(HRMManagementConstant.TRANSFER_APPLICATION_APPROVE);
                        logInfo.setActionComments(HRMManagementConstant.TRANSFER_APPL_ACTION_COMMENTS);
                        logInfo.setCreateDate(dateResrc.getDateAsLocalDate());
                        logInfo = hrEmployeeInfoLogRepository.save(logInfo);

                        // update fields value for transfer within employee entity

                        if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){

                            if(dbModelInfo.getOrganizationType().equals("Institute") && dbModelInfo.getInstitute()!=null
                                && dbModelInfo.getInstEmployee()!=null){
                                InstEmployee instEmployeeDeactivate= dbModelInfo.getInstEmployee();
                                instEmployeeDeactivate.setStatus(-5);
                                instEmployeeDeactivate.setInstitute(null);
                                instEmployeeRepository.save(instEmployeeDeactivate);
                            }
                            hrEmployeeInfoRepository.updateEmployeeForDteTransfer(modelInfo.getEmployeeInfo().getId(), emplType, organizationType,
                                orgCatId, orgId, deptId, desigId, logInfo.getId());
                            HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(modelInfo.getEmployeeInfo().getId());
                            User user = hrEmployeeInfo.getUser();
                            Set<Authority> authorities = user.getAuthorities();
                            if(authorities.contains(govtNonGovtInstAuthority)){
                                authorities.remove(govtNonGovtInstAuthority);
                            }
                            if(!authorities.contains(hrmEmployeeAuthority)){
                                authorities.add(hrmEmployeeAuthority);
                            }
                            user.setAuthorities(authorities);
                            userRepository.save(user);

                        }else if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            try {
                                hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(modelInfo.getEmployeeInfo().getId(), emplType,
                                    organizationType,
                                    instituteCatId,
                                    instituteLevelId,
                                    instituteId,
                                    deptId, desigId, logInfo.getId());
                                HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(modelInfo.getEmployeeInfo().getId());
                                instEmployee = conversionService.hrEmpToInstEmpConversion(hrEmployeeInfo);
                                InstEmployee instEmp =  instEmployeeRepository.save(instEmployee);
                                hrEmployeeInfo.setInstEmployee(instEmp);
                                hrEmployeeInfoRepository.save(hrEmployeeInfo);

                                User user = hrEmployeeInfo.getUser();
                                Set<Authority> authorities = user.getAuthorities();
                                if(!authorities.contains(govtNonGovtInstAuthority)){
                                    authorities.add(govtNonGovtInstAuthority);
                                }
                                if(!authorities.contains(hrmEmployeeAuthority)){
                                    authorities.add(hrmEmployeeAuthority);
                                }
                                user.setAuthorities(authorities);
                                userRepository.save(user);
                            }catch (Exception ex){
                                log.debug("Hrm to Inst Employee Conversion Failure " +ex.getMessage());
                            }
                        }
                    }

                    try{
                        if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            hrEmploymentInfoRepository.updateCurrentEmploymentInformationOfOrganization(employmentId, organizationType,
                                orgId,deptId, desigId);
                        }else if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            hrEmploymentInfoRepository.updateCurrentEmploymentInformationInsitute(employmentId, organizationType,
                                instituteId, deptId, desigId);
                        }

                    }catch(Exception ex){
                        log.error("Organization Employment Update Exception", ex);
                    }

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOne(modelInfo.getLogId());
                        if (modelLog != null) {
                            modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                            modelLog.setActionBy(modelInfo.getCreateBy());
                            modelLog.setActionComments(modelInfo.getLogComments());
                            modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);
                        }
                    }
                }
            }
            else {

                if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING)
                {
                    log.debug("REST TransferAppl Update -> ORG -> DEPT_HEAD REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    }
                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> ORG -> DEPT_HEAD -> SYS_ADMIN -> REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    }
                }
            }
        }
        else if(modelInfo.getEmployeeInfo().getOrganizationType().equalsIgnoreCase(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST))
        {
            if(modelInfo.getLogComments().equalsIgnoreCase(HRMManagementConstant.EMP_DATA_UPDATE_APPROVAL_APPROVED))
            {
                if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> INST TO DEPT_HEAD  APPROVAL LogStatus: {} ", HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_PENDING);
                    modelInfo.setActiveStatus(false);
                    modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_INST_HEAD_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> INST -> INST_HEAD TO SYS_ADMIN Approval LogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING);
                    modelInfo.setActiveStatus(true);
                    modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> INST -> INST_HEAD -> SYS_ADMIN - APPROVED LogStatus: {} ", HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_STATUS_FREE);
                    modelInfo.setActiveStatus(true);
                    modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);

                    // Add Transfer related updates here.
                    // Update employee information
                    // Update employment information

                    try{
                        hrEmploymentInfo = hrEmploymentInfoRepository.findByEmployeeId(modelInfo.getEmployeeInfo().getId());
                        //salaryStructureInfo = prlSalaryStructureInfoRepository.finAlldByEmployeeId(modelInfo.getEmployeeInfo().getId());
                        employmentId    = hrEmploymentInfo.getId();
                        //salaryStructureId   = salaryStructureInfo.getId();
                    }catch(Exception ex){
                        employmentId = 0l;
                        salaryStructureId = 0l;
                        log.error("Employee and Salary Structure Update Exception", ex);
                    }

                    if(modelInfo.getSelectedOption()==1)
                    {
                        if(modelInfo.getOrgTypeOptOne() !=null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptOne().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptOne().getId();
                        }else if(modelInfo.getOrgTypeOptOne() !=null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionOne().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionOne().getId();
                        }
                        deptId          = modelInfo.getDepartInfoOptOne().getId();
                        desigId         = modelInfo.getDesigOptionOne().getId();
                        organizationType = modelInfo.getOrgTypeOptOne();
                        emplType        = designationType.Teacher;
                    }
                    else if(modelInfo.getSelectedOption()==2)
                    {
                        if(modelInfo.getOrgTypeOptTwo() !=null && modelInfo.getOrgTypeOptTwo().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptTwo().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptTwo().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptTwo().getId();
                        }else if(modelInfo.getOrgTypeOptTwo() !=null && modelInfo.getOrgTypeOptTwo().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionTwo().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionTwo().getId();
                        }
//                        instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
//                        instituteId     = modelInfo.getInstituteOptOne().getId();
                        //orgCatId        = modelInfo.getOrgOptionTwo().getWorkArea().getId();
                        //orgId           = modelInfo.getOrgOptionTwo().getId();
                        deptId          = modelInfo.getDepartInfoOptTwo().getId();
                        desigId         = modelInfo.getDesigOptionTwo().getId();
                        organizationType = modelInfo.getOrgTypeOptTwo();
                        emplType        = designationType.Teacher;
                    }
                    else if(modelInfo.getSelectedOption()==3)
                    {
                        if(modelInfo.getOrgTypeOptThree() !=null && modelInfo.getOrgTypeOptThree().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptThree().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptThree().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptThree().getId();
                        }else if(modelInfo.getOrgTypeOptThree() !=null && modelInfo.getOrgTypeOptThree().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionThree().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionThree().getId();
                        }
//                        instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
//                        instituteId     = modelInfo.getInstituteOptOne().getId();
                        //orgCatId        = modelInfo.getOrgOptionThree().getWorkArea().getId();
                        //orgId           = modelInfo.getOrgOptionThree().getId();
                        deptId          = modelInfo.getDepartInfoOptThree().getId();
                        desigId         = modelInfo.getDesigOptionThree().getId();
                        organizationType = modelInfo.getOrgTypeOptThree();
                        emplType        = designationType.Teacher;
                    }else if(modelInfo.getSelectedOption()==4)
                    {
                        if(modelInfo.getOrgTypeOptFour() !=null && modelInfo.getOrgTypeOptFour().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptFour().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptFour().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptFour().getId();
                        }else if(modelInfo.getOrgTypeOptFour() !=null && modelInfo.getOrgTypeOptFour().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionFour().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionFour().getId();
                        }
//                        instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
//                        instituteId     = modelInfo.getInstituteOptOne().getId();
                        //orgCatId        = modelInfo.getOrgOptionThree().getWorkArea().getId();
                        //orgId           = modelInfo.getOrgOptionThree().getId();
                        deptId          = modelInfo.getDepartInfoOptFour().getId();
                        desigId         = modelInfo.getDesigOptionFour().getId();
                        organizationType = modelInfo.getOrgTypeOptFour();
                        emplType        = designationType.Teacher;
                    }
                    else
                    {
                        if(modelInfo.getOrgTypeOptOne() !=null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
                            instituteLevelId  = modelInfo.getInstituteOptOne().getInstLevel().getId();
                            instituteId     = modelInfo.getInstituteOptOne().getId();
                        }else if(modelInfo.getOrgTypeOptOne() !=null && modelInfo.getOrgTypeOptOne().equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            orgCatId        = modelInfo.getOrgOptionOne().getWorkArea().getId();
                            orgId           = modelInfo.getOrgOptionOne().getId();
                        }
//                        instituteCatId  = modelInfo.getInstituteOptOne().getInstCategory().getId();
//                        instituteId     = modelInfo.getInstituteOptOne().getId();
                        //orgCatId        = modelInfo.getOrgOptionOne().getWorkArea().getId();
                        //orgId           = modelInfo.getOrgOptionOne().getId();
                        deptId          = modelInfo.getDepartInfoOptOne().getId();
                        desigId         = modelInfo.getDesigOptionOne().getId();
                        organizationType = modelInfo.getOrgTypeOptOne();
                        emplType        = designationType.Teacher;
                    }

                    // Add Transfer related updates here.
                    // Update employee information
                    // Update employment information

                    // If transfer type not equals WORK_PLACE
                    if ( modelInfo.getTransferType().getTypeCode()!=null
                        && ! modelInfo.getTransferType().getTypeCode().equalsIgnoreCase(HRMManagementConstant.TRANSFER_TYPE_WORK_PLACE))
                    {

                        // Save employee information into LOG .
                        HrEmployeeInfoLog logInfo = new HrEmployeeInfoLog();
                        HrEmployeeInfo dbModelInfo = hrEmployeeInfoRepository.findOne(modelInfo.getEmployeeInfo().getId());
                        logInfo = conversionService.getLogFromSource(dbModelInfo);

                        logInfo.setLogStatus(HRMManagementConstant.TRANSFER_APPLICATION_APPROVE);
                        logInfo.setActionComments(HRMManagementConstant.TRANSFER_APPL_ACTION_COMMENTS);
                        logInfo.setCreateDate(dateResrc.getDateAsLocalDate());
                        logInfo = hrEmployeeInfoLogRepository.save(logInfo);

                        // update fields value for transfer within employee entity
//                        hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(modelInfo.getEmployeeInfo().getId(), emplType,
//                            organizationType,
//                            instituteCatId, instituteId,
//                            deptId, desigId, logInfo.getId());

                        if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            hrEmployeeInfoRepository.updateEmployeeForDteTransfer(modelInfo.getEmployeeInfo().getId(), emplType, organizationType,
                                orgCatId, orgId, deptId, desigId, logInfo.getId());
                        }else if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            try {
                                hrEmployeeInfoRepository.updateEmployeeFromDteToInstituteTransfer(modelInfo.getEmployeeInfo().getId(), emplType,
                                                                                                    organizationType,
                                                                                                    instituteCatId,instituteLevelId ,instituteId,
                                                                                                    deptId, desigId, logInfo.getId());
                                HrEmployeeInfo hrEmployeeInfo = hrEmployeeInfoRepository.findOne(modelInfo.getEmployeeInfo().getId());
                                instEmployee = conversionService.hrEmpToInstEmpConversion(hrEmployeeInfo);
                                InstEmployee instEmp =  instEmployeeRepository.save(instEmployee);
                                hrEmployeeInfo.setInstEmployee(instEmp);
                                hrEmployeeInfoRepository.save(hrEmployeeInfo);
                            }catch (Exception ex){
                                log.debug("Hrm to Inst Employee Conversion Failure " +ex.getMessage());
                            }
                        }
                    }

                    try{
//                        hrEmploymentInfoRepository.updateCurrentEmploymentInformationInsitute(employmentId, organizationType,
//                            instituteCatId, deptId, desigId);
                        if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_ORG)){
                            hrEmploymentInfoRepository.updateCurrentEmploymentInformationOfOrganization(employmentId, organizationType,
                                orgId,deptId, desigId);
                        }else if (organizationType.equals(HRMManagementConstant.EMP_ORGANIZATION_TYPE_INST)){
                            hrEmploymentInfoRepository.updateCurrentEmploymentInformationInsitute(employmentId, organizationType,
                                instituteId, deptId, desigId);
                        }
                    }catch(Exception ex){
                        log.error("Institute Employment Update Exception", ex);
                    }

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOne(modelInfo.getLogId());
                        if (modelLog != null) {
                            modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_APPROVED);
                            modelLog.setActionBy(modelInfo.getCreateBy());
                            modelLog.setActionComments(modelInfo.getLogComments());
                            modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);
                        }
                    }
                }
            }
            else
            {
                if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_DEPT_HEAD_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> INST -> DEPT_HEAD -> REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_DEPT_HEAD_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    }

                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_INST_HEAD_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> INST -> INST_HEAD -> REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_INST_HEAD_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    }
                }
                else if(modelInfo.getLogStatus()==HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_PENDING)
                {
                    log.debug("REST TransferAppl UpdateApproval -> INST -> INST_HEAD -> SysAdmin -> REJECTED LogStatus: {} ", HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_REJECTED);

                    if (modelInfo.getLogId() != 0) {
                        HrEmpTransferApplInfoLog modelLog = hrEmpTransferApplInfoLogRepository.findOneByIdAndLogStatus(modelInfo.getLogId(), HRMManagementConstant.APPROVAL_LOG_STATUS_ACTIVE);
                        modelLog.setLogStatus(HRMManagementConstant.APPROVAL_LOG_STATUS_ROLLBACKED);
                        modelLog.setActionBy(modelInfo.getCreateBy());
                        modelLog.setActionComments(modelInfo.getLogComments());
                        modelLog = hrEmpTransferApplInfoLogRepository.save(modelLog);

                        modelInfo = conversionService.getModelFromLog(modelLog, modelInfo);
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_REJECTED);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo.setActiveStatus(false);
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    } else {
                        modelInfo.setLogStatus(HRMManagementConstant.APPROVAL_SYSTEM_ADMIN_REJECTED);
                        modelInfo.setActiveStatus(false);
                        modelInfo.setLogComments(modelInfo.getLogComments());
                        modelInfo = hrEmpTransferApplInfoRepository.save(modelInfo);
                    }
                }
            }
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("modelInfo", modelInfo.getId().toString())).build();
    }
}
