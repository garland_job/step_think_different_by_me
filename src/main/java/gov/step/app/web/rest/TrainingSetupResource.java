package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.TrainingSetup;
import gov.step.app.domain.User;
import gov.step.app.repository.TrainingSetupRepository;
import gov.step.app.repository.UserRepository;
import gov.step.app.repository.search.TrainingSetupSearchRepository;
import gov.step.app.security.SecurityUtils;
import gov.step.app.web.rest.util.DateResource;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import gov.step.app.web.rest.util.TransactionIdResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TrainingSetup.
 */
@RestController
@RequestMapping("/api")
public class TrainingSetupResource {

    private final Logger log = LoggerFactory.getLogger(TrainingSetupResource.class);

    @Inject
    private TrainingSetupRepository trainingSetupRepository;

    @Inject
    private TrainingSetupSearchRepository trainingSetupSearchRepository;

    @Inject
    private UserRepository userRepository;

    DateResource dr=new DateResource();
    TransactionIdResource transactionId =new TransactionIdResource();

    /**
     * POST  /trainingSetups -> Create a new trainingSetup.
     */
    @RequestMapping(value = "/trainingSetups",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TrainingSetup> createTrainingSetup(@RequestBody TrainingSetup trainingSetup) throws URISyntaxException {
        log.debug("REST request to save TrainingSetup : {}", trainingSetup);
        if (trainingSetup.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new trainingSetup cannot already have an ID").body(null);
        }

        User user = userRepository.getOne(SecurityUtils.getCurrentUserId());
        trainingSetup.setCreateBy(user);
        trainingSetup.setCreateDate(dr.getDateAsLocalDate());
        trainingSetup.setTrainId(transactionId.getGeneratedid("Train-"));

        TrainingSetup result = trainingSetupRepository.save(trainingSetup);
        trainingSetupSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/trainingSetups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("trainingSetup", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trainingSetups -> Updates an existing trainingSetup.
     */
    @RequestMapping(value = "/trainingSetups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TrainingSetup> updateTrainingSetup(@RequestBody TrainingSetup trainingSetup) throws URISyntaxException {
        log.debug("REST request to update TrainingSetup : {}", trainingSetup);
        if (trainingSetup.getId() == null) {
            return createTrainingSetup(trainingSetup);
        }

        trainingSetup.setUpdateBy(SecurityUtils.getCurrentUserId());
        trainingSetup.setUpdateDate(dr.getDateAsLocalDate());

        TrainingSetup result = trainingSetupRepository.save(trainingSetup);
        trainingSetupSearchRepository.save(trainingSetup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("trainingSetup", trainingSetup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trainingSetups -> get all the trainingSetups.
     */
    @RequestMapping(value = "/trainingSetups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TrainingSetup>> getAllTrainingSetups(Pageable pageable)
        throws URISyntaxException {
        Page<TrainingSetup> page = trainingSetupRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trainingSetups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /trainingSetups/:id -> get the "id" trainingSetup.
     */
    @RequestMapping(value = "/trainingSetups/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TrainingSetup> getTrainingSetup(@PathVariable Long id) {
        log.debug("REST request to get TrainingSetup : {}", id);
        return Optional.ofNullable(trainingSetupRepository.findOne(id))
            .map(trainingSetup -> new ResponseEntity<>(
                trainingSetup,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /trainingSetups/:id -> delete the "id" trainingSetup.
     */
    @RequestMapping(value = "/trainingSetups/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTrainingSetup(@PathVariable Long id) {
        log.debug("REST request to delete TrainingSetup : {}", id);
        trainingSetupRepository.delete(id);
        trainingSetupSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("trainingSetup", id.toString())).build();
    }

    /**
     * SEARCH  /_search/trainingSetups/:query -> search for the trainingSetup corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/trainingSetups/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TrainingSetup> searchTrainingSetups(@PathVariable String query) {
        return StreamSupport
            .stream(trainingSetupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
