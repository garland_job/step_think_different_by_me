package gov.step.app.web.rest.jdbc.dao;

import gov.step.app.domain.AlmAttendanceInformation;
import gov.step.app.domain.payroll.*;
import gov.step.app.service.constnt.PayrollManagementConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.util.List;

/**
 * Created by Mostafiz on 3/24/17.
 */
@Component
public class AlmJdbcDao
{
    private final Logger logger = LoggerFactory.getLogger(AlmJdbcDao.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AlmJdbcDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private final String SQL_SELECT_ALL_ATTENDANCE_INFO_BY_HOLIDAY			= "SELECT * FROM ALM_ATTENDANCE_INFORMATION AAI WHERE AAI.ATTENDENCE_DATE BETWEEN ? AND ? ";

    private final String SQL_INSERT_EMP_ATT_GEN_PROC    = "{call ATTENDENCE_PACK.GEN_PRILI_EMP_ATT_DATA(?, ?, ?)}";
    private final String SQL_UPDATE_ATT_SHIFT_GEN_PROC    = "{call ATTENDENCE_PACK.UPDATE_ATTENDENCE_INFO( ?, ?, ?)}";
    private final String SQL_UPDATE_HOLIDAY_PROC       = "{call ATTENDENCE_PACK.UPDATE_HOLIDAY_ATTENDENCE( ?, ?, ?, ? )}";

    public List<AlmAttendanceInformation> geAlmAttendanceInformationListByDate (Date fromDate, Date toDate){

        List<AlmAttendanceInformation> almAttendanceInformationList = null;
        try
        {
            //setJdbcTemplate();
            almAttendanceInformationList  = jdbcTemplate.query(SQL_SELECT_ALL_ATTENDANCE_INFO_BY_HOLIDAY,  new Object[] {fromDate, toDate}, new BeanPropertyRowMapper<AlmAttendanceInformation> (AlmAttendanceInformation.class));
        }
        catch(Exception ex)
        {
            logger.error("almAttendanceInformationList Msg: "+ex.getMessage());
        }
            return almAttendanceInformationList;
    }

    public void updateAttendanceInfoByStoredProc(long p_employee_info_id)
        throws Exception
    {
        logger.info("Employee Attendance Info: " + SQL_INSERT_EMP_ATT_GEN_PROC);
        Connection conn =   jdbcTemplate.getDataSource().getConnection();
        CallableStatement callStat = null;
        callStat = conn.prepareCall(SQL_INSERT_EMP_ATT_GEN_PROC);
        callStat.setLong(1,p_employee_info_id);
        callStat.registerOutParameter(2, java.sql.Types.VARCHAR);
        callStat.registerOutParameter(3, java.sql.Types.VARCHAR);
        callStat.execute();

    }

    public void updateShiftInfoByStoredProc(long in_alm_shift_setup_id)
        throws Exception
    {
        logger.info("Shift Time updated: " + SQL_UPDATE_ATT_SHIFT_GEN_PROC);
        Connection conn =   jdbcTemplate.getDataSource().getConnection();
        CallableStatement callStat = null;
        callStat = conn.prepareCall(SQL_UPDATE_ATT_SHIFT_GEN_PROC);
        callStat.setLong(1, in_alm_shift_setup_id);
        callStat.registerOutParameter(2, java.sql.Types.VARCHAR);
        callStat.registerOutParameter(3, java.sql.Types.VARCHAR);
        callStat.execute();
    }

    public void updateHolidayInfoByStoredProc(Date in_start_date, Date in_end_date)
        throws Exception
    {
        logger.info("Holiday Updated: " + SQL_UPDATE_HOLIDAY_PROC);
        Connection conn =   jdbcTemplate.getDataSource().getConnection();
        CallableStatement callStat = null;
        callStat = conn.prepareCall(SQL_UPDATE_HOLIDAY_PROC);
        callStat.setDate(1,in_start_date);
        callStat.setDate(2,in_end_date);
        callStat.registerOutParameter(3, java.sql.Types.VARCHAR);
        callStat.registerOutParameter(4, java.sql.Types.VARCHAR);
        callStat.execute();
    }


}
