package gov.step.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.step.app.domain.CcAssignToInstitute;
import gov.step.app.repository.CcAssignToInstituteRepository;
import gov.step.app.repository.search.CcAssignToInstituteSearchRepository;
import gov.step.app.web.rest.util.HeaderUtil;
import gov.step.app.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CcAssignToInstitute.
 */
@RestController
@RequestMapping("/api")
public class CcAssignToInstituteResource {

    private final Logger log = LoggerFactory.getLogger(CcAssignToInstituteResource.class);

    @Inject
    private CcAssignToInstituteRepository ccAssignToInstituteRepository;

    @Inject
    private CcAssignToInstituteSearchRepository ccAssignToInstituteSearchRepository;

    /**
     * POST  /ccAssignToInstitutes -> Create a new ccAssignToInstitute.
     */
    @RequestMapping(value = "/ccAssignToInstitutes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CcAssignToInstitute> createCcAssignToInstitute(@Valid @RequestBody CcAssignToInstitute ccAssignToInstitute) throws URISyntaxException {
        log.debug("REST request to save CcAssignToInstitute : {}", ccAssignToInstitute);
        if (ccAssignToInstitute.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new ccAssignToInstitute cannot already have an ID").body(null);
        }
        CcAssignToInstitute result = ccAssignToInstituteRepository.save(ccAssignToInstitute);
        ccAssignToInstituteSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/ccAssignToInstitutes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("ccAssignToInstitute", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ccAssignToInstitutes -> Updates an existing ccAssignToInstitute.
     */
    @RequestMapping(value = "/ccAssignToInstitutes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CcAssignToInstitute> updateCcAssignToInstitute(@Valid @RequestBody CcAssignToInstitute ccAssignToInstitute) throws URISyntaxException {
        log.debug("REST request to update CcAssignToInstitute : {}", ccAssignToInstitute);
        if (ccAssignToInstitute.getId() == null) {
            return createCcAssignToInstitute(ccAssignToInstitute);
        }
        CcAssignToInstitute result = ccAssignToInstituteRepository.save(ccAssignToInstitute);
        ccAssignToInstituteSearchRepository.save(ccAssignToInstitute);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("ccAssignToInstitute", ccAssignToInstitute.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ccAssignToInstitutes -> get all the ccAssignToInstitutes.
     */
    @RequestMapping(value = "/ccAssignToInstitutes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CcAssignToInstitute>> getAllCcAssignToInstitutes(Pageable pageable)
        throws URISyntaxException {
        Page<CcAssignToInstitute> page = ccAssignToInstituteRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ccAssignToInstitutes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /ccAssignToInstitutes/:id -> get the "id" ccAssignToInstitute.
     */



    @RequestMapping(value = "/ccAssignToInstitutes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CcAssignToInstitute> getCcAssignToInstitute(@PathVariable Long id) {
        log.debug("REST request to get CcAssignToInstitute : {}", id);
        return Optional.ofNullable(ccAssignToInstituteRepository.findOne(id))
            .map(ccAssignToInstitute -> new ResponseEntity<>(
                ccAssignToInstitute,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /ccAssignToInstitutes/:id -> delete the "id" ccAssignToInstitute.
     */
    @RequestMapping(value = "/ccAssignToInstitutes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCcAssignToInstitute(@PathVariable Long id) {
        log.debug("REST request to delete CcAssignToInstitute : {}", id);
        ccAssignToInstituteRepository.delete(id);
        ccAssignToInstituteSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("ccAssignToInstitute", id.toString())).build();
    }

    /**
     * SEARCH  /_search/ccAssignToInstitutes/:query -> search for the ccAssignToInstitute corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/ccAssignToInstitutes/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CcAssignToInstitute> searchCcAssignToInstitutes(@PathVariable String query) {
        return StreamSupport
            .stream(ccAssignToInstituteSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
