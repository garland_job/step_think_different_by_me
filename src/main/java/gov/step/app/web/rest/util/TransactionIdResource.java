package gov.step.app.web.rest.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by leads on 1/4/16.
 */
public class TransactionIdResource {

    public String getGeneratedid(String prefixOfId){

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        long timeMilli = date.getTime();
        String generatedId = prefixOfId+timeMilli;
        return generatedId;
    }

    public String getGeneratedidWithoutPrefix(){

//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        Date currentDate = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        int year = calendar.get(Calendar.YEAR);
        //Add one to month {0 - 11}
        int month = calendar.get(Calendar.MONTH) + 1;
        int dateValue = calendar.get(Calendar.DATE);
        int hourValue = calendar.get(Calendar.HOUR);
        int minValue = calendar.get(Calendar.MINUTE);
        int secValue = calendar.get(Calendar.SECOND);

        String generatedId = year + month + dateValue + hourValue + minValue + secValue+"";

        return generatedId;
    }
}
