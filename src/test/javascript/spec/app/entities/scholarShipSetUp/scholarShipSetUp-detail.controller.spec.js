'use strict';

describe('ScholarShipSetUp Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockScholarShipSetUp;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockScholarShipSetUp = jasmine.createSpy('MockScholarShipSetUp');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'ScholarShipSetUp': MockScholarShipSetUp
        };
        createController = function() {
            $injector.get('$controller')("ScholarShipSetUpDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'stepApp:scholarShipSetUpUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
