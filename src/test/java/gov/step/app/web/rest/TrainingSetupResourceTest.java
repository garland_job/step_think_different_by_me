
/*
package gov.step.app.web.rest;

import gov.step.app.Application;
import gov.step.app.domain.TrainingSetup;
import gov.step.app.repository.TrainingSetupRepository;
import gov.step.app.repository.search.TrainingSetupSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


*/
/**
 * Test class for the TrainingSetupResource REST controller.
 *
 * @see TrainingSetupResource
 *//*

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TrainingSetupResourceTest {

    private static final String DEFAULT_TRAIN_ID = "AAAAA";
    private static final String UPDATED_TRAIN_ID = "BBBBB";
    private static final String DEFAULT_TAIN_TYPE = "AAAAA";
    private static final String UPDATED_TAIN_TYPE = "BBBBB";
    private static final String DEFAULT_TRAIN_NAME = "AAAAA";
    private static final String UPDATED_TRAIN_NAME = "BBBBB";
    private static final String DEFAULT_INSTITUTE_NAME = "AAAAA";
    private static final String UPDATED_INSTITUTE_NAME = "BBBBB";

    private static final LocalDate DEFAULT_FROM_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FROM_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_TO_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TO_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_REMARKS = "AAAAA";
    private static final String UPDATED_REMARKS = "BBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_CREATE_BY = 1L;
    private static final Long UPDATED_CREATE_BY = 2L;

    private static final Long DEFAULT_UPDATE_BY = 1L;
    private static final Long UPDATED_UPDATE_BY = 2L;

    private static final LocalDate DEFAULT_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private TrainingSetupRepository trainingSetupRepository;

    @Inject
    private TrainingSetupSearchRepository trainingSetupSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTrainingSetupMockMvc;

    private TrainingSetup trainingSetup;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TrainingSetupResource trainingSetupResource = new TrainingSetupResource();
        ReflectionTestUtils.setField(trainingSetupResource, "trainingSetupRepository", trainingSetupRepository);
        ReflectionTestUtils.setField(trainingSetupResource, "trainingSetupSearchRepository", trainingSetupSearchRepository);
        this.restTrainingSetupMockMvc = MockMvcBuilders.standaloneSetup(trainingSetupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        trainingSetup = new TrainingSetup();
        trainingSetup.setTrainId(DEFAULT_TRAIN_ID);
        trainingSetup.setTainType(DEFAULT_TAIN_TYPE);
        trainingSetup.setTrainName(DEFAULT_TRAIN_NAME);
        trainingSetup.setInstituteName(DEFAULT_INSTITUTE_NAME);
        trainingSetup.setFromDate(DEFAULT_FROM_DATE);
        trainingSetup.setToDate(DEFAULT_TO_DATE);
        trainingSetup.setRemarks(DEFAULT_REMARKS);
        trainingSetup.setCreateDate(DEFAULT_CREATE_DATE);
        trainingSetup.setCreateBy(DEFAULT_CREATE_BY);
        trainingSetup.setUpdateBy(DEFAULT_UPDATE_BY);
        trainingSetup.setUpdateDate(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void createTrainingSetup() throws Exception {
        int databaseSizeBeforeCreate = trainingSetupRepository.findAll().size();

        // Create the TrainingSetup

        restTrainingSetupMockMvc.perform(post("/api/trainingSetups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainingSetup)))
                .andExpect(status().isCreated());

        // Validate the TrainingSetup in the database
        List<TrainingSetup> trainingSetups = trainingSetupRepository.findAll();
        assertThat(trainingSetups).hasSize(databaseSizeBeforeCreate + 1);
        TrainingSetup testTrainingSetup = trainingSetups.get(trainingSetups.size() - 1);
        assertThat(testTrainingSetup.getTrainId()).isEqualTo(DEFAULT_TRAIN_ID);
        assertThat(testTrainingSetup.getTainType()).isEqualTo(DEFAULT_TAIN_TYPE);
        assertThat(testTrainingSetup.getTrainName()).isEqualTo(DEFAULT_TRAIN_NAME);
        assertThat(testTrainingSetup.getInstituteName()).isEqualTo(DEFAULT_INSTITUTE_NAME);
        assertThat(testTrainingSetup.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
        assertThat(testTrainingSetup.getToDate()).isEqualTo(DEFAULT_TO_DATE);
        assertThat(testTrainingSetup.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testTrainingSetup.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testTrainingSetup.getCreateBy()).isEqualTo(DEFAULT_CREATE_BY);
        assertThat(testTrainingSetup.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testTrainingSetup.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllTrainingSetups() throws Exception {
        // Initialize the database
        trainingSetupRepository.saveAndFlush(trainingSetup);

        // Get all the trainingSetups
        restTrainingSetupMockMvc.perform(get("/api/trainingSetups"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(trainingSetup.getId().intValue())))
                .andExpect(jsonPath("$.[*].trainId").value(hasItem(DEFAULT_TRAIN_ID.toString())))
                .andExpect(jsonPath("$.[*].tainType").value(hasItem(DEFAULT_TAIN_TYPE.toString())))
                .andExpect(jsonPath("$.[*].trainName").value(hasItem(DEFAULT_TRAIN_NAME.toString())))
                .andExpect(jsonPath("$.[*].instituteName").value(hasItem(DEFAULT_INSTITUTE_NAME.toString())))
                .andExpect(jsonPath("$.[*].fromDate").value(hasItem(DEFAULT_FROM_DATE.toString())))
                .andExpect(jsonPath("$.[*].toDate").value(hasItem(DEFAULT_TO_DATE.toString())))
                .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
                .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
                .andExpect(jsonPath("$.[*].createBy").value(hasItem(DEFAULT_CREATE_BY.intValue())))
                .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY.intValue())))
                .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getTrainingSetup() throws Exception {
        // Initialize the database
        trainingSetupRepository.saveAndFlush(trainingSetup);

        // Get the trainingSetup
        restTrainingSetupMockMvc.perform(get("/api/trainingSetups/{id}", trainingSetup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(trainingSetup.getId().intValue()))
            .andExpect(jsonPath("$.trainId").value(DEFAULT_TRAIN_ID.toString()))
            .andExpect(jsonPath("$.tainType").value(DEFAULT_TAIN_TYPE.toString()))
            .andExpect(jsonPath("$.trainName").value(DEFAULT_TRAIN_NAME.toString()))
            .andExpect(jsonPath("$.instituteName").value(DEFAULT_INSTITUTE_NAME.toString()))
            .andExpect(jsonPath("$.fromDate").value(DEFAULT_FROM_DATE.toString()))
            .andExpect(jsonPath("$.toDate").value(DEFAULT_TO_DATE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createBy").value(DEFAULT_CREATE_BY.intValue()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY.intValue()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTrainingSetup() throws Exception {
        // Get the trainingSetup
        restTrainingSetupMockMvc.perform(get("/api/trainingSetups/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrainingSetup() throws Exception {
        // Initialize the database
        trainingSetupRepository.saveAndFlush(trainingSetup);

		int databaseSizeBeforeUpdate = trainingSetupRepository.findAll().size();

        // Update the trainingSetup
        trainingSetup.setTrainId(UPDATED_TRAIN_ID);
        trainingSetup.setTainType(UPDATED_TAIN_TYPE);
        trainingSetup.setTrainName(UPDATED_TRAIN_NAME);
        trainingSetup.setInstituteName(UPDATED_INSTITUTE_NAME);
        trainingSetup.setFromDate(UPDATED_FROM_DATE);
        trainingSetup.setToDate(UPDATED_TO_DATE);
        trainingSetup.setRemarks(UPDATED_REMARKS);
        trainingSetup.setCreateDate(UPDATED_CREATE_DATE);
        trainingSetup.setCreateBy(UPDATED_CREATE_BY);
        trainingSetup.setUpdateBy(UPDATED_UPDATE_BY);
        trainingSetup.setUpdateDate(UPDATED_UPDATE_DATE);

        restTrainingSetupMockMvc.perform(put("/api/trainingSetups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(trainingSetup)))
                .andExpect(status().isOk());

        // Validate the TrainingSetup in the database
        List<TrainingSetup> trainingSetups = trainingSetupRepository.findAll();
        assertThat(trainingSetups).hasSize(databaseSizeBeforeUpdate);
        TrainingSetup testTrainingSetup = trainingSetups.get(trainingSetups.size() - 1);
        assertThat(testTrainingSetup.getTrainId()).isEqualTo(UPDATED_TRAIN_ID);
        assertThat(testTrainingSetup.getTainType()).isEqualTo(UPDATED_TAIN_TYPE);
        assertThat(testTrainingSetup.getTrainName()).isEqualTo(UPDATED_TRAIN_NAME);
        assertThat(testTrainingSetup.getInstituteName()).isEqualTo(UPDATED_INSTITUTE_NAME);
        assertThat(testTrainingSetup.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
        assertThat(testTrainingSetup.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testTrainingSetup.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testTrainingSetup.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testTrainingSetup.getCreateBy()).isEqualTo(UPDATED_CREATE_BY);
        assertThat(testTrainingSetup.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testTrainingSetup.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void deleteTrainingSetup() throws Exception {
        // Initialize the database
        trainingSetupRepository.saveAndFlush(trainingSetup);

		int databaseSizeBeforeDelete = trainingSetupRepository.findAll().size();

        // Get the trainingSetup
        restTrainingSetupMockMvc.perform(delete("/api/trainingSetups/{id}", trainingSetup.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TrainingSetup> trainingSetups = trainingSetupRepository.findAll();
        assertThat(trainingSetups).hasSize(databaseSizeBeforeDelete - 1);
    }
}
*/
