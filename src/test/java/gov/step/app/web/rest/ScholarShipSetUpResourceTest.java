
/*
package gov.step.app.web.rest;

import gov.step.app.Application;
import gov.step.app.domain.ScholarShipSetUp;
import gov.step.app.repository.ScholarShipSetUpRepository;
import gov.step.app.repository.search.ScholarShipSetUpSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


*/
/**
 * Test class for the ScholarShipSetUpResource REST controller.
 *
 * @see ScholarShipSetUpResource
 *//*

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ScholarShipSetUpResourceTest {

    private static final String DEFAULT_SCHOLAR_ID = "AAAAA";
    private static final String UPDATED_SCHOLAR_ID = "BBBBB";
    private static final String DEFAULT_SCHOLAR_NAME = "AAAAA";
    private static final String UPDATED_SCHOLAR_NAME = "BBBBB";
    private static final String DEFAULT_STIPHEN = "AAAAA";
    private static final String UPDATED_STIPHEN = "BBBBB";
    private static final String DEFAULT_GIVEN_BY = "AAAAA";
    private static final String UPDATED_GIVEN_BY = "BBBBB";
    private static final String DEFAULT_DURATION = "AAAAA";
    private static final String UPDATED_DURATION = "BBBBB";
    private static final String DEFAULT_RUN_INSTITUTE = "AAAAA";
    private static final String UPDATED_RUN_INSTITUTE = "BBBBB";
    private static final String DEFAULT_REMARKS = "AAAAA";
    private static final String UPDATED_REMARKS = "BBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_CREATE_BY = 1L;
    private static final Long UPDATED_CREATE_BY = 2L;

    private static final Long DEFAULT_UPDATE_BY = 1L;
    private static final Long UPDATED_UPDATE_BY = 2L;

    private static final LocalDate DEFAULT_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private ScholarShipSetUpRepository scholarShipSetUpRepository;

    @Inject
    private ScholarShipSetUpSearchRepository scholarShipSetUpSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restScholarShipSetUpMockMvc;

    private ScholarShipSetUp scholarShipSetUp;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ScholarShipSetUpResource scholarShipSetUpResource = new ScholarShipSetUpResource();
        ReflectionTestUtils.setField(scholarShipSetUpResource, "scholarShipSetUpRepository", scholarShipSetUpRepository);
        ReflectionTestUtils.setField(scholarShipSetUpResource, "scholarShipSetUpSearchRepository", scholarShipSetUpSearchRepository);
        this.restScholarShipSetUpMockMvc = MockMvcBuilders.standaloneSetup(scholarShipSetUpResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        scholarShipSetUp = new ScholarShipSetUp();
        scholarShipSetUp.setScholarId(DEFAULT_SCHOLAR_ID);
        scholarShipSetUp.setScholarName(DEFAULT_SCHOLAR_NAME);
        scholarShipSetUp.setStiphen(DEFAULT_STIPHEN);
        scholarShipSetUp.setGiven_by(DEFAULT_GIVEN_BY);
        scholarShipSetUp.setDuration(DEFAULT_DURATION);
        scholarShipSetUp.setRun_institute(DEFAULT_RUN_INSTITUTE);
        scholarShipSetUp.setRemarks(DEFAULT_REMARKS);
        scholarShipSetUp.setCreateDate(DEFAULT_CREATE_DATE);
        scholarShipSetUp.setCreateBy(DEFAULT_CREATE_BY);
        scholarShipSetUp.setUpdateBy(DEFAULT_UPDATE_BY);
        scholarShipSetUp.setUpdateDate(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void createScholarShipSetUp() throws Exception {
        int databaseSizeBeforeCreate = scholarShipSetUpRepository.findAll().size();

        // Create the ScholarShipSetUp

        restScholarShipSetUpMockMvc.perform(post("/api/scholarShipSetUps")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(scholarShipSetUp)))
                .andExpect(status().isCreated());

        // Validate the ScholarShipSetUp in the database
        List<ScholarShipSetUp> scholarShipSetUps = scholarShipSetUpRepository.findAll();
        assertThat(scholarShipSetUps).hasSize(databaseSizeBeforeCreate + 1);
        ScholarShipSetUp testScholarShipSetUp = scholarShipSetUps.get(scholarShipSetUps.size() - 1);
        assertThat(testScholarShipSetUp.getScholarId()).isEqualTo(DEFAULT_SCHOLAR_ID);
        assertThat(testScholarShipSetUp.getScholarName()).isEqualTo(DEFAULT_SCHOLAR_NAME);
        assertThat(testScholarShipSetUp.getStiphen()).isEqualTo(DEFAULT_STIPHEN);
        assertThat(testScholarShipSetUp.getGiven_by()).isEqualTo(DEFAULT_GIVEN_BY);
        assertThat(testScholarShipSetUp.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testScholarShipSetUp.getRun_institute()).isEqualTo(DEFAULT_RUN_INSTITUTE);
        assertThat(testScholarShipSetUp.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testScholarShipSetUp.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testScholarShipSetUp.getCreateBy()).isEqualTo(DEFAULT_CREATE_BY);
        assertThat(testScholarShipSetUp.getUpdateBy()).isEqualTo(DEFAULT_UPDATE_BY);
        assertThat(testScholarShipSetUp.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllScholarShipSetUps() throws Exception {
        // Initialize the database
        scholarShipSetUpRepository.saveAndFlush(scholarShipSetUp);

        // Get all the scholarShipSetUps
        restScholarShipSetUpMockMvc.perform(get("/api/scholarShipSetUps"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(scholarShipSetUp.getId().intValue())))
                .andExpect(jsonPath("$.[*].scholarId").value(hasItem(DEFAULT_SCHOLAR_ID.toString())))
                .andExpect(jsonPath("$.[*].scholarName").value(hasItem(DEFAULT_SCHOLAR_NAME.toString())))
                .andExpect(jsonPath("$.[*].stiphen").value(hasItem(DEFAULT_STIPHEN.toString())))
                .andExpect(jsonPath("$.[*].given_by").value(hasItem(DEFAULT_GIVEN_BY.toString())))
                .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.toString())))
                .andExpect(jsonPath("$.[*].run_institute").value(hasItem(DEFAULT_RUN_INSTITUTE.toString())))
                .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS.toString())))
                .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
                .andExpect(jsonPath("$.[*].createBy").value(hasItem(DEFAULT_CREATE_BY.intValue())))
                .andExpect(jsonPath("$.[*].updateBy").value(hasItem(DEFAULT_UPDATE_BY.intValue())))
                .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getScholarShipSetUp() throws Exception {
        // Initialize the database
        scholarShipSetUpRepository.saveAndFlush(scholarShipSetUp);

        // Get the scholarShipSetUp
        restScholarShipSetUpMockMvc.perform(get("/api/scholarShipSetUps/{id}", scholarShipSetUp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(scholarShipSetUp.getId().intValue()))
            .andExpect(jsonPath("$.scholarId").value(DEFAULT_SCHOLAR_ID.toString()))
            .andExpect(jsonPath("$.scholarName").value(DEFAULT_SCHOLAR_NAME.toString()))
            .andExpect(jsonPath("$.stiphen").value(DEFAULT_STIPHEN.toString()))
            .andExpect(jsonPath("$.given_by").value(DEFAULT_GIVEN_BY.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.toString()))
            .andExpect(jsonPath("$.run_institute").value(DEFAULT_RUN_INSTITUTE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createBy").value(DEFAULT_CREATE_BY.intValue()))
            .andExpect(jsonPath("$.updateBy").value(DEFAULT_UPDATE_BY.intValue()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingScholarShipSetUp() throws Exception {
        // Get the scholarShipSetUp
        restScholarShipSetUpMockMvc.perform(get("/api/scholarShipSetUps/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateScholarShipSetUp() throws Exception {
        // Initialize the database
        scholarShipSetUpRepository.saveAndFlush(scholarShipSetUp);

		int databaseSizeBeforeUpdate = scholarShipSetUpRepository.findAll().size();

        // Update the scholarShipSetUp
        scholarShipSetUp.setScholarId(UPDATED_SCHOLAR_ID);
        scholarShipSetUp.setScholarName(UPDATED_SCHOLAR_NAME);
        scholarShipSetUp.setStiphen(UPDATED_STIPHEN);
        scholarShipSetUp.setGiven_by(UPDATED_GIVEN_BY);
        scholarShipSetUp.setDuration(UPDATED_DURATION);
        scholarShipSetUp.setRun_institute(UPDATED_RUN_INSTITUTE);
        scholarShipSetUp.setRemarks(UPDATED_REMARKS);
        scholarShipSetUp.setCreateDate(UPDATED_CREATE_DATE);
        scholarShipSetUp.setCreateBy(UPDATED_CREATE_BY);
        scholarShipSetUp.setUpdateBy(UPDATED_UPDATE_BY);
        scholarShipSetUp.setUpdateDate(UPDATED_UPDATE_DATE);

        restScholarShipSetUpMockMvc.perform(put("/api/scholarShipSetUps")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(scholarShipSetUp)))
                .andExpect(status().isOk());

        // Validate the ScholarShipSetUp in the database
        List<ScholarShipSetUp> scholarShipSetUps = scholarShipSetUpRepository.findAll();
        assertThat(scholarShipSetUps).hasSize(databaseSizeBeforeUpdate);
        ScholarShipSetUp testScholarShipSetUp = scholarShipSetUps.get(scholarShipSetUps.size() - 1);
        assertThat(testScholarShipSetUp.getScholarId()).isEqualTo(UPDATED_SCHOLAR_ID);
        assertThat(testScholarShipSetUp.getScholarName()).isEqualTo(UPDATED_SCHOLAR_NAME);
        assertThat(testScholarShipSetUp.getStiphen()).isEqualTo(UPDATED_STIPHEN);
        assertThat(testScholarShipSetUp.getGiven_by()).isEqualTo(UPDATED_GIVEN_BY);
        assertThat(testScholarShipSetUp.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testScholarShipSetUp.getRun_institute()).isEqualTo(UPDATED_RUN_INSTITUTE);
        assertThat(testScholarShipSetUp.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testScholarShipSetUp.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testScholarShipSetUp.getCreateBy()).isEqualTo(UPDATED_CREATE_BY);
        assertThat(testScholarShipSetUp.getUpdateBy()).isEqualTo(UPDATED_UPDATE_BY);
        assertThat(testScholarShipSetUp.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void deleteScholarShipSetUp() throws Exception {
        // Initialize the database
        scholarShipSetUpRepository.saveAndFlush(scholarShipSetUp);

		int databaseSizeBeforeDelete = scholarShipSetUpRepository.findAll().size();

        // Get the scholarShipSetUp
        restScholarShipSetUpMockMvc.perform(delete("/api/scholarShipSetUps/{id}", scholarShipSetUp.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ScholarShipSetUp> scholarShipSetUps = scholarShipSetUpRepository.findAll();
        assertThat(scholarShipSetUps).hasSize(databaseSizeBeforeDelete - 1);
    }
}
*/
